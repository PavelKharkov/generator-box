import 'Libs/mobile-detect/sync.js';
import 'Libs/platform/sync.js';

if (__HAS_VUE__) {
    require('Libs/vue/sync.js');
}