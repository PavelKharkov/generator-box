let importFunc;
let popupMagnificTrigger;

if (__IS_DISABLED_MODULE__) {
    let popupMagnificTriggered;

    require('./style.scss');

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const popupMagnificCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
            .then(modules => {
                chunks.popups = true;
                popupMagnificTriggered = true;

                const AppPopupMagnific = modules.default;
                if (resolve) resolve(AppPopupMagnific);
            });
    };

    const initPopupMagnific = event => {
        if (popupMagnificTriggered) return;

        event.preventDefault();
        event.currentTarget.dataset.popupMagnificTriggered = '';

        emitInit('popupMagnific');
    };

    popupMagnificTrigger = (popupMagnificItems, popupMagnificTriggers = ['click']) => {
        if (__IS_SYNC__) return;

        const items = (popupMagnificItems instanceof Node) ? [popupMagnificItems] : popupMagnificItems;
        if (items.length === 0) return;

        items.forEach(item => {
            popupMagnificTriggers.forEach(trigger => {
                item.addEventListener(trigger, initPopupMagnific, {once: true});
            });
        });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.popups) {
            popupMagnificCallback(resolve);
            return;
        }

        onInit('popupMagnific', () => {
            popupMagnificCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('popupMagnific');
        });

        const popupMagnificItems = document.querySelectorAll('[data-popup]');
        popupMagnificTrigger(popupMagnificItems);
    });
}

export default importFunc;
export {popupMagnificTrigger};