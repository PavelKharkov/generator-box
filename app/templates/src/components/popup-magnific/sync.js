import jQuery from 'Libs/jquery'; //Используется для плагина Magnific Popup
import 'Libs/magnific-popup';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'popupMagnific';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const popupPlugin = jQuery.magnificPopup;
const popupPluginInstance = popupPlugin.instance;

//Классы и селекторы
const overflowClass = 'overflow-hidden';
const popupClass = moduleName;
const pluginPrefix = 'mfp';
const pluginContainerSelector = `.${pluginPrefix}-container`;
const pluginContentSelector = `.${pluginPrefix}-content`;
const mainClass = `${pluginPrefix}-slide-top`;
const popupWrapperSelector = `.${pluginPrefix}-wrap`;
const focusableElements = 'input[type="text"], input[type="tel"], input[type="email"], input[type="password"], textarea, select';
const initializedClass = `${moduleName}-initialized`;

//Другие опции
const removalDelay = Number.parseInt(util.getStyle('--transition-duration'), 10);

//Языковые настройки по умолчанию
const templateOptions = {
    tLoading: `${lang.tLoading}…`,
    inline: {
        tNotFound: lang.tNotFound
    },
    gallery: {
        arrowMarkup: `<button type="button" class="${pluginPrefix}-arrow ${pluginPrefix}-arrow-%dir%" title="%title%"></button>`,
        tPrev: lang.gallery.tPrev,
        tNext: lang.gallery.tNext,
        tCounter: `%curr% ${lang.gallery.tCounter} %total%`
    },
    closeMarkup: `<button type="button" class="${pluginPrefix}-close close" title="${lang.closeMarkup} (Esc)"></button>`
};
util.extend(popupPlugin.defaults, templateOptions);

//Опции по умолчанию
const defaultOptions = {
    mainClass,
    removalDelay,
    closeOnBgClick: false,
    disableCloseOnMousedown: true,
    focusOnShow: true,
    trigger: 'click'
};

let currentScroll = 0;
let isMouseDown = false;
let currentPopup;
const scrollBrowsers = !(
    (util.browser === 'microsoft-edge') ||
    (util.browser === 'ie') ||
    (util.browser === 'uc-browser')
);

/**
 * @function documentMousedown
 * @desc Функция, вызываемая при вызове события mousedown при включении опции disableCloseOnMousedown.
 * @param {Object} event - Браузерное событие mousedown.
 * @returns {undefined}
 * @ignore
 */
const documentMousedown = event => {
    if ((event.target !== document.querySelector(pluginContentSelector)) &&
        (event.target !== document.querySelector(pluginContainerSelector))) return;
    isMouseDown = true;
};

/**
 * @function documentMouseup
 * @desc Функция, вызываемая при вызове события mouseup при включении опции disableCloseOnMousedown.
 * @returns {undefined}
 * @ignore
 */
const documentMouseup = () => {
    util.defer(() => {
        isMouseDown = false;
    });
};

/**
 * @class AppPopupMagnific
 * @memberof components
 * @requires libs.jquery
 * @requires libs.magnific-popup
 * @classdesc Модуль попапов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {(HTMLElement|string)} element - Элемент попапа или HTML-контент попапа.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.target] - Идентификатор всплывающего элемента или контент всплывающего элемента.
 * @param {boolean} [options.disableCloseOnMousedown=true] - Запретить закрытие попапа, если курсор мыши был зажат на области попапа.
 * @param {boolean} [options.focusOnShow=true] - Устанавливать фокус на первое найденое поле ввода внутри попапа после его открытия.
 * @param {(string|boolean)} [options.fixedContentPos] - Фиксировать ли скролл страницы при открытии попапа.
 * @param {string} [options.mainClass='mfp-slide-top'] - Дополнительный класс попапа.
 * @param {string} [options.popupType] - Тип попапа, указывается в data-атрибуте у элемента html.
 * @param {string} [options.trigger='click'] - Событие для управляющих элементов, по которому должен открываться попап.
 * @param {*} [options...] - Другие опции плагина [Magnific popup]{@link libs.magnific-popup}.
 * @example <caption>Активация с помощью ссылки на открытие попапа</caption>
 * const popupInstance = new app.PopupMagnific(document.querySelector('.popup-link'), {
 *     disableCloseOnMousedown: false,
 *     removalDelay: 0
 * });
 * console.log(popupInstance.el); //Выведет HTML-элемент ссылки
 * console.log(popupInstance.popupEl); //Выведет HTML-элемент попапа
 *
 * @example <caption>Активация с помощью элемента попапа</caption>
 * const popupInstance = new app.PopupMagnific(document.querySelector('.popup-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <a href="#some-popup" data-popup>Ссылка на открытие попапа</a>
 *
 * <!--data-popup - селектор по умолчанию-->
 *
 * <!--Также инициализировать экземпляр модуля можно на самом элементе попапа-->
 * <div class="popup" id="some-popup" data-popup>Текст попапа</div>
 *
 * @example <caption>Инициализация кнопок управления</caption>
 * <!--HTML-->
 * <!--При инициализации самого элемента попапа, будет происходить поиск элементов управления этим попапом-->
 * <a href="#some-popup" data-popup-control>Ссылка на открытие попапа</a>
 *
 * <div class="popup" id="some-popup" data-popup>Текст попапа</div>
 *
 * @example <caption>Активация с помощью хеша</caption>
 * <!--HTML-->
 * <div id="some-id" data-hash-action="popupMagnific">Текст попапа</div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-popup-options='{"popupType": "test"}'>Текст попапа</div>
 */
const AppPopupMagnific = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        const isHTMLElement = el instanceof HTMLElement;

        //Добавление опций из data-атрибута
        if (isHTMLElement) {
            const popupOptionsData = el.dataset.popupOptions;
            if (popupOptionsData) {
                const dataOptions = util.stringToJSON(popupOptionsData);
                if (!dataOptions) util.error('incorrect data-popup-options format');
                util.extend(this.options, dataOptions);
            }
        }

        let target = this.options.target;

        /**
         * @member {string} AppPopupMagnific#target
         * @memberof components
         * @desc Идентификатор попапа.
         * @readonly
         */
        Object.defineProperty(this, 'target', {
            enumerable: true,
            get: () => target
        });

        if (!target) {
            if (isHTMLElement) {
                const linkHref = el.getAttribute('href');
                const popupData = el.dataset.popup;

                target = ((popupData === '') || (popupData === true))
                    ? (linkHref || `#${el.id}`)
                    : (popupData || linkHref || `#${el.id}`);
            } else {
                target = el;
            }

            this.options.target = target;
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppPopupMagnific#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let isShown = false;

        /**
         * @member {boolean} AppPopupMagnific#isShown
         * @memberof components
         * @desc Показан ли попап.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let controls = [];

        /**
         * @member {Array.<HTMLElement>} AppPopupMagnific#controls
         * @memberof components
         * @desc Коллекция элементов, по нажатию на которые показывается попап.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const controlsSelectors = [
            `[data-popup][href="${target}"]`,
            `[data-popup="${target}"]`,
            `[data-popup-control][href="${target}"]`,
            `[data-popup-control="${target}"]`
        ];
        const controlsElements = util.attempt(() => document.querySelectorAll(controlsSelectors.join(',')));
        if (!(controlsElements instanceof Error) && (controlsElements.length > 0)) {
            controls = [...controlsElements];
        }
        if (
            isHTMLElement &&
            !controls.includes(el) &&
            ((el.getAttribute('href') === target) || (el.dataset.popup === target))
        ) {
            controls.unshift(el);
        }

        let popupEl = el;

        /**
         * @member {HTMLElement} AppPopupMagnific#popupEl
         * @memberof components
         * @desc Элемент попапа.
         * @readonly
         */
        Object.defineProperty(this, 'popupEl', {
            enumerable: true,
            get: () => popupEl
        });

        /**
         * @function setSrc
         * @desc Устанавливает показываемый элемент для плагина.
         * @throws Выводит ошибку, если элемент попапа не найден.
         * @returns {undefined}
         * @ignore
         */
        const setSrc = () => {
            if (!params.items) {
                params.items = {};
            }
            if (!params.items.src) {
                if (util.isAbsoluteUrl(target)) {
                    if (!params.type) {
                        params.type = 'iframe';
                    }
                    params.items.src = target;
                    return;
                }

                let popupElement = util.attempt(() => document.querySelector(target));
                if (popupElement instanceof Error) {
                    popupElement = target;
                } else if (!popupElement) {
                    util.error({
                        message: `${target} not found`,
                        element: target
                    });
                }

                if (popupElement) {
                    if (typeof popupElement === 'string') {
                        const wrapper = document.createElement('div');
                        wrapper.innerHTML = util.unescapeHtml(popupElement.trim());
                        popupEl = wrapper.firstChild;
                    } else {
                        popupEl = popupElement;
                    }

                    params.items.src = jQuery(popupEl);
                } else {
                    util.error({
                        message: `can't show popup with content: ${popupElement}`,
                        element: popupElement
                    });
                }
            }
        };

        params.callbacks = util.isObject(params.callbacks) ? params.callbacks : {};
        params.callbacks.open = () => {
            this.constructor.defaultShow(params);

            isShown = true;
        };
        params.callbacks.close = () => {
            this.constructor.defaultHide(params);

            if (!isShown) return;

            isShown = false;

            this.hide();
        };

        //Добавлять опцию items.src, если это не попап галереи
        if (!params.gallery || (params.gallery && !params.gallery.enabled)) {
            setSrc();
        }

        if (popupEl instanceof HTMLElement) {
            if (!popupEl.modules) {
                popupEl.modules = {};
            }
            if (popupEl.modules[moduleName]) {
                util.error({
                    message: 'popup module already initialized on this element',
                    element: popupEl
                });
            }

            popupEl.modules[moduleName] = this;
        }

        let activeControl;

        /**
         * @member {HTMLElement} AppPopupMagnific#activeControl
         * @memberof components
         * @desc Ссылка, с помощью которой открывался текущий попап.
         * @readonly
         */
        Object.defineProperty(this, 'activeControl', {
            enumerable: true,
            get: () => activeControl
        });

        const addControl = (control, newControl = true) => {
            if (newControl) controls.push(control);

            if (!control.modules) {
                control.modules = {};
            }
            control.modules[moduleName] = this;
            control.classList.add(initializedClass);

            control.addEventListener(params.trigger, event => {
                event.preventDefault();

                activeControl = control;
                this.show();
            });
        };

        /**
         * @function AppPopupMagnific#addControl
         * @memberof components
         * @desc Инициализирует новый элемент для открытия попапа.
         * @param {HTMLElement} [control] - Элемент, по нажатию на который, должен открываться попап.
         * @param {boolean} [newControl=true] - Добавлять элемент в коллекцию элементов управления.
         * @returns {undefined}
         * @readonly
         * @example
         * const link = document.querySelector('.popup-link');
         * popupInstance.addControl(link);
         */
        Object.defineProperty(this, 'addControl', {
            enumerable: true,
            get: () => addControl
        });

        if ((controls.length > 0) && params.trigger) {
            controls.forEach(control => {
                addControl(control, false);
            });
        }

        let toggleTimeout;

        this.on({
            /**
             * @event AppPopupMagnific#show
             * @memberof components
             * @desc Показывает попап.
             * @param {Object} [options={}] - Опции показа попапа.
             * @param {*} [options...] - Опции плагина [Magnific popup]{@link libs.magnific-popup}.
             * @fires components.AppPopupMagnific#shown
             * @returns {Object} JQuery-объект показанного попапа или null, если попап уже показан.
             * @example
             * popupInstance.on('show', () => {
             *     console.log('Попап показывается');
             * });
             * console.log(popupInstance.isShown); //false
             * popupInstance.show({
             *     removalDelay: 0
             * });
             * console.log(popupInstance.isShown); //true
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (isShown) return null;
                isShown = true;

                util.defaultsDeep(options, params);

                /**
                 * @function showFunc
                 * @desc Вспомогательная функция показа попапа. Сохраняет экземпляр текущего показанного попапа в app.PopupMagnific.currentPopup.
                 * @returns {Object} Объект с параметрами открытия плагина попапа.
                 * @ignore
                 */
                const showFunc = () => {
                    this.constructor.setCurrentPopup(this);

                    const popupOpenResult = popupPlugin.open(options, options.index);

                    //Инициализация события закрытия попапа при нажатии на подложку
                    if (options.disableCloseOnMousedown) {
                        const containerEl = document.querySelector(pluginContainerSelector);
                        const contentEl = document.querySelector(pluginContentSelector);
                        containerEl.addEventListener('click', event => {
                            if (!isMouseDown || (event.target !== containerEl)) return;

                            this.hide();
                        });
                        contentEl.addEventListener('click', event => {
                            if (!isMouseDown || (event.target !== contentEl)) return;

                            this.hide();
                        });

                        document.addEventListener('mousedown', documentMousedown);
                        document.addEventListener('mouseup', documentMouseup);
                    }

                    return popupOpenResult;
                };

                if (this.constructor.currentPopup && this.constructor.currentPopup.hide) {
                    this.constructor.currentPopup.hide(); //Скрывать текущий открытый попап
                    setTimeout(showFunc, popupPluginInstance.st.removalDelay);
                    return null;
                }

                const showResult = showFunc();
                clearTimeout(toggleTimeout);
                toggleTimeout = setTimeout(() => {
                    if (options.focusOnShow) {
                        const focusableElement = [...popupPluginInstance.content[0].querySelectorAll(focusableElements)]
                            .find(item => {
                                return item.offsetParent;
                            });
                        if (focusableElement) {
                            focusableElement.focus();
                            focusableElement.dataset.autofocus = '';
                        }
                    }

                    this.emit('shown');
                }, popupPluginInstance.st.removalDelay);
                return showResult;
            },

            /**
             * @event AppPopupMagnific#hide
             * @memberof components
             * @desc Скрывает попап.
             * @fires components.AppPopupMagnific#hidden
             * @returns {undefined}
             * @example
             * popupInstance.on('hide', () => {
             *     console.log('Попап скрывается');
             * });
             * console.log(popupInstance.isShown); //true
             * popupInstance.hide();
             * console.log(popupInstance.isShown); //false
             */
            hide() {
                const elSelector = util.attempt(() => document.querySelector(popupWrapperSelector).querySelector(target));
                const elNodeSelector = util.attempt(() => document.querySelector(popupWrapperSelector).contains(target));

                clearTimeout(toggleTimeout);
                toggleTimeout = setTimeout(() => {
                    this.constructor.unsetCurrentPopup();
                    this.emit('hidden');
                }, popupPluginInstance.st.removalDelay);

                if (!isShown) return;

                isShown = false;

                if (params.disableCloseOnMousedown) {
                    document.removeEventListener('mousedown', documentMousedown);
                    document.removeEventListener('mouseup', documentMouseup);
                }

                if (
                    (elSelector && !(elSelector instanceof Error)) ||
                    (elNodeSelector && !(elNodeSelector instanceof Error)) ||
                    ((elSelector instanceof Error) && (!elNodeSelector || (elNodeSelector instanceof Error)))
                ) {
                    popupPlugin.close();
                }
            }
        });

        /**
         * @event AppPopupMagnific#shown
         * @memberof components
         * @desc Вызывается после показа попапа.
         * @returns {undefined}
         * @example
         * popupInstance.on('shown', () => {
         *     console.log('Попап показан');
         * });
         */
        this.onSubscribe(['shown']);

        /**
         * @event AppPopupMagnific#hidden
         * @memberof components
         * @desc Вызывается после скрытия попапа.
         * @returns {undefined}
         * @example
         * popupInstance.on('hidden', () => {
         *     console.log('Попап скрыт');
         * });
         */
        this.onSubscribe('hidden', () => {
            activeControl = null;
        });

        if (popupEl instanceof HTMLElement) {
            popupEl.classList.add(initializedClass);
        }

        const elements = (popupEl instanceof HTMLElement) ? [...controls, popupEl] : controls;
        elements.find(item => {
            if (item.dataset && (typeof item.dataset.popupTriggered !== 'undefined')) {
                delete item.dataset.popupTriggered;
                this.show();
                return true;
            }

            return false;
        });
    }

    /**
     * @function AppPopupMagnific.template
     * @memberof components
     * @desc Базовый шабон попапа.
     * @param {Object} [options] - Опции шаблона.
     * @param {string} [options.popupClass] - Дополнительный класс, добавляемый шаблону попапа.
     * @param {boolean} [options.inner] - Оборачивать ли контент попапа в контейнер.
     * @param {string} [options.content] - Контент попапа.
     * @returns {string} HTML-строку с попапом.
     * @example
     * const popupInstance = new app.PopupMagnific(app.PopupMagnific.template({
     *     popupClass: 'test-popup',
     *     content: 'Контент попапа'
     * }));
     */
    static template(options = {}) {
        let optionsObject = options;
        if (typeof options === 'string') {
            const content = options;
            optionsObject = {content};
        }

        if (!util.isObject(options)) {
            util.typeError(optionsObject, 'options', 'plain object');
        }

        return `
            <div class="${popupClass + (optionsObject.popupClass ? ` ${optionsObject.popupClass}` : '')}">
                ${optionsObject.inner ? `<div class="${popupClass}__inner">` : ''}
                    ${optionsObject.content}
                ${optionsObject.inner ? '</div>' : ''}
            </div>`;
    }

    /**
     * @function AppPopupMagnific.defaultShow
     * @memberof components
     * @desc Функция, по умолчанию вызываемая после открытия попапа.
     * @param {Object} [options] - Опции функции открытия попапа.
     * @param {string} [options.popupType] - Тип попапа, добавляется в data-атрибут элемента html.
     * @param {(string|boolean)} [options.fixedContentPos] - Фиксировать ли скролл страницы при открытии попапа.
     * @returns {undefined}
     */
    static defaultShow(options = {}) {
        if (!util.isObject(options)) {
            util.typeError(options, 'options', 'plain object');
        }

        if (options.popupType) {
            document.documentElement.dataset.popupType = options.popupType;
        }

        popupPluginInstance.contentContainer.get(0)
            .querySelectorAll('svg')
            .forEach(element => { //Фикс svg-элементов
                const innerHtml = element.innerHTML;
                element.innerHTML = innerHtml;
            });

        //TODO:add popupChange state
        //Зафиксировать текущую позицию прокрутки
        if (scrollBrowsers && (options.fixedContentPos !== false)) {
            currentScroll = AppWindow.scrollY();
            document.documentElement.classList.add(overflowClass);
            AppWindow.scrollTop({
                value: currentScroll
            });
        }
    }

    /**
     * @function AppPopupMagnific.defaultHide
     * @memberof components
     * @desc Функция, по умолчанию вызываемая после закрытия попапа.
     * @param {Object} [options] - Опции функции открытия попапа.
     * @param {(string|boolean)} [options.fixedContentPos] - Фиксировать ли скролл страницы при открытии попапа.
     * @returns {undefined}
     */
    static defaultHide(options = {}) {
        if (!util.isObject(options)) {
            util.typeError(options, 'options', 'plain object');
        }

        delete document.documentElement.dataset.popupType;

        //Возвращаться на предыдущую позицию прокрутки
        if (scrollBrowsers && (options.fixedContentPos !== false)) {
            document.documentElement.classList.remove(overflowClass);
            AppWindow.scrollTop({
                value: currentScroll
            });
        }
    }

    /**
     * @function AppPopupMagnific.setCurrentPopup
     * @memberof components
     * @desc Устанавливает текущий показанный экземпляр попапа.
     * @param {Object} [newPopup] - Экземпляр текущего показанного попапа.
     * @returns {undefined}
     */
    static setCurrentPopup(newPopup) {
        currentPopup = newPopup;
    }

    /**
     * @function AppPopupMagnific.unsetCurrentPopup
     * @memberof components
     * @desc Сбрасывает текущий открытый экземпляр попапа.
     * @returns {undefined}
     */
    static unsetCurrentPopup() {
        currentPopup = null;
    }

    /**
     * @function AppPopupMagnific.instance
     * @memberof components
     * @desc Get-функция. Ссылка на экземпляр плагина текущего попапа.
     * @returns {Object}
     */
    static get instance() {
        return popupPluginInstance;
    }

    /**
     * @function AppPopupMagnific.currentPopup
     * @memberof components
     * @desc Get-функция. Ссылка на экземпляр текущего показанного попапа.
     * @returns {Object}
     */
    static get currentPopup() {
        return currentPopup;
    }
});

//Настройка возможности управления с помощью адресной строки
require('Components/url').default.then(AppUrl => {
    AppUrl.registerAction({
        id: moduleName,
        selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
        action: `${moduleName}.show`
    });
});
if (window.location.hash) emitInit('url');

addModule(moduleName, AppPopupMagnific);

//Инициализировать элементы по data-атрибуту
document.querySelectorAll('[data-popup]').forEach(el => {
    if (el.classList.contains(initializedClass)) return;
    new AppPopupMagnific(el); /* eslint-disable-line no-new */
});

export default AppPopupMagnific;
export {AppPopupMagnific};