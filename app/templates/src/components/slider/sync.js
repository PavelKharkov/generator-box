import Swiper from 'Libs/swiper';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import './style.scss';

//Опции
const moduleName = 'slider';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const SliderPlugin = Swiper;

const sliderPrefix = 'slider';
const swiperPrefix = 'swiper';
const wrapperSelector = `.${swiperPrefix}-wrapper`;
const slideSelector = `.${swiperPrefix}-slide`;
const initializedClass = `${sliderPrefix}-initialized`;
const sliderPrevDisabled = `${sliderPrefix}-prev-disabled`;
const sliderNextDisabled = `${sliderPrefix}-next-disabled`;

//Опции по умолчанию
const defaultOptions = {
    goto: {
        duration: 400
    }
};

//Опции плагина слайдера по умолчанию
const defaultPluginOptions = {
    slidesPerView: 1,

    init: true,
    grabCursor: true,
    watchOverflow: true,
    lazy: true,

    breakpoints: {
        [util.media.sm]: {
            slidesPerView: 2
        },
        [util.media.md]: {
            slidesPerView: 3
        },
        [util.media.lg]: {
            slidesPerView: 4
        }
    }
};

const a11yOptions = {
    firstSlideMessage: lang.firstSlideMessage,
    lastSlideMessage: lang.lastSlideMessage,
    prevSlideMessage: lang.prevSlideMessage,
    nextSlideMessage: lang.nextSlideMessage,
    paginationBulletMessage: lang.paginationBulletMessage
};

//TODO:add examples

/**
 * @class AppSlider
 * @memberof components
 * @requires libs.swiper
 * @classdesc Модуль для контентных слайдеров.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент слайдера.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {Object} [options.goto] - Опции события goto.
 * @param {number} [options.goto.duration=400] - Длительность анимации события goto.
 * @param {number} [options.pluginOptions={}] - Опции плагина [Swiper]{@link libs.swiper}.
 */
const AppSlider = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        //TODO:temp test
        this.constructorName = this.constructor.name;
        this.newTarget = new.target;

        Module.checkHTMLElement(el);

        const sliderOptionsData = el.dataset.sliderOptions;
        if (sliderOptionsData) {
            const dataOptions = util.stringToJSON(sliderOptionsData);
            if (!dataOptions) util.error('incorrect data-slider-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        if (
            !this.options.pluginOptions ||
            (util.isObject(this.options.pluginOptions) && (Object.keys(this.options.pluginOptions).length === 0))
        ) {
            this.options.pluginOptions = util.extend({}, defaultPluginOptions);
        }

        this.options.pluginOptions.a11y = a11yOptions;

        /**
         * @member {Object} AppSlider#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const wrapper = el.querySelector(wrapperSelector);
        if (!wrapper) return;

        /**
         * @member {HTMLElement} AppSlider#wrapper
         * @memberof components
         * @desc Контейнер элементов слайдера.
         * @readonly
         */
        Object.defineProperty(this, 'wrapper', {
            enumerable: true,
            value: wrapper
        });

        if (!wrapper.modules) {
            wrapper.modules = {};
        }
        wrapper.modules[moduleName] = this;

        const items = [...wrapper.querySelectorAll(slideSelector)];

        items.forEach(item => {
            if (!item.modules) {
                item.modules = {};
            }
            item.modules[moduleName] = this;
        });

        /**
         * @member {Array.<HTMLElement>} AppSlider#items
         * @memberof components
         * @desc Коллекция элементов слайдера.
         * @readonly
         */
        Object.defineProperty(this, 'items', {
            enumerable: true,
            value: items
        });

        const isInit = params.pluginOptions.init;
        if (isInit) {
            params.pluginOptions.init = false;
        }

        let instance; /* eslint-disable-line prefer-const */

        /**
         * @member {Array.<Object>} AppSlider#instance
         * @memberof components
         * @desc Ссылка на экземпляр плагина слайдера [swiper]{@link libs.swiper}.
         * @readonly
         */
        Object.defineProperty(this, 'instance', {
            enumerable: true,
            get: () => instance
        });

        /**
         * @function setSliderDisabledNavClasses
         * @memberof components
         * @desc Добавляет классы элементу слайдера согласно активным кнопкам навигации.
         * @returns {undefined}
         * @ignore
         */
        const setSliderDisabledNavClasses = () => {
            el.classList[instance.isBeginning ? 'add' : 'remove'](sliderPrevDisabled);
            el.classList[instance.isEnd ? 'add' : 'remove'](sliderNextDisabled);
        };

        this.onSubscribe({
            /**
             * @event AppSlider#initialized
             * @memberof components
             * @desc Вызывается при вызове события 'initialized' плагина [Swiper]{@link libs.swiper}.
             * @returns {undefined}
             * @example
             * sliderInstance.on('initialized', () => {
             *     console.log('Плагин слайдера проинициализировался');
             * });
             */
            initialized() {
                setSliderDisabledNavClasses();
            },

            /**
             * @event AppSlider#slideChange
             * @memberof components
             * @desc Вызывается при вызове события 'slideChange' плагина [Swiper]{@link libs.swiper}.
             * @returns {undefined}
             * @example
             * sliderInstance.on('slideChange', () => {
             *     console.log('Изменен текущий активный набор слайдов');
             * });
             */
            slideChange() {
                setSliderDisabledNavClasses();
            }
        });

        this.on({
            /**
             * @event AppSlider#goto
             * @memberof components
             * @desc Переключает активный слайд по индексу слайда.
             * @param {Object} [options={}] - Опции переключения слайдов.
             * @param {number} [options.index=0] - На какой слайд нужно переключить.
             * @param {number} [options.duration=<значение из опции goto.duration экземпляра>] - Длительность переключения перехода.
             * @returns {undefined}
             * @example
             * sliderInstance.on('goto', () => {
             *     console.log('Активный слайд переключается');
             * });
             * sliderInstance.goto(3);
             */
            goto(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (params.goto) {
                    util.defaultsDeep(options, params.goto);
                }

                instance.slideTo(options.index || 0, options.duration);
            },

            /**
             * @event AppSlider#prev
             * @memberof components
             * @desc Переключает слайдер на предыдущий набор слайдов.
             * @param {Object} [options={}] - Опции переключения слайдов.
             * @param {number} [options.duration=<значение из опции goto.duration экземпляра>] - Длительность переключения перехода.
             * @returns {undefined}
             * @example
             * sliderInstance.on('prev', () => {
             *     console.log('Слайдер переключается на предыдущий набор слайдов');
             * });
             * sliderInstance.prev({duration: 300});
             */
            prev(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (params.goto) {
                    util.defaultsDeep(options, params.goto);
                }

                instance.slidePrev(options.duration);
            },

            /**
             * @event AppSlider#next
             * @memberof components
             * @desc Переключает слайдер на следующий набор слайдов.
             * @param {Object} [options={}] - Опции переключения слайдов.
             * @param {number} [options.duration=<значение из опции goto.duration экземпляра>] - Длительность переключения перехода.
             * @returns {undefined}
             * @example
             * sliderInstance.on('next', () => {
             *     console.log('Слайдер переключается на следующий набор слайдов');
             * });
             * sliderInstance.next({duration: 300});
             */
            next(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (params.goto) {
                    util.defaultsDeep(options, params.goto);
                }

                instance.slideNext(options.duration);
            }
        });

        el.removeAttribute('hidden');
        instance = new SliderPlugin(el, params.pluginOptions);

        instance.on('init', () => {
            this.emit('initialized');

            el.classList.add(initializedClass);
        });
        instance.on('slideChange', () => {
            this.emit('slideChange');
        });

        const init = () => {
            instance.init();
        };

        /**
         * @function AppSlider#init
         * @memberof components
         * @desc Инициализирует экземпляр плагина слайдера.
         * @returns {undefined}
         * @readonly
         */
        Object.defineProperty(this, 'init', {
            enumerable: true,
            value: init
        });

        if (isInit) {
            init();
        }
    }
});

addModule(moduleName, AppSlider);

//Инициализирует элементы по data-атрибуту
document.querySelectorAll('[data-slider]').forEach(el => new AppSlider(el));

export default AppSlider;
export {AppSlider};