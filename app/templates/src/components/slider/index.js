import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const sliderCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "sliders" */ './sync.js'))
        .then(modules => {
            chunks.sliders = true;

            const AppSlider = modules.default;
            resolve(AppSlider);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.sliders) {
        sliderCallback(resolve);
        return;
    }

    onInit('slider', () => {
        sliderCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('slider');
    });
});

export default importFunc;