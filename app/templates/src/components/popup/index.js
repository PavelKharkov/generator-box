import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let popupTriggered;

const popupCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
        .then(modules => {
            chunks.popups = true;
            popupTriggered = true;

            const AppPopup = modules.default;
            if (resolve) resolve(AppPopup);
        });
};

const initPopup = event => {
    if (popupTriggered) return;

    event.preventDefault();
    event.currentTarget.dataset.popupTriggered = '';

    emitInit('popup');
};

const popupTrigger = (popupItems, popupTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (popupItems instanceof Node) ? [popupItems] : popupItems;
    if (items.length === 0) return;

    items.forEach(item => {
        popupTriggers.forEach(trigger => {
            item.addEventListener(trigger, initPopup, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popups) {
        popupCallback(resolve);
        return;
    }

    onInit('popup', () => {
        popupCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('popup');
    });

    const popupItems = document.querySelectorAll('[data-popup]');
    popupTrigger(popupItems);
});

export default importFunc;
export {popupTrigger};