import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'popup';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Классы и селекторы
const overflowClass = 'overflow-hidden';
const popupClass = moduleName;
const initializedClass = `${moduleName}-initialized`;
const wrapperClass = `${moduleName}-wrapper`;
const bgClass = `${wrapperClass}__bg`;
const contentClass = `${wrapperClass}__content`;
const closeClasses = [`${wrapperClass}__close`, 'close'];
const placeholderClass = `${wrapperClass}__placeholder`;
const inactiveClass = 'inactive';
const hidingClass = 'hiding';
const focusableElements = 'input[type="text"], input[type="tel"], input[type="email"], input[type="password"], textarea, select';
const closeButtonSelector = '[data-popup-close]';

//Другие опции
const duration = Number.parseInt(util.getStyle('--transition-duration'), 10);
const unsetDuration = 'initial';

//Опции по умолчанию
const defaultOptions = {
    duration,
    trigger: 'click',
    addClass: '-slide-top',
    closeOnBgClick: false,
    disableCloseOnMousedown: true,
    focusOnShow: true
};

let currentScroll = 0;
let isMouseDown = false;
let currentPopup;
let markupInitialized;

let popupWrapper;
let popupContent;
let popupClose;

/**
 * @function documentMousedown
 * @desc Функция, вызываемая при вызове события mousedown при включении опции disableCloseOnMousedown.
 * @param {Object} event - Браузерное событие mousedown.
 * @returns {undefined}
 * @ignore
 */
const documentMousedown = event => {
    if (event.target !== popupContent) return;
    isMouseDown = true;
};

/**
 * @function documentMouseup
 * @desc Функция, вызываемая при вызове события mouseup при включении опции disableCloseOnMousedown.
 * @returns {undefined}
 * @ignore
 */
const documentMouseup = () => {
    util.defer(() => {
        isMouseDown = false;
    });
};

/**
 * @class AppPopup
 * @memberof components
 * @classdesc Модуль попапов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {(HTMLElement|string)} element - Элемент попапа или HTML-контент попапа.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.target] - Идентификатор всплывающего элемента или контент всплывающего элемента.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность переключения анимации попапа.
 * @param {string} [options.trigger='click'] - Событие для управляющих элементов, по которому должен открываться попап.
 * @param {string} [options.addClass='-slide-top'] - Дополнительный класс.
 * @param {string} [options.popupType] - Тип попапа, указывается в data-атрибуте у элемента html.
 * @param {boolean} [options.disableCloseOnMousedown=true] - Запретить закрытие попапа, если курсор мыши был зажат на области попапа.
 * @param {boolean} [options.focusOnShow=true] - Устанавливать фокус на первое найденое поле ввода внутри попапа после его открытия.
 * @example <caption>Активация с помощью ссылки на открытие попапа</caption>
 * const popupInstance = new app.Popup(document.querySelector('.popup-link'), {
 *     disableCloseOnMousedown: false,
 *     duration: 0
 * });
 * console.log(popupInstance.el); //Выведет HTML-элемент ссылки
 * console.log(popupInstance.popupEl); //Выведет HTML-элемент попапа
 *
 * @example <caption>Активация с помощью элемента попапа</caption>
 * const popupInstance = new app.Popup(document.querySelector('.popup-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <a href="#some-popup" data-popup>Ссылка на открытие попапа</a>
 *
 * <!--data-popup - селектор по умолчанию-->
 *
 * <!--Также инициализировать экземпляр модуля можно на самом элементе попапа-->
 * <div class="popup" id="some-popup" data-popup>Текст попапа</div>
 *
 * @example <caption>Инициализация кнопок управления</caption>
 * <!--HTML-->
 * <!--При инициализации самого элемента попапа, будет происходить поиск элементов управления этим попапом-->
 * <a href="#some-popup" data-popup-control>Ссылка на открытие попапа</a>
 *
 * <div class="popup" id="some-popup" data-popup>Текст попапа</div>
 *
 * @example <caption>Активация с помощью хеша</caption>
 * <!--HTML-->
 * <div id="some-id" data-hash-action="popup">Текст попапа</div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-popup-options='{"popupType": "test"}'>Текст попапа</div>
 */
const AppPopup = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        const isHTMLElement = el instanceof HTMLElement;

        //Добавление опций из data-атрибута
        if (isHTMLElement) {
            const popupOptionsData = el.dataset.popupOptions;
            if (popupOptionsData) {
                const dataOptions = util.stringToJSON(popupOptionsData);
                if (!dataOptions) util.error('incorrect data-popup-options format');
                util.extend(this.options, dataOptions);
            }
        }

        let target = this.options.target;

        /**
         * @member {string} AppPopup#target
         * @memberof components
         * @desc Идентификатор попапа.
         * @readonly
         */
        Object.defineProperty(this, 'target', {
            enumerable: true,
            get: () => target
        });

        if (!target) {
            if (isHTMLElement) {
                const linkHref = el.getAttribute('href');
                const popupData = el.dataset.popup;

                target = ((popupData === '') || (popupData === true))
                    ? (linkHref || `#${el.id}`)
                    : (popupData || linkHref || `#${el.id}`);
            } else {
                target = el;
            }

            this.options.target = target;
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppPopup#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let isShown = false;

        /**
         * @member {boolean} AppPopup#isShown
         * @memberof components
         * @desc Показан ли попап.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let controls = [];

        /**
         * @member {Array.<HTMLElement>} AppPopup#controls
         * @memberof components
         * @desc Коллекция элементов, по нажатию на которые показывается попап.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const controlsSelectors = [
            `[data-popup][href="${target}"]`,
            `[data-popup="${target}"]`,
            `[data-popup-control][href="${target}"]`,
            `[data-popup-control="${target}"]`
        ];
        const controlsElements = util.attempt(() => document.querySelectorAll(controlsSelectors.join(',')));
        if (!(controlsElements instanceof Error) && (controlsElements.length > 0)) {
            controls = [...controlsElements];
        }
        if (
            isHTMLElement &&
            !controls.includes(el) &&
            ((el.getAttribute('href') === target) || (el.dataset.popup === target))
        ) {
            controls.unshift(el);
        }

        let popupEl = el;

        /**
         * @member {HTMLElement} AppPopup#popupEl
         * @memberof components
         * @desc Элемент попапа.
         * @readonly
         */
        Object.defineProperty(this, 'popupEl', {
            enumerable: true,
            get: () => popupEl
        });

        let popupElement = util.attempt(() => document.querySelector(target));
        if (popupElement instanceof Error) {
            popupElement = target;
        } else if (!popupElement) {
            util.error({
                message: `${target} not found`,
                element: target
            });
        }

        if (popupElement) {
            if (typeof popupElement === 'string') {
                const wrapper = document.createElement('div');
                wrapper.innerHTML = util.unescapeHtml(popupElement.trim());
                popupEl = wrapper.firstChild;
            } else {
                popupEl = popupElement;
            }
        } else {
            util.error({
                message: `can't show popup with content: ${popupElement}`,
                element: popupElement
            });
        }

        if (popupEl instanceof HTMLElement) {
            if (!popupEl.modules) {
                popupEl.modules = {};
            }
            if ((popupEl !== el) && popupEl.modules[moduleName]) {
                util.error({
                    message: 'popup module already initialized on this element',
                    element: popupEl
                });
            }

            popupEl.modules[moduleName] = this;
        }

        let activeControl;

        /**
         * @member {HTMLElement} AppPopup#activeControl
         * @memberof components
         * @desc Ссылка, с помощью которой открывался текущий попап.
         * @readonly
         */
        Object.defineProperty(this, 'activeControl', {
            enumerable: true,
            get: () => activeControl
        });

        const addControl = (control, newControl = true) => {
            if (newControl) controls.push(control);

            if (!control.modules) {
                control.modules = {};
            }
            control.modules[moduleName] = this;
            control.classList.add(initializedClass);

            control.addEventListener(params.trigger, event => {
                event.preventDefault();

                activeControl = control;
                this.show();
            });
        };

        /**
         * @function AppPopup#addControl
         * @memberof components
         * @desc Инициализирует новый элемент для открытия попапа.
         * @param {HTMLElement} [control] - Элемент, по нажатию на который, должен открываться попап.
         * @param {boolean} [newControl=true] - Добавлять элемент в коллекцию элементов управления.
         * @returns {undefined}
         * @readonly
         * @example
         * const link = document.querySelector('.popup-link');
         * popupInstance.addControl(link);
         */
        Object.defineProperty(this, 'addControl', {
            enumerable: true,
            get: () => addControl
        });

        if ((controls.length > 0) && params.trigger) {
            controls.forEach(control => {
                addControl(control, false);
            });
        }

        let toggleTimeout;

        const popupPlaceholder = document.createElement('div');
        popupPlaceholder.classList.add(placeholderClass);

        /**
         * @function initEscapeHide
         * @desc Закрывает попап при помощи клавиши Escape.
         * @param {Object} event - Браузерное события нажатия клавиши.
         * @fires components.AppPopup#hide
         * @returns {undefined}
         * @ignore
         */
        const initEscapeHide = event => {
            if (event.key !== 'Escape') return;

            this.hide();
        };

        //TODO:init close with html init
        if (popupEl instanceof HTMLElement) {
            popupEl.querySelectorAll(closeButtonSelector).forEach(button => {
                button.addEventListener('click', () => {
                    this.hide();
                });
            });
        }

        //TODO:docs
        const markupInit = () => {
            if (markupInitialized) return;

            popupWrapper = document.createElement('div');
            popupWrapper.classList.add(wrapperClass, inactiveClass);

            const popupBg = document.createElement('div');
            popupBg.classList.add(bgClass);

            popupContent = document.createElement('div');
            popupContent.classList.add(contentClass);

            popupContent.addEventListener('click', event => {
                if (!isMouseDown || (event.target !== popupContent)) {
                    return;
                }

                this.constructor.currentPopup.hide();
            });

            popupClose = document.createElement('button');
            popupClose.type = 'button';
            popupClose.title = lang.close;
            popupClose.classList.add(...closeClasses);

            popupClose.addEventListener('click', () => {
                this.constructor.currentPopup.hide();
            });

            popupWrapper.append(popupBg, popupContent, popupClose);

            document.body.prepend(popupWrapper);

            markupInitialized = true;
        };

        this.on({
            /**
             * @event AppPopup#show
             * @memberof components
             * @desc Показывает попап.
             * @param {Object} [options={}] - Опции показа попапа.
             * @fires components.AppPopup#shown
             * @returns {undefined}
             * @example
             * popupInstance.on('show', () => {
             *     console.log('Попап показывается');
             * });
             * console.log(popupInstance.isShown); //false
             * popupInstance.show({
             *     duration: 0
             * });
             * console.log(popupInstance.isShown); //true
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (isShown) return;
                isShown = true;

                util.defaultsDeep(options, params);

                markupInit();

                if (this.constructor.currentPopup) {
                    this.constructor.currentPopup.hide({duration: 0});
                }

                util.defer(() => {
                    this.constructor.setCurrentPopup(this);

                    popupEl.after(popupPlaceholder);

                    popupContent.append(popupEl);
                    popupEl.prepend(popupClose);
                    popupEl.removeAttribute('hidden');

                    popupContent.style.transitionDuration = unsetDuration;
                    popupWrapper.style.transitionDuration = unsetDuration;
                    const addClasses = Array.isArray(options.addClass) ? options.addClass : options.addClass.split(' ');
                    popupWrapper.classList.add(...addClasses);
                    popupWrapper.classList.remove(inactiveClass, hidingClass);

                    util.animationDefer(() => {
                        popupContent.style.transitionDuration = `${options.duration}ms`;
                        popupWrapper.style.transitionDuration = `${options.duration}ms`;
                        util.activate(popupWrapper);
                    });

                    if (options.popupType) {
                        document.documentElement.dataset.popupType = options.popupType;
                    }

                    currentScroll = AppWindow.scrollY();
                    document.documentElement.classList.add(overflowClass);
                    AppWindow.scrollTop({
                        value: currentScroll
                    });

                    if (options.disableCloseOnMousedown) {
                        popupContent.addEventListener('click', event => {
                            if (!isMouseDown || (event.target !== popupContent)) {
                                return;
                            }

                            this.hide();
                        });

                        document.addEventListener('mousedown', documentMousedown);
                        document.addEventListener('mouseup', documentMouseup);
                    } else {
                        isMouseDown = true;
                    }

                    clearTimeout(toggleTimeout);
                    toggleTimeout = setTimeout(() => {
                        if (options.focusOnShow) {
                            const focusableElement = [...popupEl.querySelectorAll(focusableElements)]
                                .find(item => {
                                    return item.offsetParent;
                                });
                            if (focusableElement) {
                                focusableElement.focus();
                                focusableElement.dataset.autofocus = '';
                            }
                        }

                        document.addEventListener('keyup', initEscapeHide, {once: true});

                        this.emit('shown');
                    }, options.duration);
                });
            },

            /**
             * @event AppPopup#hide
             * @memberof components
             * @desc Скрывает попап.
             * @param {Object} [options={}] - Опции показа попапа.
             * @fires components.AppPopup#hidden
             * @returns {undefined}
             * @example
             * popupInstance.on('hide', () => {
             *     console.log('Попап скрывается');
             * });
             * console.log(popupInstance.isShown); //true
             * popupInstance.hide({
             *     duration: 0
             * }));
             * console.log(popupInstance.isShown); //false
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (!isShown) return;

                util.defaultsDeep(options, params);

                util.defer(() => {
                    isShown = false;

                    this.constructor.unsetCurrentPopup();

                    popupWrapper.style.transitionDuration = `${options.duration}ms`;
                    popupContent.style.transitionDuration = `${options.duration}ms`;
                    util.deactivate(popupWrapper);
                    popupWrapper.classList.add(hidingClass);
                });

                delete document.documentElement.dataset.popupType;

                document.documentElement.classList.remove(overflowClass);
                AppWindow.scrollTop({
                    value: currentScroll
                });

                if (options.disableCloseOnMousedown) {
                    document.removeEventListener('mousedown', documentMousedown);
                    document.removeEventListener('mouseup', documentMouseup);
                }
                isMouseDown = false;

                clearTimeout(toggleTimeout);

                //TODO:dont hide bg if show popup after another
                const hideFunction = () => {
                    popupEl.hidden = true;
                    popupPlaceholder.after(popupEl);
                    popupPlaceholder.remove();
                    const addClasses = Array.isArray(options.addClass) ? options.addClass : options.addClass.split(' ');
                    popupWrapper.classList.remove(...addClasses, hidingClass);
                    popupWrapper.classList.add(inactiveClass);
                    popupWrapper.append(popupClose);
                    popupContent.style.transitionDuration = unsetDuration;
                    popupWrapper.style.transitionDuration = unsetDuration;

                    activeControl = null;

                    this.emit('hidden');
                };

                if (options.duration) {
                    toggleTimeout = setTimeout(() => {
                        hideFunction();
                    }, options.duration);
                    return;
                }

                hideFunction();
            }
        });

        this.onSubscribe([
            /**
             * @event AppPopup#shown
             * @memberof components
             * @desc Вызывается после показа попапа.
             * @returns {undefined}
             * @example
             * popupInstance.on('shown', () => {
             *     console.log('Попап показан');
             * });
             */
            'shown',

            /**
             * @event AppPopup#hidden
             * @memberof components
             * @desc Вызывается после скрытия попапа.
             * @returns {undefined}
             * @example
             * popupInstance.on('hidden', () => {
             *     console.log('Попап скрыт');
             * });
             */
            'hidden'
        ]);

        const initialUnlink = this.unlink.bind(this); //TODO:make destroy event
        Object.defineProperty(this, 'unlink', {
            enumerable: true,
            value() {
                if (popupEl instanceof HTMLElement) {
                    delete popupEl.modules[moduleName];
                }
                initialUnlink();
            }
        });

        if (popupEl instanceof HTMLElement) {
            popupEl.classList.add(initializedClass);
        }

        const elements = (popupEl instanceof HTMLElement) ? [...controls, popupEl] : controls;
        elements.find(item => {
            if (item.dataset && (typeof item.dataset.popupTriggered !== 'undefined')) {
                delete item.dataset.popupTriggered;
                this.show();
                return true;
            }

            return false;
        });
    }

    /**
     * @function AppPopup.template
     * @memberof components
     * @desc Базовый шабон попапа.
     * @param {Object} [options] - Опции шаблона.
     * @param {string} [options.popupClass] - Дополнительный класс, добавляемый шаблону попапа.
     * @param {boolean} [options.inner] - Оборачивать ли контент попапа в контейнер.
     * @param {string} [options.content] - Контент попапа.
     * @returns {string} HTML-строку с попапом.
     * @example
     * const popupInstance = new app.Popup(app.Popup.template({
     *     popupClass: 'test-popup',
     *     content: 'Контент попапа'
     * }));
     */
    static template(options = {}) {
        let optionsObject = options;
        if (typeof options === 'string') {
            const content = options;
            optionsObject = {content};
        }

        if (!util.isObject(optionsObject)) {
            util.typeError(optionsObject, 'options', 'plain object');
        }

        return `
            <div class="${popupClass + (optionsObject.popupClass ? ` ${optionsObject.popupClass}` : '')}">
                ${optionsObject.inner ? `<div class="${popupClass}__inner">` : ''}
                    ${optionsObject.content}
                ${optionsObject.inner ? '</div>' : ''}
            </div>`;
    }

    /**
     * @function AppPopup.setCurrentPopup
     * @memberof components
     * @desc Устанавливает текущий показанный экземпляр попапа.
     * @param {Object} [newPopup] - Экземпляр текущего показанного попапа.
     * @returns {undefined}
     */
    static setCurrentPopup(newPopup) {
        currentPopup = newPopup;
    }

    /**
     * @function AppPopup.unsetCurrentPopup
     * @memberof components
     * @desc Сбрасывает текущий открытый экземпляр попапа.
     * @returns {undefined}
     */
    static unsetCurrentPopup() {
        currentPopup = null;
    }

    /**
     * @function AppPopup.currentPopup
     * @memberof components
     * @desc Get-функция. Ссылка на экземпляр текущего показанного попапа.
     * @returns {Object}
     */
    static get currentPopup() {
        return currentPopup;
    }
});

//Настройка возможности управления с помощью адресной строки
require('Components/url').default.then(AppUrl => {
    AppUrl.registerAction({
        id: moduleName,
        selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
        action: `${moduleName}.show`
    });
});
if (window.location.hash) emitInit('url');

addModule(moduleName, AppPopup);

//Инициализировать элементы по data-атрибуту
document.querySelectorAll('[data-popup]').forEach(el => {
    if (el.classList.contains(initializedClass)) return;
    new AppPopup(el); /* eslint-disable-line no-new */
});

export default AppPopup;
export {AppPopup};