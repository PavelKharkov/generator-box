import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'iframe';

const tagName = 'IFRAME';

const lazyloadedClass = 'lazyloaded';
const initializedClass = `${moduleName}-initialized`;

const attrOptions = [
    'src',
    'width',
    'height',
    'name',
    'allow',
    'allowfullscreen',
    'allowpaymentrequest',
    'referrerpolicy',
    'sandbox',
    'srcdoc',
    'scrolling'
];

//Опции по умолчанию
const defaultOptions = {
    srcAttr: 'data-src',
    widthAttr: 'data-width',
    heightAttr: 'data-height',
    nameAttr: 'data-name',
    allowAttr: 'data-iframe-allow',
    allowfullscreenAttr: 'data-iframe-allowfullscreen',
    allowpaymentrequestAttr: 'data-iframe-allowpaymentrequest',
    referrerpolicyAttr: 'data-iframe-referrerpolicy',
    sandboxAttr: 'data-iframe-sandbox',
    srcdocAttr: 'data-iframe-srcdoc',
    scrollingAttr: 'data-iframe-scrolling'
};

/**
 * @class AppIframe
 * @memberof components
 * @classdesc Модуль для тегов iframe.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент iframe.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.srcAttr='data-src'] - Атрибут, с которого будет браться значение атрибута src фрейма при отложенной загрузке.
 * @param {string} [options.widthAttr='data-width'] - Атрибут, с которого будет браться значение атрибута width фрейма при отложенной загрузке.
 * @param {string} [options.heightAttr='data-height'] - Атрибут, с которого будет браться значение атрибута height фрейма при отложенной загрузке.
 * @param {string} [options.nameAttr='data-name'] - Атрибут, с которого будет браться значение атрибута name фрейма при отложенной загрузке.
 * @param {string} [options.allowAttr='data-iframe-allow'] - Атрибут, с которого будет браться значение атрибута allow фрейма при отложенной загрузке.
 * @param {string} [options.allowfullscreenAttr='data-iframe-allowfullscreen'] - Атрибут, с которого будет браться значение атрибута allowfullscreen фрейма при отложенной загрузке.
 * @param {string} [options.allowpaymentrequestAttr='data-iframe-allowpaymentrequest'] - Атрибут, с которого будет браться значение атрибута allowpaymentrequest фрейма при отложенной загрузке.
 * @param {string} [options.referrerpolicyAttr='data-iframe-referrerpolicy'] - Атрибут, с которого будет браться значение атрибута referrerpolicy фрейма при отложенной загрузке.
 * @param {string} [options.sandboxAttr='data-iframe-sandbox'] - Атрибут, с которого будет браться значение атрибута sandbox фрейма при отложенной загрузке.
 * @param {string} [options.srcdocAttr='data-iframe-srcdoc'] - Атрибут, с которого будет браться значение атрибута srcdoc фрейма при отложенной загрузке.
 * @param {string} [options.scrollingAttr='data-iframe-scrolling'] - Атрибут, с которого будет браться значение атрибута scrolling фрейма при отложенной загрузке.
 * @ignore
 * @example
 * const iframeInstance = new app.Iframe(document.querySelector('.iframe-element'), {
 *     srcAttr: 'data-custom-src'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-lazyload="iframe" data-src="#"></div>
 *
 * <!--data-lazyload="iframe" - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-iframe-options='{"srcAttr": "data-src-custom"}'></div>
 */
const AppIframe = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const iframeOptionsData = el.dataset.iframeOptions;
        if (iframeOptionsData) {
            const dataOptions = util.stringToJSON(iframeOptionsData);
            if (!dataOptions) util.error('incorrect data-iframe-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppIframe#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let iframeEl = el;

        /**
         * @member {HTMLElement} AppIframe#iframeEl
         * @memberof components
         * @desc Элемент iframe.
         * @readonly
         * @example
         * //Если iframe был инициализирован с помощью div
         * console.log(iframeInstance.el); //Выведет HTML-элемент div
         * console.log(iframeInstance.iframeEl); //Выведет HTML-элемент iframe
         */
        Object.defineProperty(this, 'iframeEl', {
            enumerable: true,
            get: () => iframeEl
        });

        /**
         * @function initEvents
         * @desc Инициализует браузерные события iframe.
         * @ignore
         */
        const initEvents = () => {
            iframeEl.addEventListener('load', () => {
                //console.log('iframe is loaded', iframeEl);
                this.emit('load');
            });

            iframeEl.addEventListener('error', () => {
                util.error({
                    message: 'error loading iframe',
                    element: iframeEl
                });
            });

            if (iframeEl.complete) {
                util.defer(() => {
                    //console.log('iframe already loaded', iframeEl);
                    this.emit('load');
                });
            }
        };

        this.on({
            /**
             * @event AppIframe#lazyload
             * @memberof components
             * @desc Событие, вызываемое при отложенной загрузке фрейма.
             * @throws Выводит ошибку, если не указан атрибут пути к файлу.
             * @returns {undefined}
             * @example
             * iframeInstance.on('lazyload', () => {
             *     console.log('Iframe был проинициализирован с помощью отложенной загрузки');
             * });
             * iframeInstance.lazyload();
             */
            lazyload() {
                if (el.tagName === tagName) return;

                const attrs = {};
                attrOptions.forEach(attr => {
                    const attrValue = el.getAttribute(params[`${attr}Attr`]);
                    if (attrValue === null) return;
                    el.removeAttribute(params[`${attr}Attr`]);
                    attrs[attr] = attrValue;
                });

                if (!attrs.src) {
                    util.error({
                        message: 'required \'src\' attribute not provided',
                        element: el
                    });
                }

                const newEl = document.createElement('iframe');

                [...el.attributes].forEach(attr => {
                    newEl.setAttribute(attr.name, attr.value);
                });
                Object.keys(attrs).forEach(attr => {
                    if (attr === null) return;
                    newEl.setAttribute(attr, (attr === 'src') ? encodeURI(attrs[attr]) : attrs[attr]);
                });

                delete newEl.dataset.lazyload;
                newEl.classList.remove(lazyloadedClass);

                el.parentNode.replaceChild(newEl, el);
                iframeEl = newEl;
                newEl.modules = {
                    [moduleName]: this
                };

                initEvents();
            }
        });

        this.onSubscribe({
            /**
             * @event AppIframe#load
             * @memberof components
             * @desc Событие, вызываемое при загрузке содержимого iframe.
             * @returns {undefined}
             * @example
             * iframeInstance.on('load', () => {
             *     console.log('Содержимое iframe загружено');
             * });
             */
            load() {
                iframeEl.classList.add(initializedClass);
            }
        });

        if (iframeEl.tagName === tagName) {
            initEvents();
        }

        if (!el.classList.contains(lazyloadedClass)) {
            this.lazyload();
        }
    }
});

addModule(moduleName, AppIframe);

//Инициализация элементов по data-атрибуту
document.querySelectorAll(`[data-lazyload="${moduleName}"]`).forEach(el => new AppIframe(el));

export default AppIframe;
export {AppIframe};