let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const iframeCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppIframe = modules.default;
                resolve(AppIframe);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            iframeCallback(resolve);
            return;
        }

        onInit('iframe', () => {
            iframeCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('iframe');
        });
    });
}

export default importFunc;