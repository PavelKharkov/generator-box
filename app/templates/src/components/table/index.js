import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'table';

//Классы
const tableResponsiveClass = `${moduleName}-responsive`;
const fixedClass = 'position-fixed';
const fixedBottomClass = 'fixed-bottom';
const initializedClass = `${moduleName}-initialized`;

const tableHeaderSelector = '[data-table-header]';

//Опции по умолчанию
const defaultOptions = {
    responsive: true,
    fixedBlockMin: util.media.xl
};

/**
 * @class AppTable
 * @memberof components
 * @requires layout#AppWindow
 * @classdesc Модуль для работы с таблицами.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент таблицы.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.responsive=true] - Оборачивать ли таблицу в контейнер для прокрутки её содержимого на маленьких разрешениях.
 * @param {boolean} [options.fixedBlock] - Фиксировать ли заголовок таблицы (должен быть отдельным от таблицы элементом на том же уровне) при прокрутке.
 * @param {number} [options.fixedBlockMin={@link app.util.media}.xl] - Минимальная ширина отображения фиксированного заголовка.
 * @example
 * const tableInstance = new app.Table(document.querySelector('.table-element'), {
 *     responsive: false
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <table></table>
 *
 * <!--Экземпляры таблиц инициализируются по тегу-->
 *
 * @example <caption>Пример HTML-разметки для таблицы с фиксирующимся заголовком</caption>
 * <!--HTML-->
 * <div>
 *     <div data-table-header>Плавающий заголовок таблицы</div>
 *     <table></table>
 * </div>
 *
 * <!--data-table-header - селектор плавающего заголовка таблицы по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-table-options='{"responsive": false}'></div>
 */
const AppTable = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const tableOptionsData = el.dataset.tableOptions;
        if (tableOptionsData) {
            const dataOptions = util.stringToJSON(tableOptionsData);
            if (!dataOptions) util.error('incorrect data-table-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppTable#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let wrapper;

        //Оборачивает таблицу в контейнер для прокрутки для маленьких разрешений экранов
        if (params.responsive) {
            wrapper = document.createElement('div');

            /**
             * @member {HTMLElement} AppTable#wrapper
             * @memberof components
             * @desc Блок-обёртка таблицы.
             * @readonly
             */
            Object.defineProperty(this, 'wrapper', {
                enumerable: true,
                value: wrapper
            });

            wrapper.className = tableResponsiveClass;
            el.parentNode.insertBefore(wrapper, el);
            wrapper.append(el);

            wrapper.modules = {
                [moduleName]: this
            };
        }

        let isBlockFixed = false;

        /**
         * @member {boolean} AppTable#isBlockFixed
         * @memberof components
         * @desc Указывает, является ли заголовок таблицы плавающим в данный момент.
         * @readonly
         */
        Object.defineProperty(this, 'isBlockFixed', {
            enumerable: true,
            get: () => isBlockFixed
        });

        //Фиксирует заголовок таблицы при прокрутке
        if (params.fixedBlock) {
            const blockWrapper = el.parentElement;
            const fixedBlock = blockWrapper.querySelector(tableHeaderSelector);
            const fixedBlockWrapper = fixedBlock.parentElement;

            if (fixedBlock) {
                /**
                 * @function updateWidth
                 * @desc Обновляет ширину плавающего блока таблицы.
                 * @returns {undefined}
                 * @ignore
                 */
                const updateSize = () => {
                    fixedBlock.style.width = `${fixedBlockWrapper.offsetWidth}px`;
                    blockWrapper.style.minHeight = `${fixedBlock.offsetHeight}px`;
                };

                const fixedBlockMin = params.fixedBlockMin;

                //Перерисовка плавающего блока при изменении окна браузера
                AppWindow.on({
                    load() {
                        if (AppWindow.width() < fixedBlockMin) return;

                        util.defer(() => {
                            updateSize();
                        });
                    },

                    scroll() {
                        if (AppWindow.width() < fixedBlockMin) return;

                        const fixedBlockHeight = fixedBlock.offsetHeight;
                        const blockWrapperOffset = blockWrapper.getBoundingClientRect().top;
                        const blockWrapperHeight = blockWrapper.offsetHeight;
                        const blockWrapperBottom = blockWrapperOffset + blockWrapperHeight - fixedBlockHeight;

                        if (
                            (blockWrapperOffset < 0) &&
                            (blockWrapperBottom > 0)
                        ) {
                            isBlockFixed = true;
                            fixedBlock.classList.remove(fixedBottomClass);
                            fixedBlock.classList.add(fixedClass);
                            return;
                        }

                        isBlockFixed = false;
                        fixedBlock.classList.remove(fixedClass);
                        if (
                            (blockWrapperBottom <= 0) &&
                            (Math.abs(blockWrapperBottom) <= fixedBlockHeight) &&
                            (blockWrapperHeight > fixedBlockHeight)
                        ) {
                            fixedBlock.classList.add(fixedBottomClass);
                        }
                    },

                    resize() {
                        if (AppWindow.width() < fixedBlockMin) return;

                        updateSize();
                    }
                });
            }
        }

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppTable);

//Инициализация таблиц
document.querySelectorAll('table, [data-table]').forEach(el => {
    if (el.dataset.table === 'false') return;
    new AppTable(el); /* eslint-disable-line no-new */
});

export default AppTable;
export {AppTable};