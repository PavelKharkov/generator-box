import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let collapseTriggered;

const collapseCallback = resolve => {
    const imports = [require('Components/toggle').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "toggles" */ './sync.js'))
            .then(collapse => {
                chunks.toggles = true;
                collapseTriggered = true;

                const AppCollapse = collapse.initCollapse(...modules);
                if (resolve) resolve(AppCollapse);
            });
    });

    emitInit('toggle');
};

const initCollapse = event => {
    if (collapseTriggered) return;

    event.preventDefault();
    event.currentTarget.dataset.collapseTriggered = '';

    emitInit('collapse');
};

const collapseTrigger = (collapseItems, collapseTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (collapseItems instanceof Node) ? [collapseItems] : collapseItems;
    if (items.length === 0) return;

    items.forEach(item => {
        collapseTriggers.forEach(trigger => {
            item.addEventListener(trigger, initCollapse, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.toggles) {
        collapseCallback(resolve);
        return;
    }

    onInit('collapse', () => {
        collapseCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('collapse');
    });

    const collapseItems = document.querySelectorAll('[data-collapse]');
    collapseTrigger(collapseItems);
});

export default importFunc;
export {collapseTrigger};