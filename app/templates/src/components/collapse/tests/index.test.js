import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import util from 'Layout/main';
import AppToggle from 'Components/toggle/sync.js';
import collapseInit from '../sync.js';

//Инициализация тестового элемента с атрибутом data-collapse
const collapseControlWithDataEl = document.createElement('a');
collapseControlWithDataEl.dataset.collapse = '';
collapseControlWithDataEl.href = '#collapse-with-data';
document.body.append(collapseControlWithDataEl);

const collapseTargetWithDataEl = document.createElement('div');
collapseTargetWithDataEl.id = 'collapse-with-data';
collapseTargetWithDataEl.classList.add('collapse', 'toggled');
document.body.append(collapseTargetWithDataEl);

const AppCollapse = collapseInit(AppToggle);

describe('AppCollapse', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalCollapse = window.app.Collapse;

        expect(globalCollapse.prototype).toBeInstanceOf(Module);
    });

    test('должен являться экземпляром модуля \'toggle\'', () => {
        expect(AppCollapse.prototype).toBeInstanceOf(AppToggle);
    });

    const dataTypesAssertions = ['HTMLElement']
        .map(value => [value, [throwsError, 'can\'t find any target elements for toggling']]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.collapse;
            return new AppCollapse(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, 'parameter \'element\' must be a HTMLElement'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppCollapse\' с переданным значением'
    });

    test('должен сохраняться под именем \'collapse\' в свойстве \'modules\' у элементов', () => {
        const collapseControlEl = document.createElement('a');
        collapseControlEl.href = '#collapse-test';
        document.body.append(collapseControlEl);

        const collapseTargetEl = document.createElement('div');
        collapseTargetEl.id = 'collapse-test';
        collapseTargetEl.classList.add('collapse', 'toggled');
        document.body.append(collapseTargetEl);

        new AppCollapse(collapseControlEl); /* eslint-disable-line no-new */
        const collapseInstanceControlProp = collapseControlEl.modules.collapse;
        const collapseInstanceTargetProp = collapseTargetEl.modules.collapse;

        expect(collapseInstanceControlProp).toBeInstanceOf(AppCollapse);
        expect(collapseInstanceTargetProp).toBeInstanceOf(AppCollapse);
    });

    test('должен добавлять ссылку на модуль \'toggle\' в свойстве \'modules\' у элементов', () => {
        const collapseControlEl = document.createElement('a');
        collapseControlEl.href = '#collapse-test';
        document.body.append(collapseControlEl);

        const collapseTargetEl = document.createElement('div');
        collapseTargetEl.id = 'collapse-test';
        collapseTargetEl.classList.add('collapse', 'toggled');
        document.body.append(collapseTargetEl);

        new AppCollapse(collapseControlEl); /* eslint-disable-line no-new */
        const toggleInstanceControlProp = collapseControlEl.modules.toggle;
        const toggleInstanceTargetProp = collapseTargetEl.modules.toggle;

        expect(toggleInstanceControlProp).toBeInstanceOf(AppToggle);
        expect(toggleInstanceTargetProp).toBeInstanceOf(AppToggle);
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-collapse\'', () => {
        const collapseInstance = collapseControlWithDataEl.modules.collapse;

        expect(collapseInstance).toBeInstanceOf(AppCollapse);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const collapsePropsArray = [
            'isShown',
            'target',
            'controlElements',
            'targetElements',
            'show',
            'hide',
            'toggle',
            'initialHeights'
        ];
        test.each(collapsePropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const currentProp = collapseInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const animationEventsArray = [
            'show',
            'shown',
            'hide',
            'hidden',
            'toggle'
        ];
        test.each(animationEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test-events';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const eventsArray = collapseInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const collapseOptionsMap = [
            ['dataToggle', 'collapse'],
            ['dataToggleId', 'collapseId'],
            ['duration', Number.parseInt(util.getStyle('--transition-duration'), 10)],
            ['toggleInit', false],
            ['trigger', 'click']
        ];
        test.each(collapseOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const instanceOptions = collapseInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    describe('[data-collapse-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-collapse-options\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseOptions = '{"duration": 200}';
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const instanceOptions = collapseInstance.options;

            expect(instanceOptions.duration).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-collapse-options\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseOptions = 'true';
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = (() => new AppCollapse(collapseTargetEl));

            expect(collapseInstance).toThrow('incorrect data-collapse-options format');
        });

        test('должен корректно инициализировать экземпляр на управляющем элементе, с опциями, ' +
            'указанными в атрибуте \'data-collapse-options\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapseOptions = '{"duration": 200}';
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);
            const instanceOptions = collapseInstance.options;

            expect(instanceOptions.duration).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-collapse-options\' на управляющем элементе', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapseOptions = 'true';
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = (() => new AppCollapse(collapseControlEl));

            expect(collapseInstance).toThrow('incorrect data-collapse-options format');
        });
    });

    describe('поиск элементов, участвующих в работе экземпляра', () => {
        test('должен корректно находить блоки с контентом по атрибуту \'data-collapse-id\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseId = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            new AppCollapse(collapseControlEl); /* eslint-disable-line no-new */
            const collapseTargetProp = collapseTargetEl.modules.collapse;

            expect(collapseTargetProp).toBeInstanceOf(AppCollapse);
        });

        test('должен корректно находить все блоки с контентом по атрибутам \'data-collapse-id\' и \'id\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseTargetDataEl = document.createElement('div');
            collapseTargetDataEl.dataset.collapseId = 'collapse-test';
            collapseTargetDataEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetDataEl);

            new AppCollapse(collapseControlEl); /* eslint-disable-line no-new */
            const collapseTargetProp = collapseTargetEl.modules.collapse;
            const collapseTargetDataProp = collapseTargetEl.modules.collapse;

            expect(collapseTargetProp).toBeInstanceOf(AppCollapse);
            expect(collapseTargetDataProp).toBeInstanceOf(AppCollapse);
        });

        test('должен выводить ошибку, если контентный блок не найден', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseInstance = (() => new AppCollapse(collapseControlEl));

            expect(collapseInstance).toThrow('can\'t find any target elements for toggling');
        });

        test('должен корректно инициализироваться на блоке с контентом', () => {
            const collapseTargetEl1 = document.createElement('div');
            collapseTargetEl1.id = 'collapse-test1';
            collapseTargetEl1.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl1);

            const collapseTargetEl2 = document.createElement('div');
            collapseTargetEl2.dataset.collapseId = 'collapse-test2';
            collapseTargetEl2.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl2);

            const collapseInstance1 = new AppCollapse(collapseTargetEl1);
            const collapseInstance2 = new AppCollapse(collapseTargetEl2);

            expect(collapseInstance1).toBeInstanceOf(AppCollapse);
            expect(collapseInstance2).toBeInstanceOf(AppCollapse);
        });

        test('должен находить все связанные управляющие элементы при инициализироваться на блоке с контентом', () => {
            const collapseControlEl1 = document.createElement('a');
            collapseControlEl1.dataset.collapse = '#collapse-test1';
            document.body.append(collapseControlEl1);

            const collapseControlEl2 = document.createElement('a');
            collapseControlEl2.dataset.collapse = '#collapse-test1';
            document.body.append(collapseControlEl2);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test1';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            new AppCollapse(collapseTargetEl); /* eslint-disable-line no-new */
            const collapseControlProp1 = collapseControlEl1.modules.collapse;
            const collapseControlProp2 = collapseControlEl2.modules.collapse;

            expect(collapseControlProp1).toBeInstanceOf(AppCollapse);
            expect(collapseControlProp2).toBeInstanceOf(AppCollapse);
        });
    });

    describe('.isShown', () => {
        test('должен корректно вычислять свойство \'isShown\' для скрытого блока контента после инициализации', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const isShown = collapseInstance.isShown;
            expect(isShown).toBe(false);
        });

        test('должен корректно вычислять свойство \'isShown\' для открытого блока контента после инициализации', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const isShown = collapseInstance.isShown;
            expect(isShown).toBe(true);
        });

        test('должен корректно вычислять свойство \'isShown\' для нескольких блоков контента после инициализации', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseTargetDataEl = document.createElement('div');
            collapseTargetDataEl.dataset.collapseId = 'collapse-test';
            collapseTargetDataEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetDataEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const isShown = collapseInstance.isShown;
            expect(isShown).toBe(true);
        });

        test('должен выводить ошибку при попытке поменять свойство \'isShown\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const isShown = (() => {
                collapseInstance.isShown = false;
            });
            expect(isShown).toThrow('Cannot set property isShown of #<ImmutableClass> which has only a getter');
        });

        test('должен корректно синхронизировать атрибуты управляющих элементов для открытого блока контента после инициализации', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.classList.add('inactive');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);
            collapseInstance.on('shown', () => {
                expect(collapseControlEl.classList).toContain('active');
                expect(collapseControlEl.classList).not.toContain('inactive');
                expect(collapseControlEl.ariaExpanded).toBe('true');
            });
        });
    });

    describe('.dataToggle', () => {
        test('должен корректно инициализироваться с опцией \'dataToggle\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse2 = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseOptions = {
                dataToggle: 'collapse2'
            };
            new AppCollapse(collapseTargetEl, collapseOptions); /* eslint-disable-line no-new */

            const collapseInstanceProp = collapseControlEl.modules.collapse;
            expect(collapseInstanceProp).toBeInstanceOf(AppCollapse);
        });

        test('должен корректно инициализироваться с некорректной опцией \'dataToggle\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseOptions = {
                dataToggle: ''
            };
            const collapseInstance = new AppCollapse(collapseTargetEl, collapseOptions);

            expect(collapseInstance).toBeInstanceOf(AppCollapse);
        });

        test('должен корректно инициализироваться с опцией \'dataToggleId\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseId2 = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseOptions = {
                dataToggleId: 'collapse-id2'
            };
            new AppCollapse(collapseControlEl, collapseOptions); /* eslint-disable-line no-new */

            const collapseInstanceProp = collapseTargetEl.modules.collapse;
            expect(collapseInstanceProp).toBeInstanceOf(AppCollapse);
        });

        test('должен выбрасывать ошибку при некорректной опции \'dataToggleId\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseId = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseOptions = {
                dataToggleId: '-'
            };
            const collapseInstance = (() => new AppCollapse(collapseControlEl, collapseOptions));

            expect(collapseInstance).toThrow('can\'t find any target elements for toggling');
        });
    });

    describe('.target', () => {
        test('должен корректно инициализироваться с опцией \'target\'', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                target: 'collapse-test'
            });

            const target = collapseInstance.target;
            const collapseTargetProp = collapseTargetEl.modules.collapse;
            expect(target).toBe('collapse-test');
            expect(collapseTargetProp).toBeInstanceOf(AppCollapse);
        });

        test('должен корректно инициализироваться с ошибочной опцией \'target\', если была корректно передана опция \'targetElements\'', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                target: '123',
                targetElements: collapseTargetEl
            });

            const target = collapseInstance.target;
            const collapseTargetProp = collapseTargetEl.modules.collapse;
            expect(target).toBe('123');
            expect(collapseTargetProp).toBeInstanceOf(AppCollapse);
        });

        test('должен выбрасывать ошибку с некорректной опцией \'target\' при инициализации на управляющем элементе', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = (() => {
                return new AppCollapse(collapseControlEl, {
                    target: '123'
                });
            });

            expect(collapseInstance).toThrow('can\'t find any target elements for toggling');
        });

        test('должен выводить ошибку при попытке поменять свойство', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);

            const target = (() => {
                collapseInstance.target = false;
            });
            expect(target).toThrow('Cannot assign to read only property \'target\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('controlElements', () => {
        test('должен корректно устанавливать свойство \'controlElements\', если ни одного управляющего элемента не было найдено', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);

            const controlElements = collapseInstance.controlElements;
            expect(Array.isArray(controlElements)).toBe(true);
            expect(controlElements).toHaveLength(0);
        });

        test('должен корректно инициализироваться с опцией \'controlElements\'', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl, {
                controlElements: collapseControlEl
            });

            const controlElements = collapseInstance.controlElements;
            expect(Array.isArray(controlElements)).toBe(true);
            expect(controlElements[0]).toBe(collapseControlEl);
        });

        test('должен корректно инициализироваться с опцией \'controlElements\' на контентном блоке с атрибутом \'data-collapse-id\'', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseId = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl, {
                controlElements: collapseControlEl
            });

            const controlElements = collapseInstance.controlElements;
            expect(Array.isArray(controlElements)).toBe(true);
            expect(controlElements[0]).toBe(collapseControlEl);
        });

        test('должен корректно инициализироваться с опцией \'controlElements\' в виде коллекции элементов', () => {
            const collapseControlEl1 = document.createElement('a');
            document.body.append(collapseControlEl1);

            const collapseControlEl2 = document.createElement('a');
            document.body.append(collapseControlEl2);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.dataset.collapseId = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl, {
                controlElements: [collapseControlEl1, collapseControlEl2]
            });

            const controlElements = collapseInstance.controlElements;
            expect(Array.isArray(controlElements)).toBe(true);
            expect(controlElements).toHaveLength(2);
            expect(controlElements[0]).toBe(collapseControlEl1);
            expect(controlElements[1]).toBe(collapseControlEl2);
        });

        test('должен выбрасывать ошибку, если неверно передана опция \'controlElements\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = (() => {
                return new AppCollapse(collapseTargetEl, {
                    controlElements: ['123']
                });
            });

            expect(collapseInstance).toThrow('toggle elements are set incorrectly');
        });

        test('должен выводить ошибку при попытке поменять свойство', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);

            const controlElements = (() => {
                collapseInstance.controlElements = [];
            });
            expect(controlElements).toThrow('Cannot assign to read only property \'controlElements\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.targetElements', () => {
        test('должен корректно инициализироваться с опцией \'targetElements\'', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                targetElements: collapseTargetEl
            });

            const targetElements = collapseInstance.targetElements;
            expect(Array.isArray(targetElements)).toBe(true);
            expect(targetElements[0]).toBe(collapseTargetEl);
        });

        test('должен корректно инициализироваться с опцией \'targetElements\' на контентном блоке с установленным атрибутом \'href\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                targetElements: collapseTargetEl
            });

            const targetElements = collapseInstance.targetElements;
            expect(Array.isArray(targetElements)).toBe(true);
            expect(targetElements[0]).toBe(collapseTargetEl);
        });

        test('должен корректно инициализироваться с опцией \'targetElements\' в виде коллекции элементов', () => {
            const collapseControlEl = document.createElement('a');
            document.body.append(collapseControlEl);

            const collapseTargetEl1 = document.createElement('div');
            collapseTargetEl1.dataset.collapseId = 'collapse-test';
            collapseTargetEl1.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl1);

            const collapseTargetEl2 = document.createElement('div');
            collapseTargetEl2.dataset.collapseId = 'collapse-test';
            collapseTargetEl2.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl2);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                targetElements: [collapseTargetEl1, collapseTargetEl2]
            });

            const targetElements = collapseInstance.targetElements;
            expect(Array.isArray(targetElements)).toBe(true);
            expect(targetElements).toHaveLength(2);
            expect(targetElements[0]).toBe(collapseTargetEl1);
            expect(targetElements[1]).toBe(collapseTargetEl2);
        });

        test('должен выбрасывать ошибку, если неверно передана опция \'targetElements\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.dataset.collapse = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseInstance = (() => {
                return new AppCollapse(collapseControlEl, {
                    targetElements: ['123']
                });
            });

            expect(collapseInstance).toThrow('toggle elements are set incorrectly');
        });

        test('должен выводить ошибку при попытке поменять свойство', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);

            const targetElements = (() => {
                collapseInstance.targetElements = [];
            });
            expect(targetElements).toThrow('Cannot assign to read only property \'targetElements\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('взаимодействие с управляющим элементом', () => {
        test('должен корректно регистрировать браузерное событие, указанное в опции \'trigger\'', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                trigger: 'mouseenter'
            });

            const showFunction = jest.fn();
            collapseInstance.on('show', () => showFunction());

            const mouseenterEvent = new Event('mouseenter');
            collapseControlEl.dispatchEvent(mouseenterEvent);
            expect(showFunction).toHaveBeenCalledWith();
        });

        test('должен корректно регистрировать браузерное событие, если в опции \'trigger\' передана коллекция событий', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl, {
                trigger: ['mouseenter', 'mouseleave']
            });

            const showFunction = jest.fn();
            collapseInstance.on('show', () => {
                showFunction();

                const mouseleaveEvent = new Event('mouseleave');
                collapseControlEl.dispatchEvent(mouseleaveEvent);
            });

            const hideFunction = jest.fn();
            collapseInstance.on('hide', () => {
                hideFunction();

                expect(hideFunction).toHaveBeenCalledWith();
            });

            const mouseenterEvent = new Event('mouseenter');
            collapseControlEl.dispatchEvent(mouseenterEvent);

            expect(showFunction).toHaveBeenCalledWith();
        });

        test('должен вызывать событие \'show\' по нажатию на управляющий элемент экземпляра', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);

            const showFunction = jest.fn();
            collapseInstance.on('show', () => showFunction());

            collapseControlEl.click();
            expect(showFunction).toHaveBeenCalledWith();
        });

        test('должен вызывать событие \'hide\' по нажатию на управляющий элемент экземпляра', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.href = '#collapse-test';
            util.activate(collapseControlEl);
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);

            const hideFunction = jest.fn();
            collapseInstance.on('hide', () => hideFunction());

            collapseControlEl.click();
            expect(hideFunction).toHaveBeenCalledWith();
        });
    });

    test('должен вызывать событие \'hide\' при нажатии вне элементов экземпляра при включённой опции \'outsideClickHide\'', () => {
        const collapseTargetEl = document.createElement('div');
        collapseTargetEl.id = 'collapse-test';
        collapseTargetEl.classList.add('collapse', 'toggled', 'show');
        document.body.append(collapseTargetEl);

        const collapseInstance = new AppCollapse(collapseTargetEl, {
            outsideClickHide: true
        });

        const hideFunction = jest.fn();
        collapseInstance.on('hide', () => hideFunction());

        collapseTargetEl.click();
        expect(hideFunction).not.toHaveBeenCalled();

        document.body.click();
        expect(hideFunction).toHaveBeenCalledWith();
    });

    test('должен вызывать событие \'toggle\' при инициализации с опцией \'toggleInit\'', () => {
        const collapseTargetEl = document.createElement('div');
        collapseTargetEl.id = 'collapse-test';
        collapseTargetEl.classList.add('collapse', 'toggled');
        document.body.append(collapseTargetEl);

        const collapseInstance = new AppCollapse(collapseTargetEl, {
            toggleInit: true
        });

        const shownFunction = jest.fn();
        collapseInstance.on('shown', () => {
            shownFunction();

            expect(shownFunction).toHaveBeenCalledWith();
        });
    });

    test('должен прерывать браузерное событие активации для скрытого блока контента ' +
        'через управляющий элемент с включёнными опциями \'parent\' и \'tabs\'', () => {
        const collapseControlEl = document.createElement('a');
        collapseControlEl.href = '#collapse-test';
        document.body.append(collapseControlEl);

        const collapseParentEl = document.createElement('div');
        document.body.append(collapseParentEl);

        const collapseTargetEl = document.createElement('div');
        collapseTargetEl.id = 'collapse-test';
        collapseTargetEl.classList.add('collapse', 'toggled', 'show');
        collapseParentEl.append(collapseTargetEl);

        const collapseInstance = new AppCollapse(collapseControlEl, {
            parent: true,
            tabs: true
        });

        expect(collapseInstance.isShown).toBe(true);

        const hideFunction = jest.fn();
        collapseInstance.on('hide', () => {
            hideFunction();

            expect(hideFunction).not.toHaveBeenCalled();
        });

        collapseControlEl.click();
    });

    //TODO:test app.url

    //TODO:test height 0, test custom height, test initialHeights for shown elements, initialHeights for multiple elements, initialHeight option
});