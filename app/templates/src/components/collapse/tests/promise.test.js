import 'Base/scripts/test';

describe('AppCollapse', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const collapsePromise = require('..').default;

        expect(collapsePromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в функцию инициализации класса', async () => {
        await new Promise(done => {
            return require('..').default.then(AppCollapseTest => {
                expect(AppCollapseTest).toBeInstanceOf(Function);
                done();
            });
        });
    });
});