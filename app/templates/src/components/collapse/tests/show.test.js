import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import AppToggle from 'Components/toggle/sync.js';
import collapseInit from '../sync.js';

const AppCollapse = collapseInit(AppToggle);

describe('AppCollapse', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.show()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(showFunction, value) {
                return showFunction(value);
            },
            setupFunction() {
                const collapseTargetEl = document.createElement('div');
                collapseTargetEl.id = 'collapse-test';
                collapseTargetEl.classList.add('collapse', 'toggled');
                document.body.append(collapseTargetEl);

                const collapseInstance = new AppCollapse(collapseTargetEl);
                const showFunction = collapseInstance.show;

                return showFunction;
            }
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const showOptions = [
                ['duration', 'строка', '300', 'parameter \'options.duration\' must be a number'],
                ['duration', 'NaN', Number.NaN, 'parameter \'options.duration\' must be a number']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(showOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const collapseTargetEl = document.createElement('div');
                    collapseTargetEl.id = 'collapse-test';
                    collapseTargetEl.classList.add('collapse', 'toggled');
                    document.body.append(collapseTargetEl);

                    const collapseInstance = new AppCollapse(collapseTargetEl);

                    const showConfig = {};
                    showConfig[optionName] = value;
                    const showResult = (() => {
                        return collapseInstance.show(showConfig);
                    });

                    expect(showResult).toThrow(errorMessage);
                });
        });

        test('должен вызывать событие \'shown\' только с помощью метода \'emit\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const shownCall = (() => collapseInstance.shown());
            const shownEmit = collapseInstance.emit('shown');

            expect(shownCall).toThrow('collapseInstance.shown is not a function');
            expect(shownEmit).toBeUndefined();
        });

        test('должен вызывать событие \'shown\' после завершения открытия контентного блока', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const shownFunction = jest.fn();
            collapseInstance.on('shown', () => shownFunction());

            collapseInstance.show();

            jest.runAllTimers();

            expect(shownFunction).toHaveBeenCalledWith();
        });

        test('должен вызывать событие \'shown\' только для скрытых элементов', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const shownFunction = jest.fn();
            collapseInstance.on('shown', () => shownFunction());

            collapseInstance.show();

            jest.runAllTimers();

            expect(shownFunction).not.toHaveBeenCalled();
        });

        test('должен скрывать все переключающиеся элементы внутри родительского контейнера с опцией \'parent\'', () => {
            const collapseParentEl = document.createElement('div');
            document.body.append(collapseParentEl);

            const collapseTargetEl1 = document.createElement('div');
            collapseTargetEl1.id = 'collapse-test1';
            collapseTargetEl1.classList.add('collapse', 'toggled');
            collapseParentEl.append(collapseTargetEl1);

            const collapseTargetEl2 = document.createElement('div');
            collapseTargetEl2.dataset.collapseId = 'collapse-test2';
            collapseTargetEl2.classList.add('collapse', 'toggled', 'show');
            collapseParentEl.append(collapseTargetEl2);

            const collapseInstance = new AppCollapse(collapseTargetEl1, {
                parent: collapseParentEl
            });
            const collapseInstance2 = new AppCollapse(collapseTargetEl2, {
                parent: collapseParentEl
            });

            const hideFunction = jest.fn();
            collapseInstance2.on('hide', () => hideFunction());

            collapseInstance.show();

            jest.runAllTimers();

            expect(hideFunction).toHaveBeenCalledWith();
        });

        test('не должен скрывать все переключающиеся элементы внутри родительского контейнера с некорректной опцией \'parent\'', () => {
            const collapseParentEl = document.createElement('div');
            document.body.append(collapseParentEl);

            const collapseTargetEl1 = document.createElement('div');
            collapseTargetEl1.id = 'collapse-test1';
            collapseTargetEl1.classList.add('collapse', 'toggled');
            collapseParentEl.append(collapseTargetEl1);

            const collapseTargetEl2 = document.createElement('div');
            collapseTargetEl2.dataset.collapseId = 'collapse-test2';
            collapseTargetEl2.classList.add('collapse', 'toggled', 'show');
            collapseParentEl.append(collapseTargetEl2);

            const collapseInstance = new AppCollapse(collapseTargetEl1, {
                parent: '.parent-element'
            });
            const collapseInstance2 = new AppCollapse(collapseTargetEl2, {
                parent: '.parent-element'
            });

            const hideFunction = jest.fn();
            collapseInstance2.on('hide', () => hideFunction());

            collapseInstance.show();

            jest.runAllTimers();

            expect(hideFunction).not.toHaveBeenCalled();
        });

        test('должен скрывать с нулевой длительностью все переключающиеся элементы внутри родительского контейнера ' +
            'с опциями \'parent\' и \'tabs\'', () => {
            const collapseParentEl = document.createElement('div');
            document.body.append(collapseParentEl);

            const collapseTargetEl1 = document.createElement('div');
            collapseTargetEl1.id = 'collapse-test1';
            collapseTargetEl1.classList.add('collapse', 'toggled');
            collapseParentEl.append(collapseTargetEl1);

            const collapseTargetEl2 = document.createElement('div');
            collapseTargetEl2.dataset.collapseId = 'collapse-test2';
            collapseTargetEl2.classList.add('collapse', 'toggled', 'show');
            collapseParentEl.append(collapseTargetEl2);

            const collapseInstance = new AppCollapse(collapseTargetEl1, {
                parent: collapseParentEl,
                tabs: true
            });
            const collapseInstance2 = new AppCollapse(collapseTargetEl2, {
                parent: collapseParentEl
            });

            collapseInstance2.on('hide', () => {
                expect(window.getComputedStyle(collapseTargetEl2).transitionDuration).toBe('0ms');
            });

            collapseInstance.show();

            jest.runAllTimers();
        });

        test('должен корректно обновлять атрибуты управляющих элементов', () => {
            const collapseControlEl = document.createElement('a');
            collapseControlEl.classList.add('inactive');
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);
            collapseInstance.on('shown', () => {
                expect(collapseControlEl.classList).toContain('active');
                expect(collapseControlEl.classList).not.toContain('inactive');
                expect(collapseControlEl.ariaExpanded).toBe('true');
            });

            collapseInstance.show();
        });

        test('должен корректно обновлять классы контентных элементов', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            collapseInstance.on('shown', () => {
                expect(collapseTargetEl.classList).toContain('show');
                expect(collapseTargetEl.classList).not.toContain('toggling');
            });

            collapseInstance.show();

            jest.advanceTimersByTime(1);

            expect(collapseTargetEl.classList).toContain('toggling');

            jest.runAllTimers();
        });

        test('должен корректно обновлять свойство \'isShown\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            collapseInstance.on('shown', () => {
                expect(collapseInstance.isShown).toBe(true);
            });

            collapseInstance.show();
        });

        //TODO:test height change, transition-duration
    });
});