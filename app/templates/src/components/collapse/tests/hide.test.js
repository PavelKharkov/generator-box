import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';
import AppToggle from 'Components/toggle/sync.js';
import collapseInit from '../sync.js';

const AppCollapse = collapseInit(AppToggle);

describe('AppCollapse', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.hide()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(hideFunction, value) {
                return hideFunction(value);
            },
            setupFunction() {
                const collapseTargetEl = document.createElement('div');
                collapseTargetEl.id = 'collapse-test';
                collapseTargetEl.classList.add('collapse', 'toggled');
                document.body.append(collapseTargetEl);

                const collapseInstance = new AppCollapse(collapseTargetEl);
                const hideFunction = collapseInstance.hide;

                return hideFunction;
            }
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const hideOptions = [
                ['duration', 'строка', '300', 'parameter \'options.duration\' must be a number'],
                ['duration', 'NaN', Number.NaN, 'parameter \'options.duration\' must be a number']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(hideOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const collapseTargetEl = document.createElement('div');
                    collapseTargetEl.id = 'collapse-test';
                    collapseTargetEl.classList.add('collapse', 'toggled');
                    document.body.append(collapseTargetEl);

                    const collapseInstance = new AppCollapse(collapseTargetEl);

                    const hideConfig = {};
                    hideConfig[optionName] = value;
                    const hideResult = (() => {
                        return collapseInstance.hide(hideConfig);
                    });

                    expect(hideResult).toThrow(errorMessage);
                });
        });

        test('должен вызывать событие \'hidden\' только с помощью метода \'emit\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const hiddenCall = (() => collapseInstance.hidden());
            const hiddenEmit = collapseInstance.emit('hidden');

            expect(hiddenCall).toThrow('collapseInstance.hidden is not a function');
            expect(hiddenEmit).toBeUndefined();
        });

        test('должен вызывать событие \'hidden\' после завершения скрытия контентного блока', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const hiddenFunction = jest.fn();
            collapseInstance.on('hidden', () => hiddenFunction());

            collapseInstance.hide();

            jest.runAllTimers();

            expect(hiddenFunction).toHaveBeenCalledWith();
        });

        test('должен вызывать событие \'hidden\' только для видимых элементов', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            const hiddenFunction = jest.fn();
            collapseInstance.on('hidden', () => hiddenFunction());

            collapseInstance.hide();

            jest.runAllTimers();

            expect(hiddenFunction).not.toHaveBeenCalled();
        });

        test('должен корректно обновлять атрибуты управляющих элементов', () => {
            const collapseControlEl = document.createElement('a');
            util.activate(collapseControlEl);
            collapseControlEl.href = '#collapse-test';
            document.body.append(collapseControlEl);

            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseControlEl);
            collapseInstance.on('hidden', () => {
                expect(collapseControlEl.classList).toContain('inactive');
                expect(collapseControlEl.classList).not.toContain('active');
                expect(collapseControlEl.ariaExpanded).toBe('false');
            });

            collapseInstance.hide();
        });

        test('должен корректно обновлять классы контентных элементов', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            collapseInstance.on('hidden', () => {
                expect(collapseTargetEl.classList).not.toContain('show');
                expect(collapseTargetEl.classList).not.toContain('toggling');
            });

            collapseInstance.hide();

            jest.advanceTimersByTime(1);

            expect(collapseTargetEl.classList).toContain('toggling');

            jest.runAllTimers();
        });

        test('должен корректно обновлять свойство \'isShown\'', () => {
            const collapseTargetEl = document.createElement('div');
            collapseTargetEl.id = 'collapse-test';
            collapseTargetEl.classList.add('collapse', 'toggled', 'show');
            document.body.append(collapseTargetEl);

            const collapseInstance = new AppCollapse(collapseTargetEl);
            collapseInstance.on('hidden', () => {
                expect(collapseInstance.isShown).toBe(false);
            });

            collapseInstance.hide();
        });

        //TODO:test height change, transition-duration
    });
});