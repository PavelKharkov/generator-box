import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'collapse';

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

//data-атрибуты
const dataCollapse = moduleName;
const dataCollapseId = `${moduleName}Id`;
const dataCollapseSelector = `data-${util.kebabCase(dataCollapse)}`;
const dataCollapseIdSelector = `data-${util.kebabCase(dataCollapseId)}`;

//Классы
const showClass = 'show';
const collapsingClass = 'toggling';
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    duration: transitionDuration,
    dataToggle: dataCollapse,
    dataToggleId: dataCollapseId
};

const initCollapse = AppToggle => {
    /**
     * @class AppCollapse
     * @memberof components
     * @requires components#AppToggle
     * @requires components#AppUrl
     * @requires layout#AppWindow
     * @classdesc Модуль плавного появления.
     * @desc Наследует: [AppToggle]{@link components.AppToggle}.
     * @async
     * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность переключения элемента.
     * @param {string} [options.dataToggle='collapse'] - Data-атрибут для селектора активирующего элемента.
     * @param {string} [options.dataToggleId='collapseId'] - Data-атрибут для селектора переключаемого элемента.
     * @param {number} [options.initialHeight] - Высота переключаемого элемента в скрытом состоянии. По умолчанию высчитывается из текущей высоты элемента.
     * @example
     * const collapseInstance = new app.Collapse(document.querySelector('.collapse-button'), {
     *     duration: 350,
     *     initialHeight: 0,
     *     parent: document.querySelector('.collapse-container')
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <a href="#collapse-target" role="button" aria-expanded="false" aria-controls="collapse-target">Переключатель</a>
     * <!--Можно также использовать data-collapse для указания целевого элемента-->
     *
     * <div class="collapse" id="collapse-target" class="collapse toggled">Контент</div>
     * <!--Можно также использовать data-collapse-id для идентификации элемента-->
     *
     * <!--data-collapse - селектор по умолчанию-->
     *
     * @example <caption>Пример HTML-разметки с родительским элементом</caption>
     * <!--HTML-->
     * <a href="#collapse-target" role="button" aria-expanded="true" class="active" aria-controls="collapse-target">Переключатель</a>
     * <a href="#collapse-target2" role="button" aria-expanded="false" aria-controls="collapse-target">Переключатель 2</a>
     *
     * <div class="collapse-container">
     *     <div id="collapse-target" data-hash-action="collapse" class="collapse toggled show">Контент</div>
     *     <div id="collapse-target2" data-hash-action="collapse" class="collapse toggled">Контент 2</div>
     * </div>
     *
     * @example <caption>Активация с помощью хеша</caption>
     * <!--HTML-->
     * <div id="collapse-target" data-hash-action="collapse">Контент</div>
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div id="collapse-target" data-collapse-options='{"duration": 350}'>Контент</div>
     */
    const AppCollapse = immutable(class extends AppToggle {
        constructor(el, opts, appname = moduleName) {
            Module.checkHTMLElement(el);

            if ((typeof opts !== 'undefined') && !util.isObject(opts)) {
                util.typeError(opts, 'opts', 'plain object');
            }
            const collapseOptions = (typeof opts === 'undefined') ? {} : opts;

            //Добавление опций из data-атрибута
            const collapseOptionsData = el.dataset.collapseOptions;
            if (collapseOptionsData) {
                const dataOptions = util.stringToJSON(collapseOptionsData);
                if (!dataOptions) util.error('incorrect data-collapse-options format');
                util.extend(collapseOptions, dataOptions);
            }

            util.defaultsDeep(collapseOptions, defaultOptions);

            /**
             * @member {Object} AppCollapse#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            super(el, collapseOptions, appname);

            const targetElements = this.targetElements;

            const toggleElements = [...this.controlElements, ...targetElements];
            toggleElements.forEach(element => { //Добавить ссылку на модуль collapse всем участвующим элементам
                if (!element.modules) {
                    element.modules = {};
                }
                if (!element.modules[moduleName]) {
                    element.modules[moduleName] = this;
                }
            });

            let initialHeights;

            /**
             * @member {Array.<number>} AppCollapse#initialHeights
             * @memberof components
             * @desc Коллекция высот раскрывающихся элементов по умолчанию.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'initialHeights', {
                enumerable: true,
                get: () => initialHeights
            });

            /**
             * @function setInitialStyles
             * @desc Определяет высоты блоков в свёрнутом состоянии.
             * @returns {undefined}
             * @ignore
             */
            const setInitialStyles = () => {
                initialHeights = targetElements.map(element => {
                    if (this.isShown || !element.offsetParent) {
                        return 0;
                    }

                    const initialHeight = (typeof this.params.initialHeight === 'undefined')
                        ? element.offsetHeight
                        : this.params.initialHeight;
                    if (initialHeight > 0) {
                        element.style.display = 'block';
                        element.style.overflow = 'hidden';
                    }
                    element.style.height = `${initialHeight}px`;
                    return initialHeight;
                });
            };
            setInitialStyles();

            /**
             * @function removeHeightStyles
             * @desc Сбрасывает стилевые свойства высоты раскрывающегося элемента.
             * @returns {undefined}
             * @ignore
             */
            const removeHeightStyles = () => {
                targetElements.forEach(element => {
                    element.style.height = '';
                    element.style.maxHeight = '';
                });
            };

            //Привязка скриптов плавного раскрытия блоков к событиям модуля переключения блоков
            this.on({
                show() {
                    util.animationDefer(() => {
                        targetElements.forEach(element => {
                            if (element.classList.contains(showClass)) return;

                            const newHeight = `${element.scrollHeight}px`;
                            element.style.height = newHeight;
                            element.style.maxHeight = newHeight;
                        });
                    });
                },

                hide() {
                    targetElements.forEach((element, elementIndex) => {
                        if (element.classList.contains(showClass) && !element.classList.contains(collapsingClass)) {
                            return;
                        }

                        element.style.height = `${element.scrollHeight}px`;
                        util.animationDefer(() => {
                            const heightNumber = initialHeights ? initialHeights[elementIndex] : '0';
                            element.style.height = `${heightNumber}px`;
                        });
                    });
                }
            });

            this.onSubscribe({
                shown() {
                    removeHeightStyles();
                },

                hidden() {
                    removeHeightStyles();
                }
            });

            toggleElements.forEach(element => {
                element.classList.add(initializedClass);
            });

            if (typeof el.dataset.collapseTriggered !== 'undefined') {
                delete el.dataset.collapseTriggered;
                this.toggle();
            }
        }
    });

    //Настройка возможности управления с помощью адресной строки
    require('Components/url').default.then(AppUrl => {
        AppUrl.registerAction({
            id: moduleName,
            selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
            action: `${moduleName}.toggle`,
            idSelectors: ['id', `${dataCollapseIdSelector}`]
        });
    });
    if (window.location.hash) emitInit('url');

    addModule(moduleName, AppCollapse);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll(`[${dataCollapseSelector}]`).forEach(el => new AppCollapse(el));

    return AppCollapse;
};

export default initCollapse;
export {initCollapse};