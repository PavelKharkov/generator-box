import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let dropdownTriggered;

const dropdownCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popovers" */ './sync.js'))
        .then(modules => {
            chunks.popovers = true;
            dropdownTriggered = true;

            const AppDropdown = modules.default;
            if (resolve) resolve(AppDropdown);
        });
};

const initDropdown = event => {
    if (dropdownTriggered) return;

    event.currentTarget.dataset.dropdownTriggered = '';

    emitInit('dropdown');
};

const dropdownTrigger = (dropdownItems, dropdownTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (dropdownItems instanceof Node) ? [dropdownItems] : dropdownItems;
    if (items.length === 0) return;

    items.forEach(item => {
        dropdownTriggers.forEach(trigger => {
            item.addEventListener(trigger, initDropdown, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popovers) {
        dropdownCallback(resolve);
        return;
    }

    onInit('dropdown', () => {
        dropdownCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('dropdown');
    });

    const dropdownItems = document.querySelectorAll('[data-dropdown]');
    dropdownTrigger(dropdownItems);
});

export default importFunc;
export {dropdownTrigger};