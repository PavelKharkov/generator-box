import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import AppDropdown from '../sync.js';

describe('AppDropdown', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.show()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(showFunction, value) {
                return showFunction(value);
            },
            setupFunction() {
                const dropdownEl = document.createElement('div');

                const dropdownButtonEl = document.createElement('button');
                dropdownButtonEl.classList.add('dropdown-toggle');
                dropdownEl.append(dropdownButtonEl);

                const dropdownMenuEl = document.createElement('div');
                dropdownMenuEl.classList.add('dropdown-menu');
                dropdownEl.append(dropdownMenuEl);

                document.body.append(dropdownEl);

                const dropdownInstance = new AppDropdown(dropdownEl);
                const showFunction = dropdownInstance.show;

                return showFunction;
            }
        });
    });
});