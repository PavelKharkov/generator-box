import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import util from 'Layout/main';

//Инициализация тестового элемента с атрибутом data-dropdown
const dropdownWithDataEl = document.createElement('div');
dropdownWithDataEl.dataset.dropdown = '';

const dropdownButtonInitialEl = document.createElement('button');
dropdownButtonInitialEl.classList.add('dropdown-toggle');
dropdownWithDataEl.append(dropdownButtonInitialEl);

const dropdownMenuInitialEl = document.createElement('div');
dropdownMenuInitialEl.classList.add('dropdown-menu');
dropdownWithDataEl.append(dropdownMenuInitialEl);

document.body.append(dropdownWithDataEl);

const AppDropdown = require('../sync.js').default;

describe('AppDropdown', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const dropdownPromise = require('..').default;

        expect(dropdownPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            return require('..').default.then(AppDropdownTest => {
                expect(AppDropdownTest.name).toBe(AppDropdown.name);
                done();
            });
        });
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalDropdown = window.app.Dropdown;

        expect(globalDropdown.prototype).toBeInstanceOf(Module);
    });

    const dataTypesAssertions = ['HTMLElement']
        .map(value => [value, [throwsError, 'can\'t find toggle control element']]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.dropdown;
            return new AppDropdown(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, 'parameter \'element\' must be a HTMLElement'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDropdown\' с переданным значением'
    });

    test('должен сохраняться под именем \'dropdown\' в свойстве \'modules\' у элементов', () => {
        const dropdownEl = document.createElement('div');

        const dropdownButtonEl = document.createElement('button');
        dropdownButtonEl.classList.add('dropdown-toggle');
        dropdownEl.append(dropdownButtonEl);

        const dropdownMenuEl = document.createElement('div');
        dropdownMenuEl.classList.add('dropdown-menu');
        dropdownEl.append(dropdownMenuEl);

        document.body.append(dropdownEl);

        new AppDropdown(dropdownEl); /* eslint-disable-line no-new */
        const dropdownInstanceProp = dropdownEl.modules.dropdown;

        expect(dropdownInstanceProp).toBeInstanceOf(AppDropdown);
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-dropdown\'', () => {
        const dropdownInstance = dropdownWithDataEl.modules.dropdown;

        expect(dropdownInstance).toBeInstanceOf(AppDropdown);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const dropdownPropsArray = [
            'placement',
            'isShown',
            'toggleElement',
            'menuElement',
            'show',
            'hide'
        ];
        test.each(dropdownPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);

            const currentProp = dropdownInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const dropdownEventsArray = [
            'show',
            'hide',
            'shown',
            'hidden'
        ];
        test.each(dropdownEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const eventsArray = dropdownInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const dropdownOptionsMap = [
            ['duration', Number.parseInt(util.getStyle('--transition-duration'), 10)],
            ['outsideClickHide', true],
            ['boundary', 'clippingParents'],
            ['placement', 'bottom']
        ];
        test.each(dropdownOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const instanceOptions = dropdownInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    test('должен выбрасывать ошибку при отсутствии элемента переключения меню', () => {
        const dropdownEl = document.createElement('div');

        const dropdownMenuEl = document.createElement('div');
        dropdownMenuEl.classList.add('dropdown-menu');
        dropdownEl.append(dropdownMenuEl);

        document.body.append(dropdownEl);

        const dropdownInstance = (() => new AppDropdown(dropdownEl));

        expect(dropdownInstance).toThrow('can\'t find toggle control element');
    });

    test('должен выбрасывать ошибку при отсутствии элемента меню', () => {
        const dropdownEl = document.createElement('div');

        const dropdownButtonEl = document.createElement('button');
        dropdownButtonEl.classList.add('dropdown-toggle');
        dropdownEl.append(dropdownButtonEl);

        document.body.append(dropdownEl);

        const dropdownInstance = (() => new AppDropdown(dropdownEl));

        expect(dropdownInstance).toThrow('can\'t find menu element');
    });

    test('должен добавлять класс корневому элементу при успешной инициализации', () => {
        const dropdownEl = document.createElement('div');

        const dropdownButtonEl = document.createElement('button');
        dropdownButtonEl.classList.add('dropdown-toggle');
        dropdownEl.append(dropdownButtonEl);

        const dropdownMenuEl = document.createElement('div');
        dropdownMenuEl.classList.add('dropdown-menu');
        dropdownEl.append(dropdownMenuEl);

        document.body.append(dropdownEl);

        new AppDropdown(dropdownEl); /* eslint-disable-line no-new */
        const dropdownClass = dropdownEl.getAttribute('class');

        expect(dropdownClass).toBe('dropdown-initialized');
    });

    describe('[data-dropdown-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-dropdown-options\'', () => {
            const dropdownEl = document.createElement('div');
            dropdownEl.dataset.dropdownOptions = '{"duration": 200}';

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const instanceOptions = dropdownInstance.options;

            expect(instanceOptions.duration).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-dropdown-options\'', () => {
            const dropdownEl = document.createElement('div');
            dropdownEl.dataset.dropdownOptions = 'true';

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = (() => new AppDropdown(dropdownEl));

            expect(dropdownInstance).toThrow('incorrect data-dropdown-options format');
        });
    });

    describe('опции', () => {
        describe('duration', () => {
            const durationTypesAssertions = [
                'zero',
                'number',
                'float number',
                'negative number',
                'undefined'
            ].map(el => [el, [isInstance, AppDropdown]]);
            dataTypesCheck({
                checkFunction(dropdownEl, value) {
                    if (dropdownEl.modules) delete dropdownEl.modules.dropdown;
                    const datepickerInstance = new AppDropdown(dropdownEl, {
                        duration: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: durationTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.duration\' must be a number'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDropdown\' с опцией \'duration\'',
                setupFunction() {
                    const dropdownEl = document.createElement('div');

                    const dropdownButtonEl = document.createElement('button');
                    dropdownButtonEl.classList.add('dropdown-toggle');
                    dropdownEl.append(dropdownButtonEl);

                    const dropdownMenuEl = document.createElement('div');
                    dropdownMenuEl.classList.add('dropdown-menu');
                    dropdownEl.append(dropdownMenuEl);

                    document.body.append(dropdownEl);

                    return dropdownEl;
                }
            });
        });

        describe('boundary', () => {
            const boundaryTypesAssertions = [
                'Function',
                'HTMLElement',
                'undefined'
            ].map(el => [el, [isInstance, AppDropdown]]);
            dataTypesCheck({
                checkFunction(dropdownEl, value) {
                    if (dropdownEl.modules) delete dropdownEl.modules.dropdown;
                    const datepickerInstance = new AppDropdown(dropdownEl, {
                        boundary: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: boundaryTypesAssertions,
                defaultValue: [
                    throwsError,
                    'parameter \'options.boundary\' must be a \'clippingParents\', instance of HTMLElement or function'
                ],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDropdown\' с опцией \'boundary\'',
                setupFunction() {
                    const dropdownEl = document.createElement('div');

                    const dropdownButtonEl = document.createElement('button');
                    dropdownButtonEl.classList.add('dropdown-toggle');
                    dropdownEl.append(dropdownButtonEl);

                    const dropdownMenuEl = document.createElement('div');
                    dropdownMenuEl.classList.add('dropdown-menu');
                    dropdownEl.append(dropdownMenuEl);

                    document.body.append(dropdownEl);

                    return dropdownEl;
                }
            });
        });

        describe('placement', () => {
            const placementTypesAssertions = [
                'string number',
                'empty string',
                'space',
                'string',
                'HTML string',
                'undefined'
            ].map(el => [el, [isInstance, AppDropdown]]);
            dataTypesCheck({
                checkFunction(dropdownEl, value) {
                    if (dropdownEl.modules) delete dropdownEl.modules.dropdown;
                    const datepickerInstance = new AppDropdown(dropdownEl, {
                        placement: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: placementTypesAssertions,
                defaultValue: [
                    throwsError,
                    'parameter \'options.placement\' must be a string'
                ],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDropdown\' с опцией \'placement\'',
                setupFunction() {
                    const dropdownEl = document.createElement('div');

                    const dropdownButtonEl = document.createElement('button');
                    dropdownButtonEl.classList.add('dropdown-toggle');
                    dropdownEl.append(dropdownButtonEl);

                    const dropdownMenuEl = document.createElement('div');
                    dropdownMenuEl.classList.add('dropdown-menu');
                    dropdownEl.append(dropdownMenuEl);

                    document.body.append(dropdownEl);

                    return dropdownEl;
                }
            });
        });
    });

    describe('toggleElement', () => {
        test('должно содержать элемент с классом \'dropdown-toggle\' внутри инициализирующего элемента', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const toggleElement = dropdownInstance.toggleElement;

            expect(toggleElement).toBe(dropdownButtonEl);
        });

        test('должно добавлять ссылку на экземпляр модуля на элемент переключателя', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            new AppDropdown(dropdownEl); /* eslint-disable-line no-new */
            const instanceProp = dropdownButtonEl.modules.dropdown;

            expect(instanceProp).toBeInstanceOf(AppDropdown);
        });

        test('должно выбрасывать ошибку при попытке изменить свойство', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const toggleElementChange = (() => {
                dropdownInstance.toggleElement = document.createElement('div');
            });

            expect(toggleElementChange).toThrow('Cannot assign to read only property \'toggleElement\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('menuElement', () => {
        test('должно содержать элемент с классом \'dropdown-menu\' внутри инициализирующего элемента', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const menuElement = dropdownInstance.menuElement;

            expect(menuElement).toBe(dropdownMenuEl);
        });

        test('должно добавлять ссылку на экземпляр модуля на элемент меню', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            new AppDropdown(dropdownEl); /* eslint-disable-line no-new */
            const instanceProp = dropdownMenuEl.modules.dropdown;

            expect(instanceProp).toBeInstanceOf(AppDropdown);
        });

        test('должно выбрасывать ошибку при попытке изменить свойство', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const menuElementChange = (() => {
                dropdownInstance.menuElement = document.createElement('div');
            });

            expect(menuElementChange).toThrow('Cannot assign to read only property \'menuElement\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('placement', () => {
        test('должно при инициализации получать значение из опции \'placement\'', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl, {
                placement: 'top'
            });
            const placement = dropdownInstance.placement;
            const placementOption = dropdownInstance.options.placement;

            expect(placement).toBe(placementOption);
        });

        test('должно выбрасывать ошибку при попытке изменить свойство', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const placementChange = (() => {
                dropdownInstance.placement = 'top';
            });

            expect(placementChange).toThrow('Cannot set property placement of #<ImmutableClass> which has only a getter');
        });

        test('должно обновляться после изменения позиционирования выпадающего меню', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show({
                placement: 'top'
            });

            expect(dropdownInstance.placement).toBe('top');
        });

        test('должно добавлять соответствующий класс элементу экземпляра при изменении позиционирования', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show({
                placement: 'top'
            });
            const containsPlacementClass = dropdownEl.classList.contains('dropdown-top');

            expect(containsPlacementClass).toBe(true);
        });
    });

    describe('isShown', () => {
        test('должно при инициализации быть false', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const isShown = dropdownInstance.isShown;

            expect(isShown).toBe(false);
        });

        test('должно выбрасывать ошибку при попытке изменить свойство', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const shownChange = (() => {
                dropdownInstance.isShown = true;
            });

            expect(shownChange).toThrow('Cannot set property isShown of #<ImmutableClass> which has only a getter');
        });

        test('должно корректно обновляться после открытия выпадающего меню', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show();

            expect(dropdownInstance.isShown).toBe(true);
        });

        test('должно корректно обновляться после закрытия выпадающего меню', async () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show();

            expect(dropdownInstance.isShown).toBe(true);

            await new Promise(done => {
                dropdownInstance.hide();

                setTimeout(() => {
                    expect(dropdownInstance.isShown).toBe(false);
                    done();
                }, Number.parseInt(util.getStyle('--transition-duration'), 10) + 1);

                jest.runAllTimers();
            });
        });
    });

    describe('pluginInstance', () => {
        test('должно инициализироваться с неустановленным значением', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);

            expect(dropdownInstance.pluginInstance).toBeUndefined();
        });

        test('должно хранить ссылку на экземпляр плагина показа выпадающего меню после открытия меню', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show();

            expect(dropdownInstance.pluginInstance).toBeInstanceOf(Object);
        });

        const optionsArray = [
            'state',
            'setOptions',
            'forceUpdate',
            'update',
            'destroy'
        ];
        test.each(optionsArray)('должно являться объектом, содержащим свойство \'%s\'', property => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            dropdownInstance.show();
            const pluginInstance = dropdownInstance.pluginInstance;

            expect(Object.prototype.hasOwnProperty.call(pluginInstance, property)).toBe(true);
        });

        test('должно выбрасывать ошибку при попытке изменить свойство', () => {
            const dropdownEl = document.createElement('div');

            const dropdownButtonEl = document.createElement('button');
            dropdownButtonEl.classList.add('dropdown-toggle');
            dropdownEl.append(dropdownButtonEl);

            const dropdownMenuEl = document.createElement('div');
            dropdownMenuEl.classList.add('dropdown-menu');
            dropdownEl.append(dropdownMenuEl);

            document.body.append(dropdownEl);

            const dropdownInstance = new AppDropdown(dropdownEl);
            const pluginInstanceChange = (() => {
                dropdownInstance.pluginInstance = {};
            });

            expect(pluginInstanceChange).toThrow('Cannot set property pluginInstance of #<ImmutableClass> which has only a getter');
        });
    });
});