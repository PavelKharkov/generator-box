import createPopper from 'Libs/popper';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'dropdown';

const dropdownPlugin = createPopper;

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

//Селекторы и классы
const dropdownClass = moduleName;
const dropdownToggleSelector = `.${dropdownClass}-toggle`;
const dropdownMenuSelector = `.${dropdownClass}-menu`;
const activeClass = 'active';
const showClass = 'show';
const hidingClass = 'hiding';
const initializedClass = `${dropdownClass}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    duration: transitionDuration,
    outsideClickHide: true,
    boundary: 'clippingParents',
    placement: 'bottom'
};

/**
 * @class AppDropdown
 * @memberof components
 * @requires libs.popper
 * @classdesc Модуль для выпадающих меню.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент контейнера выпадающего меню.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность анимации переключения выпадающего меню.
 * @param {boolean} [options.outsideClickHide=true] - Закрывать ли выпадающее меню при нажатии вне него.
 * @param {('scrollParent'|HTMLElement|Function)} [options.boundary='clippingParents'] - В границах какого элемента пытаться уместить выпадающее меню.
 * @param {string} [options.placement='bottom'] - Направления открытия меню по умолчанию.
 * @param {boolean} [options.flipVariations=true] - Изменять ли направление показа выпадающего меню на противоположное, если он целиком не помещается в границах опции boundary.
 * @example
 * const dropdownInstance = new app.Dropdown(document.querySelector('.dropdown-element'), {
 *     duration: 300,
 *     placement: 'top'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="dropdown" data-dropdown>
 *     <button class="dropdown-toggle" type="button"></button>
 *     <div class="dropdown-menu">
 *         <ul class="dropdown-list">
 *             <li class="dropdown-item"></li>
 *         </ul>
 *     </div>
 * </div>
 *
 * <!--data-dropdown - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div class="dropdown" data-dropdown-options='{"placement": "top"}'></div>
 */
const AppDropdown = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const toggleElement = el.querySelector(dropdownToggleSelector);
        if (!toggleElement) util.error('can\'t find toggle control element');

        const menuElement = el.querySelector(dropdownMenuSelector);
        if (!menuElement) util.error('can\'t find menu element');

        const dropdownOptionsData = el.dataset.dropdownOptions;
        if (dropdownOptionsData) {
            const dataOptions = util.stringToJSON(dropdownOptionsData);
            if (!dataOptions) util.error('incorrect data-dropdown-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppDatepicker#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        if (!Number.isFinite(params.duration)) {
            util.typeError(params.duration, 'params.duration', 'number');
        }

        if (
            ((typeof params.boundary !== 'string') || (params.boundary !== 'clippingParents')) &&
            !(params.boundary instanceof HTMLElement) &&
            (typeof params.boundary !== 'function')
        ) {
            util.typeError(params.boundary, 'params.boundary', '\'clippingParents\', instance of HTMLElement or function');
        }

        if (typeof params.placement !== 'string') {
            util.typeError(params.placement, 'params.placement', 'string');
        }

        toggleElement.modules = toggleElement.modules || {};
        toggleElement.modules[moduleName] = this;

        /**
         * @member {HTMLElement} AppDropdown#toggleElement
         * @memberof components
         * @desc Элемент кнопки, по нажатию на которую переключается выпадающее меню.
         * @readonly
         */
        Object.defineProperty(this, 'toggleElement', {
            enumerable: true,
            value: toggleElement
        });

        menuElement.modules = menuElement.modules || {};
        menuElement.modules[moduleName] = this;

        /**
         * @member {HTMLElement} AppDropdown#menuElement
         * @memberof components
         * @desc Элемент выпадающего меню.
         * @readonly
         */
        Object.defineProperty(this, 'menuElement', {
            enumerable: true,
            value: menuElement
        });

        if (el.classList.contains(`${dropdownClass}-top`)) {
            params.placement = 'top';
        }
        let placement = params.placement;

        /**
         * @member {string} AppDropdown#placement
         * @memberof components
         * @desc Хранит последнее направление открытого выпадающего меню.
         * @readonly
         * @example
         * const dropdownInstance = new app.Dropdown(document.querySelector('.dropdown-element'));
         * console.log(dropdownInstance.placement); //По умолчанию, хранит направление открытия меню из опции инициализации
         */
        Object.defineProperty(this, 'placement', {
            enumerable: true,
            get: () => placement
        });

        let isShown = menuElement.classList.contains(showClass);

        /**
         * @member {boolean} AppDropdown#isShown
         * @memberof components
         * @desc Указывает, показан ли элемент.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let pluginInstance;

        /**
         * @member {Object} AppDropdown#pluginInstance
         * @memberof components
         * @desc Объект экземпляра плагина отображения выпадающего меню.
         * @readonly
         */
        Object.defineProperty(this, 'pluginInstance', {
            enumerable: true,
            get: () => pluginInstance
        });

        toggleElement.addEventListener('click', event => {
            event.preventDefault();

            if (isShown) {
                this.hide();
                return;
            }

            this.show();
        });

        //Скрывать меню по нажатию пункта меню
        menuElement.addEventListener('click', event => {
            if (!(isShown && event.target.closest('.dropdown-link'))) return;

            this.hide();
        });

        if (params.outsideClickHide) {
            document.addEventListener('click', event => {
                const isOutsideClick = isShown && !toggleElement.contains(event.target) && !menuElement.contains(event.target);
                if (isOutsideClick) {
                    this.hide();
                }
            });
        }

        let toggleTimeout;

        //Внутренние функции
        /**
         * @function updatePlacement
         * @desc Обновляет элементы и свойства после изменения положения выпадающего меню.
         * @param {string} newPlacement - Новое направление положения выпадающего меню.
         * @returns {undefined}
         * @ignore
         */
        const updatePlacement = newPlacement => {
            el.classList.remove(`${dropdownClass}-${placement}`);
            el.classList.add(`${dropdownClass}-${newPlacement}`);
            placement = newPlacement;
        };

        /**
         * @function clearDropdown
         * @desc Очищает атрибуты, классы и настройки плагина после скрытия выпадающего меню.
         * @returns {undefined}
         * @ignore
         */
        const clearDropdown = () => {
            if (!pluginInstance) return;

            menuElement.classList.remove(hidingClass);
            toggleElement.classList.remove(activeClass);

            pluginInstance.destroy();
        };

        this.on({
            /**
             * @event AppDropdown#show
             * @memberof components
             * @desc Показывает выпадающее меню. После завершения анимации показа вызывает событие [shown]{@link components.AppDropdown#event:shown}.
             * @param {Object} [options] - Опции открытия выпадающего меню.
             * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность анимации переключения выпадающего меню.
             * @param {('clippingParents'|HTMLElement|Function)} [options.boundary='clippingParents'] - В границах какого элемента пытаться уместить выпадающее меню.
             * @param {string} [options.placement='bottom'] - Направления открытия меню по умолчанию.
             * @param {boolean} [options.flipVariations=true] - Изменять ли направление показа выпадающего меню на противоположное, если он целиком не помещается в границах опции boundary.
             * @fires components.AppDropdown#shown
             * @returns {undefined}
             * @example
             * dropdownInstance.on('show', () => {
             *     console.log('Выпадающее меню показывается');
             * });
             * dropdownInstance.show({
             *     duration: 0
             * });
             * setTimeout(() => {
             *     console.log(dropdownInstance.isShown); //true
             * }, 0);
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                const boundary = (typeof options.boundary === 'function') ? options.boundary.call(this) : options.boundary;
                if (((typeof boundary !== 'string') || (boundary !== 'clippingParents')) &&
                    !(boundary instanceof HTMLElement) && (boundary !== 'function')) {
                    util.typeError(boundary, 'options.boundary', '\'clippingParents\', instance of HTMLElement or function');
                }

                if (typeof options.placement !== 'string') {
                    util.typeError(options.placement, 'options.placement', 'string');
                }

                if (isShown) return;

                if (!el.offsetParent) return; //Если элемент скрыт

                clearDropdown();
                clearTimeout(toggleTimeout);

                const flipVariations = options.flipVariations;

                updatePlacement(options.placement);

                toggleElement.classList.add(activeClass);

                util.animationDefer(() => {
                    menuElement.classList.add(showClass);
                });

                pluginInstance = dropdownPlugin(toggleElement, menuElement, {
                    placement: options.placement,
                    modifiers: [
                        {
                            name: 'flip',
                            options: {
                                flipVariations
                            }
                        },
                        {
                            name: 'preventOverflow',
                            options: {
                                boundary
                            }
                        },
                        {
                            name: 'updatePlacement',
                            enabled: true,
                            phase: 'afterWrite',
                            fn({state}) {
                                updatePlacement(state.placement);
                            }
                        }
                    ],
                    onFirstUpdate(state) {
                        updatePlacement(state.placement);
                    }
                });

                isShown = true;

                toggleTimeout = setTimeout(() => {
                    this.emit('shown');
                }, params.duration);
            },

            /**
             * @event AppDropdown#hide
             * @memberof components
             * @desc Скрывает выпадающее меню. После завершения анимации скрытия вызывает событие [hidden]{@link components.AppDropdown#event:hidden}.
             * @param {Object} [options] - Опции открытия выпадающего меню.
             * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность анимации переключения выпадающего меню.
             * @fires components.AppDropdown#hidden
             * @returns {undefined}
             * @example
             * dropdownInstance.on('hide', () => {
             *     console.log('Выпадающее меню скрывается');
             * });
             * dropdownInstance.hide({
             *     duration: 0
             * });
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                if (!isShown) return;

                isShown = false;

                clearTimeout(toggleTimeout);

                menuElement.classList.add(hidingClass);
                menuElement.classList.remove(showClass);

                toggleTimeout = setTimeout(() => {
                    clearDropdown();

                    this.emit('hidden');
                }, options.duration);
            }
        });

        this.onSubscribe({
            /**
             * @event AppDropdown#shown
             * @memberof components
             * @desc Вызывается после показа выпадающего меню.
             * @returns {undefined}
             * @example
             * dropdownInstance.on('shown', () => {
             *     console.log('выпадающее меню показано');
             * });
             */
            shown: true,

            /**
             * @event AppDropdown#hidden
             * @memberof components
             * @desc Вызывается после скрытия выпадающего меню.
             * @returns {undefined}
             * @example
             * dropdownInstance.on('hidden', () => {
             *     console.log('выпадающее меню скрыто');
             * });
             */
            hidden() {
                updatePlacement(params.placement);
            }
        });

        el.classList.add(initializedClass);

        if (typeof el.dataset.dropdownTriggered !== 'undefined') {
            delete el.dataset.dropdownTriggered;
            this.show();
        }
    }
});

addModule(moduleName, AppDropdown);

//Инициализировать элементы по data-атрибуту
document.querySelectorAll('[data-dropdown]').forEach(el => new AppDropdown(el));

export default AppDropdown;
export {AppDropdown};