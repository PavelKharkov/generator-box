let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const sliderOwlCallback = resolve => {
        const imports = [require('Components/svg').default];

        Promise.all(imports).then(modules => {
            (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "sliders" */ './sync.js'))
                .then(slider => {
                    chunks.sliders = true;

                    const AppSliderOwl = slider.sliderOwlInit(...modules);
                    resolve(AppSliderOwl);
                });
        });

        emitInit('svg');
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.sliders) {
            sliderOwlCallback(resolve);
            return;
        }

        onInit('sliderOwl', () => {
            sliderOwlCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('sliderOwl');
        });
    });
}

export default importFunc;