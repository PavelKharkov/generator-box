import jQuery from 'Libs/jquery'; //Используется для плагина owlCarousel
import 'Libs/owl-carousel';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'sliderOwl';

//Селекторы
const sliderPrefix = 'slider';
const pluginPrefix = 'owl';
const sliderAddClass = `${pluginPrefix}-carousel`;
const itemsWrapperSelector = `.${pluginPrefix}-stage-outer`;
const itemSelector = `.${pluginPrefix}-item`;
const itemActiveClass = 'active';
const navSelector = `.${pluginPrefix}-nav`;
const prevClass = `${pluginPrefix}-prev`;
const nextClass = `${pluginPrefix}-next`;
const dotsSelector = `.${pluginPrefix}-dots`;
const equalHeightClass = `${pluginPrefix}-height`;
const disabledClass = 'disabled';
const sliderPrevDisabled = 'slider-prev-disabled';
const sliderNextDisabled = 'slider-next-disabled';
const initializingClass = `${sliderPrefix}-initializing`;
const initializedClass = `${sliderPrefix}-initialized`;

//Другие опции
const arrowIconConfig = {
    id: 'arrow',
    width: 11,
    height: 9
};

const defaultTransitionDuration = 400;

//Опции по умолчанию
const defaultOptions = {
    init: true,
    minItems: 2,
    gotoDuration: defaultTransitionDuration,

    heightRecount: true,
    controlsRecount: true,
    addNavArrowIcon: true,
    dotsBetweenNav: true
};

//Опции плагина слайдера по умолчанию
const defaultPluginOptions = {
    items: 1,
    lazyLoad: true,

    //TODO:add other default options to docs
    nav: true,
    navText: false,
    dotsEach: 1,

    responsiveRefreshRate: 0,
    responsive: {
        [util.media.sm]: {
            items: 2
        },
        [util.media.md]: {
            items: 3
        },
        [util.media.lg]: {
            items: 4
        }
    }
};

const sliderOwlInit = AppSvg => {
    /**
     * @class AppSliderOwl
     * @memberof components
     * @requires libs.jquery
     * @requires libs.owl-carousel
     * @requires components#AppSvg
     * @classdesc Модуль для контентных слайдеров.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент слайдера.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {boolean} [options.init=true] - Инициализировать слайдер сразу же после инициализации экземпляра.
     * @param {number} [options.minItems=2] - Минимальное количество элементов, необходимое для инициализации слайдера.
     * @param {number} [options.gotoDuration=400] - Длительность перехода на другой сладй при использовании методов.
     * @param {boolean} [options.controlsRecount=true] - Вызывать ли метод controlsRecount при инициализации и при изменении слайдера.
     * @param {boolean} [options.heightRecount=true] - Вызывать ли метод heightRecount при инициализации и при изменении слайдера.
     * @param {boolean} [options.addNavArrowIcon=true] - Вызывать ли метод addNavArrowIcon при инициализации.
     * @param {boolean} [options.dotsBetweenNav=true] - Вызывать ли метод dotsBetweenNav при инициализации.
     * @param {Object} [options.pluginOptions={}] - Опции плагина [Owl Carousel]{@link libs.owl-carousel}.
     * @param {number} [options.pluginOptions.items] - Количество одновременно отображаемых слайда. Если опции плагина не указаны, то items = 1.
     * @param {boolean} [options.pluginOptions.lazyLoad] - Испрользовать ли отложенную загрузку изображений в слайдере. Если опции плагина не указаны, то lazyLoad = true.
     * @param {boolean} [options.pluginOptions.nav] - Показывать ли элементы навигации. Если опции плагина не указаны, то nav = true.
     * @param {(boolean|Array.<string>)} [options.pluginOptions.navText] - Текст стрелок для навигации. Если опции плагина не указаны, то navText = false.
     * @param {number} [options.pluginOptions.dotsEach] - Сколько слайдов составляют одну страницу слайдера. Если опции плагина не указаны, то dotsEach = 1.
     * @param {*} [options.pluginOptions...] - Другие опции плагина [Owl Carousel]{@link libs.owl-carousel}.
     * @example
     * const sliderInstance = new app.SliderOwl(document.querySelector('.slider-element'), {
     *     minItems: 3,
     *     pluginOptions: {
     *         items: 3,
     *         nav: false
     *     }
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="slider" data-slider-owl hidden>
     *     <div>Элемент слайдера 1</div>
     *     <div>Элемент слайдера 2</div>
     * </div>
     *
     * <!--data-slider-owl - селектор по умолчанию-->
     *
     * @example <caption>Отложенная инициализация</caption>
     * const sliderInstance = new app.SliderOwl(document.querySelector('.slider-element'), {
     *     init: false
     * });
     * sliderInstance.init();
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-slider-owl-options='{"minItems": 3}'></div>
     */
    const AppSliderOwl = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const $el = jQuery(el);

            const sliderOwlOptionsData = el.dataset.sliderOwlOptions;
            if (sliderOwlOptionsData) {
                const dataOptions = util.stringToJSON(sliderOwlOptionsData);
                if (!dataOptions) util.error('incorrect data-slider-owl-options format');
                util.extend(this.options, dataOptions);
            }

            util.defaultsDeep(this.options, defaultOptions);

            if (
                !this.options.pluginOptions ||
                (util.isObject(this.options.pluginOptions) && (Object.keys(this.options.pluginOptions).length === 0))
            ) {
                this.options.pluginOptions = util.extend({}, defaultPluginOptions);
            }

            /**
             * @member {Object} AppSliderOwl#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            const items = [...el.children];

            /**
             * @member {Array.<HTMLElement>} AppSliderOwl#items
             * @memberof layout
             * @desc Коллекция элементов слайдера.
             * @readonly
             */
            Object.defineProperty(this, 'items', {
                enumerable: true,
                value: items
            });

            let pluginData;

            /**
             * @member {Array.<HTMLElement>} AppSliderOwl#pluginData
             * @memberof layout
             * @desc Экземпляр плагина слайдера [Owl Carousel]{@link libs.owl-carousel}.
             * @readonly
             */
            Object.defineProperty(this, 'pluginData', {
                enumerable: true,
                get: () => pluginData
            });

            //Методы
            const controlsRecount = () => {
                util.defer(() => {
                    const itemsCount = items.length;
                    const activeItemsCount =
                        Math.floor(el.querySelector(itemsWrapperSelector).offsetWidth / el.querySelector(itemSelector).offsetWidth); //TODO:plus margin
                    const pluginNav = el.querySelector(navSelector);
                    const addClass = (itemsCount === activeItemsCount);

                    pluginNav.classList[addClass ? 'add' : 'remove'](disabledClass); //TODO:check options.nav
                });
            };

            /**
             * @function AppSliderOwl#controlsRecount
             * @memberof components
             * @desc Изменяет отображение навигационных стрелок слайдера в зависимости от показываемого количества слайдов. Стрелки скрываются, если все слайды умещаются на экране.
             * @returns {undefined}
             * @readonly
             * @example
             * sliderInstance.controlsRecount();
             */
            Object.defineProperty(this, 'controlsRecount', {
                enumerable: true,
                value: controlsRecount
            });

            const heightRecount = () => {
                util.defer(() => {
                    const wrapperEl = el.querySelector(itemsWrapperSelector);
                    const oldHeight = wrapperEl.offsetHeight;
                    let maxHeight = 0;

                    wrapperEl.classList.add(equalHeightClass);
                    wrapperEl.style.height = 'auto';
                    el.querySelectorAll(`${itemSelector}.${itemActiveClass}`).forEach(element => {
                        const newHeight = element.offsetHeight;
                        if (newHeight > maxHeight) {
                            maxHeight = newHeight;
                        }
                    });

                    wrapperEl.style.height = `${oldHeight}px`;
                    wrapperEl.style.height = `${maxHeight}px`;
                });
            };

            /**
             * @function AppSliderOwl#heightRecount
             * @memberof components
             * @desc Пересчитывает высоты слайдера согласно показываемым на экране слайдам.
             * @returns {undefined}
             * @readonly
             * @example
             * sliderInstance.heightRecount();
             */
            Object.defineProperty(this, 'heightRecount', {
                enumerable: true,
                value: heightRecount
            });

            const minItemsCheck = (minItems = params.minItems) => {
                return el.children.length >= minItems;
            };

            /**
             * @function AppSliderOwl#minItemsCheck
             * @memberof components
             * @desc Сравнивает количество слайдов в слайдере с передаваемым значением.
             * @param {number} [minItems=<значение из опций экземпляра>] - Минимальное количество слайдов для инициализации слайдера.
             * @returns {boolean} Больше или равно количеству слайдов в слайдере передаваемому значению или меньше.
             * @readonly
             * @example
             * sliderInstance.minItemsCheck(3);
             */
            Object.defineProperty(this, 'minItemsCheck', {
                enumerable: true,
                value: minItemsCheck
            });

            const addNavArrowIcon = (iconHtml = AppSvg.inline(arrowIconConfig)) => {
                el.querySelectorAll(`.${prevClass}, .${nextClass}`).forEach(arrow => {
                    arrow.innerHTML = `<span class="icon">${iconHtml}</span>`;
                });
            };

            /**
             * @function AppSliderOwl#addNavArrowIcon
             * @memberof components
             * @desc Добавляет иконки стрелок элементам навигации слайдера.
             * @param {string} [iconHtml] - HTML-строка с элементами иконок. По умолчанию, используется спрайт arrow.
             * @returns {undefined}
             * @readonly
             * @example
             * const iconConfig = {
             *     id: 'arrow',
             *     width: 11,
             *     height: 9
             * };
             * const iconHtml = app.Svg.inline(iconConfig);
             * sliderInstance.addNavArrowIcon(iconHtml);
             */
            Object.defineProperty(this, 'addNavArrowIcon', {
                enumerable: true,
                value: addNavArrowIcon
            });

            const addNavClasses = () => {
                if (!el.querySelector(`.${prevClass}`)) return;

                const prevDisabled = el.querySelector(`.${prevClass}`).classList.contains(disabledClass);
                const nextDisabled = el.querySelector(`.${nextClass}`).classList.contains(disabledClass);
                el.classList[prevDisabled ? 'add' : 'remove'](sliderPrevDisabled);
                el.classList[nextDisabled ? 'add' : 'remove'](sliderNextDisabled);
            };

            /**
             * @function AppSliderOwl#addNavClasses
             * @memberof components
             * @desc Добавляет классы элементу слайдера согласно активным кнопкам навигации.
             * @returns {undefined}
             * @readonly
             * @example
             * sliderInstance.addNavClasses();
             */
            Object.defineProperty(this, 'addNavClasses', {
                enumerable: true,
                value: addNavClasses
            });

            const dotsBetweenNav = () => {
                //TODO:check options.nav
                const dotsEl = el.querySelector(dotsSelector);
                const nextEl = el.querySelector(`.${nextClass}`);
                nextEl.before(dotsEl);
            };

            /**
             * @function AppSliderOwl#dotsBetweenNav
             * @memberof components
             * @desc Помещает пагинацию слайдера между стрелками слайдера.
             * @returns {undefined}
             * @readonly
             * @example
             * sliderInstance.dotsBetweenNav();
             */
            Object.defineProperty(this, 'dotsBetweenNav', {
                enumerable: true,
                value: dotsBetweenNav
            });

            let sliderInitialized;

            const init = () => {
                if (sliderInitialized) return sliderInitialized;

                sliderInitialized = minItemsCheck();
                el.removeAttribute('hidden');

                if (sliderInitialized) {
                    el.classList.add(sliderAddClass, initializingClass);
                    this.on('initialized', () => {
                        el.classList.remove(initializingClass);
                    });
                    $el.owlCarousel(params.pluginOptions);
                }

                return sliderInitialized;
            };

            /**
             * @function AppSliderOwl#init
             * @memberof components
             * @desc Инициализирует плагин слайдера.
             * @returns {boolean} Была ли начата инициализация слайдера.
             * @readonly
             * @example
             * const sliderInstance = new app.Slider(document.querySelector('.slider-element'), {
             *     init: false
             * });
             * //Настройка слайдера перед инициализацией
             * sliderInstance.init();
             * console.log(sliderInstance.init()); //Вернёт true и не будет инициализироваться повторно
             */
            Object.defineProperty(this, 'init', {
                enumerable: true,
                value: init
            });

            //Привязка событий экземпляра к событиям плагина
            $el
                .on('initialized.owl.carousel', event => {
                    util.defer(() => {
                        this.emit('initialized', event);
                    });
                })
                .on('resized.owl.carousel', event => {
                    this.emit('resized', event);
                })
                .on('refreshed.owl.carousel', event => {
                    this.emit('refreshed', event);
                })
                .on('dragged.owl.carousel', event => {
                    this.emit('dragged', event);
                })
                .on('translate.owl.carousel', event => {
                    this.emit('translate', event);
                })
                .on('translated.owl.carousel', event => {
                    this.emit('translated', event);
                })
                .on('changed.owl.carousel', event => {
                    this.emit('changed', event);
                });

            this.on({
                /**
                 * @event AppSliderOwl#goto
                 * @memberof components
                 * @desc Переключает активный слайд по индексу слайда.
                 * @param {Object} [options={}] - Опции переключения слайдов.
                 * @param {number} [options.index=0] - На какой слайд нужно переключить.
                 * @param {number} [options.duration=<значение из опции gotoDuration экземпляра>] - Длительность переключения перехода.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('goto', () => {
                 *     console.log('Активный слайд переключается');
                 * });
                 * sliderInstance.goto(3);
                 */
                goto(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    options.index = options.index || 0;
                    options.duration = (typeof options.duration === 'undefined') ? params.gotoDuration : options.duration;

                    $el
                        .trigger('to.owl.carousel', [options.index, options.duration])
                        .trigger('goto.app.slider', [options.index]);
                },

                /**
                 * @event AppSliderOwl#prev
                 * @memberof components
                 * @desc Переключает слайдер на предыдущий набор слайдов.
                 * @param {Object} [options={}] - Опции переключения слайдов.
                 * @param {number} [options.duration=<значение из опции gotoDuration экземпляра>] - Длительность переключения перехода.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('prev', () => {
                 *     console.log('Слайдер переключается на предыдущий набор слайдов');
                 * });
                 * sliderInstance.prev();
                 */
                prev(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    options.duration = options.duration || params.gotoDuration;

                    $el
                        .trigger('prev.owl.carousel', [options.duration])
                        .trigger('prev.app.slider');
                },

                /**
                 * @event AppSliderOwl#next
                 * @memberof components
                 * @desc Переключает слайдер на следующий набор слайдов.
                 * @param {Object} [options={}] - Опции переключения слайдов.
                 * @param {number} [options.duration=<значение из опции gotoDuration экземпляра>] - Длительность переключения перехода.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('next', () => {
                 *     console.log('Слайдер переключается на следующий набор слайдов');
                 * });
                 * sliderInstance.next();
                 */
                next(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    options.duration = options.duration || params.gotoDuration;

                    $el
                        .trigger('next.owl.carousel', [options.duration])
                        .trigger('next.app.slider');
                }
            });

            //TODO:reverse call events
            this.onSubscribe([
                /**
                 * @event AppSliderOwl#initialized
                 * @memberof components
                 * @desc Вызывается при вызове события 'initialized' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('initialized', () => {
                 *     console.log('Плагин слайдера проинициализировался');
                 * });
                 */
                'initialized',

                /**
                 * @event AppSliderOwl#resized
                 * @memberof components
                 * @desc Вызывается при вызове события 'resized' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('resized', () => {
                 *     console.log('Изменились размеры слайдера');
                 * });
                 */
                'resized',

                /**
                 * @event AppSliderOwl#refreshed
                 * @memberof components
                 * @desc Вызывается при вызове события 'refreshed' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('refreshed', () => {
                 *     console.log('Изменились опции или элементы слайдера');
                 * });
                 */
                'refreshed',

                /**
                 * @event AppSliderOwl#dragged
                 * @memberof components
                 * @desc Вызывается при вызове события 'dragged' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('dragged', () => {
                 *     console.log('Элементы слайдера были перетасканы пользователем');
                 * });
                 */
                'dragged',

                /**
                 * @event AppSliderOwl#translate
                 * @memberof components
                 * @desc Вызывается при вызове события 'translate' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('translate', () => {
                 *     console.log('Анимация переключения слайдов начинается');
                 * });
                 */
                'translate',

                /**
                 * @event AppSliderOwl#translated
                 * @memberof components
                 * @desc Вызывается при вызове события 'translated' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('translated', () => {
                 *     console.log('Анимация переключения слайдов закончилась');
                 * });
                 */
                'translated',

                /**
                 * @event AppSliderOwl#changed
                 * @memberof components
                 * @desc Вызывается при вызове события 'changed' плагина [Owl Carousel]{@link libs.owl-carousel}.
                 * @param {Object} [event] - Событие плагина.
                 * @returns {undefined}
                 * @example
                 * sliderInstance.on('changed', () => {
                 *     console.log('Изменено состояние слайдера');
                 * });
                 */
                'changed'
            ]);

            //Вызов методов слайдера после инициализации
            this.on({
                initialized() {
                    if (params.controlsRecount) controlsRecount();
                    if (params.heightRecount) heightRecount();
                    if (params.addNavArrowIcon) addNavArrowIcon();
                    if (params.dotsBetweenNav) dotsBetweenNav();

                    pluginData = $el.data('owl.carousel');

                    util.defer(() => $el.trigger('refresh.owl.carousel'));

                    el.classList.add(initializedClass);
                },
                changed() {
                    if (!items || (items.length === 0)) return;

                    if (params.controlsRecount) controlsRecount();
                    if (params.heightRecount) heightRecount();
                },
                translated() {
                    if (params.heightRecount) heightRecount();
                }
            });

            if (params.init) init();
        }

        /**
         * @function AppSliderOwl.navClass
         * @memberof components
         * @desc Возвращает коллекцию с классами стрелок слайдера.
         * @param {string} [classModifier] - Модификатор класса, который будет складываться с классами стрелок.
         * @returns {Array.<string>} Возвращает коллекцию с классами стрелок слайдера.
         * @example
         * console.log(app.Slider.navClass('round')); //Выведет ['owl-prev owl-prev-round', 'owl-next owl-prev-next']
         */
        static navClass(classModifier) {
            const prevWithModifier = classModifier ? `${prevClass}-${classModifier}` : '';
            const nextWithModifier = classModifier ? `${nextClass}-${classModifier}` : '';
            return [
                prevClass + prevWithModifier,
                nextClass + nextWithModifier
            ];
        }
    });

    addModule(moduleName, AppSliderOwl);

    //Инициализирует элементы по data-атрибуту
    document.querySelectorAll('[data-slider-owl]').forEach(el => new AppSliderOwl(el));

    return AppSliderOwl;
};

//TODO:arrow change method

export default sliderOwlInit;
export {sliderOwlInit};