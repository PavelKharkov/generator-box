import 'Base/scripts/test';

import {Module, immutable} from '..';

describe('immutable', () => {
    test('должен замораживать добавленный модуль', () => {
        const testModule = immutable(new Module(
            function() {
                this.test = true; /* eslint-disable-line no-invalid-this */
            },

            'testModule'
        ));

        const moduleAdd = (() => {
            testModule.test2 = true;
        });
        const moduleEdit = (() => {
            testModule.test = false;
        });
        const moduleRemove = (() => {
            delete testModule.test;
        });

        expect(testModule.test2).toBeUndefined();
        expect(moduleAdd).toThrow('Cannot add property test2, object is not extensible');

        expect(testModule.test).toBe(true);
        expect(moduleEdit).toThrow('Cannot assign to read only property \'test\' of object \'#<Module>\'');

        expect(moduleRemove).toThrow('Cannot delete property \'test\' of #<Module>');
    });
});

//TODO:other tests