/* eslint-disable max-classes-per-file */

import util from 'Layout/main';

/**
 * @typedef {(Object|Function)} app.Module.instanceOptions
 * @desc Опции экземпляра.
 * @returns {Object} Объект с опциями. Может также быть функцией, возвращающей объект с опциями.
 */

//Свойство модулей для хранения названия экземпляра в объектах modules
const nameProp = '_name';

//Свойство модулей для хранения функций-слушателей
const listeners = '_listeners';

/**
 * @function isFunction
 * @desc Проверяет, что передаваемое значение является функцией.
 * @param {*} value - Проверяемое значение.
 * @returns {boolean} Является ли значение функцией.
 * @ignore
 */
const isFunction = value => {
    return (typeof value === 'function');
};

/**
 * @function isObject
 * @desc Проверяет, что передаваемое значение является обычным объектом.
 * @param {*} value - Проверяемое значение.
 * @returns {boolean} Является ли значение обычным объектом.
 * @ignore
 */
const isObject = value => {
    return (String(value) === '[object Object]');
};

/**
 * @function initEvent
 * @desc Внутренняя функция для регистрации события.
 * @param {string} eventName - Название регистрируемого события.
 * @param {Object} [options={}] - Опции инициализации события.
 * @param {boolean} [options.subscribeOnly] - Указывает, что событие можно вызывать только через метод emit.
 * @param {(Function|*)} [eventProps=function() {}] - Функция, вызываемая данным событием. Остальные параметры — это данные, передающиеся регистриуемому событию.
 * @this Module
 * @returns {undefined}
 * @ignore
 */
const initEvent = function(eventName, options, ...eventProps) {
    let eventData = [];
    let eventHandler;
    if (Array.isArray(eventProps)) {
        eventData = eventProps.slice(1);
        [eventHandler] = eventProps;
    }
    if (!isFunction(eventHandler)) {
        eventHandler = () => {
            //пустая функция
        };
    }

    const listenersEvent = this[listeners][eventName];
    const instanceMethod = this[eventName];
    const subscribeRegistered = listenersEvent && !instanceMethod; //Зарегистрировано ли событие уже как доступное только по подписке

    Object.defineProperty(this[listeners], eventName, {
        configurable: true,
        enumerable: true,
        value: listenersEvent || []
    });

    this[listeners][eventName][options.prepend ? 'unshift' : 'push'](Object.freeze([
        eventHandler,
        options.once,
        ...eventData
    ]));

    if (!subscribeRegistered && !options.subscribeOnly && !instanceMethod) {
        Object.defineProperty(this, eventName, {
            enumerable: true,
            value: (...emitParameters) => this.emit(eventName, ...emitParameters)
        });
    }

    if (!__IS_DIST__ || __IS_DEBUG__) {
        if (window.debug) {
            window.log(`event '${eventName}' initialized on module '${this[nameProp]}'`, 'warning', this, options, ...eventData);
        }
    }
};

/**
 * @function handleOn
 * @desc Вспомогательная функция для регистрации событий.
 * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
 * @param {Object} [options={}] - Опции инициализации события.
 * @param {boolean} [options.subscribeOnly] - Указывает, что события можно вызывать только через метод emit.
 * @param {boolean} [options.once] - Указывает, что события вызываются только один раз.
 * @param {boolean} [options.prepend] - Указывает, что события нужно помещать в начало коллекции зарегистрированных событий.
 * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистрируемому событию (если первый параметр это строка). Параметры, указываемые при регистрации события, будут передаваться соответствующей функции несмотря на параметры, передаваемые при вызове данного события.
 * @this Module
 * @throws Выводит ошибку если объект с событиями - falsy-значение.
 * @returns {undefined}
 * @ignore
 */
const handleOn = function(eventsObject, options = {}, ...eventHandler) {
    if (!eventsObject) throw new Error('eventsObject must not be falsy');
    if (isObject(eventsObject)) {
        Object.keys(eventsObject).forEach(eventProps => {
            const propsArray = [eventsObject[eventProps]];
            initEvent.call(this, eventProps, options, ...propsArray, ...eventHandler);
        });
    } else if (Array.isArray(eventsObject)) {
        eventsObject.forEach(eventProps => {
            initEvent.call(this, eventProps, options);
        });
    } else {
        initEvent.call(this, eventsObject, options, ...eventHandler);
    }
};

/**
 * @class Module
 * @memberof app
 * @desc Конструктор модуля.
 * @param {(*|app.Module.instanceOptions)} [element=document] - Элемент либо опции экземпляра.
 * @param {app.Module.instanceOptions} [options={}] - Опции экземпляра или функция, возвращающая объект опций.
 * @param {string} appname - Название модуля в объектах modules. Можно указать вторым параметром вместо options.
 * @example
 * const newModule = new app.Module(
 *     document.body,
 *     function() {
 *         this.prop = 'property'; //добавление свойства экземпляру
 *         console.log(this.el); //document
 *         console.log(this._listeners); //в данный объект добавляются все события экземпляра
 *         console.log(this._name); //newModule
 *         console.log(this.el.modules); //список модулей, проинициализированных у данного элемента (добавляются только расширенные классы)
 *         return {option: 'some option'}; //функции следует возвращать объект с опциями экземпляра, они добавятся в свойство options
 *     },
 *     'newModule'
 *     //обязательно нужно указывать название модуля
 *     //(т.к. минификатор меняет названия переменных и нельзя получить оригинальное название модуля)
 * );
 *
 * console.log(newModule.options.option); //'some option'
 */
class Module {
    constructor(element, options, appname) {
        let optionsObject = options;
        let currentAppname = appname;
        let el;

        if (((typeof optionsObject === 'undefined') || (typeof optionsObject === 'string')) &&
            ((typeof element === 'undefined') || isObject(element) || isFunction(element))) {
            el = document;
            optionsObject = element;
            if (typeof currentAppname === 'undefined') {
                currentAppname = options;
            }
        } else {
            el = element;
            if (typeof optionsObject === 'string') {
                currentAppname = options;
                optionsObject = {};
            }
        }

        Object.defineProperty(this, 'el', {
            enumerable: true,
            value: el
        });

        if ((typeof currentAppname !== 'string') || !currentAppname) {
            throw new TypeError('parameter \'appname\' must be a non-empty string');
        }

        if ((el instanceof HTMLElement) || (el === document)) {
            el.modules = isObject(el.modules) ? el.modules : {};
            if (el.modules[currentAppname]) {
                const errorObject = {
                    message: `module '${currentAppname}' already initialized on this element`,
                    element: el
                };
                throw errorObject;
            } else {
                el.modules[currentAppname] = this;
            }
        }

        Object.defineProperty(this, nameProp, {
            value: currentAppname
        });
        Object.defineProperty(this, listeners, {
            value: {}
        });

        let optionsResult;
        if (isObject(optionsObject)) {
            optionsResult = optionsObject;
        } else if (isFunction(optionsObject)) {
            optionsResult = optionsObject.call(this);
            optionsResult = isObject(optionsResult) ? optionsResult : {};
        } else if (typeof optionsObject === 'undefined') {
            optionsResult = {};
        } else {
            throw new TypeError('last argument should be plain object or function that returns object');
        }

        Object.defineProperty(this, 'options', {
            enumerable: true,
            value: optionsResult
        });
    }

    /**
     * @function app.Module#on
     * @desc Регистрирует событие.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.on('viewData', arg => {
     *     console.log(arg); //'data'
     * }, 'data');
     * newModule.emit('viewData', 'data2');
     * //'data' - Параметры, указываемые при регистрации события будут передаваться
     * //соответствующей функции не смотря на параметры, передаваемые при вызове данного события
     *
     * @example
     * newModule.on(['show', 'hide']); //позволяет подписаться и вызывать данные события
     *
     * @example
     * newModule.on({
     *     show() {
     *         console.log('событие "show" вызвано');
     *     },
     *
     *     hide() {
     *         console.log('событие "hide" вызвано');
     *     }
     * });
     */
    on(eventsObject, ...eventHandler) {
        handleOn.call(this, eventsObject, {}, ...eventHandler);
    }

    /**
     * @function app.Module#onSubscribe
     * @desc Регистрирует событие, вызываемое только через метод emit.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.onSubscribe('shown', function() {});
     * newModule.shown(); //выведет ошибку, что данного метода не существует
     * newModule.emit('shown'); //в крайнем случае можно вызвать данное событие через emit
     * newModule.on('shown', function() {}); //после инициализации, можно подписываться на данное событие через on
     *
     * @example
     * newModule.onSubscribe(['shown', 'hidden']); //Как и в случае с методом on, можно регистрировать события через коллекцию
     *
     * @example
     * //Ещё один вариант регистрации событий
     * newModule.onSubscribe({
     *     show: true,
     *     hidden: true
     * });
     */
    onSubscribe(eventsObject, ...eventHandler) {
        handleOn.call(this, eventsObject, {subscribeOnly: true}, ...eventHandler);
    }

    /**
     * @function app.Module#once
     * @desc Регистрирует событие, вызываемое только один раз.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.once('show', () => {
     *     console.log('Вызвано событие show');
     * });
     * newModule.show(); //вызовет событие
     * console.log(newModule.show); //данного события больше не существует
     *
     * @example
     * newModule.once(['shown', 'hidden']); //Как и в случае с методом on, можно регистрировать события через коллекцию
     *
     * @example
     * //Ещё один вариант регистрации событий
     * newModule.once({
     *     show: true,
     *     hidden: true
     * });
     */
    once(eventsObject, ...eventHandler) {
        handleOn.call(this, eventsObject, {once: true}, ...eventHandler);
    }

    /**
     * @function app.Module#onceSubscribe
     * @desc Регистрирует событие, вызываемое только через метод emit и только один раз.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.onceSubscribe('show', () => {
     *     console.log('Вызвано событие show');
     * });
     * newModule.emit('show'); //вызовет событие
     * console.log(newModule.show); //данного события больше не существует
     *
     * @example
     * newModule.onceSubscribe(['shown', 'hidden']); //Как и в случае с методом on, можно регистрировать события через коллекцию
     *
     * @example
     * //Ещё один вариант регистрации событий
     * newModule.onceSubscribe({
     *     show: true,
     *     hidden: true
     * });
     */
    onceSubscribe(eventsObject, ...eventHandler) {
        const initConfig = {
            once: true,
            subscribeOnly: true
        };
        handleOn.call(this, eventsObject, initConfig, ...eventHandler);
    }

    /**
     * @function app.Module#prepend
     * @desc Регистрирует событие и помещает его в начало коллекции зарегистрированных функций.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.on('show', () => {
     *     console.log(1);
     * });
     * newModule.prepend('show', () => {
     *     console.log(2);
     * });
     * newModule.show(); //вначале выведет 2, потом 1
     *
     * @example
     * newModule.prepend(['shown', 'hidden']); //Как и в случае с методом on, можно регистрировать события через коллекцию
     *
     * @example
     * //Ещё один вариант регистрации событий
     * newModule.prepend({
     *     show: true,
     *     hidden: true
     * });
     */
    prepend(eventsObject, ...eventHandler) {
        handleOn.call(this, eventsObject, {prepend: true}, ...eventHandler);
    }

    /**
     * @function app.Module#prependOnce
     * @desc Регистрирует событие, вызываемое только один раз и помещает его в начало коллекции зарегистрированных функций.
     * @param {(Object|string|Array.<string>)} eventsObject - Ассоциативный объект с событиями, либо строка - название регистрируемого события, либо коллекция с событиями-пустышками.
     * @param {(Function|*)} [eventHandler=function() {}] - Если предыдущий параметр является строкой, то это функция, вызываемая данным событием. Если предыдущий параметр это объект с событиями, то это агрументы, прикрепляющиеся к данному событию. Остальные параметры это данные, передающиеся регистриуемому событию (если первый агрумент это строка). Параметры, указываемые при регистрации события будут передаваться соответствующей функции не смотря на параметры, передаваемые при вызове данного события.
     * @returns {undefined}
     * @example
     * newModule.once('show', () => {
     *     console.log(1);
     * });
     * newModule.prependOnce('show', () => {
     *     console.log(2);
     * });
     * newModule.show(); //вначале выведет 2, потом 1
     * console.log(newModule.show); //данного события больше не существует
     *
     * @example
     * newModule.prependOnce(['shown', 'hidden']); //Как и в случае с методом on, можно регистрировать события через коллекцию
     *
     * @example
     * //Ещё один вариант регистрации событий
     * newModule.prependOnce({
     *     show: true,
     *     hidden: true
     * });
     */
    prependOnce(eventsObject, ...eventHandler) {
        const initConfig = {
            once: true,
            prepend: true
        };
        handleOn.call(this, eventsObject, initConfig, ...eventHandler);
    }

    /**
     * @function app.Module#off
     * @desc Удаляет событие.
     * @param {string} eventName - Название удаляемого события.
     * @param {Function} [eventParameter] - Ищет событие, вызывающее данную функцию и удаляет только его.
     * @throws Выводит ошибку, если название события - falsy-значение.
     * @returns {undefined}
     * @example
     * newModule.on('show', function() {});
     * newModule.off('show'); //удаляет все зарегистрированные события
     *
     * @example
     * const func1 = () => console.log('1');
     * const func2 = () => console.log('2');
     * newModule.on('show', func1);
     * newModule.on('show', func2);
     * mewModule.show(); //'1' '2'
     * newModule.off('show', func2); //удаляет определённый триггер события
     * newModule.show(); //'1'
     */
    off(eventName, eventParameter) {
        if (!eventName) throw new Error('eventName must not be falsy');
        if (eventParameter) {
            const newEventArray = this[listeners][eventName].filter(eventCallback => {
                return String(eventParameter) !== String(eventCallback[0]);
            });
            this[listeners][eventName].length = 0;
            this[listeners][eventName].push(...newEventArray);
        } else this[listeners][eventName].length = 0;
    }

    /**
     * @function app.Module#emit
     * @desc Вызывает событие.
     * @param {string} eventName - Название вызываемого события.
     * @param {...*} [emitParameters] - Параметры, передающиеся событию.
     * @throws Выводит ошибку, если название события - falsy-значение или вызывается неинициализированное событие.
     * @returns {*} Возвращает первое не-undefined значение, возвращаемое функциями события.
     * @example
     * newModule.on('test', () => {
     *     console.log('1');
     *     return '1';
     * });
     * newModule.on('test', () => {
     *     console.log('2');
     *     return '2';
     * });
     * const result = newModule.emit('test'); //'1' '2'
     * console.log(result); //'1'
     *
     * //Данный метод аналогичен простому вызову метода объекта, т.е.:
     * newModule.test();
     */
    emit(eventName, ...emitParameters) {
        if (!eventName) throw new Error('eventName must not be falsy');
        if (!this[listeners][eventName]) throw new Error(`unknown event ${eventName}`);
        const emitResult = this[listeners][eventName].reduce((firstReturn, eventCallback, currentIndex) => {
            const functionResult = eventCallback[0].call(this, ...((eventCallback.length > 2) ? eventCallback.slice(2) : emitParameters));
            if (eventCallback[1]) this.off(eventName, eventCallback[0]); //Удаляет этого слушателя после вызова события
            return ((currentIndex === 0) || (typeof firstReturn === 'undefined')) ? functionResult : firstReturn;
        }, null);

        if (!__IS_DIST__ || __IS_DEBUG__) {
            if (window.debug) window.log(`event '${eventName}' emitted on module '${this[nameProp]}'`, 'info', this, ...emitParameters);
        }

        return emitResult;
    }

    /**
     * @function app.Module#unlink
     * @desc Отсоединяет экземпляр класса от инициализируемого элемента.
     * @returns {undefined}
     * @example
     * const div = document.querySelector('div');
     * const newModule = new app.Module(div, 'newModule');
     * newModule.unlink();
     * console.log(div.modules); //выведет пустой объект
     * console.log(newModule); //экземпляр класса всё равно доступен через переменную
     */
    unlink() {
        if (this.el instanceof HTMLElement) {
            delete this.el.modules[this[nameProp]];
        }
    }

    /**
     * @function app.Module.checkHTMLElement
     * @desc Проверяет переданный параметр на экзмепляр HTMLElement.
     * @param {*} element - Проверяемая переменная.
     * @throws Выводит ошибку, если параметр не является экзмепляром HTMLElement.
     * @returns {undefined}
     * @example
     * const newModule = new app.Module(document, () => {
     *     Module.checkHTMLElement(this.el); //Сообщения об ошибке не будет
     * }, 'newModule');
     *
     * const newModule2 = new app.Module('string', () => {
     *     Module.checkHTMLElement(this.el); //TypeError: parameter 'element' must be a HTMLElement
     * }, 'newModule');
     */
    static checkHTMLElement(element) {
        if (element instanceof HTMLElement) return;

        throw new TypeError('parameter \'element\' must be a HTMLElement');
    }

    //TODO:add example
    /**
     * @function app.Module.setParams
     * @desc Добавляет ключ с параметрами в экземпляр класса.
     * @param {Module} instance - Экземпляр класса.
     * @param {Object} [options] - Опции экземпляра.
     * @throws Выводит ошибку, если параметр не является экзмепляром Module.
     * @returns {Object} Объект с параметрами экземпляра.
     */
    static setParams(instance, options) {
        if (!(instance instanceof Module)) {
            throw new TypeError('parameter \'instance\' must be instance of Module');
        }

        const params = util.extend((instance.params || {}), (options || instance.options));

        Object.defineProperty(instance, 'params', {
            enumerable: true,
            value: params
        });

        return params;
    }
}

/**
 * @function app.Module#events
 * @desc Возвращает все прослушиваемые события экземпляра.
 * @returns {Array.<string>} Коллекция с названиями событий экземпляра.
 * @example
 * console.log(newModule.events()); //Выведет коллекцию со строками
 */
Module.prototype.events = function() {
    return Object.keys(this[listeners]).map(listenerName => listenerName);
};

/**
 * @function app.Module#listeners
 * @desc Возвращает все слушатели передаваемого события экземпляра с параметрами.
 * @param {string} event - Название события.
 * @returns {Array.<Array>} Коллекция с коллекциями вида [Функция-слушатель, ...параметры слушателя].
 * @example
 * console.log(newModule.listeners()); //Выведет коллекцию с коллекциями
 */
Module.prototype.listeners = function(event) {
    return this[listeners][event];
};

Object.freeze(Module);

/**
 * @function immutable
 * @desc Замораживает наследующий класс или экземпляр класса.
 * @param {Object} OriginalClass - Наследуемый класс или экземпляр класса.
 * @returns {Object} Замороженный класс.
 * @ignore
 */
const immutable = OriginalClass => {
    //Замораживание экземпляра модуля
    if (OriginalClass instanceof Module) {
        Object.freeze(OriginalClass);
        return OriginalClass;
    }

    //Замораживание класса модуля
    const ImmutableClass = class extends OriginalClass {
        constructor(...args) {
            super(...args);
            if (new.target === ImmutableClass) {
                Object.freeze(this);
            }
        }
    };

    Object.freeze(ImmutableClass);
    return ImmutableClass;
};

export default Module;
export {Module, immutable};