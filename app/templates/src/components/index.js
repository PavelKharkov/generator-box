import './ajax/autoload.js';
import './alert/autoload.js';
import './animation/autoload.js';
import './badge';
import './button';
import './card';
import './check';
import './close';
import './collapse/autoload.js';
import './color/autoload.js';
import './cookie';
import './counter/autoload.js';
import './date/autoload.js';
import './datepicker/autoload.js';
import './dropdown/autoload.js';
import './file/autoload.js';
import './float-label/autoload.js';
import './form/autoload.js';
import './gallery/autoload.js';
import './history';
import './iframe/autoload.js';
import './image/autoload.js';
import './image-diff/autoload.js';
import './input';
import './link';
import './list';
import './map/autoload.js';
import './map-google/autoload.js';
import './mask/autoload.js';
import './money';
import './nav';
import './pagination';
import './popover/autoload.js';
import './popup/autoload.js';
import './popup-gallery/autoload.js';
import './popup-magnific/autoload.js';
import './popup-zoom-gallery/autoload.js';
import './preloader/autoload.js';
import './progress';
import './range/autoload.js';
import './rating/autoload.js';
import './scroll/autoload.js';
import './select/autoload.js';
import './slideout/autoload.js';
import './slider/autoload.js';
import './slider-owl/autoload.js';
import './social/autoload.js';
import './svg';
import './table/autoload.js';
import './tabs/autoload.js';
import './toaster/autoload.js';
import './toggle/autoload.js';
import './touch/autoload.js';
import './url';
import './validation/autoload.js';
import './video/autoload.js';