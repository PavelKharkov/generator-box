import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'map';

let scriptPlaced = false;
let maps;

const apiUrl = '//api-maps.yandex.ru/2.1/';

const apikey = app.globalConfig.ymapsApikey;

//Классы
const mapsReadyClass = 'maps-ready';
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    init: true
};

/**
 * @class AppMap
 * @memberof components
 * @classdesc Модуль карт.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент для вывода карты.
 * @param {app.Module.instanceOptions} options - Опции экземпляра.
 * @param {(Array.<(number|string)>|string)} [options.placemarkCoords] - Коллекция координат центра карты или строка с двумя координатами, разделенными запятой.
 * @param {string} [options.placemarkContent] - Текст подсказки при наведении на точку карты.
 * @param {boolean} [options.init=true] - Создавать ли точку на карте при инициализации экземпляра карты.
 * @example
 * const mapInstance = new app.Map(document.querySelector('.map-element'));
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-map-options='{"init": false}'></div>
 */
const AppMap = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const mapOptionsData = el.dataset.mapOptions;
        if (mapOptionsData) {
            const dataOptions = util.stringToJSON(mapOptionsData);
            if (!dataOptions) util.error('incorrect data-map-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppMap#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let map;

        /**
         * @member {Object} AppMap#map
         * @memberof components
         * @desc Экземпляр карты скрипта для работы с картами.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'map', {
            enumerable: true,
            get: () => map
        });

        /**
         * @function readyFunction
         * @desc Функция, вызываемая после загрузки скрипта для работы с картами.
         * @returns {undefined}
         * @ignore
         */
        const readyFunction = () => {
            this.emit('init');
        };

        if (!scriptPlaced) {
            const keyParam = `apikey=${this.constructor.apikey}`;
            const langParam = `lang=${util.lang || __LANG__}_${util.region || __REGION__}`;
            util.addScript(
                `${apiUrl}?${keyParam}&${langParam}`,
                () => {
                    if (!window.ymaps) util.error('Yandex maps can\'t load');

                    maps = window.ymaps;

                    maps.ready(readyFunction);
                }
            );
            scriptPlaced = true;
        }

        this.onSubscribe({
            /**
             * @event AppMap#init
             * @memberof components
             * @desc Событие, вызываемое после инициализации скрипта для работы с картами.
             * @example
             * mapInstance.on('init', () => {
             *     console.log('Скрипт для работы с картами загружен');
             * });
             */
            init() {
                document.documentElement.classList.add(mapsReadyClass);

                //TODO:move map init there

                el.classList.add(initializedClass);
            }
        });

        if (maps) {
            util.defer(() => maps.ready(readyFunction));
        }

        if (params.init) {
            const placemarkCoords = params.placemarkCoords;
            if (typeof placemarkCoords !== 'undefined') {
                const coords = (typeof placemarkCoords === 'string') ? placemarkCoords.split(',') : placemarkCoords;
                if (Array.isArray(coords) && (coords.length === 2)) {
                    const placemarkContent = params.placemarkContent;

                    this.on('init', () => {
                        map = new maps.Map(el, {
                            center: coords,
                            zoom: 16
                        });

                        const placemark = new maps.Placemark(
                            coords,
                            {
                                hintContent: placemarkContent
                            },
                            {
                                preset: 'islands#redIcon' //TODO:option for placemark options & config
                            }
                        );

                        map.geoObjects.add(placemark);
                    });
                } else {
                    util.error('incorrect coords');
                }
            }
        }
    }

    /**
     * @function AppMap.maps
     * @memberof components
     * @desc Get-функция. Ссылка на переменную window.ymaps.
     * @async
     * @returns {Object}
     * @example
     * mapInstance.on('init', () => {
     *     const maps = app.Map.maps;
     *     const currentMap = new maps.Map(mapContainer, {
     *          center: [0,0],
     *          zoom: 15,
     *          controls: ['zoomControl']
     *      });
     * });
     */
    static get maps() {
        return maps;
    }

    /**
     * @function AppMap.apikey
     * @memberof components
     * @desc Get-функция. Возвращает api-ключ Яндекс карт. По умолчанию, берётся из app.globalConfig.ymapsApikey.
     * @async
     * @returns {string}
     */
    static get apikey() {
        return apikey; //TODO:setApikey func
    }
});

addModule(moduleName, AppMap);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-map]').forEach(el => new AppMap(el));

export default AppMap;
export {AppMap};