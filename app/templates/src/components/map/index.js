let importFunc;
let mapTrigger;

if (__IS_ENABLED_MODULE__) {
    let mapTriggered;

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const mapCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "contacts" */ './sync.js'))
            .then(modules => {
                chunks.contacts = true;
                mapTriggered = true;

                const AppMap = modules.default;
                resolve(AppMap);
            });
    };

    const initMap = () => {
        if (mapTriggered) return;

        emitInit('map');
    };

    mapTrigger = (mapItems, mapTriggers = ['click']) => {
        if (__IS_SYNC__) return;

        const items = (mapItems instanceof Node) ? [mapItems] : mapItems;
        if (items.length === 0) return;

        items.forEach(item => {
            mapTriggers.forEach(trigger => {
                item.addEventListener(trigger, initMap, {once: true});
            });
        });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.contacts) {
            mapCallback(resolve);
            return;
        }

        onInit('map', () => {
            mapCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('map');
        });

        const mapItems = document.querySelectorAll('[data-map]');
        mapTrigger(mapItems);
    });
}

export default importFunc;
export {mapTrigger};