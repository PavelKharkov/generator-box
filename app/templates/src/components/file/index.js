import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

import 'Components/input';

let fileTriggered;

const fileCallback = resolve => {
    const imports = [
        require('Components/alert').default,
        require('Components/svg').default,
        require('Components/toaster').default
    ];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "forms" */ './sync.js'))
            .then(file => {
                chunks.forms = true;
                fileTriggered = true;

                const AppFile = file.initFile(...modules);
                if (resolve) resolve(AppFile);
            });
    });

    emitInit('alert', 'svg', 'toaster');
};

const initFile = () => {
    if (fileTriggered) return;

    emitInit('file');
};

const fileTrigger = (fileItems, fileTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (fileItems instanceof Node) ? [fileItems] : fileItems;
    if (items.length === 0) return;

    items.forEach(item => {
        fileTriggers.forEach(trigger => {
            item.addEventListener(trigger, initFile, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        fileCallback(resolve);
        return;
    }

    onInit('file', () => {
        fileCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('file');
    });

    const fileItems = document.querySelectorAll('[data-file]');
    fileTrigger(fileItems);
});

export default importFunc;
export {fileTrigger};