import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import 'Components/form';

//Опции
const moduleName = 'file';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Селекторы и классы
const fileClass = moduleName;
const fileInputSelector = 'input[type="file"]';
const listSelector = `.${fileClass}-list`;
const labelClass = `${fileClass}-label`;
const imageClass = `${fileClass}-image`;
const labelSelector = `.${labelClass}`;
const initializedClass = `${moduleName}-initialized`;
const previewClass = '-preview';
const filenameClass = '-filename';
const dragClass = '-drag';
const isDragoverClass = 'is-dragover';
const maxClass = '-max';

const clearIconHtml = {
    id: 'close',
    width: 10,
    height: 10
};
const fileIconHtml = {
    id: 'document',
    width: 40,
    height: 48
};

const errorAlertOptions = {
    title: 'Ошибка',
    toast: true,
    dismissible: true,
    color: 'danger'
};

const kbValue = 1024; //1 Кб
const mbValue = kbValue ** 2; //1 Мб

//Языковые настройки
const langOptions = {
    selected: lang.selected,
    accept: lang.accept,
    duplicate: lang.duplicate,
    max: lang.max,
    size: lang.size
};

//Опции по умолчанию
const defaultOptions = {
    drag: true,
    max: 6,
    size: 10,
    listSelector,
    labelSelector,
    lang: langOptions
};

//TODO:multipleSize

/**
 * @function template
 * @desc Заменяет фразы в строке согласно объекту опций.
 * @param {string} string - Строка, в которой нужно сделать подстановку фраз.
 * @param {Object} options - Объект со значениями для подстановки. Ключами являются фразы, которые надо заменить.
 * @returns {string} Отформатированная строка.
 * @ignore
 */
const template = (string, options) => {
    let newString = string;
    Object.keys(options).forEach(option => {
        newString = newString.replace(new RegExp(`\\${option}`), options[option]);
    });
    return newString;
};

let itemTemplate;

const initFile = (AppAlert, AppSvg) => {
    const toasterEl = document.querySelector('.toaster.-top.-right');
    const toaster = toasterEl && toasterEl.modules.toaster;

    const imagePreviewTypes = new Set([
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/gif',
        'image/webp'
    ]);
    const videoPreviewTypes = new Set(['video/mp4']);

    /**
     * @function setFilePreview
     * @desc Настраивает миниатюру файла.
     * @param {HTMLElement} label - HTML-элемент лейбла, в который неоходимо поместить миниатюру файла.
     * @param {Object} file - Объект файла.
     * @ignore
     */
    const setFilePreview = (label, file) => {
        const fileType = file.type;

        //Изображения
        if (imagePreviewTypes.has(fileType)) {
            const fileReader = new FileReader();
            fileReader.addEventListener('load', () => {
                const imageEl = document.createElement('span');
                imageEl.classList.add(imageClass);
                imageEl.style.backgroundImage = `url(${fileReader.result})`;
                label.append(imageEl);
            });
            fileReader.readAsDataURL(file);
            return;
        }

        //Видео
        if (videoPreviewTypes.has(fileType)) {
            const fileReader = new FileReader();
            fileReader.addEventListener('load', () => {
                const buffer = fileReader.result;
                const videoBlob = new Blob([new Uint8Array(buffer)], {
                    type: file.type
                });

                const videoUrl = window.URL.createObjectURL(videoBlob);

                const videoEl = document.createElement('video');
                videoEl.classList.add(`${fileClass}-video`);
                videoEl.muted = true;
                videoEl.setAttribute('muted', '');
                videoEl.src = videoUrl;
                label.append(videoEl);
            });
            fileReader.readAsArrayBuffer(file);
            return;
        }

        const fileIcon = document.createElement('span');
        fileIcon.classList.add(`${fileClass}-icon`);

        //Форматы docx & doc
        if (
            (fileType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') ||
            (fileType === 'application/msword')
        ) {
            fileIcon.classList.add('-doc');
            //fileIcon.innerHTML = AppSvg.inline(fileIconHtml);
        }

        //Формат pdf
        if (fileType === 'application/pdf') {
            fileIcon.classList.add('-pdf');
        }

        //Формат xlsx & xls
        if (
            (fileType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') ||
            (fileType === 'application/vnd.ms-excel')
        ) {
            fileIcon.classList.add('-xls');
        }

        //Другие форматы
        if (fileIcon.children.length === 0) {
            fileIcon.innerHTML = AppSvg.inline(fileIconHtml);
        }
        label.append(fileIcon);
    };

    /**
     * @function cancelTemplate
     * @desc Шаблон кнопки отмены загрузки файла.
     * @param {string} [title] - Атрибут title элемента кнопки.
     * @returns {string} HTML-строка элемента.
     * @ignore
     */
    const cancelTemplate = (title = '') => {
        const cancelIcon = AppSvg.inline(clearIconHtml);
        return `<button type="button" class="${fileClass}-cancel" title="${title}">${cancelIcon}</button>`;
    };

    langOptions.cancel = cancelTemplate(lang.cancel);

    /**
     * @function itemTemplate
     * @desc Шаблон элемента нового файла.
     * @param {Object} [options={}] - Опции шаблона элемента нового файла.
     * @param {string} [options.title] - Атрибут title элемента файла. По умолчанию соответствует пути файла, указанного в опции label.
     * @param {string} [options.label] - Путь к выбранному файлу, является текстом элемента.
     * @param {boolean} [options.cancel] - Показывать ли кнопку отмены загрузки файла.
     * @returns {string} HTML-строка элемента.
     * @ignore
     */
    itemTemplate = (options = {}) => {
        if (!util.isObject(options)) {
            util.typeError(options, 'options', 'plain object');
        }

        const title = options.title || options.label;
        if (options.label) {
            return `<span title="${title}" class="${fileClass}-item">
                <span class="${fileClass}-name">${options.label}</span>
                ${options.cancel ? ` ${langOptions.cancel}` : ''}
            </span>`;
        }

        return '';
    };

    /**
     * @class AppFile
     * @memberof components
     * @requires components.alert
     * @requires components.input
     * @requires components.form
     * @requires components.svg
     * @requires components.toaster
     * @classdesc Модуль для работы с полями добавления файлов.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент контейнера поля для файла.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {boolean} [options.multiple] - Возможность множественного добавления файлов.
     * @param {boolean} [options.preview] - Показывать ли превью файла.
     * @param {boolean} [options.filename] - Показывать ли название файла с превью файла.
     * @param {boolean} [options.drag=true] - Добавлять ли возможность загрузки файлов с помощью перетаскивания.
     * @param {number} [options.max=6] - Максимальное количество загружаемых файлов.
     * @param {number} [options.size=10] - Максимальный размер загружаемого файла (в Кб). По умолчанию — 10 Мб.
     * @param {string} [options.accept] - Возможные расширения загружаемых файлов.
     * @param {(string|Array.<string>)} [options.name=`FILE${this.options.multiple ? '[]' : ''}`] - Название поля для файла или коллекция с названиями полей для файлов. По умолчанию берётся из атрибута name поля для файла.
     * @param {string} [options.listSelector='.file-list'] - Селектор для элемента, в котором будет отображаться путь к выбранному файлу.
     * @param {string} [options.labelSelector='.file-label'] - Селектор лейбла для поля для файла.
     * @example
     * const fileInstance = new app.File(document.querySelector('.file-input'), {
     *     accept: 'image/*,.pdf',
     *     size: 15 * 1024 * 1024 //15 Мб
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <label for="file">Файл</label>
     * <div class="file" data-file>
     *     <input class="file-input" type="file" id="file">
     *     <label class="file-list" for="file"></label>
     * </div>
     *
     * <!--
     * data-file - селектор по умолчанию
     * .file-list - селектор для блока загружаемых файлов
     * -->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div class="file" data-file-options='{"listSelector": ".custom-file-list"}'></div>
     */
    const AppFile = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const input = el.querySelector(fileInputSelector);

            const fileOptionsData = el.dataset.fileOptions;
            if (fileOptionsData) {
                const dataOptions = util.stringToJSON(fileOptionsData);
                if (!dataOptions) util.error('incorrect data-file-options format');
                util.extend(this.options, dataOptions);
            }

            if (input.accept) {
                this.options.accept = input.accept;
            } else if (this.options.accept) {
                input.accept = this.options.accept;
            }

            if (input.multiple) {
                this.options.multiple = true;
            } else if (this.options.multiple) {
                input.multiple = true;
            }

            if (!this.options.name) {
                this.options.name = input.name || `FILE${this.options.multiple ? '[]' : ''}`;
            }

            if (el.classList.contains(previewClass)) {
                this.options.preview = true;
                if (el.classList.contains(filenameClass)) {
                    this.options.filename = true;
                }
            } else if (this.options.preview) {
                el.classList.add(previewClass);
                if (this.options.filename) {
                    el.classList.add(filenameClass);
                }
            }

            if (el.classList.contains(dragClass)) {
                this.options.drag = true;
            } else if (this.options.drag) {
                el.classList.add(dragClass);
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppFile#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            /**
             * @member {HTMLElement} AppFile#input
             * @memberof components
             * @desc Элемент поля ввода файла.
             * @readonly
             */
            Object.defineProperty(this, 'input', {
                enumerable: true,
                value: input
            });

            if (!input.modules) {
                input.modules = {};
            }
            input.modules[moduleName] = this;

            const fileList = el.querySelector(params.listSelector);
            if (!fileList) return;

            /**
             * @member {HTMLElement} AppFile#fileList
             * @memberof components
             * @desc Элемент контейнера для элементов загружаемых файлов.
             * @readonly
             */
            Object.defineProperty(this, 'fileList', {
                enumerable: true,
                value: fileList
            });

            if (!fileList.modules) {
                fileList.modules = {};
            }
            fileList.modules[moduleName] = this;

            let form;

            /**
             * @member {Object} AppFile#form
             * @memberof components
             * @desc Экземпляр модуля родительской формы [AppForm]{@link components.AppForm} файла.
             * @readonly
             * @example
             * const fileInstance = new app.File(document.querySelector('.file-input'));
             *
             * const form = fileInstance.form;
             * console.log(form); //Выведет модуль формы
             * console.log(form.files); //Выведет коллекцию модулей файлов
             */
            Object.defineProperty(this, 'form', {
                enumerable: true,
                get: () => form
            });

            const fromElement = el.closest('form');
            if (fromElement) {
                form = fromElement.modules && fromElement.modules.form;

                if (form) form.files.push(this); //Сохраняет ссылку на себя в модуле родительской формы
            }

            let manualAdd = false;
            let manualCancel = false;
            let cancelFile;

            const files = [];

            /**
             * @member {Array.<Object>} AppFile#files
             * @memberof components
             * @desc Коллекция загруженных файлов.
             * @readonly
             */
            Object.defineProperty(this, 'files', {
                enumerable: true,
                value: files
            });

            //Если уже имеются загруженные файлы
            [...fileList.children].forEach(fileLabel => {
                if (fileLabel.classList.contains(labelClass)) return;

                files.push({
                    name: fileLabel.title,
                    preloaded: true
                });

                const closeButton = fileLabel.querySelector('button');
                if (!closeButton) return;

                closeButton.addEventListener('click', event => {
                    event.stopPropagation();
                    event.preventDefault();

                    if (params.multiple) {
                        const currentIndex = [...fileList.children].indexOf(fileLabel);
                        cancelFile(currentIndex);
                        return;
                    }

                    cancelFile();
                });
            });

            let multipleInput;

            /**
             * @member {HTMLElement} AppFile#multipleInput
             * @memberof components
             * @desc Элемент поля ввода для множественной загрузки файлов.
             * @readonly
             */
            Object.defineProperty(this, 'multipleInput', {
                enumerable: true,
                get: () => multipleInput
            });

            const label = el.querySelector(params.labelSelector);

            /**
             * @member {HTMLElement} AppFile#label
             * @memberof components
             * @desc Элемент лейбла.
             * @readonly
             */
            Object.defineProperty(this, 'label', {
                enumerable: true,
                get: () => label
            });

            let errorAlert;

            /**
             * @function checkSize
             * @desc Проверяет размер загружаемого файла. Если файл не проходит валидацию, то показывает ошибку с помощью события [append]{@link components.AppToaster#event:append} модуля [AppToaster]{@link components.AppToaster}.
             * @param {Object} file - Загружаемый файл.
             * @fires components.AppToaster#append
             * @returns {boolean} Возвращает true, если файл не превышает лимит размера.
             * @ignore
             */
            const checkSize = file => {
                if (!file) return true;

                const size = file.size / mbValue;
                if (size > params.size) {
                    const fixedSize = params.size.toFixed(1);
                    const shownSize = `${fixedSize.endsWith('0') ? Number.parseInt(fixedSize, 10) : fixedSize} ${lang.mb}`;
                    errorAlert = new AppAlert(AppAlert.template({
                        ...errorAlertOptions,
                        content: template(params.lang.size, {
                            $file: file.name,
                            $size: shownSize
                        })
                    }));
                    toaster.append(errorAlert);
                    return false;
                }
                return true;
            };

            /**
             * @function checkAccept
             * @desc Проверяет расширение загружаемого файла. Если файл не проходит валидацию, то показывает ошибку с помощью события [append]{@link components.AppToaster#event:append} модуля [AppToaster]{@link components.AppToaster}.
             * @param {Object} file - Загружаемый файл.
             * @fires components.AppToaster#append
             * @returns {boolean} Возвращает true, если файл имеет подходящее расширение.
             * @ignore
             */
            const checkAccept = file => {
                if (!file) return true;

                const mimeType = file.type;
                const accept = params.accept ? params.accept.split(',') : [];
                if ((accept.length > 0) && !accept.includes(mimeType)) {
                    const extension = `.${file.name.split('.').pop()}`;
                    if (!accept.includes(extension)) {
                        errorAlert = new AppAlert(AppAlert.template({
                            ...errorAlertOptions,
                            content: template(params.lang.accept, {
                                $ext: extension,
                                $accept: accept.join(', ')
                            })
                        }));
                        toaster.append(errorAlert);
                        return false;
                    }
                }
                return true;
            };

            let inputChangeFunc;

            if (params.multiple) {
                if (Array.isArray(params.name)) {
                    params.max = params.name.length;
                }

                /**
                 * @function checkMax
                 * @desc Проверяет количество загружаемых файлов. Если файл не проходит валидацию, то показывает ошибку с помощью события [append]{@link components.AppToaster#event:append} модуля [AppToaster]{@link components.AppToaster}.
                 * @fires components.AppToaster#append
                 * @returns {boolean} Возвращает true, если количество загруженных файлов не превысило лимит количества.
                 * @ignore
                 */
                const checkMax = () => {
                    const filesLength = files.length;
                    if (filesLength >= params.max) {
                        errorAlert = new AppAlert(AppAlert.template({
                            ...errorAlertOptions,
                            content: template(params.lang.max, {
                                $max: params.max
                            })
                        }));
                        toaster.append(errorAlert);
                        el.classList.add(maxClass);
                        return false;
                    }
                    return true;
                };

                /**
                 * @function checkDuplicate
                 * @desc Проверяет загружаемый файл на повтор. Если файл не проходит валидацию, то показывает ошибку с помощью события [append]{@link components.AppToaster#event:append} модуля [AppToaster]{@link components.AppToaster}.
                 * @param {Object} file - Загружаемый файл.
                 * @fires components.AppToaster#append
                 * @returns {boolean} Возвращает true, если такой файл ещё не загружен.
                 * @ignore
                 */
                const checkDuplicate = file => {
                    const fileName = file.name;
                    const size = file.size;
                    const lastModified = file.lastModified;

                    const isDuplicate = files.some(testFile => {
                        const testName = testFile.name;
                        if (fileName !== testName) return false;

                        const testSize = testFile.size;
                        if (size !== testSize) return false;

                        const testModified = testFile.lastModified;
                        return lastModified === testModified;
                    });

                    if (isDuplicate) {
                        errorAlert = new AppAlert(AppAlert.template({
                            ...errorAlertOptions,
                            content: params.lang.duplicate
                        }));
                        toaster.append(errorAlert);
                    }

                    return !isDuplicate;
                };

                const inputId = input.id;
                const labels = document.querySelectorAll(`label[for="${inputId}"]`);

                /**
                 * @function setInput
                 * @desc Добавляет новое поле ввода для файла для множественной загрузки файлов.
                 * @returns {undefined}
                 * @ignore
                 */
                const setInput = () => {
                    const inputCopy = input.cloneNode();
                    const index = util.uniqueId();
                    const newId = `${inputId}-${index}`;
                    inputCopy.id = newId;
                    inputCopy.modules = {
                        [moduleName]: this
                    };
                    el.append(inputCopy);
                    labels.forEach(labelItem => {
                        labelItem.htmlFor = newId;
                    });

                    if (multipleInput) multipleInput.remove();
                    multipleInput = inputCopy;
                    //TODO:add validation for multiple

                    //Перенести лейбл внутрь списка файлов, если уже имеются загруженные файлы
                    if (label && (fileList.children.length > 0)) {
                        fileList.append(label);
                    }

                    /**
                     * @function setFile
                     * @desc Добавляет файл в список загружаемых файлов.
                     * @param {Object} file - Объект файла.
                     * @returns {boolean} Возникла ли ошибка при настройке файла.
                     * @ignore
                     */
                    const setFile = file => {
                        const maxCheck = checkMax();
                        if (!maxCheck) return true;

                        const duplicateCheck = checkDuplicate(file);
                        if (!duplicateCheck) return true;

                        const sizeCheck = checkSize(file);
                        if (!sizeCheck) return true;

                        const acceptCheck = checkAccept(file);
                        if (!acceptCheck) return true;

                        file.inputName = Array.isArray(params.name) ? params.name[files.length] : params.name;
                        files.push(file);
                        const fileName = file.name;
                        const filesContent = itemTemplate({
                            title: template(params.lang.selected, {
                                $file: fileName
                            }),
                            label: fileName,
                            image: params.preview && file,
                            cancel: true
                        });

                        const wrapper = document.createElement('div');
                        wrapper.innerHTML = filesContent;
                        const fileLabel = wrapper.firstChild;
                        const button = fileLabel.querySelector('button');
                        button.addEventListener('click', event => {
                            event.stopPropagation();
                            event.preventDefault();

                            const currentIndex = [...fileList.children].indexOf(fileLabel);
                            cancelFile(currentIndex);
                        });
                        fileList.append(fileLabel);

                        if (params.preview) {
                            setFilePreview(fileLabel, file);
                            fileList.append(label);
                        }

                        if (!manualAdd) this.add(file);

                        return false;
                    };

                    /**
                     * @function inputChangeFunc
                     * @desc После добавления файла пользователем проверяет загруженный файл и выполняет дальнейшие действия. При прохождении валидации вызывает событие [add]{@link components.AppFile#event:add} и передает в него загруженный файл. После этого вызывает событие [change]{@link components.AppFile#event:change} и передает в него список всех загруженных файлов.
                     * @param {Array.<Object>} [inputtedFiles] - Коллекция добавленных файлов.
                     * @fires components.AppFile#add
                     * @fires components.AppFile#change
                     * @returns {undefined}
                     * @ignore
                     */
                    inputChangeFunc = inputtedFiles => {
                        const addedFiles = inputtedFiles ? [...inputtedFiles] : [...inputCopy.files];
                        if (addedFiles.length === 0) return;

                        const isError = addedFiles.find(file => {
                            return setFile(file);
                        });
                        if (isError) return;

                        if (!manualAdd) {
                            manualAdd = true;
                        }

                        this.emit('change', files);
                        manualAdd = false;

                        setInput();
                    };

                    inputCopy.addEventListener('change', () => {
                        inputChangeFunc();
                    });
                };

                /**
                 * @function cancelFile
                 * @desc Удаляет файл из коллекции загруженных файлов. Вызывает событие [cancel]{@link components.AppFile#event:cancel}. После этого вызывает событие [change]{@link components.AppFile#event:change} и передает в него коллекцию загруженных файлов.
                 * @param {number} index - Индекс файла в коллекции загруженных файлов.
                 * @fires components.AppFile#cancel
                 * @fires components.AppFile#change
                 * @returns {undefined}
                 * @ignore
                 */
                cancelFile = index => {
                    if (typeof index === 'undefined') {
                        files.length = 0;
                        multipleInput.remove();
                        if (params.multiple) {
                            el.append(label);
                        }
                        fileList.innerHTML = '';

                        setInput();
                    } else {
                        files.splice(index, 1);
                        fileList.children[index].remove();
                    }

                    if (!manualCancel) {
                        manualCancel = true;
                        this.cancel(index);
                    }
                    this.emit('change', files);
                };

                setInput();
            } else {
                /**
                 * @function cancelFile
                 * @desc Удаляет загруженный файл. Вызывает событие [cancel]{@link components.AppFile#event:cancel}. После этого вызывает событие [change]{@link components.AppFile#event:change} и передает в загруженный файл.
                 * @fires components.AppFile#cancel
                 * @fires components.AppFile#change
                 * @returns {undefined}
                 * @ignore
                 */
                cancelFile = () => {
                    files.length = 0;
                    input.value = '';
                    fileList.innerHTML = '';
                    if (!manualCancel) {
                        manualCancel = true;
                        this.cancel();
                    }
                    this.emit('change', files[0]);
                };

                /**
                 * @function inputChangeFunc
                 * @desc После добавления файла пользователем проверяет загруженный файл и выполняет дальнейшие действия. Если пользователь не выбрал файл, то вызывает событие [cancel]{@link components.AppFile#event:cancel}. При прохождении валидации вызывает событие [add]{@link components.AppFile#event:add} и передает в него загруженный файл. После этого вызывает событие [change]{@link components.AppFile#event:change} и передает в него загруженный файл.
                 * @param {Object} [inputtedFile] - Добавленный файл.
                 * @fires components.AppFile#add
                 * @fires components.AppFile#change
                 * @returns {undefined}
                 * @ignore
                 */
                inputChangeFunc = inputtedFile => {
                    const file = inputtedFile || input.files[0];
                    if (!file) {
                        this.cancel();
                        return;
                    }

                    const sizeCheck = checkSize(file);
                    if (!sizeCheck) return;

                    const acceptCheck = checkAccept(file);
                    if (!acceptCheck) return;

                    const fileLabel = file.name;

                    files.length = 0;
                    file.inputName = Array.isArray(params.name) ? params.name[0] : params.name;
                    files.push(file);
                    fileList.innerHTML = this.constructor.itemTemplate({
                        label: fileLabel,
                        cancel: params.preview
                    });
                    if (params.preview) {
                        setFilePreview(fileList.firstChild, file);
                    }

                    const button = fileList.querySelector('button');
                    if (button) {
                        button.addEventListener('click', event => {
                            event.stopPropagation();
                            event.preventDefault();

                            cancelFile();
                        });
                    }

                    if (!manualAdd) {
                        manualAdd = true;
                        this.add(file);
                    }

                    this.emit('change', files[0]);
                    manualAdd = false;
                };

                input.addEventListener('change', () => {
                    inputChangeFunc();
                });
            }

            if (params.drag) {
                el.addEventListener('drop', event => {
                    event.preventDefault();

                    el.classList.remove(isDragoverClass);

                    const dataTransfer = event.dataTransfer;
                    const droppedFiles = dataTransfer.files;

                    manualAdd = true;
                    inputChangeFunc(params.multiple ? droppedFiles : droppedFiles[0]);
                });

                el.addEventListener('dragenter', event => {
                    event.preventDefault();

                    el.classList.add(isDragoverClass);
                });

                el.addEventListener('dragleave', event => {
                    event.preventDefault();

                    el.classList.remove(isDragoverClass);
                });

                el.addEventListener('dragover', event => {
                    event.preventDefault();

                    el.classList.add(isDragoverClass);
                });
            }

            this.on({
                /**
                 * @event AppFile#add
                 * @memberof components
                 * @desc Инициирует диалог выбора файла, либо вызывается после выбора файла пользователем.
                 * @fires components.AppAlert#hide
                 * @fires components.AppFile#change
                 * @returns {undefined}
                 * @example
                 * fileInstance.on('add', () => {
                 *     console.log('Файл выбран пользователем');
                 * });
                 * fileInstance.add(); //Показывает диалог выбора файла
                 */
                add() {
                    if (errorAlert) errorAlert.hide();

                    el.classList.remove(maxClass);

                    if (manualAdd) {
                        manualAdd = false;
                        return;
                    }
                    manualAdd = true;

                    if (params.multiple) {
                        multipleInput.click();
                        return;
                    }

                    input.click();
                },

                /**
                 * @event AppFile#cancel
                 * @memberof components
                 * @desc Удаляет файл из списка файлов, загруженных пользователем.
                 * @param {number} [index] - Индекс файла в поле с множественной загрузкой, который надо удалить. Если индекс не передан, то удаляются все файлы.
                 * @fires components.AppAlert#hide
                 * @fires components.AppFile#change
                 * @returns {undefined}
                 * @example
                 * fileInstance.on('cancel', () => {
                 *     console.log('Файл удалён');
                 * });
                 * fileInstance.cancel(2); //Удаляет третий загруженный пользователем файл
                 */
                cancel(index) {
                    if (errorAlert) errorAlert.hide();

                    el.classList.remove(maxClass);

                    if (manualCancel) {
                        manualCancel = false;
                        return;
                    }
                    manualCancel = true;

                    if (params.multiple) {
                        cancelFile(index);
                        return;
                    }

                    cancelFile();
                    manualCancel = false;
                }
            });

            this.onSubscribe([
                /**
                 * @event AppFile#change
                 * @memberof components
                 * @desc Вызывается, когда был изменён список загруженных файлов.
                 * @param {Array.<Object>} files - Коллекция оставшихся загруженных файлов.
                 * @returns {undefined}
                 * @example
                 * fileInstance.on('change', files => {
                 *     console.log('Файл был удалён или добавлен');
                 *     console.log(files);
                 * });
                 */
                'change'
            ]);

            el.classList.add(initializedClass);
        }

        /**
         * @function AppFile.itemTemplate
         * @memberof components
         * @desc Шаблон элемента для отображения текста пути к файлу.
         * @param {Object} [options={}] - Опции.
         * @param {string} [options.label] - Текст элемента.
         * @param {string} [options.title=options.label] - Атрибут title элемента.
         * @param {boolean} [options.cancel] - Показываить ли кнопку отмены загрузки файла.
         * @returns {string} Строка с HTML-разметкой элемента.
         * @example
         * console.log(app.File.itemTemplate({label: 'Название файла'})); //Выведет HTML-строку с элементом загруженного файла
         */
        static itemTemplate(options = {}) {
            if (typeof itemTemplate !== 'function') return '';

            return itemTemplate(options);
        }
    });

    addModule(moduleName, AppFile);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll('[data-file]').forEach(el => new AppFile(el));

    return AppFile;
};

export default initFile;
export {initFile};