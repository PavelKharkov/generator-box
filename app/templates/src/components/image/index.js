import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'image';

const tagName = 'IMG';

const lazyloadedClass = 'lazyloaded';
const initializedClass = `${moduleName}-initialized`;

const attrOptions = [
    'src',
    'alt',
    'width',
    'height',
    'srcset',
    'sizes'
];

//Опции по умолчанию
const defaultOptions = {
    srcAttr: 'data-src',
    altAttr: 'data-alt',
    widthAttr: 'data-width',
    heightAttr: 'data-height',
    srcsetAttr: 'data-srcset',
    sizesAttr: 'data-sizes'
};

/**
 * @class AppImage
 * @memberof components
 * @classdesc Модуль изображений.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент изображения.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.srcAttr='data-src'] - Атрибут, с которого будет браться значение атрибута src изображения при отложенной загрузке.
 * @param {string} [options.altAttr='data-alt'] - Атрибут, с которого будет браться значение атрибута alt изображения при отложенной загрузке.
 * @param {string} [options.widthAttr='data-width'] - Атрибут, с которого будет браться значение атрибута width изображения при отложенной загрузке.
 * @param {string} [options.heightAttr='data-height'] - Атрибут, с которого будет браться значение атрибута height изображения при отложенной загрузке.
 * @param {string} [options.heightAttr='data-srcset'] - Атрибут, с которого будет браться значение атрибута srcset изображения при отложенной загрузке.
 * @param {string} [options.heightAttr='data-sizes'] - Атрибут, с которого будет браться значение атрибута sizes изображения при отложенной загрузке.
 * @example
 * const imageInstance = new app.Image(document.querySelector('.image-element'), {
 *     srcAttr: 'data-custom-src'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-lazyload="image" data-src="#" data-alt=""></div>
 *
 * <!--data-lazyload="image" - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-image-options='{"srcAttr": "data-src-custom"}'></div>
 */
const AppImage = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const imageOptionsData = el.dataset.imageOptions;
        if (imageOptionsData) {
            const dataOptions = util.stringToJSON(imageOptionsData);
            if (!dataOptions) util.error('incorrect data-image-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppImage#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let imageEl = el;

        /**
         * @member {HTMLElement} AppImage#imageEl
         * @memberof components
         * @desc Элемент изображения.
         * @readonly
         */
        Object.defineProperty(this, 'imageEl', {
            enumerable: true,
            get: () => imageEl
        });

        /**
         * @function initEvents
         * @desc Инициализует браузерные события изображения.
         * @ignore
         */
        const initEvents = () => {
            imageEl.addEventListener('load', () => {
                //console.log('image is loaded', imageEl);
                this.emit('load');
            });

            imageEl.addEventListener('error', () => {
                util.error({
                    message: 'error loading image',
                    element: imageEl
                });
            });

            if (imageEl.complete) {
                util.defer(() => {
                    //console.log('image already loaded', imageEl);
                    this.emit('load');
                });
            }
        };

        this.on({
            /**
             * @event AppImage#lazyload
             * @memberof components
             * @desc Событие, вызываемое при отложенной загрузке изображения.
             * @throws Выводит ошибку, если не указан атрибут пути к файлу.
             * @returns {undefined}
             * @example
             * imageInstance.on('lazyload', () => {
             *     console.log('Изображение было проинициализировано с помощью отложенной загрузки');
             * });
             * imageInstance.lazyload();
             */
            lazyload() {
                if (el.tagName === tagName) return;

                const attrs = {};
                attrOptions.forEach(attr => {
                    const attrValue = el.getAttribute(params[`${attr}Attr`]);
                    if (attrValue === null) return;
                    el.removeAttribute(params[`${attr}Attr`]);
                    attrs[attr] = attrValue;
                });

                if (!attrs.src) {
                    util.error({
                        message: 'required \'src\' attribute not provided',
                        element: el
                    });
                }
                if (!attrs.alt) {
                    attrs.alt = '';
                }

                const newEl = document.createElement('img');

                [...el.attributes].forEach(attr => {
                    newEl.setAttribute(attr.name, attr.value);
                });
                Object.keys(attrs).forEach(attr => {
                    if (attr === null) return;
                    newEl.setAttribute(attr, (attr === 'src') ? encodeURI(attrs[attr]) : attrs[attr]);
                });

                delete newEl.dataset.lazyload;
                newEl.classList.remove(lazyloadedClass);

                el.parentNode.replaceChild(newEl, el);
                imageEl = newEl;
                newEl.modules = {
                    [moduleName]: this
                };

                initEvents();
            }
        });

        this.onSubscribe({
            /**
             * @event AppImage#load
             * @memberof components
             * @desc Событие, вызываемое при загрузке изображения.
             * @returns {undefined}
             * @example
             * imageInstance.on('load', () => {
             *     console.log('Содержимое изображения загружено');
             * });
             */
            load() {
                imageEl.classList.add(initializedClass);
            }
        });

        if (imageEl.tagName === tagName) {
            initEvents();
        }

        if (!el.classList.contains(lazyloadedClass)) {
            this.lazyload();
        }
    }
});

addModule(moduleName, AppImage);

//Инициализация элементов по data-атрибуту
document.querySelectorAll(`[data-lazyload="${moduleName}"]`).forEach(el => new AppImage(el));

export default AppImage;
export {AppImage};

//TODO:parallax image