let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const imageDiffCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppImageDiff = modules.default;
                resolve(AppImageDiff);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            imageDiffCallback(resolve);
            return;
        }

        onInit('imageDiff', () => {
            imageDiffCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('imageDiff');
        });
    });
}

export default importFunc;