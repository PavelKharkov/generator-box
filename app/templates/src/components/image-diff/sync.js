import jQuery from 'Libs/jquery'; //Используется для плагина Twentytwenty
import 'Libs/twentytwenty';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'imageDiff';

//Опции по умолчанию
const defaultOptions = {
    noOverlay: true
};

//Классы
const imageDiffClass = util.kebabCase(moduleName);
const baseClass = 'twentytwenty';
const initializedClass = `${imageDiffClass}-initialized`;

/**
 * @class AppImageDiff
 * @memberof components
 * @requires libs.jquery
 * @requires libs.twentytwenty
 * @classdesc Модуль виджета для сравнения изображений.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент контейнера с изображениями.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.noOverlay=true] - Не отображать полупрозрачный фон при наведении.
 * @ignore
 * @example
 * const imageDiffInstance = new app.ImageDiff(document.querySelector('.image-diff-container'), {
 *     noOverlay: false
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-image-diff>
 *     <img src="" alt="">
 *     <img src="" alt="">
 * </div>
 *
 * <!--data-image-diff - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-image-diff-options='{"noOverlay": false}'></div>
 */
const AppImageDiff = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const images = el.querySelectorAll('img');

        /**
         * @member {Array.<HTMLElement>} AppImageDiff#images
         * @memberof components
         * @desc Коллекция элементов изображений для сравнения.
         * @readonly
         */
        Object.defineProperty(this, 'images', {
            enumerable: true,
            value: images
        });

        if (images.length < 2) util.error('image-diff module requires two \'img\' tags');

        const imageDiffOptionsData = el.dataset.imageDiffOptions;
        if (imageDiffOptionsData) {
            const dataOptions = util.stringToJSON(imageDiffOptionsData);
            if (!dataOptions) util.error('incorrect data-image-diff-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppImageDiff#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const pluginOptions = {
            no_overlay: params.noOverlay /* eslint-disable-line camelcase */
        };
        util.defaultsDeep(params, pluginOptions);

        jQuery(el).twentytwenty(params);

        const handle = el.querySelector(`.${baseClass}-handle`);

        /**
         * @member {HTMLElement} AppImageDiff#handle
         * @memberof components
         * @desc HTML-элеменm ползунка, двигая который, можно изменять видимость изображений.
         * @readonly
         */
        Object.defineProperty(this, 'handle', {
            enumerable: true,
            value: handle
        });

        handle.innerHTML = `
            <div class="${baseClass}-handle-inner">
                <div class="${baseClass}-handle-bg"></div>
                ${handle.innerHTML}
            </div>`;

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppImageDiff);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-image-diff]').forEach(el => new AppImageDiff(el));

export default AppImageDiff;
export {AppImageDiff};