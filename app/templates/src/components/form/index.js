import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let formTriggered;

const formCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "forms" */ './sync.js'))
        .then(modules => {
            chunks.forms = true;
            formTriggered = true;

            const AppForm = modules.default;
            if (resolve) resolve(AppForm);
        });
};

const initForm = event => {
    if (formTriggered) return;

    if (((event.target.tagName === 'BUTTON') || (event.target.tagName === 'INPUT')) &&
        (event.target.type === 'submit')) {
        event.preventDefault();
        event.currentTarget.dataset.validationTriggered = '';
    }

    emitInit('form');
};

const formTrigger = (formItems, formTriggers = ['click', 'input']) => {
    if (__IS_SYNC__) return;

    const items = (formItems instanceof Node) ? [formItems] : formItems;
    if (items.length === 0) return;

    items.forEach(item => {
        formTriggers.forEach(trigger => {
            item.addEventListener(trigger, initForm, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        formCallback(resolve);
        return;
    }

    onInit('form', () => {
        formCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('form');
    });

    const formItems = document.querySelectorAll('[data-form]');
    formTrigger(formItems);
});

export default importFunc;
export {formTrigger};