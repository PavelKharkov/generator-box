import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'form';

const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    initSubmitEvent: true
};

//Регулярные выражения для проверки при сериализации формы
const CRLF = /\r?\n/g;
const submittable = /^(?:input|select|textarea|keygen)/i;
const submitterTypes = /^(?:submit|button|image|reset|file)$/i;
const checkableTypes = /^(?:checkbox|radio)$/i;

//Функции сериализации
//Возвращает коллекцию значений выпадающего списка
const serializeSelect = element => {
    const result = [];

    [...element.options].forEach(option => {
        if (!option.selected) return;

        result.push(option.value);
    });

    return result;
};

//Сериализирует содержимое формы в коллекцию
const serializeArray = formEl => {
    const elements = [...formEl.elements];

    const filteredElements = elements
        .filter(element => {
            const type = element.type;

            return element.name && !element.disabled &&
                submittable.test(element.nodeName) && !submitterTypes.test(type) &&
                (element.checked || !checkableTypes.test(type));
        })
        .map(element => {
            const value = ((element.tagName === 'SELECT') && element.multiple)
                ? serializeSelect(element)
                : element.value;

            if ((typeof value === 'undefined') || (value === null)) {
                return null;
            }

            if (Array.isArray(value)) {
                return {
                    name: element.name,
                    value: value.map(valueItem => {
                        return valueItem.replace(CRLF, '\r\n');
                    })
                };
            }

            return {
                name: element.name,
                value: value.replace(CRLF, '\r\n')
            };
        });

    return filteredElements;
};

//Сериализирует содержимое формы в ассоциативную коллекцию
const serializeAssocArray = formEl => {
    const assocArray = serializeArray(formEl).map(element => {
        const value = ((element.tagName === 'SELECT') && element.multiple)
            ? serializeSelect(element)
            : element.value;
        return [element.name, value];
    });

    return assocArray;
};

//Сериализирует содержимое формы в объект
const serializeObject = formEl => {
    const resultObject = {};
    serializeArray(formEl).forEach(element => {
        const value = ((element.tagName === 'SELECT') && element.multiple)
            ? serializeSelect(element)
            : element.value;

        if (resultObject[element.name]) {
            if (Array.isArray(resultObject[element.name])) {
                resultObject[element.name].push(value);
            } else {
                resultObject[element.name] = [
                    resultObject[element.name],
                    value
                ];
            }
            return;
        }

        resultObject[element.name] = value;
    });

    return resultObject;
};

//Сериализирует содержимое формы в строку
const serialize = formEl => {
    const serializedArray = serializeAssocArray(formEl);

    return util.serialize(serializedArray);
};

/**
 * @class AppForm
 * @memberof components
 * @classdesc Модуль форм.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент формы.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.preventSubmit] - Предотвращать нативную отправку формы при вызове события submit.
 * @param {boolean} [options.initSubmitEvent=true] - Вызывать ли событие submit модуля при нативной отправке формы.
 * @example
 * const formInstance = new app.Form(document.querySelector('form'), {
 *     initSubmitEvent: false
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <form></form>
 *
 * <!--Экземпляры форм инициализируются по тегу или по атрибуту data-form-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <form data-form-options='{"preventSubmit": true}'></form>
 */
const AppForm = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const formOptionsData = el.dataset.formOptions;
        if (formOptionsData) {
            const dataOptions = util.stringToJSON(formOptionsData);
            if (!dataOptions) util.error('incorrect data-form-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppForm#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let submitPrevented = params.preventSubmit;

        /**
         * @member {boolean} AppForm#submitPrevented
         * @memberof components
         * @desc Указывает, предотвращать ли нативную отправку формы при вызове события submit.
         * @readonly
         */
        Object.defineProperty(this, 'submitPrevented', {
            enumerable: true,
            get: () => submitPrevented
        });

        /**
         * @function AppForm#preventSubmit
         * @memberof components
         * @desc Предотвращает нативную отправку формы при вызове события submit.
         * @returns {undefined}
         * @readonly
         */
        Object.defineProperty(this, 'preventSubmit', {
            enumerable: true,
            value() {
                submitPrevented = true;
            }
        });

        /**
         * @function AppForm#unpreventSubmit
         * @memberof components
         * @desc Отменяет предотвращение нативной отправки формы при вызове события submit.
         * @returns {undefined}
         * @readonly
         */
        Object.defineProperty(this, 'unpreventSubmit', {
            enumerable: true,
            value() {
                submitPrevented = false;
            }
        });

        /**
         * @function AppForm#serializeArray
         * @memberof components
         * @desc Сериализирует содержимое формы в коллекцию.
         * @returns {Array.<Object>} Возвращает коллекцию объектов вида {'название поля': 'значение'}.
         * @readonly
         * @example
         * //К примеру, есть форма с полями name и phone
         * formInstance.serializeArray(); //[{name: 'test name'}, {phone: '+7 (999) 999-99-99'}]
         */
        Object.defineProperty(this, 'serializeArray', {
            enumerable: true,
            value() {
                return serializeArray(el);
            }
        });

        /**
         * @function AppForm#serializeAssocArray
         * @memberof components
         * @desc Сериализирует содержимое формы в ассоциативную коллекцию.
         * @returns {Array.<Array>} Возвращает коллекцию с коллекциями вида ['название поля', 'значение'].
         * @readonly
         * @example
         * //К примеру, есть форма с полями name и phone
         * formInstance.serializeAssocArray(); //[['name', 'test name'], ['phone', '+7 (999) 999-99-99']]
         */
        Object.defineProperty(this, 'serializeAssocArray', {
            enumerable: true,
            value() {
                return serializeAssocArray(el);
            }
        });

        /**
         * @function AppForm#serializeObject
         * @memberof components
         * @desc Сериализирует содержимое формы в объект.
         * @returns {Object} Возвращает объект с ключами 'название поля': 'значение'.
         * @readonly
         * @example
         * //К примеру, есть форма с полями name и phone
         * formInstance.serializeObject(); //{name: 'test name', phone: '+7 (999) 999-99-99'}
         */
        Object.defineProperty(this, 'serializeObject', {
            enumerable: true,
            value() {
                return serializeObject(el);
            }
        });

        /**
         * @function AppForm#serialize
         * @memberof components
         * @desc Сериализирует содержимое формы в строку.
         * @returns {string} Возвращает отформатированную строку.
         * @readonly
         * @example
         * //К примеру, есть форма с полями name и phone
         * formInstance.serialize(); //'&name=test%20name&phone=+7%20(999)%20999-99-99
         */
        Object.defineProperty(this, 'serialize', {
            enumerable: true,
            value() {
                return serialize(el);
            }
        });

        const files = [];

        /**
         * @member {Array.<Object>} AppForm#files
         * @memberof components
         * @desc Коллекция ссылок на модули добавления файлов [AppFile]{@link components.AppFile} формы.
         * @readonly
         */
        Object.defineProperty(this, 'files', {
            enumerable: true,
            value: files
        });

        el.querySelectorAll('input[type="file"]').forEach(input => {
            const fileInstance = input.modules && input.modules.file;
            if (!fileInstance) return;

            files.push(fileInstance);
        });

        this.on({
            /**
             * @event AppForm#submit
             * @memberof components
             * @desc Отправляет форму, если свойство preventSubmit не false. После отправки формы вызывает событие [submitted]{@link components.AppForm#event:submitted}.
             * @param {Object} [event={}] - Браузерное событие отправки формы.
             * @fires components.AppForm#submitted
             * @returns {undefined}
             * @example
             * formInstance.on('submit', event => {
             *     console.log(event); //Выведет браузерный объект события отправки
             *     console.log('Форма отправляется');
             * });
             * formInstance.submit();
             */
            submit(event) {
                const eventType = String(event);
                if ((eventType === '[object Event]') || (eventType === '[object SubmitEvent]')) {
                    event.preventDefault();
                }

                if (submitPrevented) return;

                let hiddenInput;
                const {submitter} = event;
                if (submitter) {
                    hiddenInput = document.createElement('input');
                    hiddenInput.type = 'hidden';
                    hiddenInput.name = submitter.name;
                    hiddenInput.value = submitter.value;
                    el.append(hiddenInput);
                }

                el.submit();
                this.emit('submitted');

                if (hiddenInput) hiddenInput.remove();
            }
        });

        this.onSubscribe([
            /**
             * @event AppForm#submitted
             * @memberof components
             * @desc Вызывается при успешной отправке формы.
             * @returns {undefined}
             * @example
             * formInstance.on('submitted', () => {
             *     console.log('Форма была отправлена успешно');
             * });
             */
            'submitted'
        ]);

        if (params.initSubmitEvent) {
            el.addEventListener('submit', event => this.submit(event));
        }

        el.classList.add(initializedClass);

        if (typeof el.dataset.formTriggered !== 'undefined') {
            delete el.dataset.formTriggered;
            if (typeof el.dataset.validationTriggered === 'undefined') {
                this.submit();
            }
        }
    }
});

addModule(moduleName, AppForm);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-form]').forEach(el => new AppForm(el));

export default AppForm;
export {AppForm};