import 'Base/scripts/test';

describe('AppAjax', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const ajaxPromise = require('..').default;

        expect(ajaxPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в функцию инициализации класса', async () => {
        await new Promise(done => {
            return require('..').default.then(AppAjaxTest => {
                expect(AppAjaxTest).toBeInstanceOf(Function);
                done();
            });
        });
    });
});