import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import ajaxInit from '../sync.js';
import AppForm from 'Components/form/sync.js';

const AppAjax = ajaxInit(AppForm);

describe('AppAjax', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.fetch()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value, [isInstance, Promise]]);
        dataTypesCheck({
            checkFunction(fetchFunction, value) {
                return fetchFunction(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            setupFunction() {
                const formEl = document.createElement('form');
                formEl.method = 'get';
                formEl.action = 'api.php';
                document.body.append(formEl);

                new AppForm(formEl); /* eslint-disable-line no-new */
                const ajaxInstance = new AppAjax(formEl);
                const fetchFunction = ajaxInstance.fetch;

                return fetchFunction;
            }
        });

        test('должен возвращать Promise при корректной отправке запроса', () => {
            const formEl = document.createElement('form');
            formEl.method = 'get';
            formEl.action = 'api.php';
            document.body.append(formEl);
            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);

            const ajaxResult = ajaxInstance.fetch();

            expect(ajaxResult).toBeInstanceOf(Promise);
        });

        //TODO:check done, fail, check promise then, check successCallback, check preloader.hide & form.submitted

        // //TODO:use xhr-mock library
        // _test('?', () => {
        //     let doneResult = false;
        //     ajaxInstance.fetch({
        //         done: responseOptions => {
        //             doneResult = responseOptions;
        //         }
        //     });
        //
        //     expect(doneResult).toBe(true);
        // });

        test('должен показывать индикатор загрузки, если вызывается с формы', async () => {
            const formEl = document.createElement('form');
            formEl.method = 'get';
            formEl.action = 'api.php';
            document.body.append(formEl);
            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);

            const preloader = ajaxInstance.preloader;
            const showFunction = jest.fn();
            preloader.on('show', () => showFunction());

            ajaxInstance.fetch();

            await new Promise(done => {
                const isPreloaderShown = preloader.isShown;
                expect(isPreloaderShown).toBe(false);

                setTimeout(() => {
                    const isPreloaderShownAfter = preloader.isShown;
                    expect(showFunction).toHaveBeenCalledWith();
                    expect(isPreloaderShownAfter).toBe(true);
                    done();
                }, 1);

                jest.runAllTimers();
            });
        });

        test('должен вызываться при отправке формы вместо стандартной отправки', () => {
            const formEl = document.createElement('form');
            formEl.method = 'get';
            formEl.action = 'api.php';
            document.body.append(formEl);
            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);

            const fetchFunction = jest.fn();
            ajaxInstance.on('fetch', () => fetchFunction());

            ajaxInstance.form.submit();

            expect(fetchFunction).toHaveBeenCalledWith();
        });

        //TODO:check form files, check fetch with options
    });
});