import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import ajaxInit from '../sync.js';
import AppForm from 'Components/form/sync.js';
import AppPreloader from 'Components/preloader';

const AppAjax = ajaxInit(AppForm);

describe('AppAjax', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalAjax = window.app.Ajax;

        expect(globalAjax.prototype).toBeInstanceOf(Module);
    });

    const dataTypesAssertions = ['HTMLElement']
        .map(value => [value, [throwsError, 'only form nodes can be used for initializing ajax module']]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.ajax;
            return new AppAjax(value);
        },
        dataTypesAssertions,
        defaultValue: [isInstance, AppAjax],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppAjax\' с переданным значением'
    });

    test('должен сохраняться под именем \'ajax\' в свойстве \'modules\' у элементов', () => {
        const formEl = document.createElement('form');
        document.body.append(formEl);

        new AppForm(formEl); /* eslint-disable-line no-new */
        new AppAjax(formEl); /* eslint-disable-line no-new */
        const ajaxInstanceProp = formEl.modules.ajax;

        expect(ajaxInstanceProp).toBeInstanceOf(AppAjax);
    });

    test('должен иметь свойство \'form\' с ссылкой на модуль формы, если инициализирован на HTML-форме', () => {
        const formEl = document.createElement('form');
        document.body.append(formEl);

        new AppForm(formEl); /* eslint-disable-line no-new */
        const ajaxInstance = new AppAjax(formEl);

        const formProperty = ajaxInstance.form;

        expect(formProperty).toBeInstanceOf(AppForm);
    });

    test('должен выбрасывать ошибку, если инициализирован на форме, не имеющей экземпляра \'AppForm\'', () => {
        const formEl = document.createElement('form');
        document.body.append(formEl);
        const ajaxDivInstance = (() => new AppAjax(formEl));

        expect(ajaxDivInstance).toThrow('can\'t find module \'form\' for instance');
    });

    test('должен выбрасывать ошибку, если инициализирован на элементе, не являющимся формой', () => {
        const divEl = document.createElement('div');
        document.body.append(divEl);
        const ajaxDivInstance = (() => new AppAjax(divEl));

        expect(ajaxDivInstance).toThrow('only form nodes can be used for initializing ajax module');
    });

    test('должен инициализировать модуль индикатора загрузки и сохранять экземпляр в свойстве \'preloader\' ' +
        'если инициализирован на элементе', () => {
        const formEl = document.createElement('form');
        document.body.append(formEl);

        new AppForm(formEl); /* eslint-disable-line no-new */
        const ajaxInstance = new AppAjax(formEl);

        const preloaderProperty = ajaxInstance.preloader;

        expect(preloaderProperty).toBeInstanceOf(AppPreloader);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const ajaxPropsArray = [
            'form',
            'preloader',
            'fetch'
        ];
        test.each(ajaxPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const formEl = document.createElement('form');
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);

            const currentProp = ajaxInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const ajaxEventsArray = [
            'fetch',
            'abort'
        ];
        test.each(ajaxEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const formEl = document.createElement('form');
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);
            const eventsArray = ajaxInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const ajaxOptionsMap = [
            ['method', 'post'],
            ['url', '#'],
            ['requestType', 'formData'],
            ['responseType', 'json'],

            ['preventSubmit', true],
            ['serializeFunction', 'serializeObject'],
            ['headers', {'X-Requested-With': 'XMLHttpRequest'}],
            ['preloader', true]
        ];
        test.each(ajaxOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const formEl = document.createElement('form');
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);
            const instanceOptions = ajaxInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption)[(typeof currentOption === 'object') ? 'toMatchObject' : 'toBe'](value); /* eslint-disable-line jest/no-conditional-in-test */
        });
    });

    //TODO:test requestType opt, test filesField, requestTransform

    //TODO:test options types

    describe('[data-ajax-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-ajax-options\'', () => {
            const formEl = document.createElement('form');
            formEl.dataset.ajaxOptions = '{"method": "get", "url": "api.php"}';
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);
            const instanceOptions = ajaxInstance.options;

            expect(instanceOptions.method).toBe('get');
            expect(instanceOptions.url).toBe('api.php');
        });

        test('должен перезаписывать опции из атрибутов формы опциями, указанными в атрибуте \'data-ajax-options\'', () => {
            const formEl = document.createElement('form');
            formEl.method = 'post';
            formEl.action = 'form-api.php';
            formEl.dataset.ajaxOptions = '{"method": "get", "url": "api.php"}';
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);
            const instanceOptions = ajaxInstance.options;

            expect(formEl.method).toBe('post');
            expect(formEl.action).toBe('form-api.php');
            expect(instanceOptions.method).toBe('get');
            expect(instanceOptions.url).toBe('api.php');
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-ajax-options\'', () => {
            const formEl = document.createElement('form');
            formEl.dataset.ajaxOptions = 'true';
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = (() => new AppAjax(formEl));

            expect(ajaxInstance).toThrow('incorrect data-ajax-options format');
        });
    });

    describe('получение опций отправки аякса из атрибутов формы', () => {
        const ajaxOptionsMap = [
            ['method', 'get'],
            ['url', 'api.php']
        ];
        test.each(ajaxOptionsMap)('опция \'%s\' должна браться из атрибута формы', (option, value) => {
            const formEl = document.createElement('form');
            formEl.method = 'get';
            formEl.action = 'api.php';
            document.body.append(formEl);

            new AppForm(formEl); /* eslint-disable-line no-new */
            const ajaxInstance = new AppAjax(formEl);
            const instanceOptions = ajaxInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    test('должен корректно устанавливать свойство \'submitPrevented\' ' +
        'при инициализации экземпляра аякса с опцией \'preventSubmit\' на элементе', () => {
        const formEl = document.createElement('form');
        document.body.append(formEl);

        new AppForm(formEl); /* eslint-disable-line no-new */
        const ajaxInstance = new AppAjax(formEl);

        const formProperty = ajaxInstance.form;

        expect(formProperty.submitPrevented).toBe(true);
    });

    //TODO:abort tests
});