import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const ajaxCallback = resolve => {
    const imports = [require('Components/form').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "forms" */ './sync.js'))
            .then(ajax => {
                chunks.forms = true;

                const AppAjax = ajax.initAjax(...modules);
                resolve(AppAjax);
            });
    });

    emitInit('form');
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        ajaxCallback(resolve);
        return;
    }

    onInit('ajax', () => {
        ajaxCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('ajax');
    });
});

export default importFunc;