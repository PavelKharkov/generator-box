import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import 'Components/file';
import AppPreloader from 'Components/preloader';

//Опции
const moduleName = 'ajax';

//Карта типов контента
const contentTypes = {
    formData: 'multipart/form-data',
    urlencoded: 'application/x-www-form-urlencoded',
    json: 'application/json'
};

//Опции по умолчанию
const defaultOptions = {
    responseType: 'json',
    preventSubmit: true,
    serializeFunction: 'serializeObject',
    headers: {
        'X-Requested-With': 'XMLHttpRequest' //Заголовок, указывающий на то, что запрос отправлен с помощью XMLHttpRequest
    },
    preloader: true,
    timeout: 60 * 1000
};

/**
 * @function formatDataKey
 * @desc Форматирует ключ объекта в строку.
 * @param {string} key - Ключ объекта.
 * @param {string} [parentKey] - Родительский ключ объекта.
 * @returns {string} Отформатированная строка элемента для сериализации.
 * @ignore
 */
const formatDataKey = (key, parentKey) => {
    let keyString = key;
    if (parentKey) {
        const dataName = key.split('[');
        const nameStart = `${parentKey}[${dataName[0]}]`;
        const nameEnd = ((dataName.length > 1) ? '[' : '') + dataName.slice(1).join('[');
        keyString = nameStart + nameEnd;
    }
    return keyString;
};

/**
 * @function addDataItem
 * @desc Добавляет свойства в контентный объект formData.
 * @param {Object} data - Контентный объект formData.
 * @param {string} key - Текущий ключ объекта, к которому добавляются форматируемые свойства.
 * @param {*} value - Значение текущего ключа.
 * @returns {undefined}
 * @ignore
 */
const addDataItem = (data, key, value) => {
    if (util.isObject(value) || Array.isArray(value)) {
        Object.keys(value).forEach(innerItem => {
            const dataField = formatDataKey(innerItem, key);
            addDataItem(data, dataField, value[innerItem]);
        });
        return;
    }
    data.append(key, value);
};

const initAjax = AppForm => {
    /**
     * @class AppAjax
     * @memberof components
     * @requires components#AppForm
     * @classdesc Модуль для работы с аяксом.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} [element] - Элемент формы.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {string} [options.method='post'] - Метод отправки запроса.
     * @param {string} [options.url='#'] - Путь отправки запроса.
     * @param {(Object|Array<*>|Function)} [options.data] - Данные, передаваемые в запрос или функция, возвращающая объект с данными.
     * @param {(Object|Array<*>|Function)} [options.addData] - Дополнительные данные, передаваемые в запрос или функция, возвращающая объект с дополнительными данными.
     * @param {('formData'|'urlencoded'|'json')} [options.requestType] - Тип контента, отправляемого в запросе. По умолчанию определяется согласно атрибуту enctype формы, если он не указан, то соответствует 'formData'.
     * @param {string} [options.responseType='json'] - Тип контента, ожидаемого в ответе на аякс-запрос.
     * @param {Function} [options.done] - Функция, вызываемая после успешной отправке ajax-запроса. Передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
     * @param {Function} [options.fail] - Функция, вызываемая после ошибочной отправки ajax-запроса. Передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
     * @param {Array.<Function>} [options.then] - Коллекция из двух функций (функция при успешном ответе или ошибочном ответе). В каждую функцию передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
     * @param {Function} [options.always] - Функция, вызываемая после успешной или ошибочной отправки ajax-запроса. По умолчанию функция отправляет форму и скрывает индикатор загрузки. Параметром передаются опции запроса.
     * @param {boolean} [options.preventSubmit=true] - Предотвращать нативную отправку формы при вызове события submit формы.
     * @param {string} [options.elementsField] - Название ключа со значениями элементов формы при отправке.
     * @param {string} [options.filesField] - Название ключа со значениями элементов файлов формы при отправке.
     * @param {('serializeArray'|'serializeObject')} [options.serializeFunction='serializeObject'] - Функция для сериализации элементов формы при отправке.
     * @param {Function} [options.requestTransform] - Функция для трансформации тела запроса перед отправкой. Если в функции возвращается промис, то запрос будет ожидать выполнения промиса перед отправкой.
     * @param {Object} [options.headers] - Заголовки, отправляемые с аякс-запросом. По умолчанию, отправляется заголовок X-Requested-With со значением 'XMLHttpRequest'.
     * @param {(boolean|Object)} [options.preloader=true] - Опции инициализации индикатора загрузки. Если передаётся false, то не инициализировать индикатор загрузки.
     * @param {number} [options.timeout=60000] - Время ожидания завершения ответа.
     * @example
     * const ajaxInstance = new app.Ajax(document.querySelector('form'), {
     *     method: 'get',
     *     url: '/api/some_method',
     *     requestType: 'json',
     *
     *     //При вызове метода fetch, отправляемые данные будут объединены с данными формы
     *     addData() {
     *         return {
     *             somedata: 'value'
     *         };
     *     }
     *     elementsField: 'params.elements', //Вложенные пути необходимо добавлять через точку
     *
     *     //События при ответе на запрос
     *     done(response, status, xhr, options) {
     *         console.log('Отправлено успешно');
     *         console.log(response); //Ответ запроса
     *         console.log(status); //Статус ответа
     *         console.log(xhr); //XMLHttpRequest-объект ответа запроса
     *         console.log(options); //Опции запроса
     *     },
     *     fail(response, status, xhr, options) {
     *         console.log('Ошибка при отправке запроса');
     *         console.log(response); //Ответ запроса
     *         console.log(status); //Статус ответа
     *         console.log(xhr); //XMLHttpRequest-объект ответа запроса
     *         console.log(options); //Опции запроса
     *     },
     *     always(options) {
     *         console.log('Запрос был отправлен');
     *         console.log(options); //Опции запроса
     *     },
     *
     *     requestTransform(data) {
     *         console.log(data); //Выведет трансформированные данные, готовые к отправке запроса
     *     },
     *     headers: {
     *         'some-header': 'header content'
     *     },
     *
     *     preloader: {
     *         size: 'lg'
     *     }
     * });
     *
     * @example <caption>Инициализация модуля аякса без формы</caption>
     * const ajaxInstance = new app.Ajax({
     *     url: '/api/some_method',
     *     //Если модуль инициализируется без формы, то при вызове метода fetch,
     *     //отправляемые данные будут объединены с данными, указанными при инициализации
     *     data: {
     *         somedata: 'value'
     *     }
     * });
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <form data-ajax-options='{"method": "get"}'></form>
     */
    const AppAjax = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            const isHTMLElement = el instanceof HTMLElement;

            let form;

            let isFetching = false;

            /**
             * @member {boolean} AppAjax#isFetching
             * @memberof components
             * @desc Указывает, происходит ли аякс-запрос в данный момент.
             * @readonly
             */
            Object.defineProperty(this, 'isFetching', {
                enumerable: true,
                get: () => isFetching
            });

            if (isHTMLElement) {
                if (el.tagName === 'FORM') {
                    form = el.modules.form || new AppForm(el);

                    /**
                     * @member {Object} AppAjax#form
                     * @memberof components
                     * @desc Если элемент экземпляра является формой, то хранит ссылку на модуль формы [AppForm]{@link components.AppForm}.
                     * @readonly
                     */
                    Object.defineProperty(this, 'form', {
                        enumerable: true,
                        value: form
                    });

                    if (!(form instanceof AppForm)) {
                        util.error({
                            message: '\'form\' module is not instance of \'Form\' class',
                            element: el
                        });
                    }
                } else {
                    util.error('only form nodes can be used for initializing ajax module');
                }
            }

            let fetchEnctype;

            if (form) {
                //Добавление опций из data-атрибута
                const ajaxOptionsData = el.dataset.ajaxOptions;
                if (ajaxOptionsData) {
                    const dataOptions = util.stringToJSON(ajaxOptionsData);
                    if (!dataOptions) util.error('incorrect data-ajax-options format');
                    util.extend(this.options, dataOptions);
                }

                if (!this.options.method && (util.getNodeProperty(el, 'method') !== null)) {
                    this.options.method = util.getNodeProperty(el, 'method');
                }
                if (!this.options.url && (util.getNodeProperty(el, 'action') !== null)) {
                    this.options.url = util.getNodeProperty(el, 'action');
                }
                if (!this.options.requestType && (el.getAttribute('enctype') !== null)) {
                    fetchEnctype = el.getAttribute('enctype'); //Если у формы есть именно атрибут enctype, а не свойство
                }
            }

            if (!this.options.method) {
                this.options.method = 'post';
            }
            if (!this.options.url) {
                this.options.url = '#';
            }
            if (!(fetchEnctype || this.options.requestType)) {
                this.options.requestType = 'formData';
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppAjax#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            let preloader;

            if (form && params.preloader) {
                const preloaderOptions = util.isObject(params.preloader) ? params.preloader : {};
                preloader = new AppPreloader(el, preloaderOptions);

                /**
                 * @member {Object} AppAjax#preloader
                 * @memberof components
                 * @desc Если элемент экземпляра является формой, то хранит ссылку на модуль индикатора загрузки [AppPreloader]{@link components.AppPreloader}.
                 * @readonly
                 */
                Object.defineProperty(this, 'preloader', {
                    enumerable: true,
                    value: preloader
                });
            }

            this.on({
                /**
                 * @event AppAjax#fetch
                 * @memberof components
                 * @desc Делает аякс-запрос. Если экземпляр модуля был инициализирован на форме, то после получения ответа от сервера вызывает событие [submitted]{@link components.AppForm#event:submitted}.
                 * @param {Object} [options={}] - Опции запроса.
                 * @param {string} [options.method] - Метод отправки запроса.
                 * @param {string} [options.url] - Путь отправки запроса.
                 * @param {(Object|Array<*>|Function)} [options.data] - Данные, передаваемые в запрос или функция, возвращающая объект с данными.
                 * @param {(Object|Array<*>|Function)} [options.addData] - Дополнительные данные, передаваемые в запрос или функция, возвращающая объект с дополнительными данными.
                 * @param {('formData'|'urlencoded'|'json')} [options.requestType] - Тип контента, отправляемого в запросе. По умолчанию определяется согласно атрибуту enctype формы, если он не указан, то соответствует 'formData'.
                 * @param {string} [options.responseType=<значение из опций экземпляра>] - Тип контента, ожидаемого в ответе на аякс-запрос.
                 * @param {Function} [options.done] - Функция, вызываемая после успешной отправке ajax-запроса. Передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
                 * @param {Function} [options.fail] - Функция, вызываемая после ошибочной отправки ajax-запроса. Передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
                 * @param {Array.<Function>} [options.then] - Коллекция из двух функций (функция при успешном ответе или ошибочном ответе). В каждую функцию передаются параметры: контент ответа, статус ответа, xhr-объект ответа и опции запроса.
                 * @param {Function} [options.always] - Функция, вызываемая после успешной или ошибочной отправки ajax-запроса. По умолчанию функция отправляет форму и скрывает индикатор загрузки. Параметром передаются опции запроса.
                 * @param {string} [options.elementsField] - Название ключа со значениями элементов формы при отправке.
                 * @param {string} [options.filesField] - Название ключа со значениями элементов файлов формы при отправке.
                 * @param {('serializeArray'|'serializeObject')} [options.serializeFunction=<значение из опций экземпляра>] - Функция для сериализации элементов формы при отправке.
                 * @param {Function} [options.requestTransform] - Функция для трансформации тела запроса перед отправкой. Если в функции возвращается промис, то запрос будет ожидать выполнения промиса перед отправкой.
                 * @param {Object} [options.headers] - Заголовки, отправляемые с аякс-запросом. По умолчанию, отправляется заголовок X-Requested-With со значением 'XMLHttpRequest'.
                 * @param {(boolean|Object)} [options.preloader=<значение из опций экземпляра>] - Показывать ли индикатор загруки при отправке формы.
                 * @param {number} [options.timeout=<значение из опций экземпляра>] - Время ожидания завершения ответа.
                 * @fires components.AppPreloader#show
                 * @fires components.AppPreloader#hide
                 * @fires components.AppForm#submitted
                 * @fires components.AppAjax#abort
                 * @fires components.AppAjax#timeout
                 * @throws Выводит ошибку при ошибке сериализации полей формы.
                 * @returns {Object} Промис ajax-запроса. Также имеет метод abort, при вызове которого отменяется текущий аякс-запрос.
                 * @example
                 * const promise = ajaxInstance.fetch({
                 *     url: 'api/script.php',
                 *     method: 'post', //По умолчанию
                 *     data: {
                 *         name: 'Test',
                 *         email: 'test@test.com'
                 *     },
                 *     done(response, status, xhr, options) {
                 *         console.log('Отправлено успешно');
                 *         console.log(response); //Ответ запроса
                 *         console.log(status); //Статус ответа
                 *         console.log(xhr); //XMLHttpRequest-объект ответа запроса
                 *         console.log(options); //Опции запроса
                 *         console.log(ajaxInstance.isFetching); //false
                 *     }
                 * });
                 * console.log(ajaxInstance.isFetching); //true
                 *
                 * //Метод fetch возвращает промис
                 * promise
                 * .catch((response, error) => {
                 *     console.log(error);
                 * })
                 * .then(data => {
                 *     console.log(data);
                 * });
                 *
                 * @example <caption>Отмена аякс-запроса</caption>
                 * ajaxInstance.on('abort', xhr => {
                 *     console.log('Аякс-запрос отменён');
                 *     console.log(xhr);
                 * });
                 * const promise = ajaxInstance.fetch();
                 * promise.abort(); //Выведет 'Аякс-запрос отменён' и XMLHttpRequest-объект запроса
                 * //При вызове метода abort промис получает статус rejected
                 */
                fetch(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    util.defaultsDeep(options, params);

                    //Если экземпляр привязан к форме, то использовать атрибуты формы
                    if (form) {
                        const formDefaultOptions = {
                            method: util.getNodeProperty(el, 'method') || options.method,
                            url: util.getNodeProperty(el, 'action') || options.url
                        };
                        fetchEnctype = el.getAttribute('enctype');

                        util.defaultsDeep(options, formDefaultOptions);
                    }

                    if (!fetchEnctype) {
                        fetchEnctype = contentTypes[options.requestType];
                    }

                    if ((typeof options.method !== 'string') || !options.method) {
                        util.typeError(options.method, 'options.method', 'non-empty string');
                    }
                    if ((typeof options.url !== 'string') || !options.url) {
                        util.typeError(options.url, 'options.url', 'non-empty string');
                    }
                    if (typeof fetchEnctype !== 'string') {
                        util.typeError(fetchEnctype, 'fetchEnctype', 'string');
                    }
                    if ((typeof options.data !== 'undefined') && (typeof options.data !== 'object') && (typeof options.data !== 'function')) {
                        util.typeError(options.data, 'options.data', 'object or function');
                    }
                    if (typeof options.serializeFunction !== 'string') {
                        util.typeError(options.serializeFunction, 'options.serializeFunction', 'string');
                    }
                    if (!Number.isFinite(options.timeout)) {
                        util.typeError(options.timeout, 'options.timeout', 'number');
                    }

                    let data = {};
                    const isGet = options.method === 'get';
                    const isFormData = fetchEnctype === 'multipart/form-data';
                    const isJson = fetchEnctype === 'application/json';
                    //Если тип запроса это FormData и метод запроса не 'get'
                    if (isFormData && !isGet) {
                        const formData = new FormData();

                        const initialData = (typeof options.data === 'function') ? options.data() : options.data;
                        //Может быть объектом или коллекцией
                        if (typeof initialData === 'object') {
                            //Заполнить результирующий объект formData контентом из опции data
                            Object.keys(initialData).forEach(item => {
                                addDataItem(formData, item, initialData[item]);
                            });
                        }

                        //Если экземпляр привязан к форме, то получить контент для отправки с помощью сериализации
                        if (form) {
                            const formFieldsData = util.attempt(() => form[options.serializeFunction]());
                            if (formFieldsData instanceof Error) util.error('error serializing form fields');

                            if (Array.isArray(formFieldsData)) {
                                formFieldsData.forEach((item, index) => {
                                    const dataField = formatDataKey(String(index), options.elementsField);
                                    addDataItem(formData, dataField, item);
                                });
                            } else {
                                Object.keys(formFieldsData).forEach(item => {
                                    const dataField = formatDataKey(item, options.elementsField);
                                    const value = formFieldsData[item];
                                    if (Array.isArray(value)) {
                                        value.forEach(valueItem => {
                                            formData.append(dataField, valueItem);
                                        });
                                        return;
                                    }
                                    formData.append(dataField, formFieldsData[item]);
                                });
                            }

                            const files = form.files;
                            if (files.length > 0) {
                                files.forEach(fileModule => {
                                    if (fileModule.files.length === 0) return;

                                    const inputName = fileModule.input.name;
                                    fileModule.files.forEach(file => {
                                        if (file.preloaded) return;
                                        const fileInputName = file.inputName || inputName;
                                        const dataField = formatDataKey(fileInputName, options.filesField);
                                        formData.append(dataField, file);
                                    });
                                });
                            }
                        }

                        //Добавление дополнительного контента из опции addData в запрос
                        data = formData;
                        const additionalData = (typeof options.addData === 'function') ? options.addData() : options.addData;
                        //Может быть объектом или коллекцией
                        if (typeof additionalData === 'object') {
                            Object.keys(additionalData).forEach(key => {
                                addDataItem(data, key, additionalData[key]);
                            });
                        }
                    } else { //Если для запроса не используется FormData
                        //Создание объекта с контентом запроса
                        let formFieldsData;
                        const initialData = (typeof options.data === 'function') ? options.data() : options.data;
                        //Может быть объектом или коллекцией
                        if (typeof initialData === 'object') {
                            data = {...initialData};
                        } else if (form) {
                            data = {};
                            formFieldsData = util.attempt(() => form[options.serializeFunction]());
                            if (formFieldsData instanceof Error) util.error('error serializing form fields');
                            if (typeof formFieldsData === 'object') {
                                if (options.elementsField) { /* eslint-disable-line max-depth */
                                    data[options.elementsField] = {};
                                    util.defaultsDeep(data[options.elementsField], formFieldsData);
                                } else {
                                    util.defaultsDeep(data, formFieldsData);
                                }
                            }
                        }

                        //Добавление дополнительных данных
                        const additionalData = (typeof options.addData === 'function') ? options.addData() : options.addData;
                        //Может быть объектом или коллекцией
                        if (typeof additionalData === 'object') {
                            util.defaultsDeep(data, additionalData);
                        }

                        //Сериализация контента для отправки запроса
                        if (isGet || !isJson) {
                            data = util.serialize(data);
                            if (data) data = data.slice(1);
                        }

                        //Преобразование контента при отправке json
                        if (isJson) {
                            data = JSON.stringify(data);
                        }

                        //Если опция method — 'get', то создаётся ссылка с сериализованным контентом для запроса
                        if (isGet) {
                            const stringData = (form && (typeof formFieldsData === 'string')) ? formFieldsData.slice(1) : '';
                            const getParamsPrefix = (stringData || data) ? '?' : '';
                            const getParams = getParamsPrefix + stringData + data;
                            const urlPrefix = (options.url === '#') ? window.location.href : options.url;
                            options.url = urlPrefix + getParams;
                        }
                    }

                    if (typeof options.requestTransform === 'function') {
                        data = options.requestTransform(data);
                    }

                    //Показ индикатора загрузки
                    if (isHTMLElement && options.preloader) {
                        preloader.show();
                    }

                    //Инициализация аякс-объекта
                    const xhr = new XMLHttpRequest();

                    //Инициализация промиса-обёртки аякс-запроса
                    const ajaxPromise = new Promise((resolve, reject) => {
                        xhr.addEventListener('readystatechange', () => {
                            if (xhr.readyState !== XMLHttpRequest.DONE) return;

                            const responseData = (__HAS_POLYFILLS__ && (util.browser === 'ie') && (options.responseType === 'json'))
                                ? JSON.parse(xhr.response)
                                : xhr.response;
                            const response = {
                                data: responseData,
                                status: xhr.status,
                                xhr,
                                options
                            };
                            if (xhr.status === 200) {
                                resolve(response);
                                return;
                            }

                            reject(response);
                            this.emit('abort', xhr);
                        });

                        xhr.addEventListener('timeout', () => {
                            const response = {
                                data: xhr.response,
                                status: xhr.status,
                                xhr,
                                options
                            };

                            reject(response);
                            this.emit('timeout', xhr);
                        });

                        xhr.open(
                            options.method,
                            options.url
                        );

                        if (typeof options.responseType !== 'undefined') {
                            xhr.responseType = options.responseType;
                        }

                        xhr.timeout = options.timeout;

                        //Установка заголовков запроса
                        if (typeof options.headers === 'object') {
                            Object.keys(options.headers).forEach(key => {
                                xhr.setRequestHeader(key, options.headers[key]);
                            });
                        }
                        if (!isFormData) {
                            xhr.setRequestHeader('Content-Type', fetchEnctype); //Установка заголовка 'Content-Type'
                        }

                        if (data instanceof Promise) {
                            data
                                .then(
                                    promisedData => {
                                        xhr.send(promisedData);
                                    },
                                    () => {
                                        //empty function
                                    }
                                )
                                .catch(error => {
                                    if (form && options.preloader) {
                                        preloader.hide();
                                    }
                                    throw error;
                                });
                        } else {
                            xhr.send(data);
                        }

                        return xhr;
                    });

                    //Метод, добавляемый промису запроса для отмены аякс-запроса
                    ajaxPromise.abort = () => {
                        isFetching = false;
                        xhr.abort();
                    };

                    //Подключение функций обратного вызова из опций
                    ajaxPromise
                        .then(
                            //При успешном выполнении промиса
                            response => {
                                if (typeof options.done === 'function') {
                                    options.done(...Object.values(response));
                                }

                                if (Array.isArray(options.then) && (typeof options.then[0] === 'function')) {
                                    options.then[0](...Object.values(response));
                                }
                            },

                            //При ошибке во время выполнения промиса
                            error => {
                                if (typeof options.fail === 'function') {
                                    options.fail(...Object.values(error));
                                }

                                if (Array.isArray(options.then) && (typeof options.then[1] === 'function')) {
                                    options.then[1](...Object.values(error));
                                }
                            }
                        )

                        //Действие по завершению аякс-запроса при любом исходе после отправки запроса
                        .finally(() => {
                            //Скрытие индикатора загрузки и вызов события 'submitted' формы после отправки запроса
                            if (form) {
                                if (options.preloader) preloader.hide();
                                form.emit('submitted');
                            }
                            if (typeof options.always === 'function') options.always(options);

                            isFetching = false;
                        });

                    isFetching = true;

                    return ajaxPromise;
                }
            });

            this.onSubscribe([
                /**
                 * @event AppAjax#abort
                 * @memberof components
                 * @desc Вызывается при отмене аякс-запроса.
                 * @param {Object} xhr - XMLHttpRequest-объект запроса.
                 */
                'abort',

                /**
                 * @event AppAjax#timeout
                 * @memberof components
                 * @desc Вызывается при превышении времени ожидания ответа.
                 * @param {Object} xhr - XMLHttpRequest-объект запроса.
                 */
                'timeout'
            ]);

            //Вызывать аякс-запрос и препятствовать выполнению формы по умолчанию с опцией preventSubmit. Если у формы проинициализирован модуль валидации, то отправка аякса должна производиться через модуль валидации
            if (params.preventSubmit && form && !el.modules.validation) {
                form.preventSubmit();
                form.on('submit', () => {
                    return this.fetch();
                });
            }
        }
    });

    addModule(moduleName, AppAjax);

    return AppAjax;
};

export default initAjax;
export {initAjax};

//TODO:add withCredentials opt, fix headers opts merge, remove spaces from js templates