import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const urlCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const AppUrl = modules.default;
            resolve(AppUrl);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        urlCallback(resolve);
        return;
    }

    onInit('url', () => {
        urlCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('url');
    });
});

export default importFunc;