import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'url';

const dataHashAction = 'data-hash-action';

//Объект для хранения зарегистрированных действий
const actions = {};

//Возвращает хэш из строки
const getHash = urlString => {
    const hashIndex = String(urlString).indexOf('#');
    return (hashIndex === -1) ? '' : urlString.slice(hashIndex + 1);
};

//Ищет соответствие переданного хэша для переданного объекта действий
const testAction = (actionId, urlString = window.location.hash) => {
    const hash = getHash(urlString);
    if (!hash) return false;

    const actionObject = actions[actionId];
    let actionElement = document.querySelector(`#${hash}`);

    if (actionObject.idSelectors) {
        const matchedIdSelector = actionObject.idSelectors.find(idSelector => {
            const foundElement = document.querySelector(`[${idSelector}="${hash}"]`);
            return foundElement && foundElement.matches(actionObject.selector);
        });
        actionElement = document.querySelector(`[${matchedIdSelector}="${hash}"]`);
    } else {
        const matchedSelector = util.attempt(() => actionElement.matches(actionObject.selector));
        if ((matchedSelector instanceof Error) || !matchedSelector) {
            return false;
        }
    }
    if (!actionElement || !actionElement.modules) {
        return false;
    }

    const functionPath = actionObject.action.split('.');
    const action = functionPath.reduce((fullPath, currentPath) => {
        return fullPath[currentPath];
    }, actionElement.modules);

    //Добавить параметр hash в передаваемые вызываемой функции данные
    if (typeof actionObject.data === 'undefined') {
        actionObject.data = {hash};
    } else if (Array.isArray(actionObject.data)) {
        actionObject.data.unshift(hash);
    } else if (util.isObject(actionObject.data) && (actionObject.data !== null)) {
        actionObject.data.hash = hash;
    }

    if (typeof action === 'function') {
        const actionData = Array.isArray(actionObject.data) ? actionObject.data : [actionObject.data];
        action.call(window, ...actionData);
    } else {
        util.error({
            message: `action ${actionObject.action} not found on matched element`,
            element: actionElement
        });
    }

    return true;
};

//Регистрирует функции модулей для вызова по хэшу
const registerAction = (options = {}) => {
    if (!util.isObject(options)) {
        util.typeError(options, 'options', 'plain object');
    }

    if (!(options.id && options.selector && options.action)) {
        util.error('id, selector and action properties are required');
    }

    const actionId = options.id;
    delete options.id;
    actions[actionId] = options;

    //Проверка адресной строки после регистрации действия
    testAction(actionId);
};

/**
 * @function AppUrl
 * @namespace components.AppUrl
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для работы с адресной строкой.
 * @async
 */
const AppUrl = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppUrl
     * @returns {undefined}
     */
    function() {
        /**
         * @member {Object} components.AppUrl#actions
         * @memberof components
         * @desc Объект для хранения зарегистрированных действий.
         * @readonly
         */
        Object.defineProperty(this, 'actions', {
            enumerable: true,
            value: actions
        });

        /**
         * @function components.AppUrl#getHash
         * @desc Возвращает хэш из строки.
         * @param {string} urlString - Строка для чтения хэша.
         * @returns {string} Строка с хэшем не включая '#'.
         * @readonly
         * @example
         * app.url.getHash(window.location.hash);
         */
        Object.defineProperty(this, 'getHash', {
            enumerable: true,
            value: getHash
        });

        /**
         * @function components.AppUrl#registerAction
         * @desc Регистрирует функции модулей для вызова по хэшу.
         * @param {Object} [options={}] - Опции инициализации.
         * @param {string} options.id - Идентификатор действия.
         * @param {string} options.selector - Селекторы, чьи атрибуты будут сравниваться с хешем.
         * @param {string} options.action - Действие, которое надо выполнить при совпадении идентификатора в атрибуте селектора и хеша. Поиск действия осуществляется в параметре modules элемента.
         * @param {string} options.idSelectors - Атрибуты, содержимое которых будет сравниваться с хешем.
         * @param {string} options.data - Дополнительные параметры, которые должны передаваться в вызываемую функцию.
         * @returns {undefined}
         * @readonly
         * @example
         * app.url.registerAction({
         *     id: 'collapse',
         *     selector: '[data-hash-action="collapse"]',
         *     action: 'collapse.show',
         *     idSelectors: ['id', 'data-collapse-id']
         * });
         * //Если элемент с селектором [data-hash-action="collapse"] будет совпадать с хешем (при вызове getAction) по атрибутам 'id' или 'data-collapse-id',
         * //то у этого элемента вызывается функция по пути modules.collapse.show
         */
        Object.defineProperty(this, 'registerAction', {
            enumerable: true,
            value: registerAction
        });

        /**
         * @function components.AppUrl#testAction
         * @desc Ищет соответствие переданного хэша для переданного объекта действий.
         * @param {string} [actionId] - Идентификатор действия. По умолчанию используется id последнего зарегистрированного действия.
         * @param {string} [urlString=window.location.hash] - Строка для чтения хэша.
         * @returns {boolean} Найден ли подходящий элемент.
         * @readonly
         * @example
         * app.url.testAction('popup', '#some-popup'); //Ищет элемент с идентификатором #some-popup и пытается активировать его через модуль popup
         */
        Object.defineProperty(this, 'testAction', {
            enumerable: true,
            value: testAction
        });

        /**
         * @function components.AppUrl#getAction
         * @desc Ищет соответствие переданного хэша в списке зарегистрированных функций и вызывает её.
         * @param {string} [urlString=window.location.hash] - Строка для чтения хэша.
         * @returns {undefined}
         * @readonly
         * @example
         * app.url.getAction('#test-action');
         */
        Object.defineProperty(this, 'getAction', {
            enumerable: true,
            value(urlString = window.location.hash) {
                if (!getHash(urlString)) return;

                Object.keys(actions).some(actionId => {
                    return testAction(actionId, urlString);
                });
            }
        });

        /**
         * @function components.AppUrl#removeAction
         * @desc Удаляет зарегистрированное для открытия по хэшу действие.
         * @param {string} actionId - Идентификатор, по которому будет искаться удаляемое действие модуля по хешу.
         * @returns {undefined}
         * @readonly
         * @example
         * app.url.removeAction('popup');
         */
        Object.defineProperty(this, 'removeAction', {
            enumerable: true,
            value(actionId) {
                if (!actionId) return;

                delete actions[actionId];
            }
        });

        /**
         * @member {string} components.AppUrl#dataHashAction
         * @memberof components
         * @desc Data-атрибут, по которому определяется выполняемое действие.
         * @readonly
         */
        Object.defineProperty(this, 'dataHashAction', {
            enumerable: true,
            value: dataHashAction
        });
    },

    moduleName
));

addModule(moduleName, AppUrl);

export default AppUrl;
export {AppUrl};