import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const slideoutCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "slideout" */ './sync.js'))
        .then(slideoutModules => {
            chunks.slideout = true;

            const {AppSlideout} = slideoutModules;

            if (isModules) {
                const {AppSlideoutButton} = slideoutModules;
                resolve({
                    AppSlideout,
                    AppSlideoutButton
                });
                return;
            }

            resolve(AppSlideout);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.slideout) {
        slideoutCallback(resolve);
        return;
    }

    onInit('slideout', () => {
        slideoutCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('slideout');
    });
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.slideout) {
        slideoutCallback(resolve, true);
        return;
    }

    onInit('slideout', () => {
        slideoutCallback(resolve, true);
    });

    //TODO:slideout trigger
});

export default importFunc;
export {modules};