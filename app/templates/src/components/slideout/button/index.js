import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'slideoutButton';

const slideoutClass = '.slideout';

const slideoutButtonEl = document.querySelectorAll('[data-slideout]');

//TODO:add example
/**
 * @class AppSlideoutButton
 * @memberof components
 * @classdesc Кнопка открытия выезжающего меню.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - HTML-элемент экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [target] - Селектор элемента, который должен быть помещён в выезжающее меню.
 * @param {*} [options...] - Другие опции модуля выезжающего меню [AppSlideout]{@link components.AppSlideout}.
 */
const AppSlideoutButton = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const slideoutOptionsData = el.dataset.slideoutOptions;
        if (slideoutOptionsData) {
            const dataOptions = util.stringToJSON(slideoutOptionsData);
            if (!dataOptions) util.error('incorrect data-slideout-options format');
            util.extend(this.options, dataOptions);
        }

        /**
         * @member {Object} AppSlideoutButton#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        require('Components/slideout').default.then(AppSlideout => {
            const {baseSlideout} = AppSlideout;

            el.addEventListener('click', event => {
                event.preventDefault();

                const target = params.target || el.getAttribute('href');
                if (!target) {
                    util.typeError(target, 'target', 'non-empty string');
                }

                const content = document.querySelector(target);
                if (!content) {
                    util.error({
                        message: 'Slideout content element not found',
                        element: content
                    });
                }

                const checkFunction = () => {
                    const targetInSlideout = content.closest(slideoutClass);
                    if (!targetInSlideout) {
                        const slideoutContent = document.querySelector(`${slideoutClass}__content`);
                        slideoutContent.append(content);
                    }
                    baseSlideout.off('show', checkFunction);
                };
                baseSlideout.on('show', checkFunction);
                baseSlideout.show(params);
            });

            //TODO:emitInit
        });
    }
});

addModule(moduleName, AppSlideoutButton);

//Инициализация элементов по data-атрибуту
slideoutButtonEl.forEach(el => new AppSlideoutButton(el));

export default AppSlideoutButton;
export {AppSlideoutButton};