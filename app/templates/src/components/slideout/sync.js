import AppSlideout from './base';
import AppSlideoutButton from './button';

export default AppSlideout;
export {
    AppSlideout,
    AppSlideoutButton
};