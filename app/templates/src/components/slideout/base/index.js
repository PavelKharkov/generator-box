import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'slideout';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Классы
const slideoutClass = moduleName;
const wrapperClass = `${slideoutClass}__wrapper`;
const contentClass = `${slideoutClass}__content`;
const closeClasses = `.${slideoutClass}__shim, .${slideoutClass}__close`;

//Другие опции
const dataSlideout = 'data-slideout-';
const focusableElements = 'input[type="text"], input[type="tel"], input[type="email"], input[type="password"], textarea, select';

//Элементы
const pageWrapper = document.querySelector('.page-wrapper');
const pageWrapperInner = document.querySelector('.page-wrapper-inner');

//Параметры по умолчанию для методов выезжающего меню
const defaultPosition = 'right';
const defaultDuration = 300;
const defaultPushContent = false;
const defaultWidth = Number.parseInt(util.getStyle('--slideout-width'), 10);

//Опции по умолчанию
const defaultOptions = {
    position: defaultPosition,
    duration: defaultDuration,
    pushContent: defaultPushContent,
    width: defaultWidth,
    closeButton: true,
    closeButtonInside: true,
    focusOnShow: true
};

/**
 * @function removeDataAttrs
 * @desc Удаляет data-атрибуты, относящиеся к выезжающему меню.
 * @param {Array.<string>} attrsArray - Коллекция data-атрибутов, которые надо удалить.
 * @returns {undefined}
 * @ignore
 */
const removeDataAttrs = attrsArray => {
    attrsArray.forEach(attr => {
        document.documentElement.removeAttribute(dataSlideout + attr);
    });
};

const slideoutElement = document.querySelectorAll(`.${slideoutClass}`);

let baseSlideout; /* eslint-disable-line prefer-const */

/**
 * @class AppSlideout
 * @memberof components
 * @requires components#AppTouch
 * @requires components#AppWindow
 * @classdesc Модуль выезжающего меню.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент меню.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.position='right'] - Сторона расположения меню.
 * @param {number} [options.duration=300] - Длительность анимаци меню.
 * @param {boolean} [options.pushContent=false] - Сдвигать ли контент страницы при появлении меню.
 * @param {number} [options.width=320] - Максимальная ширина элемента меню.
 * @param {boolean} [options.focusOnShow=true] - Устанавливать фокус на первое найденное поле ввода внутри выезжающего меню после его открытия.
 * @example
 * const slideoutInstance = new app.Slideout(document.querySelector('.slideout-element'), {
 *     position: 'left',
 *     duration: 200,
 *     pushContent: true,
 *     width: 400
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="slideout">
 *     <div class="slideout__overlay"></div>
 *     <div class="slideout__bg"></div>
 *     <div class="slideout__wrapper">
 *         <div class="slideout__inner">
 *             <aside class="slideout__content"></aside>
 *             <div class="slideout__shim"></div>
 *         </div>
 *         <button class="slideout__close close" type="button" title="Закрыть меню (Esc)"></button>
 *     </div>
 * </div>
 *
 * <!--.slideout - селектор по умолчанию-->
 */
const AppSlideout = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        let slideoutEl = el;

        /**
         * @member {HTMLElement} AppSlideout#slideoutEl
         * @memberof components
         * @desc Элемент выезжающего меню.
         * @readonly
         */
        Object.defineProperty(this, 'slideoutEl', {
            enumerable: true,
            get: () => slideoutEl
        });

        let markupInitialized = slideoutEl instanceof HTMLElement;

        /**
         * @member {boolean} AppSlideout#markupInitialized
         * @memberof components
         * @desc Указывает, проинициализирована ли разметка меню.
         * @readonly
         */
        Object.defineProperty(this, 'markupInitialized', {
            enumerable: true,
            get: () => markupInitialized
        });

        if (markupInitialized) {
            const slideoutOptionsData = slideoutEl.dataset.slideoutOptions;
            if (slideoutOptionsData) {
                const dataOptions = util.stringToJSON(slideoutOptionsData);
                if (!dataOptions) util.error('incorrect data-slideout-options format');
                util.extend(this.options, dataOptions);
            }
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppSlideout#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const shownBefore = {};

        /**
         * @member {Object} AppSlideout#shownBefore
         * @memberof components
         * @desc Хранит проинициализированные типы меню.
         * @readonly
         */
        Object.defineProperty(this, 'shownBefore', {
            enumerable: true,
            value: shownBefore
        });

        //Переменные состояния
        let isShown = false;

        /**
         * @member {boolean} AppSlideout#isShown
         * @memberof components
         * @desc Указывает, показано ли меню в данный момент.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });
        let showTimeout;

        //Текущие параметры меню
        let currentPosition;
        let currentDuration;
        let currentPushContent;
        let currentType;
        let currentWidth;

        /**
         * @member {string} AppSlideout#currentPosition
         * @memberof components
         * @desc Текущая сторона расположения открытого меню.
         * @readonly
         */
        Object.defineProperty(this, 'currentPosition', {
            enumerable: true,
            get: () => currentPosition
        });

        /**
         * @member {boolean} AppSlideout#currentDuration
         * @memberof components
         * @desc Текущая длительность анимации открытого меню.
         * @readonly
         */
        Object.defineProperty(this, 'currentDuration', {
            enumerable: true,
            get: () => currentDuration
        });

        /**
         * @member {boolean} AppSlideout#currentPushContent
         * @memberof components
         * @desc Указывает, сдвигается ли контент при текущем открытом меню.
         * @readonly
         */
        Object.defineProperty(this, 'currentPushContent', {
            enumerable: true,
            get: () => currentPushContent
        });

        /**
         * @member {string} AppSlideout#currentType
         * @memberof components
         * @desc Текущий тип открытого меню.
         * @readonly
         */
        Object.defineProperty(this, 'currentType', {
            enumerable: true,
            get: () => currentType
        });

        /**
         * @member {boolean} AppSlideout#currentWidth
         * @memberof components
         * @desc Текущая максимальная ширина открытого меню.
         * @readonly
         */
        Object.defineProperty(this, 'currentWidth', {
            enumerable: true,
            get: () => currentWidth
        });

        let scroller;

        /**
         * @member {HTMLElement} AppSlideout#scroller
         * @memberof components
         * @desc Элемент обёртки выезжающего меню.
         * @readonly
         */
        Object.defineProperty(this, 'scroller', {
            enumerable: true,
            get: () => scroller
        });

        let content;

        /**
         * @member {HTMLElement} AppSlideout#content
         * @memberof components
         * @desc Внутренний элемент контента выезжающего меню.
         * @readonly
         */
        Object.defineProperty(this, 'content', {
            enumerable: true,
            get: () => content
        });

        let touch;

        /**
         * @member {Object} AppSlideout#touch
         * @memberof components
         * @desc Ссылка на модуль touch-взаимодействий [AppTouch]{@link components.AppTouch} контентного блока выезжающего меню.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'touch', {
            enumerable: true,
            get: () => touch
        });

        /**
         * @function initInnerElements
         * @desc Инициализирует скрипты для внутренних элементов меню.
         * @returns {undefined}
         * @ignore
         */
        const initInnerElements = () => {
            scroller = slideoutEl.querySelector(`.${wrapperClass}`);

            content = slideoutEl.querySelector(`.${contentClass}`);

            if (content && util.isTouch) {
                require('Components/touch').default.then(AppTouch => {
                    touch = new AppTouch(slideoutEl, {
                        target: content
                    });
                    touch.on('touchmove', event => {
                        if (!event.target.closest(`.${contentClass}`)) return;

                        const moveCoords = touch.moveCoords;
                        const positionModifier = (currentPosition === 'left') ? -1 : 1;
                        if (
                            (moveCoords[0] * positionModifier) >= (content.offsetWidth / 3) &&
                            (Math.abs(moveCoords[1]) <= 70)
                        ) { //TODO:slideout swipe for top & bottom positions
                            this.hide();
                        }
                    });
                });
                emitInit('touch');
            }
        };

        /**
         * @function markupInit
         * @desc Инициализирует разметку выезжающего меню.
         * @param {Object} [options={}] - Опции инициализации разметки.
         * @param {boolean} [options.closeButton] - Показывать ли кнопку закрытия меню.
         * @param {boolean} [options.closeButtonInside] - Показывать ли кнопку закрытия меню внутри контентной области.
         * @returns {undefined}
         * @ignore
         */
        const markupInit = (options = {}) => {
            if (!util.isObject(options)) {
                util.typeError(options, 'options', 'plain object');
            }

            util.defaultsDeep(options, params);

            const currentSlideoutEl = document.createElement('div');
            currentSlideoutEl.classList.add(slideoutClass);
            const closeButtonMarkup = options.closeButton
                ? `<button class="${slideoutClass}__close close" type="button" title="${lang.close}"></button>`
                : '';
            const markup = `
                <div class="${slideoutClass}__overlay"></div>
                <div class="${slideoutClass}__bg"></div>

                <div class="${wrapperClass}" aria-hidden="true">
                    <div class="${slideoutClass}__inner">
                        <aside class="${contentClass}"></aside>
                        <div class="${slideoutClass}__shim"></div>
                    </div>

                    ${options.closeButtonInside ? closeButtonMarkup : ''}
                </div>

                ${options.closeButtonInside ? '' : closeButtonMarkup}`;
            currentSlideoutEl.innerHTML = markup;

            document.body.append(currentSlideoutEl);
            slideoutEl = currentSlideoutEl;
            if (!currentSlideoutEl.modules) {
                currentSlideoutEl.modules = {};
            }
            if (!currentSlideoutEl.modules[moduleName]) {
                currentSlideoutEl.modules[moduleName] = this;
            }

            initInnerElements();

            markupInitialized = true;
        };

        /**
         * @function init
         * @desc Инициализирует разметку меню и добавляет браузернные события.
         * @returns {undefined}
         * @ignore
         */
        const init = () => {
            markupInit();

            slideoutEl.querySelectorAll(closeClasses).forEach(element => {
                element.addEventListener('click', () => this.hide());
            });
        };

        if (markupInitialized) {
            initInnerElements();
        }

        /**
         * @function initEscapeHide
         * @desc Закрывает выезжающее меню при помощи клавиши Escape.
         * @param {Object} event - Браузерное события нажатия клавиши.
         * @fires components.AppSlideout#hide
         * @returns {undefined}
         * @ignore
         */
        const initEscapeHide = event => {
            if (event.key !== 'Escape') return;

            this.hide();
        };

        this.on({
            /**
             * @event AppSlideout#show
             * @memberof components
             * @desc Показывает выезжающее меню.
             * @param {Object} [options={}] - Опции.
             * @param {string} [options.position=<значение из опций экземпляра>] - Сторона размещения меню.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации появления меню.
             * @param {boolean} [options.pushContent=<значение из опций экземпляра>] - Сдвигать ли контент страницы при появлении меню.
             * @param {number} [options.width=<значение из опций экземпляра>] - Ширина элемента меню.
             * @fires components.AppSlideout#shown
             * @returns {undefined}
             * @example
             * slideoutInstance.on('show', options => {
             *     //Инициализировать внутреннее меню при первой активации выезжющего меню
             *     if ((options.type === 'menu') && !this.slideout.shownBefore.menu) {
             *         console.log('Первое открытие выезжающего меню с типом "menu"');
             *     }
             * });
             * slideoutInstance.show({
             *     type: 'menu',
             *     position: 'left'
             * }); //'Первое открытие выезжающего меню с типом "menu"'
             *
             * //При первом открытии меню любого типа, оно сохранится в объекте shownBefore
             * console.log(slideoutInstance.shownBefore); //{menu: true}
             * slideoutInstance.show({
             *     type: 'menu'
             * }); //Второй раз сообщение не отобразится
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (!markupInitialized) init();

                util.defaultsDeep(options, params);

                const windowScrollTop = AppWindow.scrollY() || 0;

                //Сохранение опций в data-атрибут
                currentPosition = options.position;
                document.documentElement.setAttribute(`${dataSlideout}position`, currentPosition);

                currentDuration = options.duration;
                document.documentElement.setAttribute(`${dataSlideout}duration`, currentDuration);

                currentPushContent = options.pushContent;
                document.documentElement.setAttribute(`${dataSlideout}push-content`, currentPushContent);

                currentType = options.type;
                document.documentElement.setAttribute(`${dataSlideout}type`, currentType);

                currentWidth = options.width;
                document.documentElement.setAttribute(`${dataSlideout}width`, currentWidth);

                util.setStyle('--slideout-width', `${currentWidth}px`);

                //TODO:height

                //Инициализация выезжающего меню по типу
                if (!shownBefore[currentType]) {
                    util.defer(() => {
                        shownBefore[currentType] = true;
                    });
                }

                //TODO:maxScreenWidth

                //Начало открытия меню
                document.documentElement.removeAttribute(`${dataSlideout}hidden`);
                util.animationDefer(() => {
                    isShown = true;
                    const setScroll = !document.documentElement.getAttribute(`${dataSlideout}shown`);
                    const cssDuration = `${currentDuration * 0.001}s`;

                    document.documentElement.setAttribute(`${dataSlideout}shown`, 'true');
                    slideoutEl.querySelectorAll(`.${slideoutClass}__overlay, .${slideoutClass}__shim, .${slideoutClass}__inner`)
                        .forEach(transitionItem => {
                            transitionItem.style.transitionDuration = cssDuration;
                        });

                    if (currentPushContent) {
                        pageWrapperInner.style.transitionDuration = cssDuration;
                    }
                    if (setScroll) {
                        pageWrapper.scrollTop = windowScrollTop; //Сохранение текущего значения прокрутки
                    }
                });

                //Конец анимации появления меню
                clearTimeout(showTimeout);
                showTimeout = setTimeout(() => {
                    document.documentElement.setAttribute(`${dataSlideout}shown-end`, 'true');
                    document.addEventListener('keyup', initEscapeHide, {once: true});

                    slideoutEl.ariaHidden = false;

                    if (options.focusOnShow) {
                        const focusableElement = [...content.querySelectorAll(focusableElements)]
                            .find(item => {
                                return item.offsetParent;
                            });
                        if (focusableElement) {
                            focusableElement.focus();
                            focusableElement.dataset.autofocus = '';
                        }
                    }

                    this.emit('shown');
                }, currentDuration);
            },

            /**
             * @event AppSlideout#hide
             * @memberof components
             * @desc Скрывает выезжающее меню.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration] - Длительность анимации скрытия меню. По умолчанию используется длительность из свойства currentDuration экземпляра.
             * @fires layout.AppWindow.scrollTop
             * @fires components.AppSlideout#hidden
             * @returns {undefined}
             * @example
             * slideoutInstance.hide(); //Тип меню указывать не нужно
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, {
                    duration: currentDuration
                });

                const wrapperScrollTop = pageWrapper.scrollTop;
                document.documentElement.setAttribute(`${dataSlideout}hidden`, 'true');
                document.documentElement.removeAttribute(`${dataSlideout}shown-end`);
                clearTimeout(showTimeout);

                showTimeout = setTimeout(() => {
                    isShown = false;

                    slideoutEl.ariaHidden = true;
                    slideoutEl.querySelector(`.${slideoutClass}__inner`).style.transitionDuration = '';
                    removeDataAttrs([
                        'position',
                        'duration',
                        'push-content',
                        'shown',
                        'type',
                        'width'
                    ]);
                    util.setStyle('--slideout-width', `${defaultWidth}px`);

                    AppWindow.scrollTop({
                        value: wrapperScrollTop
                    });

                    this.emit('hidden');
                }, options.duration);
            }
        });

        this.onSubscribe([
            /**
             * @event AppSlideout#shown
             * @memberof components
             * @desc Вызывается, когда выезжающее меню полностью показано.
             * @returns {undefined}
             * @example
             * slideoutInstance.on('shown', () => {
             *     console.log('Выезжающее меню показано');
             * });
             */
            'shown',

            /**
             * @event AppSlideout#hidden
             * @memberof components
             * @desc Вызывается, когда выезжающее меню полностью скрыто.
             * @returns {undefined}
             * @example
             * slideoutInstance.on('hidden', () => {
             *     console.log('Выезжающее меню скрыто');
             * });
             */
            'hidden'
        ]);
    }

    /**
     * @function AppSlideout.baseSlideout
     * @memberof components
     * @desc Get-функция. Ссылка на основной модуль контента меню.
     * @returns {Object}
     */
    static get baseSlideout() {
        return baseSlideout;
    }
});

baseSlideout = new AppSlideout();

addModule(moduleName, AppSlideout);

//Инициализация элементов по классу
slideoutElement.forEach(el => new AppSlideout(el));

export default AppSlideout;
export {AppSlideout};