let importFunc;
let mapGoogleTrigger;

if (__IS_DISABLED_MODULE__) {
    let mapGoogleTriggered;

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const mapGoogleCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "contacts" */ './sync.js'))
            .then(modules => {
                chunks.contacts = true;
                mapGoogleTriggered = true;

                const AppMapGoogle = modules.default;
                resolve(AppMapGoogle);
            });
    };

    const initMapGoogle = () => {
        if (mapGoogleTriggered) return;

        emitInit('mapGoogle');
    };

    mapGoogleTrigger = (mapGoogleItems, mapGoogleTriggers = ['click']) => {
        if (__IS_SYNC__) return;

        const items = (mapGoogleItems instanceof Node) ? [mapGoogleItems] : mapGoogleItems;
        if (items.length === 0) return;

        items.forEach(item => {
            mapGoogleTriggers.forEach(trigger => {
                item.addEventListener(trigger, initMapGoogle, {once: true});
            });
        });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.contacts) {
            mapGoogleCallback(resolve);
            return;
        }

        onInit('mapGoogle', () => {
            mapGoogleCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('mapGoogle');
        });

        const mapGoogleItems = document.querySelectorAll('[data-map-google]');
        mapGoogleTrigger(mapGoogleItems);
    });
}

export default importFunc;
export {mapGoogleTrigger};