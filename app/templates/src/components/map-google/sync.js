import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'mapGoogle';

let scriptPlaced = false;
let maps;

const apiUrl = '//maps.googleapis.com/maps/api/js';

const apikey = app.globalConfig.gmapsApikey;

//Классы
const mapsReadyClass = 'maps-google-ready';
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    init: true
};

/**
 * @class AppMapGoogle
 * @memberof components
 * @classdesc Модуль карт Google.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент для вывода карты.
 * @param {app.Module.instanceOptions} options - Опции экземпляра.
 * @param {string} [options.libraries] - Список библиотек, которые необъодимо подключить в скрипте карт.
 * @param {(Array.<(number|string)>|string)} [options.markerCoords] - Коллекция координат центра карты или строка с двумя координатами, разделенными запятой.
 * @param {string} [options.markerContent] - Текст подсказки при наведении на точку карты.
 * @param {boolean} [options.init=true] - Создавать ли точку на карте при инициализации экземпляра карты.
 * @example
 * const mapGoogleInstance = new app.MapGoogle(document.querySelector('.map-google-element'));
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-map-google-options='{"init": false}'></div>
 */
const AppMapGoogle = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const mapGoogleOptionsData = el.dataset.mapGoogleOptions;
        if (mapGoogleOptionsData) {
            const dataOptions = util.stringToJSON(mapGoogleOptionsData);
            if (!dataOptions) util.error('incorrect data-map-google-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppMapGoogle#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        /**
         * @function readyFunction
         * @desc Функция, вызываемая после загрузки скрипта для работы с картами.
         * @returns {undefined}
         * @ignore
         */
        const readyFunction = () => {
            this.emit('init');
        };

        if (!scriptPlaced) {
            const keyParam = `key=${this.constructor.apikey}`;
            const versionParam = 'v=weekly';
            const langParam = `language=${util.lang || __LANG__}`;
            const regionParam = `region=${util.region || __REGION__}`;
            const librariesParam = params.libraries ? `libraries=${params.libraries}` : '';
            util.addScript(
                `${apiUrl}?${keyParam}&${versionParam}&${langParam}&${regionParam}&${librariesParam}`,
                () => {
                    if (!(window.google && window.google.maps)) util.error('Google maps can\'t load');

                    maps = window.google.maps;

                    readyFunction();
                }
            );
            scriptPlaced = true;
        }

        this.onSubscribe({
            /**
             * @event AppMapGoogle#init
             * @memberof components
             * @desc Событие, вызываемое после инициализации скрипта для работы с картами.
             */
            init() {
                document.documentElement.classList.add(mapsReadyClass);

                //TODO:move map init there

                el.classList.add(initializedClass);
            }
        });

        if (maps) {
            readyFunction();
        }

        let map;

        /**
         * @member {Object} AppMapGoogle#map
         * @memberof components
         * @desc Экземпляр карты скрипта для работы с картами.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'map', {
            enumerable: true,
            get: () => map
        });

        let marker;

        /**
         * @member {Object} AppMapGoogle#marker
         * @memberof components
         * @desc Экземпляр карты скрипта для работы с картами.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'marker', {
            enumerable: true,
            get: () => marker
        });

        if (params.init) {
            const markerCoords = params.markerCoords;
            if (typeof markerCoords !== 'undefined') {
                const coords = (typeof markerCoords === 'string') ? markerCoords.split(',') : markerCoords;
                if (Array.isArray(coords) && (coords.length === 2)) {
                    const markerContent = params.markerContent;

                    this.on('init', () => {
                        const mapCoords = new maps.LatLng(...coords);
                        map = new maps.Map(el, {
                            center: mapCoords,
                            zoom: 17,
                            mapTypeId: 'roadmap'
                        });

                        marker = new maps.Marker({
                            position: mapCoords,
                            map,
                            title: markerContent
                        });
                    });
                } else {
                    util.error('incorrect coords');
                }
            }
        }
    }

    /**
     * @function AppMapGoogle.maps
     * @memberof components
     * @desc Get-функция. Ссылка на переменную window.google.maps.
     * @async
     * @returns {Object}
     */
    static get maps() {
        return maps;
    }

    /**
     * @function AppMapGoogle.apikey
     * @memberof components
     * @desc Get-функция. Возвращает api-ключ Google карт. По умолчанию, берётся из app.globalConfig.gmapsApikey.
     * @async
     * @returns {string}
     */
    static get apikey() {
        return apikey; //TODO:setApikey func
    }
});

addModule(moduleName, AppMapGoogle);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-map-google]').forEach(el => new AppMapGoogle(el));

export default AppMapGoogle;
export {AppMapGoogle};