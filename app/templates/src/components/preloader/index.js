import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'preloader';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Селекторы и классы
const preloaderClass = moduleName;
const preloaderActiveClass = `${preloaderClass}-active`;
const preloaderOverlayClass = `${preloaderClass}-overlay`;
const preloaderSpinnerClass = `${preloaderClass}-spinner`;
const preloaderTextClass = `${preloaderClass}-text`;
const rootSelector = '.page-wrapper-inner';
const initialPreloader = document.querySelector(`${rootSelector} > .${preloaderClass}`);
const activeClass = 'active';
const hidingClass = 'hiding';
const initializedClass = `${moduleName}-initialized`;

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);
const ariaBusy = 'aria-busy';
const defaultLoadingText = lang.loadingText;

/**
 * @function defaultTemplate
 * @desc Шаблон индикатора загрузки.
 * @param {Object} [options] - Опции шаблона индикатора загрузки.
 * @param {string} [options.size] - Дополнительный класс, отвечающий за размер индикатора загрузки.
 * @param {string} [options.text] - Текст индикатора загрузки.
 * @returns {string} HTML-код шаблона индикатора загрузки.
 * @ignore
 */
const defaultTemplate = (options = {}) => {
    if (!util.isObject(options)) {
        util.typeError(options, 'options', 'plain object');
    }

    const preloaderText = options.text
        ? `<span class="${preloaderClass}__text ${preloaderTextClass}">
                ${(options.text === true) ? defaultLoadingText : options.text}
            </span>`
        : '';

    /* eslint-disable unicorn/explicit-length-check */
    const sizeClass = options.size ? ` ${preloaderClass}-${options.size}` : '';
    return `<div class="${preloaderClass + sizeClass}" role="status">
        <div class="${preloaderSpinnerClass}">
            ${preloaderText}
        </div>
    </div>`;
    /* eslint-enable unicorn/explicit-length-check */
};

//Опции по умолчанию
const defaultOptions = {
    template: (initialPreloader && initialPreloader.outerHTML) || defaultTemplate,
    duration: transitionDuration,
    overlay: true,
    newElement: true
};

let contentPreloader;

/**
 * @class AppPreloader
 * @memberof components
 * @classdesc Модуль индикаторов загрузки.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент контейнера с индикатором загрузки.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность появляния и скрытия индикатора.
 * @param {(string|Function)} [options.template] - HTML-шаблон индикатора загрузки или функция, возвращающая HTML-код элемента индикатора загрузки.
 * @param {(boolean|string)} [options.text] - Текст индикатора загрузки. Если true, то берётся из языкового файла модуля.
 * @param {boolean} [options.overlay=true] - Показывать ли наложение на контент при показе индикатора.
 * @param {string} [options.size] - Размер элемента индикатора.
 * @param {boolean} [options.newElement=true] - Использовать новый элемент для индикатора, а не искать внутри элемента.
 * @param {boolean} [options.once] - Удалять ли экземпляр индикатора загрузки после первого скрытия.
 * @example
 * const preloaderInstance = new app.Preloader(document.querySelector('.preloader-element'), {
 *     size: 'lg'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="preloader" role="status">
 *     <div class="preloader__spinner preloader-spinner">
 *         <span class="preloader__text preloader-text">Загрузка…</span>
 *     </div>
 * </div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-preloader-options='{"size": "lg"}'></div>
 */
const AppPreloader = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        if (/IMG|INPUT/.test(el.tagName)) {
            util.error('can\'t initialize preloader on self-closing element');
        }

        const preloaderOptionsData = el.dataset.preloaderOptions;
        if (preloaderOptionsData) {
            const dataOptions = util.stringToJSON(preloaderOptionsData);
            if (!dataOptions) util.error('incorrect data-preloader-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppPreloader#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        //Инициализация элемента
        const preloaderContainer = document.createElement('div');
        const template = (typeof params.template === 'function') ? params.template(params) : params.template;
        preloaderContainer.innerHTML = template;

        let isShown = false;

        /**
         * @member {boolean} AppPreloader#isShown
         * @memberof components
         * @desc Показан ли индикатор загрузки.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let preloader = preloaderContainer.firstChild;

        /**
         * @member {HTMLElement} AppPreloader#preloader
         * @memberof components
         * @desc Элемент индикатора.
         * @readonly
         */
        Object.defineProperty(this, 'preloader', {
            enumerable: true,
            get: () => preloader
        });

        if (params.newElement) {
            el.append(preloader);
        } else {
            const preloaderSelector = [...preloader.classList].reduce((result, currentValue) => {
                return `${result + (result ? ' ' : '')}.${currentValue}`;
            }, '');
            if (el.querySelector(preloaderSelector)) {
                preloader = el.querySelector(preloaderSelector);
            } else {
                el.append(preloader);
            }
        }

        if (!preloader.modules) {
            preloader.modules = {};
        }
        preloader.modules[moduleName] = this;
        preloader.style.transitionDuration = `${params.duration}ms`;

        let preloaderTimeout;

        this.on({
            /**
             * @event AppPreloader#show
             * @memberof components
             * @desc Показывает индикатор загрузки.
             * @fires components.AppPreloader#shown
             * @returns {undefined}
             * @example
             * preloaderInstance.on('show', () => {
             *     console.log('Индикатор загрузки показывается');
             * });
             * console.log(preloaderInstance.isShown); //false
             * preloaderInstance.show();
             * console.log(preloaderInstance.isShown); //true
             */
            show() {
                if (params.overlay) el.classList.add(preloaderOverlayClass);
                preloader.classList.add(activeClass);

                util.animationDefer(() => {
                    el.classList.add(preloaderActiveClass);
                    el.ariaBusy = true;

                    isShown = true;

                    if (typeof params.duration === 'undefined') {
                        this.emit('shown');
                        return;
                    }

                    clearTimeout(preloaderTimeout);
                    preloaderTimeout = setTimeout(() => {
                        this.emit('shown');
                    }, params.duration);
                });
            },

            /**
             * @event AppPreloader#hide
             * @memberof components
             * @desc Скрывает индикатор загрузки.
             * @fires components.AppPreloader#hidden
             * @returns {undefined}
             * @example
             * preloaderInstance.on('hide', () => {
             *     console.log('Индикатор загрузки скрывается');
             * });
             * console.log(preloaderInstance.isShown); //true
             * preloaderInstance.hide();
             * console.log(preloaderInstance.isShown); //false
             */
            hide() {
                preloader.classList.add(hidingClass);
                if (params.overlay) el.classList.remove(preloaderOverlayClass);

                util.animationDefer(() => {
                    el.ariaBusy = false;
                    el.classList.remove(preloaderActiveClass);

                    isShown = false;

                    clearTimeout(preloaderTimeout);
                    preloaderTimeout = setTimeout(() => {
                        preloader.classList.remove(activeClass, hidingClass);
                        this.emit('hidden');
                    }, params.duration);
                });
            }
        });

        this.onSubscribe([
            /**
             * @event AppPreloader#shown
             * @memberof components
             * @desc Вызывается после показа индикатора загрузки.
             * @returns {undefined}
             * @example
             * preloaderInstance.on('shown', () => {
             *     console.log('Индикатор загрузки показан');
             * });
             */
            'shown',

            /**
             * @event AppPreloader#hidden
             * @memberof components
             * @desc Вызывается после скрытия индикатора загрузки.
             * @returns {undefined}
             * @example
             * preloaderInstance.on('hidden', () => {
             *     console.log('Индикатор загрузки скрыт');
             * });
             */
            'hidden'
        ]);

        if (params.once) {
            this.on('hidden', () => {
                if (!preloader.parentNode) return;

                clearTimeout(preloaderTimeout);
                preloader.remove();
                el.removeAttribute(ariaBusy);
                this.unlink();
            });
        }

        el.classList.add(initializedClass);
    }

    /**
     * @function AppPreloader.initContentPreloader
     * @memberof components
     * @desc Инициализирует общий индикатор загрузки контентной области.
     * @returns {undefined}
     */
    static initContentPreloader() {
        if (contentPreloader) return;

        const mainContent = document.querySelector('.main-content');

        contentPreloader = new AppPreloader(mainContent, {
            size: 'lg',
            overlay: false,
            once: true
        });
        contentPreloader.show();
    }

    /**
     * @function AppPreloader.removeContentPreloader
     * @memberof components
     * @desc Скрывает общий индикатор загрузки контентной области.
     * @returns {undefined}
     */
    static removeContentPreloader() {
        if (!contentPreloader) return;

        contentPreloader.on('hidden', () => {
            contentPreloader = null;
        });
        setTimeout(() => {
            contentPreloader.hide();
            document.documentElement.classList.remove('content-preloader');
        }, transitionDuration);
    }

    /**
     * @function AppPreloader.contentPreloader
     * @memberof components
     * @desc Get-функция. Экземпляр [AppPreloader]{@link components.AppPreloader} общего индикатора загрузки контентной области.
     * @returns {Object}
     */
    static get contentPreloader() {
        return contentPreloader;
    }
});

addModule(moduleName, AppPreloader);

export default AppPreloader;
export {AppPreloader};