import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let selectTriggered;

const selectCallback = resolve => {
    const imports = [require('Components/svg').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "select" */ './sync.js'))
            .then(select => {
                chunks.select = true;
                selectTriggered = true;

                const AppSelect = select.selectInit(...modules);
                if (resolve) resolve(AppSelect);
            });
    });

    emitInit('svg');
};

const initSelect = event => {
    if (selectTriggered) return;

    event.preventDefault();
    event.currentTarget.dataset.selectTriggered = '';

    emitInit('select');
};

const selectTrigger = (selectItems, selectTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (selectItems instanceof Node) ? [selectItems] : selectItems;
    if (items.length === 0) return;

    items.forEach(item => {
        selectTriggers.forEach(trigger => {
            item.addEventListener(trigger, initSelect, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.select) {
        selectCallback(resolve);
        return;
    }

    onInit('select', () => {
        selectCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('select');
    });

    const selectItems = document.querySelectorAll('[data-select]');
    selectTrigger(selectItems);
});

export default importFunc;
export {selectTrigger};