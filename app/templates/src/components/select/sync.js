import Choices from 'Libs/choices';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'select';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Классы
const blockClass = 'choices';
const validClass = 'is-valid';
const invalidClass = 'is-invalid';
const disabledClass = 'disabled';
const highlightedClass = 'is-highlighted';
const initializedClass = `${moduleName}-initialized`;
const hideEmptyClass = '-hide-empty';

const SelectPlugin = Choices;

const arrowIconHtml = {
    id: 'arrow',
    width: 10,
    height: 13
};

const clearIconHtml = {
    id: 'close',
    width: 10,
    height: 10
};

//Опции по умолчанию
const defaultOptions = {
    allowHTML: true,
    searchEnabled: false,
    shouldSort: false,

    loadingText: lang.loadingText,
    noResultsText: lang.noResultsText,
    noChoicesText: lang.noChoicesText,
    itemSelectText: lang.itemSelectText,

    classNames: {
        containerInner: 'form-control'
    }
};

const selectInit = AppSvg => {
    /**
     * @class AppSelect
     * @memberof components
     * @requires libs.choices
     * @requires components#AppSvg
     * @classdesc Модуль для раскрывающихся списков.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент списка.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {boolean} [options.allowHTML=true] - Рендерить ли HTML-код в шаблонах списка.
     * @param {boolean} [options.searchEnabled=false] - Показывать ли поле для поиска элементов в списке.
     * @param {boolean} [options.shouldSort=false] - Нужно ли сортировать элементы в списке в алфавитном порядке.
     * @param {boolean} [options.removeItemButton] - Нужно ли показывать кнопку удаления выбранного элемента. По умолчанию true у множественного списка.
     * @param {string} [options.itemSelectText=''] - Текст, показываемый напротив элемента выбора при наведении.
     * @param {Function} [options.callbackOnInit] - Функция вызываемая после инициализации плагина списка. По умолчанию добавляет svg-иконку стрелки выпадающего списка.
     * @param {boolean} [options.disabled] - Является ли список отключенным при инициализации.
     * @param {Object} [options.classNames={}] - Объект с названиями классов элементов списка.
     * @param {Object} [options.classNames.containerInner='form-control'] - Класс, добавляемый активирующему элементу списка.
     * @param {HTMLElement} [options.itemsContainer] - HTML-контейнер со списком элементов для генерации опций выпадающего списка.
     * @param {Function} [options.itemInnerTemplate={@link components.AppSelect.itemInnerTemplate}] - Функция для генерации внутреннего контента элемента выпадающего списка.
     * @param {boolean} [options.hideEmptyOption] - Скрывать опцию с пустым значением из списка опций.
     * @param {*} [options...] - Другие опции плагина [Choices]{@link libs.choices}.
     * @example
     * const selectInstance = new app.Select(document.querySelector('.select-element'), {
     *     searchEnabled: true,
     *     classNames: {
     *         containerInner: 'container-class'
     *     }
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <select id="test-select" data-select></select>
     * <div hidden>
     *     <div data-value="1" data-custom-prop="123">Текст опции 1</div>
     *     <div data-value="2" data-custom-prop="234">Текст опции 2</div>
     * </div>
     *
     * <!--data-select - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-select-options='{"disabled": true}'></div>
     */
    const AppSelect = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const selectOptionsData = el.dataset.selectOptions;
            if (selectOptionsData) {
                const dataOptions = util.stringToJSON(selectOptionsData);
                if (!dataOptions) util.error('incorrect data-select-options format');
                util.extend(this.options, dataOptions);
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppSelect#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            let inner; /* eslint-disable-line prefer-const */

            /**
             * @member {HTMLElement} AppSelect#inner
             * @memberof components
             * @desc HTML-элемент внутреннего контейнера.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'inner', {
                enumerable: true,
                get: () => inner
            });

            if (typeof params.callbackOnInit !== 'function') {
                params.callbackOnInit = () => {
                    util.defer(() => {
                        AppSelect.addIcon(inner);
                    });
                };
            }

            if (params.classNames && params.classNames.containerInner) {
                params.classNames.containerInner = `${blockClass}__inner ${params.classNames.containerInner}`;
            }
            //TODO:other classes

            let showDropdown = false;
            let hideDropdown = false;

            //Проверка на классы валидности
            if (el.classList.contains(validClass)) {
                params.classNames.containerInner += ` ${validClass}`;
            }
            if (el.classList.contains(invalidClass)) {
                params.classNames.containerInner += ` ${invalidClass}`;
            }

            if (el.multiple) {
                params.removeItemButton = true;
            }

            /* eslint-disable prefer-const */
            let instance;
            let initialValues;
            let pluginItemsContainer;
            /* eslint-enable prefer-const */

            let disabled = params.disabled;

            /**
             * @member {boolean} AppSelect#disabled
             * @memberof components
             * @desc Является ли список неактивным. Для изменения необходимо использовать методы disable/enable.
             * @readonly
             */
            Object.defineProperty(this, 'disabled', {
                enumerable: true,
                get: () => disabled
            });

            if (el.disabled) {
                disabled = true;
                el.disabled = false;
            }

            /**
             * @function AppSelect#getValue
             * @memberof components
             * @desc Возвращает выбранное значение списка.
             * @param {boolean} [fullValue] - Если true, то возвращает объект с атрибутами выбранного значения списка.
             * @returns {(string|Object|Array.<string>|Array.<Object>)} Строка с выбранным значением или коллекция значений, если список с множественным выбором, а если вызов был с опцией fullValue, то вместо строковых значений возвращаются объекты с атрибутами выбранных значений списка.
             * @readonly
             * @example
             * //Если список с единичным выбором
             * console.log(selectInstance.getValue()); //Вернёт строку с текущим значением выпадающего списка
             * console.log(selectInstance.getValue(true)); //Вернёт объект с опциями текущего значения
             *
             * //Если список с множественным выбором
             * console.log(selectInstance.getValue()); //Вернёт коллекцию строк с текущими значениями выпадающего списка
             * console.log(selectInstance.getValue(true)); //Вернёт коллекцию объектов с опциями текущих значений
             */
            const getValue = fullValue => {
                return instance.getValue(fullValue !== true);
            };

            Object.defineProperty(this, 'getValue', {
                enumerable: true,
                value: getValue
            });

            /**
             * @function AppSelect#setValue
             * @memberof components
             * @desc Устанавливает новое значение списка по значению её элемента.
             * @param {string} value - Новое значение списка по значению её элемента.
             * @returns {Object} Возвращает экземпляр плагина списка [Choices]{@link libs.choices}.
             * @readonly
             * @example
             * selectInstance.setValue('10'); //Установит данное значение, если опция с таким значением есть в списке
             */
            Object.defineProperty(this, 'setValue', {
                enumerable: true,
                value(value) {
                    return instance.setChoiceByValue(value);
                }
            });

            //Определение функции генерации шаблона внутреннего контента элемента выпадающего списка
            let itemInnerTemplate;
            if (typeof params.itemInnerTemplate === 'function') {
                itemInnerTemplate = params.itemInnerTemplate;
                delete params.itemInnerTemplate;
            } else {
                itemInnerTemplate = this.constructor.itemInnerTemplate;
            }

            /**
             * @function AppSelect#itemInnerTemplate
             * @memberof components
             * @desc Генерирует шаблон внутреннего контента элемента выпадающего списка.
             * @param {string} [label] - Текст элемента списка.
             * @param {Object} [data] - Дополнительные атрибуты элемента.
             * @returns {string} Возвращает HTML-строку внутреннего контента элемента выпадающего списка.
             * @readonly
             */
            Object.defineProperty(this, 'itemInnerTemplate', {
                enumerable: true,
                value: itemInnerTemplate
            });

            //Генерация списка из сторонних элементов
            const instanceObj = this; /* eslint-disable-line consistent-this, unicorn/no-this-assignment */

            let itemsContainer = params.itemsContainer;

            /**
             * @member {HTMLElement} AppSelect#itemsContainer
             * @memberof components
             * @desc HTML-контейнер со списком начальных элементов для генерации опций выпадающего списка.
             * @readonly
             */
            Object.defineProperty(this, 'itemsContainer', {
                enumerable: true,
                get: () => itemsContainer
            });

            if (!itemsContainer) {
                itemsContainer = document.createElement('select'); //Контейнер должен быть select чтобы корректно определялась выбранная опция

                [...el.children].forEach(option => {
                    itemsContainer.append(option);
                });
            }

            /**
             * @member {HTMLElement} AppSelect#pluginItemsContainer
             * @memberof components
             * @desc Элемент контейнера списка опций выпадающего списка.
             * @readonly
             */
            Object.defineProperty(this, 'pluginItemsContainer', {
                enumerable: true,
                get: () => pluginItemsContainer
            });

            /**
             * @function updateHighlightedItem
             * @desc Обновляет текущую выделенную опцию выпадающего списка.
             * @returns {undefined}
             * @ignore
             */
            const updateHighlightedItem = () => {
                if (instance.containerInner.type === 'select-multiple') {
                    //TODO:fix multiple
                } else {
                    const highlightedItem = pluginItemsContainer.querySelector(`.${blockClass}__item--choice.${highlightedClass}`);
                    if (highlightedItem) {
                        highlightedItem.classList.remove(highlightedClass);
                        highlightedItem.removeAttribute('aria-selected');
                    }

                    pluginItemsContainer.querySelectorAll(`.${blockClass}__item--choice`).forEach(choice => {
                        if (typeof choice.dataset.selected === 'undefined') return;

                        choice.classList.add(highlightedClass);
                        choice.ariaSelected = true;
                    });
                }
            };

            /**
             * @function setDataAttrs
             * @desc Добавляет data-атрибуты опции выпадающего списка.
             * @param {HTMLElement} option - Элемент опции.
             * @param {Object} optionsObj - Объект с параметрами, которые должны быть добавлены в виде data-атрибутов.
             * @returns {undefined}
             * @ignore
             */
            const setDataAttrs = (option, optionsObj) => {
                const dataAttributes = Object.keys(option.dataset);
                if (dataAttributes.length > 0) {
                    const customProperties = {};
                    dataAttributes.forEach(dataValue => {
                        customProperties[dataValue] = option.dataset[dataValue];
                    });
                    optionsObj.customProperties = customProperties;
                }
            };

            /**
             * @function setOption
             * @desc Настраивает объект с параметрами опции выпадающего списка.
             * @param {HTMLElement} option - Элемент опции.
             * @param {Object} optionsObj - Текущий объект с параметрами.
             * @ignore
             */
            const setOption = (option, optionsObj) => {
                const isOption = option.tagName === 'OPTION';
                optionsObj.value = isOption ? option.value : option.dataset.value;
                optionsObj.label = option.textContent;
                optionsObj.selected = isOption
                    ? (option.getAttribute('selected') !== null)
                    : (typeof option.dataset.selected !== 'undefined');
                optionsObj.disabled = option.disabled || option.dataset.disabled;

                setDataAttrs(option, optionsObj);
            };

            const groupsCustomProperties = [];

            /**
             * @member {Array.<Object>} AppSelect#groupsCustomProperties
             * @memberof components
             * @desc Коллекция объектов с дополнительными параметрами групп опций.
             * @readonly
             */
            Object.defineProperty(this, 'groupsCustomProperties', {
                enumerable: true,
                value: groupsCustomProperties
            });

            let optionsItemIndex = 1;
            const optionsItems = [...itemsContainer.children].map(optionItem => {
                const optionsObj = {};
                const isOptgroup = optionItem.tagName === 'OPTGROUP';
                const isGroup = isOptgroup || optionItem.dataset.optgroup;
                if (isGroup) {
                    optionsObj.label = isOptgroup ? optionItem.label : optionItem.dataset.label;
                    optionsObj.id = optionsItemIndex;
                    const groupDisabled = isOptgroup ? optionItem.disabled : optionItem.dataset.disabled;
                    optionsObj.choices = [...optionItem.children].map(option => {
                        const optionObj = {};
                        setOption(option, optionObj);
                        return optionObj;
                    });

                    optionsObj.customProperties = {};
                    setDataAttrs(optionItem, optionsObj);
                    if (groupDisabled) {
                        optionsObj.customProperties.disabled = true;
                    }
                    groupsCustomProperties.push(optionsObj.customProperties);
                } else {
                    setOption(optionItem, optionsObj);
                }
                optionsItemIndex += 1;
                return optionsObj;
            });
            //TODO:save optionsItems as prop

            params.choices = params.choices || optionsItems;
            delete params.itemsContainer;

            //TODO:pass templates in options & save default templates as static props, use html dom api

            params.callbackOnCreateTemplates = template => {
                return {
                    item({classNames}, data) {
                        let customDataAttrs = '';
                        const customProperties = data.customProperties;
                        if (customProperties) {
                            Object.keys(customProperties).forEach(dataAttr => {
                                customDataAttrs += `data-${dataAttr}="${customProperties[dataAttr]}" `;
                            });
                        }

                        /* eslint-disable max-len, vue/max-len */
                        const classes = `${classNames.item} ${(data.highlighted ? classNames.highlightedState : classNames.itemSelectable) + (data.disabled ? ` ${disabledClass}` : '')}`;
                        const dataAttrs = `data-item data-id="${data.id}" data-value="${data.value}" ${data.active ? 'aria-selected="true"' : ''} ${data.disabled ? 'aria-disabled="true"' : ''}`;
                        const removeItemButton = instanceObj.params.removeItemButton ? ` <button type="button" class="${classNames.button}" data-button aria-label="${lang.removeButtonText}: '${data.label}'">${AppSvg.inline(clearIconHtml)}</button>` : '';
                        return template(`
                        <div class="${classes}" ${dataAttrs} ${customDataAttrs}>
                            ${instanceObj.itemInnerTemplate(data.label, customProperties) + removeItemButton}
                        </div>`);
                        /* eslint-enable max-len, vue/max-len */
                    },

                    choice({classNames}, data) {
                        let customDataAttrs = '';
                        const customProperties = data.customProperties;
                        if (customProperties) {
                            Object.keys(customProperties).forEach(dataAttr => {
                                customDataAttrs += `data-${dataAttr}="${customProperties[dataAttr]}" `;
                            });
                        }

                        const groupId = data.groupId;
                        if (groupId > -1) {
                            const groupProperties = groupsCustomProperties[groupId - 1];
                            if (groupProperties.disabled) {
                                data.disabled = true;
                            }
                        }

                        /* eslint-disable max-len, vue/max-len */
                        const classes = `${classNames.item} ${classNames.itemChoice + (data.disabled ? ` ${disabledClass}` : '')}`;
                        const dataAttrs = `data-choice data-id="${data.id}" data-value="${data.value}" ${data.disabled ? 'data-choice-disabled aria-disabled="true"' : 'data-choice-selectable'} ${data.selected ? 'data-selected' : ''} ${data.disabled ? 'aria-disabled="true"' : ''}`;
                        return template(`
                        <div class="${classes}" ${dataAttrs} ${customDataAttrs} role="option">
                            ${instanceObj.itemInnerTemplate(data.label, customProperties)}
                        </div>`);
                        /* eslint-enable max-len, vue/max-len */
                    },

                    choiceGroup({classNames}, data) {
                        let customDataAttrs = '';
                        const customProperties = groupsCustomProperties[data.id - 1];
                        if (customProperties) {
                            Object.keys(customProperties).forEach(dataAttr => {
                                customDataAttrs += `data-${dataAttr}="${customProperties[dataAttr]}" `;
                            });
                        }

                        const classes = classNames.group + (customProperties.disabled ? ` ${disabledClass}` : '');
                        const dataAttrs = `data-group data-id="${data.id}" data-value="${data.value}"`;
                        const headingEl = `<div class="${classNames.groupHeading}">${data.value}</div>`;
                        return template(`
                        <div class="${classes}" ${dataAttrs} ${customDataAttrs} role="group">
                            ${headingEl}
                            ${instanceObj.itemInnerTemplate(false, customProperties, 'group')}
                        </div>`);
                    }

                    //TODO:use this template
                    //option(config, data) {
                    //    return template(`
                    //    <option value="${data.value}" ${data.active ? 'selected' : ''} ${data.disabled ? 'disabled' : ''}>
                    //        ${data.label}
                    //    </option>`);
                    //}
                };
            };

            const placeholder = el.placeholder; //TODO:set as option

            //Инициализация плагина списков
            instance = new SelectPlugin(el, params);

            inner = instance.containerInner.element;

            if (placeholder) {
                instance.input.element.placeholder = placeholder;
            }

            /**
             * @member {Object} AppSelect#instance
             * @memberof components
             * @desc Ссылка на экземпляр плагина списка [Choices]{@link libs.choices}.
             * @readonly
             */
            Object.defineProperty(this, 'instance', {
                enumerable: true,
                value: instance
            });

            this.on({
                /**
                 * @event AppSelect#reset
                 * @memberof components
                 * @desc Устанавливает начальное значение списка.
                 * @returns {Object} Возвращает экземпляр плагина списка [Choices]{@link libs.choices} или null, если начальное значение совпадает с текущим значением.
                 * @example
                 * selectInstance.on('reset', () => {
                 *     console.log('Состояние списка было сброшено');
                 * });
                 * selectInstance.reset();
                 */
                reset() {
                    const currentValue = getValue();
                    if ((typeof currentValue === 'string') && (typeof initialValues === 'string')) {
                        if (currentValue === initialValues) return null;
                    } else if (Array.isArray(currentValue) && Array.isArray(initialValues)) {
                        //TODO:test array equality
                    }

                    return this.setValue(initialValues);
                },

                /**
                 * @event AppSelect#disable
                 * @memberof components
                 * @desc Отключает возможность взаимодействия пользователя со списком.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('disable', () => {
                 *     console.log('Список был отключён');
                 * });
                 * selectInstance.disable();
                 */
                disable() {
                    disabled = true;
                    instance.disable();
                    util.disable(instance.containerOuter.element);
                },

                /**
                 * @event AppSelect#enable
                 * @memberof components
                 * @desc Включает возможность взаимодействия пользователя со списком.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('enable', () => {
                 *     console.log('Список был включён');
                 * });
                 * selectInstance.enable();
                 */
                enable() {
                    disabled = false;
                    instance.enable();
                    util.enable(instance.containerOuter.element);
                },

                /**
                 * @event AppSelect#showDropdown
                 * @memberof components
                 * @desc Показывает выпадающий список.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('showDropdown', () => {
                 *     console.log('Выпадающий список показан');
                 * });
                 * selectInstance.showDropdown();
                 */
                showDropdown() {
                    showDropdown = true;
                    instance.showDropdown();
                },

                /**
                 * @event AppSelect#hideDropdown
                 * @memberof components
                 * @desc Скрывает выпадающий список.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('hideDropdown', () => {
                 *     console.log('Выпадающий список скрыт');
                 * });
                 * selectInstance.hideDropdown();
                 */
                hideDropdown() {
                    hideDropdown = true;
                    instance.hideDropdown();
                }

                //TODO:add methods highlightAll, unhighlightAll, removeActiveItemsByValue, removeActiveItems, removeHighlightedItems, toggleDropdown, setChoices, setValue, clearStore, clearInput
            });

            const selectPluginEvents = [
                /**
                 * @event AppSelect#addItem
                 * @memberof components
                 * @desc Вызывается при добавлении нового выбора в списке через плагин.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('addItem', () => {
                 *     console.log('Выбран новый элемент списка');
                 * });
                 */
                'addItem',

                /**
                 * @event AppSelect#removeItem
                 * @memberof components
                 * @desc Вызывается при удалении выбора в списке через плагин.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('removeItem', () => {
                 *     console.log('Элемент списка удалён из выбранных');
                 * });
                 */
                'removeItem',

                /**
                 * @event AppSelect#highlightItem
                 * @memberof components
                 * @desc Вызывается при наведении пользователем на элемент списка.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('highlightItem', () => {
                 *     console.log('Пользователь навёл на элемент списка');
                 * });
                 */
                'highlightItem',

                /**
                 * @event AppSelect#unhighlightItem
                 * @memberof components
                 * @desc Вызывается при снятии наведения с элемента списка.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('unhighlightItem', () => {
                 *     console.log('Пользователь убрал наведение с элемента списка');
                 * });
                 */
                'unhighlightItem',

                /**
                 * @event AppSelect#choice
                 * @memberof components
                 * @desc Вызывается при выборе элемента списка не программно.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('choice', () => {
                 *     console.log('Пользователь выбрал элемент списка');
                 * });
                 */
                'choice',

                /**
                 * @event AppSelect#change
                 * @memberof components
                 * @desc Вызывается при добавлении/удалении элемента в списке пользователем.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('choice', () => {
                 *     console.log('Пользователь изменил текущие выбранные элементы списка');
                 * });
                 */
                'change',

                /**
                 * @event AppSelect#search
                 * @memberof components
                 * @desc Вызывается при изменении значения поля поиска списка.
                 * @returns {undefined}
                 * @example
                 * selectInstance.on('search', () => {
                 *     console.log('Пользователь вводит что-то в поле поиска списка');
                 * });
                 */
                'search'
            ];
            this.onSubscribe(selectPluginEvents);
            //Связывание событий плагина с событиями модуля
            selectPluginEvents.forEach(eventName => {
                el.addEventListener(eventName, event => this.emit(eventName, event));
            });
            //el.addEventListener('change', () => Validation.trigger(el)); //TODO:fix

            el.addEventListener('showDropdown', event => {
                if (!showDropdown) this.showDropdown(event);
                showDropdown = false;

                updateHighlightedItem();
            });
            el.addEventListener('hideDropdown', event => {
                if (!hideDropdown) this.hideDropdown(event);
                hideDropdown = false;
            });

            const container = instance.containerOuter.element;

            /**
             * @member {HTMLElement} AppSelect#container
             * @memberof components
             * @desc Элемент контейнера выпадающего списка.
             * @readonly
             */
            Object.defineProperty(this, 'container', {
                enumerable: true,
                value: container
            });

            if (!container.modules) {
                container.modules = {};
            }
            container.modules[moduleName] = this; //установить ссылку на этот модуль корневому элементу плагина

            if (params.hideEmptyOption) {
                container.classList.add(hideEmptyClass);
            }

            pluginItemsContainer = instance.choiceList.element;

            const callbackOnAddItem = (instance.containerInner.type === 'select-multiple')
                ? () => {
                    //TODO:make callbackOnAddItem for multiple
                }
                : () => {
                    const valueProps = getValue(true);
                    if (!valueProps) return;
                    container.querySelector(`.${blockClass}__item--selectable`).innerHTML =
                        itemInnerTemplate(valueProps.label, valueProps.customProperties);
                };

            this.on('addItem', () => {
                callbackOnAddItem();
                el.blur();
            });
            callbackOnAddItem();

            updateHighlightedItem();

            initialValues = getValue();

            /**
             * @member {(string|Array.<string>)} AppSelect#initialValues
             * @memberof components
             * @desc Начальные значения списка.
             * @readonly
             */
            Object.defineProperty(this, 'initialValues', {
                enumerable: true,
                value: initialValues
            });

            /**
             * @function getCurrentState
             * @desc Возвращает коллекцию текущих элементов плагина выпадающего списка.
             * @returns {Array.<Object>}
             * @ignore
             */
            const getCurrentState = () => {
                return instance._currentState; /* eslint-disable-line no-underscore-dangle */
            };

            /**
             * @function AppSelect#disableOption
             * @memberof components
             * @desc Отключает опцию в списке опций по значению.
             * @param {string} value - Значение опции, которую нужно отключить в списке опций.
             * @returns {undefined}
             * @readonly
             * @example
             * selectInstance.disableOption('10'); //Опция со значением 10 становится отключена
             */
            Object.defineProperty(this, 'disableOption', {
                enumerable: true,
                value(value) {
                    const currentState = getCurrentState();
                    const optionElement = pluginItemsContainer.querySelector(`.${blockClass}__item--choice[data-value="${value}"]`);
                    util.disable(optionElement);
                    const optionId = Number.parseInt(optionElement.dataset.id, 10);
                    const currentOption = currentState.choices.find(item => {
                        return item.id === optionId;
                    });
                    currentOption.disabled = true;
                }
            });

            /**
             * @function AppSelect#enableOption
             * @memberof components
             * @desc Включает опцию в списке опций по значению.
             * @param {string} value - Значение опции, которую нужно включить в списке опций.
             * @returns {undefined}
             * @readonly
             * @example
             * selectInstance.enableOption('10'); //Опция со значением 10 перестаёт быть отключённой
             */
            Object.defineProperty(this, 'enableOption', {
                enumerable: true,
                value(value) {
                    const currentState = getCurrentState();
                    const optionElement = pluginItemsContainer.querySelector(`.${blockClass}__item--choice[data-value="${value}"]`);
                    util.enable(optionElement);
                    const optionId = Number.parseInt(optionElement.dataset.id, 10);
                    const currentOption = currentState.choices.find(item => {
                        return item.id === optionId;
                    });
                    currentOption.disabled = false;
                }
            });

            /**
             * @function AppSelect#disableGroup
             * @memberof components
             * @desc Отключает группу опций в списке опций по значению.
             * @param {number} index - Индекс группы, которую нужно отключить в списке опций.
             * @returns {undefined}
             * @readonly
             * @example
             * selectInstance.disableGroup(2); //Группа опций по номеру 2 становится отключённой
             */
            Object.defineProperty(this, 'disableGroup', {
                enumerable: true,
                value(index) {
                    const currentState = getCurrentState();
                    const currentGroup = currentState.groups[index];
                    const groupId = currentGroup.id;

                    const currentGroupProperties = groupsCustomProperties[index];
                    currentGroupProperties.disabled = true;
                    const groupElement = pluginItemsContainer.querySelector(`.${blockClass}__group[data-id="${groupId}"]`);
                    util.disable(groupElement);

                    currentState.choices.forEach(item => {
                        if (item.groupId === groupId) this.disableOption(item.value);
                    });
                }
            });

            /**
             * @function AppSelect#enableGroup
             * @memberof components
             * @desc Включает группу опций в списке опций по значению.
             * @param {number} index - Индекс группы, которую нужно включить в списке опций.
             * @returns {undefined}
             * @readonly
             * @example
             * selectInstance.enableGroup(2); //Группа опций по номеру 2 перестаёт быть отключённой
             */
            Object.defineProperty(this, 'enableGroup', {
                enumerable: true,
                value(index) {
                    const currentState = getCurrentState();
                    const currentGroup = currentState.groups[index];
                    const groupId = currentGroup.id;

                    const currentGroupProperties = groupsCustomProperties[index];
                    currentGroupProperties.disabled = false;
                    const groupElement = pluginItemsContainer.querySelector(`.${blockClass}__group[data-id="${groupId}"]`);
                    util.enable(groupElement);

                    currentState.choices.forEach(item => {
                        if (item.groupId === groupId) this.enableOption(item.value);
                    });
                }
            });

            if (disabled || params.disabled) {
                this.disable();
            }

            container.classList.add(initializedClass);

            if (typeof el.dataset.selectTriggered !== 'undefined') {
                delete el.dataset.selectTriggered;
                container.click();
            }
        }

        /**
         * @function AppSelect.addIcon
         * @memberof components
         * @desc Добавляет иконку стрелки.
         * @param {HTMLElement} [container] - Элемент, к которому будет добавляться иконка.
         * @returns {HTMLElement} Элемент добавленной иконки.
         * @example
         * app.Select.addIcon(document.querySelector('.select-current-option'));
         */
        static addIcon(container) {
            const icon = document.createElement('span');
            icon.className = 'icon';
            icon.innerHTML = AppSvg.inline(arrowIconHtml);
            container.append(icon);
            return icon;
        }

        /**
         * @function AppSelect.itemInnerTemplate
         * @memberof components
         * @desc Генерирует шаблон внутреннего контента элемента выпадающего списка.
         * @param {string} [label] - Текст элемента списка.
         * @param {Object} [data] - Дополнительные атрибуты элемента.
         * @param {string} [type='item'] - Тип элемента.
         * @returns {string} Возвращает HTML-строку внутреннего контента элемента выпадающего списка.
         */
        static itemInnerTemplate(label, data, type = 'item') {
            let contentString = (label === false) ? '' : `<div class="${blockClass}__${type}-label">${label}</div>`;
            if (data) {
                const innerClass = `${blockClass}__${type}-inner`;
                Object.keys(data).forEach(dataAttr => {
                    if (dataAttr === 'disabled') return;
                    contentString += `<div class="${innerClass} -${dataAttr}">${data[dataAttr]}</div>`;
                });
            }

            return contentString;
        }
    });

    addModule(moduleName, AppSelect);

    //Инициализировать элементы по data-атрибуту
    document.querySelectorAll('[data-select]').forEach(el => new AppSelect(el));

    return AppSelect;
};

export default selectInit;
export {selectInit};