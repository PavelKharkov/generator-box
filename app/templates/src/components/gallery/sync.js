import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'gallery';

const transitionDuration = 400;
const previewsBreakpointLg = util.media.lg;

//Селекторы и классы
const verticalClass = '-vertical';
const singleClass = '-single';
const sliderSelector = `.${moduleName}-slider__inner`;
const itemSelector = `.${moduleName}-slider__item`;
const previewsSelector = `.${moduleName}__previews`;
const previewsInnerSelector = `.${moduleName}-previews__inner`;
const previewsButtonSelector = `.${moduleName}-previews-item__button`;
const previewsItemSelector = `.${moduleName}-previews__item`;
const initializedClass = `${moduleName}-initialized`;
const moreClass = '-more';

//Опции слайдера по умолчанию
const defaultOptionsSlider = {};
const defaultPluginOptionsSlider = {
    slidesPerView: 1,

    init: false,
    grabCursor: true,
    watchOverflow: true,
    lazy: true,

    autoplay: {
        delay: 4000
    },

    effect: 'fade'
};

//Опции слайдера миниатюр по умолчанию
const defaultItems = {
    0: 3,
    [previewsBreakpointLg]: 5
};
const defaultOptionsPreviews = {};
const defaultPluginOptionsPreviews = {
    slidesPerView: defaultItems[0],

    spaceBetween: 8,

    breakpoints: {
        [previewsBreakpointLg]: {
            slidesPerView: defaultItems[previewsBreakpointLg]
        }
    }
};

//Опции события перехода по умолчанию
const defaultGotoOptions = {
    duration: transitionDuration
};

const initGallery = AppSlider => {
    /**
     * @class AppGallery
     * @memberof components
     * @requires components#AppPopupGallery
     * @requires components#AppSlider
     * @requires components#AppUrl
     * @requires layout#AppWindow
     * @classdesc Модуль галерей.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент галереи.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {Object} [options.slider] - Опции слайдера галереи.
     * @param {Object} [options.previews] - Опции слайдера миниатюр галереи.
     * @param {Object} [options.goto] - Опции события переключения слайдов.
     * @param {boolean} [options.vertical] - Показывать ли миниатюры вертикально.
     * @example
     * const galleryInstance = new app.Gallery(document.querySelector('.gallery-element'), {
     *     slider: {
     *         autoplay: {
     *             delay: 5000
     *         }
     *     },
     *     vertical: true
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="gallery" data-gallery>
     *     <div class="gallery__inner">
     *         <a class="gallery-slider__link" href="#" target="_blank" data-popup-gallery-item>
     *             <img src="" alt="">
     *         </a>
     *     </div>
     *     <div class="gallery__previews">
     *         <button type="button" class="gallery-previews__link">
     *             <img src="" alt="">
     *         </button>
     *     </div>
     * </div>
     *
     * <!--data-gallery - селектор по умолчанию-->
     *
     * @example <caption>Активация с помощью хеша</caption>
     * <!--HTML-->
     * <div id="gallery-item-1" data-hash-action="gallery">
     *     <img src="" alt="">
     * </div>
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-gallery-options='{"vertical": true}'></div>
     */
    const AppGallery = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const galleryOptionsData = el.dataset.galleryOptions;
            if (galleryOptionsData) {
                const dataOptions = util.stringToJSON(galleryOptionsData);
                if (!dataOptions) util.error('incorrect data-gallery-options format');
                util.extend(this.options, dataOptions);
            }

            if (el.classList.contains(verticalClass)) {
                this.options.vertical = true;
            } else if (this.options.vertical) {
                el.classList.add(verticalClass);
            }

            /**
             * @member {Object} AppGallery#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            /**
             * @function addInitializedClass
             * @desc Добавляет класс к галерее, указывающий на то, что галерея инициализирована.
             * @returns {undefined}
             * @ignore
             */
            const addInitializedClass = () => {
                el.classList.add(initializedClass);
            };

            //Сохранение элементов галереи
            const sliderElement = el.querySelector(sliderSelector);

            /**
             * @member {HTMLElement} AppGallery#sliderElement
             * @memberof components
             * @desc Элемент основного слайдера.
             * @readonly
             */
            Object.defineProperty(this, 'sliderElement', {
                enumerable: true,
                value: sliderElement
            });

            if (!sliderElement) {
                addInitializedClass();
                return;
            }

            const previewsElement = el.querySelector(previewsSelector);

            /**
             * @member {HTMLElement} AppGallery#previewsElement
             * @memberof components
             * @desc Контейнер блока миниатюр.
             * @readonly
             */
            Object.defineProperty(this, 'previewsElement', {
                enumerable: true,
                value: previewsElement
            });

            let previewsInner;

            /**
             * @member {HTMLElement} AppGallery#previewsInner
             * @memberof components
             * @desc Элемент слайдера миниатюр.
             * @readonly
             */
            Object.defineProperty(this, 'previewsInner', {
                enumerable: true,
                get: () => previewsInner
            });

            let previewButtons;

            /**
             * @member {Array.<HTMLElement>} AppGallery#previewButtons
             * @memberof components
             * @desc Коллекция элементов миниатюр.
             * @readonly
             */
            Object.defineProperty(this, 'previewButtons', {
                enumerable: true,
                get: () => previewButtons
            });

            if (previewsElement) {
                previewsInner = el.querySelector(previewsInnerSelector);

                //TODO:throw error if inner element not found

                previewButtons = [...previewsInner.querySelectorAll(previewsButtonSelector)];
            }

            //TODO:set active by slider
            const activeItem = previewsInner.querySelector(`${previewsButtonSelector}.active`);
            let index = previewsElement ? previewButtons.indexOf(activeItem) : 0;

            /**
             * @member {number} AppGallery#index
             * @memberof components
             * @desc Индекс текущего активного элемента галереи.
             * @readonly
             */
            Object.defineProperty(this, 'index', {
                enumerable: true,
                get: () => index
            });

            //Настройка опций
            params.slider = (typeof params.slider === 'undefined') ? {} : params.slider;
            if (params.slider) {
                util.defaultsDeep(params.slider, defaultOptionsSlider);

                if (!params.slider.pluginOptions ||
                    (util.isObject(params.slider.pluginOptions) && Object.keys(params.slider.pluginOptions).length === 0)) {
                    params.slider.pluginOptions = util.extend({}, defaultPluginOptionsSlider);
                }

                Object.assign(params.slider.pluginOptions, {
                    navigation: {
                        prevEl: el.querySelector('.gallery-slider-nav__arrow.-prev'),
                        nextEl: el.querySelector('.gallery-slider-nav__arrow.-next')
                    },
                    pagination: {
                        el: el.querySelector('.gallery-slider__pagination')
                    }
                });
            }

            //Настройка опций слайдера миниатюр
            params.previews = (typeof params.previews === 'undefined') ? {} : params.previews;
            if (params.previews) {
                util.defaultsDeep(params.previews, defaultOptionsPreviews);

                if (!params.previews.pluginOptions ||
                    (util.isObject(params.previews.pluginOptions) && Object.keys(params.previews.pluginOptions).length === 0)) {
                    params.previews.pluginOptions = util.extend({}, defaultPluginOptionsPreviews);
                }
            }

            params.goto = (typeof params.goto === 'undefined') ? {} : params.goto;
            if (params.goto) {
                util.defaultsDeep(params.goto, defaultGotoOptions);
            }

            el.querySelectorAll(itemSelector).forEach(item => {
                if (!item.modules) {
                    item.modules = {};
                }
                item.modules[moduleName] = this;
            });

            let popupGallery;

            /**
             * @member {Object} AppGallery#popupGallery
             * @memberof components
             * @desc Экземпляр [AppPopupGallery]{@link components.AppPopupGallery} для основного слайдера галереи.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'popupGallery', {
                enumerable: true,
                get: () => popupGallery
            });

            //События
            if (previewButtons) {
                previewButtons.forEach(button => {
                    button.addEventListener('click', () => {
                        if (popupGallery && button.classList.contains(moreClass)) {
                            popupGallery.show({
                                index: Number.parseInt(button.dataset.popupGalleryIndex, 10)
                            });
                            return;
                        }

                        const currentIndex = previewButtons.indexOf(button);
                        this.goto({
                            index: currentIndex
                        });
                    });
                });
            }

            //TODO:test
            /**
             * @function countPreviewsCenter
             * @desc Вычисляет центральный элемент в слайдере миниатюр. Если показывается нечётное количество миниатюр, то возвращает индекс наименьшей от центра.
             * @returns {number} Индекс центральной миниатюры.
             * @ignore
             */
            const countPreviewsCenter = () => {
                const previewsLength = (AppWindow.width() >= previewsBreakpointLg)
                    ? params.slider.responsive[previewsBreakpointLg].items
                    : (params.slider.items);
                const previewsCenter = Math.floor(previewsLength / 2);
                return previewsCenter;
            };

            let slider;

            /**
             * @member {Object} AppGallery#slider
             * @memberof components
             * @desc Экземпляр [AppSlider]{@link components.AppSlider} для основного слайдера галереи.
             * @readonly
             */
            Object.defineProperty(this, 'slider', {
                enumerable: true,
                get: () => slider
            });

            //Инициализация слайдеров
            if (params.slider) {
                slider = new AppSlider(sliderElement, params.slider);

                slider.on({
                    initialized() {
                        addInitializedClass();
                    },
                    slideChange: () => {
                        if (slider.instance.realIndex === index) return;

                        this.goto({
                            index: slider.instance.activeIndex
                        });
                    }
                });

                util.defer(() => {
                    const isInit = slider.init();
                    if (!isInit) addInitializedClass();
                });
            }

            let previewsSlider;

            /**
             * @member {Object} AppGallery#previewsSlider
             * @memberof components
             * @desc Экземпляр [AppSlider]{@link components.AppSlider} для слайдера миниатюр.
             * @readonly
             */
            Object.defineProperty(this, 'previewsSlider', {
                enumerable: true,
                get: () => previewsSlider
            });

            if (params.previews && !params.vertical) {
                previewsSlider = new AppSlider(previewsInner, params.previews);

                if (previewButtons.length === 1) {
                    previewsElement.classList.add(singleClass);
                }
            } else {
                previewsInner.removeAttribute('hidden');
            }

            this.on({
                /**
                 * @event AppGallery#goto
                 * @memberof components
                 * @desc Переходит на определённый слайд галереи.
                 * @param {Object} [options={}] - Опции.
                 * @param {number} [options.index] - Индекс элемента галереи, к которому надо перейти.
                 * @param {number} [options.duration] - Длительность перехода на другой слайд.
                 * @param {string} [options.id] - Идентификатор слайда, на который надо перейти.
                 * @param {string} [options.hash] - Идентификатор слайда, на который надо перейти. Данный параметр передаётся автоматически при открытии галереи с помощью хеша.
                 * @fires components.AppSlider#goto
                 * @returns {undefined}
                 * @example
                 * galleryInstance.on('goto', options => {
                 *     console.log(options); //Выведет опции события перехода
                 *     console.log('Произошел переход на другой слайд галереи');
                 * });
                 *
                 * console.log(galleryInstance.index); //0
                 * galleryInstance.goto({index: 1});
                 * console.log(galleryInstance.index); //1
                 */
                goto(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }
                    if (!slider) return;
                    if (params.goto) {
                        util.defaultsDeep(options, params.goto);
                    }

                    const slideId = options.id || options.hash;
                    //TODO:check slideId type

                    if (slideId) {
                        const nextItem = document.querySelector(`#${slideId}`);
                        if (!nextItem || !el.contains(nextItem)) return;
                        options.index = [...sliderElement.querySelectorAll(itemSelector)].indexOf(nextItem);
                    }

                    const previewsNextItem = previewsInner.querySelectorAll(previewsItemSelector)[options.index]; //TODO:use galleryIndex data attr
                    previewButtons.forEach(button => util.deactivate(button));
                    if (previewsNextItem) {
                        util.activate(previewsNextItem.querySelector(previewsButtonSelector));
                    }

                    if (params.vertical) {
                        const previewsHalfHeight = (previewsElement.offsetHeight / 2) - (previewsNextItem.offsetHeight / 2);

                        //TODO:change to transform
                        const topValue = Math.min(
                            0,
                            Math.max(
                                -(previewsInner.offsetHeight - previewsElement.offsetHeight),
                                (previewsHalfHeight - previewsNextItem.offsetTop)
                            )
                        );

                        previewsInner.style.top = `${topValue}px`;
                    }

                    ({index} = options);
                    slider.goto({
                        index,
                        duration: options.duration
                    });
                    if (previewsSlider) {
                        previewsSlider.goto({
                            index: options.index - countPreviewsCenter(),
                            duration: options.duration
                        });
                    }
                }
            });

            //Открывать большие изображения в попапе
            if (__GLOBAL_SETTINGS__.zoomGallery) {
                if (typeof sliderElement.dataset.popupZoomGallery !== 'undefined') {
                    require('Components/popup-zoom-gallery').default.then(() => {
                        popupGallery = sliderElement.modules && sliderElement.modules.popupZoomGallery;
                    });
                    emitInit('popupZoomGallery');
                }
            } else if (typeof sliderElement.dataset.popupGallery !== 'undefined') {
                require('Components/popup-gallery').default.then(() => {
                    popupGallery = sliderElement.modules && sliderElement.modules.popupGallery;
                });
                emitInit('popupGallery');
            }
        }
    });

    //Настройка возможности управления с помощью адресной строки
    require('Components/url').default.then(AppUrl => {
        AppUrl.registerAction({
            id: moduleName,
            selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
            action: `${moduleName}.goto`
        });
    });
    if (window.location.hash) emitInit('url');

    addModule(moduleName, AppGallery);

    //Инициализировать элементы по data-атрибуту
    document.querySelectorAll('[data-gallery]').forEach(el => new AppGallery(el));

    return AppGallery;
};

export default initGallery;
export {initGallery};