import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const galleryCallback = resolve => {
    const imports = [require('Components/slider').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "gallery" */ './sync.js'))
            .then(gallery => {
                chunks.gallery = true;

                const AppGallery = gallery.initGallery(...modules);
                resolve(AppGallery);
            });
    });

    emitInit('slider');
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.gallery) {
        galleryCallback(resolve);
        return;
    }

    onInit('gallery', () => {
        galleryCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('gallery');
    });
});

export default importFunc;