import Pikaday from 'Libs/pikaday';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'datepicker';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const DatepickerPlugin = Pikaday;

const arrowIconOptions = {
    id: 'arrow-line-thin',
    width: 8,
    height: 13
};

//Селекторы и классы
const wrapperSelector = '.form-group';
const datepickerInputSelector = '[data-date]';
const datepickerButtonSelector = '[data-datepicker]';
const datepickerSelector = '.pika-single';
const datepickerPrevSelector = '.pika-prev';
const datepickerNextSelector = '.pika-next';
const initializedClass = `${moduleName}-datepicker`;

//Опции по умолчанию
const defaultOptions = {
    mask: true,

    firstDay: 1
};

//Языковые опции календаря
const langOptions = {
    previousMonth: lang.previousMonth,
    nextMonth: lang.nextMonth,
    months: lang.months,
    weekdays: lang.weekdays,
    weekdaysShort: lang.weekdaysShort
};

const initDatepicker = (AppDate, AppSvg) => {
    //Инициализация разметки иконки стрелки
    const arrowIconHtml = AppSvg.inline(arrowIconOptions);

    const {currentDate} = AppDate;
    defaultOptions.minDate = currentDate.el;

    /**
     * @class AppDatepicker
     * @memberof components
     * @requires libs.pikaday
     * @requires components#AppDate
     * @requires components#AppMask
     * @requires components#AppSvg
     * @classdesc Модуль для работы с календарями.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент с датой.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {(boolean|Object)} [options.mask=true] - Опции для подключаемого модуля маски [AppMask]{@link components.AppMask}. Если true то используются опции [dateOptions]{@link components.AppMask.dateOptions}. Если false, то не подключать модуль маски.
     * @param {(Date|boolean)} [options.minDate=app.Date.currentDate.el] - Минимальная выбираемая дата. По умолчанию, сегодняшняя дата. Если false - то без минимальной даты.
     * @param {number} [options.firstDay=1] - Какой день недели показывать в календаре первым (0 - воскресенье, 1 - понедельник, …).
     * @param {HTMLInputElement} [options.input] - Элемент поля для ввода даты.
     * @param {HTMLInputElement} [options.field] - Аналог свойства input.
     * @param {HTMLElement} [options.container] - Элемент контейнера календаря.
     * @param {HTMLElement} [options.trigger] - Активирующий элемент для показа календаря. По умолчанию, ищет родительский элемент .form-group, внутри него ищет элемент [data-datepicker].
     * @param {HTMLInputElement} [options.hiddenInput] - Элемент скрытого поля ввода для помещения неотформатированной для пользователя даты. Если экземпляр модуля был проинициализирован на кнопке открытия календаря, то по умолчанию ищется скрытое поле ввода внутри кнопки.
     * @param {Function} [options.onDraw] - Функция, вызываемая после открытия календаря. По умолчанию — [defaultRender]{@link components.AppDatepicker#defaultRender}.
     * @param {Function} [options.onSelect] - Функция, вызываемая после выбора даты в календаре. По умолчанию — [defaultSelect]{@link components.AppDatepicker#defaultSelect}.
     * @param {*} [options...] - Другие опции плагина [Pikaday]{@link libs.pikaday}.
     * @example
     * const datepickerInstance = new app.Datepicker(document.querySelector('.date-input'), {
     *     minDate: false,
     *     mask: {
     *         pattern: '99.99.9999'
     *     }
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="form-group">
     *     <input type="date" data-date>
     *     <button type="button" data-detepicker>
     *         <input type="hidden"> <!--Скрытое поле для хранения выбранной даты-->
     *     </button>
     * </div>
     *
     * <!--
     * data-date — селектор по умолчанию
     * data-detepicker — селектор кнопки открытия календаря по умолчанию
     * -->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <input type="date" data-datepicker-options='{"lightColor": 200}'>
     */
    const AppDatepicker = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            const isHTMLElement = el instanceof HTMLElement;

            if (isHTMLElement) {
                const datepickerOptionsData = el.dataset.datepickerOptions;
                if (datepickerOptionsData) {
                    const dataOptions = util.stringToJSON(datepickerOptionsData);
                    if (!dataOptions) util.error('incorrect data-datepicker-options format');
                    util.extend(this.options, dataOptions);
                }
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppDropdown#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            /* eslint-disable prefer-const */
            let value;
            let input;
            let hiddenInput;
            let container;
            let instance;
            /* eslint-enable prefer-const */

            //Вспомогательные функции
            const instanceDate = () => {
                return instance._d; /* eslint-disable-line no-underscore-dangle */
            };

            const instanceOptions = () => {
                return instance._o; /* eslint-disable-line no-underscore-dangle */
            };

            //Функции по умолчанию
            /**
             * @function defaultRender
             * @desc Функция обратного вызова по умолчанию вызываемая после отображения календаря.
             * @returns {undefined}
             * @ignore
             */
            const defaultRender = () => {
                const currentContainer = container || document.querySelector(datepickerSelector);
                if (!currentContainer) return;
                const navButtons = currentContainer.querySelectorAll(`${datepickerPrevSelector}, ${datepickerNextSelector}`);
                const iconEl = document.createElement('span');
                iconEl.className = 'icon';
                iconEl.innerHTML = arrowIconHtml;

                navButtons.forEach(button => {
                    button.append(iconEl.cloneNode(true));
                });
            };

            /**
             * @function AppDatepicker#defaultRender
             * @memberof components
             * @desc Функция обратного вызова по умолчанию вызываемая после отображения календаря.
             * @returns {undefined}
             * @readonly
             */
            Object.defineProperty(this, 'defaultRender', {
                enumerable: true,
                value: defaultRender
            });

            let disabledDefaultRender = false;

            /**
             * @function AppDatepicker#disableDefaultRender
             * @memberof components
             * @desc Отключает функцию отображения календаря по умолчанию.
             * @returns {undefined}
             * @readonly
             * @example
             * datepickerInstance.disableDefaultRender(); //Функция отображения календаря больше не будет запускаться
             */
            Object.defineProperty(this, 'disableDefaultRender', {
                enumerable: true,
                value() {
                    disabledDefaultRender = true;
                }
            });

            /**
             * @function defaultSelect
             * @desc Функция обратного вызова по умолчанию вызываемая после выбора даты в календаре.
             * @returns {undefined}
             * @ignore
             */
            const defaultSelect = () => {
                if (!input) return;
                value = instanceDate();
                if (typeof value === 'undefined') return;

                if (hiddenInput) {
                    const formattedDate = AppDate.formatDate(value);
                    hiddenInput.value = formattedDate;
                    hiddenInput.setAttribute('value', formattedDate);
                }

                const shownDate = AppDate.shownDate(value);
                input.value = shownDate;
                input.setAttribute('value', shownDate);
            };

            /**
             * @function AppDatepicker#defaultSelect
             * @memberof components
             * @desc Функция обратного вызова по умолчанию вызываемая после выбора даты в календаре.
             * @returns {undefined}
             * @readonly
             */
            Object.defineProperty(this, 'defaultSelect', {
                enumerable: true,
                value: defaultSelect
            });

            let disabledDefaultSelect = false;

            /**
             * @function AppDatepicker#disableDefaultSelect
             * @memberof components
             * @desc Отключает функцию обратного вызова после отображения календаря по умолчанию.
             * @returns {undefined}
             * @readonly
             * @example
             * datepickerInstance.disableDefaultSelect(); //Функция обратного вызова после отображения календаря больше не будет запускаться
             */
            Object.defineProperty(this, 'disableDefaultSelect', {
                enumerable: true,
                value() {
                    disabledDefaultSelect = true;
                }
            });

            /**
             * @function AppDatepicker#instanceDate
             * @memberof components
             * @desc Возвращает выбранную в календаре дату.
             * @returns {Date} Выбранная в календаре дата.
             * @readonly
             * @example
             * const datepickerInstance = new app.Datepicker(document.querySelector('.date-input'));
             * console.log(datepickerInstance.instanceDate());
             * //Если в календаре была выбрана дата, то выведет объект даты.
             * //Если в календаре не была выведена дата, то выведет undefined
             */
            Object.defineProperty(this, 'instanceDate', {
                enumerable: true,
                value: instanceDate
            });

            /**
             * @function AppDatepicker#instanceOptions
             * @memberof components
             * @desc Возвращает опции плагина календаря.
             * @returns {Object} Опции календаря.
             * @readonly
             * @example
             * console.log(datepickerInstance.instanceOptions()); //Выведет опции плагина календаря
             */
            Object.defineProperty(this, 'instanceOptions', {
                enumerable: true,
                value: instanceOptions
            });

            //TODO:isShown
            //let isShown = false;

            let isDatepickerButton;
            let trigger;

            const onDraw = params.onDraw;
            params.onDraw = () => {
                if (!disabledDefaultRender) defaultRender();
                if (typeof onDraw === 'function') onDraw();
            };

            const onSelect = params.onSelect;
            params.onSelect = () => {
                if (!disabledDefaultSelect) defaultSelect();
                if (typeof onSelect === 'function') onSelect();
            };

            if (isHTMLElement) {
                const datepickerWrapper = el.closest(wrapperSelector);
                isDatepickerButton = typeof el.dataset.datepicker !== 'undefined';
                if (el.tagName === 'INPUT') {
                    params.input = params.input || params.field || el;
                    delete params.field;
                } else if (isDatepickerButton) {
                    params.input = params.input || params.field || datepickerWrapper.querySelector(datepickerInputSelector);
                    delete params.field;
                } else {
                    params.container = params.container || el;
                }

                trigger = !isDatepickerButton && datepickerWrapper && datepickerWrapper.querySelector(datepickerButtonSelector);
            }

            if (!(params.minDate instanceof Date) && (params.minDate !== false)) {
                util.typeError(params.minDate, 'params.minDate', 'date object or false');
            }
            if (!Number.isFinite(params.firstDay)) {
                util.typeError(params.firstDay, 'params.firstDay', 'number');
            }
            if (params.input && !(params.input instanceof HTMLInputElement)) {
                util.typeError(params.input, 'params.input', 'instance of HTMLInputElement');
            }
            if (params.trigger && !(params.trigger instanceof HTMLElement)) {
                util.typeError(params.trigger, 'params.trigger', 'instance of HTMLElement');
            }
            if (params.hiddenInput && !(params.hiddenInput instanceof HTMLInputElement)) {
                util.typeError(params.hiddenInput, 'params.hiddenInput', 'instance of HTMLInputElement');
            }
            if (params.container && !(params.container instanceof HTMLElement)) {
                util.typeError(params.container, 'params.container', 'instance of HTMLElement');
            }

            if (trigger) {
                //Добавление ссылки на экземпляр модуля элементу
                if (!trigger.modules) {
                    trigger.modules = {};
                }
                trigger.modules[moduleName] = this;

                params.trigger = params.trigger || trigger;
            }

            /**
             * @member {HTMLElement} AppDatepicker#trigger
             * @memberof components
             * @desc Элемент кнопки открытия календаря.
             * @readonly
             */
            Object.defineProperty(this, 'trigger', {
                enumerable: true,
                value: trigger
            });

            hiddenInput = params.hiddenInput || (trigger && trigger.querySelector('input[type="hidden"]'));

            /**
             * @member {HTMLElement} AppDatepicker#hiddenInput
             * @memberof components
             * @desc Элемент скрытого поля ввода для помещения неотформатированной для пользователя даты.
             * @readonly
             */
            Object.defineProperty(this, 'hiddenInput', {
                enumerable: true,
                value: hiddenInput
            });
            if (hiddenInput) {
                if (!hiddenInput.modules) {
                    hiddenInput.modules = {};
                }
                hiddenInput.modules[moduleName] = this;
            }

            const pluginOptions = {
                i18n: langOptions,
                field: params.input
            };
            util.defaultsDeep(pluginOptions, params);

            const inputValue = params.input && params.input.value;
            instance = new DatepickerPlugin(pluginOptions);
            if (inputValue) {
                const defaultDate = AppDate.getDate(inputValue);
                instance.setDate(AppDate.stringDate(defaultDate), true);
                if (hiddenInput) {
                    const formattedDate = AppDate.formatDate(defaultDate);
                    hiddenInput.value = formattedDate;
                    hiddenInput.setAttribute('value', formattedDate);
                }
            }

            /**
             * @member {Object} AppDatepicker#instance
             * @memberof components
             * @desc Экземпляр плагина календаря [Pikaday]{@link libs.pikaday}.
             * @readonly
             */
            Object.defineProperty(this, 'instance', {
                enumerable: true,
                value: instance
            });

            const datepicker = instance.el;
            if (!datepicker.modules) {
                datepicker.modules = {};
            }
            datepicker.modules[moduleName] = this;

            /**
             * @member {HTMLElement} AppDatepicker#datepicker
             * @memberof components
             * @desc Элемент календаря.
             * @readonly
             */
            Object.defineProperty(this, 'datepicker', {
                enumerable: true,
                value: datepicker
            });

            container = instanceOptions().container;
            if (container) {
                if (!container.modules) {
                    container.modules = {};
                }
                container.modules[moduleName] = this;
            }

            /**
             * @member {HTMLElement} AppDatepicker#container
             * @memberof components
             * @desc Элемент контейнера календаря.
             * @readonly
             */
            Object.defineProperty(this, 'container', {
                enumerable: true,
                value: container
            });

            input = instanceOptions().field;

            /**
             * @member {HTMLElement} AppDatepicker#input
             * @memberof components
             * @desc Элемент поля ввода, соответствующий календарю.
             * @readonly
             */
            Object.defineProperty(this, 'input', {
                enumerable: true,
                value: input
            });

            let mask;

            /**
             * @member {Object} AppDatepicker#mask
             * @memberof components
             * @desc Экземпляр [AppMask]{@link components.AppMask} для маски элемента. Использует опции маски [dateOptions]{@link components.AppMask.dateOptions}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'mask', {
                enumerable: true,
                get: () => mask
            });

            if (input) {
                if (!input.modules) {
                    input.modules = {};
                }
                input.modules[moduleName] = this;

                input.addEventListener('blur', () => {
                    value = AppDate.getDate(input.value);

                    if (hiddenInput) {
                        const shownDate = AppDate.shownDate(value);
                        input.value = shownDate;
                        input.setAttribute('value', shownDate);

                        const formattedDate = AppDate.formatDate(value);
                        hiddenInput.value = formattedDate;
                        hiddenInput.setAttribute('value', formattedDate);
                    }
                });

                const instanceDateValue = instanceDate();
                if (instanceDateValue) {
                    value = instanceDateValue;
                    if (input.value) {
                        const shownDate = AppDate.shownDate(value);
                        input.value = shownDate;
                        input.setAttribute('value', shownDate);
                    }
                }

                if (params.mask) {
                    const maskRequire = require('Components/mask');
                    maskRequire.default.then(AppMask => {
                        const maskOptions = util.isObject(params.mask) ? params.mask : AppMask.dateOptions;
                        mask = new AppMask(input, maskOptions);
                    });
                    maskRequire.maskTrigger(input);
                }

                //Не показывать календарь после ручного ввода символов в поле ввода
                if (trigger) {
                    input.addEventListener('blur', () => {
                        document.querySelector(datepickerSelector).hidden = true;
                    });
                    trigger.addEventListener('click', () => {
                        document.querySelector(datepickerSelector).removeAttribute('hidden');
                    });
                }
            }

            /**
             * @member {Date} AppDatepicker#value
             * @memberof components
             * @desc Текущая выбранная дата.
             * @readonly
             */
            Object.defineProperty(this, 'value', {
                enumerable: true,
                get: () => value
            });

            //TODO:check minDate

            const initialValue = value;

            /**
             * @member {(Date|undefined)} AppDatepicker#initialValue
             * @memberof components
             * @desc Значение, установленное в поле ввода календаря до его инициализации.
             * @readonly
             */
            Object.defineProperty(this, 'initialValue', {
                enumerable: true,
                value: initialValue
            });

            //События
            //this.on({
            //    show() {
            //        if (isShown) return;
            //
            //        instance.show();
            //
            //        isShown = true;
            //    }
            //});

            //TODO:add methods, add render & select events

            if (trigger) {
                trigger.classList.add(initializedClass);

                if (typeof trigger.dataset.datepickerTriggered !== 'undefined') {
                    delete trigger.dataset.datepickerTriggered;
                    instance.show();
                }
            }
        }
    });

    addModule(moduleName, AppDatepicker);

    //Инициализировать элементы по data-атрибуту
    document.querySelectorAll('[data-date], [data-datepicker]').forEach(el => {
        if (el.classList.contains(initializedClass)) return;
        new AppDatepicker(el); /* eslint-disable-line no-new */
    });

    return AppDatepicker;
};

export default initDatepicker;
export {initDatepicker};