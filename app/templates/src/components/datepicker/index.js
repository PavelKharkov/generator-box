import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let datepikcerTriggered;

const datepickerCallback = resolve => {
    const imports = [
        require('Components/date').default,
        require('Components/svg').default
    ];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "dates" */ './sync.js'))
            .then(datepicker => {
                chunks.dates = true;
                datepikcerTriggered = true;

                const AppDatepicker = datepicker.initDatepicker(...modules);
                if (resolve) resolve(AppDatepicker);
            });
    });

    emitInit('date', 'svg');
};

const initDatepicker = event => {
    if (datepikcerTriggered) return;

    if (event.currentTarget.dataset.datepicker) {
        event.currentTarget.dataset.datepickerTriggered = '';
    }

    emitInit('datepicker');
};

const datepickerTrigger = (datepickerItems, datepickerTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (datepickerItems instanceof Node) ? [datepickerItems] : datepickerItems;
    if (items.length === 0) return;

    items.forEach(item => {
        datepickerTriggers.forEach(trigger => {
            item.addEventListener(trigger, initDatepicker, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.dates) {
        datepickerCallback(resolve);
        return;
    }

    onInit('datepicker', () => {
        datepickerCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('datepicker');
    });

    const datepickerItems = document.querySelectorAll('[data-date], [data-datepicker]');
    datepickerTrigger(datepickerItems);
});

export default importFunc;
export {datepickerTrigger};