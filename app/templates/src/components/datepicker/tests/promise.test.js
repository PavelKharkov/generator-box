import 'Base/scripts/test';

describe('AppDatepicker', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const datepickerPromise = require('..').default;

        expect(datepickerPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в функцию инициализации класса', async () => {
        await new Promise(done => {
            return require('..').default.then(AppDatepickerTest => {
                expect(AppDatepickerTest).toBeInstanceOf(Function);
                done();
            });
        });
    });
});