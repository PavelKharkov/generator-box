import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';

import AppDate from 'Components/date/sync.js';
import AppSvg from 'Components/svg/sync.js';
import datepickerInit from '../sync.js';

//Инициализация тестового элемента с атрибутом data-date
const datepickerWithDataEl = document.createElement('div');
datepickerWithDataEl.dataset.date = '';
document.body.append(datepickerWithDataEl);

const AppDatepicker = datepickerInit(AppDate, AppSvg);

describe('AppDatepicker', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalDatepicker = window.app.Datepicker;

        expect(globalDatepicker.prototype).toBeInstanceOf(Module);
    });

    const dataTypesAssertions = [];
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.datepicker;
            return new AppDatepicker(value);
        },
        dataTypesAssertions,
        defaultValue: [isInstance, AppDatepicker],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с переданным значением'
    });

    test('должен сохраняться под именем \'datepicker\' в свойстве \'modules\' у элементов', () => {
        const datepickerEl = document.createElement('div');
        document.body.append(datepickerEl);

        new AppDatepicker(datepickerEl); /* eslint-disable-line no-new */
        const datepickerInstanceProp = datepickerEl.modules.datepicker;

        expect(datepickerInstanceProp).toBeInstanceOf(AppDatepicker);
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-date\'', () => {
        const datepickerInstance = datepickerWithDataEl.modules.datepicker;

        expect(datepickerInstance).toBeInstanceOf(AppDatepicker);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const datepickerPropsArray = [
            'container',
            'datepicker',
            'input',
            'hiddenInput',
            'instance',
            'value',
            'initialValue',
            'defaultRender',
            'defaultSelect',
            'instanceDate',
            'instanceOptions'
        ];
        test.each(datepickerPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            expect(Object.prototype.hasOwnProperty.call(datepickerInstance, property)).toBe(true);
        });
    });

    describe('опции по умолчанию', () => {
        const datepickerOptionsMap = [
            ['mask', true],
            ['firstDay', 1]
        ];
        test.each(datepickerOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceOptions = datepickerInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    describe('[data-datepicker-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-datepicker-options\'', () => {
            const datepickerEl = document.createElement('div');
            datepickerEl.dataset.datepickerOptions = '{"firstDay": 2}';
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceOptions = datepickerInstance.options;

            expect(instanceOptions.firstDay).toBe(2);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-datepicker-options\'', () => {
            const datepickerEl = document.createElement('div');
            datepickerEl.dataset.datepickerOptions = 'true';
            document.body.append(datepickerEl);

            const datepickerInstance = (() => new AppDatepicker(datepickerEl));

            expect(datepickerInstance).toThrow('incorrect data-datepicker-options format');
        });
    });

    describe('опции', () => {
        describe('minDate', () => {
            const minDateTypesAssertions = [
                'false',
                'Date',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        minDate: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: minDateTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.minDate\' must be a date object or false'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'minDate\''
            });
        });

        describe('firstDay', () => {
            const firstDayTypesAssertions = [
                'zero',
                'number',
                'float number',
                'negative number',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        firstDay: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: firstDayTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.firstDay\' must be a number'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'firstDay\''
            });
        });

        describe('mask', () => {
            test('должен инициализировать модуль маски при любом положительном значении опции', async () => {
                const datepickerEl = document.createElement('input');
                document.body.append(datepickerEl);

                /* eslint-disable-next-line no-new */
                new AppDatepicker(datepickerEl, {
                    mask: 'truthful'
                });

                expect(datepickerEl.modules.mask).toBeUndefined();
                await new Promise(done => {
                    return require('Components/mask').default.then(AppMask => {
                        const inputMaskProp = datepickerEl.modules.mask;
                        expect(inputMaskProp).toBeInstanceOf(AppMask);
                        done();
                    });
                });
            });
        });

        describe('input', () => {
            const inputTypesAssertions = [
                'false',
                'zero',
                'NaN',
                'empty string',
                'null',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        input: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: inputTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.input\' must be a instance of HTMLInputElement'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'input\''
            });
        });

        describe('field', () => {
            const inputTypesAssertions = [
                'false',
                'zero',
                'NaN',
                'empty string',
                'null',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        input: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: inputTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.input\' must be a instance of HTMLInputElement'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'field\''
            });

            test('должен передавать значение свойству \'input\', если это свойство не задано', () => {
                const datepickerEl = document.createElement('input');
                document.body.append(datepickerEl);

                const datepickerInputEl = document.createElement('input');
                document.body.append(datepickerInputEl);

                const datepickerInstance = new AppDatepicker(datepickerEl, {
                    field: datepickerInputEl
                });

                expect(datepickerInstance.options.input).toBe(datepickerInputEl);
            });

            test('не должен передавать значение свойству \'input\', если это свойство задано', () => {
                const datepickerEl = document.createElement('input');
                document.body.append(datepickerEl);

                const datepickerInputEl = document.createElement('input');
                document.body.append(datepickerInputEl);

                const datepickerFieldEl = document.createElement('input');
                document.body.append(datepickerFieldEl);

                const datepickerInstance = new AppDatepicker(datepickerEl, {
                    input: datepickerInputEl,
                    field: datepickerFieldEl
                });

                expect(datepickerInstance.options.input).toBe(datepickerInputEl);
            });
        });

        describe('container', () => {
            const triggerTypesAssertions = [
                'false',
                'zero',
                'NaN',
                'empty string',
                'HTMLElement',
                'null',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        container: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: triggerTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.container\' must be a instance of HTMLElement'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'container\''
            });
        });

        describe('trigger', () => {
            const triggerTypesAssertions = [
                'false',
                'zero',
                'NaN',
                'empty string',
                'HTMLElement',
                'null',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        trigger: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: triggerTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.trigger\' must be a instance of HTMLElement'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'trigger\''
            });
        });

        describe('hiddenInput', () => {
            const hiddenInputTypesAssertions = [
                'false',
                'zero',
                'NaN',
                'empty string',
                'null',
                'undefined'
            ].map(el => [el, [isInstance, AppDatepicker]]);
            dataTypesCheck({
                checkFunction(setupResult, value) {
                    if (document.modules) delete document.modules.datepicker;
                    const datepickerInstance = new AppDatepicker(null, {
                        hiddenInput: value
                    });
                    return datepickerInstance;
                },
                dataTypesAssertions: hiddenInputTypesAssertions,
                defaultValue: [throwsError, 'parameter \'options.hiddenInput\' must be a instance of HTMLInputElement'],
                describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDatepicker\' с опцией \'hiddenInput\''
            });
        });

        describe('onDraw', () => {
            test('вызов функции должен вызывать метод \'defaultRender\'', () => {
                const datepickerEl = document.createElement('input');
                document.body.append(datepickerEl);

                const calendarEl = document.createElement('div');
                const prevEl = document.createElement('div');
                prevEl.classList.add('pika-prev');
                calendarEl.append(prevEl);
                document.body.append(calendarEl);

                const datepickerInstance = new AppDatepicker(datepickerEl, {
                    container: calendarEl
                });
                datepickerInstance.options.onDraw();

                expect(prevEl.querySelector('svg')).toBeInstanceOf(SVGSVGElement);
            });
        });

        describe('onSelect', () => {
            test('вызов функции должен вызывать метод \'defaultSelect\'', () => {
                const datepickerEl = document.createElement('input');
                datepickerEl.value = '01.01.2020';
                document.body.append(datepickerEl);

                const hiddenInputEl = document.createElement('input');
                document.body.append(hiddenInputEl);

                const datepickerInstance = new AppDatepicker(datepickerEl, {
                    minDate: false,
                    hiddenInput: hiddenInputEl
                });
                datepickerInstance.options.onSelect();

                expect(hiddenInputEl.value).toBe('1.1.2020');
            });
        });
    });

    describe('.mask', () => {
        test('должен инициализировать модуль маски при наличии элемента \'input\' экземпляра', async () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            new AppDatepicker(datepickerEl); /* eslint-disable-line no-new */

            expect(datepickerEl.modules.mask).toBeUndefined();
            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    const inputMaskProp = datepickerEl.modules.mask;
                    expect(inputMaskProp).toBeInstanceOf(AppMask);
                    done();
                });
            });
        });

        test('должен сохранять ссылку на модуль маски в свойстве', async () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    expect(datepickerInstance.mask).toBeInstanceOf(AppMask);
                    done();
                });
            });
        });

        test('должен инициализировать модуль маски с корректными опциями', async () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    const dateOptions = AppMask.dateOptions;

                    expect(datepickerInstance.mask.options).toMatchObject(dateOptions);
                    done();
                });
            });
        });
    });

    describe('.defaultRender()', () => {
        test('должен возвращать undefined', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            const renderResult = datepickerInstance.defaultRender();

            expect(renderResult).toBeUndefined();
        });

        test('должен корректно устанавливать спрайты иконок на кнопках навигации', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const calendarEl = document.createElement('div');
            const prevEl = document.createElement('div');
            prevEl.classList.add('pika-prev');
            calendarEl.append(prevEl);
            const nextEl = document.createElement('div');
            nextEl.classList.add('pika-next');
            calendarEl.append(nextEl);
            document.body.append(calendarEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                container: calendarEl
            });

            datepickerInstance.defaultRender();

            expect(prevEl.querySelector('svg')).toBeInstanceOf(SVGSVGElement);
            expect(nextEl.querySelector('svg')).toBeInstanceOf(SVGSVGElement);
        });
    });

    describe('.defaultSelect()', () => {
        test('должен возвращать undefined', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            const selectResult = datepickerInstance.defaultSelect();

            expect(selectResult).toBeUndefined();
        });

        //TODO:hiddenInput change
    });

    describe('hiddenInput', () => {
        test('должно получать значение из опции \'hiddenInput\'', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const hiddenInputEl = document.createElement('input');
            document.body.append(hiddenInputEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                hiddenInput: hiddenInputEl
            });

            const hiddenInput = datepickerInstance.hiddenInput;

            expect(hiddenInput).toBe(hiddenInputEl);
        });

        test('должно искать скрытое поле внутри кнопки открытия календаря при отрицательном значении опции \'hiddenInput\'', () => {
            const formGroupEl = document.createElement('div');
            formGroupEl.classList.add('form-group');
            document.body.append(formGroupEl);

            const datepickerEl = document.createElement('input');
            formGroupEl.append(datepickerEl);

            const datepickerButtonEl = document.createElement('button');
            datepickerButtonEl.dataset.datepicker = '';
            formGroupEl.append(datepickerButtonEl);

            const hiddenInputEl = document.createElement('input');
            hiddenInputEl.type = 'hidden';
            datepickerButtonEl.append(hiddenInputEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                hiddenInput: null
            });

            const hiddenInput = datepickerInstance.hiddenInput;

            expect(hiddenInput).toBe(hiddenInputEl);
        });

        test('должно сохранять ссылку на экземпляр модуля на элементе скрытого поля', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const hiddenInputEl = document.createElement('input');
            document.body.append(hiddenInputEl);

            /* eslint-disable-next-line no-new */
            new AppDatepicker(datepickerEl, {
                hiddenInput: hiddenInputEl
            });

            const datepickerInstanceProp = hiddenInputEl.modules.datepicker;

            expect(datepickerInstanceProp).toBeInstanceOf(AppDatepicker);
        });
    });

    describe('instance', () => {
        const instancePropsArray = [
            'el',
            '_o'
        ];
        test.each(instancePropsArray)('должно содержать свойство \'%s\'', property => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);

            const instance = datepickerInstance.instance;
            const currentProp = instance[property];

            expect(currentProp).toBeDefined();
        });

        test('должно содержать свойство \'_d\', если в календаре выбрана дата', () => {
            const datepickerEl = document.createElement('input');
            datepickerEl.value = '01.01.2020';
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                minDate: false
            });

            const instance = datepickerInstance.instance;
            const dateProp = instance._d; /* eslint-disable-line no-underscore-dangle */

            expect(dateProp).toBeDefined();
        });
    });

    describe('datepicker', () => {
        test('должно содержать ссылку на элемент календаря', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const datepickerProp = datepickerInstance.datepicker;

            expect(datepickerProp.classList.contains('pika-single')).toBe(true);
        });

        test('должно содержать ссылку на экземпляр модуля', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const datepickerProp = datepickerInstance.datepicker;

            const datepickerInstanceProp = datepickerProp.modules.datepicker;

            expect(datepickerInstanceProp).toBeInstanceOf(AppDatepicker);
        });
    });

    describe('container', () => {
        test('должно получать значение из опции \'container\'', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerContainerEl = document.createElement('div');
            document.body.append(datepickerContainerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                container: datepickerContainerEl
            });
            const containerProp = datepickerInstance.container;

            expect(containerProp).toBe(datepickerContainerEl);
        });

        test('должно содержать ссылку на контейнер календаря', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const containerProp = datepickerInstance.container;

            expect(containerProp).toBe(datepickerEl);
        });

        test('должно содержать ссылку на экземпляр модуля', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerContainerEl = document.createElement('div');
            document.body.append(datepickerContainerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                container: datepickerContainerEl
            });
            const containerProp = datepickerInstance.container;
            const datepickerInstanceProp = containerProp.modules.datepicker;

            expect(datepickerInstanceProp).toBeInstanceOf(AppDatepicker);
        });
    });

    describe('input', () => {
        test('должно получать значение из опции \'input\', даже при наличии опции \'field\'', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerFieldEl = document.createElement('input');
            document.body.append(datepickerFieldEl);

            const datepickerInputEl = document.createElement('input');
            document.body.append(datepickerInputEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                field: datepickerFieldEl,
                input: datepickerInputEl
            });
            const inputProp = datepickerInstance.input;

            expect(inputProp).toBe(datepickerInputEl);
        });

        test('должно получать значение из опции \'field\', если не указана опция \'input\'', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerFieldEl = document.createElement('input');
            document.body.append(datepickerFieldEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                field: datepickerFieldEl
            });
            const inputProp = datepickerInstance.input;

            expect(inputProp).toBe(datepickerFieldEl);
        });

        test('должно содержать ссылку на поле ввода даты', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const inputProp = datepickerInstance.input;

            expect(inputProp).toBe(datepickerEl);
        });

        test('должно содержать ссылку на экземпляр модуля', () => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInputEl = document.createElement('input');
            document.body.append(datepickerInputEl);

            const datepickerInstance = new AppDatepicker(datepickerEl, {
                input: datepickerInputEl
            });
            const inputProp = datepickerInstance.input;
            const datepickerInstanceProp = inputProp.modules.datepicker;

            expect(datepickerInstanceProp).toBeInstanceOf(AppDatepicker);
        });
    });

    describe('value', () => {
        test('должно быть \'undefined\' при инициализации экземпляра, если нет начальной даты', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const valueProp = datepickerInstance.value;

            expect(valueProp).toBeUndefined();
        });

        test('должно содержать выбранную дату при инициализации экземпляра', () => {
            const datepickerEl = document.createElement('input');
            datepickerEl.value = '01.01.2020';
            document.body.append(datepickerEl);

            const currentDate = new Date(2020, 0, 1);
            const datepickerInstance = new AppDatepicker(datepickerEl, {
                minDate: false
            });
            const valueProp = datepickerInstance.value;

            expect(valueProp.getTime()).toBe(currentDate.getTime());
        });

        test('должно выводить ошибку при попытке изменить значение напрямую', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const value = (() => {
                datepickerInstance.value = new Date();
            });

            expect(value).toThrow('Cannot set property value of #<ImmutableClass> which has only a getter');
        });

        test('должно содержать выбранную дату при смене даты в календаре', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const currentDate = new Date(2020, 0, 1);
            const datepickerInstance = new AppDatepicker(datepickerEl, {
                minDate: false
            });

            datepickerInstance.instance.setDate('2020-01-01');
            const valueProp = datepickerInstance.value;

            expect(valueProp.getTime()).toBe(currentDate.getTime());
        });
    });

    describe('initialValue', () => {
        test('должно быть \'undefined\' при инициализации экземпляра, если нет начальной даты', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const valueProp = datepickerInstance.initialValue;

            expect(valueProp).toBeUndefined();
        });

        test('должно содержать выбранную дату при инициализации экземпляра', () => {
            const datepickerEl = document.createElement('input');
            datepickerEl.value = '01.01.2020';
            document.body.append(datepickerEl);

            const currentDate = new Date(2020, 0, 1);
            const datepickerInstance = new AppDatepicker(datepickerEl, {
                minDate: false
            });
            const valueProp = datepickerInstance.initialValue;

            expect(valueProp.getTime()).toBe(currentDate.getTime());
        });

        test('должно выводить ошибку при попытке изменить значение напрямую', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const initialValue = (() => {
                datepickerInstance.initialValue = new Date();
            });

            expect(initialValue).toThrow('Cannot assign to read only property \'initialValue\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.instanceDate', () => {
        test('должен возвращать выбранную в календаре дату', () => {
            const datepickerEl = document.createElement('input');
            datepickerEl.value = '01.01.2020';
            document.body.append(datepickerEl);

            const currentDate = new Date(2020, 0, 1);
            const datepickerInstance = new AppDatepicker(datepickerEl, {
                minDate: false
            });
            const instanceDate = datepickerInstance.instanceDate();

            expect(instanceDate.getTime()).toBe(currentDate.getTime());
        });

        test('должен возвращать \'undefined\', если начальная дата не выбрана', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceDate = datepickerInstance.instanceDate();

            expect(instanceDate).toBeUndefined();
        });
    });

    describe('.instanceOptions', () => {
        test('должен возвращать объект с настройками плагина', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceOptions = datepickerInstance.instanceOptions();

            expect(typeof instanceOptions).toBe('object');
        });

        const optionsArray = [
            'field',
            'bound',
            'ariaLabel',
            'position',
            'reposition',
            'format',
            'toString',
            'parse',
            'defaultDate',
            'setDefaultDate',
            'firstDay',
            'formatStrict',
            'minDate',
            'maxDate',
            'yearRange',
            'showWeekNumber',
            'pickWholeWeek',
            'minYear',
            'maxYear',
            'minMonth',
            'maxMonth',
            'startRange',
            'endRange',
            'isRTL',
            'yearSuffix',
            'showMonthAfterYear',
            'showDaysInNextAndPreviousMonths',
            'enableSelectionDaysInNextAndPreviousMonths',
            'numberOfMonths',
            'mainCalendar',
            'container',
            'blurFieldOnSelect',
            'i18n',
            'theme',
            'events',
            'onSelect',
            'onOpen',
            'onClose',
            'onDraw',
            'keyboardInput',
            'mask',
            'trigger',
            'disableWeekends',
            'disableDayFn'
        ];
        test.each(optionsArray)('должно возвращать объект, содержащий свойство \'%s\'', property => {
            const datepickerEl = document.createElement('div');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceOptions = datepickerInstance.instanceOptions();

            expect(Object.prototype.hasOwnProperty.call(instanceOptions, property)).toBe(true);
        });

        test('должен возвращать объект, содержащий свойство \'input\' при инициализации на поле ввода', () => {
            const datepickerEl = document.createElement('input');
            document.body.append(datepickerEl);

            const datepickerInstance = new AppDatepicker(datepickerEl);
            const instanceOptions = datepickerInstance.instanceOptions();
            const inputOption = instanceOptions.input;

            expect(inputOption).toBeDefined();
        });
    });

    //TODO:test defalutRender, defalutSelect, hiddenInput, instance, instanceOptions, instanceDate, input, mask, container, datepicker readonly, trigger
});