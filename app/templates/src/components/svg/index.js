import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const svgCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const AppSvg = modules.default;
            resolve(AppSvg);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        svgCallback(resolve);
        return;
    }

    onInit('svg', () => {
        svgCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('svg');
    });
});

export default importFunc;