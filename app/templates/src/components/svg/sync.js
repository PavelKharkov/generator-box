import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'svg';

const defaultOptions = {
    href: `${app.globalConfig.assetsPath}media/sprite.svg`
};

let svgPlugin;

if (__HAS_POLYFILLS__) {
    const svg4everybody = require('Libs/svg4everybody').default;
    svgPlugin = svg4everybody;

    //Опции плагина
    defaultOptions.nosvg = false;
}

/**
 * @function AppSvg
 * @namespace components.AppSvg
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для работы с svg.
 * @async
 */
const AppSvg = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppSvg
     * @returns {undefined}
     */
    function() {
        const moduleOptions = {};
        util.defaultsDeep(moduleOptions, defaultOptions);

        /**
         * @member {Object} AppSvg.params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this, moduleOptions);

        this.on({
            /**
             * @event AppSvg.inline
             * @memberof components
             * @desc Возвращает HTML-код svg-тега. Для добавления спрайта в общий файл спрайтов его нужно дополнительно подключить в HTML-шаблонах (Все спрайты, подключаемые через скрипты, должны добавляться на странице sitemap).
             * @param {Object} [options={}] - Опции переключения слайдов.
             * @param {number} [options.width] - Ширина элемента.
             * @param {number} [options.height] - Высота элемента.
             * @param {string} [options.id] - Идентификатор иконки в спрайте.
             * @param {string} [options.href='<app.globalConfig.assetsPath>media/sprite.svg'] - Ссылка на спрайт.
             * @returns {string} Возвращает строку с элементом спрайта.
             * @example
             * const iconConfig = {
             *     id: 'arrow',
             *     width: 11,
             *     height: 9
             * };
             * const iconHtml = app.svg.inline(iconConfig); //Вернёт HTML-строку с иконкой
             */
            inline(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (!options.id) return '';

                util.defaultsDeep(options, params);

                let style = '';
                if (options.width || options.height) {
                    style += ' style="';
                    if (options.width) {
                        style += `width: ${options.width}px;`;
                    }
                    if (options.height) {
                        if (options.width) {
                            style += ' ';
                        }
                        style += `height: ${options.height}px;`;
                    }
                    style += '"';
                }

                const useTag = `<use xlink:href="${options.href}#sprite-${options.id}">`;
                return `<svg aria-hidden="true"${style}>${useTag}</svg>`;
            }
        });

        if (__HAS_POLYFILLS__) {
            //Инициализация svg-полифила для IE и Edge при загрузке
            svgPlugin(moduleOptions);
        }

        return moduleOptions;
    },

    moduleName
));

addModule(moduleName, AppSvg);

export default AppSvg;
export {AppSvg};