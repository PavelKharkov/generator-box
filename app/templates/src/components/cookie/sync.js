import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'cookie';

/**
 * @function decode
 * @desc Декодирует ключ cookie.
 * @param {string} key - Ключ cookie для декодирования.
 * @returns {string} Декодированное значение ключа.
 * @ignore
 */
const decodeKey = key => {
    return key.replace(/(%[\dA-Z]{2})+/g, decodeURIComponent);
};

//Добавляет или изменяет cookie
const setCookie = (key = util.required('key'), value = '', attributes = {}) => {
    if ((typeof key !== 'string') || !key) {
        util.typeError(key, 'key', 'non-empty string');
    }
    if (!util.isObject(attributes)) {
        util.typeError(attributes, 'attributes', 'plain object');
    }

    let encodedKey = key;
    let encodedValue = value;

    //Проверка, что в cookie необходимо сохранить json-объект
    const objectValue = util.attempt(() => JSON.stringify(value));
    if (!(objectValue instanceof Error) && /^[[{]/.test(objectValue)) {
        encodedValue = objectValue;
    }

    //Кодирование значений для сохранения
    encodedKey = encodeURIComponent(String(encodedKey))
        .replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
        .replaceAll('(', '%28')
        .replaceAll(')', '%29');
    encodedValue = encodeURIComponent(String(encodedValue))
        .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

    let cookieString = `${encodedKey}=${encodedValue}`;

    //Атрибуты cookie
    attributes.path = attributes.path || '/';

    if (typeof attributes.expires === 'number') {
        attributes.expires = new Date(Number(new Date().getTime() + (attributes.expires * 864e+5))).toUTCString();
    } else {
        const expiresDate = new Date(attributes.expires);
        attributes.expires = Number.isNaN(expiresDate.getTime()) ? '' : expiresDate.toUTCString();
    }

    Object.keys(attributes).forEach(attribute => {
        cookieString += `; ${attribute}`;

        if (attributes[attribute] === true) return;
        cookieString += `=${String(attributes[attribute]).split(';')[0]}`;
    });

    document.cookie = cookieString; /* eslint-disable-line unicorn/no-document-cookie */

    return cookieString;
};

//Получает cookie
const getCookie = (key, asObject) => {
    if ((typeof key !== 'string') || !key) {
        util.typeError(key, 'key', 'non-empty string');
    }

    let formattedCookie;
    const cookiesArray = document.cookie ? document.cookie.split('; ') : [];

    if (key) {
        const foundCookie = cookiesArray.find(cookie => {
            return cookie.split('=')[0] === key;
        });
        if (foundCookie) {
            formattedCookie = decodeKey(foundCookie.split('=')[1]);
        }
        if (asObject) {
            const objectValue = util.stringToJSON(formattedCookie);
            formattedCookie = (objectValue === null) ? formattedCookie : objectValue;
        }
    } else {
        const foundCookies = {};
        cookiesArray.forEach(cookie => {
            const cookieArray = cookie.split('=');
            foundCookies[cookieArray[0]] = decodeKey(cookieArray[1]);
        });
        return foundCookies;
    }

    return formattedCookie;
};

//Удаляет cookie
const removeCookie = (key = util.required('key'), attributes = {}) => {
    if ((typeof key !== 'string') || !key) {
        util.typeError(key, 'key', 'non-empty string');
    }
    if (!util.isObject(attributes)) {
        util.typeError(attributes, 'attributes', 'plain object');
    }

    attributes.expires = -1;

    const cookieString = setCookie(key, '', attributes);
    return cookieString;
};

/**
 * @function AppCookie
 * @namespace components.AppCookie
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для работы с cookie.
 * @async
 */
const AppCookie = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppCookie
     * @returns {undefined}
     */
    function() {
        /**
         * @function components.AppCookie#set
         * @desc Добавляет или изменяет cookie.
         * @param {string} key - Ключ cookie.
         * @param {*} value - Значение cookie.
         * @param {Object} [attributes] - Объект с параметрами cookie.
         * @param {string} [attributes.path='/'] - Путь, где cookie будет активен.
         * @param {(number|Date|string)} [attributes.expires=''] - Указывает, когда cookie будет удалён. Переданное число обозначает количество дней, сколько cookie будет хранится. По умолчанию cookie хранится до конца сессии.
         * @param {string} [attributes.domain] - Домен, где cookie будет активен. По умолчанию, текущий домен.
         * @param {boolean} [attributes.secure] - Указывает, нужен ли https протокол для отображения cookie.
         * @param {string} [attributes.samesite] - Указывает, нужно ли ограничивать доступ к cookie.
         * @returns {string} Отформатированная строка, передаваемая в document.cookie.
         * @readonly
         * @example
         * //Установить cookie длительностью 7 дней на поддомене
         * app.cookie.set('name', 'value', {
         *     expires: 7,
         *     domain: 'subdomain.site.com'
         * });
         */
        Object.defineProperty(this, 'set', {
            enumerable: true,
            value: setCookie
        });

        /**
         * @function components.AppCookie#get
         * @desc Получает cookie.
         * @param {string} [key] - Ключ читаемого cookie.
         * @param {boolean} [asObject] - Отформатировать возвращаемое значение как объект.
         * @returns {(string|Object)} Значение прочитанного cookie.
         * @readonly
         * @example <caption>Получить cookie 'name'</caption>
         * const nameCookie = app.cookie.get('name'); //'value'
         *
         * @example <caption>Получить все cookie</caption>
         * const allCookies = app.cookie.get(); //{name: 'value'}
         *
         * @example <caption>Получить cookie 'name', если оно было сохранено как объект</caption>
         * const nameCookie = app.cookie.get('name', true); //{param: 'value'}
         */
        Object.defineProperty(this, 'get', {
            enumerable: true,
            value: getCookie
        });

        /**
         * @function components.AppCookie#remove
         * @desc Удаляет cookie.
         * @param {string} key - Ключ удаляемого cookie.
         * @param {Object} attributes - Атрибуты, по которым будет искаться удаляемый cookie. Атрибуты path и domain в точности должны соответствовать сохранённым значениям.
         * @returns {string} Отформатированная строка, передаваемая в document.cookie.
         * @readonly
         * @example
         * app.cookie.remove('name'); //Удаление cookie с параметрами path и domain по умолчанию
         *
         * @example
         * const removedCookie = app.cookie.remove('name', {path: '/page', domain: 'subdomain.site.com'});
         * //Удаление cookie с нестандартными параметрами path и domain
         * console.log(removedCookie); //Возвращает удалённый cookie
         */
        Object.defineProperty(this, 'remove', {
            enumerable: true,
            value: removeCookie
        });
    },

    moduleName
));

addModule(moduleName, AppCookie);

export default AppCookie;
export {AppCookie};