import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const cookieCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const AppCookie = modules.default;
            resolve(AppCookie);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        cookieCallback(resolve);
        return;
    }

    onInit('cookie', () => {
        cookieCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('cookie');
    });
});

export default importFunc;