import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import AppCookie from '../sync.js';

describe('AppCookie', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.get()', () => {
        const dataTypesAssertions = [
            'string number',
            'space',
            'string',
            'HTML string'
        ].map(value => [value]);
        dataTypesAssertions.push(['undefined', [isInstance, Object]]);
        dataTypesCheck({
            checkFunction(getFunction, value) {
                return getFunction(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'key\' must be a non-empty string'],
            setupFunction() {
                const getFunction = AppCookie.get;
                return getFunction;
            }
        });

        test('должен возвращать объект со всеми сохранёнными cookie, если не указан параметр \'key\'', () => {
            AppCookie.set('test', '');
            AppCookie.set('test2', '');

            const cookieObject = {
                test: '',
                test2: ''
            };
            const getResult = AppCookie.get();

            expect(getResult).toBeInstanceOf(Object);
            expect(getResult).toMatchObject(cookieObject);
        });

        test('должен возвращать сохранённое значение cookie, указанного в параметре \'key\'', () => {
            AppCookie.set('test', '123');

            const getResult = AppCookie.get('test');

            expect(getResult).toBe('123');
        });

        test('должен корректно декодировать полученное значение cookie', () => {
            const setResult = AppCookie.set('test', {
                prop: 1
            });

            const getResult = AppCookie.get('test');

            expect(setResult).toBe('test={%22prop%22:1}; path=/; expires=');
            expect(getResult).toBe('{"prop":1}');
        });

        test('должен возвращать сохранённый в cookie объект при включённой опции \'json\'', () => {
            const testObject = {
                prop: 1
            };
            const setResult = AppCookie.set('test', testObject);

            const getResult = AppCookie.get('test', true);

            expect(setResult).toBe('test={%22prop%22:1}; path=/; expires=');
            expect(getResult).toMatchObject(testObject);
        });

        test('должен возвращать строковое значение cookie при попытке получить значение, не являющееся объектом с опцией \'json\'', () => {
            AppCookie.set('test', 'abc');

            const getResult = AppCookie.get('test', true);

            expect(getResult).toBe('abc');
        });
    });
});