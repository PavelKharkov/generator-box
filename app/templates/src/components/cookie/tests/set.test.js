/* eslint-disable unicorn/consistent-function-scoping */

import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import AppCookie from '../sync.js';

describe('AppCookie', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.set()', () => {
        const dataTypesAssertions = [
            ['string number', '1=; path=/; expires='],
            ['space', '%20=; path=/; expires='],
            ['string', 'abc=; path=/; expires='],
            ['HTML string', '%3Cdiv%3E%3C%2Fdiv%3E=; path=/; expires='],
            ['undefined', [throwsError, 'missing parameter \'key\'']]
        ];
        dataTypesCheck({
            checkFunction(setFunction, value) {
                return setFunction(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'key\' must be a non-empty string'],
            setupFunction() {
                const setFunction = AppCookie.set;
                return setFunction;
            }
        });

        test('должен выбрасывать ошибку при некорректном параметре \'attributes\'', () => {
            const setResult = (() => AppCookie.set('test', '', []));

            expect(setResult).toThrow('parameter \'attributes\' must be a plain object');
        });

        test('должен сохранять cookie со значением \'path\' по умолчанию при отрицательном значении опции \'path\'', () => {
            const setResult = AppCookie.set('test', '', {path: false});

            const testCookie = AppCookie.get('test');

            expect(testCookie).toBe('');
            expect(setResult).toMatch('; path=/; ');
        });

        describe('expires', () => {
            test('должен сохранять cookie с сессионной длительностью при некорректном типе опции \'expires\'', () => {
                const setResult = AppCookie.set('test', '', {expires: ''});

                const testCookie = AppCookie.get('test');

                expect(testCookie).toBe('');
                expect(setResult).toMatch('; expires=; ');
            });

            test('должен вычислять колчество дней для атрибута \'expires\' при числовом значении этого параметра', () => {
                const setResult = AppCookie.set('test', '', {expires: 7});

                const testCookie = AppCookie.get('test');
                const expectedDate = new Date(Number(new Date().getTime() + (7 * 864e+5))).toUTCString();

                expect(testCookie).toBe('');
                expect(setResult).toMatch(`; expires=${expectedDate}; `);
            });

            test('должен корректно устанавливать атрибут \'expires\', если в параметрах был передан объект даты', () => {
                const expiresDate = new Date().getTime() + 5000;
                const setResult = AppCookie.set('test', '', {expires: new Date(expiresDate)});

                const testCookie = AppCookie.get('test');
                const expectedDate = new Date(expiresDate).toUTCString();

                expect(testCookie).toBe('');
                expect(setResult).toMatch(`; expires=${expectedDate}; `);
            });

            test('должен корректно устанавливать атрибут \'expires\', если в параметрах был передан некорректный объект даты', () => {
                const setResult = AppCookie.set('test', '', {expires: new Date('420000')});

                const testCookie = AppCookie.get('test');

                expect(testCookie).toBe('');
                expect(setResult).toMatch('; expires=; ');
            });
        });

        test('должен сохранять cookie без указания дополнительных атрибутов', () => {
            AppCookie.set('test');

            const testCookie = AppCookie.get('test');

            expect(testCookie).toBe('');
        });

        test('должен корректно сохранять атрибуты cookie со значением \'true\'', () => {
            const setResult = AppCookie.set('test', '', {
                attr: true
            });

            const testCookie = AppCookie.get('test');

            expect(setResult).toMatch('; attr; ');
            expect(testCookie).toBe('');
        });

        test('должен корректно сохранять атрибуты cookie со не-строкровыми значениями', () => {
            const setResult = AppCookie.set('test', '', {
                attr1: 123,
                attr2: null,
                attr3: {},
                attr4: [1, 2],
                attr5: undefined /* eslint-disable-line no-undefined */
            });

            const testCookie = AppCookie.get('test');

            expect(setResult).toMatch('; attr1=123; attr2=null; attr3=[object Object]; attr4=1,2; attr5=undefined; ');
            expect(testCookie).toBe('');
        });

        test('должен кодировать символы \';\' в значении cookie', () => {
            const setResult = AppCookie.set('test', 'a;b');

            const testCookie = AppCookie.get('test');

            expect(setResult).toMatch('test=a%3Bb; ');
            expect(testCookie).toBe('a;b');
        });

        test('должен обрезать значения атрибутов до первого символа \';\'', () => {
            const setResult = AppCookie.set('test', '', {
                attribute: 'a;b'
            });

            const testCookie = AppCookie.get('test');

            expect(setResult).toMatch('attribute=a; ');
            expect(testCookie).toBe('');
        });

        test('должен корректно кодировать ключ cookie', () => {
            const setResult = AppCookie.set('test key', '');

            const testCookie = AppCookie.get('test%20key');

            expect(setResult).toMatch('test%20key=; ');
            expect(testCookie).toBe('');
        });

        test('должен корректно кодировать значение cookie', () => {
            const setResult = AppCookie.set('test', 'test value');

            const testCookie = AppCookie.get('test');

            expect(setResult).toMatch('test=test%20value; ');
            expect(testCookie).toBe('test value');
        });

        test('должен корректно кодировать значение cookie при сохранении объекта', () => {
            const testObject = {
                testProperty: 1,
                testArray: ['a', 'b'],
                innerObject: {
                    prop1: 1,
                    prop2: 'abc',
                    prop3: true
                }
            };
            const setResult = AppCookie.set('test', testObject);

            const testCookie = AppCookie.get('test');

            const matchedString = 'test={%22testProperty%22:1%2C%22testArray%22:[%22a%22%2C%22b%22]%2C%22innerObject%22:' +
                '{%22prop1%22:1%2C%22prop2%22:%22abc%22%2C%22prop3%22:true}}; ';

            expect(setResult.indexOf(matchedString)).toBe(0);
            expect(testCookie).toBe('{"testProperty":1,"testArray":["a","b"],"innerObject":{"prop1":1,"prop2":"abc","prop3":true}}');
        });
    });
});