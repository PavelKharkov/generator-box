import 'Base/scripts/test';

import Module from 'Components/module';
import AppCookie from '../sync.js';

describe('AppCookie', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const coookiePromise = require('..').default;

        expect(coookiePromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в экземпляр класса', async () => {
        await new Promise(done => {
            return require('..').default.then(AppCookieTest => {
                expect(AppCookieTest.constructor.name).toBe(AppCookie.constructor.name);
                done();
            });
        });
    });

    test('должен являться экземпляром модуля', () => {
        expect(AppCookie).toBeInstanceOf(Module);
    });
});