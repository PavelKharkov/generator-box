import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import AppCookie from '../sync.js';

describe('AppCookie', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.remove()', () => {
        const mockDate = new Date(1466424490000);
        jest.spyOn(global, 'Date').mockImplementation(() => mockDate);

        const removedDate = mockDate.toUTCString();
        const dataTypesAssertions = [
            ['string number', `1=; expires=${removedDate}; path=/`],
            ['space', `%20=; expires=${removedDate}; path=/`],
            ['string', `abc=; expires=${removedDate}; path=/`],
            ['HTML string', `%3Cdiv%3E%3C%2Fdiv%3E=; expires=${removedDate}; path=/`],
            ['undefined', [throwsError, 'missing parameter \'key\'']]
        ];
        dataTypesCheck({
            checkFunction(removeFunction, value) {
                return removeFunction(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'key\' must be a non-empty string'],
            setupFunction() {
                const removeFunction = AppCookie.remove;
                return removeFunction;
            }
        });
    });

    test('должен удалять найденый cookie, установленный без атрибутов \'path\' и \'domain\'', () => {
        AppCookie.set('test', '');

        AppCookie.remove('test');
        const getResult = AppCookie.get('test');

        expect(getResult).toBeUndefined();
    });

    //TODO:test path & domain attrs
});