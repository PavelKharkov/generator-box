import {dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';

import AppCounter from '../sync.js';

describe('AppCounter', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.setValue()', () => {
        afterEach(() => {
            document.body.innerHTML = '';
        });

        const dataTypesAssertions = [
            ['number', 1],
            ['float number', 0.5],
            ['string number', 1]
        ];
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: 0,
            checkFunction(counterInstance, value) {
                const result = counterInstance.setValue(value);
                counterInstance.change(0, true);
                return result;
            },
            setupFunction() {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                return counterInstance;
            }
        });

        test('должно вызываться после вызова события \'change\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            const setValueFunction = jest.fn();
            counterInstance.on('setValue', () => setValueFunction());

            counterInstance.change();

            expect(setValueFunction).toHaveBeenCalledWith();
        });

        describe('параметры', () => {
            test('должно получать параметры события \'change\', если событие \'beforeChange\' не возвращает другие параметры', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                let delta;
                let absolute;
                counterInstance.on('setValue', (deltaParam, absoluteParam) => {
                    delta = deltaParam;
                    absolute = absoluteParam;
                });

                counterInstance.change(1, true);

                expect(delta).toBe(1);
                expect(absolute).toBe(true);
            });

            test('должно получать параметр \'delta\' события \'beforeChange\', если оно возвращает результат', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                let delta;
                let absolute;
                counterInstance.on('beforeChange', () => {
                    return 10;
                });
                counterInstance.on('setValue', (deltaParam, absoluteParam) => {
                    delta = deltaParam;
                    absolute = absoluteParam;
                });

                counterInstance.change(1, true);

                expect(delta).toBe(10);
                expect(absolute).toBe(true);
            });

            test('должно использовать опцию \'min\' для установки значения, если было передано некорректное значение \'delta\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '1';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                expect(counterInstance.value).toBe(1);
                counterInstance.change('true', true);
                expect(counterInstance.value).toBe(0);
            });

            test('должно получать параметр \'absolute\' в булевом типе', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '1';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                let absolute;
                counterInstance.on('setValue', (deltaParam, absoluteParam) => {
                    absolute = absoluteParam;
                });

                counterInstance.change(2, 'true');
                expect(absolute).toBe(true);
                expect(counterInstance.value).toBe(2);
            });
        });

        test('должно корректно устанавливать значение \'prevValue\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.prevValue).toBeNull();
            counterInstance.change(2);
            expect(counterInstance.prevValue).toBe(1);
        });

        test('должно возвращать установленное значение', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl, {
                roundTo: 'upper'
            });
            const changeResult = counterInstance.change(2);

            expect(changeResult).toBe(2);
            expect(counterInstance.value).toBe(2);
        });

        test('должно возвращать округлённое значение согласно свойству \'roundFunction\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl, {
                roundTo: 'upper'
            });
            const changeResult = counterInstance.change(1.5);

            expect(changeResult).toBe(2);
        });

        test('должно устанавливать значение опции \'max\' при попытке поменять значение на большее чем эта опция', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl, {
                max: 1
            });

            counterInstance.change(2);

            expect(counterInstance.value).toBe(1);
        });

        test('должно вызывать функцию \'shownFormat\', если она была указана в опциях', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const shownFormat = jest.fn();

            const counterInstance = new AppCounter(counterEl, {
                shownFormat
            });

            expect(shownFormat).toHaveBeenCalledWith('0');
            counterInstance.change(1);
            expect(shownFormat).toHaveBeenCalledWith('1');
        });

        describe('.outMax()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const outMaxCall = (() => counterInstance.outMax());
                const outMaxEmit = counterInstance.emit('outMax');

                expect(outMaxCall).toThrow('counterInstance.outMax is not a function');
                expect(outMaxEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение больше значения опции \'max\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const outMaxFunction = jest.fn();
                counterInstance.on('outMax', outMaxFunction);

                counterInstance.change(2);
                expect(outMaxFunction).toHaveBeenCalledWith(2);
            });

            test('не должно вызываться, если новое значение меньше или равно значению опции \'max\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const outMaxFunction = jest.fn();
                counterInstance.on('outMax', outMaxFunction);

                counterInstance.change(1);
                expect(outMaxFunction).not.toHaveBeenCalledWith();
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const outMaxFunction = jest.fn();
                counterInstance.on('outMax', outMaxFunction);

                counterInstance.change();
                expect(outMaxFunction).not.toHaveBeenCalledWith();
            });
        });

        test('не должно деактивировать кнопку увеличения значения, если новое значение больше или равно значению опции \'max\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            const counterInstance = new AppCounter(counterEl, {
                max: 1
            });

            counterInstance.change(2);

            expect(util.isDisabled(counterPlusButtonEl)).toBe(false);
        });

        test('не должно деактивировать кнопку увеличения значения с отключённой опцией \'disableOuts\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            const counterInstance = new AppCounter(counterEl, {
                max: 1,
                disableOuts: false
            });

            counterInstance.change(2);

            expect(util.isDisabled(counterPlusButtonEl)).toBe(false);
        });

        describe('.max()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const maxCall = (() => counterInstance.max());
                const maxEmit = counterInstance.emit('max');

                expect(maxCall).toThrow('counterInstance.max is not a function');
                expect(maxEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение соответствует значению опции \'max\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const maxFunction = jest.fn();
                counterInstance.on('max', maxFunction);

                counterInstance.change(1);
                expect(maxFunction).toHaveBeenCalledWith(1);
            });

            test('должно вызываться, если новое значение больше значения опции \'max\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const maxFunction = jest.fn();
                counterInstance.on('max', maxFunction);

                counterInstance.change(2);
                expect(maxFunction).toHaveBeenCalledWith(1);
            });

            test('не должно вызываться, если новое значение меньше значения опции \'max\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 2
                });
                const maxFunction = jest.fn();
                counterInstance.on('max', maxFunction);

                counterInstance.change(1);
                expect(maxFunction).not.toHaveBeenCalledWith();
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '2';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 2
                });
                const maxFunction = jest.fn();
                counterInstance.on('max', maxFunction);

                counterInstance.change();
                expect(maxFunction).not.toHaveBeenCalledWith();
            });
        });

        test('не должно активировать кнопку уменьшения значения, если новое значение больше значения опции \'min\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl);

            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
            counterInstance.change(1);
            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
        });

        describe('.more()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const moreCall = (() => counterInstance.more());
                const moreEmit = counterInstance.emit('more');

                expect(moreCall).toThrow('counterInstance.more is not a function');
                expect(moreEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение больше предыдущего значения', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const moreFunction = jest.fn();
                counterInstance.on('more', moreFunction);

                counterInstance.change(1);
                expect(moreFunction).toHaveBeenCalledWith(1);
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const moreFunction = jest.fn();
                counterInstance.on('more', moreFunction);

                counterInstance.change();
                expect(moreFunction).not.toHaveBeenCalledWith();
            });
        });

        describe('.outMin()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const outMinCall = (() => counterInstance.outMin());
                const outMinEmit = counterInstance.emit('outMin');

                expect(outMinCall).toThrow('counterInstance.outMin is not a function');
                expect(outMinEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение больше значения опции \'min\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const outMinFunction = jest.fn();
                counterInstance.on('outMin', outMinFunction);

                counterInstance.change(-1);
                expect(outMinFunction).toHaveBeenCalledWith(-1);
            });

            test('не должно вызываться, если новое значение меньше или равно значению опции \'min\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const outMinFunction = jest.fn();
                counterInstance.on('outMin', outMinFunction);

                counterInstance.change();
                expect(outMinFunction).not.toHaveBeenCalledWith();
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const outMinFunction = jest.fn();
                counterInstance.on('outMin', outMinFunction);

                counterInstance.change();
                expect(outMinFunction).not.toHaveBeenCalledWith();
            });
        });

        test('не должно деактивировать кнопку уменьшения значения, если новое значение меньше или равно значению опции \'min\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl);

            counterInstance.change(-1);

            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
        });

        test('не должно деактивировать кнопку уменьшения значения с отключённой опцией \'disableOuts\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl, {
                disableOuts: false
            });

            counterInstance.change(-1);

            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
        });

        describe('.min()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const minCall = (() => counterInstance.min());
                const minEmit = counterInstance.emit('min');

                expect(minCall).toThrow('counterInstance.min is not a function');
                expect(minEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение соответствует значению опции \'min\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const minFunction = jest.fn();
                counterInstance.on('min', minFunction);

                counterInstance.change();
                expect(minFunction).toHaveBeenCalledWith(0);
            });

            test('должно вызываться, если новое значение меньше значения опции \'min\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const minFunction = jest.fn();
                counterInstance.on('min', minFunction);

                counterInstance.change(-1);
                expect(minFunction).toHaveBeenCalledWith(0);
            });

            test('не должно вызываться, если новое значение больше значения опции \'min\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const minFunction = jest.fn();
                counterInstance.on('min', minFunction);

                counterInstance.change(1);
                expect(minFunction).not.toHaveBeenCalledWith();
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '2';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 2
                });
                const minFunction = jest.fn();
                counterInstance.on('min', minFunction);

                counterInstance.change();
                expect(minFunction).not.toHaveBeenCalledWith();
            });
        });

        describe('.less()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const lessCall = (() => counterInstance.less());
                const lessEmit = counterInstance.emit('less');

                expect(lessCall).toThrow('counterInstance.less is not a function');
                expect(lessEmit).toBeUndefined();
            });

            test('должно вызываться, если новое значение меньше предыдущего значения', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '2';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const lessFunction = jest.fn();
                counterInstance.on('less', lessFunction);

                counterInstance.change(-1);
                expect(lessFunction).toHaveBeenCalledWith(1);
            });

            test('не должно вызываться, если новое значение соответствует предыдущему значению', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const lessFunction = jest.fn();
                counterInstance.on('less', lessFunction);

                counterInstance.change();
                expect(lessFunction).not.toHaveBeenCalledWith();
            });
        });

        test('не должно активировать кнопку увеличения значения, если новое значение меньше значения опции \'max\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl);

            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
            counterInstance.change(1);
            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
        });

        test('должно устанавливать атрибут \'value\' поля ввода при завершении события', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            counterInstance.change(1);
            expect(counterInputEl.getAttribute('value')).toBe('1');
        });

        test('должно устанавливать свойство \'value\' поля ввода при завершении', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            counterInstance.change(1);
            expect(counterInputEl.value).toBe('1');
        });

        describe('.changed()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const changedCall = (() => counterInstance.changed());
                const changedEmit = counterInstance.emit('changed');

                expect(changedCall).toThrow('counterInstance.changed is not a function');
                expect(changedEmit).toBeUndefined();
            });

            test('должно вызываться, если значение счётчика изменилось', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const changedFunction = jest.fn();
                counterInstance.on('changed', changedFunction);

                counterInstance.change(1);
                expect(changedFunction).toHaveBeenCalledWith(1, '1');
            });

            test('не должно вызываться, если значение счётчика не изменилось', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const changedFunction = jest.fn();
                counterInstance.on('changed', changedFunction);

                counterInstance.change();
                expect(changedFunction).not.toHaveBeenCalledWith();
            });
        });

        describe('.afterChange()', () => {
            test('должно вызываться только с помощью метода \'emit\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const afterChangeCall = (() => counterInstance.afterChange());
                const afterChangeEmit = counterInstance.emit('afterChange');

                expect(afterChangeCall).toThrow('counterInstance.afterChange is not a function');
                expect(afterChangeEmit).toBeUndefined();
            });

            test('должно вызываться событие после вызова события \'setValue\'', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const afterChangeFunction = jest.fn();
                counterInstance.on('afterChange', afterChangeFunction);

                counterInstance.change(1);
                expect(afterChangeFunction).toHaveBeenCalledWith(1, '1');
            });
        });

        //TODO:test with disableOuts
    });
});