/* eslint-disable max-nested-callbacks */

import {dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';

import AppCounter from '../sync.js';

describe('AppCounter', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.ajax()', () => {
        dataTypesCheck({
            dataTypesAssertions: [],
            checkFunction(counterInstance, value) {
                const result = counterInstance.beforeChange(value);
                counterInstance.change(0, true);
                return result;
            },
            setupFunction() {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                return counterInstance;
            }
        });

        test('должно вызываться после вызова события \'change\'', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            await counterInstance.on('ajax', (newValue, resolve) => {
                const ajaxFunction = jest.fn();
                ajaxFunction();
                expect(ajaxFunction).toHaveBeenCalledWith();
                resolve(newValue);
            });

            counterInstance.change(1);

            jest.runAllTimers();
        });

        test('должно вызываться только с помощью метода \'emit\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            const ajaxCall = (() => counterInstance.ajax());
            const ajaxEmit = counterInstance.emit('ajax');

            expect(ajaxCall).toThrow('counterInstance.ajax is not a function');
            expect(ajaxEmit).toBeUndefined();
        });

        test('должно корректно получать три параметра', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            await counterInstance.on('ajax', (newValue, resolve, reject) => {
                expect(typeof newValue).toBe('number');
                expect(newValue).toBe(1);
                expect(typeof resolve).toBe('function');
                expect(typeof reject).toBe('function');
                resolve(newValue);
            });

            counterInstance.change(1);

            jest.runAllTimers();
        });

        test('должно получать результат события \'setValue\' при вызове события \'change\'', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            const changeValue = counterInstance.change(1);
            await counterInstance.on('ajax', (newValue, resolve) => {
                const ajaxFunction = jest.fn();
                ajaxFunction(newValue);
                expect(changeValue).toBe(1);
                expect(newValue).toBe(changeValue);
                expect(ajaxFunction).toHaveBeenCalledWith(newValue);
                resolve(newValue);
            });

            jest.runAllTimers();
        });

        test('при возвращении любого значения должен показываться индикатор загрузки счётчика', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            await counterInstance.on('ajax', (newValue, resolve) => {
                return util.animationDefer(() => {
                    resolve(newValue);
                });
            });

            counterInstance.change(1);

            jest.runAllTimers();
            expect(counterInstance.preloader.isShown).toBe(true);
        });

        test('должно после завершения промиса скрывать индикатор загрузки счётчика', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            await (async () => {
                await counterInstance.on('ajax', (newValue, resolve) => {
                    return util.animationDefer(() => {
                        expect(counterInstance.preloader.preloader.classList).toContain('active');
                        resolve(newValue);
                    });
                });

                expect(counterInstance.preloader.isShown).toBe(false);
            })();

            counterInstance.change(1);

            jest.runAllTimers();
        });

        test('при возвращении значения при успешном выполнении промиса должно вызывать событие \'setValue\' с этим значением', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            const valueArray = [1, 11];
            counterInstance.on('setValue', value => {
                const assertValue = valueArray.shift();
                expect(value).toBe(assertValue);
            });

            await (async () => {
                await counterInstance.on('ajax', (newValue, resolve) => {
                    return resolve(newValue + 10);
                });
            })();

            counterInstance.change(1);

            jest.runAllTimers();
            expect.assertions(2);
        });

        test('при отклонённом промисе не должно обновлять значение счётчика', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);

            counterInstance.on('setValue', value => {
                expect(value).toBe(1);
            });

            await (async () => {
                await counterInstance.on('ajax', (newValue, resolve, reject) => {
                    const passedValue = newValue + 10;
                    expect(passedValue).toBe(11);
                    return reject(passedValue);
                });
            })();

            counterInstance.change(1);

            jest.runAllTimers();
            expect.assertions(2);
        });

        test('должно запрещать вызывать событие повторно во время, указанное в опции \'debounceDuration\'', async () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl, {
                debounceDuration: 250
            });

            counterInstance.change(1);
            counterInstance.change(2);
            await counterInstance.on('ajax', (newValue, resolve) => {
                const ajaxFunction = jest.fn();
                ajaxFunction();
                expect(ajaxFunction).toHaveBeenCalledWith();
                resolve(newValue);
            });

            jest.advanceTimersByTime(250);
            expect.assertions(1);

            counterInstance.change(1);
            jest.runAllTimers();
            expect.assertions(2);
        });
    });
});