import {dataTypesCheck} from 'Base/scripts/test';

import AppCounter from '../sync.js';

describe('AppCounter', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.beforeChange()', () => {
        dataTypesCheck({
            dataTypesAssertions: [],
            checkFunction(counterInstance, value) {
                const result = counterInstance.beforeChange(value);
                counterInstance.change(0, true);
                return result;
            },
            setupFunction() {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                return counterInstance;
            }
        });

        test('должно вызываться после вызова \'change\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            const beforeChangeFunction = jest.fn();
            counterInstance.on('beforeChange', () => beforeChangeFunction());

            counterInstance.change();

            expect(beforeChangeFunction).toHaveBeenCalledWith();
        });

        test('должно получать параметры события \'change\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            let delta;
            let absolute;
            counterInstance.on('beforeChange', (deltaParam, absoluteParam) => {
                delta = deltaParam;
                absolute = absoluteParam;
            });

            counterInstance.change(1, true);

            expect(delta).toBe(1);
            expect(absolute).toBe(true);
        });

        test('должно получать значение параметра \'absolute\' в булевом типе', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            let absolute;
            counterInstance.on('beforeChange', (deltaParam, absoluteParam) => {
                absolute = absoluteParam;
            });

            counterInstance.change(1, 'true');

            expect(absolute).toBe(true);
        });

        test('должно возвращать результат событию \'change\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.on('beforeChange', () => {
                return 10;
            });

            counterInstance.change();

            expect(counterInstance.value).toBe(10);
        });

        test('при возвращении значения \'undefined\' событие \'change\' должно передавать начальное значение параметра \'delta\'', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.on('beforeChange', () => {
                let undefinedValue;
                return undefinedValue;
            });

            counterInstance.change(1);

            expect(counterInstance.value).toBe(1);
        });
    });
});