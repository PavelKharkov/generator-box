import {dataTypesCheck} from 'Base/scripts/test';

import AppCounter from '../sync.js';

describe('AppCounter', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.change()', () => {
        const dataTypesAssertions = [
            ['number', 1],
            ['float number', 0.5],
            ['string number', 1]
        ];
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: 0,
            checkFunction(counterInstance, value) {
                const result = counterInstance.change(value);
                counterInstance.change(0, true);
                return result;
            },
            setupFunction() {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                return counterInstance;
            }
        });

        test('должно корректно вызываться с опциями по умолчанию', () => {
            const counterEl = document.createElement('div');
            document.body.append(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterInstance = new AppCounter(counterEl);
            let delta;
            let absolute;
            counterInstance.on('beforeChange', (deltaParam, absoluteParam) => {
                delta = deltaParam;
                absolute = absoluteParam;
            });

            counterInstance.change();

            expect(delta).toBe(0);
            expect(absolute).toBe(false);
        });

        describe('absolute', () => {
            test('должно прибавляться значение параметра \'delta\' к существующему значению при отрицательном значении параметра', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '1';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                counterInstance.change(1, false);

                expect(counterInstance.value).toBe(2);
            });

            test('должно устанавливаться новое значение \'delta\' независимо от старого значения при положительном значении параметра', () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '1';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);

                counterInstance.change(1, true);

                expect(counterInstance.value).toBe(1);
            });
        });

        describe('порядок вызова событий', () => {
            test('должны вызываться события в корректном порядке при уменьшении значения счётчика', async () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.value = '1';
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl);
                const eventsArray = [];
                const expectedArray = [
                    'beforeChange',
                    'outMin',
                    'min',
                    'less',
                    'changed',
                    'afterChange',
                    'setValue',
                    'ajax'
                ];

                counterInstance.on({
                    beforeChange() {
                        eventsArray.push('beforeChange');
                    },
                    outMin() {
                        eventsArray.push('outMin');
                    },
                    min() {
                        eventsArray.push('min');
                    },
                    less() {
                        eventsArray.push('less');
                    },
                    changed() {
                        eventsArray.push('changed');
                    },
                    afterChange() {
                        eventsArray.push('afterChange');
                    },
                    setValue() {
                        eventsArray.push('setValue');
                    }
                });

                await counterInstance.on('ajax', (newValue, resolve) => {
                    eventsArray.push('ajax');
                    expect(eventsArray).toStrictEqual(expect.arrayContaining(expectedArray));
                    resolve(newValue);
                });

                counterInstance.change(-2);

                jest.runAllTimers();
            });

            test('должны вызываться события в корректном порядке при увеличения значения счётчика', async () => {
                const counterEl = document.createElement('div');
                document.body.append(counterEl);

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                const counterInstance = new AppCounter(counterEl, {
                    max: 1
                });
                const eventsArray = [];
                const expectedArray = [
                    'beforeChange',
                    'outMax',
                    'max',
                    'more',
                    'changed',
                    'afterChange',
                    'setValue',
                    'ajax'
                ];

                counterInstance.on({
                    beforeChange() {
                        eventsArray.push('beforeChange');
                    },
                    outMax() {
                        eventsArray.push('outMax');
                    },
                    max() {
                        eventsArray.push('max');
                    },
                    more() {
                        eventsArray.push('more');
                    },
                    changed() {
                        eventsArray.push('changed');
                    },
                    afterChange() {
                        eventsArray.push('afterChange');
                    },
                    setValue() {
                        eventsArray.push('setValue');
                    }
                });

                await counterInstance.on('ajax', (newValue, resolve) => {
                    eventsArray.push('ajax');
                    expect(eventsArray).toStrictEqual(expect.arrayContaining(expectedArray));
                    resolve(newValue);
                });

                counterInstance.change(2);

                jest.runAllTimers();
            });
        });
    });
});