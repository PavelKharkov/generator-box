import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import util from 'Layout/main';

import AppPreloader from 'Components/preloader';

//Инициализация тестового элемента с атрибутом data-counter
const counterWithDataEl = document.createElement('div');
counterWithDataEl.dataset.counter = '';

const counterInputWithDataEl = document.createElement('input');
counterInputWithDataEl.classList.add('counter-input');
counterWithDataEl.append(counterInputWithDataEl);

document.body.append(counterWithDataEl);

const AppCounter = require('../sync.js').default;

describe('AppCounter', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const counterPromise = require('..').default;

        expect(counterPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            return require('..').default.then(AppCounterTest => {
                expect(AppCounterTest.name).toBe(AppCounter.name);
                done();
            });
        });
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalCounter = window.app.Counter;

        expect(globalCounter.prototype).toBeInstanceOf(Module);
    });

    const dataTypesAssertions = ['HTMLElement']
        .map(value => [value, [throwsError, 'input element isn\'t found']]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.counter;
            return new AppCounter(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, '\'element\' must be a HTMLElement'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppCounter\' с переданным значением'
    });

    test('должен сохраняться под именем \'counter\' в свойстве \'modules\' у элементов', () => {
        const counterEl = document.createElement('div');

        const counterInputEl = document.createElement('input');
        counterInputEl.classList.add('counter-input');
        counterEl.append(counterInputEl);

        document.body.append(counterEl);

        new AppCounter(counterEl); /* eslint-disable-line no-new */
        const counterInstanceProp = counterEl.modules.counter;
        const counterInstanceInputProp = counterInputEl.modules.counter;

        expect(counterInstanceProp).toBeInstanceOf(AppCounter);
        expect(counterInstanceInputProp).toBeInstanceOf(AppCounter);
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-counter\'', () => {
        const counterInstance = counterWithDataEl.modules.counter;

        expect(counterInstance).toBeInstanceOf(AppCounter);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const counterPropsArray = [
            'beforeChange',
            'change',
            'disable',
            'disabled',
            'enable',
            'initialValue',
            'input',
            'minusButton',
            'plusButton',
            'preloader',
            'prevValue',
            'roundFunction',
            'setValue',
            'shownValue',
            'setShownValue',
            'value'
        ];
        test.each(counterPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            const currentProp = counterInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const counterEventsArray = [
            'afterChange',
            'ajax',
            'beforeChange',
            'change',
            'changed',
            'less',
            'max',
            'min',
            'more',
            'outMax',
            'outMin',
            'setValue'
        ];
        test.each(counterEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const eventsArray = counterInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const counterOptionsMap = [
            ['min', 0],
            ['minChange', 1],
            ['debounceDuration', 0],
            ['input', '.counter-input'],
            ['minusButton', '.counter-minus'],
            ['plusButton', '.counter-plus'],
            ['preloader', true],
            ['mask', true]
        ];
        test.each(counterOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instanceOptions = counterInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    describe('[data-counter-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-counter-options\'', () => {
            const counterEl = document.createElement('div');
            counterEl.dataset.counterOptions = '{"min": 5}';

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instanceOptions = counterInstance.options;

            expect(instanceOptions.min).toBe(5);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-counter-options\'', () => {
            const counterEl = document.createElement('div');
            counterEl.dataset.counterOptions = 'true';

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = (() => new AppCounter(counterEl));

            expect(counterInstance).toThrow('incorrect data-counter-options format');
        });
    });

    describe('.minChange', () => {
        describe('некорректные значения опции', () => {
            const minChangeOptionMap = [
                ['строка', '1'],
                ['NaN', Number.NaN],
                ['ноль', 0]
            ];
            test.each(minChangeOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => {
                    return new AppCounter(counterEl, {
                        minChange: value
                    });
                });
                expect(counterInstance).toThrow('\'params.minChange\' must be a non-zero number');
            });
        });
    });

    describe('.roundFunction', () => {
        describe('корректные значения опции \'roundTo\'', () => {
            const roundToOptionMap = [
                ['upper', 'upper'],
                ['lower', 'lower'],
                ['nearest', 'nearest'],
                ['noRound', false]
            ];
            test.each(roundToOptionMap)('должен корректно сохранять функцию округления \'%s\', указанную в опциях', (functionName, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = new AppCounter(counterEl, {
                    roundTo: value
                });
                const roundFunction = counterInstance.roundFunction;

                expect(typeof roundFunction).toBe('function');
            });
        });

        describe('некорректные значения опции \'roundTo\'', () => {
            const roundToOptionMap = [
                ['число', 1],
                ['неизвестная функция', 'test'],
                ['true', true]
            ];
            test.each(roundToOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => {
                    return new AppCounter(counterEl, {
                        roundTo: value
                    });
                });
                expect(counterInstance).toThrow('possible values for \'roundTo\' option: \'upper\', \'lower\', \'nearest\', false');
            });
        });

        describe('корректные значения для установки свойства по умолчанию', () => {
            const roundToOptionMap = [
                ['false', false],
                ['0', 0],
                ['null', null],
                ['undefined', undefined], /* eslint-disable-line no-undefined */
                ['NaN', Number.NaN]
            ];
            test.each(roundToOptionMap)('должен устанавливать функцию округления по умолчанию при любом не-строковом falsy-значении опции (%s)',
                (paramType, value) => {
                    const counterEl = document.createElement('div');

                    const counterInputEl = document.createElement('input');
                    counterInputEl.classList.add('counter-input');
                    counterEl.append(counterInputEl);

                    document.body.append(counterEl);

                    const counterInstance = new AppCounter(counterEl, {
                        roundTo: value
                    });
                    const roundFunction = counterInstance.roundFunction;

                    expect(typeof roundFunction).toBe('function');
                });
        });

        describe('варианты округления', () => {
            /* eslint-disable array-bracket-newline */
            const roundFunctionMap = [
                ['округление вверх', 'upper', 2],
                ['округление вниз', 'lower', 1],
                ['округление к ближайшему', 'nearest', 2],
                ['без округления', false, 1.5]
            ];
            /* eslint-enable array-bracket-newline */
            test.each(roundFunctionMap)('должен корректно округлять значение счётчика (%s) при значении опции \'%s\'',
                (roundType, roundOpt, result) => {
                    const counterEl = document.createElement('div');

                    const counterInputEl = document.createElement('input');
                    counterInputEl.classList.add('counter-input');
                    counterInputEl.value = '1.5';
                    counterEl.append(counterInputEl);

                    document.body.append(counterEl);

                    const counterInstance = new AppCounter(counterEl, {
                        roundTo: roundOpt
                    });
                    expect(counterInstance.value).toBe(result);
                });
        });
    });

    describe('.min', () => {
        describe('некорректные значения опции', () => {
            const minOptionMap = [
                ['строка', '1'],
                ['NaN', Number.NaN]
            ];
            test.each(minOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => {
                    return new AppCounter(counterEl, {
                        min: value
                    });
                });
                expect(counterInstance).toThrow('\'params.min\' must be a number');
            });
        });

        describe('некорректные значения data-атрибута \'min\' у поля ввода', () => {
            const minChangeOptionMap = [['не-числовое значение', 'abc']];
            test.each(minChangeOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.dataset.min = value;
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => new AppCounter(counterEl));
                expect(counterInstance).toThrow('\'input.dataset.min\' must be a number');
            });
        });

        test('должен сохранять числовое значение data-атрибута \'min\' у поля ввода, установленное корректно', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.dataset.min = '2';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const min = counterInstance.options.min;

            expect(min).toBe(2);
        });
    });

    describe('.max', () => {
        describe('некорректные значения опции', () => {
            const maxOptionMap = [
                ['строка', '1'],
                ['NaN', Number.NaN]
            ];
            test.each(maxOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => {
                    return new AppCounter(counterEl, {
                        max: value
                    });
                });
                expect(counterInstance).toThrow('\'params.max\' must be a number');
            });
        });

        describe('некорректные значения data-атрибута \'max\' у поля ввода', () => {
            const minChangeOptionMap = [['не-числовое значение', 'abc']];
            test.each(minChangeOptionMap)('должен выбрасывать ошибку, если получает некорректный тип опции (%s)', (paramType, value) => {
                const counterEl = document.createElement('div');

                const counterInputEl = document.createElement('input');
                counterInputEl.classList.add('counter-input');
                counterInputEl.dataset.max = value;
                counterEl.append(counterInputEl);

                document.body.append(counterEl);

                const counterInstance = (() => new AppCounter(counterEl));
                expect(counterInstance).toThrow('\'input.dataset.max\' must be a number');
            });
        });

        test('должен сохранять числовое значение data-атрибута \'max\' у поля ввода, установленное корректно', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.dataset.max = '2';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const max = counterInstance.options.max;

            expect(max).toBe(2);
        });

        test('должен округлять значение опции \'max\' до кратного числа к опции \'minChange\' с опцией \'minChangeRound\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                max: 2.5,
                minChange: 1,
                minChangeRound: true
            });
            const max = counterInstance.options.max;

            expect(max).toBe(2);
        });

        test('должен корректно округлять значение отрицательной опции \'max\' до кратного числа к отрицательной опции \'minChange\' ' +
            'с опцией \'minChangeRound\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                min: -5,
                max: -2.5,
                minChange: -1,
                minChangeRound: true
            });
            const max = counterInstance.options.max;

            expect(max).toBe(-2);
        });

        test('должен выбрасывать ошибку, если значение опции \'min\' больше значения опции \'max\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = (() => {
                return new AppCounter(counterEl, {
                    min: 2,
                    max: 1
                });
            });

            expect(counterInstance).toThrow('\'min\' option can\'t be more than \'max\'');
        });
    });

    describe('.shownFormat()', () => {
        test('должен выбрасывать ошибку при некорректном значении опции \'shownFormat\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = (() => {
                return new AppCounter(counterEl, {
                    shownFormat: true
                });
            });

            expect(counterInstance).toThrow('\'params.shownFormat\' must be a function');
        });

        test('должен изменять значение свойства \'shownValue\' при вызове события \'change\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                shownFormat(value) {
                    return `${value}.1`;
                }
            });
            counterInstance.change(1);

            expect(counterInstance.shownValue).toBe('1.1');
        });

        test('должен изменять значение поля ввода счётчика при вызове события \'change\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                shownFormat(value) {
                    return `${value}.1`;
                }
            });
            counterInstance.change(1);

            expect(counterInstance.input.value).toBe('1.1');
        });

        test('должен не изменять свойство \'value\' при вызове события \'change\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                shownFormat(value) {
                    return `${value}.1`;
                }
            });
            counterInstance.change(1);

            expect(counterInstance.value).toBe(1);
        });
    });

    describe('.initialValue', () => {
        test('должен сохранять оригинальное значение счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.initialValue).toBe('1');
            expect(counterInstance.value).toBe(1);
        });

        test('должен выводить пустую строку при инициализации с полем ввода без атрибута \'value\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.initialValue).toBe('');
            expect(counterInstance.value).toBe(0);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const initialValue = (() => {
                counterInstance.initialValue = '2';
            });

            expect(initialValue).toThrow('Cannot assign to read only property \'initialValue\' of object \'#<ImmutableClass>\'');
        });

        test('должен не менять значение свойства при изменении текущего значения счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.initialValue).toBe('1');
            counterInstance.change(2, true);
            expect(counterInstance.initialValue).toBe('1');
            expect(counterInstance.value).toBe(2);
        });
    });

    describe('.value', () => {
        test('должен хранить числовое значение счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.value).toBe(1);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const value = (() => {
                counterInstance.value = 2;
            });

            expect(value).toThrow('Cannot set property value of #<ImmutableClass> which has only a getter');
        });

        test('должен менять свойство после изменения значения счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            expect(counterInstance.value).toBe(1);

            counterInstance.change(2, true);
            expect(counterInstance.value).toBe(2);
        });

        test('должен использовать значение опции \'min\', если значение счётчика по умолчанию не установлено', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                min: 5
            });
            expect(counterInstance.value).toBe(5);
        });

        test('должен использовать значение опции \'min\', если значение счётчика по умочанию установлено не корректно', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = 'abc';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                min: 5
            });
            expect(counterInstance.value).toBe(5);
        });

        test('должен менять значение счётчика после инициализации, если оно меньше значения опции \'min\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                min: 5
            });
            expect(counterInstance.value).toBe(5);
        });

        test('должен менять значение счётчика после инициализации, если оно больше значения опции \'max\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '5';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                max: 3
            });
            expect(counterInstance.value).toBe(3);
        });

        test('должен округлять значение счётчика после инициализации согласно свойству \'roundFunction\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1.5';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                roundTo: 'upper'
            });
            expect(counterInstance.value).toBe(2);
        });
    });

    describe('.prevValue', () => {
        test('должно иметь значение \'null\' при инициализации', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            expect(counterInstance.prevValue).toBeNull();
            expect(counterInstance.value).toBe(0);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const prevValue = (() => {
                counterInstance.prevValue = 2;
            });

            expect(prevValue).toThrow('Cannot set property prevValue of #<ImmutableClass> which has only a getter');
        });

        test('должен хранить предыдущее значение счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            expect(counterInstance.prevValue).toBeNull();
            counterInstance.change(2);
            expect(counterInstance.prevValue).toBe(1);
        });
    });

    describe('.shownValue', () => {
        test('должно иметь значение соответствующее значению счётчика при инициализации', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            expect(counterInstance.shownValue).toBe('0');
        });

        test('должно меняться после изменения значения счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.change(1);
            expect(counterInstance.shownValue).toBe('1');
        });

        test('должно предотвращать вызов события изменения при совпадении значения поля ввода и данного свойства ' +
            'при вызове браузерного события \'blur\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1.5';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());

            expect(counterInstance.shownValue).toBe('1.5');
            const blurEvent = new Event('blur');
            counterInputEl.dispatchEvent(blurEvent);
            expect(changeFunction).not.toHaveBeenCalledWith();
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const shownValue = (() => {
                counterInstance.shownValue = '2';
            });

            expect(shownValue).toThrow('Cannot set property shownValue of #<ImmutableClass> which has only a getter');
        });

        test('должен не обновлять свойство \'value\' после вызова метода \'setShownValue\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.setShownValue('1');

            expect(counterInstance.value).toBe(0);
        });

        test('должен корректно устанавливать свойство после вызова метода \'setShownValue\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.setShownValue('1');

            expect(counterInstance.value).toBe(0);
            expect(counterInstance.shownValue).toBe('1');
        });

        test('должен корректно обновлять свойство \'value\' элемента после вызова метода \'setShownValue\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.setShownValue('1');

            expect(counterInstance.input.value).toBe('1');
        });

        test('должен выбрасывать ошибку при попытке поменять метод \'setShownValue\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const shownValue = (() => {
                counterInstance.setShownValue = null;
            });

            expect(shownValue).toThrow('Cannot assign to read only property \'setShownValue\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.disabled', () => {
        test('должен корректно передавать значение опции \'disabled\' в соответствующее свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                disabled: true
            });

            expect(counterInstance.disabled).toBe(true);
        });

        test('должен вычислять свойство с любым положительным значением опции \'disabled\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                disabled: 'true'
            });

            expect(counterInstance.disabled).toBe(true);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const disabled = (() => {
                counterInstance.disabled = true;
            });

            expect(disabled).toThrow('Cannot set property disabled of #<ImmutableClass> which has only a getter');
        });

        test('должен корректно менять атрибуты элемента экземпляра согласно свойству', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterConfig = {
                disabled: true
            };
            new AppCounter(counterEl, counterConfig); /* eslint-disable-line no-new */

            expect(util.isDisabled(counterEl)).toBe(true);
        });

        test('должен корректно вычислять свойство при инициализации согласно атрибутам элемента экземпляра', () => {
            const counterEl = document.createElement('div');
            util.disable(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            expect(counterEl.classList).toContain('disabled');
            const counterInstance = new AppCounter(counterEl);

            expect(counterInstance.disabled).toBe(true);
        });

        test('должен корректно устанавливать свойство после вызова метода \'disable\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.disable();

            expect(counterInstance.disabled).toBe(true);
        });

        test('должен корректно обновлять атрибуты элемента после вызова метода \'disable\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.disable();

            expect(util.isDisabled(counterEl)).toBe(true);
        });

        test('должен корректно устанавливать свойство после вызова метода \'enable\'', () => {
            const counterEl = document.createElement('div');
            util.disable(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.enable();

            expect(counterInstance.disabled).toBe(false);
        });

        test('должен корректно обновлять атрибуты элемента после вызова метода \'enable\'', () => {
            const counterEl = document.createElement('div');
            util.disable(counterEl);

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            counterInstance.enable();

            expect(util.isDisabled(counterEl)).toBe(false);
        });

        test('должен выбрасывать ошибку при попытке поменять метод \'disable\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const disable = (() => {
                counterInstance.disable = null;
            });

            expect(disable).toThrow('Cannot assign to read only property \'disable\' of object \'#<ImmutableClass>\'');
        });

        test('должен выбрасывать ошибку при попытке поменять метод \'enable\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const enable = (() => {
                counterInstance.enable = null;
            });

            expect(enable).toThrow('Cannot assign to read only property \'enable\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.minusButton', () => {
        test('должен хранить значение \'null\' в свойстве, если элемент кнопки не найден', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instanceMinusButton = counterInstance.minusButton;

            expect(instanceMinusButton).toBeNull();
        });

        test('должен хранить ссылку на элемент кнопки в свойстве, если элемент кнопки найден', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl);
            const instanceMinusButton = counterInstance.minusButton;

            expect(instanceMinusButton).toBe(counterMinusButtonEl);
        });

        test('должен находить кнопку согласно опции \'minusButton\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus-custom');
            counterEl.append(counterMinusButtonEl);

            const counterInstance = new AppCounter(counterEl, {
                minusButton: '.counter-minus-custom'
            });
            const instanceMinusButton = counterInstance.minusButton;

            expect(instanceMinusButton).toBe(counterMinusButtonEl);
        });

        test('должен хранить ссылку на экземпляр модуля на кнопке', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            new AppCounter(counterEl); /* eslint-disable-line no-new */
            const minusButtonInstanceProp = counterMinusButtonEl.modules.counter;

            expect(minusButtonInstanceProp).toBeInstanceOf(AppCounter);
        });

        test('кнопка не должна быть отключена, если значение счётчика соответствует значению опции \'min\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            document.body.append(counterEl);

            /* eslint-disable-next-line no-new */
            new AppCounter(counterEl, {
                min: 1
            });

            expect(util.isDisabled(counterMinusButtonEl)).toBe(false);
        });

        test('при нажатии на кнопку должно вызываться событие \'change\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '2';
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());
            counterMinusButtonEl.click();

            expect(changeFunction).toHaveBeenCalledWith();
            expect(counterInstance.value).toBe(1);
        });

        test('при нажатии на кнопку значение счётчика должно меняться на значение опции \'minChange\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '3';
            counterEl.append(counterInputEl);

            const counterMinusButtonEl = document.createElement('button');
            counterMinusButtonEl.classList.add('counter-minus');
            counterEl.append(counterMinusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            counterMinusButtonEl.click();
            expect(counterInstance.value).toBe(2);

            counterInstance.options.minChange = 2;
            counterMinusButtonEl.click();
            expect(counterInstance.value).toBe(0);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const minusButton = (() => {
                counterInstance.minusButton = null;
            });

            expect(minusButton).toThrow('Cannot assign to read only property \'minusButton\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.plusButton', () => {
        test('должен хранить значение \'null\' в свойстве, если элемент кнопки не найден', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instancePlusButton = counterInstance.plusButton;

            expect(instancePlusButton).toBeNull();
        });

        test('должен хранить ссылку на элемент кнопки в свойстве, если элемент кнопки найден', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instancePlusButton = counterInstance.plusButton;

            expect(instancePlusButton).toBe(counterPlusButtonEl);
        });

        test('должен находить кнопку согласно опции \'plusButton\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus-custom');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                plusButton: '.counter-plus-custom'
            });
            const instancePlusButton = counterInstance.plusButton;

            expect(instancePlusButton).toBe(counterPlusButtonEl);
        });

        test('должен хранить ссылку на экземпляр модуля на кнопке', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            new AppCounter(counterEl); /* eslint-disable-line no-new */
            const plusButtonInstanceProp = counterPlusButtonEl.modules.counter;

            expect(plusButtonInstanceProp).toBeInstanceOf(AppCounter);
        });

        test('кнопка не должна быть отключена, если значение счётчика соответствует значению опции \'max\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            /* eslint-disable-next-line no-new */
            new AppCounter(counterEl, {
                max: 1
            });

            expect(util.isDisabled(counterPlusButtonEl)).toBe(false);
        });

        test('при нажатии на кнопку должно вызываться событие \'change\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());
            counterPlusButtonEl.click();

            expect(changeFunction).toHaveBeenCalledWith();
            expect(counterInstance.value).toBe(1);
        });

        test('при нажатии на кнопку значение счётчика должно меняться на значение опции \'minChange\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            const counterPlusButtonEl = document.createElement('button');
            counterPlusButtonEl.classList.add('counter-plus');
            counterEl.append(counterPlusButtonEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            counterPlusButtonEl.click();
            expect(counterInstance.value).toBe(1);

            counterInstance.options.minChange = 2;
            counterPlusButtonEl.click();
            expect(counterInstance.value).toBe(3);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const plusButton = (() => {
                counterInstance.plusButton = null;
            });

            expect(plusButton).toThrow('Cannot assign to read only property \'plusButton\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.input', () => {
        test('должен корректно сохранять элемент поля ввода', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const instanceInput = counterInstance.input;

            expect(instanceInput).toBe(counterInputEl);
        });

        test('должен находить поле ввода значения согласно опции \'input\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input-custom');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl, {
                input: '.counter-input-custom'
            });
            const instanceInput = counterInstance.input;

            expect(instanceInput).toBe(counterInputEl);
        });

        test('должен хранить ссылку на экземпляр модуля на элементе поля ввода', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            new AppCounter(counterEl); /* eslint-disable-line no-new */
            const inputInstanceProp = counterInputEl.modules.counter;

            expect(inputInstanceProp).toBeInstanceOf(AppCounter);
        });

        test('должно вызываться событие \'change\' при вызове браузерного события \'blur\' поля ввода', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());

            counterInputEl.value = '1';
            const blurEvent = new Event('blur');
            counterInputEl.dispatchEvent(blurEvent);
            expect(changeFunction).toHaveBeenCalledWith();
        });

        test('не должно вызываться событие \'change\' при вызове браузерного события \'blur\' поля ввода ' +
            'если значение поля соответствует значению \'shownValue\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '1';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());

            counterInputEl.value = '1';
            const blurEvent = new Event('blur');
            counterInputEl.dispatchEvent(blurEvent);
            expect(changeFunction).not.toHaveBeenCalledWith();
        });

        test('должно передаваться числовое значение в событие \'change\' при вызове браузерного события \'blur\'', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            counterInputEl.value = '1,5';
            const blurEvent = new Event('blur');
            counterInputEl.dispatchEvent(blurEvent);
            expect(counterInstance.value).toBe(1.5);
        });

        test('должно вызываться событие \'change\' и увеличиваться значение счётчика при нажатии кнопки \'наверх\' на клавиатуре', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());

            const keydownEvent = new KeyboardEvent('keydown', {
                key: 'ArrowUp'
            });
            counterInputEl.dispatchEvent(keydownEvent);
            expect(changeFunction).toHaveBeenCalledWith();
            expect(counterInstance.value).toBe(1);
        });

        test('должно вызываться событие \'change\' и уменьшаться значение счётчика при нажатии кнопки \'вниз\' на клавиатуре', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterInputEl.value = '2';
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());

            const keydownEvent = new KeyboardEvent('keydown', {
                key: 'ArrowDown'
            });
            counterInputEl.dispatchEvent(keydownEvent);
            expect(changeFunction).toHaveBeenCalledWith();
            expect(counterInstance.value).toBe(1);
        });

        test('должно вызываться событие \'change\' и уменьшаться значение счётчика при нажатии кнопки \'Enter\' на клавиатуре', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const changeFunction = jest.fn();
            counterInstance.on('change', () => changeFunction());
            counterInputEl.value = '2';

            const keydownEvent = new KeyboardEvent('keydown', {
                key: 'Enter'
            });
            counterInputEl.dispatchEvent(keydownEvent);
            expect(changeFunction).toHaveBeenCalledWith();
            expect(counterInstance.value).toBe(2);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const input = (() => {
                counterInstance.input = null;
            });

            expect(input).toThrow('Cannot assign to read only property \'input\' of object \'#<ImmutableClass>\'');
        });
    });

    describe('.mask', () => {
        test('должен инициализировать модуль маски после загрузки модуля работы с масками', async () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            new AppCounter(counterEl); /* eslint-disable-line no-new */

            expect(counterInputEl.modules.mask).toBeUndefined();
            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    const inputMaskProp = counterInputEl.modules.mask;
                    expect(inputMaskProp).toBeInstanceOf(AppMask);
                    done();
                });
            });
        });

        test('должен сохранять ссылку на модуль маски в свойстве', async () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    expect(counterInstance.mask).toBeInstanceOf(AppMask);
                    done();
                });
            });
        });

        test('должен инициализировать модуль маски с корректными опциями', async () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);

            await new Promise(done => {
                return require('Components/mask').default.then(AppMask => {
                    const floatNumberOptions = AppMask.floatNumberOptions;

                    expect(counterInstance.mask.options).toMatchObject(floatNumberOptions);
                    done();
                });
            });
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const mask = (() => {
                counterInstance.mask = null;
            });

            expect(mask).toThrow('Cannot set property mask of #<ImmutableClass> which has only a getter');
        });
    });

    describe('.preloader', () => {
        test('должен инициализировать модуль индикатора загрузки счётчика', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            new AppCounter(counterEl); /* eslint-disable-line no-new */

            expect(counterEl.modules.preloader).toBeInstanceOf(AppPreloader);
        });

        test('должен сохранять ссылку на модуль индикатора загрузки в свойстве', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const preloader = counterInstance.preloader;

            expect(preloader).toBeInstanceOf(AppPreloader);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const counterEl = document.createElement('div');

            const counterInputEl = document.createElement('input');
            counterInputEl.classList.add('counter-input');
            counterEl.append(counterInputEl);

            document.body.append(counterEl);

            const counterInstance = new AppCounter(counterEl);
            const preloader = (() => {
                counterInstance.preloader = null;
            });

            expect(preloader).toThrow('Cannot assign to read only property \'preloader\' of object \'#<ImmutableClass>\'');
        });
    });

    //TODO:preloader opt
});