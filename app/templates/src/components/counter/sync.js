import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppPreloader from 'Components/preloader';

//Опции
const moduleName = 'counter';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

//Селекторы
const wrapperSelector = `.${moduleName}-wrapper`;
const inputSelector = `.${moduleName}-input`;
const minusButtonSelector = `.${moduleName}-minus`;
const plusButtonSelector = `.${moduleName}-plus`;

//Классы
const initializedClass = `${moduleName}-initialized`;
const outMaxClass = `-${moduleName}-max`;

//Функции округления
const roundTo = {
    //Округление до верхней границы шага (по умолчанию)
    upper(value, minChange) {
        const steps = 1 / minChange;
        return Math.ceil(value * steps) / steps;
    },

    //Округление до нижней границы шага
    lower(value, minChange) {
        const steps = 1 / minChange;
        return Math.floor(value * steps) / steps;
    },

    //Округление до ближайшего целого значения шага
    nearest(value, minChange) {
        const steps = 1 / minChange;
        return Math.round(value * steps) / steps;
    },

    //Без округления до шага счётчика
    noRound(value) {
        return value;
    }
};

//Опции по умолчанию
const defaultOptions = {
    debounceDuration: 0,
    input: inputSelector,
    minusButton: minusButtonSelector,
    plusButton: plusButtonSelector,
    min: 0,
    minChange: 1,
    setOnInit: true,

    mask: true,
    preloader: true,
    outMaxPopover: {
        title: lang.outMaxText,
        class: outMaxClass,
        placement: 'bottom',
        trigger: [],
        tooltip: true,
        hideTimeout: 1800
    }
};

/**
 * @class AppCounter
 * @memberof components
 * @requires components#AppMask
 * @requires components#AppPopover
 * @requires components#AppPreloader
 * @classdesc Модуль счётчиков.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - HTML-контейнер счётчика.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.value] - Начальное значение счётчика.
 * @param {number} [options.min=0] - Минимальное значение счётчика.
 * @param {number} [options.max] - Максимальное значение счётчика.
 * @param {number} [options.minChange=1] - Минимальное значение изменения счётчика. По умолчанию, данное значение устанавливается на кнопки уменьшения/увеличения. При меньшем изменения счётчика, конечное значение округляется в большую сторону.
 * @param {boolean} [options.minChangeRound] - Устанавливать ли максимальное значение кратным минимальному значенению изменения счётчика.
 * @param {('upper'|'lower'|'nearest'|boolean|Function)} [options.roundTo] - Способ округления значения счётчика при изменении. Возможные значения - 'upper': до верхнего значения, 'lower': до нижнего, 'nearest': до ближайшего, false: без округления, также может быть функцией.
 * @param {boolean} [options.setOnInit=true] - Вызывать ли событие [setValue]{@link components.AppCounter#event:setValue} при инициализации счётчика.
 * @param {number} [options.debounceDuration=0] - Время ожидания перед вызовом события change счётчика.
 * @param {string} [options.input='.counter-input'] - Селектор для поля ввода внутри счётчика.
 * @param {string} [options.minusButton='.counter-minus'] - Селектор для кнопки уменьшения значения внутри счётчика.
 * @param {string} [options.plusButton='.counter-plus'] - Селектор для кнопки увеличения значения внутри счётчика.
 * @param {boolean} [options.disabled] - Инициализировать ли счётчик отключённым.
 * @param {boolean} [options.disableOuts] - Отключать ли возможность выхода за лимиты значений счётчика при достижении граничных значений.
 * @param {Function} [options.shownFormat] - Функция для форматирования отображаемого в поле ввода значения счётчика.
 * @param {(boolean|Object)} [options.mask=true] - Опции для подключаемого модуля маски [AppMask]{@link components.AppMask}. Если true то используются опции [floatNumberOptions]{@link components.AppMask.floatNumberOptions}. Если false, то не подключать модуль маски.
 * @param {(boolean|Object)} [options.preloader=true] - Опции для подключаемого модуля индикатора загрузки [AppPreloader]{@link components.AppPreloader}. Если false, то не подключать модуль индикатора загрузки.
 * @param {(boolean|Object)} [options.outMaxPopover={}] - Опции для подключаемого модуля информера [AppPopover]{@link components.AppPopover}. Если false, то не подключать модуль информера.
 * @param {(string|Array.<string>)} [options.outMaxPopover.trigger=[]] - Событие переключения информера.
 * @param {string} [options.outMaxPopover.class='-counter-max'] - Класс, добавляемый в шаблон информера.
 * @param {string} [options.outMaxPopover.title] - Заголовок информера. По умолчанию берется из объекта с языковыми строками.
 * @param {string} [options.outMaxPopover.placement='bottom'] - Положение информера относительно активирующего элемента.
 * @param {boolean} [options.outMaxPopover.tooltip=true] - Применять ли к информеру стили и поведение информера-подсказки.
 * @param {(number|boolean)} [options.outMaxPopover.hideTimeout=1800] - Время, через которое должен скрыться информер. Если 0 или false, то информер не скрывается.
 * @example
 * const counterInstance = new app.Counter(document.querySelector('.counter-element'), {
 *     min: 5,
 *     minChange: 0.5, //Могут быть установлены дробные значения
 *     value: 6, //Данная опция перебивает значение, установленное в поле ввода
 *     disableOuts: true, //В поле ввода счётчика нельзя поставить значение, меньше чем опция min счётчика
 *     mask: false, //Необходимо отключить маску, чтобы в данном случае корректно работала опция shownFormat
 *     preloader: {
 *         size: 'lg'
 *     },
 *     shownFormat(value) => {
 *         return `--${value}--`;
 *     }
 * });
 * //Опции задаются либо через data-атрибуты, либо при инициализации
 *
 * console.log(counterInstance.initialValue);
 * //К примеру, до инициализации модуля в инпуте было значение '5', тогда выведет '5',
 * //данное свойство хранит значение поля ввода до инициализации экземпляра модуля счётчика
 * console.log(counterInstance.value);
 * //Выведет 10, так как при инициализации, установленый в опциях параметр value
 * //будет округлён согласно опции roundTo (по умолчанию, в большую сторону)
 * console.log(counterInstance.input.value); //'--10--'
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="counter" data-counter>
 *     <button type="button" class="counter-minus">уменьшить на 1</button>
 *     <input type="text" class="counter-input" value="5" data-max="100">
 *     <button type="button" class="counter-plus">увеличить на 1</button>
 * </div>
 *
 * <!--data-color-counter - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-counter-options='{"min": 5}'>
 *     <input type="text" class="counter-input" value="5">
 * </div>
 */
const AppCounter = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const counterOptionsData = el.dataset.counterOptions;
        if (counterOptionsData) {
            const dataOptions = util.stringToJSON(counterOptionsData);
            if (!dataOptions) util.error('incorrect data-counter-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppCounter#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const input = el.querySelector(params.input);

        /**
         * @member {HTMLElement} AppCounter#input
         * @memberof components
         * @desc Элемент поля ввода счётчика.
         * @readonly
         */
        Object.defineProperty(this, 'input', {
            enumerable: true,
            value: input
        });
        if (!input) {
            util.error('input element isn\'t found');
        }

        if (!input.modules) {
            input.modules = {};
        }
        input.modules[moduleName] = this;

        const wrapper = el.querySelector(wrapperSelector);

        const minusButton = el.querySelector(params.minusButton);

        /**
         * @member {HTMLElement} AppCounter#minusButton
         * @memberof components
         * @desc Элемент кнопки уменьшения значения.
         * @readonly
         */
        Object.defineProperty(this, 'minusButton', {
            enumerable: true,
            value: minusButton
        });

        const plusButton = el.querySelector(params.plusButton);

        /**
         * @member {HTMLElement} AppCounter#plusButton
         * @memberof components
         * @desc Элемент кнопки увеличения значения.
         * @readonly
         */
        Object.defineProperty(this, 'plusButton', {
            enumerable: true,
            value: plusButton
        });

        //Опция minChange
        if ((typeof params.minChange !== 'number') || (params.minChange <= 0)) {
            util.typeError(params.minChange, 'params.minChange', 'positive number');
        }

        //Функция округления
        if (params.roundTo) {
            const notStringOrFunction = (typeof params.roundTo !== 'string') && (typeof params.roundTo !== 'function');
            const notAllowedString = (typeof params.roundTo === 'string') && !/^(upper|lower|nearest)$/.test(params.roundTo);
            if (notStringOrFunction || notAllowedString) {
                util.error({
                    message: 'possible values for \'roundTo\' param: \'upper\', \'lower\', \'nearest\', false, Function',
                    value: params.roundTo
                });
            }
        }

        const roundFunction = (typeof this.params.roundTo === 'function')
            ? params.roundTo
            : roundTo[params.roundTo || 'noRound'];

        /**
         * @member {Function} AppCounter#roundFunction
         * @memberof components
         * @desc Округляет значение счётчика при изменении. По умолчанию определяется согласно опции roundTo.
         * @param {number} value - Значение счётчика.
         * @returns {number} Округлённое значение.
         * @readonly
         */
        Object.defineProperty(this, 'roundFunction', {
            enumerable: true,
            value: roundFunction
        });

        if ((typeof params.shownFormat !== 'undefined') && (typeof params.shownFormat !== 'function')) {
            util.typeError(params.shownFormat, 'params.shownFormat', 'function');
        }

        const shownFormat = params.shownFormat;

        //Граничные значения
        //Опция min
        if (!Number.isFinite(params.min)) {
            util.typeError(params.min, 'params.min', 'number');
        }

        //Опция max
        if ((typeof params.max !== 'undefined') && !Number.isFinite(params.max)) {
            util.typeError(params.max, 'params.max', 'number');
        }

        if (params.min > params.max) {
            util.error('\'min\' option can\'t be more than \'max\' option');
        }

        //Округление опции max до кратного числа к опции minChange
        if ((typeof params.max !== 'undefined') && params.minChangeRound) {
            params.max -= (params.max % params.minChange);
        }

        const initialValue = input.value;

        /**
         * @member {string} AppCounter#initialValue
         * @memberof components
         * @desc Значение, установленное в поле ввода счётчика до его инициализации.
         * @readonly
         */
        Object.defineProperty(this, 'initialValue', {
            enumerable: true,
            value: initialValue
        });

        const numberValue = Number.parseFloat(params.value || initialValue || params.min);

        let value = Number.isNaN(numberValue) ? params.min : numberValue;

        /**
         * @member {number} AppCounter#value
         * @memberof components
         * @desc Значение счётчика. Если не указано в опциях и в поле ввода, либо в поле ввода указано не числовое значение, то используется значение опции min.
         * @readonly
         */
        Object.defineProperty(this, 'value', {
            enumerable: true,
            get: () => value
        });

        let prevValue = null;

        /**
         * @member {(number|Object)} AppCounter#prevValue
         * @memberof components
         * @desc Хранит предыдущее значение счётчика. По умолчанию имеет значение null.
         * @readonly
         * @example
         * console.log(counterInstance.value); //1
         * console.log(counterInstance.prevValue); //null
         * counterInstance.change(2);
         * console.log(counterInstance.value); //2
         * console.log(counterInstance.prevValue); //1
         */
        Object.defineProperty(this, 'prevValue', {
            enumerable: true,
            get: () => prevValue
        });

        let shownValue = String(value);

        /**
         * @member {string} AppCounter#shownValue
         * @memberof components
         * @desc Свойство для хранения значения счётчика, отображаемого в поле ввода, если оно отличается от реального значения счётчика. По умолчанию изменяется при помощи опции shownFormat. При необходимости обновляется вручную через метод setShownValue. При активации браузерного события blur поля ввода, для вызова события change, проверяется соответствие текущего значения поля ввода и данного свойства.
         * @readonly
         */
        Object.defineProperty(this, 'shownValue', {
            enumerable: true,
            get: () => shownValue
        });

        /**
         * @function AppCounter#setShownValue
         * @memberof components
         * @desc Устанавливает отображаемое значение счётчика в поле ввода.
         * @param {string} newValue - Отображаемое значение в поле ввода счётчика.
         * @returns {undefined}
         * @readonly
         * @example
         * console.log(counterInstance.value); //1
         * counterInstance.setShownValue(`--${counterInstance.value}--`);
         * console.log(counterInstance.value); //1
         * console.log(counterInstance.shownValue); //'--1--'
         * console.log(counterInstance.input.value); //'--1--'
         */
        Object.defineProperty(this, 'setShownValue', {
            enumerable: true,
            value(newValue) {
                shownValue = String(newValue);
                input.value = shownValue;
                input.setAttribute('value', shownValue);
            }
        });

        let disabled = !!params.disabled;

        /**
         * @member {boolean} AppCounter#disabled
         * @memberof components
         * @desc Указывает, отключён ли счётчик.
         * @readonly
         */
        Object.defineProperty(this, 'disabled', {
            enumerable: true,
            get: () => disabled
        });

        /**
         * @function AppCounter#disable
         * @memberof components
         * @desc Отключает счётчик.
         * @returns {undefined}
         * @readonly
         * @example
         * console.log(counterInstance.disabled); //false
         * counterInstance.disable();
         * console.log(counterInstance.disabled); //true
         */
        Object.defineProperty(this, 'disable', {
            enumerable: true,
            value() {
                disabled = true;
                util.disable(el);
            }
        });

        /**
         * @function AppCounter#enable
         * @memberof components
         * @desc Включает счётчик.
         * @returns {undefined}
         * @readonly
         * @example
         * console.log(counterInstance.disabled); //true
         * counterInstance.enable();
         * console.log(counterInstance.disabled); //false
         */
        Object.defineProperty(this, 'enable', {
            enumerable: true,
            value() {
                disabled = false;
                util.enable(el);
            }
        });

        //Функция, срабатывающая при нажатии на кнопку уменьшения значения
        const minusChange = () => {
            if (params.disableOuts && (value === params.min)) return;

            this.change(-params.minChange);
        };

        if (minusButton) {
            if (!minusButton.modules) {
                minusButton.modules = {};
            }
            minusButton.modules[moduleName] = this;

            minusButton.addEventListener('click', () => minusChange());
        }

        //Функция, срабатывающая при нажатии на кнопку увеличения значения
        const plusChange = () => {
            if (params.disableOuts && (typeof params.max !== 'undefined') && (value === params.max)) {
                return;
            }

            this.change(params.minChange);
        };

        if (plusButton) {
            if (!plusButton.modules) {
                plusButton.modules = {};
            }
            plusButton.modules[moduleName] = this;

            plusButton.addEventListener('click', () => plusChange());
        }

        //Браузерные события поля ввода

        //Функция, срабатывающая при вызове события blur поля ввода
        const blurChange = () => {
            const formattedValue = util.removeDelimiters(input.value, '.');
            if (String(Number.parseFloat(input.value)) !== shownValue) {
                this.change(formattedValue, true);
            }
        };

        input.addEventListener('blur', () => blurChange());
        input.addEventListener('keydown', event => {
            switch (event.key) {
                case 'ArrowUp':
                    plusChange();
                    break;
                case 'ArrowDown':
                    minusChange();
                    break;
                case 'Enter':
                    blurChange();

                //no default
            }
        });

        let mask;
        if (params.mask) {
            const maskRequire = require('Components/mask');
            maskRequire.default.then(AppMask => {
                const maskOptions = util.isObject(params.mask) ? params.mask : AppMask.floatNumberOptions;
                mask = new AppMask(input, maskOptions);
            });
            maskRequire.maskTrigger(input);
        }

        /**
         * @member {Object} AppCounter#mask
         * @memberof components
         * @desc Экземпляр маски [AppMask]{@link components.AppMask} для поля ввода элемента.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'mask', {
            enumerable: true,
            get: () => mask
        });

        let preloader;
        if (params.preloader) {
            const preloaderOptions = util.isObject(params.preloader) ? params.preloader : {};
            preloader = new AppPreloader(el, preloaderOptions);
        }

        /**
         * @member {Object} AppCounter#preloader
         * @memberof components
         * @desc Экземпляр индикатора загрузки [AppPreloader]{@link components.AppPreloader} для элемента.
         * @readonly
         */
        Object.defineProperty(this, 'preloader', {
            enumerable: true,
            value: preloader
        });

        let outMaxPopover;

        /**
         * @member {Object} AppCounter#outMaxPopover
         * @memberof components
         * @desc Экземпляр информера [AppPopover]{@link components.AppPopover}, показывающегося при достижении верхнего граничного значения.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'outMaxPopover', {
            enumerable: true,
            get: () => outMaxPopover
        });

        let outMaxFunc;

        /**
         * @function AppCounter#outMaxFunc
         * @memberof components
         * @desc Функция показа информера при достижении верхнего граничного значения.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'outMaxFunc', {
            enumerable: true,
            get: () => outMaxFunc
        });

        let outMaxTimeout;

        if (!params.disableOuts && params.outMaxPopover) {
            require('Components/popover').default.then(AppPopover => {
                const popoverEl = wrapper || el;
                const popoverOptions = util.isObject(params.outMaxPopover) ? params.outMaxPopover : {};
                outMaxPopover = new AppPopover(popoverEl, popoverOptions);

                outMaxFunc = () => {
                    setTimeout(() => {
                        outMaxPopover.show();

                        if (params.outMaxPopover.hideTimeout) {
                            clearTimeout(outMaxTimeout);
                            outMaxTimeout = setTimeout(() => {
                                outMaxPopover.hide();
                            }, params.outMaxPopover.hideTimeout);
                        }
                    }, 100); //Показывать информер с небольшой задержкой
                };

                util.animationDefer(() => {
                    this.on({
                        max: () => outMaxFunc(),
                        outMax: () => outMaxFunc()
                    });
                }, 0);
            });
            emitInit('popover');
        }

        /**
         * @function ajaxFunction
         * @desc Отложенный вызов события [ajax]{@link components.AppCounter#event:ajax}. Сразу после вызова события, показывает индикатор загрузки. После получения ответа от события ajax, вызывает событие [setValue]{@link components.AppCounter#event:setValue} и передаёт в него результат промиса, резолвящегося внутри события ajax. После этого скрывает индикатор загрузки.
         * @param {number} newValue - Новое значение, передаваемое в событие [ajax]{@link components.AppCounter#event:ajax}.
         * @fires components.AppCounter#ajax
         * @fires components.AppPreloader#show
         * @fires components.AppCounter#setValue
         * @fires components.AppPreloader#hide
         * @returns {undefined}
         * @ignore
         */
        const ajaxFunction = util.debounce(newValue => {
            const promise = new Promise((resolve, reject) => {
                const ajaxReturn = this.emit('ajax', newValue, resolve, reject);
                if (preloader && (typeof ajaxReturn !== 'undefined')) {
                    preloader.show();
                }
            });
            promise
                .then(ajaxValue => {
                    if (ajaxValue === newValue) return;
                    this.setValue(ajaxValue, true);
                })
                .catch(() => {
                    //empty function
                })
                .finally(() => {
                    if (preloader) preloader.hide();
                });
        }, params.debounceDuration);

        this.on({
            /**
             * @event AppCounter#change
             * @memberof components
             * @desc Изменяет значение счётчика. Сразу после вызова события вызывается событие [beforeChange]{@link components.AppCounter#event:beforeChange}. Значение, возвращённое событием beforeChange передаётся в событие [setValue]{@link components.AppCounter#event:setValue}, в котором производятся проверки нового значения на валидность и запись нового значения в DOM. После этого, если новое значение счётчика не совпадает с предыдущим значением, то новое значение передается в событие [ajax]{@link components.AppCounter#event:ajax}.
             * @param {number} [delta=0] - Разница в изменении значения.
             * @param {boolean} [absolute=false] - Изменять значение счётчика независимо от предыдущего значения.
             * @fires components.AppCounter#beforeChange
             * @fires components.AppCounter#setValue
             * @fires components.AppCounter#ajax
             * @returns {number} Новое значение счётчика.
             * @example
             * counterInstance.on('change', (delta, absolute) => {
             *     console.log(`Значение счётчика меняется на ${delta}`);
             * });
             *
             * console.log(counterInstance.value); //1
             * counterInstance.change(10);
             * console.log(counterInstance.change(10)); //11
             * console.log(counterInstance.value); //11
             * console.log(counterInstance.change(-2)); //9
             * console.log(counterInstance.change(5, true)); //5
             * console.log(counterInstance.value); //5
             */
            change(delta = 0, absolute = false) {
                const beforeChangeValue = this.beforeChange(delta, !!absolute);
                const deltaValue = (typeof beforeChangeValue === 'undefined') ? delta : beforeChangeValue;

                const resultValue = this.setValue(deltaValue, !!absolute);
                if (prevValue !== resultValue) {
                    ajaxFunction(resultValue);
                }
                return resultValue;
            },

            /**
             * @event AppCounter#setValue
             * @memberof components
             * @desc Валидирует и записывает новое значение счётчика. Вызывается после вызова события [change]{@link components.AppCounter#event:change}, не нужно использовать это событие напрямую.
             * @param {number} [delta=0] - Планируемое значение, на которое должно измениться текущее значение счётчика или новое значение счётчика, если передан параметр absolute.
             * @param {boolean} [absolute=false] - Изменять значение счётчика независимо от предыдущего значения.
             * @fires components.AppCounter#outMax
             * @fires components.AppCounter#max
             * @fires components.AppCounter#more
             * @fires components.AppCounter#outMin
             * @fires components.AppCounter#min
             * @fires components.AppCounter#less
             * @fires components.AppCounter#changed
             * @fires components.AppCounter#afterChange
             * @returns {number} Новое значение счётчика.
             * @example
             * counterInstance.on('setValue', (delta, absolute) => {
             *     console.log(`Значение счётчика начинает меняться на ${delta}`);
             * });
             */
            setValue(delta = 0, absolute = false) {
                prevValue = value;

                let parsedValue = Number.parseFloat(delta);
                parsedValue = roundFunction(Number.isNaN(parsedValue) ? params.min : parsedValue, params.minChange);
                let newValue = absolute ? parsedValue : (prevValue + parsedValue);
                if (newValue >= prevValue) {
                    const outMax = (typeof params.max !== 'undefined') && (newValue >= params.max);
                    if (outMax) {
                        const savedNewValue = newValue;
                        if (savedNewValue > params.max) {
                            this.emit('outMax', savedNewValue);
                        }
                        newValue = params.max;
                        if (plusButton && params.disableOuts) {
                            util.disable(plusButton);
                        }
                        this.emit('max', newValue);
                    }

                    if (minusButton && (newValue > params.min)) {
                        util.enable(minusButton);
                    }
                    if (newValue > prevValue) {
                        this.emit('more', newValue);
                    }
                }
                if (newValue <= prevValue) {
                    if (newValue <= params.min) {
                        const savedNewValue = newValue;
                        if (savedNewValue < params.min) {
                            this.emit('outMin', savedNewValue);
                        }
                        newValue = params.min;
                        if (minusButton && params.disableOuts) {
                            util.disable(minusButton);
                        }
                        this.emit('min', newValue);
                    }

                    const lessThanMax = (typeof params.max === 'undefined') || (newValue < params.max);
                    if (plusButton && lessThanMax) {
                        util.enable(plusButton);
                    }
                    if (newValue !== prevValue) {
                        this.emit('less', newValue);
                    }
                }

                const stringValue = String(newValue);
                const newShownValue = shownFormat ? shownFormat(stringValue) : stringValue;
                value = newValue;
                input.value = newShownValue;
                input.setAttribute('value', newShownValue);
                shownValue = newShownValue;

                if (newValue !== prevValue) {
                    this.emit('changed', newValue, newShownValue);
                }
                this.emit('afterChange', newValue, newShownValue);
                return newValue;
            },

            /**
             * @event AppCounter#beforeChange
             * @memberof components
             * @desc Вызывается перед изменением значения счётчика. Если возвращает число, то использует его вместо value в событии [change]{@link components.AppCounter#event:change}.
             * @example
             * counterInstance.on('beforeChange', value => {
             *     return value + 10;
             * });
             * console.log(counterInstance.value); //1
             * counterInstance.change(1);
             * console.log(counterInstance.value); //12
             * //По умолчанию изменяет значение на 1, т.е. в итоге должно получиться 2. В beforeChange добавляется еще 10, в итоге будет 12
             */
            beforeChange: true
        });

        this.onSubscribe([
            /**
             * @event AppCounter#outMax
             * @memberof components
             * @desc Вызывается, если установлено большее, чем максимальное допустимое значение счётчика, записанное в опции max. После вызова данного события значение увеличивается до значения опции max и вызывается соответствующее событие.
             * @example
             * counterInstance.on('outMax', value => {
             *     console.log(`Попытка установить значение ${value} при максимальном значении ${counterInstance.max}`);
             * });
             * console.log(counterInstance.max); //10
             * counterInstance.change(11, true); //'Попытка установить значение 11 при максимальном значении 10'
             */
            'outMax',

            /**
             * @event AppCounter#max
             * @memberof components
             * @desc Вызывается, если установлено максимальное допустимое значение счётчика, записанное в опции max.
             * @example
             * counterInstance.on('max', value => {
             *     console.log(`Установлено максимальное значение ${value}`);
             * });
             * console.log(counterInstance.max); //10
             * counterInstance.change(10, true); //'Установлено максимальное значение 10'
             */
            'max',

            /**
             * @event AppCounter#more
             * @memberof components
             * @desc Вызывается, если новое значение счётчика больше предыдущего.
             * @example
             * counterInstance.on('more', value => {
             *     const delta = value - counterInstance.prevValue;
             *     console.log(`Значение счётчика увеличилось на ${delta}`);
             * });
             * console.log(counterInstance.value); //1
             * counterInstance.change(5, true); //'Значение счётчика увеличилось на 4'
             */
            'more',

            /**
             * @event AppCounter#outMin
             * @memberof components
             * @desc Вызывается, если установлено меньшее, чем минимальное допустимое значение счётчика, записанное в опции min. После вызова данного события значение уменьшается до значения опции min и вызывается соответствующее событие.
             * @example
             * counterInstance.on('outMin', value => {
             *     console.log(`Попытка установить значение ${value} при минимальном значении ${counterInstance.min}`);
             * });
             * console.log(counterInstance.min); //1
             * counterInstance.change(0, true); //'Попытка установить значение 0 при минимальном значении 1'
             */
            'outMin',

            /**
             * @event AppCounter#min
             * @memberof components
             * @desc Вызывается, если установлено минимальное допустимое значение счётчика, записанное в опции min.
             * @example
             * counterInstance.on('min', value => {
             *     console.log(`Установлено минимальное значение ${value}`);
             * });
             * console.log(counterInstance.min); //1
             * counterInstance.change(1, true); //'Установлено минимальное значение 1'
             */
            'min',

            /**
             * @event AppCounter#less
             * @memberof components
             * @desc Вызывается, если новое значение счётчика меньше предыдущего.
             * @example
             * counterInstance.on('less', value => {
             *     const delta = counterInstance.prevValue - value;
             *     console.log(`Значение счётчика уменьшилось на ${delta}`);
             * });
             * console.log(counterInstance.value); //5
             * counterInstance.change(1, true); //'Значение счётчика уменьшилось на 4'
             */
            'less',

            /**
             * @event AppCounter#changed
             * @memberof components
             * @desc Вызывается после изменения значения счётчика. В событие передаётся новое значение счётчика и новое отображаемое значение.
             * @example
             * const counterInstance = new app.Counter(document.querySelector('.counter-element'), {
             *     shownFormat(value) {
             *         return `${value}.5`;
             *     }
             * });
             * counterInstance.on('changed', (newValue, newShownValue) => {
             *     console.log(`Новое значение счётчика — ${newValue}, отображаемое значение — ${newShownValue}`);
             * });
             * console.log(counterInstance.value); //5
             * counterInstance.change(0); //Событие 'changed' не вызывается, т.к. значение счётчика не изменилось
             * counterInstance.change(5); //"Новое значение счётчика — 10, отображаемое значение — '10.5'"
             */
            'changed',

            /**
             * @event AppCounter#afterChange
             * @memberof components
             * @desc Вызывается после вызова события [setValue]{@link components.AppCounter#event:setValue}, независимо от того, изменилось ли значение. В событие передаётся новое значение счётчика и новое отображаемое значение.
             * @example
             * const counterInstance = new app.Counter(document.querySelector('.counter-element'), {
             *     shownFormat(value) {
             *         return `${value}.5`;
             *     }
             * });
             * counterInstance.on('afterChange', (newValue, newShownValue) => {
             *     console.log(`Событие setValue завершено, значение счётчика — ${newValue}, отображаемое значение — ${newShownValue}`);
             * });
             * console.log(counterInstance.value); //5
             * counterInstance.change(0); //"Событие setValue завершено, значение счётчика — 0, отображаемое значение — '0.5'"
             * counterInstance.change(5); //"Событие setValue завершено, значение счётчика — 5, отображаемое значение — '5.5'"
             */
            'afterChange',

            /**
             * @event AppCounter#ajax
             * @memberof components
             * @desc Вызывается вместе с проверкой нового значения счётчика. Перед вызовом события создается промис. В функцию события передаётся три параметра - планируемое новое значение счётчика, resolve и reject функции промиса. Если возвращает что-нибудь, кроме undefined, то показывает индикатор загрузки. После успешного исполнения промиса с новым значением, проверяет, совпадает ли оно с изменённым значением счётчика, если не совпадает, то вызывает событие [setValue]{@link components.AppCounter#event:setValue} и передаёт в него результат промиса, резолвящегося внутри события ajax. После этого скрывает индикатор загрузки.
             * @fires components.AppPreloader#show
             * @fires components.AppCounter#setValue
             * @fires components.AppPreloader#hide
             * @example
             * counterInstance.on('ajax', (value, resolve, reject) => {
             *     return setTimeout(() => {
             *         resolve(value + 10);
             *     }, 1000);
             * });
             * //Т.к. в событии возвращается не-undefined значение, то отобразится индикатор загрузки
             * console.log(counterInstance.value); //1
             * counterInstance.change(1);
             * console.log(counterInstance.value); //Выведет 2. Значение поменяется сразу же после вызова события change
             * setTimeout(() => {
             *     console.log(counterInstance.value); //12
             * }, 2000);
             * //По умолчанию изменяет значение на 1, т.е. в итоге должно получиться 2. В ajax добавляется еще 10, в итоге будет 12
             *
             * @example
             * counterInstance.on('ajax', (value, resolve, reject) => {
             *     reject(); //Новое значение не будет применено
             * });
             */
            'ajax'
        ]);

        if (disabled) util.disable(el);
        if (util.isDisabled(el)) {
            disabled = true;
        }

        //Вызов события установки значения при инициализации
        if (params.setOnInit) {
            this.setValue(value, true);
        }

        prevValue = null;

        el.classList.add(initializedClass);

        if (minusButton && (typeof minusButton.dataset.counterTriggered !== 'undefined')) {
            minusChange();
        } else if (plusButton && (typeof plusButton.dataset.counterTriggered !== 'undefined')) {
            plusChange();
        }
    }
});

addModule(moduleName, AppCounter);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-counter]').forEach(el => new AppCounter(el));

export default AppCounter;
export {AppCounter};