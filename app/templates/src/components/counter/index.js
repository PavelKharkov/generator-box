import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let counterTriggered;

const counterCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "counter" */ './sync.js'))
        .then(modules => {
            chunks.counter = true;
            counterTriggered = true;

            const AppCounter = modules.default;
            if (resolve) resolve(AppCounter);
        });
};

const initCounter = event => {
    if (counterTriggered) return;

    if (event.target.classList.contains('counter-minus') || event.target.classList.contains('counter-plus')) {
        event.target.dataset.counterTriggered = '';
    }

    emitInit('counter');
};

const counterTrigger = (counterItems, counterTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (counterItems instanceof Node) ? [counterItems] : counterItems;
    if (items.length === 0) return;

    items.forEach(item => {
        counterTriggers.forEach(trigger => {
            item.addEventListener(trigger, initCounter, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.counter) {
        counterCallback(resolve);
        return;
    }

    onInit('counter', () => {
        counterCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('counter');
    });

    const counterItems = document.querySelectorAll('[data-counter]');
    counterTrigger(counterItems);
});

export default importFunc;
export {counterTrigger};