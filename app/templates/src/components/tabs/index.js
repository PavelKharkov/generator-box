import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let tabsTriggered;

const tabsCallback = resolve => {
    const imports = [
        require('Components/collapse').default,
        require('Components/toggle').default
    ];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "toggles" */ './sync.js'))
            .then(tabs => {
                chunks.toggles = true;
                tabsTriggered = true;

                const AppTabs = tabs.tabsInit(...modules);
                if (resolve) resolve(AppTabs);
            });
    });

    emitInit('collapse', 'toggle');
};

const initTabs = event => {
    if (tabsTriggered) return;

    event.preventDefault();
    const tabLink = event.target.closest('a');
    tabLink.dataset.tabsTriggered = '';

    emitInit('tabs');
};

const tabsTrigger = (tabsItems, tabsTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (tabsItems instanceof Node) ? [tabsItems] : tabsItems;
    if (items.length === 0) return;

    items.forEach(item => {
        tabsTriggers.forEach(trigger => {
            item.addEventListener(trigger, initTabs, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.toggles) {
        tabsCallback(resolve);
        return;
    }

    onInit('tabs', () => {
        tabsCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('tabs');
    });

    const tabsItems = document.querySelectorAll('[data-tabs]');
    tabsTrigger(tabsItems);
});

export default importFunc;
export {tabsTrigger};