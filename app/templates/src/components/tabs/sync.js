import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//TODO:add border-scroll option, activeTab prop

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'tabs';

//Селекторы и классы
const collapseLinkSelector = `.${moduleName}-collapse-link`;
const tabContentSelector = `.${moduleName}-content`;
const fadeClass = 'fade';
const initializedClass = `${moduleName}-initialized`;

//Другие опции
const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

const tabsInit = (AppCollapse, AppToggle) => {
    /**
     * @class AppTabs
     * @memberof components
     * @requires components#AppCollapse
     * @requires components#AppToggle
     * @requires components#AppUrl
     * @classdesc Модуль для интерфейса вкладок.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент меню вкладок.
     * @param {app.Module.instanceOptions} options - Опции экземпляра.
     * @param {(boolean|Object)} [options.collapse] - Использовать ли плавную анимацию переключения вкладок. Если является объектом, то это список опций коллапса.
     * @example
     * const tabsInstance = new app.Tabs(document.querySelector('.tabs-element'), {
     *     collapse: true
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="tabs">
     *     <nav class="tabs-menu">
     *         <ul class="tabs-list" role="tablist" data-tabs>
     *             <li class="nav-item">
     *                 <a class="nav-link active" href="#tab-pane-1" id="tab-1" role="tab" aria-controls="tab-pane-1" aria-expanded="true">Вкладка 1</a>
     *             </li>
     *             <li class="nav-item">
     *                 <a class="nav-link" href="#tab-pane-2" id="tab-2" role="tab" aria-controls="tab-pane-2" aria-expanded="false">Вкладка 2</a>
     *             </li>
     *         </ul>
     *     </nav>
     *     <div class="tabs-content">
     *         <div class="tabs__collapse">
     *             <a class="tabs-collapse__link tabs-collapse-link active" href="#tab-pane-1" aria-controls="tab-pane-1" aria-expanded="true"></a>
     *         </div>
     *         <div class="tab-pane toggled fade show collapse" role="tabpanel" id="tab-pane-1" aria-labelledby="tab-1">Контент вкладки 1</div>
     *         <div class="tabs__collapse">
     *             <a class="tabs-collapse__link tabs-collapse-link" href="#tab-pane-2" aria-controls="tab-pane-2" aria-expanded="false"></a>
     *         </div>
     *         <div class="tab-pane toggled fade collapse" role="tabpanel" id="tab-pane-2" aria-labelledby="tab-2">Контент вкладки 2</div>
     *     </div>
     * </div>
     *
     * <!--data-tabs - селектор по умолчанию-->
     *
     * @example <caption>Активация с помощью хеша</caption>
     * <!--HTML-->
     * <a class="nav-link active" href="#tab-pane-1" id="tab-1" role="tab" aria-controls="tab-pane-1" aria-expanded="true" data-hash-action="tabs">Вкладка 1</a>
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-tabs-options='{"collapse": true}'></div>
     */
    const AppTabs = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const tabsOptionsData = el.dataset.tabsOptions;
            if (tabsOptionsData) {
                const dataOptions = util.stringToJSON(tabsOptionsData);
                if (!dataOptions) util.error('incorrect data-tabs-options format');
                util.extend(this.options, dataOptions);
            }

            const collapseOptionsData = el.dataset.tabsCollapse;
            if (typeof collapseOptionsData === 'string') {
                if (collapseOptionsData === '') {
                    this.options.collapse = true;
                } else {
                    const collapseDataOptions = util.stringToJSON(collapseOptionsData);
                    if (!collapseDataOptions) util.error('incorrect data-collapse-options format');
                    this.options.collapse = collapseDataOptions;
                }
            }

            /**
             * @member {Object} AppTabs#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            const tabLink = el.querySelectorAll('a');
            const tabsContent = util.attempt(() => document.querySelector(tabLink[0].getAttribute('href')).closest(tabContentSelector));
            if ((tabsContent instanceof Error) || !tabsContent) return;

            let isCollapsing = false;

            /**
             * @member {boolean} AppTabs#isCollapsing
             * @memberof components
             * @desc Указывает, изменяются ли вкладки с помощью плавного появляения в данный момент.
             * @readonly
             */
            Object.defineProperty(this, 'isCollapsing', {
                enumerable: true,
                get: () => isCollapsing
            });

            const collapse = [];

            /**
             * @member {Array.<Object>} AppTabs#collapse
             * @memberof components
             * @desc Коллекция экземпляров модулей [AppCollapse]{@link components.AppCollapse} для вкладок.
             * @readonly
             */
            Object.defineProperty(this, 'collapse', {
                enumerable: true,
                value: collapse
            });

            let manualShow = false;
            let manualHide = false;

            //Инициализация экземпляров модулей переключения блоков
            const toggle = [...tabLink].map(link => {
                if (!link.modules) {
                    link.modules = {};
                }
                link.modules[moduleName] = this;

                const toggleInstance = new AppToggle(link, {
                    parent: tabsContent,
                    tabs: true
                });
                if (toggleInstance.targetElements[0].classList.contains(fadeClass)) {
                    toggleInstance.params.duration = transitionDuration;
                }

                //Привязка события модуля к событиям модулей переключающихся блоков
                toggleInstance.on({
                    show: () => {
                        const tabElement = toggleInstance.targetElements[0];
                        tabElement.style.overflow = isCollapsing ? 'hidden' : 'visible';

                        if (!manualShow) {
                            manualShow = true;
                            this.emit('show', {
                                link,
                                tab: tabElement
                            });
                        }
                        util.defer(() => {
                            manualShow = false;
                        });
                    },
                    shown: () => {
                        const tabElement = toggleInstance.targetElements[0];
                        this.emit('shown', {
                            link,
                            tab: tabElement
                        });
                    },
                    hide: () => {
                        const tabElement = toggleInstance.targetElements[0];
                        if (!manualHide) {
                            manualHide = true;
                            this.emit('hide', {
                                link,
                                tab: tabElement
                            });
                        }
                        util.defer(() => {
                            manualHide = false;
                        });
                    },
                    hidden: () => {
                        const tabElement = toggleInstance.targetElements[0];
                        this.emit('hidden', {
                            link,
                            tab: tabElement
                        });
                    }
                });

                const collapseLink = tabsContent.querySelector(`${collapseLinkSelector}[href="#${toggleInstance.target}"]`);
                if (params.collapse && collapseLink) {
                    const collapseOptions = {
                        parent: tabsContent
                    };
                    if (util.isObject(params.collapse)) {
                        util.extend(collapseOptions, params.collapse);
                    }
                    const collapseInstance = new AppCollapse(collapseLink, collapseOptions);
                    collapseInstance.on({
                        show() {
                            isCollapsing = true;
                            util.activate(toggleInstance.controlElements[0]);
                        },
                        shown() {
                            isCollapsing = false;
                            AppWindow.scrollTop({
                                value: AppWindow.scrollY() + collapseInstance.controlElements[0].getBoundingClientRect().top,
                                duration: 200
                            });
                        },
                        hide() {
                            isCollapsing = true;
                            util.deactivate(toggleInstance.controlElements[0]);
                            toggleInstance.hide();
                        },
                        hidden() {
                            isCollapsing = false;
                        }
                    });
                    toggleInstance.on({
                        show() {
                            util.activate(collapseInstance.controlElements[0]);
                            toggleInstance.targetElements[0].style.height = 'auto';
                        },
                        hide() {
                            util.deactivate(collapseInstance.controlElements[0]);
                        }
                    });
                    collapse.push(collapseInstance);
                }
                return toggleInstance;
            });

            /**
             * @member {Array.<Object>} AppTabs#toggle
             * @memberof components
             * @desc Коллекция экземпляров модулей [AppToggle]{@link components.AppToggle} для вкладок.
             * @readonly
             */
            Object.defineProperty(this, 'toggle', {
                enumerable: true,
                value: toggle
            });

            this.on({
                /**
                 * @event AppTabs#show
                 * @memberof components
                 * @desc Показывает вкладку.
                 * @param {Object} options - Опции показа вкладки. Необходимо использовать одну из опций, указывающих на вкладку, которую надо показать.
                 * @param {(HTMLElement|string)} [options.tab] - Элемент вкладки, которую надо показать или атрибут id вкладки.
                 * @param {string} [options.hash] - Можно использовать вместо опции tab. Добавляется при вызове метода при помощи [AppUrl]{@link components.AppUrl}.
                 * @param {HTMLElement} [options.link] - Можно использовать вместо опции tab. Ссылка на элемент вкладки, которую надо показать. Добавляется при подписке на это событие.
                 * @fires components.AppToggle#show
                 * @fires components.AppTabs#shown
                 * @returns {undefined}
                 * @example
                 * tabsInstance.on('show', () => {
                 *     console.log('Вкладка показываеся');
                 * });
                 * tabsInstance.show({
                 *     tab: document.querySelector('#tab-1')
                 * });
                 */
                show(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    if (manualShow) return;
                    manualShow = true;

                    const tab = options.tab || options.hash || options.link;
                    const tabElement = (typeof tab === 'string') ? document.querySelector(`#${tab}`) : tab;
                    if (el.contains(tabElement) && tabElement.modules && tabElement.modules.toggle) {
                        tabElement.modules.toggle.show();
                    }
                },

                /**
                 * @event AppTabs#hide
                 * @memberof components
                 * @desc Скрывает вкладки.
                 * @fires components.AppToggle#hide
                 * @fires components.AppTabs#hidden
                 * @returns {undefined}
                 * @example
                 * tabsInstance.on('hide', () => {
                 *     console.log('Вкладка скрывается');
                 * });
                 * tabsInstance.hide();
                 */
                hide() {
                    if (manualHide) return;
                    manualHide = true;

                    toggle.forEach(tab => {
                        tab.hide();
                    });
                }
            });

            this.onSubscribe([
                /**
                 * @event AppTabs#shown
                 * @memberof components
                 * @desc Вызывается при окончательном показе вкладки.
                 * @returns {undefined}
                 * @example
                 * tabsInstance.on('shown', () => {
                 *     console.log('Вкладка показана');
                 * });
                 */
                'shown',

                /**
                 * @event AppTabs#hidden
                 * @memberof components
                 * @desc Вызывается при окончательном скрытии вкладки.
                 * @returns {undefined}
                 * @example
                 * tabsInstance.on('hidden', () => {
                 *     console.log('Вкладка скрыта');
                 * });
                 */
                'hidden'
            ]);

            el.classList.add(initializedClass);

            this.toggle.find(toggleItem => {
                if (typeof toggleItem.el.dataset.tabsTriggered === 'undefined') {
                    return false;
                }
                this.show({
                    tab: toggleItem.el
                });
                delete toggleItem.el.dataset.tabsTriggered;
                return true;
            });
        }
    });

    //Настройка возможности управления с помощью адресной строки
    require('Components/url').default.then(AppUrl => {
        AppUrl.registerAction({
            id: moduleName,
            selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
            action: `${moduleName}.show`
        });
    });
    if (window.location.hash) emitInit('url');

    addModule(moduleName, AppTabs);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll('[data-tabs]').forEach(el => new AppTabs(el));

    return AppTabs;
};

export default tabsInit;
export {tabsInit};