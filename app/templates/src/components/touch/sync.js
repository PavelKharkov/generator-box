import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'touch';

//Классы
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    mouseTouch: true
};

let prevented = false;

/**
 * @class AppTouch
 * @memberof components
 * @classdesc Модуль для touch-взаимодействий.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент для touch-событий.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {HTMLElement} [options.target] - Элемент, от позиции которого будут расчитываться координаты нажатия.
 * @param {boolean} [options.mouseTouch=true] - Имитировать ли touch-события событиями нажатий мыши.
 * @example
 * const touchInstance = new app.Touch(document.querySelector('.touch-element'), {
 *     mouseTouch: false
 * });
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-toggle-options='{"mouseTouch": false}'></div>
 */
const AppTouch = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const touchOptionsData = el.dataset.touchOptions;
        if (touchOptionsData) {
            const dataOptions = util.stringToJSON(touchOptionsData);
            if (!dataOptions) util.error('incorrect data-touch-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        if (!this.options.target) {
            this.options.target = el;
        }
        const target = this.options.target;

        /**
         * @member {Object} AppTouch#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        /**
         * @member {HTMLElement} AppTouch#target
         * @memberof components
         * @desc Элемент, от позиции которого будут расчитываться координаты нажатия.
         * @readonly
         */
        Object.defineProperty(this, 'target', {
            enumerable: true,
            value: target
        });

        let isTouched = false;

        /**
         * @member {boolean} AppTouch#isTouched
         * @memberof components
         * @desc Указывает, нажат ли элемент в данный момент.
         * @readonly
         */
        Object.defineProperty(this, 'isTouched', {
            enumerable: true,
            get: () => isTouched
        });

        let instancePrevented = false;

        /**
         * @member {boolean} AppTouch#prevented
         * @memberof components
         * @desc Если true, то не вызывает события экземпляра модуля при браузерных событиях.
         * @readonly
         */
        Object.defineProperty(this, 'prevented', {
            enumerable: true,
            get: () => instancePrevented
        });

        /**
         * @function AppTouch#setPrevented
         * @memberof components
         * @desc Предотвращает вызов событий экземпляра модуля при браузерных событиях.
         * @returns {undefined}
         * @readonly
         * @example
         * touchInstance.setPrevented();
         * console.log(touchInstance.prevented); //true
         */
        Object.defineProperty(this, 'setPrevented', {
            enumerable: true,
            value() {
                instancePrevented = true;
            }
        });

        /**
         * @function AppTouch#setUnprevented
         * @memberof components
         * @desc Отменяет предотвращение вызова событий экземпляра модуля при браузерных событиях.
         * @returns {undefined}
         * @readonly
         * @example
         * touchInstance.setUnprevented();
         * console.log(touchInstance.prevented); //false
         */
        Object.defineProperty(this, 'setUnprevented', {
            enumerable: true,
            value() {
                instancePrevented = false;
            }
        });

        this.onSubscribe([
            /**
             * @event AppTouch#touchstart
             * @memberof components
             * @desc Вызывается при вызове браузерного события touchstart или mousedown.
             * @returns {undefined}
             * @example
             * touchInstance.on('touchstart', () => {
             *     console.log('На элементе вызвано браузерное событие touchstart');
             * });
             */
            'touchstart',

            /**
             * @event AppTouch#touchmove
             * @memberof components
             * @desc Вызывается при вызове браузерного события touchmove или mousemove.
             * @returns {undefined}
             * @example
             * touchInstance.on('touchmove', () => {
             *     console.log('На элементе вызвано браузерное событие touchmove');
             * });
             */
            'touchmove',

            /**
             * @event AppTouch#touchend
             * @memberof components
             * @desc Вызывается при вызове браузерного события touchend или mouseup.
             * @returns {undefined}
             * @example
             * touchInstance.on('touchend', () => {
             *     console.log('На элементе вызвано браузерное событие touchend');
             * });
             */
            'touchend'
        ]);

        const startCoords = [];
        const startInnerCoords = [];
        const moveCoords = [];
        const endCoords = [];

        const touches = [];
        const changedTouches = [];

        /**
         * @member {Array.<number>} AppTouch#startCoords
         * @memberof components
         * @desc Координаты начала первого нажатия.
         * @readonly
         */
        Object.defineProperty(this, 'startCoords', {
            enumerable: true,
            value: startCoords
        });

        /**
         * @member {Array.<number>} AppTouch#startInnerCoords
         * @memberof components
         * @desc Координаты начала первого нажатия внутри элемента.
         * @readonly
         */
        Object.defineProperty(this, 'startInnerCoords', {
            enumerable: true,
            value: startInnerCoords
        });

        /**
         * @member {Array.<number>} AppTouch#moveCoords
         * @memberof components
         * @desc Координаты движения первого нажатия.
         * @readonly
         */
        Object.defineProperty(this, 'moveCoords', {
            enumerable: true,
            value: moveCoords
        });

        /**
         * @member {Array.<number>} AppTouch#endCoords
         * @memberof components
         * @desc Координаты окончания первого нажатия.
         * @readonly
         */
        Object.defineProperty(this, 'endCoords', {
            enumerable: true,
            value: endCoords
        });

        /**
         * @member {Array.<Object>} AppTouch#touches
         * @memberof components
         * @desc Коллекция всех активных нажатий в текущий момент. Добавляется только при активном util.isTouch.
         */
        Object.defineProperty(this, 'touches', {
            enumerable: true,
            value: touches
        });

        /**
         * @member {Array.<Object>} AppTouch#changedTouches
         * @memberof components
         * @desc Коллекция всех активных нажатий во время последнего события нажатия. Добавляется только при активном util.isTouch.
         */
        Object.defineProperty(this, 'changedTouches', {
            enumerable: true,
            value: changedTouches
        });

        //Привязка события экземпляра к браузерным событиям
        if (util.isTouch) {
            el.addEventListener('touchstart', event => {
                if (instancePrevented || prevented) return;
                isTouched = true;

                touches.length = 0;
                touches.push(...event.touches);
                changedTouches.length = 0;
                changedTouches.push(...event.changedTouches);
                startCoords.length = 0;
                startCoords.push(
                    event.changedTouches[0].clientX,
                    event.changedTouches[0].clientY
                );
                startInnerCoords.length = 0;
                startInnerCoords.push(
                    startCoords[0] - target.getBoundingClientRect().left,
                    startCoords[1] - target.getBoundingClientRect().top
                );
                this.emit('touchstart', event);
            });

            el.addEventListener('touchmove', event => {
                if (!isTouched || instancePrevented || prevented) return;

                touches.length = 0;
                touches.push(...event.touches);
                changedTouches.length = 0;
                changedTouches.push(...event.changedTouches);
                moveCoords.length = 0;
                moveCoords.push(
                    event.changedTouches[0].clientX - startCoords[0],
                    event.changedTouches[0].clientY - startCoords[1]
                );
                this.emit('touchmove', event);
            });

            el.addEventListener('touchend', event => {
                if (instancePrevented || prevented) return;
                isTouched = false;

                touches.length = 0;
                touches.push(...event.touches);
                changedTouches.length = 0;
                changedTouches.push(...event.changedTouches);
                endCoords.length = 0;
                endCoords.push(
                    event.changedTouches[0].clientX - startCoords[0],
                    event.changedTouches[0].clientY - startCoords[1]
                );
                this.emit('touchend', event);
            });
        } else if (params.mouseTouch) {
            //Имитация touch-событий на устройствах без поддержки touch
            el.addEventListener('mousedown', event => {
                if (instancePrevented || prevented) return;
                isTouched = true;

                startCoords.length = 0;
                startCoords.push(
                    event.clientX,
                    event.clientY
                );
                startInnerCoords.length = 0;
                startInnerCoords.push(
                    startCoords[0] - target.getBoundingClientRect().left,
                    startCoords[1] - target.getBoundingClientRect().top
                );
                this.emit('touchstart', event);
            });

            el.addEventListener('mousemove', event => {
                if (!isTouched || instancePrevented || prevented) return;

                moveCoords.length = 0;
                moveCoords.push(
                    event.clientX - startCoords[0],
                    event.clientY - startCoords[1]
                );
                this.emit('touchmove', event);
            });

            el.addEventListener('mouseup', event => {
                if (instancePrevented || prevented) return;
                isTouched = false;

                endCoords.length = 0;
                endCoords.push(
                    event.clientX - startCoords[0],
                    event.clientY - startCoords[1]
                );
                this.emit('touchend', event);
            });
        }

        el.classList.add(initializedClass);
    }

    /**
     * @function AppTouch.prevented
     * @memberof components
     * @desc Get-функция. Если true, то не вызывает события экземпляров модуля при браузерных событиях.
     * @returns {boolean}
     */
    static get prevented() {
        return prevented;
    }

    /**
     * @function AppTouch.setPrevented
     * @memberof components
     * @desc Предотвращает вызов событий экземпляров модуля при браузерных событиях.
     * @returns {undefined}
     * @example
     * app.Touch.setPrevented();
     * console.log(app.Touch.prevented); //true
     */
    static setPrevented() {
        prevented = true;
    }

    /**
     * @function AppTouch.setUnprevented
     * @memberof components
     * @desc Отменяет предотвращение вызова событий экземпляров модуля при браузерных событиях.
     * @returns {undefined}
     * @example
     * app.Touch.setUnprevented();
     * console.log(app.Touch.prevented); //false
     */
    static setUnprevented() {
        prevented = false;
    }
});

addModule(moduleName, AppTouch);

export default AppTouch;
export {AppTouch};