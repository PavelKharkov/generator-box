import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const touchCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const AppTouch = modules.default;
            resolve(AppTouch);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        touchCallback(resolve);
        return;
    }

    onInit('touch', () => {
        touchCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('touch');
    });
});

export default importFunc;