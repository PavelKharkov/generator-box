import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'history';

const windowHistory = window.history;
const windowLocation = window.location;

//Добавляет новый адрес страницы в историю браузера
const pushState = (newUrl = util.required('newUrl')) => {
    windowHistory.pushState('', '', newUrl);
};

//Заменяет текущий адрес страницы в истории браузера на новый адрес
const replaceState = (newUrl = util.required('newUrl')) => {
    windowHistory.replaceState('', '', newUrl);
};

//Регулярное выражение для поиска get-параметра в строке
const paramRegExp = key => {
    return new RegExp(`(\\?|&)(${key}=)(.*?)(&|$)`);
};

//Получает get-параметр из адреса страницы
const getParameter = (key = util.required('key')) => {
    let result;
    const getParams = windowLocation.search.slice(1);
    getParams.split('&').some(item => {
        const itemArray = item.split('=');
        if (itemArray[0] === key) {
            result = decodeURIComponent(itemArray[1]);
            return true;
        }
        return false;
    });
    return result;
};

//Добавляет один ключ в get-параметры адреса страницы
const setParameter = (key, value) => {
    let newUrl = windowLocation.origin + windowLocation.pathname;
    const keyRegExp = paramRegExp(key);

    newUrl += windowLocation.search
        ? windowLocation.search.replace(keyRegExp, (...args) => {
            return args[1] + args[2] + value + args[4];
        })
        : '?';

    if (!keyRegExp.test(newUrl)) {
        newUrl += `&${key}=${value}`;
    }
    newUrl += windowLocation.hash;
    return newUrl;
};

//Удаляет значение ключа из get-параметров адреса страницы
const removeParameter = key => {
    let newUrl = windowLocation.origin + windowLocation.pathname;
    if (!windowLocation.search) return newUrl;

    newUrl += windowLocation.search.replace(paramRegExp(key), '');
    newUrl += windowLocation.hash;
    return newUrl;
};

/**
 * @function AppHistory
 * @namespace components.AppHistory
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для работы с window.history.
 * @async
 * @ignore
 */
const AppHistory = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppHistory
     * @returns {undefined}
     */
    function() {
        //TODO:browser history go, back, forward

        /**
         * @function components.AppHistory#push
         * @desc Меняет адрес страницы, а также добавляет новый адрес в историю браузера.
         * @param {string} newUrl - Новый адрес страницы.
         * @returns {undefined}
         * @readonly
         * @example
         * app.history.push(`${window.location.href}#test`); //Обновит адрес страницы
         */
        Object.defineProperty(this, 'push', {
            enumerable: true,
            value: pushState
        });

        /**
         * @function components.AppHistory#replace
         * @desc Меняет адрес страницы, и заменяет текущий элемент в истории браузера.
         * @param {string} newUrl - Новый адрес страницы.
         * @returns {undefined}
         * @readonly
         * @example
         * app.history.replace(`${window.location.href}#test`); //Обновит адрес страницы
         */
        Object.defineProperty(this, 'replace', {
            enumerable: true,
            value: replaceState
        });

        /**
         * @function components.AppHistory#set
         * @desc Добавляет ключ или ключи в get-параметры адреса страницы или заменяет текущий параметр.
         * @param {(string|Object)} keys - Название get-ключа или объект с ключами и значениями, которые надо добавить.
         * @param {string} [value=''] - Значение get-ключа.
         * @param {boolean} [replace=false] - Заменять ли текущий элемент в истории браузера.
         * @returns {undefined}
         * @readonly
         * @example
         * app.history.set('page', 3); //Добавит &page=3 в get-параметры адреса страницы или заменит текущий параметр page
         * app.history.set('page', 3, true); //Заменить текущий элемент в истории браузера
         *
         * //Добавит все ключи объекта в get-параметры адреса страницы или заменит текущие параметры
         * app.history.set({
         *     page: 3,
         *     param2: 'param'
         * });
         */
        Object.defineProperty(this, 'set', {
            enumerable: true,
            value(keys = util.required('keys'), value = '', replace = false) {
                let newUrl;
                if (typeof keys === 'object') {
                    Object.keys(keys).forEach(key => {
                        newUrl = setParameter(key, keys[key]);
                    });
                } else {
                    newUrl = setParameter(keys, value);
                }

                if (replace) {
                    replaceState(newUrl);
                    return;
                }

                pushState(newUrl);
            }
        });

        /**
         * @function components.AppHistory#get
         * @desc Получает значение ключа из get-параметров адреса страницы.
         * @param {string} key - Название get-ключа, который надо получить.
         * @returns {(string|undefined)} Значение get-параметра или undefined, если такого ключа нет.
         * @readonly
         * @example
         * //Если в адресе страницы содержится get-параметр page=3
         * app.history.get('page'); //Вернёт '3'
         */
        Object.defineProperty(this, 'get', {
            enumerable: true,
            value: getParameter
        });

        /**
         * @function components.AppHistory#remove
         * @desc Удаляет значение ключа из get-параметров адреса страницы.
         * @param {string} key - Название get-ключа, который надо удалить.
         * @param {boolean} [replace=false] - Заменять ли текущий элемент в истории браузера.
         * @returns {undefined}
         * @readonly
         * @example
         * app.history.remove('page'); //Из адресной строки удалится параметр page
         * app.history.remove('page', true); //Заменить текущий элемент в истории браузера
         */
        Object.defineProperty(this, 'remove', {
            enumerable: true,
            value(key = util.required('key'), replace = false) {
                const newUrl = removeParameter(key, replace);

                if (replace) {
                    replaceState(newUrl);
                    return;
                }

                pushState(newUrl);
            }
        });
    },

    moduleName
));

addModule(moduleName, AppHistory);

export default AppHistory;
export {AppHistory};