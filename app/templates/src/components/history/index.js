let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const historyCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppHistory = modules.default;
                resolve(AppHistory);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            historyCallback(resolve);
            return;
        }

        onInit('history', () => {
            historyCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('history');
        });
    });
}

export default importFunc;