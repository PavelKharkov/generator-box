import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let popupZoomGalleryTriggered;

const popupZoomGalleryCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
        .then(modules => {
            chunks.popups = true;
            popupZoomGalleryTriggered = true;

            const AppPopupZoomGallery = modules.default;
            if (resolve) resolve(AppPopupZoomGallery);
        });
};

const initPopupZoomGallery = event => {
    if (popupZoomGalleryTriggered) return;

    event.preventDefault();
    let itemIndex = '';
    const galleryItem = event.target.closest('[data-popup-gallery-item]');
    if (galleryItem) {
        itemIndex = galleryItem.dataset.popupGalleryIndex;
    }

    event.currentTarget.dataset.popupZoomGalleryTriggered = itemIndex;

    emitInit('popupZoomGallery');
};

const popupZoomGalleryTrigger = (popupZoomGalleryItems, popupZoomGalleryTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (popupZoomGalleryItems instanceof Node) ? [popupZoomGalleryItems] : popupZoomGalleryItems;
    if (items.length === 0) return;

    items.forEach(item => {
        popupZoomGalleryTriggers.forEach(trigger => {
            item.addEventListener(trigger, initPopupZoomGallery, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popups) {
        popupZoomGalleryCallback(resolve);
        return;
    }

    onInit('popupZoomGallery', () => {
        popupZoomGalleryCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('popupZoomGallery');
    });

    const popupZoomGalleryItems = document.querySelectorAll('[data-popup-zoom-gallery]');
    popupZoomGalleryTrigger(popupZoomGalleryItems);
});

export default importFunc;
export {popupZoomGalleryTrigger};