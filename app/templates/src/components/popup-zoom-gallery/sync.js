import {PhotoSwipe, PhotoSwipeUIDefault} from 'Libs/photoswipe';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import 'Components/slider';

//Опции
const moduleName = 'popupZoomGallery';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const PopupZoomGalleryPlugin = PhotoSwipe;
const PopupZoomGalleryPluginUI = PhotoSwipeUIDefault;

//Классы
const popupZoomGalleryClass = util.kebabCase(moduleName);
const baseClass = 'pswp';
const socialShareClass = 'ya-share2';
const socialShareVisibleClass = `${socialShareClass}__popup_visible`;
const initializedClass = `${popupZoomGalleryClass}-initialized`;
const shownClass = `${popupZoomGalleryClass}-shown`;

//Опции по умолчанию
const defaultOptions = {
    history: false,
    fullscreenEl: false,
    mainClass: `${baseClass}--minimal--dark`,
    bgOpacity: 0.85
};
const defaultMarkupOptions = {};
const defaultShareOptions = {
    theme: {
        limit: 0,
        direction: 'vertical'
    }
};

let social;

/**
 * @function fixGalleryItem
 * @desc Настраивает элемент галереи.
 * @param {HTMLElement} item - Опции инициализации шаблона.
 * @returns {(string|Object)} Атрибут href для ссылки элемента или объект с HTML-контентом элементам галереи.
 * @ignore
 */
const fixGalleryItem = item => {
    //Настройка протокола ссылки
    let formattedHref = item.getAttribute('href');
    const protocol = window.location.protocol;
    const isAbsoluteUrl = util.isAbsoluteUrl(formattedHref);
    const urlProtocol = (isAbsoluteUrl && formattedHref.split('//')[0]) || protocol;
    if (protocol && urlProtocol && (urlProtocol !== protocol)) {
        formattedHref = protocol + formattedHref.slice(urlProtocol.length);
    }

    if (item.dataset.popupZoomGalleryItem === 'iframe') {
        //Обработка адресов с youtube
        const youtubeBase = `${protocol}//www.youtube.com/`;
        const youtubeBasePrefix = `${youtubeBase}watch?v=`;

        if (formattedHref.indexOf(youtubeBasePrefix) === 0) {
            formattedHref = `${youtubeBase}embed/${formattedHref.slice(youtubeBasePrefix.length)}`;
        } else {
            const youtubeShortPrefix = `${protocol}//youtu.be/`;
            if (formattedHref.indexOf(youtubeShortPrefix) === 0) {
                formattedHref = `${youtubeBase}embed/${formattedHref.slice(youtubeShortPrefix.length)}`;
            }
        }

        return {
            html: `<iframe src="${formattedHref}" allow="autoplay; encrypted-media" allowfullscreen></iframe>`
        };
    }

    return formattedHref;
};

/**
 * @function initMarkup
 * @desc Инициализация HTML-шаблона галереи.
 * @param {Object} [options={}] - Опции инициализации шаблона.
 * @param {(Object|boolean)} [options.share] - Если true, то добавляет разметку кнопки соц сетей с настройками по умолчанию; Либо объект с опциями кнопки соц сетей.
 * @this window
 * @returns {undefined}
 * @ignore
 */
const initMarkup = function(options = {}) {
    if (!util.isObject(options)) {
        util.typeError(options, 'options', 'plain object');
    }

    util.defaultsDeep(options, defaultMarkupOptions);

    options.share = (options.share === true) ? {} : options.share;

    const shareButtonMarkup = options.share
        ? `<button class="${baseClass}__button ${baseClass}__button--share" title="${lang.shareTitle}"></button>`
        : '';
    const shareModalMarkup = options.share
        ? `<div class="${baseClass}__share-modal ${baseClass}__share-modal--hidden ${baseClass}__single-tap">
            <div class="${baseClass}__share-tooltip"></div>
        </div>
        <div class="${socialShareClass}"></div>`
        : '';

    const markup = `
        <div class="${baseClass}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="${baseClass}__bg"></div>

            <div class="${baseClass}__scroll-wrap">
                <div class="${baseClass}__container">
                    <div class="${baseClass}__item"></div>
                    <div class="${baseClass}__item"></div>
                    <div class="${baseClass}__item"></div>
                </div>

                <div class="${baseClass}__ui ${baseClass}__ui--hidden">
                    <div class="${baseClass}__top-bar">
                        <div class="${baseClass}__counter"></div>

                        <button class="${baseClass}__button ${baseClass}__button--close" title="${lang.closeTitle}"></button>
                        ${shareButtonMarkup}
                        <button class="${baseClass}__button ${baseClass}__button--fs" title="${lang.fsTitle}"></button>
                        <button class="${baseClass}__button ${baseClass}__button--zoom" title="${lang.zoomTitle}"></button>

                        <div class="${baseClass}__preloader">
                            <div class="${baseClass}__preloader__icn">
                                <div class="${baseClass}__preloader__cut">
                                    <div class="${baseClass}__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    ${shareModalMarkup}

                    <button class="${baseClass}__button ${baseClass}__button--arrow--left" title="${lang.arrowLeftTitle}"></button>
                    <button class="${baseClass}__button ${baseClass}__button--arrow--right" title="${lang.arrowRightTitle}"></button>

                    <div class="${baseClass}__caption">
                        <div class="${baseClass}__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>`;

    document.body.insertAdjacentHTML('beforeend', markup);

    if (util.isObject(options.share) && (options.share !== null)) {
        const container = document.querySelector(`.${baseClass}`);

        /**
         * @function shareButtonFunction
         * @desc Открытие попапа со списком социальных сетей.
         * @returns {undefined}
         * @ignore
         */
        const shareButtonFunction = () => {
            util.defer(() => {
                const shareEl = container.querySelector(`.${socialShareVisibleClass}`);
                if (shareEl) shareEl.classList.remove(socialShareVisibleClass);
                container.querySelector(`.${socialShareClass}__item_more`).click();
            });
        };
        const shareButton = document.querySelector(`.${baseClass}__button--share`);
        shareButton.addEventListener('click', shareButtonFunction);
        shareButton.addEventListener('touchstart', shareButtonFunction);

        util.defaultsDeep(options.share, defaultShareOptions);

        const socialRequire = require('Components/social');
        socialRequire.default.then(AppSocial => {
            social = new AppSocial(container.querySelector(`.${socialShareClass}`), options.share);
        });

        socialRequire.socialTrigger(shareButton);

        const initSocial = () => {
            emitInit('social');
        };
        shareButton.addEventListener('click', initSocial, {once: true});
    }
};

/**
 * @class AppPopupZoomGallery
 * @memberof components
 * @requires libs.photoswipe
 * @requires components#AppSlider
 * @requires components#AppSocial
 * @requires components#AppUrl
 * @classdesc Модуль галерей в попапе (с возможностью масштабирования изображения).
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент галереи.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.history=false] - Сохранять ли индекс текущего открытого изображения в адресной строке.
 * @param {Array.<Object>} [options.shareButtons=[]] - Коллекция опций кнопок социальных сетей плагина [Photoswipe]{@link libs.photoswipe}.
 * @param {Object} [options.markup] - Опции инициализации шаблона.
 * @param {(boolean|app.Module.instanceOptions)} [options.markup.share=true] - Если true, то добавляет разметку кнопки социальных сетей с настройками по умолчанию; Либо объект с опциями кнопки социальных сетей.
 * @param {*} [options...] - Другие опции плагина [Photoswipe]{@link libs.photoswipe}.
 * @ignore
 * @example
 * const popupZoomGalleryInstance = new app.PopupZoomGallery(document.querySelector('.zoom-gallery-container'), {
 *     shareButtons: [{id: 'facebook', label: 'Share on Facebook', url: 'https://www.facebook.com/sharer/sharer.php?u={{url}}'}],
 *     markup: {
 *         share: false
 *     }
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-popup-zoom-gallery>
 *     <a href="#" target="_blank" data-popup-gallery-item>
 *         <img src="" alt="">
 *     </a>
 *     <a href="#" target="_blank" data-popup-gallery-item>
 *         <img src="" alt="">
 *     </a>
 * </div>
 *
 * <!--data-popup-zoom-gallery - селектор по умолчанию-->
 *
 * @example <caption>Активация с помощью хеша</caption>
 * <!--HTML-->
 * <div id="some-id" data-hash-action="popupZoomGallery">.</div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-popup-zoom-gallery-options='{"history": true}'></div>
 */
const AppPopupZoomGallery = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const popupZoomGalleryOptionsData = el.dataset.popupZoomGalleryOptions;
        if (popupZoomGalleryOptionsData) {
            const dataOptions = util.stringToJSON(popupZoomGalleryOptionsData);
            if (!dataOptions) util.error('incorrect data-popup-zoom-gallery-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        this.options.shareButtons = this.options.shareButtons || [];

        /**
         * @member {Object} AppPopupZoomGallery#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const items = [...el.querySelectorAll('[data-popup-gallery-item]')];
        if (items.length === 0) return;

        let markupInitialized = false;

        /**
         * @member {boolean} AppPopupZoomGallery#markupInitialized
         * @memberof components
         * @desc Инициализирован ли шаблон попапа галереи.
         * @readonly
         */
        Object.defineProperty(this, 'markupInitialized', {
            enumerable: true,
            get: () => markupInitialized
        });

        let isShown = false;

        /**
         * @member {boolean} AppPopupZoomGallery#isShown
         * @memberof components
         * @desc Показана ли галерея.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let slider;

        /**
         * @member {boolean} AppPopupZoomGallery#slider
         * @memberof components
         * @desc Если галерея является слайдером, то содержит ссылку на экземпляр [AppSlider]{@link components.AppSlider}.
         * @readonly
         */
        Object.defineProperty(this, 'slider', {
            enumerable: true,
            get: () => slider
        });

        if (el.modules.slider) {
            slider = el.modules.slider;
        }

        let galleryInstance;

        /**
         * @member {HTMLElement} AppPopupZoomGallery#galleryInstance
         * @memberof components
         * @desc Экземпляр плагина галереи [Photoswipe]{@link libs.photoswipe}.
         */
        Object.defineProperty(this, 'galleryInstance', {
            enumerable: true,
            get: () => galleryInstance
        });

        let galleryContainer;

        /**
         * @member {HTMLElement} AppPopupZoomGallery#galleryContainer
         * @memberof components
         * @desc Элемент контейнера попапа галереи.
         */
        Object.defineProperty(this, 'galleryContainer', {
            enumerable: true,
            get: () => galleryContainer
        });

        //Создает коллекцию элементов галереи
        const galleryItems = items.map((item, index) => {
            if (!item.modules) {
                item.modules = {};
            }
            item.modules[moduleName] = this;

            const formattedHref = fixGalleryItem(item);
            if (typeof formattedHref === 'object') return formattedHref;

            //TODO:fix click trigger on swipe
            item.addEventListener('click', event => {
                event.preventDefault();

                this.show({index});
            });

            return {
                src: formattedHref,
                w: item.dataset.width || 0,
                h: item.dataset.height || 0
            };
        });

        /**
         * @member {Array.<Object>} AppPopupZoomGallery#galleryItems
         * @memberof components
         * @desc Коллекция элементов галереи.
         * @readonly
         */
        Object.defineProperty(this, 'galleryItems', {
            enumerable: true,
            value: galleryItems
        });

        //TODO:plugin methods

        this.on({
            /**
             * @event AppPopupZoomGallery#show
             * @memberof components
             * @desc Показывает галерею.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.index] - Текущий показываемый элемент галереи.
             * @param {number} [options.hash] - Идентификатор изображения, с индекса которого нужно показывать галерею.
             * @param {*} [options...] - Опции плагина [Photoswipe]{@link libs.photoswipe}.
             * @returns {Object} Экземпляр открытой галереи.
             * @example
             * popupZoomGalleryInstance.on('show', () => {
             *     console.log('Галерея показывается');
             * });
             * console.log(popupZoomGalleryInstance.isShown); //false
             * popupZoomGalleryInstance.show({
             *     index: 1
             * });
             * console.log(popupZoomGalleryInstance.isShown); //true
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                //Инициализация разметки
                if (!markupInitialized) {
                    initMarkup.call(this, params.markup);

                    galleryContainer = document.querySelector(`.${baseClass}`);

                    markupInitialized = true;
                }

                galleryInstance = new PopupZoomGalleryPlugin(
                    galleryContainer,
                    PopupZoomGalleryPluginUI,
                    galleryItems,
                    options
                );

                isShown = true;

                galleryInstance.listen('gettingData', (index, item) => {
                    if (!item.src || ((item.w !== 0) && (item.h !== 0))) return;

                    const image = new Image();

                    /**
                     * @function onloadFunction
                     * @desc Функция, вызываемая при загрузке изображения галереи.
                     * @this image
                     * @returns {undefined}
                     * @ignore
                     */
                    const onloadFunction = function() {
                        item.w = item.w || this.width;
                        item.h = item.h || this.height;
                        galleryInstance.invalidateCurrItems();
                        galleryInstance.updateSize(true);
                    };
                    image.addEventListener('load', onloadFunction);
                    image.src = item.src;
                });

                galleryInstance.listen('close', () => {
                    document.documentElement.classList.remove(shownClass);
                });

                galleryInstance.listen('destroy', () => {
                    document.documentElement.classList.remove(shownClass);
                    isShown = false;
                });

                const currentSlider = slider || el.modules.slider;
                if (currentSlider) {
                    if (!slider) {
                        slider = currentSlider;
                    }
                    galleryInstance.listen('afterChange', () => {
                        slider.goto({
                            index: galleryInstance.getCurrentIndex(),
                            duration: 0
                        });
                    });
                }

                document.documentElement.classList.add(shownClass);
                galleryInstance.init();
                return galleryInstance;
            },

            /**
             * @event AppPopupZoomGallery#hide
             * @memberof components
             * @desc Скрывает галерею.
             * @returns {undefined}
             * @example
             * popupZoomGalleryInstance.on('hide', () => {
             *     console.log('Галерея скрывается');
             * });
             * console.log(popupZoomGalleryInstance.isShown); //true
             * popupZoomGalleryInstance.hide();
             * console.log(popupZoomGalleryInstance.isShown); //false
             */
            hide() {
                galleryInstance.close();
            }
        });

        el.classList.add(initializedClass);

        const currentIndex = el.dataset.popupZoomGalleryTriggered;
        if (typeof currentIndex !== 'undefined') {
            delete el.dataset.popupZoomGalleryTriggered;
            if (currentIndex !== '') {
                this.show({
                    index: Number.parseInt(currentIndex, 10)
                });
            }
        }
    }

    /**
     * @function AppPopupZoomGallery.social
     * @memberof components
     * @desc Get-функция. Ссылка на экземпляр модуля работы с социальными сетями [AppSocial]{@link components.AppSocial}.
     * @returns {Object}
     */
    static get social() {
        return social;
    }
});

//Настройка возможности управления с помощью адресной строки
require('Components/url').default.then(AppUrl => {
    //TODO:test
    AppUrl.registerAction({
        id: moduleName,
        selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
        action: `${moduleName}.show`
    });
});
if (window.location.hash) emitInit('url');

addModule(moduleName, AppPopupZoomGallery);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-popup-zoom-gallery]').forEach(el => new AppPopupZoomGallery(el));

//TODO:add slideshow option - https://github.com/dimsemenov/PhotoSwipe/issues/753

export default AppPopupZoomGallery;
export {AppPopupZoomGallery};