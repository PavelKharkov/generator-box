const Masonry = require('Libs/masonry').default; //TODO:change to another lib

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppImage from 'Components/image';
import AppPreloader from 'Components/preloader';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'layout';

const LayoutPlugin = Masonry;

const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    preloader: true,
    horizontalOrder: true,
    imageSelector: 'img, [data-src]'
};

/**
 * @class AppLayout
 * @memberof components
 * @requires libs.masonry
 * @classdesc Модуль размещения элементов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент контейнера с элементами.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {(boolean|Object)} [options.preloader=true] - Опции инициализации индикатора загрузки. Если передаётся false, то не инициализировать индикатор загрузки.
 * @param {number} [options.gutter] - Отступ между элементами в пикселях.
 * @param {number} [options.horizontalOrder=true] - Прижимать ли элементы к краю контейнера.
 * @param {string} [options.imageSelector='img, [data-src]'] - Селектор для изображений внутри элемента контейнера.
 * @param {boolean} [options.renderOnLoad] - Перестраивать сетку после загрузки каждого изображения.
 * @param {*} [options...] - Другие опции плагина [Masonry]{@link libs.masonry}.
 * @ignore
 * @example
 * const layoutInstance = new app.Layout(document.querySelector('.layout-container'), {
 *     gutter: 30
 * });
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-layout-options='{"gutter": 20}'></div>
 */
const AppLayout = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const layoutOptionsData = el.dataset.layoutOptions;
        if (layoutOptionsData) {
            const dataOptions = util.stringToJSON(layoutOptionsData);
            if (!dataOptions) util.error('incorrect data-layout-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppLayout#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        this.onSubscribe([
            /**
             * @event AppLayout#rendered
             * @memberof components
             * @desc Вызывается, когда сетка элементов полностью простроена.
             * @returns {undefined}
             */
            'rendered'
        ]);

        const instance = new LayoutPlugin(el, params);

        /**
         * @member {Object} AppLayout#instance
         * @memberof components
         * @desc Инициализированный экземпляр плагина [Masonry]{@link libs.masonry}.
         */
        Object.defineProperty(this, 'instance', {
            enumerable: true,
            value: instance
        });

        let preloader;

        /**
         * @member {Object} AppLayout#preloader
         * @memberof components
         * @desc Ссылка на модуль индикатора загрузки [AppPreloader]{@link components.AppPreloader}.
         */
        Object.defineProperty(this, 'preloader', {
            enumerable: true,
            get: () => preloader
        });

        if (params.preloader) {
            preloader = new AppPreloader(el, util.isObject(params.preloader) ? params.preloader : {});
            preloader.show();
        }

        let lazyloadInit = false;
        instance.on('layoutComplete', () => {
            if (lazyloadInit) return;
            lazyloadInit = true;

            if (params.preloader) preloader.hide();
            el.classList.add(initializedClass);
            AppWindow.initLazyLoading();

            this.emit('rendered');
        });

        const images = el.querySelectorAll(params.imageSelector);
        const imagesCount = images.length;
        let imagesLoaded = 0;
        let isFullyLoaded = false;

        const finishRender = () => {
            instance.layout();
            if (params.preloader) preloader.hide();
            el.classList.add(initializedClass);
        };

        //TODO:docs
        const checkLoaded = () => {
            if (imagesLoaded !== imagesCount) return;

            isFullyLoaded = true;
            finishRender();
        };

        images.forEach(image => {
            if (!(image.modules && image.modules.image)) {
                new AppImage(image); /* eslint-disable-line no-new */
                if (image.tagName !== 'IMG') {
                    image.dataset.lazyload = 'image';
                }
            }

            if (image.complete) {
                imagesLoaded += 1;
                if (params.renderOnLoad) {
                    util.debounce(instance.layout, 250, true);
                }
            } else {
                image.modules.image.on('load', () => {
                    imagesLoaded += 1;
                    if (params.renderOnLoad) {
                        util.debounce(instance.layout, 250, true);
                    }
                    checkLoaded();
                });
            }

            checkLoaded();
        });

        setTimeout(() => {
            if (!isFullyLoaded) finishRender();
        }, 3000);

        //TODO:video & iframe load
        //TODO:masonry events

        instance.layout();

        //TODO:rerender on width resize
        //
        //let prevWidth = AppWindow.width();
        //AppWindow.on('resize', () => {
        //    const newWidth = AppWindow.width();
        //    let isWidthResized;
        //    if (newWidth !== prevWidth) {
        //        isWidthResized = true;
        //    }
        //    prevWidth = newWidth;
        //    if (!isWidthResized) return;
        //
        //    instance.destroy();
        //    this.options.initLayout = true;
        //    instance = new LayoutPlugin(el, this.options);
        //});
    }
});

addModule(moduleName, AppLayout);

export default AppLayout;
export {AppLayout};