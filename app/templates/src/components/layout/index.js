let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const layoutCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppLayout = modules.default;
                resolve(AppLayout);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            layoutCallback(resolve);
            return;
        }

        onInit('layout', () => {
            layoutCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('layout');
        });
    });
}

export default importFunc;