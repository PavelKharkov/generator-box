import {throwsError, dataTypesCheck} from 'Base/scripts/test';

const AppDate = require('../sync.js').default;

describe('AppDate', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.formatDate()', () => {
        test('должен быть статическим методом', () => {
            expect(AppDate.formatDate).toBeDefined();
        });

        test('должен сохраняться в экземпляре модуля с установленным параметром', () => {
            const testDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(testDate);
            const formatDate = dateInstance.formatDate();

            expect(formatDate).toBe('1.11.2019');
        });

        const dataTypesAssertions = [
            ['Date', '1.11.2019'],
            ['undefined', [throwsError, 'missing parameter \'inputDate\'']]
        ];
        dataTypesCheck({
            checkFunction(setupResult, value) {
                return AppDate.formatDate(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'inputDate\' must be a date object']
        });

        test('должен корректно форматировать дату', () => {
            const testDate = new Date(2019, 10, 1);
            const formatDate = AppDate.formatDate(testDate);

            expect(formatDate).toBe('1.11.2019');
        });

        test('должен выбрасывать ошибку при попытке поменять метод экземпляра \'formatDate\'', () => {
            const currentDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(currentDate);

            const formatDate = (() => {
                dateInstance.formatDate = null;
            });

            expect(formatDate).toThrow('Cannot assign to read only property \'formatDate\' of object \'#<ImmutableClass>\'');
        });
    });
});