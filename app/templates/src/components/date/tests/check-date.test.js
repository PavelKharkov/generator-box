import {dataTypesCheck} from 'Base/scripts/test';

const AppDate = require('../sync.js').default;

describe('AppDate', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.checkDate()', () => {
        test('должен быть статическим методом', () => {
            expect(AppDate.checkDate).toBeDefined();
        });

        test('должен всегда возвращать \'true\' для инициализированных экземпляров', () => {
            const dateInstance = new AppDate(new Date(2019, 10, 1));

            expect(AppDate.checkDate(dateInstance.el)).toBe(true);
        });

        const dataTypesAssertions = [
            'true',
            'false',
            'zero',
            'number',
            'float number',
            'negative number',
            'string number',
            'Date',
            'null'
        ].map(value => [value, true]);
        dataTypesCheck({
            checkFunction(setupResult, value) {
                return AppDate.checkDate(value);
            },
            dataTypesAssertions,
            defaultValue: false
        });
    });
});