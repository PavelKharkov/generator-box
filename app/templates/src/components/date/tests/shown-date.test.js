import {throwsError, dataTypesCheck} from 'Base/scripts/test';

const AppDate = require('../sync.js').default;

describe('AppDate', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.shownDate()', () => {
        test('должен быть статическим методом', () => {
            expect(AppDate.shownDate).toBeDefined();
        });

        test('должен сохраняться в экземпляре модуля с установленным параметром', () => {
            const testDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(testDate);
            const shownDate = dateInstance.shownDate();

            expect(shownDate).toBe('01.11.2019');
        });

        const dataTypesAssertions = [['Date', '01.11.2019']];
        dataTypesAssertions.push(['undefined', [throwsError, 'missing parameter \'inputDate\'']]);
        dataTypesCheck({
            checkFunction(setupResult, value) {
                return AppDate.shownDate(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'inputDate\' must be a date object']
        });

        test('должен корректно возвращать строку с датой при передаче объекта даты', () => {
            const shownDate = AppDate.shownDate(new Date(2019, 10, 1));

            expect(shownDate).toBe('01.11.2019');
        });

        test('должен выбрасывать ошибку при попытке поменять метод экземпляра \'shownDate\'', () => {
            const currentDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(currentDate);

            const shownDate = (() => {
                dateInstance.shownDate = null;
            });

            expect(shownDate).toThrow('Cannot assign to read only property \'shownDate\' of object \'#<ImmutableClass>\'');
        });
    });
});