import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';

const AppDate = require('../sync.js').default;

describe('AppDate', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const datePromise = require('..').default;

        expect(datePromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            return require('..').default.then(AppDateTest => {
                expect(AppDateTest.name).toBe(AppDate.name);
                done();
            });
        });
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalCounter = window.app.Date;

        expect(globalCounter.prototype).toBeInstanceOf(Module);
    });

    const staticMethodsArray = [
        'checkDate',
        'getDate',
        'shownDate',
        'formatDate',
        'currentDate',
        'definitions',
        'datePattern'
    ];
    test.each(staticMethodsArray)('должен иметь статический метод \'%s\'', method => {
        const currentMethod = AppDate[method];
        expect(currentMethod).toBeDefined();
    });

    const dataTypesAssertions = [
        'true',
        'false',
        'zero',
        'number',
        'float number',
        'negative number',
        'string number',
        'Date',
        'null'
    ].map(value => [value, [isInstance, AppDate]]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.date;
            return new AppDate(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, 'module initialized with invalid date'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppDate\' с переданным значением'
    });

    test('должен корректно инициализироваться с объектом даты', () => {
        const currentDate = new Date(2019, 10, 1);
        const dateInstance = new AppDate(currentDate);

        expect(dateInstance).toBeInstanceOf(AppDate);
        expect(dateInstance.el.getTime()).toBe(1572555600000);
    });

    test('должен корректно инициализироваться из строки даты', () => {
        const currentDate = new Date('2019-11-1');
        const dateInstance = new AppDate(currentDate);

        expect(dateInstance).toBeInstanceOf(AppDate);
        expect(dateInstance.el.getTime()).toBe(1572555600000);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const datePropsArray = [
            'dateEl',
            'formatDate',
            'getDate',
            'shownDate'
        ];
        test.each(datePropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const currentDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(currentDate);

            const currentProp = dateInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('.dateEl', () => {
        test('должен хранить объект даты экземпляра при инициализации с помощью даты', () => {
            const currentDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(currentDate);

            const dateEl = dateInstance.dateEl;

            expect(dateEl).toBeInstanceOf(Date);
            expect(dateEl.getTime()).toBe(1572555600000);
        });

        test('должен хранить объект даты экземпляра при инициализации не через объект даты', () => {
            const currentDate = 1572555600000;
            const dateInstance = new AppDate(currentDate);

            const dateEl = dateInstance.dateEl;

            expect(dateEl).toBeInstanceOf(Date);
            expect(dateEl.getTime()).toBe(1572555600000);
        });

        test('должен выбрасывать ошибку при попытке поменять свойство', () => {
            const currentDate = 1572555600000;
            const dateInstance = new AppDate(currentDate);

            const dateEl = (() => {
                dateInstance.dateEl = new Date();
            });

            expect(dateEl).toThrow('Cannot assign to read only property \'dateEl\' of object \'#<ImmutableClass>\'');
        });
    });
});