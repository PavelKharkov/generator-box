import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

const AppDate = require('../sync.js').default;

describe('AppDate', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    describe('.getDate()', () => {
        test('должен быть статическим методом', () => {
            expect(AppDate.getDate).toBeDefined();
        });

        test('должен сохраняться в экземпляре модуля с установленным параметром', () => {
            const testDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(testDate);
            const getDate = dateInstance.getDate();

            expect(getDate.getTime()).toBe(testDate.getTime());
        });

        const dataTypesAssertions = [
            'string number',
            'empty string',
            'space',
            'string',
            'HTML string'
        ].map(value => [value, [isInstance, Date]]);
        dataTypesAssertions.push(['undefined', [throwsError, 'missing parameter \'outputDate\'']]);
        dataTypesCheck({
            checkFunction(setupResult, value) {
                return AppDate.getDate(value);
            },
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'outputDate\' must be a string']
        });

        test('должен корректно возвращать дату при передаче параметра вида \'datePattern\'', () => {
            const getDateResult = AppDate.getDate('01.11.2019');
            const testDate = new Date(2019, 10, 1);
            const testTime = testDate.getTime();

            expect(getDateResult).toBeInstanceOf(Date);
            expect(getDateResult.getTime()).toBe(testTime);
        });

        test('должен выбрасывать ошибку при попытке поменять метод экземпляра \'getDate\'', () => {
            const currentDate = new Date(2019, 10, 1);
            const dateInstance = new AppDate(currentDate);

            const getDate = (() => {
                dateInstance.getDate = null;
            });

            expect(getDate).toThrow('Cannot assign to read only property \'getDate\' of object \'#<ImmutableClass>\'');
        });
    });
});