import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'date';

let currentDate; /* eslint-disable-line prefer-const */

//Маска даты
const dateDays = 'dd';
const dateMonths = 'mm';
const dateYears = 'yyyy';
const dateDelimiter = '.';
const dateScriptDelimiter = '-';
const datePattern = dateDays + dateDelimiter + dateMonths + dateDelimiter + dateYears; //TODO:parse datePattern

const definitions = {
    day: dateDays,
    month: dateMonths,
    year: dateYears,
    delimiter: dateDelimiter
};

// const milliseconds = 1000;
// const seconds = 60;
// const minutes = 60;
// const hours = 24;
// const day = milliseconds * seconds * minutes * hours;

/**
 * @class AppDate
 * @memberof components
 * @classdesc Модуль для работы с датами.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const dateInstance = new app.Date(new Date());
 */
const AppDate = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        /**
         * @member {Object} AppDate#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        Module.setParams(this);

        if (!this.constructor.checkDate(el)) {
            util.error('module initialized with invalid date');
        }

        const dateEl = new Date(el);

        /**
         * @member {Date} AppDate#dateEl
         * @memberof components
         * @desc Объект даты экземпляра.
         * @readonly
         * @example
         * const dateInstance = new app.Date('2019-11-1');
         * console.log(dateInstance.el); //'2019-11-1'
         * console.log(dateInstance.dateEl); //Fri Nov 01 2019 00:00:00 GMT+0300 (Moscow Standard Time)
         */
        Object.defineProperty(this, 'dateEl', {
            enumerable: true,
            value: dateEl
        });

        Object.defineProperty(this, 'getDate', {
            enumerable: true,
            value: () => dateEl
        });
        Object.defineProperty(this, 'shownDate', {
            enumerable: true,
            value: () => this.constructor.shownDate(dateEl)
        });
        Object.defineProperty(this, 'formatDate', {
            enumerable: true,
            value: () => this.constructor.formatDate(dateEl)
        });
        Object.defineProperty(this, 'stringDate', {
            enumerable: true,
            value: () => this.constructor.stringDate(dateEl)
        });
    }

    /**
     * @function AppDate.checkDate
     * @memberof components
     * @desc Проверяет валидность объекта даты или строки даты.
     * @param {*} [checkedDate] - Объект даты или строка с датой.
     * @returns {boolean} Является ли дата корректной.
     * @example
     * console.log(app.Date.checkDate(new Date())); //true
     * console.log(app.Date.checkDate(new Date('invalid date'))); //false
     * console.log(app.Date.checkDate('2019-11-1')); //true
     * console.log(app.Date.checkDate('2019-11-111')); //false
     */
    static checkDate(checkedDate) {
        const parsedDate = new Date(checkedDate);
        return Number.isFinite(parsedDate.getTime());
    }

    /**
     * @function AppDate.getDate
     * @memberof components
     * @desc Считывает объект-дату из строки вида [datePattern]{@link components.AppDate.datePattern}. Также доступен в виде метода экземпляра.
     * @param {string} outputDate - Строка с датой.
     * @returns {Object} Объект даты, считанной из переданной строки.
     * @example <caption>В виде метода экземпляра</caption>
     * const dateInstance = new app.Date('2019-11-1');
     * console.log(dateInstance.getDate()); //Fri Nov 01 2019 00:00:00 GMT+0300 (Moscow Standard Time)
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Date.getDate('1.11.2019')); //Fri Nov 01 2019 00:00:00 GMT+0300 (Moscow Standard Time)
     */
    static getDate(outputDate = util.required('outputDate')) {
        if (typeof outputDate !== 'string') {
            util.typeError(outputDate, 'outputDate', 'string');
        }

        const datePatternArray = datePattern.split(dateDelimiter);
        const dateArray = outputDate.split(dateDelimiter);
        const resultDateArray = {};
        datePatternArray.forEach((datePart, index) => {
            resultDateArray[datePart] = dateArray[index];
        });

        const resultDateYears = resultDateArray[dateYears];
        const resultDateMonths = Number.parseInt(resultDateArray[dateMonths], 10) - 1;
        const resultDateDays = resultDateArray[dateDays];
        const date = new Date(resultDateYears, resultDateMonths, resultDateDays);

        return date;
    }

    /**
     * @function AppDate.shownDate
     * @memberof components
     * @desc Приводит объект-дату в строку вида [datePattern]{@link components.AppDate.datePattern}. Также доступен в виде метода экземпляра.
     * @param {Object} inputDate - Объект даты.
     * @returns {string} Строка с отформатированной датой.
     * @example <caption>В виде метода экземпляра</caption>
     * const dateInstance = new app.Date('2019-11-1');
     * console.log(dateInstance.shownDate()); //01.11.2019
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Date.shownDate(new Date('2019-11-1'))); //01.11.2019
     */
    static shownDate(inputDate = util.required('inputDate')) {
        if (!(inputDate instanceof Date)) {
            util.typeError(inputDate, 'inputDate', 'date object');
        }

        const shownDay = inputDate.getDate();
        const shownMonth = inputDate.getMonth() + 1;
        const shownYear = inputDate.getFullYear();
        const dayString = (shownDay < 10) ? `0${shownDay}` : shownDay;
        const monthString = (shownMonth < 10) ? `0${shownMonth}` : shownMonth;

        const dateString = datePattern
            .replace(new RegExp(dateDays), dayString)
            .replace(new RegExp(dateMonths), monthString)
            .replace(new RegExp(dateYears), shownYear);

        return dateString;
    }

    /**
     * @function AppDate.formatDate
     * @memberof components
     * @desc Форматирует дату в строку для передачи в бэкенд. Также доступен в виде метода экземпляра.
     * @param {Object} inputDate - Объект даты.
     * @returns {string} Строка с отформатированной датой.
     * @example <caption>В виде метода экземпляра</caption>
     * const dateInstance = new app.Date('2019-11-1');
     * console.log(dateInstance.formatDate()); //1.11.2019
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Date.formatDate(new Date('2019-11-1'))); //1.11.2019
     */
    static formatDate(inputDate = util.required('inputDate')) {
        if (!(inputDate instanceof Date)) {
            util.typeError(inputDate, 'inputDate', 'date object');
        }

        const formattedDays = inputDate.getDate();
        const formattedMonths = inputDate.getMonth() + 1;
        const formattedYears = inputDate.getFullYear();
        const formattedDate = formattedDays + dateDelimiter + formattedMonths + dateDelimiter + formattedYears;

        return formattedDate;
    }

    /**
     * @function AppDate.stringDate
     * @memberof components
     * @desc Форматирует дату в строку для инициализации экземпляра даты. Также доступен в виде метода экземпляра.
     * @param {Object} inputDate - Объект даты.
     * @returns {string} Строка с отформатированной датой.
     * @example <caption>В виде метода экземпляра</caption>
     * const dateInstance = new app.Date(new Date(2019, 10, 1));
     * console.log(dateInstance.stringDate()); //2019-11-1
     *
     * const dateInstance2 = new app.Date(dateInstance.stringDate());
     * console.log(dateInstance.formatDate(), dateInstance2.formatDate()); //В обоих экземплярах одинаковая дата
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Date.stringDate(new Date(2019, 10, 1))); //2019-11-1
     */
    static stringDate(inputDate = util.required('inputDate')) {
        if (!(inputDate instanceof Date)) {
            util.typeError(inputDate, 'inputDate', 'date object');
        }

        const stringYears = inputDate.getFullYear();
        const stringDays = inputDate.getDate();
        const stringMonths = inputDate.getMonth() + 1;
        const stringDate = stringYears + dateScriptDelimiter + stringMonths + dateScriptDelimiter + stringDays;

        return stringDate;
    }

    //TODO:validate date method

    /**
     * @function AppDate.currentDate
     * @memberof components
     * @desc Get-функция. Возвращает экземпляр [AppDate]{@link components.AppDate} с текущей датой.
     * @returns {Object} Экземпляр [AppDate]{@link components.AppDate} с текущей датой.
     * @example
     * console.log(app.Date.currentDate); //Выведет экземпляр модуля даты для текущей даты
     */
    static get currentDate() {
        return currentDate;
    }

    /**
     * @function AppDate.definitions
     * @memberof components
     * @desc Get-функция. Возвращает объект с определениями дат для форматирования.
     * @property {string} [day='dd'] - Определение строки дней.
     * @property {string} [month='mm'] - Определение строки месяцев.
     * @property {string} [year='yyyy'] - Определение строки лет.
     * @property {string} [delimiter='.'] - Определение символа-разделителя частей маски даты.
     * @returns {Object} Объект с определениями дат для форматирования.
     * @example
     * console.log(app.Date.definitions); //Выведет объект с определениями дат для форматирования
     */
    static get definitions() {
        return definitions;
    }

    /**
     * @function AppDate.datePattern
     * @memberof components
     * @desc Get-функция. Возвращает маску даты.
     * @returns {string} Маска даты.
     * @readonly
     * @example
     * console.log(app.Date.datePattern); //'dd.mm.yyyy'
     */
    static get datePattern() {
        return datePattern;
    }
});

currentDate = new AppDate(new Date()); //Инициализация глобального экземпляра даты с датой при загрузке страницы

addModule(moduleName, AppDate);

export default AppDate;
export {AppDate};