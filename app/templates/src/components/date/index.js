import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const dateCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "dates" */ './sync.js'))
        .then(modules => {
            chunks.dates = true;

            const AppDate = modules.default;
            resolve(AppDate);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.dates) {
        dateCallback(resolve);
        return;
    }

    onInit('date', () => {
        dateCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('date');
    });
});

export default importFunc;