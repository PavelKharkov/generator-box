import './style.scss';

import AppWindow from 'Layout/window';

//Опции
const roubleCurrency = '₽';
const roubleCurrencyReplacer = 'Р';

const currencyClass = 'currency';

//Проверка отображения браузером символа рубля
AppWindow.on('load', () => {
    const currentCurrency = document.querySelector(`.${currencyClass}`);
    if (!currentCurrency) return;

    const currencyText = currentCurrency.textContent.trim();
    if (currencyText !== roubleCurrency) return;

    const testElement = document.createElement('div');
    testElement.classList.add(currencyClass);
    testElement.textContent = currencyText;
    testElement.style.position = 'absolute';
    testElement.style.top = '-9999px';
    testElement.style.left = '-9999px';
    testElement.style.fontFamily = 'Arial';
    testElement.style.fontSize = '200px';
    testElement.style.fontWeight = '400';
    document.body.append(testElement);

    if ((testElement.offsetWidth <= 90) || //Android
        (testElement.offsetWidth > 140) //PC
    ) {
        document.querySelectorAll(`.${currencyClass}`).forEach(currencyEl => {
            currencyEl.textContent = roubleCurrencyReplacer;
        });
    }

    //Корректный символ рубля будет сохранён в атрибуте data-currency в body
    document.documentElement.dataset.currency = testElement.textContent;
    testElement.remove();
});