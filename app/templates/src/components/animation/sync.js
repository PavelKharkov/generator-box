import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'animation';

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

const animatingClass = 'animating';

//Опции по умолчанию
const defaultOptions = {
    duration: transitionDuration
};

/**
 * @class AppAnimation
 * @memberof components
 * @classdesc Модуль анимации.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Анимирующийся элемент.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность анимации.
 * @param {Function} [options.easing] - Функция плавности анимации.
 * @example
 * const easeOutQuad = value => { //Функция плавности анимации. Принимает значение от 0 до 1, что соотвествует проценту завершения анимации
 *     const reverseValue = 1 - value;
 *     return 1 - (reverseValue * reverseValue);
 * };
 *
 * const animationInstance = new app.Animation(document.querySelector('div'), {
 *     duration: 500, //При указании этих опции при инициализаци экземпляра, они будут применены по умолчанию ко всем анимациям экземпляра
 *     easing: easeOutQuad
 * });
 * animationInstance.animate({
 *     property: 'width',
 *     value: 300
 * });
 * console.log(animationInstance.isAnimating); //true
 * setTimeout(() => {
 *     animationInstance.stop(); //Значения анимированных свойств останутся в том положении, в котором были во время остановки анимации
 * }, 200);
 *
 * @example
 * const bodyAnimationInstance = new app.Animation(document.querySelector('body'));
 * bodyAnimationInstance.animate({ //Анимация прокрутки к началу страницы
 *     duration: 300,
 *     property: 'scrollTop',
 *     value: 0
 * });
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-animation-options='{"duration": 300}'></div>
 */
const AppAnimation = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Добавление опций из data-атрибута
        const animationOptionsData = el.dataset.animationOptions;
        if (animationOptionsData) {
            const dataOptions = util.stringToJSON(animationOptionsData);
            if (!dataOptions) util.error('incorrect data-animation-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppAnimation#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let isAnimating = false;

        /**
         * @member {boolean} AppAnimation#isAnimating
         * @memberof components
         * @desc Указывает, анимируется ли элемент в данный момент.
         * @readonly
         */
        Object.defineProperty(this, 'isAnimating', {
            enumerable: true,
            get: () => isAnimating
        });

        //Объект для хранения промисов анимации
        const animationPromises = {};

        //Объект для хранения идентификаторов функций requestAnimationFrame
        const animationFrames = {};

        /**
         * @function stopFunction
         * @desc Внутренняя функция для остановки анимации.
         * @param {Object} [options={}] - Опции функции остановки анимации.
         * @param {string} [options.property] - Анимируемое свойство, которое надо остановить.
         * @returns {undefined}
         * @ignore
         */
        const stopFunction = (options = {}) => {
            if (!util.isObject(options)) {
                util.typeError(options, 'options', 'plain object');
            }

            if (options.property) {
                el.classList.remove(`${animatingClass}-${options.property}`);
                if (animationFrames[options.property]) {
                    cancelAnimationFrame(animationFrames[options.property]);
                }
            } else {
                Object.keys(animationFrames).forEach(animationFrame => {
                    cancelAnimationFrame(animationFrames[animationFrame]);
                    el.classList.remove(`${animatingClass}-${animationFrame}`);
                });
            }

            el.classList.remove(animatingClass);
            isAnimating = false;
        };

        this.on({
            /**
             * @event AppAnimation#stop
             * @memberof components
             * @desc Останавливает запущенные анимации.
             * @param {Object} [options={}] - Опции.
             * @param {string} [options.property] - Стилевое свойство или свойства scrollTop или scrollLeft, анимацию которого нужно остановить. Если данный параметр не передан, то останавливаются все анимации элемента.
             * @returns {undefined}
             * @example
             * animationInstance.on('stop', () => {
             *     console.log('Анимация остановлена');
             * });
             * animationInstance.animate({
             *     property: 'width',
             *     value: 300
             * });
             * setTimeout(() => {
             *     animationInstance.stop({
             *         property: 'width' //В данном случае указывать не обязательно
             *     });
             * }, 200);
             */
            stop(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if ((typeof options.property !== 'string') || !options.property) {
                    util.typeError(options.property, 'options.property', 'non-empty string');
                }

                stopFunction(options);

                //Вызывать функции reject промисов анимации только при ручной остановке анимации
                if (options.property) {
                    animationPromises[options.property][1]();
                    return;
                }

                Object.keys(animationFrames).forEach(animationFrame => {
                    animationPromises[animationFrame][1]();
                });
            },

            /**
             * @event AppAnimation#animate
             * @memberof components
             * @desc Запускает анимацию свойства элемента. После завершения анимации вызывает событие [animated]{@link components.AppAnimation#event:animated}.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации.
             * @param {Function} [options.easing] - Функция плавности для анимации.
             * @param {string} [options.property] - Стилевое свойство или свойства scrollTop или scrollLeft, значение которого нужно анимировать.
             * @param {number} [options.value] - Конечное анимированное значение.
             * @fires components.AppAnimation#animated
             * @throws Выводит ошибку при попытке анимировать свойстьва размеров у скрытого элемента.
             * @returns {Object} Объект с методами resolve, reject и finally или null, если анимация не происходила.
             * @example
             * animationInstance.on('animate', () => {
             *     console.log('Элемент анимируется');
             * });
             * const animationObject = animationInstance.animate({
             *     duration: 300,
             *     property: 'scrollLeft',
             *     value: 500
             * });
             * if (animationObject !== null) { //Проверка, что анимация началась успешно
             *     animationObject.reject(() => { //Вызывается, если анимация не была завершена до конца
             *         console.log('Анимация остановлена');
             *     });
             *     animationObject.resolve(() => { //Вызывается при успешном завершении анимации
             *         console.log('Анимация завершена успешно');
             *     });
             *     animationObject.finally(() => { //Вызывается при любом завершении анимации после вызова функций resolve и reject
             *         console.log('Анимация закончена');
             *     });
             * }
             */
            animate(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                //Проверка обязательных опций
                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }
                if ((typeof options.property !== 'string') || !options.property) {
                    util.typeError(options.property, 'options.property', 'non-empty string');
                }
                if (!Number.isFinite(options.value)) {
                    util.typeError(options.value, 'options.value', 'number');
                }

                const isNotStyle = /^(scrollTop|scrollLeft)$/.test(options.property);

                const styleProperty = String(isNotStyle ? el[options.property] : window.getComputedStyle(el)[options.property]);
                const propertyNumber = Number.parseFloat(styleProperty);
                if (propertyNumber === options.value) return null; //Не анимировать, если значение свойства соответствует новому значению

                if (Number.isNaN(propertyNumber)) {
                    if (!el.offsetParent) {
                        const isSizeProperty = /^(width|height)$/.test(options.property);
                        if (isSizeProperty) util.error(`property '${options.property}' isn't animatable on hidden elements`);
                    }

                    util.error(`property '${options.property}' isn't animatable`);
                }

                //Остановить предыдущую анимацию свойства
                stopFunction({
                    property: options.property
                });

                let animationStart = 0;
                el.classList.add(animatingClass);
                el.classList.add(`${animatingClass}-${options.property}`);
                isAnimating = true;

                const valueDelta = options.value - propertyNumber;
                const unit = styleProperty.slice(String(propertyNumber).length);

                /**
                 * @function stepFunction
                 * @desc Обновляет шаг анимации.
                 * @param {number} timestamp - Идентификатор шага анимации.
                 * @returns {undefined}
                 * @ignore
                 */
                const stepFunction = timestamp => {
                    if (!animationStart) {
                        animationStart = timestamp;
                    }
                    const progress = Math.min(timestamp - animationStart, options.duration);
                    const percentProgress = (options.duration === 0) ? 1 : ((100 * progress) / options.duration / 100);

                    const easingValue = (typeof options.easing === 'function') ? options.easing(percentProgress) : percentProgress;
                    const updatedValue = propertyNumber + (valueDelta * easingValue) + unit;
                    if (isNotStyle) {
                        el[options.property] = updatedValue;
                    } else {
                        el.style[options.property] = updatedValue;
                    }

                    if (progress < options.duration) {
                        animationFrames[options.property] = requestAnimationFrame(stepFunction);
                        return;
                    }

                    stopFunction({
                        property: options.property
                    });

                    animationPromises[options.property][0]();
                };

                //Настройка промиса для анимации
                animationPromises[options.property] = [];
                const animationPromise = new Promise((resolve, reject) => {
                    animationPromises[options.property][0] = resolve;
                    animationPromises[options.property][1] = reject;

                    animationFrames[options.property] = requestAnimationFrame(stepFunction);
                });

                //Настройка объекта с функциями обратного вызова, возвращаемого при старте анимации
                let promiseResolveCallback;
                let promiseRejectCallback;
                let promiseFinallyCallback;
                const returnObj = {
                    resolve(resolveCallback) {
                        promiseResolveCallback = resolveCallback;
                        return this;
                    },
                    reject(rejectCallback) {
                        promiseRejectCallback = rejectCallback;
                        return this;
                    },
                    finally(finallyCallback) {
                        promiseFinallyCallback = finallyCallback;
                        return this;
                    }
                };

                //Вызов функций обратного вызова, вызываемых после завершения анимации
                animationPromise
                    .then(
                        () => {
                            if (typeof promiseResolveCallback !== 'function') return;
                            promiseResolveCallback();
                        },
                        () => {
                            if (typeof promiseRejectCallback !== 'function') return;
                            promiseRejectCallback();
                        }
                    )
                    .finally(() => {
                        if (typeof promiseFinallyCallback === 'function') {
                            promiseFinallyCallback();
                        }
                        this.emit('animated');
                    });

                return returnObj;
            }
        });

        this.onSubscribe([
            /**
             * @event AppAnimation#animated
             * @memberof components
             * @desc Вызывается, когда анимация полностью завершена.
             * @returns {undefined}
             * @example
             * animationInstance.on('animated', () => {
             *     console.log('анимация завершена');
             * });
             */
            'animated'
        ]);
    }
});

addModule(moduleName, AppAnimation);

export default AppAnimation;
export {AppAnimation};