import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const animationCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const AppAnimation = modules.default;
            resolve(AppAnimation);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        animationCallback(resolve);
        return;
    }

    onInit('animation', () => {
        animationCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('animation');
    });
});

export default importFunc;