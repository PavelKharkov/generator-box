import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';
import AppAnimation from '../sync.js';

describe('AppAnimation', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.animate()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value, [throwsError, 'parameter \'options.property\' must be a non-empty string']]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(animateFunction, value) {
                return animateFunction(value);
            },
            setupFunction() {
                const animationEl = document.createElement('div');
                document.body.append(animationEl);

                const animationInstance = new AppAnimation(animationEl);
                const animateFunction = animationInstance.animate;

                return animateFunction;
            }
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const animateOptions = [
                ['duration', 'строка', util.getStyle('--transition-duration'), 'parameter \'options.duration\' must be a number'],
                ['duration', 'NaN', Number.NaN, 'parameter \'options.duration\' must be a number'],
                ['property', 'число', 1, 'parameter \'options.property\' must be a non-empty string'],
                ['property', 'пустая строка', '', 'parameter \'options.property\' must be a non-empty string'],
                ['value', 'строка', '100', 'parameter \'options.value\' must be a number'],
                ['value', 'NaN', Number.NaN, 'parameter \'options.value\' must be a number']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(animateOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const animationEl = document.createElement('div');
                    document.body.append(animationEl);
                    const animationInstance = new AppAnimation(animationEl);

                    const animateConfig = {
                        property: 'width',
                        value: 100
                    };
                    animateConfig[optionName] = value;
                    const animationResult = (() => animationInstance.animate(animateConfig));

                    expect(animationResult).toThrow(errorMessage);
                });
        });

        test('должен возвращать \'null\', если значение текущего анимируемого свойства совпадает с новым значением', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '100px';
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            expect(animationResult).toBeNull();
        });

        test('должен выбрасывать ошибку при попытке анимировать неанимируемое свойство', () => {
            const animationEl = document.createElement('div');
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'display',
                value: 100
            };
            const animationResult = (() => animationInstance.animate(animateConfig));

            expect(animationResult).toThrow('property \'display\' isn\'t animatable');
        });

        const sizeOptions = [
            'width',
            'height'
        ];
        test.each(sizeOptions)('должен выбрасывать ошибку при попытке анимировать свойство \'%s\' скрытого элемента', property => {
            const animationEl = document.createElement('div');
            animationEl.style.display = 'none';
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property,
                value: 100
            };
            const animationResult = (() => animationInstance.animate(animateConfig));

            expect(animationResult).toThrow(`property '${property}' isn't animatable on hidden elements`);
        });

        test('должен возвращать объект с функциями обратного вызова при успешном старте анимации', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            expect(typeof animationResult.resolve).toBe('function');
            expect(typeof animationResult.reject).toBe('function');
            expect(typeof animationResult.finally).toBe('function');
        });

        test('должен добавлять общий класс анимируемого элемента', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            animationInstance.animate(animateConfig);

            expect(animationEl.classList).toContain('animating');
        });

        test('должен добавлять класс, соответствующий анимируемому свойству элемента', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            animationInstance.animate(animateConfig);

            expect(animationEl.classList).toContain('animating-width');
        });

        test('должен корректно обновлять значение свойства \'isAnimating\'', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            animationInstance.animate(animateConfig);

            expect(animationInstance.isAnimating).toBe(true);
        });

        test('должен вызывать функцию обратного вызова после успешного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const resolveFunction = jest.fn();
                animationResult.resolve(() => {
                    resolveFunction();
                    expect(resolveFunction).toHaveBeenCalledWith();
                    done();
                });

                expect(resolveFunction).not.toHaveBeenCalled();

                jest.runAllTimers();
            });
        });

        test('должен вызывать функцию обратного вызова \'finally\' после успешного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const finallyFunction = jest.fn();
                animationResult.finally(() => {
                    finallyFunction();
                    expect(finallyFunction).toHaveBeenCalledWith();
                    done();
                });

                jest.runAllTimers();

                expect(finallyFunction).not.toHaveBeenCalled();
            });
        });

        test('должен корректно обновлять стили элемента после успешного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const elementWidth = window.getComputedStyle(animationEl).width;
                expect(elementWidth).toBe('0px');

                animationResult.resolve(() => {
                    const elementResolveWidth = window.getComputedStyle(animationEl).width;
                    expect(elementResolveWidth).toBe('100px');
                    done();
                });

                jest.runAllTimers();
            });
        });

        //TODO:test animation with easing, test animation non-style prop
    });
});