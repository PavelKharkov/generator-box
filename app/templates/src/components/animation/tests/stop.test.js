import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';
import AppAnimation from '../sync.js';

describe('AppAnimation', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.stop()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(stopFunction, value) {
                return stopFunction(value);
            },
            setupFunction() {
                const animationEl = document.createElement('div');
                document.body.append(animationEl);

                const animationInstance = new AppAnimation(animationEl);
                const stopFunction = animationInstance.stop;

                return stopFunction;
            }
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const stopOptions = [
                ['property', 'число', 1, 'parameter \'options.property\' must be a non-empty string'],
                ['property', 'пустая строка', '', 'parameter \'options.property\' must be a non-empty string']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(stopOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const animationEl = document.createElement('div');
                    document.body.append(animationEl);
                    const animationInstance = new AppAnimation(animationEl);

                    const animateConfig = {
                        property: 'width'
                    };
                    animateConfig[optionName] = value;
                    const stopResult = (() => animationInstance.stop(animateConfig));

                    expect(stopResult).toThrow(errorMessage);
                });
        });

        test('должен выбрасывать ошибку при получении пустого свойства для анимирования', () => {
            const animationEl = document.createElement('div');
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const stopConfig = {
                property: ''
            };
            const stopResult = (() => animationInstance.stop(stopConfig));

            expect(stopResult).toThrow('parameter \'options.property\' must be a non-empty string');
        });

        test('должен завершать воспроизведение анимации после вызова метода \'stop\'', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const stopHalfModifier = 0.5;
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const resolveFunction = jest.fn();
                const rejectFunction = jest.fn();
                animationResult.resolve(() => {
                    resolveFunction();
                });
                animationResult.reject(() => {
                    rejectFunction();
                });
                animationResult.finally(() => {
                    expect(resolveFunction).not.toHaveBeenCalled();
                    expect(rejectFunction).toHaveBeenCalledWith();
                    done();
                });

                const duration = Number.parseInt(util.getStyle('--transition-duration'), 10);
                jest.advanceTimersByTime((duration * stopHalfModifier) + 1);
                animationInstance.stop();
                jest.advanceTimersByTime((duration * stopHalfModifier) + 1);
            });
        });

        test('должен корректно обновлять значение свойства \'isAnimating\'', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const stopHalfModifier = 0.5;
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                animationResult.reject(() => {
                    expect(animationInstance.isAnimating).toBe(false);
                    done();
                });

                const duration = Number.parseInt(util.getStyle('--transition-duration'), 10);
                jest.advanceTimersByTime((duration * stopHalfModifier) + 1);
                animationInstance.stop();
                jest.advanceTimersByTime((duration * stopHalfModifier) + 1);
            });
        });

        test('должен вызывать функцию обратного вызова после неоконченного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const rejectFunction = jest.fn();
                animationResult.reject(() => {
                    rejectFunction();
                    expect(rejectFunction).toHaveBeenCalledWith();
                    done();
                });

                expect(rejectFunction).not.toHaveBeenCalled();

                animationInstance.stop();

                jest.runAllTimers();
            });
        });

        test('должен вызывать функцию обратного вызова \'finally\' после неоконченного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const finallyFunction = jest.fn();
                animationResult.finally(() => {
                    finallyFunction();
                    expect(finallyFunction).toHaveBeenCalledWith();
                    done();
                });

                expect(finallyFunction).not.toHaveBeenCalled();

                animationInstance.stop();

                jest.runAllTimers();
            });
        });

        test('должен корректно обновлять стили элемента после неоконченного завершения анимации', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const stopModifier = 0.5;
            const animationResult = animationInstance.animate(animateConfig);

            await new Promise(done => {
                const elementWidth = window.getComputedStyle(animationEl).width;
                expect(elementWidth).toBe('0px');

                animationResult.finally(() => {
                    const elementRejectWidth = Math.ceil(Number.parseFloat(window.getComputedStyle(animationEl).width));
                    expect(elementRejectWidth).not.toBe('0px');
                    expect(elementRejectWidth).not.toBe('100px');
                    done();
                });

                jest.advanceTimersByTime(Number.parseInt(util.getStyle('--transition-duration'), 10) * stopModifier);

                animationInstance.stop();
            });
        });

        test('должен корректно завершать анимации, которые были указаны через параметры вызова метода \'stop\'', async () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            animationEl.style.height = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateWidthConfig = {
                property: 'width',
                value: 100
            };
            const animateHeightConfig = {
                property: 'height',
                value: 100
            };
            const animationWidthResult = animationInstance.animate(animateWidthConfig);
            const animationHeightResult = animationInstance.animate(animateHeightConfig);

            await new Promise(done => {
                const resolveWidthFunction = jest.fn();
                const resolveHeightFunction = jest.fn();

                animationWidthResult.finally(() => {
                    resolveWidthFunction();
                    expect(resolveHeightFunction).toHaveBeenCalledWith();
                    expect(resolveWidthFunction).toHaveBeenCalledWith();
                    done();
                });
                animationHeightResult.finally(() => {
                    resolveHeightFunction();
                    expect(resolveHeightFunction).toHaveBeenCalledWith();
                    expect(resolveWidthFunction).not.toHaveBeenCalled();

                    jest.runAllTimers();
                });

                animationInstance.stop({
                    property: 'height'
                });
            });
        });

        test('должен удалять общий класс анимируемого элемента', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animatingClass = 'animating';
            animationInstance.animate(animateConfig);

            expect(animationEl.classList).toContain(animatingClass);
            animationInstance.stop();
            expect(animationEl.classList).not.toContain(animatingClass);
        });

        test('должен удалять класс, соответствующий анимируемому свойству элемента', () => {
            const animationEl = document.createElement('div');
            animationEl.style.width = '0'; //имитация ширины элемента для jsdom
            document.body.append(animationEl);
            const animationInstance = new AppAnimation(animationEl);

            const animateConfig = {
                property: 'width',
                value: 100
            };
            const animatingClass = 'animating-width';
            animationInstance.animate(animateConfig);

            expect(animationEl.classList).toContain(animatingClass);
            animationInstance.stop();
            expect(animationEl.classList).not.toContain(animatingClass);
        });
    });
});