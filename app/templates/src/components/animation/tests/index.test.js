import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import util from 'Layout/main';
import AppAnimation from '../sync.js';

describe('AppAnimation', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const animationPromise = require('..').default;

        expect(animationPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            return require('..').default.then(AppAnimationTest => {
                expect(AppAnimationTest.name).toBe(AppAnimation.name);
                done();
            });
        });
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalAnimation = window.app.Animation;

        expect(globalAnimation.prototype).toBeInstanceOf(Module);
    });

    const dataTypesAssertions = ['HTMLElement']
        .map(value => [value, [isInstance, AppAnimation]]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.animation;
            return new AppAnimation(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, 'parameter \'element\' must be a HTMLElement'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppAnimation\' с переданным значением'
    });

    test('должен сохраняться под именем \'animation\' в свойстве \'modules\' у элементов', () => {
        const animationEl = document.createElement('div');
        document.body.append(animationEl);

        new AppAnimation(animationEl); /* eslint-disable-line no-new */
        const animationInstanceProp = animationEl.modules.animation;

        expect(animationInstanceProp).toBeInstanceOf(AppAnimation);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const animationPropsArray = [
            'isAnimating',
            'stop',
            'animate'
        ];
        test.each(animationPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const animationEl = document.createElement('div');
            document.body.append(animationEl);

            const animationInstance = new AppAnimation(animationEl);

            const currentProp = animationInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const animationEventsArray = [
            'stop',
            'animate'
        ];
        test.each(animationEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const animationEl = document.createElement('div');
            document.body.append(animationEl);

            const animationInstance = new AppAnimation(animationEl);
            const eventsArray = animationInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const animationOptionsMap = [['duration', Number.parseInt(util.getStyle('--transition-duration'), 10)]];
        test.each(animationOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const animationEl = document.createElement('div');
            document.body.append(animationEl);

            const animationInstance = new AppAnimation(animationEl);
            const instanceOptions = animationInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    describe('[data-animation-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-animation-options\'', () => {
            const animationEl = document.createElement('div');
            animationEl.dataset.animationOptions = '{"duration": 200}';
            document.body.append(animationEl);

            const animationInstance = new AppAnimation(animationEl);
            const instanceOptions = animationInstance.options;

            expect(instanceOptions.duration).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-animation-options\'', () => {
            const animationEl = document.createElement('div');
            animationEl.dataset.animationOptions = 'true';
            document.body.append(animationEl);

            const animationInstance = (() => new AppAnimation(animationEl));

            expect(animationInstance).toThrow('incorrect data-animation-options format');
        });
    });

    test('должен выводить ошибку при попытке поменять свойство \'isAnimating\'', () => {
        const animationEl = document.createElement('div');
        document.body.append(animationEl);

        const animationInstance = new AppAnimation(animationEl);

        const isAnimating = (() => {
            animationInstance.isAnimating = false;
        });
        expect(isAnimating).toThrow('Cannot set property isAnimating of #<ImmutableClass> which has only a getter');
    });
});