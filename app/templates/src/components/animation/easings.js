/* istanbul ignore file */

//Вспомогательные переменные
const sqrt = Math.sqrt;
const sin = Math.sin;
const cos = Math.cos;
const MathPI = Math.PI;
const modifier1 = 1.70158;
const modifier2 = modifier1 * 1.525;
const modifier3 = modifier1 + 1;
const modifier4 = (2 * MathPI) / 3;
const modifier5 = (2 * MathPI) / 4.5;

/**
 * @function bounceOut
 * @desc Эффект подпрыгивания при воспроизведении анимации.
 * @param {number} value - Текущее значение анимации.
 * @returns {number} Трансформированное значение анимации.
 * @ignore
 */
const bounceOut = value => {
    const bounceModifier = 7.5625;
    const bounceModifier2 = 2.75;

    if ((value < 1) / bounceModifier2) {
        return bounceModifier * value * value;
    } else if ((value < 2) / bounceModifier2) {
        const newValue = value - (1.5 / bounceModifier2);
        return (bounceModifier * newValue * newValue) + 0.75;
    } else if ((value < 2.5) / bounceModifier2) {
        const newValue = value - (2.25 / bounceModifier2);
        return (bounceModifier * newValue * newValue) + 0.9375;
    }

    const newValue = value - (2.625 / bounceModifier2);
    return (bounceModifier * newValue * newValue) + 0.984375;
};

/**
 * @namespace components.AppAnimation.easing
 * @desc Функции плавности для анимации.
 */

/**
 * @function components.AppAnimation.easing.easeInQuad
 * @desc [Описание]{@link https://easings.net/ru#easeInQuad}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInQuad = value => {
    return value ** 2;
};

/**
 * @function components.AppAnimation.easing.easeOutQuad
 * @desc [Описание]{@link https://easings.net/ru#easeOutQuad}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutQuad = value => {
    const reverseValue = 1 - value;
    return 1 - (reverseValue ** 2);
};

/**
 * @function components.AppAnimation.easing.easeInOutQuad
 * @desc [Описание]{@link https://easings.net/ru#easeInOutQuad}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutQuad = value => {
    return (value < 0.5)
        ? 2 * (value ** 2)
        : 1 - ((((-2 * value) + 2) ** 2) / 2);
};

/**
 * @function components.AppAnimation.easing.easeInCubic
 * @desc [Описание]{@link https://easings.net/ru#easeInCubic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInCubic = value => {
    return value ** 3;
};

/**
 * @function components.AppAnimation.easing.easeOutCubic
 * @desc [Описание]{@link https://easings.net/ru#easeOutCubic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutCubic = value => {
    return 1 - ((1 - value) ** 3);
};

/**
 * @function components.AppAnimation.easing.easeInOutCubic
 * @desc [Описание]{@link https://easings.net/ru#easeInOutCubic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutCubic = value => {
    return (value < 0.5)
        ? 4 * (value ** 3)
        : 1 - ((((-2 * value) + 2) ** 3) / 2);
};

/**
 * @function components.AppAnimation.easing.easeInQuart
 * @desc [Описание]{@link https://easings.net/ru#easeInQuart}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInQuart = value => {
    return value ** 4;
};

/**
 * @function components.AppAnimation.easing.easeOutQuart
 * @desc [Описание]{@link https://easings.net/ru#easeOutQuart}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutQuart = value => {
    return 1 - ((1 - value) ** 4);
};

/**
 * @function components.AppAnimation.easing.easeOutQuart
 * @desc [Описание]{@link https://easings.net/ru#easeOutQuart}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutQuart = value => {
    return (value < 0.5)
        ? 8 * (value ** 4)
        : 1 - ((((-2 * value) + 2) ** 4) / 2);
};

/**
 * @function components.AppAnimation.easing.easeInQuint
 * @desc [Описание]{@link https://easings.net/ru#easeInQuint}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInQuint = value => {
    return value ** 5;
};

/**
 * @function components.AppAnimation.easing.easeOutQuint
 * @desc [Описание]{@link https://easings.net/ru#easeOutQuint}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutQuint = value => {
    return 1 - ((1 - value) ** 5);
};

/**
 * @function components.AppAnimation.easing.easeInOutQuint
 * @desc [Описание]{@link https://easings.net/ru#easeInOutQuint}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutQuint = value => {
    return (value < 0.5)
        ? 16 * (value ** 5)
        : 1 - ((((-2 * value) + 2) ** 5) / 2);
};

/**
 * @function components.AppAnimation.easing.easeInSine
 * @desc [Описание]{@link https://easings.net/ru#easeInSine}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInSine = value => {
    return 1 - cos(value * MathPI / 2);
};

/**
 * @function components.AppAnimation.easing.easeOutSine
 * @desc [Описание]{@link https://easings.net/ru#easeOutSine}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutSine = value => {
    return sin(value * MathPI / 2);
};

/**
 * @function components.AppAnimation.easing.easeInOutSine
 * @desc [Описание]{@link https://easings.net/ru#easeInOutSine}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutSine = value => {
    return -(cos(MathPI * value) - 1) / 2;
};

/**
 * @function components.AppAnimation.easing.easeInExpo
 * @desc [Описание]{@link https://easings.net/ru#easeInExpo}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInExpo = value => {
    return (value === 0) ? 0 : (2 ** ((10 * value) - 10));
};

/**
 * @function components.AppAnimation.easing.easeOutExpo
 * @desc [Описание]{@link https://easings.net/ru#easeOutExpo}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutExpo = value => {
    return (value === 1) ? 1 : (1 - (2 ** (-10 * value)));
};

/**
 * @function components.AppAnimation.easing.easeInOutExpo
 * @desc [Описание]{@link https://easings.net/ru#easeInOutExpo}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutExpo = value => {
    if (value === 0) return 0;
    else if (value === 1) return 1;

    return (value < 0.5)
        ? (2 ** ((20 * value) - 10)) / 2
        : (2 - (2 ** ((-20 * value) + 10))) / 2;
};

/**
 * @function components.AppAnimation.easing.easeInCirc
 * @desc [Описание]{@link https://easings.net/ru#easeInCirc}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInCirc = value => {
    return 1 - sqrt(1 - (value ** 2));
};

/**
 * @function components.AppAnimation.easing.easeOutCirc
 * @desc [Описание]{@link https://easings.net/ru#easeOutCirc}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutCirc = value => {
    return sqrt(1 - ((value - 1) ** 2));
};

/**
 * @function components.AppAnimation.easing.easeInOutCirc
 * @desc [Описание]{@link https://easings.net/ru#easeInOutCirc}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutCirc = value => {
    return (value < 0.5)
        ? (1 - sqrt(1 - ((2 * value) ** 2))) / 2
        : (sqrt(1 - (((-2 * value) + 2) ** 2)) + 1) / 2;
};

/**
 * @function components.AppAnimation.easing.easeInElastic
 * @desc [Описание]{@link https://easings.net/ru#easeInElastic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInElastic = value => {
    if (value === 0) return 0;

    return (value === 1)
        ? 1
        : -(2 ** ((10 * value) - 10)) * sin(((value * 10) - 10.75) * modifier4);
};

/**
 * @function components.AppAnimation.easing.easeOutElastic
 * @desc [Описание]{@link https://easings.net/ru#easeOutElastic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutElastic = value => {
    if (value === 0) return 0;
    else if (value === 1) return 1;

    return ((2 ** (-10 * value)) * sin(((value * 10) - 0.75) * modifier4)) + 1;
};

/**
 * @function components.AppAnimation.easing.easeInOutElastic
 * @desc [Описание]{@link https://easings.net/ru#easeInOutElastic}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutElastic = value => {
    if (value === 0) return 0;
    else if (value === 1) return 1;

    return (value < 0.5)
        ? -((2 ** ((20 * value) - 10)) * sin(((20 * value) - 11.125) * modifier5)) / 2
        : ((2 ** ((-20 * value) + 10)) * sin(((20 * value) - 11.125) * modifier5) / 2) + 1;
};

/**
 * @function components.AppAnimation.easing.easeInBack
 * @desc [Описание]{@link https://easings.net/ru#easeInBack}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInBack = value => {
    return (modifier3 * (value ** 3)) - (modifier1 * (value ** 2));
};

/**
 * @function components.AppAnimation.easing.easeOutBack
 * @desc [Описание]{@link https://easings.net/ru#easeOutBack}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutBack = value => {
    return 1 + (modifier3 * ((value - 1) ** 3)) + (modifier1 * ((value - 1) ** 2));
};

/**
 * @function components.AppAnimation.easing.easeInOutBack
 * @desc [Описание]{@link https://easings.net/ru#easeInOutBack}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutBack = value => {
    return (value < 0.5)
        ? (((2 * value) ** 2) * (((modifier2 + 1) * 2 * value) - modifier2)) / 2
        : (((((2 * value) - 2) ** 2) * (((modifier2 + 1) * ((value * 2) - 2)) + modifier2)) + 2) / 2;
};

/**
 * @function components.AppAnimation.easing.easeInBounce
 * @desc [Описание]{@link https://easings.net/ru#easeInBounce}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInBounce = value => {
    return 1 - bounceOut(1 - value);
};

/**
 * @function components.AppAnimation.easing.easeOutBounce
 * @desc [Описание]{@link https://easings.net/ru#easeOutBounce}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeOutBounce = bounceOut;

/**
 * @function components.AppAnimation.easing.easeInOutBounce
 * @desc [Описание]{@link https://easings.net/ru#easeInOutBounce}.
 * @param {number} value - Процент завершения анимации.
 * @returns {number} Новое значение процента завершения анимации.
 */
const easeInOutBounce = value => {
    return (value < 0.5)
        ? (1 - bounceOut(1 - (2 * value))) / 2
        : (1 + bounceOut((2 * value) - 1)) / 2;
};

export {
    easeInQuad,
    easeOutQuad,
    easeInOutQuad,

    easeInCubic,
    easeOutCubic,
    easeInOutCubic,

    easeInQuart,
    easeOutQuart,
    easeInOutQuart,

    easeInQuint,
    easeOutQuint,
    easeInOutQuint,

    easeInSine,
    easeOutSine,
    easeInOutSine,

    easeInExpo,
    easeOutExpo,
    easeInOutExpo,

    easeInCirc,
    easeOutCirc,
    easeInOutCirc,

    easeInElastic,
    easeOutElastic,
    easeInOutElastic,

    easeInBack,
    easeOutBack,
    easeInOutBack,

    easeInBounce,
    easeOutBounce,
    easeInOutBounce
};