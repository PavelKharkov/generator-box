import {isInstance, throwsError, dataTypesCheck} from 'Base/scripts/test';

import Module from 'Components/module';
import util from 'Layout/main';

//Инициализация тестового элемента с атрибутом data-alert
const alertWithDataEl = document.createElement('div');
alertWithDataEl.dataset.alert = '';
document.body.append(alertWithDataEl);

const AppAlert = require('../sync.js').default;

describe('AppAlert', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const alertPromise = require('..').default;

        expect(alertPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            return require('..').default.then(AppAlertTest => {
                expect(AppAlertTest.name).toBe(AppAlert.name);
                done();
            });
        });
    });

    test('должен сохраняться в глобальной переменной приложения', () => {
        const globalAlert = window.app.Alert;

        expect(globalAlert.prototype).toBeInstanceOf(Module);
    });

    const htmlStringError = [throwsError, 'HTML-string must be instance of HTMLElement'];
    const dataTypesAssertions = [
        ['HTMLElement', [isInstance, AppAlert]],
        ['string number', htmlStringError],
        ['empty string', htmlStringError],
        ['space', htmlStringError],
        ['string', htmlStringError],
        ['HTML string', [isInstance, AppAlert]]
    ];
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.alert;
            return new AppAlert(value);
        },
        dataTypesAssertions,
        defaultValue: [throwsError, 'parameter \'element\' must be a HTMLElement or HTML-string'],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppAlert\' с переданным значением'
    });

    test('должен сохраняться под именем \'alert\' в свойстве \'modules\' у элементов', () => {
        const alertEl = document.createElement('div');
        document.body.append(alertEl);

        new AppAlert(alertEl); /* eslint-disable-line no-new */
        const alertInstanceProp = alertEl.modules.alert;

        expect(alertInstanceProp).toBeInstanceOf(AppAlert);
    });

    test('должен сохраняться под именем \'alert\' в свойстве \'modules\' при инициализации через HTML строку', () => {
        const alertHtml = '<div></div>';

        const alertInstance = new AppAlert(alertHtml);
        const alertInstanceEl = alertInstance.alertEl;
        const alertInstanceProp = alertInstanceEl.modules.alert;

        expect(alertInstanceProp).toBeInstanceOf(AppAlert);
    });

    test('должен выбрасывать ошибку при потытке инициализации через некорректную HTML строку', () => {
        const alertInvalidHtml = '<div';

        const alertInstance = (() => new AppAlert(alertInvalidHtml));

        expect(alertInstance).toThrow('HTML-string must be instance of HTMLElement');
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-alert\'', () => {
        const alertInstance = alertWithDataEl.modules.alert;

        expect(alertInstance).toBeInstanceOf(AppAlert);
    });

    describe('доступные свойства и методы экземпляра', () => {
        const alertPropsArray = [
            'alertEl',
            'isShown',
            'show',
            'hide'
        ];
        test.each(alertPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const currentProp = alertInstance[property];
            expect(currentProp).toBeDefined();
        });
    });

    describe('доступные события экземпляра', () => {
        const alertEventsArray = [
            'show',
            'shown',
            'hide',
            'hidden'
        ];
        test.each(alertEventsArray)('у экземпляра должно быть доступно событие \'%s\' по умолчанию', event => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);
            const eventsArray = alertInstance.events();

            expect(eventsArray).toContain(event);
        });
    });

    describe('опции по умолчанию', () => {
        const alertOptionsMap = [
            ['duration', Number.parseInt(util.getStyle('--transition-duration'), 10)],
            ['dismissible', true]
        ];
        test.each(alertOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);
            const instanceOptions = alertInstance.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    //TODO:test options types, test timeout option & template method

    describe('[data-alert-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-alert-options\'', () => {
            const alertEl = document.createElement('div');
            alertEl.dataset.alertOptions = '{"duration": 200}';
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);
            const instanceOptions = alertInstance.options;

            expect(instanceOptions.duration).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-alert-options\'', () => {
            const alertEl = document.createElement('div');
            alertEl.dataset.alertOptions = 'true';
            document.body.append(alertEl);

            const alertInstance = (() => new AppAlert(alertEl));

            expect(alertInstance).toThrow('incorrect data-alert-options format');
        });
    });

    describe('isShown', () => {
        test('должен корректно устанавливать свойство, если уведомление имеет класс \'show\' при инициализации', () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);

            expect(alertInstance.isShown).toBe(true);
        });

        test('должен корректно устанавливать свойство, если уведомление не имеет класса \'show\' при инициализации', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);

            expect(alertInstance.isShown).toBe(false);
        });

        test('должен выводить ошибку при попытке поменять свойство', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);
            const isShown = (() => {
                alertInstance.isShown = false;
            });

            expect(isShown).toThrow('Cannot set property isShown of #<ImmutableClass> which has only a getter');
        });
    });

    describe('alertEl', () => {
        test('должен содержать HTML элемент экземпляра при инициализации через HTML строку', () => {
            const alertHtml = '<div></div>';

            const alertInstance = new AppAlert(alertHtml);
            const alertInstanceEl = alertInstance.alertEl;
            alertInstanceEl.removeAttribute('class');

            expect(alertInstanceEl.outerHTML).toBe(alertHtml);
        });

        test('должен совпадать со свойством \'el\' экземпляра при инициализации через HTML элемент', () => {
            const alertHtml = document.createElement('div');

            const alertInstance = new AppAlert(alertHtml);
            const alertInstanceEl = alertInstance.el;
            const alertInstanceAlertEl = alertInstance.alertEl;

            expect(alertInstanceEl).toBe(alertInstanceAlertEl);
        });

        test('должен выводить ошибку при попытке поменять свойство', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);

            const alertInstance = new AppAlert(alertEl);
            const alertElChange = (() => {
                alertInstance.alertEl = document.createElement('div');
            });

            expect(alertElChange).toThrow('Cannot assign to read only property \'alertEl\' of object \'#<ImmutableClass>\'');
        });
    });

    test('должен вызывать событие \'hide\' при нажатии на элемент внутри уведомления с атрибутом \'data-alert-close\'' +
        ' с включённой опцией \'dismissible\'', () => {
        const alertEl = document.createElement('div');
        alertEl.classList.add('show');
        document.body.append(alertEl);

        const closeButton = document.createElement('button');
        closeButton.dataset.alertClose = '';
        alertEl.append(closeButton);

        const alertInstance = new AppAlert(alertEl);
        const hideFunction = jest.fn();
        alertInstance.on('hide', () => hideFunction());
        alertEl.querySelector('[data-alert-close]').click();

        expect(hideFunction).toHaveBeenCalledWith();
        expect(alertInstance.isShown).toBe(false);
    });
});