import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';
import AppAlert from '../sync.js';

describe('AppAlert', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.show()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(showFunction, value) {
                return showFunction(value);
            },
            setupFunction() {
                const alertEl = document.createElement('div');
                document.body.append(alertEl);

                const alertInstance = new AppAlert(alertEl);
                const showFunction = alertInstance.show;

                return showFunction;
            }
        });

        test('должен корректно обновлять класс элемента', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.show();

            expect(alertEl.classList).toContain('show');
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const stopOptions = [
                ['duration', 'строка', '300', 'parameter \'options.duration\' must be a number'],
                ['duration', 'NaN', Number.NaN, 'parameter \'options.duration\' must be a number']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(stopOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const alertEl = document.createElement('div');
                    document.body.append(alertEl);
                    const alertInstance = new AppAlert(alertEl);

                    const alertConfig = {};
                    alertConfig[optionName] = value;
                    const alertHideResult = (() => {
                        return alertInstance.show(alertConfig);
                    });

                    expect(alertHideResult).toThrow(errorMessage);
                });
        });

        test('должен корректно обновлять встроенный стиль элемента', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.show({
                duration: 300
            });

            expect(window.getComputedStyle(alertEl).transitionDuration).toBe('300ms');
        });

        test('должен вызывать событие \'shown\' после завершения анимации появления', async () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const shownFunction = jest.fn();
            alertInstance.on('shown', () => shownFunction());

            alertInstance.show();

            await new Promise(done => {
                expect(shownFunction).not.toHaveBeenCalled();

                setTimeout(() => {
                    expect(shownFunction).toHaveBeenCalledWith();
                    done();
                }, Number.parseInt(util.getStyle('--transition-duration'), 10) + 1);

                jest.runAllTimers();
            });
        });

        test('должен вызывать событие \'shown\' только с помощью метода \'emit\'', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const shownCall = (() => alertInstance.shown());
            const shownEmit = alertInstance.emit('shown');

            expect(shownCall).toThrow('alertInstance.shown is not a function');
            expect(shownEmit).toBeUndefined();
        });

        test('должен корректно обновлять свойство \'isShown\'', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.show();

            expect(alertInstance.isShown).toBe(true);
        });
    });
});