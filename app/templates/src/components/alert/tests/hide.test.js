import {throwsError, dataTypesCheck} from 'Base/scripts/test';

import util from 'Layout/main';
import AppAlert from '../sync.js';

describe('AppAlert', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.hide()', () => {
        const dataTypesAssertions = [
            'Object',
            'undefined'
        ].map(value => [value]);
        dataTypesCheck({
            dataTypesAssertions,
            defaultValue: [throwsError, 'parameter \'options\' must be a plain object'],
            checkFunction(hideFunction, value) {
                return hideFunction(value);
            },
            setupFunction() {
                const alertEl = document.createElement('div');
                alertEl.classList.add('show');
                document.body.append(alertEl);

                const alertInstance = new AppAlert(alertEl);
                const hideFunction = alertInstance.hide;

                return hideFunction;
            }
        });

        test('должен корректно обновлять класс элемента', () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.hide();

            expect(alertEl.classList).not.toContain('show');
        });

        test('должен выбрасывать ошибку при неверном типе опции \'duration\'', () => {
            const alertEl = document.createElement('div');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const alertHideResult = (() => {
                return alertInstance.hide({
                    duration: '300'
                });
            });

            expect(alertHideResult).toThrow('parameter \'options.duration\' must be a number');
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const stopOptions = [
                ['duration', 'строка', '300', 'parameter \'options.duration\' must be a number'],
                ['duration', 'NaN', Number.NaN, 'parameter \'options.duration\' must be a number']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(stopOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const alertEl = document.createElement('div');
                    document.body.append(alertEl);
                    const alertInstance = new AppAlert(alertEl);

                    const alertConfig = {};
                    alertConfig[optionName] = value;
                    const alertHideResult = (() => {
                        return alertInstance.hide(alertConfig);
                    });

                    expect(alertHideResult).toThrow(errorMessage);
                });
        });

        test('должен корректно обновлять встроенный стиль элемента', () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.hide({
                duration: 300
            });

            expect(window.getComputedStyle(alertEl).transitionDuration).toBe('300ms');
        });

        test('должен вызывать событие \'hidden\' после завершения анимации скрытия', async () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const hiddenFunction = jest.fn();
            alertInstance.on('hidden', () => hiddenFunction());

            alertInstance.hide();

            await new Promise(done => {
                expect(hiddenFunction).not.toHaveBeenCalled();

                setTimeout(() => {
                    expect(hiddenFunction).toHaveBeenCalledWith();
                    done();
                }, Number.parseInt(util.getStyle('--transition-duration'), 10) + 1);

                jest.runAllTimers();
            });
        });

        test('должен удалять элемент уведомления после завершения анимации скрытия уведомления', async () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.hide();

            await new Promise(done => {
                setTimeout(() => {
                    expect(document.querySelector('div')).toBeNull();
                    done();
                }, Number.parseInt(util.getStyle('--transition-duration'), 10) + 1);

                jest.runAllTimers();
            });
        });

        test('должен вызывать событие \'hidden\' только с помощью метода \'emit\'', () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            const hiddenCall = (() => alertInstance.hidden());
            const hiddenEmit = alertInstance.emit('hidden');

            expect(hiddenCall).toThrow('alertInstance.hidden is not a function');
            expect(hiddenEmit).toBeUndefined();
        });

        test('должен корректно обновлять свойство \'isShown\'', () => {
            const alertEl = document.createElement('div');
            alertEl.classList.add('show');
            document.body.append(alertEl);
            const alertInstance = new AppAlert(alertEl);

            alertInstance.hide();

            expect(alertInstance.isShown).toBe(false);
        });
    });
});