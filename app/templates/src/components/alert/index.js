import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let alertTriggered;

const alertCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "toggles" */ './sync.js'))
        .then(modules => {
            chunks.toggles = true;
            alertTriggered = true;

            const AppAlert = modules.default;
            if (resolve) resolve(AppAlert);
        });
};

const initAlert = event => {
    if (alertTriggered) return;

    if (typeof event.target.dataset.alertClose !== 'undefined') {
        event.target.dataset.alertTriggered = '';
    }

    emitInit('alert');
};

const alertTrigger = (alertItems, alertTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (alertItems instanceof Node) ? [alertItems] : alertItems;
    if (items.length === 0) return;

    items.forEach(item => {
        alertTriggers.forEach(trigger => {
            item.addEventListener(trigger, initAlert, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.toggles) {
        alertCallback(resolve);
        return;
    }

    onInit('alert', () => {
        alertCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('alert');
    });

    const alertItems = document.querySelectorAll('[data-alert]');
    alertTrigger(alertItems);
});

export default importFunc;
export {alertTrigger};