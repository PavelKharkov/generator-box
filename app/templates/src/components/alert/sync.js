import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'alert';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

const alertClass = 'alert';
const showClass = 'show';
const fadeClass = 'fade';
const toastClass = '-toast';
const fullwidthClass = '-fullwidth';
const dismissibleClass = `${alertClass}-dismissible`;
const alertCloseData = 'data-alert-close';
const innerPostfix = 'inner';
const innerClasses = `${alertClass}__${innerPostfix} ${alertClass}-${innerPostfix}`;
const closePostfix = 'close';
const closeClasses = `${alertClass}__${closePostfix} close`;
const titlePostfix = 'title';
const titleClasses = `${alertClass}__${titlePostfix} ${alertClass}-${titlePostfix}`;
const iconClasses = `${alertClass}-icon`;
const initializedClass = `${alertClass}-initialized`;

const titleTag = 'h4';

//Опции по умолчанию
const defaultOptions = {
    duration: transitionDuration,
    dismissible: true
};

//Опции шаблона по умолчанию
const defaultTemplateOptions = {
    fade: true
};

/**
 * @class AppAlert
 * @memberof components
 * @classdesc Модуль уведомлений.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {(HTMLElement|string)} element - Элемент уведомления или HTML-строка элемента уведомления.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность переключения анимации уведомления.
 * @param {boolean} [options.dismissible=true] - Является ли уведомление закрываемым.
 * @param {number} [options.timeout] - Через какое время скрывать уведомление после показа.
 * @example
 * const alertInstance = new app.Alert(document.querySelector('.alert-element'), {
 *     duration: 150,
 *     dismissible: false,
 *     timeout: 3000 //Уведомление скроется через 3 сек.
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="alert alert-danger alert-dismissible" data-alert>
 *     <button class="alert__close close" type="button" data-alert-close></button>
 *     <h4 class="alert__title alert-title">Заголовок уведомления</h4>
 *     Текст уведомления
 * </div>
 *
 * <!--
 * data-alert - селектор по умолчанию
 * data-alert-close - селектор для кнопки закрытия уведомления
 * -->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div class="alert" data-alert-options='{"duration": 150}'></div>
 */
const AppAlert = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        const isHTMLElement = el instanceof HTMLElement;

        //Добавление опций из data-атрибута
        if (isHTMLElement) {
            const alertOptionsData = el.dataset.alertOptions;
            if (alertOptionsData) {
                const dataOptions = util.stringToJSON(alertOptionsData);
                if (!dataOptions) util.error('incorrect data-alert-options format');
                util.extend(this.options, dataOptions);
            }
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppAlert#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        //Если элементом экзмепляра является строка, то создаёт элемент из этой строки
        let alertEl = el;
        if (!isHTMLElement) {
            if (typeof el === 'string') {
                const elWrapper = document.createElement('div');
                elWrapper.innerHTML = el.trim();
                alertEl = elWrapper.firstChild;
                if (!(alertEl instanceof HTMLElement)) {
                    util.error('HTML-string must be instance of HTMLElement');
                }

                alertEl.modules = {
                    [moduleName]: this
                };
            } else {
                util.typeError(el, 'element', 'HTMLElement or HTML-string');
            }
        }

        let isShown = alertEl.classList.contains(showClass);

        /**
         * @member {boolean} AppAlert#isShown
         * @memberof components
         * @desc Указывает, показано ли уведомление.
         * @readonly
         * @example
         * const alertInstance = new app.Alert(document.querySelector('.alert-element'));
         * console.log(alertInstance.isShown); //false
         * alertInstance.show();
         * console.log(alertInstance.isShown); //true
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        if (isShown && params.timeout) {
            setTimeout(() => {
                this.hide();
            }, params.timeout);
        }

        /**
         * @member {HTMLElement} AppAlert#alertEl
         * @memberof components
         * @desc Элемент уведомления.
         * @readonly
         * @example
         * const alertInstance = new app.Alert('<div>Уведомление</div>');
         * console.log(alertInstance.el); //Выведет строку, с которой энициализировался экземпляр
         * console.log(alertInstance.alertEl); //Выведет HTML-элемент
         */
        Object.defineProperty(this, 'alertEl', {
            enumerable: true,
            value: alertEl
        });

        this.on({
            /**
             * @event AppAlert#show
             * @memberof components
             * @desc Показывает элемент уведомления. После завершения анимации показа вызывает событие [shown]{@link components.AppAlert#event:shown}. С указанием опции timeout вызывает событие [hide]{@link components.AppAlert#event:hide}.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность переключения анимации уведомления.
             * @param {number} [options.timeout] - Через какое время скрывать уведомление после показа.
             * @fires components.AppAlert#shown
             * @fires components.AppAlert#hide
             * @returns {undefined}
             * @example
             * alertInstance.on('show', () => {
             *     console.log('Элемент уведомления показывается');
             * });
             * alertInstance.show({
             *     duration: 0
             * });
             * setTimeout(() => {
             *     console.log(alertInstance.isShown); //true
             * }, 0);
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                alertEl.style.transitionDuration = `${options.duration}ms`;
                alertEl.classList.add(showClass);
                isShown = true;

                if (options.timeout) {
                    setTimeout(() => {
                        this.hide();
                    }, options.timeout);
                }

                setTimeout(() => {
                    this.emit('shown');
                }, options.duration);
            },

            /**
             * @event AppAlert#hide
             * @memberof components
             * @desc Скрывает элемент уведомления. После завершения анимации скрытия вызывает событие [hidden]{@link components.AppAlert#event:hidden} и удаляет элемент уведомления.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность переключения анимации уведомления.
             * @fires components.AppAlert#hidden
             * @returns {undefined}
             * @example
             * alertInstance.on('hide', () => {
             *     console.log('Элемент уведомления скрывается');
             * });
             * alertInstance.hide({
             *     duration: 100
             * });
             * setTimeout(() => {
             *     console.log(alertInstance.el.offsetParent); //Выведет null, т.к. элемента больше нет на странице
             * }, 100);
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                alertEl.style.transitionDuration = `${options.duration}ms`;
                alertEl.classList.remove(showClass);
                isShown = false;

                setTimeout(() => {
                    this.emit('hidden');
                    alertEl.remove();
                }, options.duration);
            }
        });

        this.onSubscribe([
            /**
             * @event AppAlert#shown
             * @memberof components
             * @desc Вызывается, когда уведомление полностью показано.
             * @returns {undefined}
             * @example
             * alertInstance.on('shown', () => {
             *     console.log('уведомление показано');
             * });
             */
            'shown',

            /**
             * @event AppAlert#hidden
             * @memberof components
             * @desc Вызывается, когда уведомление полностью скрыто.
             * @example
             * alertInstance.on('hidden', () => {
             *     console.log('уведомление скрыто');
             * });
             */
            'hidden'
        ]);

        let closeButtons;

        /**
         * @member {Array.<HTMLElement>} AppAlert#closeButtons
         * @memberof components
         * @desc Коллекция элементов закрытия уведомления.
         * @readonly
         */
        Object.defineProperty(this, 'closeButtons', {
            enumerable: true,
            get: () => closeButtons
        });

        //Инициализация кнопок скрытия уведомления
        if (params.dismissible) {
            closeButtons = [...alertEl.querySelectorAll(`[${alertCloseData}]`)];

            closeButtons.forEach(item => {
                item.addEventListener('click', () => this.hide());
            });
        }

        alertEl.classList.add(initializedClass);

        if (typeof alertEl.dataset.alertTriggered === 'undefined') {
            const triggeredClose = closeButtons && closeButtons.find(item => {
                return (typeof item.dataset.alertTriggered !== 'undefined');
            });
            if (triggeredClose) {
                delete triggeredClose.dataset.alertTriggered;
                this.hide();
            }
        } else {
            delete alertEl.dataset.alertTriggered;
            this.hide();
        }
    }

    /**
     * @function AppAlert.template
     * @memberof components
     * @desc Базовый шабон уведомления.
     * @param {Object} [options] - Опции шаблона.
     * @param {string} [options.color] - Цветовой класс, добавляемый шаблону уведомления.
     * @param {boolean} [options.show] - Добавлять ли класс show.
     * @param {boolean} [options.fade=true] - Добавлять ли класс fade.
     * @param {boolean} [options.toast] - Добавлять ли модификатор toast.
     * @param {boolean} [options.fullwidth] - Добавлять ли модификатор fullwidth.
     * @param {boolean} [options.dismissible] - Добавлять ли класс alert-dismissible.
     * @param {string} [options.role='alert'] - Атрибут role. При включённой опции toast, по умолчанию, ставится роль alertdialog.
     * @param {string} [options.icon] - Иконка уведомления.
     * @param {string} [options.title] - Заголовок уведомления.
     * @param {string} [options.content] - Контент уведомления.
     * @returns {string} HTML-строку с уведомлением.
     * @example
     * const alertInstance = new app.Alert(app.Alert.template({
     *     color: 'danger',
     *     dismissible: true,
     *     content: 'Уведомление с кнопкой скрытия',
     *     icon: app.svg.inline({
     *         id: 'check',
     *         width: 17,
     *         height: 13
     *     })
     * }));
     */
    static template(options = {}) {
        let optionsObject = options;
        if (typeof options === 'string') {
            const content = options;
            optionsObject = {content};
        }

        if (!util.isObject(optionsObject)) {
            util.typeError(optionsObject, 'options', 'plain object');
        }

        util.defaultsDeep(optionsObject, defaultTemplateOptions);

        const alertClasses = [alertClass];
        if (optionsObject.color) alertClasses.push(`${alertClass}-${optionsObject.color}`);
        if (optionsObject.show) alertClasses.push(showClass);
        if (optionsObject.fade) alertClasses.push(fadeClass);
        if (optionsObject.dismissible) alertClasses.push(dismissibleClass);
        if (optionsObject.toast) alertClasses.push(toastClass);
        if (optionsObject.fullwidth) alertClasses.push(fullwidthClass);
        const role = optionsObject.role || (optionsObject.toast && optionsObject.dismissible ? 'alertdialog' : 'alert');

        const alertAttrs = `class="${alertClasses.join(' ')}" role="${role}"`;
        const closeButton = optionsObject.dismissible
            ? `<button class="${closeClasses}" type="button" title="${lang.close}" ${alertCloseData}></button>`
            : '';
        const icon = optionsObject.icon
            ? `<div class="${iconClasses}">${optionsObject.icon}</div>`
            : '';
        const title = optionsObject.title
            ? `<${titleTag} class="${titleClasses}">${optionsObject.title}</${titleTag}>`
            : '';
        const content = optionsObject.content || '';

        const iconPrev = icon
            ? `<div class="${innerClasses}">
                ${icon}
                <div>`
            : '';
        const iconNext = icon
            ? '</div></div>'
            : '';

        return `
            <div ${alertAttrs}>
                ${closeButton}
                ${iconPrev}
                ${title}
                ${content}
                ${iconNext}
            </div>`;
    }
});

addModule(moduleName, AppAlert);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-alert]').forEach(el => new AppAlert(el));

export default AppAlert;
export {AppAlert};