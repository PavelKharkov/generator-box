//TODO:test fields with same name

import validate from 'Libs/validate';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'validation';

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const validatePlugin = validate;

//Регулярные выражения
const nameRegExp = new RegExp(`^([${lang.nameRegExpContents}-]+\\s)*[${lang.nameRegExpContents}\\s-]+$`, 'i');
const telRegExp = new RegExp(`^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[-. \\\\/]?)?((?:\\(?\\d+\\)?[ ]?[-. \\\\/]?[ ]?)*)(?:[-. \\\\/]?(?:#|${lang.telExtRegExpContents})[-. \\\\/]?(\\d+))?$`, 'i');
const emailRegExp = new RegExp('^[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$', 'i'); /* eslint-disable-line no-useless-escape */

//Вспомогательные функции
const nameValidate = value => {
    return (value === '') || nameRegExp.test(value);
};

//Селекторы и классы
const elementContainerSelector = '.form-group';
const invalidClass = 'is-invalid';
const validClass = 'is-valid';
const defaultNamePrefix = 'validateElement-';

//TODO:add ignore, triggers, normalizer?, errorLabelContainer

/**
 * @function defaultValidHandler
 * @desc Функция, по умолчанию вызываемая при успешном прохождении валидации формы.
 * @param {HTMLElement} [form] - Элемент валидируемой формы.
 * @returns {undefined}
 * @ignore
 */
const defaultValidHandler = (form, event) => {
    if (!form) return;

    const formModule = form.modules && form.modules.form;
    if (formModule) {
        formModule.unpreventSubmit();
        formModule.submit(event);
        formModule.preventSubmit();
        return;
    }

    form.submit();
};

/**
 * @function defaultErrorPlacement
 * @desc Помещает элемент ошибки относительно ошибочного поля (по умолчанию).
 * @param {HTMLElement} error - Элемент валидируемой формы.
 * @param {HTMLElement} element - Текущий валидируемый элемент.
 * @returns {HTMLElement} Элемент ошибки.
 * @ignore
 */
const defaultErrorPlacement = (error, element) => {
    const elementContainer = element.closest(elementContainerSelector);
    if (elementContainer) {
        elementContainer.append(error);
        return error;
    }

    return element.parentNode.insertBefore(error, element.nextSibling);
};

/**
 * @function transformLengthRule
 * @desc Трансформирует правило длины валидатора модуля для элемента в правило валидатора плагина.
 * @param {string} rule - Название правила валидатора.
 * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
 * @param {string} newName - Название правила валидатора для плагина.
 * @returns {undefined}
 * @ignore
 */
const transformLengthRule = (rule, element, newName) => {
    const numberValue = Number(element[rule]);
    if ((rule === 'length') && Number.isFinite(numberValue)) {
        element[newName] = {
            is: numberValue
        };
    }
};

/**
 * @function transformMinlengthRule
 * @desc Трансформирует правило минимальной длины валидатора модуля для элемента в правило валидатора плагина.
 * @param {string} rule - Название правила валидатора.
 * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
 * @param {string} newName - Название правила валидатора для плагина.
 * @returns {undefined}
 * @ignore
 */
const transformMinlengthRule = (rule, element, newName) => {
    const numberValue = Number(element[rule]);
    if ((rule === 'minlength') && Number.isFinite(numberValue)) {
        element[newName] = {
            minimum: numberValue
        };
    }
};

//Опции по умолчанию
const defaultOptions = {
    elementContainer: elementContainerSelector,
    invalidClass,
    validClass,

    focusInvalid: true,
    validHandler: defaultValidHandler
};
//Опции плагина валидации по умолчанию
const defaultValidateOptions = {
    visual: true
};

//Валидаторы
//Валидатор даты
validatePlugin.extend(validatePlugin.validators.datetime, {
    parse(value) {
        return new Date(value);
    },
    format(value, options) {
        const newValue = options.dateOnly ? value.slice(0, value.indexOf(value.indexOf('T') ? 'T' : ' ')) : value;
        return newValue;
    }
});

//Валидатор почты
validatePlugin.validators.email.PATTERN = emailRegExp;

//Валидатор почта/телефон
validatePlugin.validators.emailortel = function(value, options) {
    const notEmpty = (value !== '') && (value !== null);
    const valueTest = () => {
        return value.includes('@') ? emailRegExp.test(value) : telRegExp.test(value);
    };
    if (notEmpty && !valueTest()) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор ФИО
validatePlugin.validators.fio = function(value, options) {
    if (!nameValidate(value)) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор одинаковости
const defaultEqualityFunc = (value1, value2) => {
    return value1 === value2;
};

validatePlugin.validators.equality = function(value, options, attribute, attributes) { /* eslint-disable-line max-params */
    const errors = [];
    const validatorOptions = {};

    if (this.options) util.extend(validatorOptions, this.options);
    util.extend(validatorOptions, options);

    if ((typeof options.attribute !== 'string') || !options.attribute) {
        util.typeError(options.attribute, 'key', 'non-empty string');
    }

    const otherValue = attributes[options.attribute];
    const comparator = options.comparator || defaultEqualityFunc;

    if (!comparator(value, otherValue, options, attribute, attributes)) {
        errors.push(validatePlugin.format(validatorOptions.message || this.message, {
            attribute: validatorOptions.attribute
        }));
    }

    if (errors.length > 0) {
        return validatorOptions.message || errors;
    }
    return null;
};

//Валидатор почтового индекса
validatePlugin.validators.index = function(value, options) {
    if ((value === '') || /^\d+$/.test(value)) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор длины
validatePlugin.validators.length = function(value, options) {
    if ((typeof value === 'undefined') || (value === null)) {
        return null;
    }

    const errors = [];
    const validatorOptions = {};
    if (this.options) util.extend(validatorOptions, this.options || {});
    util.extend(validatorOptions, options);
    const lengthPlural = validatorOptions.lengthPlural || this.lengthPlural;

    const newValue = (typeof validatorOptions.tokenizer === 'function') ? validatorOptions.tokenizer(value) : value;
    const valueLength = newValue.length;
    if (typeof valueLength !== 'number') {
        errors.push(validatorOptions.notValid || this.notValid);
    }

    //Проверки на длину
    if ((typeof options.is === 'number') && (valueLength !== validatorOptions.is)) {
        errors.push(validatePlugin.format(validatorOptions.wrongLength || this.wrongLength, {
            count: validatorOptions.is,
            plural: util.formatPlural(validatorOptions.is, ...lengthPlural)
        }));
    }

    if ((typeof options.minimum === 'number') && (valueLength < validatorOptions.minimum)) {
        errors.push(validatePlugin.format(validatorOptions.tooShort || this.tooShort, {
            count: validatorOptions.minimum,
            plural: util.formatPlural(validatorOptions.minimum, ...lengthPlural)
        }));
    }

    if ((typeof options.maximum === 'number') && (valueLength > validatorOptions.maximum)) {
        errors.push(validatePlugin.format(validatorOptions.tooLong || this.tooLong, {
            count: validatorOptions.maximum,
            plural: util.formatPlural(validatorOptions.maximum, ...lengthPlural)
        }));
    }

    if (errors.length > 0) {
        return validatorOptions.message || errors;
    }
    return null;
};

//Валидатор обязательного поля
validatePlugin.validators.presence = function(value, options, key, attributes, globalOptions) { /* eslint-disable-line max-params */
    const validatorOptions = {};
    if (this.options) util.extend(validatorOptions, this.options);
    util.extend(validatorOptions, options);

    const emptyFormField = globalOptions.form && (value === false);
    const emptyProp = options.allowEmpty ? ((value !== null) && (typeof value !== 'undefined')) : validatePlugin.isEmpty(value);
    if (emptyFormField || emptyProp) {
        return options.message || this.message || 'can\'t be blank';
    }

    return null;
};

//Валидатор имени
validatePlugin.validators.name = function(value, options) {
    if (!nameValidate(value)) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор отчества
validatePlugin.validators.patronymic = function(value, options) {
    if (!nameValidate(value)) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор фамилии
validatePlugin.validators.surname = function(value, options) {
    if (!nameValidate(value)) {
        return options.message || this.message;
    }
    return null;
};

//Валидатор телефона
validatePlugin.validators.tel = function(value, options) {
    const notEmpty = (value !== '') && (value !== null);
    if (notEmpty && !telRegExp.test(value)) {
        return options.message || this.message;
    }
    return null;
};
//-----------------------------

//Преобразование валидаторов модуля в валидаторы плагина
const validatorsTransform = {
    date: {
        options: {
            notValid: lang.date
        },
        message: lang.date
    },
    datetime: {
        options: {
            notValid: lang.date
        },
        message: lang.date
    },
    email: {
        message: lang.email
    },
    emailortel: {
        message: lang.emailortel
    },
    equalto: {
        name: 'equality',
        message: lang.equalto
    },
    fio: {
        message: lang.fio
    },
    index: {
        message: lang.index
    },
    length: {
        options: {
            wrongLength: lang.length,
            lengthPlural: lang.lengthPlural
        }
    },
    maxdate: {
        name: 'datetime',
        options: {
            tooLate: lang.maxDate
        }
    },
    maxlength: {
        name: 'length',
        options: {
            tooLong: lang.maxlength,
            lengthPlural: lang.lengthPlural
        }
    },
    //TODO:test min & max date
    mindate: {
        name: 'datetime',
        options: {
            tooEarly: lang.minDate
        }
    },
    minlength: {
        name: 'length',
        options: {
            tooShort: lang.minlength,
            lengthPlural: lang.lengthPlural
        }
    },
    name: {
        message: lang.name
    },
    patronymic: {
        message: lang.patronymic
    },
    required: {
        name: 'presence',
        options: {
            allowEmpty: false
        },
        message: lang.required
    },
    surname: {
        message: lang.surname
    },
    tel: {
        message: lang.tel
    }
};

//Применение преобразования валидаторов
Object.keys(validatorsTransform).forEach(validatorName => {
    const {message} = validatorsTransform[validatorName];
    if (!message) return;

    if (validatorsTransform[validatorName].name) {
        validatePlugin.validators[validatorsTransform[validatorName].name].message = message;
        return;
    }

    validatePlugin.validators[validatorName].message = message;
});

//TODO:add type validators

/* eslint-disable jsdoc/valid-types */
/**
 * @typedef {Object.<Object>} components.AppValidation.validationRules
 * @desc Правила для проверки.
 * @property {Object} [{element}] - Содержит правила для определённого элемента.
 * @property {Object} [{element}.{rule}] - Правило элемента.
 * @property {Object} [{element}.{rule}.message] - Сообщение об ошибке. В сообщении можно использовать литералы %{...}, которые будут при выводе заменены на соответствующие значения.
 * @property {(boolean|Object)} [{element}.date] - Валидация даты.
 * @property {boolean} [{element}.date.dateOnly=true] - Валидна только дата без указания времени.
 * @property {(Date|string)} [{element}.date.earliest] - Дата валидна только если больше, чем указанная.
 * @property {(Date|string)} [{element}.date.latest] - Дата валидна только если меньше, чем указанная.
 * @property {(boolean|Object)} [{element}.datetime] - Валидация даты с указанием времени. Аналогично валидатору date с dateOnly: false.
 * @property {boolean} [{element}.email] - Валидация электронной почты.
 * @property {boolean} [{element}.emailortel] - Валидация значения, которое может быть электронной почтой или телефоном.
 * @property {(string|Object)} [{element}.equalto] - Валидация соответствия. В строке указывается, с каким свойством сравнивать.
 * @property {string} [{element}.equalto.attribute] - С каким свойством сравнивать.
 * @property {Function} [{element}.equalto.comparator] - Функция для сравнения свойств. По умолчанию сравнивает два значения через ===, и, если валидируется форма, то скрывает ошибки в обеих полях при прохождении валидации.
 * @property {boolean} [{element}.fio] - Валидация ФИО.
 * @property {boolean} [{element}.index] - Валидация почтового индекса.
 * @property {(number|Object)} [{element}.length] - Валидация длины.
 * @property {Object} [{element}.maxdate] - Валидация максимально допустимой даты.
 * @property {number} [{element}.maxlength] - Валидация максимальной длины.
 * @property {Object} [{element}.mindate] - Валидация минимально допустимой даты.
 * @property {number} [{element}.minlength] - Валидация минимальной длины.
 * @property {boolean} [{element}.name] - Валидация имени.
 * @property {boolean} [{element}.patronymic] - Валидация отчества.
 * @property {(boolean|Object)} [{element}.required] - Валидация заполненности.
 * @property {boolean} [{element}.required.allowEmpty=false] - Учитывать ли falsy-значения ({}, [], '', ' ').
 * @property {boolean} [{element}.surname] - Валидация фамилии.
 * @property {boolean} [{element}.tel] - Валидация телефона.
 */
/* eslint-enable jsdoc/valid-types */
//TODO:length validator options

const validationInit = AppForm => {
    /**
     * @class AppValidation
     * @memberof components
     * @requires libs.validate
     * @requires components#AppForm
     * @classdesc Модуль для валидации форм. [Правила для валидации]{@link components.AppValidation.validationRules}.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {(HTMLElement|Object)} element - Элемент формы или валидируемый объект.
     * @param {app.Module.instanceOptions} options - Опции экземпляра.
     * @param {components.AppValidation.validationRules} options.rules - Объект с правилами для проверки.
     * @param {string} [options.elementContainer='.form-group'] - Селектор родительского контейнера для поля ввода и заголовка поля.
     * @param {string} [options.invalidClass='is-invalid'] - Класс для обозначения ошибки.
     * @param {string} [options.validClass='is-valid'] - Класс для обозначения правильно заполненного поля.
     * @param {Function} [options.validHandler] - Функция, вызываемая после успешного прохождения валидации.
     * @param {Function} [options.invalidHandler] - Функция, вызываемая после прохождения валидации с ошибками.
     * @param {Function} [options.validElementHandler] - Функция, вызываемая после успешного прохождения валидации элемента.
     * @param {Function} [options.invalidElementHandler] - Функция, вызываемая после прохождения валидации элемента с ошибками.
     * @param {boolean} [options.focusInvalid=true] - Ставить ли фокус на последнее поле с ошибками после валидации.
     * @param {boolean} [options.validateOnInput] - Валидировать ли поле при вводе.
     * @param {Function} [options.errorPlacement] - Функция, помещающая элемент с сообщением об ошибке относительно поля с ошибкой. В функцию передаются параметры error - элемент с сообщением, element - элемент поля с ошибкой. По умолчанию находит родительский элемент, указанный в опции elementContainer и помещает сообщение в конец элемента.
     * @param {boolean} [options.form] - Является ли элемент экземпляра формой. Добавляется автоматически.
     * @param {*} [options...] - Другие опции плагина [Validate.js]{@link libs.validate}.
     * @example <caption>Валидация формы</caption>
     * const validationInstance = new app.Validation(document.querySelector('.form-to-validate'), {
     *     rules: { //объект с правилами для проверки
     *         emailField: {
     *             email: true
     *         }
     *     },
     *     validHandler(form, event) {
     *         form.submit(); //стандартная отправка формы
     *     },
     *     invalidHandler(errors, form, event) {
     *         console.log(errors); //объект с ошибками
     *     }
     * });
     *
     * @example <caption>Валидация объекта</caption>
     * const validationInstance = new app.Validation({
     *     goodEmail: 'test@test.com',
     *     badEmail: 'test@test'
     * }, {
     *     rules: {
     *         goodEmail: {
     *             email: true
     *         },
     *         badEmail: {
     *             email: true
     *         }
     *     }
     * });
     * console.log(validationInstance.validate()); // {badEmail: ['Введите корректный email']}
     *
     * @example <caption>Поля формы можно валидировать через data-validate</caption>
     * <!--HTML-->
     * <form data-validate>
     *     <input type="email" name="email" data-validate="required, email">
     *     <input type="text" data-validate="required, fio"> <!--Если у поля не указан name, то при иницаилации валидации оно получит уникальный name-->
     *     <textarea name="message" data-validate="required, minlength: 3"></textarea> <!--Параметры указываются через двоеточие-->
     *     <input type="new-password" data-validate="equalto: password-repeat"> <!--Пример указания валидатора equalto-->
     *     <input type="password-repeat" data-validate="equalto: new-password">
     *     <button type="submit">Отправить</button>
     *     <!--Валидация будет происходить при отправке формы, а также при изменении значений полей пользователем-->
     * </form>
     *
     * <!--data-validate - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <form data-validation-options='{"focusInvalid": false}'></form>
     */
    const AppValidation = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            const isHTMLElement = el instanceof HTMLElement;
            const isForm = isHTMLElement && (el.tagName === 'FORM');
            if (isHTMLElement && !isForm) return;

            let form;

            /**
             * @member {Object} AppValidation#form
             * @memberof components
             * @desc Если элемент экземпляра является формой, то хранит ссылку на [модуль формы]{@link components.AppForm}.
             * @readonly
             */
            Object.defineProperty(this, 'form', {
                enumerable: true,
                get: () => form
            });

            if (isForm) {
                const validationOptionsData = el.dataset.validationOptions;
                if (validationOptionsData) {
                    const dataOptions = util.stringToJSON(validationOptionsData);
                    if (!dataOptions) util.error('incorrect data-validation-options format');
                    util.extend(this.options, dataOptions);
                }

                form = el.modules.form || new AppForm(el);
                if (!(form instanceof AppForm)) {
                    util.error({
                        message: '\'form\' module is not instance of \'Form\' class',
                        element: el
                    });
                }

                form.preventSubmit();
                this.options.form = true;
                el.noValidate = true;
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppValidation#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            params.fullMessages = false;
            params.rules = params.rules || {};
            const rules = params.rules;
            if (params.constraints) delete params.constraints;

            const errors = {};

            /**
             * @member {Object} AppValidation#errors
             * @memberof components
             * @desc Объект с коллекциями текущих ошибок валидации.
             * @readonly
             */
            Object.defineProperty(this, 'errors', {
                enumerable: true,
                value: errors
            });

            const inputs = [];

            /**
             * @member {Array.<HTMLElement>} AppValidation#inputs
             * @memberof components
             * @desc Коллекция валидируемых элементов формы.
             * @readonly
             */
            Object.defineProperty(this, 'inputs', {
                enumerable: true,
                value: inputs
            });

            const errorMessages = {};

            let currentEvent;

            /**
             * @member {Object} AppValidation#currentEvent
             * @memberof components
             * @desc Объект события, с помощью которого была вызвана валидация.
             * @readonly
             */
            Object.defineProperty(this, 'currentEvent', {
                enumerable: true,
                get: () => currentEvent
            });

            //Функции
            /**
             * @function validateFunc
             * @desc Валидирует элемент экземпляра через плагин.
             * @param {Object} [options={}] - Опции валидации.
             * @param {Object} [options.event] - Объект события, с помощью которого была вызвана валидация.
             * @returns {Object} Объект с ошибками при валидации.
             * @ignore
             */
            const validateFunc = (options = {}) => {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                const validateOptions = {};
                util.extend(validateOptions, params);
                util.defaultsDeep(validateOptions, options);

                if (isForm) {
                    currentEvent = validateOptions.event;
                }

                const currentErrors = validatePlugin(el, rules, params);
                if (isForm && currentErrors) {
                    const activeErrors = {};
                    Object.keys(currentErrors).forEach(inputName => {
                        if (!el.elements[inputName] || el.elements[inputName].disabled) return;
                        activeErrors[inputName] = currentErrors[inputName];
                    });
                    return (Object.keys(activeErrors).length > 0) && activeErrors;
                }

                return currentErrors || {};
            };

            /**
             * @function checkRule
             * @desc Проверяет наличие валидатора.
             * @param {string} ruleName - Проверяемое правило валидации.
             * @throws Выводит ошибку, если валидатор не найден.
             * @ignore
             */
            const checkRule = ruleName => {
                if (!this.constructor.validators[ruleName]) {
                    util.error(`method '${ruleName}' not found`);
                }
            };

            /**
             * @function showInputErrors
             * @desc Отображает ошибки определённого поля формы.
             * @param {HTMLElement} input - Элемент поля формы.
             * @returns {undefined}
             * @ignore
             */
            const showInputErrors = input => {
                const inputName = input.name;
                if (!errors[inputName]) return;
                if (!input.id) {
                    input.id = input.name;
                }
                input.classList.remove(params.validClass);
                input.classList.add(params.invalidClass);

                errorMessages[inputName] = errors[inputName].map(error => {
                    const newErrorMessage = document.createElement('label');
                    newErrorMessage.classList.add(params.invalidClass);
                    newErrorMessage.htmlFor = input.id;
                    newErrorMessage.textContent = error;

                    const placedError = (typeof params.errorPlacement === 'function')
                        ? params.errorPlacement(newErrorMessage, input)
                        : defaultErrorPlacement(newErrorMessage, input);
                    if (!placedError) return null;

                    return newErrorMessage;
                });
            };

            let errorsFocus;

            /**
             * @function showInputsErrors
             * @desc Отображает все ошибки формы.
             * @returns {undefined}
             * @ignore
             */
            const showInputsErrors = () => {
                inputs.forEach(input => showInputErrors(input));
                if (params.focusInvalid) {
                    const errorInputs = Object.keys(errors);
                    errorsFocus = true;
                    el.elements[errorInputs[0]].focus();
                    errorsFocus = false;
                }
            };

            /**
             * @function removeInputErrors
             * @desc Скрывает ошибки поля формы.
             * @param {HTMLElement} input - Элемент поля формы.
             * @returns {undefined}
             * @ignore
             */
            const removeInputErrors = input => {
                input.classList.add(params.validClass);
                if (!errorMessages[input.name]) return;
                input.classList.remove(params.invalidClass);
                errorMessages[input.name].forEach(error => {
                    if (!error) return;
                    error.remove();
                });
                delete errorMessages[input.name];
            };

            /**
             * @function removeInputsErrors
             * @desc Скрывает все ошибки формы.
             * @returns {undefined}
             * @ignore
             */
            const removeInputsErrors = () => {
                inputs.forEach(input => removeInputErrors(input));
            };

            let postEqualityCheck = true;

            /**
             * @function checkEqual
             * @desc Сравнивает два значения для валидации одинаковости.
             * @param {*} value1 - Первое сравниваемое значение.
             * @param {*} value2 - Второе сравниваемое значение.
             * @returns {boolean} Одинаковые ли два значения или нет.
             * @ignore
             */
            const checkEqual = (value1, value2) => {
                const isEqual = value1 === value2;
                const notFocus = (currentEvent.type !== 'submit') && !errorsFocus;
                const firstEmpty = (value1 === '') || (value1 === null);
                const secondEmpty = (value2 === '') || (value2 === null);
                const firstOrSecondEmpty = notFocus && (firstEmpty || secondEmpty);

                return isEqual || firstOrSecondEmpty;
            };

            /**
             * @function transformEqualtoRule
             * @desc Трансформирует правило соотвестствия валидатора модуля для элемента в правило валидатора плагина.
             * @param {string} rule - Название правила валидатора.
             * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
             * @param {string} newName - Название правила валидатора для плагина.
             * @returns {undefined}
             * @ignore
             */
            const transformEqualtoRule = (rule, element, newName) => {
                if ((rule !== 'equalto') || util.isObject(element[rule])) {
                    return;
                }

                element[newName] = {
                    attribute: element[rule],
                    comparator: (value1, value2, options, attribute) => { /* eslint-disable-line max-params */
                        const isEqual = checkEqual(value1, value2);
                        if (isForm && isEqual) {
                            if (postEqualityCheck) {
                                postEqualityCheck = false;
                                const currentErrors = this.validateElement(el.elements[attribute], {
                                    event: currentEvent
                                });
                                const hasErrors = currentErrors && currentErrors[attribute] && (currentErrors[attribute].length > 0);
                                if (currentErrors && (!currentErrors[attribute] || hasErrors)) {
                                    removeInputErrors(el.elements[options.attribute]);
                                    removeInputErrors(el.elements[attribute]);
                                }
                            }
                            util.defer(() => {
                                postEqualityCheck = true;
                            });
                        }
                        return isEqual;
                    }
                };
            };

            /**
             * @function transformRule
             * @desc Трансформирует правила валидатора модуля для элемента в правило валидатора плагина.
             * @param {string} rule - Название правила валидатора.
             * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
             * @returns {undefined}
             * @ignore
             */
            const transformRule = (rule, element) => {
                if (!validatorsTransform[rule]) return;

                const transformedRule = validatorsTransform[rule];
                const newName = transformedRule.name || rule;
                checkRule(newName);

                transformLengthRule(rule, element, newName);
                transformMinlengthRule(rule, element, newName);
                transformEqualtoRule(rule, element, newName);

                if (!util.isObject(element[newName])) {
                    element[newName] = {};
                }
                if (util.isObject(element[rule])) {
                    util.extend(element[newName], element[rule]);
                }

                if (transformedRule.options) {
                    util.defaultsDeep(element[newName], transformedRule.options);
                } else if (!element[newName]) {
                    element[newName] = element[rule];
                }
                if (newName !== rule) {
                    delete element[rule];
                }
            };

            /**
             * @function transformRules
             * @desc Трансформирует все правила валидации элемента.
             * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
             * @returns {undefined}
             * @ignore
             */
            const transformRules = element => {
                Object.keys(element).forEach(rule => {
                    transformRule(rule, element);
                });
            };

            /**
             * @function addElement
             * @desc Добавляет новый элемент для валидации в общий объект валидируемых элементов.
             * @param {Object} element - Ссылка на объект со списком правил валидатора элемента.
             * @returns {Object} При возникновении ошибки возвращает null.
             * @ignore
             */
            const addElement = element => {
                let elementName = element.name;
                if (elementName && rules[elementName]) {
                    inputs.push(element);
                }

                //TODO:validate by input type
                const validateDataObj = element.dataset.validate;
                if (validateDataObj) {
                    inputs.push(element);

                    if (!elementName) {
                        element.name = defaultNamePrefix + util.uniqueId();
                        elementName = element.name;
                    }

                    validateDataObj.split(',').forEach(rule => {
                        const formattedRule = rule.trim();
                        if (formattedRule === '') return null;

                        rules[elementName] = rules[elementName] || {};

                        if (formattedRule.includes(':')) {
                            const ruleData = formattedRule.split(':');
                            const ruleName = ruleData[0].trim();
                            rules[elementName][ruleName] = ruleData[1].trim();
                        } else {
                            rules[elementName][formattedRule] = true;
                        }

                        transformRules(rules[elementName]);

                        return null;
                    });
                }

                //TODO:docs
                const elementUpdate = (event, visual) => {
                    this.validateElement(element, {
                        event,
                        visual
                    });

                    if (errors && errors[element.name] && (typeof params.invalidElementHandler === 'function')) {
                        params.invalidElementHandler(errors, element, event);
                    } else if (typeof params.validElementHandler === 'function') {
                        params.validElementHandler(element, event);
                    }
                };

                if (params.validateOnInput) {
                    element.addEventListener('input', event => {
                        elementUpdate(event, false);
                    });
                }
                if (element.tagName === 'SELECT') {
                    element.addEventListener('change', event => {
                        elementUpdate(event, false);
                    });
                }
                element.addEventListener('blur', event => {
                    if (typeof element.dataset.autofocus !== 'undefined') {
                        delete element.dataset.autofocus;
                        return;
                    }

                    elementUpdate(event);
                });

                return null;
            };

            this.on({
                /**
                 * @event AppValidation#addElement
                 * @memberof components
                 * @desc Добавляет элемент поля формы в набор правил для валидации.
                 * @param {(HTMLElement|Object)} element - Элемент поля формы или объект элемента для проверки.
                 * @returns {undefined}
                 * @example
                 * validationInstance.on('addElement', () => {
                 *     console.log('Новый элемент добавлен в набор правил для валидации');
                 * });
                 * validationInstance.addElement(document.querySelector('input'));
                 */
                addElement(element = util.required('element')) {
                    if (isForm) {
                        addElement(element);
                    } else {
                        transformRules(element);
                    }
                },

                //TODO:removeElement

                /**
                 * @event AppValidation#validate
                 * @memberof components
                 * @desc Валидирует объект экземпляра.
                 * @param {Object} [options={}] - Опции валидации.
                 * @param {Object} [options.visual=true] - Обновлять ли элементы после валидации.
                 * @fires components.AppValidation#invalid
                 * @fires components.AppValidation#valid
                 * @returns {*} Ответ событий valid или invalid в зависимости от результата прохождения валидации. Если валидация прошла с ошибками и в ответе события invalid ничего не вернулось, возвращается объект с ошибками.
                 * @example
                 * validationInstance.on('validate', () => {
                 *     console.log('Форма была провалидирована');
                 * });
                 * const validateResult = validationInstance.validate();
                 * console.log(validateResult); //Выведет список ошибок
                 * //Также список ошибок можно посмотреть с помощью свойства errors
                 * console.log(validationInstance.errors);
                 */
                validate(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    util.defaultsDeep(options, defaultValidateOptions);

                    Object.keys(errors).forEach(error => {
                        delete errors[error];
                    });
                    const currentErrors = validateFunc(options);
                    Object.keys(currentErrors).forEach(error => {
                        errors[error] = currentErrors[error];
                    });

                    const hasErrors = Object.keys(errors).length > 0;
                    if (isForm && options.visual) {
                        removeInputsErrors();
                        if (hasErrors) showInputsErrors();
                    }

                    const validateResult = hasErrors
                        ? (this.emit('invalid', errors, el) || errors)
                        : this.emit('valid', el);

                    return validateResult;
                },

                /**
                 * @event AppValidation#validateElement
                 * @memberof components
                 * @desc Валидирует отдельный элемент формы.
                 * @param {HTMLElement} element - Проверяемый элемент поля формы.
                 * @param {Object} [options={}] - Опции валидации.
                 * @param {Object} [options.event] - Объект события, с помощью которого была вызвана валидация элемента.
                 * @param {Object} [options.visual=true] - Обновлять ли элемент после валидации.
                 * @fires components.AppValidation#invalidElement
                 * @fires components.AppValidation#validElement
                 * @returns {*} Ответ событий valid или invalid в зависимости от результата прохождения валидации.
                 * @example
                 * validationInstance.on('validateElement', () => {
                 *     console.log('Элемент формы был провалидирован');
                 * });
                 * const validateElementResult = validationInstance.validateElement();
                 * console.log(validateElementResult); //Выведет список ошибок элемента
                 */
                validateElement(element, options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    util.defaultsDeep(options, defaultValidateOptions);

                    Object.keys(errors).forEach(error => {
                        delete errors[error];
                    });
                    const currentErrors = validateFunc(options);
                    Object.keys(currentErrors).forEach(error => {
                        errors[error] = currentErrors[error];
                    });

                    const hasErrors = Object.keys(errors).length > 0;
                    if (isForm) {
                        removeInputErrors(element);
                        if (hasErrors && options.visual) {
                            showInputErrors(element);
                        }
                    }

                    const validateResult = hasErrors
                        ? (this.emit('invalidElement', errors, element) || errors)
                        : this.emit('validElement', element);

                    return validateResult;
                }

                //TODO:reset errors method
                //
                // resetInvalid(element = util.required('element')) {
                //     el.ariaInvalid = 'false';
                //     el.classList.remove(params.invalidClass);
                //     element.closest(elementContainer).querySelector(`label.${params.invalidClass}`).remove();
                // }
            });

            this.onSubscribe([
                /**
                 * @event AppValidation#valid
                 * @memberof components
                 * @desc Вызывается после успешной валидации. Если не получает возвращаемое значение, срабатывает событие validHandler.
                 * @param {HTMLElement} [element] - Проверяемая форма.
                 * @returns {undefined}
                 * @example
                 * validationInstance.on('valid', element => {
                 *     console.log('Валидация успешно пройдена');
                 *     console.log(element); //Выведет проверяемый элемент экземпляра
                 * });
                 */
                'valid',

                /**
                 * @event AppValidation#invalid
                 * @memberof components
                 * @desc Вызывается после валидации с ошибками. Если не получает возвращаемое значение, срабатывает событие invalidHandler.
                 * @param {Object} [errors] - Объект с ошибками.
                 * @param {HTMLElement} [element] - Проверяемая форма.
                 * @returns {undefined}
                 * @example
                 * validationInstance.on('invalid', (errors, element) => {
                 *     console.log('Валидация пройдена с ошибками');
                 *     console.log(errors); //Выведет объект с ошибками
                 *     console.log(element); //Выведет проверяемый элемент экземпляра
                 * });
                 */
                'invalid',

                /**
                 * @event AppValidation#validElement
                 * @memberof components
                 * @desc Вызывается после успешной валидации элемента. Если не получает возвращаемое значение, срабатывает событие validHandler.
                 * @param {HTMLElement} [element] - Проверяемый элемент формы.
                 * @returns {undefined}
                 * @example
                 * validationInstance.on('validElement', element => {
                 *     console.log('Валидация элемента успешно пройдена');
                 *     console.log(element); //Выведет проверяемый элемент экземпляра
                 * });
                 */
                'validElement',

                /**
                 * @event AppValidation#invalidElement
                 * @memberof components
                 * @desc Вызывается после валидации элемента с ошибками. Если не получает возвращаемое значение, срабатывает событие invalidHandler.
                 * @param {Object} [errors] - Объект со всеми ошибками формы.
                 * @param {HTMLElement} [element] - Проверяемый элемент формы.
                 * @returns {undefined}
                 * @example
                 * validationInstance.on('invalidElement', (errors, element) => {
                 *     console.log('Валидация элемента пройдена с ошибками');
                 *     console.log(errors); //Выведет объект с ошибками
                 *     console.log(element); //Выведет проверяемый элемент экземпляра
                 * });
                 */
                'invalidElement'
            ]);

            Object.keys(rules).forEach(elementName => {
                const element = rules[elementName];
                transformRules(element);
            });

            if (isForm) {
                [...el.elements].forEach(element => {
                    addElement(element);
                });

                //Привязка валидации на применение формы
                const submitFunc = event => {
                    event.preventDefault();

                    this.validate({event});

                    const hasErrors = Object.keys(errors).length > 0;
                    if (hasErrors) {
                        if (typeof params.invalidHandler === 'function') {
                            params.invalidHandler(errors, el, event);
                        }
                    } else if (typeof params.validHandler === 'function') {
                        params.validHandler(el, event);
                    }
                };
                el.addEventListener('submit', submitFunc);

                const initialUnlink = this.unlink.bind(this); //TODO:make destroy event
                Object.defineProperty(this, 'unlink', {
                    enumerable: true,
                    value() {
                        el.removeEventListener('submit', submitFunc);
                        initialUnlink();
                    }
                });

                if (typeof el.dataset.validationTriggered !== 'undefined') {
                    this.validate();
                    delete el.dataset.validationTriggered;
                }
            }
        }

        /**
         * @function AppValidation.validators
         * @memberof components
         * @desc Get-функция. Возвращает валидаторы плагина.
         * @returns {Object}
         */
        static get validators() {
            return validatePlugin.validators;
        }
    });

    addModule(moduleName, AppValidation);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll('form[data-validate]').forEach(el => new AppValidation(el));

    return AppValidation;
};

export default validationInit;
export {validationInit};