import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let validationTriggered;

const validationCallback = resolve => {
    const imports = [require('Components/form').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "validation" */ './sync.js'))
            .then(validation => {
                chunks.validation = true;
                validationTriggered = true;

                const AppValidation = validation.validationInit(...modules);
                if (resolve) resolve(AppValidation);
            });
    });

    emitInit('form');
};

const initValidation = event => {
    if (validationTriggered) return;

    const form = event.target.closest('form[data-validate]');
    if (
        form &&
        ((event.target.tagName === 'BUTTON') || (event.target.tagName === 'INPUT')) &&
        (event.target.type === 'submit')
    ) {
        event.preventDefault();
        form.dataset.validationTriggered = '';
    }

    emitInit('validation');
};

const defaultValidationTriggers = [
    'click',
    'change',
    'input'
];
const validationTrigger = (validationItems, validationTriggers = defaultValidationTriggers) => {
    if (__IS_SYNC__) return;

    const items = (validationItems instanceof Node) ? [validationItems] : validationItems;
    if (items.length === 0) return;

    items.forEach(item => {
        validationTriggers.forEach(trigger => {
            item.addEventListener(trigger, initValidation, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.validation) {
        validationCallback(resolve);
        return;
    }

    onInit('validation', () => {
        validationCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('validation');
    });

    const validationItems = document.querySelectorAll('form[data-validate]');
    validationTrigger(validationItems);
});

export default importFunc;
export {validationTrigger};