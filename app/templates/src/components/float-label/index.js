import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'floatLabel';

//Селекторы и классы
const floatLabelClass = util.kebabCase(moduleName);
const focusClass = 'focus';
const floatInputSelectors =
    'input[type="text"], input[type="tel"], input[type="email"], input[type="password"], input[type="file"], textarea, select';
const initializedClass = `${floatLabelClass}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    inputSelector: floatInputSelectors
};

/* eslint-disable jsdoc/require-hyphen-before-param-description */
/**
 * @class AppFloatLabel
 * @memberof components
 * @requires layout#AppWindow
 * @classdesc Модуль динамических заголовков полей.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент контейнера поля.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.inputSelector='input[type="text"], input[type="tel"], input[type="email"], input[type="password"], input[type="file"], textarea, select'] - Селектор элементов ввода, по которым будут активироваться события модуля.
 * @example
 * const floatLabelInstance = new app.FloatLabel(document.querySelector('.float-label'), {
 *     inputSelector: 'input[type="text"]' //Использовать только селектор текстового поля ввода для активации события модуля
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="float-label" data-float-label>
 *     <label for="input">Поле ввода</label>
 *     <input type="text" id="input">
 * </div>
 *
 * <!--data-float-label - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div class="float-label" data-float-label-options='{"inputSelector": "input[type=\"text\"]"}'></div>
 */
/* eslint-enable jsdoc/require-hyphen-before-param-description */
const AppFloatLabel = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const floatLabelOptionsData = el.dataset.floatLabelOptions;
        if (floatLabelOptionsData) {
            const dataOptions = util.stringToJSON(floatLabelOptionsData);
            if (!dataOptions) util.error('incorrect data-float-label-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppFloatLabel#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const input = el.querySelector(params.inputSelector);
        if (!input) return;

        /**
         * @member {HTMLElement} AppFloatLabel#input
         * @memberof components
         * @desc Элемент поля ввода.
         * @readonly
         */
        Object.defineProperty(this, 'input', {
            enumerable: true,
            value: input
        });

        //Добавление ссылки на экземпляр модуля к элементу
        if (!input.modules) {
            input.modules = {};
        }
        input.modules[moduleName] = this;

        //TODO:add focus class on select focus

        //Добавление браузерных событий
        if (input.type !== 'file') {
            input.addEventListener('focus', () => {
                el.classList.add(focusClass);
                this.update(true);
            });
            input.addEventListener('blur', () => {
                el.classList.remove(focusClass);
                this.update();
            });
        }

        input.addEventListener('change', () => this.update());
        input.addEventListener('keyup', () => this.update(true));

        /**
         * @function autofillListener
         * @desc Вызывается при вызове события autofill в браузере Chrome при загрузке страницы.
         * @fires components.AppFloatLabel#update
         * @returns {undefined}
         * @ignore
         */
        const autofillListener = () => {
            this.update(true);
        };
        input.addEventListener('animationstart', autofillListener, {once: true});

        this.on({
            /**
             * @event AppFloatLabel#update
             * @memberof components
             * @desc Обновляет активность заголовка поля.
             * @param {boolean} [activate=false] - Если true, то активировать заголовок.
             * @returns {boolean} Возвращает состояние активности заголовка.
             * @example
             * floatLabelInstance.on('update', () => {
             *     console.log('Состояние заголовка поля было обнвлено');
             * });
             * floatLabelInstance.update();
             */
            update(activate = false) {
                const toggle = activate || (input.value !== '');

                if (toggle) {
                    util.activate(el);
                } else {
                    util.deactivate(el);
                }
                return toggle;
            }
        });

        el.classList.add(initializedClass);

        //Обновить классы при загрузке экземпляра и после события AppWindow.load
        this.update();

        /**
         * @function loadFunction
         * @desc Обновляет состояние поля ввода с помощью события [update]{@link components.AppFloatLabel#event:update}, если оно не в фокусе в данный момент.
         * @fires components.AppFloatLabel#update
         * @returns {undefined}
         * @ignore
         */
        const loadFunction = () => {
            if (input !== document.activeElement) {
                this.update();
            }
        };
        AppWindow.onload(loadFunction);
    }
});

addModule(moduleName, AppFloatLabel);

//Инициализировать элементы по data-атрибуту
document.querySelectorAll('[data-float-label]').forEach(el => new AppFloatLabel(el));

export default AppFloatLabel;
export {AppFloatLabel};