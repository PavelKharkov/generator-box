import './style.scss';

let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const colorCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppColor = modules.default;
                resolve(AppColor);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            colorCallback(resolve);
            return;
        }

        onInit('color', () => {
            colorCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('color');
        });
    });
}

export default importFunc;