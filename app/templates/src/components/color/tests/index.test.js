import {isInstance, throwsError, dataTypesCheck, handleDisabled} from 'Base/scripts/test';

//Инициализация тестового элемента с атрибутом data-color-bg
const colorWithDataEl = document.createElement('div');
colorWithDataEl.style.backgroundColor = '';
colorWithDataEl.dataset.colorBg = '';
document.body.append(colorWithDataEl);

const AppColor = require('../sync.js').default;

describe('AppColor', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    test('должен экспортировать промис при импорте index-файла модуля', () => {
        const colorPromise = handleDisabled(() => {
            return require('Components/color').default;
        });

        expect(colorPromise).toBeInstanceOf(Promise);
    });

    test('должен экспортировать промис, который резолвится в класс', async () => {
        await new Promise(done => {
            const coloRequire = handleDisabled(() => {
                return require('Components/color').default;
            });
            return coloRequire.then(AppColorTest => {
                expect(AppColorTest.name).toBe(AppColor.name);
                done();
            });
        });
    });

    const dataTypesAssertions = [
        'Object',
        'Function',
        'undefined'
    ].map(value => [value, [throwsError, 'can\'t initialize \'Color\' instance on document']]);
    dataTypesCheck({
        checkFunction(setupResult, value) {
            if (document.modules) delete document.modules.color;
            return new AppColor(value);
        },
        dataTypesAssertions,
        defaultValue: [isInstance, AppColor],
        describeMessage: 'должен корректно обрабатывать создание экземпляра \'AppColor\' с переданным значением'
    });

    //TODO:test options types

    const staticMethodsArray = [
        'isHex',
        'isRgb',
        'isRgba',
        'isHsl',
        'isHsla',
        'type',
        'longHex',
        'hexToRgbObject',
        'parseRgb',
        'isLight'
    ];
    test.each(staticMethodsArray)('должен иметь статический метод \'%s\'', method => {
        const currentMethod = AppColor[method];
        expect(currentMethod).toBeDefined();
    });

    test('должен сохраняться под именем \'color\' в свойстве \'modules\' у элементов', () => {
        const colorEl = document.createElement('div');
        colorEl.style.backgroundColor = '';
        document.body.append(colorEl);

        new AppColor(colorEl); /* eslint-disable-line no-new */
        const colorInstanceProp = colorEl.modules.color;

        expect(colorInstanceProp).toBeInstanceOf(AppColor);
    });

    test('должен корректно инициализироваться на элементах с атрибутом \'data-color-bg\'', () => {
        const colorInstance = colorWithDataEl.modules.color;
        expect(colorInstance).toBeInstanceOf(AppColor);
    });

    describe('опции по умолчанию', () => {
        const colorOptionsMap = [['lightColor', 225]];
        test.each(colorOptionsMap)('у экземпляра должна быть установлена опция \'%s\' по умолчанию', (option, value) => {
            const colorEl = document.createElement('div');
            colorEl.style.backgroundColor = '';
            document.body.append(colorEl);

            new AppColor(colorEl); /* eslint-disable-line no-new */
            const instanceOptions = colorEl.modules.color.options;

            const currentOption = instanceOptions[option];
            expect(currentOption).toBe(value);
        });
    });

    describe('[data-color-options]', () => {
        test('должен корректно инициализировать экземпляр с опциями, указанными в атрибуте \'data-color-options\'', () => {
            const colorEl = document.createElement('div');
            colorEl.dataset.colorOptions = '{"lightColor": 200}';
            document.body.append(colorEl);

            const colorInstance = new AppColor(colorEl);
            const instanceOptions = colorInstance.options;

            expect(instanceOptions.lightColor).toBe(200);
        });

        test('должен выбрасывать ошибку при некорректном значении атрибута \'data-color-options\'', () => {
            const colorEl = document.createElement('div');
            colorEl.dataset.colorOptions = 'true';
            document.body.append(colorEl);

            const colorInstance = (() => new AppColor(colorEl));

            expect(colorInstance).toThrow('incorrect data-color-options format');
        });
    });

    describe('доступные свойства и методы экземпляра', () => {
        const colorPropsArray = [
            'color',
            'setLightElement',
            'isHex',
            'isRgb',
            'isRgba',
            'isHsl',
            'isHsla',
            'type',
            'longHex',
            'hexToRgbObject',
            'parseRgb',
            'isLight'
        ];
        test.each(colorPropsArray)('у экземпляра должно быть доступно свойство \'%s\' по умолчанию', property => {
            const colorWithoutDataEl = document.createElement('div');
            colorWithoutDataEl.style.backgroundColor = '';
            document.body.append(colorWithoutDataEl);
            const colorInstance = new AppColor(colorWithoutDataEl);

            const currentProp = colorInstance[property];
            expect(typeof currentProp).toBeDefined();
        });
    });

    //TODO:test DOM color & setLightElement methods
});

//TODO:test color, isHex, isRgb, isRgba, isHsl, isHsla, isLight, setLightElement

//TODO:check base module props in components/module tests

//TODO:test readonly instance methods