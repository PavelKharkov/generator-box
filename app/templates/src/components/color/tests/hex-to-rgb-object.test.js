import {dataTypesCheck} from 'Base/scripts/test';

import AppColor from '../sync.js';

//Функция проверки вхождения числа в диапазон hex
const isInRgbRange = value => {
    return value >= 0 && value < 256;
};

describe('AppColor', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.hexToRgbObject()', () => {
        //TODO:test static & not-static

        dataTypesCheck({
            defaultValue: null,
            checkFunction(value) {
                return AppColor.hexToRgbObject(value);
            }
        });

        test('должен возвращать объект с ключами \'red\', \'green\', \'blue\'', () => {
            const colorString = '#666';
            const rgbObject = AppColor.hexToRgbObject(colorString);
            const expectedObject = {
                red: 102,
                green: 102,
                blue: 102
            };
            expect(rgbObject).toStrictEqual(expectedObject);
        });

        test('должен возвращать объект с ключами, содержащими только значения типа \'number\'', () => {
            const colorString = '#666';
            const rgbObject = AppColor.hexToRgbObject(colorString);
            const expectedType = 'number';

            expect(typeof rgbObject.red).toBe(expectedType);
            expect(typeof rgbObject.green).toBe(expectedType);
            expect(typeof rgbObject.blue).toBe(expectedType);
        });

        test('должен возвращать объект, содержащий только числа, валидные в диапазоне rgb', () => {
            const colorString = '#666';
            const rgbObject = AppColor.hexToRgbObject(colorString);

            expect(isInRgbRange(rgbObject.red)).toBe(true);
            expect(isInRgbRange(rgbObject.green)).toBe(true);
            expect(isInRgbRange(rgbObject.blue)).toBe(true);
        });

        test('должен корректно преобразовывать цвета в форрмате \'hex\', если экземпляр проинициализирован с цветовой строкой', () => {
            const colorString = '#333';
            const rgbObject = new AppColor(colorString).hexToRgbObject();
            const expectedObject = {
                red: 51,
                green: 51,
                blue: 51
            };

            expect(rgbObject).toStrictEqual(expectedObject);
        });
    });
});