import {dataTypesCheck} from 'Base/scripts/test';

import AppColor from '../sync.js';

describe('AppColor', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.type()', () => {
        //TODO:test static & not-static

        dataTypesCheck({
            defaultValue: null,
            checkFunction(value) {
                return AppColor.type(value);
            }
        });

        const formatMap = [
            ['hex', '#999'],
            ['rgb', 'rgb(153,153,153)'],
            ['rgba', 'rgba(153,153,153,.5)'],
            ['hsl', 'hsl(255,100%,25%)'],
            ['hsla', 'hsla(255,100%,25%,.5)']
        ];
        test.each(formatMap)('должен возвращать правильный формат цвета, если получает валидный цвет в формате \'%s\'', (format, value) => {
            const colorType = AppColor.type(value);
            expect(colorType).toBe(format);
        });

        describe('невалидные значения', () => {
            const invalidFormatsMap = [
                ['hex', '#6666gg'],
                ['hex length', '#44444'],
                ['rgb', 'rgb(255,255,256)'],
                ['rgba', 'rgba(255,255,255,1.1)'],
                ['hsl', 'hsl(255,101%,25%)'],
                ['hsla', 'hsla(255,100%,25%,1.5)']
            ];
            test.each(invalidFormatsMap)('должен возвращать null, если получает невалидное \'%s\' значение', (format, received) => {
                const colorType = AppColor.type(received);
                expect(colorType).toBeNull();
            });
        });

        test('должен корректно выводить тип, если инициализирован с цветовой строкой', () => {
            const colorString = '#333';
            const colorType = new AppColor(colorString).type();

            expect(colorType).toBe('hex');
        });

        describe('тесты DOM', () => {
            /* eslint-disable array-bracket-newline */
            const convertFormatMap = [
                ['rgb', 'hex', '#999'],
                ['rgb', 'rgb', 'rgb(153,153,153)'],
                ['rgba', 'rgba', 'rgba(153,153,153,.5)'],
                ['rgb', 'hsl', 'hsl(255,100%,25%)'],
                ['rgba', 'hsla', 'hsla(255,100%,25%,.5)']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(convertFormatMap)('должен возвращать тип \'%s\', если вызван у экземпляра, инициализированного на DOM-элементе ' +
                'с css-свойством background-color в формате \'%s\' ' +
                '(браузеры преобразовывают все цветовые значения в формат rgb)', (expected, type, color) => {
                const colorEl = document.createElement('div');
                colorEl.style.backgroundColor = color;
                document.body.append(colorEl);
                const colorInstance = new AppColor(colorEl);
                const colorType = colorInstance.type();

                expect(colorType).toBe(expected);
            });

            test('должен корректно возвращать тип после изменения css-свойства \'background-color\' элемента', () => {
                const colorEl = document.createElement('div');
                colorEl.style.backgroundColor = 'rgb(153,153,153)';
                document.body.append(colorEl);

                const colorInstance = new AppColor(colorEl);
                const initialType = colorInstance.type();
                colorEl.style.backgroundColor = 'rgba(153,153,153,.5)';
                const newType = colorInstance.type();

                expect(initialType).not.toBe(newType);
            });

            test('должен возвращать \'null\', если вызван на элементе с некорректным или отсутствующим css-свойством \'background-color\'', () => {
                const colorEl = document.createElement('div');
                colorEl.style.backgroundColor = 'not-color';
                document.body.append(colorEl);

                const colorInstance = new AppColor(colorEl);
                const colorType = colorInstance.type();

                expect(colorType).toBeNull();
            });
        });
    });
});