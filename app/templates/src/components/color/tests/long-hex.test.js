import {dataTypesCheck} from 'Base/scripts/test';

import AppColor from '../sync.js';

describe('AppColor', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.longHex()', () => {
        //TODO:test static & not-static, test hex with alpha

        dataTypesCheck({
            defaultValue: null,
            checkFunction(value) {
                return AppColor.longHex(value);
            }
        });

        test('должен возвращать \'null\', если передан цвет в не-hex формате', () => {
            const colorString = 'rgb(0,0,0)';
            const longHex = AppColor.longHex(colorString);

            expect(longHex).toBeNull();
        });

        test('должен возвращать длинное значение \'hex\', если был передан цвет в коротком формате hex', () => {
            const colorString = '#fff';
            const longHex = AppColor.longHex(colorString);
            const expectedHex = '#ffffff';

            expect(longHex).toBe(expectedHex);
        });

        test('должен корректно дублировать значения из короткого hex в длинный', () => {
            const colorString = '#f3c';
            const longHex = AppColor.longHex(colorString);
            const expectedHex = '#ff33cc';

            expect(longHex).toBe(expectedHex);
        });

        test('должен возвращать переданное значение, если был передан длинный hex', () => {
            const colorString = '#999999';
            const longHex = AppColor.longHex(colorString);

            expect(longHex).toBe(colorString);
        });

        test('должен корректно преобразовывать цвет в формате \'hex\', если инициализирован с цветовой строкой', () => {
            const colorString = '#333';
            const longHex = new AppColor(colorString).longHex();
            const expectedHex = '#333333';

            expect(longHex).toStrictEqual(expectedHex);
        });
    });
});