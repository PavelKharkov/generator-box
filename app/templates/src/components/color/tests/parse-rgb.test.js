import {dataTypesCheck} from 'Base/scripts/test';

import AppColor from '../sync.js';

describe('AppColor', () => { /* eslint-disable-line jest/prefer-lowercase-title */
    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('.parseRgb()', () => {
        //TODO:test static & not-static

        dataTypesCheck({
            defaultValue: null,
            checkFunction(value) {
                return AppColor.parseRgb(value);
            }
        });

        test('должен возвращать объект с ключами \'red\', \'green\', \'blue\', \'alpha\'', () => {
            const colorString = 'rgba(102,51,153,0.5)';
            const rgbObject = AppColor.parseRgb(colorString);
            const expectedObject = {
                red: 102,
                green: 51,
                blue: 153,
                alpha: 0.5
            };
            expect(rgbObject).toStrictEqual(expectedObject);
        });

        test('должен корректно обрабатывать цвет, если инициализирован с цветовой строкой', () => {
            const colorString = 'rgba(102,51,153,0.5)';
            const rgbObject = new AppColor(colorString).parseRgb();
            const expectedObject = {
                red: 102,
                green: 51,
                blue: 153,
                alpha: 0.5
            };

            expect(rgbObject).toStrictEqual(expectedObject);
        });
    });
});