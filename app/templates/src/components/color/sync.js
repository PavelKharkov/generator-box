import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'color';

const rgbaKeys = [
    'red',
    'green',
    'blue',
    'alpha'
];

const hslaKeys = [
    'hue',
    'saturation',
    'lightness',
    'alpha'
];

//Опции по умолчанию
const defaultOptions = {
    lightColor: 225 //Цвет считается светлым, если больше данного значения
};

/**
 * @class AppColor
 * @memberof components
 * @classdesc Модуль для работы с цветом.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {(HTMLElement|string)} [element] - Элемент у которого будет вычисляться цвет или строка с цветом.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {number} [options.lightColor=225] - С какого значения цвет считается светлым.
 * @ignore
 * @example
 * const colorInstance = new app.Color('#ff0000'); //Можно инициализировать цвет из строки
 *
 * const colorElementInstance = new app.Color(document.querySelector('.color-element')); //Можно инициализировать цвет из элемента
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-color style="background-color: #ff0000;"></div>
 *
 * <!--data-color - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-color-options='{"lightColor": 200}' style="background-color: #ff0000;"></div>
 */
const AppColor = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        if (this.el === document) {
            util.error('can\'t initialize \'Color\' instance on document');
        }

        const isHTMLElement = el instanceof HTMLElement;

        if (isHTMLElement) {
            const colorOptionsData = el.dataset.colorOptions;
            if (colorOptionsData) {
                const dataOptions = util.stringToJSON(colorOptionsData);
                if (!dataOptions) util.error('incorrect data-color-options format');
                util.extend(this.options, dataOptions);
            }
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppColor#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let setLightElement;

        Object.defineProperty(this, 'setLightElement', {
            enumerable: true,
            get: () => setLightElement
        });

        let color;
        if (isHTMLElement) {
            color = () => window.getComputedStyle(el).backgroundColor;

            /**
             * @function AppColor#setLightElement
             * @memberof components
             * @desc Добавляет атрибут data-color-bg="light", если background-color элемента является светлым (определяется по опции lightColor).
             * @returns {boolean} Является ли элемент светлым.
             * @readonly
             * @example
             * const colorElementInstance = new app.Color(document.querySelector('.color-element'));
             * colorElementInstance.setLightElement();
             *
             * @example
             * const colorElementInstance = new app.Color('#ffffff');
             * console.log(colorElementInstance.setLightElement); //Вернёт undefined, у экземпляров, инициализированных через строку, нет данного метода
             */
            setLightElement = () => {
                const isLight = this.constructor.isLight(color());

                if (isLight) {
                    el.dataset.colorBg = 'light';
                } else {
                    delete el.dataset.colorBg;
                }
                return isLight;
            };

            setLightElement();
        } else {
            color = () => el;
        }

        /**
         * @member {string} AppColor#color
         * @memberof components
         * @desc Строковое значение цвета. Если экземпляр был инициализирован с помощью элемента, то возвращает цвет фона элемента.
         * @readonly
         * @example
         * const colorInstance = new app.Color('#ff0000');
         * console.log(colorInstance.color); //Выведет переданное значение
         *
         * const colorElementInstance = new app.Color(document.querySelector('.color-element'));
         * console.log(colorElementInstance.color); //Выведет значение стиля background-color элемента
         */
        Object.defineProperty(this, 'color', {
            enumerable: true,
            get: color
        });

        //Добавление статичных методов экземпляру класса
        Object.defineProperty(this, 'isHex', {
            enumerable: true,
            value: () => this.constructor.isHex(color())
        });
        Object.defineProperty(this, 'isRgb', {
            enumerable: true,
            value: () => this.constructor.isRgb(color())
        });
        Object.defineProperty(this, 'isRgba', {
            enumerable: true,
            value: () => this.constructor.isRgba(color())
        });
        Object.defineProperty(this, 'isHsl', {
            enumerable: true,
            value: () => this.constructor.isHsl(color())
        });
        Object.defineProperty(this, 'isHsla', {
            enumerable: true,
            value: () => this.constructor.isHsla(color())
        });
        Object.defineProperty(this, 'type', {
            enumerable: true,
            value: () => this.constructor.type(color())
        });
        Object.defineProperty(this, 'longHex', {
            enumerable: true,
            value: () => this.constructor.longHex(color())
        });
        Object.defineProperty(this, 'hexToRgbObject', {
            enumerable: true,
            value: () => this.constructor.hexToRgbObject(color())
        });
        Object.defineProperty(this, 'parseRgb', {
            enumerable: true,
            value: () => this.constructor.parseRgb(color())
        });
        Object.defineProperty(this, 'parseHsl', {
            enumerable: true,
            value: () => this.constructor.parseHsl(color())
        });
        Object.defineProperty(this, 'hslToRgbObject', {
            enumerable: true,
            value: () => this.constructor.hslToRgbObject(color())
        });
        Object.defineProperty(this, 'isLight', {
            enumerable: true,
            value: () => this.constructor.isLight(color(), params)
        });
    }

    //Вспомогательные функции
    /**
     * @function AppColor.isHex
     * @memberof components
     * @desc Определяет, является ли цвет формата hex. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {boolean}
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('#fff');
     * console.log(colorInstance.isHex()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isHex('rgb(255,0,0)')); //false
     *
     * @example <caption>Пример проверки цвета с прозрачностью</caption>
     * console.log(app.Color.isHex('#ff9900ff')); //true
     */
    static isHex(colorString) {
        return /^#(?:[\da-f]{3,4}){1,2}$/i.test(colorString);
    }

    /**
     * @function AppColor.isRgb
     * @memberof components
     * @desc Определяет, является ли цвет формата rgb. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {boolean}
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('rgb(255,0,0)');
     * console.log(colorInstance.isRgb()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isRgb('rgb(255,0,0)')); //true
     */
    static isRgb(colorString) {
        return new RegExp('^rgb[(](?:\\s*0*(?:\\d\\d?(?:\\.\\d+)?(?:\\s*%)?|\\.\\d+\\s*%|100(?:\\.0*)?\\s*%|(?:1\\d\\d|2[0-4]\\d|25[0-5])(?:\\.\\d+)?)\\s*(?:,(?![)])|(?=[)]))){3}[)]$').test(colorString);
    }

    /**
     * @function AppColor.isRgba
     * @memberof components
     * @desc Определяет, является ли цвет формата rgba. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {boolean}
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('rgba(255,0,0,0.5)');
     * console.log(colorInstance.isRgba()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isRgba('rgba(255,0,0,0.5)')); //true
     */
    static isRgba(colorString) {
        return new RegExp('^rgba[(](?:\\s*0*(?:\\d\\d?(?:\\.\\d+)?(?:\\s*%)?|\\.\\d+\\s*%|100(?:\\.0*)?\\s*%|(?:1\\d\\d|2[0-4]\\d|25[0-5])(?:\\.\\d+)?)\\s*,){3}\\s*0*(?:\\.\\d+|0|1(?:\\.0*)?)\\s*[)]$').test(colorString);
    }

    /**
     * @function AppColor.isHsl
     * @memberof components
     * @desc Определяет, является ли цвет формата hsl. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {boolean}
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('hsl(120,100%,50%)');
     * console.log(colorInstance.isHsl()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isHsl('hsl(120,100%,50%)')); //true
     */
    static isHsl(colorString) {
        return new RegExp('^hsl[(]\\s*0*(?:[12]?\\d{1,2}|3(?:[0-5]\\d|60))\\s*(?:\\s*,\\s*0*(?:\\d\\d?(?:\\.\\d+)?\\s*%|\\.\\d+\\s*%|100(?:\\.0*)?\\s*%)){2}\\s*[)]$').test(colorString);
    }

    /**
     * @function AppColor.isHsla
     * @memberof components
     * @desc Определяет, является ли цвет формата hsla. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {boolean}
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('hsla(120,100%,50%,0.3)');
     * console.log(colorInstance.isHsla()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isHsla('hsla(120,100%,50%,0.3)')); //true
     */
    static isHsla(colorString) {
        return new RegExp('^hsla[(]\\s*0*(?:[12]?\\d{1,2}|3(?:[0-5]\\d|60))\\s*(?:\\s*,\\s*0*(?:\\d\\d?(?:\\.\\d+)?\\s*%|\\.\\d+\\s*%|100(?:\\.0*)?\\s*%)){2}\\s*,\\s*0*(?:\\.\\d+|1(?:\\.0*)?)\\s*[)]$').test(colorString);
    }

    /**
     * @function AppColor.type
     * @memberof components
     * @desc Определяет тип цвета в строке. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {(string|Object)} ('hex', 'rgb', 'rgba', 'hsl', 'hsla') тип обозначения цвета. Возвращает null в случае, если не подошен ни один тип цвета.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('rgba(0,0,0,.5)');
     * console.log(colorInstance.type()); //'rgba'
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.type('rgba(0,0,0,.5)')); //'rgba'
     *
     * @example <caption>Пример невалидного цвета</caption>
     * console.log(app.Color.type('red')); //null
     */
    static type(colorString) {
        if (this.isHex(colorString)) return 'hex';

        if (this.isRgb(colorString)) return 'rgb';

        if (this.isRgba(colorString)) return 'rgba';

        if (this.isHsl(colorString)) return 'hsl';

        if (this.isHsla(colorString)) return 'hsla';

        return null;
    }

    /**
     * @function AppColor.longHex
     * @memberof components
     * @desc Переводит короткий hex в длинный. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {(string|Object)} Длинный hex. Возвращает null, если передавался невалидный hex.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('#f90');
     * console.log(colorInstance.longHex()); //'#ff9900'
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.longHex('#f90')); //'#ff9900'
     *
     * @example <caption>Перевод hex-цвета с прозрачностью</caption>
     * console.log(app.Color.longHex('#f909')); //'#ff990099'
     */
    static longHex(colorString) {
        const isHex = this.isHex(colorString);
        if (!isHex) return null;
        if (isHex && ((colorString.length === 7) || (colorString.length === 9))) { //Уже длинный hex
            return colorString;
        }

        const result = [...colorString.slice(1)].reduce((accumulator, symbol) => {
            return accumulator + symbol.repeat(2);
        }, '#');
        return result;
    }

    /**
     * @function AppColor.hexToRgbObject
     * @memberof components
     * @desc Переводит hex-строку цвета в rgba-объект. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Hex-строка с цветом (короткий или длинный).
     * @returns {Object} Объект со свойствами red, green, blue, alpha, или null, если была передана строка не в формате hex.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('#f90');
     * console.log(colorInstance.hexToRgbObject()); //{red: 255, green: 153, blue: 0}
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.hexToRgbObject('#ff990080')); //{red: 255, green: 153, blue: 0, alpha: 0.5}
     *
     * @example <caption>Перевод hex-цвета с прозрачностью</caption>
     * console.log(app.Color.hexToRgbObject('#f909')); //{red: 255, green: 153, blue: 0, alpha: 0.6}
     */
    static hexToRgbObject(colorString) {
        const hexString = this.longHex(colorString);
        const hexArray = new RegExp('^#?([a-f\\d]{2})([a-f\\d]{2})([a-f\\d]{2})([a-f\\d]{2})?$', 'i').exec(hexString);
        if (!hexArray) return null;
        const rgbObject = {};

        hexArray.slice(1).forEach((hexPart, index) => {
            if (typeof hexPart === 'undefined') return;
            rgbObject[rgbaKeys[index]] = hexPart && Number.parseInt(hexPart, 16);
        });
        if (rgbObject.alpha) {
            rgbObject.alpha /= 255;
        }

        return rgbObject;
    }

    /**
     * @function AppColor.parseRgb
     * @memberof components
     * @desc Переводит строку rgb() или rgba() в rgba-объект. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {Object} Объект со свойствами red, green, blue, alpha, или null, если передано невалидное значение.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('rgb(0,0,0)');
     * console.log(colorInstance.parseRgb()); //{red: 0, green: 0, blue: 0}
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.parseRgb('rgba(0,0,0,.5)')); //{red: 0, green: 0, blue: 0, alpha: 0.5}
     */
    static parseRgb(colorString) {
        if ((typeof colorString !== 'string') || (!this.isRgb(colorString) && !this.isRgba(colorString))) {
            return null;
        }

        const rgbArray = colorString.slice(colorString.indexOf('(') + 1).split(',');
        const rgbObject = {};
        rgbArray.forEach((value, index) => {
            rgbObject[rgbaKeys[index]] = (index === (rgbaKeys.length - 1))
                ? Number.parseFloat(value)
                : Number.parseInt(value, 10);
        });

        return rgbObject;
    }

    /**
     * @function AppColor.parseHsl
     * @memberof components
     * @desc Переводит строку hsl() или hsla() в hsla-объект. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @returns {Object} Объект со свойствами hue, saturation, lightness, alpha, или null, если передано невалидное значение.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('hsl(185, 45%, 45%)');
     * console.log(colorInstance.parseHsl()); //{hue: 185, saturation: 0.45, lightness: 0.45}
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.parseHsl('hsla(185, 45%, 45%, .5)')); //{hue: 0, saturation: 0, lightness: 0, alpha: 0.5}
     */
    static parseHsl(colorString) {
        if ((typeof colorString !== 'string') || (!this.isHsl(colorString) && !this.isHsla(colorString))) {
            return null;
        }
        const hslArray = colorString.slice(colorString.indexOf('(') + 1).split(',');
        const hslObject = {};

        hslArray.forEach((value, index) => {
            if (index === 0) {
                hslObject[hslaKeys[index]] = Number.parseInt(value, 10);
            } else if (index < (hslaKeys.length - 1)) {
                hslObject[hslaKeys[index]] = Number.parseInt(value, 10) / 100;
            } else {
                hslObject[hslaKeys[index]] = Number.parseFloat(value);
            }
        });

        return hslObject;
    }

    /**
     * @function AppColor.hslToRgbObject
     * @memberof components
     * @desc Переводит строку hsl() или hsla() в rgba-объект. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Hsl-строка или hsla-строка с цветом.
     * @returns {Object} Объект со свойствами red, green, blue, alpha, или null, если передано невалидное значение.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('hsl(185, 45%, 45%)');
     * console.log(colorInstance.hslToRgbObject()); //{red: 63, green: 158, blue: 166}
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.hslToRgbObject('hsla(185, 45%, 45%, .5)')); //{red: 63, green: 158, blue: 166, alpha: 0.5}
     */
    static hslToRgbObject(colorString) {
        const hslObject = this.parseHsl(colorString);
        const rgbObject = {};
        let red;
        let green;
        let blue;
        const {
            hue,
            saturation,
            lightness,
            alpha
        } = hslObject;

        if (saturation === 0) {
            red = lightness;
            green = red;
            blue = red;
        } else {
            const color1 = (1 - Math.abs((2 * lightness) - 1)) * saturation;
            const color2 = color1 * (1 - Math.abs(((hue / 60) % 2) - 1));
            const color3 = lightness - (color1 / 2);

            if ((hue >= 0) && (hue < 60)) {
                red = color1;
                green = color2;
                blue = 0;
            } else if ((hue >= 60) && (hue < 120)) {
                red = color2;
                green = color1;
                blue = 0;
            } else if ((hue >= 120) && (hue < 180)) {
                red = 0;
                green = color1;
                blue = color2;
            } else if ((hue >= 180) && (hue < 240)) {
                red = 0;
                green = color2;
                blue = color1;
            } else if ((hue >= 240) && (hue < 300)) {
                red = color2;
                green = 0;
                blue = color1;
            } else if ((hue >= 300) && (hue < 360)) {
                red = color1;
                green = 0;
                blue = color2;
            }

            red = Math.round((red + color3) * 255);
            green = Math.round((green + color3) * 255);
            blue = Math.round((blue + color3) * 255);
        }

        Object.assign(rgbObject, {
            red,
            green,
            blue,
            alpha
        });

        return rgbObject;
    }

    /**
     * @function AppColor.isLight
     * @memberof components
     * @desc Определяет, является ли цвет светлым. Также доступен в виде метода экземпляра.
     * @param {string} colorString - Строка с цветом.
     * @param {Object} [options={}] - Опции.
     * @param {number} [options.lightColor=<значение из опций экземпляра>] - С какого числа цвет считается светлым.
     * @returns {(boolean|Object)} Возвращает null, если не удалось разобрать строку.
     * @example <caption>В виде метода экземпляра</caption>
     * const colorInstance = new app.Color('#eee');
     * console.log(colorInstance.isLight()); //true
     *
     * @example <caption>В виде статического метода</caption>
     * console.log(app.Color.isLight('rgba(200, 200, 200, .5)')); //false
     *
     * @example <caption>Проверка свотлости цвета у HTMl-элемента</caption>
     * const colorInstance = new app.Color(document.querySelector('.color-element')); //Элемент с цветом фона #eee
     * console.log(colorInstance.isLight()); //true
     */
    static isLight(colorString, options = {}) {
        if (!util.isObject(options)) {
            util.typeError(options, 'options', 'plain object');
        }

        //Преобразование переданного цвета в объект
        const color = (() => {
            switch (this.type(colorString)) {
                case 'hex':
                    return this.hexToRgbObject(colorString);
                case 'rgb':
                case 'rgba':
                    return this.parseRgb(colorString);
                case 'hsl':
                case 'hsla':
                    return this.hslToRgbObject(colorString);
                default:
                    return false;
            }
        })();
        if (!color) return null;

        util.defaultsDeep(options, defaultOptions);

        const lightColorsNum = Object.keys(color).reduce((accumulator, prop) => { //Проверка каждой части rgba-объекта цвета
            const isLightPart = (prop !== 'alpha') && (color[prop] > options.lightColor);
            return accumulator + (isLightPart ? 1 : 0);
        }, 0);

        return lightColorsNum >= 2; //Возвращает true, если две или более части цвета светлые
    }
});

addModule(moduleName, AppColor);

//Инициализация всех элементов цвета по data-атрибуту
document.querySelectorAll('[data-color], [data-color-bg]').forEach(el => new AppColor(el));

export default AppColor;
export {AppColor};