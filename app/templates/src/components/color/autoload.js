import './style.scss';

if (__IS_DISABLED_MODULE__) {
    if (__IS_SYNC__ || document.querySelector('[data-color], [data-color-init]')) {
        require('.');
    }
}