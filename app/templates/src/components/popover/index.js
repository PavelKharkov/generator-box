import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let popoverTriggered;

const popoverCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popovers" */ './sync.js'))
        .then(modules => {
            chunks.popovers = true;
            popoverTriggered = true;

            const AppPopover = modules.default;
            if (resolve) resolve(AppPopover);
        });
};

const initPopover = event => {
    if (popoverTriggered) return;

    event.currentTarget.dataset.popoverTriggered = '';

    emitInit('popover');
};

const popoverTrigger = (popoverItems, popoverTriggers = ['click', 'mouseenter']) => {
    if (__IS_SYNC__) return;

    const items = (popoverItems instanceof Node) ? [popoverItems] : popoverItems;
    if (items.length === 0) return;

    items.forEach(item => {
        popoverTriggers.forEach(trigger => {
            item.addEventListener(trigger, initPopover, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popovers) {
        popoverCallback(resolve);
        return;
    }

    onInit('popover', () => {
        popoverCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('popover');
    });

    const popoverItems = document.querySelectorAll('[data-popover]');
    popoverTrigger(popoverItems);
});

export default importFunc;
export {popoverTrigger};