import createPopper from 'Libs/popper';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'popover';

const popoverPlugin = createPopper;

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

//Селекторы и классы
const popoverClass = moduleName;
const headerClass = `${popoverClass}-header`;
const bodyClass = `${popoverClass}-body`;
const arrowClass = `${popoverClass}-arrow`;
const showClass = 'show';
const tooltipClass = 'tooltip';
const popoverCloseSelector = `.${popoverClass}-close`;
const initializedClass = `${moduleName}-initialized`;

const ariaDescribedby = 'aria-describedby';

//Опции по умолчанию
const defaultOptions = {
    duration: transitionDuration,
    boundary: 'clippingParents',
    container: document.body,
    html: true,
    outsideClickHide: true,
    placement: 'top',
    flipVariations: true,
    trigger: 'click'
};

const defaultTooltipTrigger = ['mouseenter', 'mouseleave'];

/**
 * @function template
 * @desc Добавляет дополнительный класс в шаблон информера.
 * @param {string} [newClass] - Дополнительный класс информера.
 * @returns {string} HTML-строка шаблона информера.
 * @ignore
 */
const template = newClass => {
    return `
        <div class="${popoverClass + (newClass ? ` ${newClass}` : '')}" role="tooltip">
            <div class="${arrowClass}"></div>
            <div class="${headerClass}"></div>
            <div class="${bodyClass}"></div>
        </div>`;
};

/**
 * @class AppPopover
 * @memberof components
 * @requires libs.popper
 * @requires components#AppUrl
 * @classdesc Модуль информеров.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент информера.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {(string|Function|HTMLElement|boolean)} [options.title] - Заголовок информера.
 * @param {(string|Function|HTMLElement)} [options.content] - Основной контент информера.
 * @param {string} [options.class] - Произвольный класс, добавляемый в шаблон информера.
 * @param {boolean} [options.html=true] - Отображать ли контент информера в виде HTML.
 * @param {string} [options.template] - HTML-шаблон информера. По умолчанию повторяет HTML-шаблон информера бутстрапа.
 * @param {boolean} [options.outsideClickHide=true] - Скрывать ли информер при нажатии вне его.
 * @param {number} [options.duration=Number.parseInt({@link app.util.getStyle}('--transition-duration'), 10)] - Длительность анимации появления информера.
 * @param {('clippingParents'|HTMLElement|Function)} [options.boundary='clippingParents'] - В границах какого элемента пытаться уместить выпадающее меню.
 * @param {(HTMLElement|Function)} [options.container=document.body] - В конец какого элемента добавлять элемент информера.
 * @param {(string|Function)} [options.placement='top'] - Положение информера относительно активирующего элемента по умолчанию.
 * @param {boolean} [options.flipVariations=true] - Изменять ли направление показа информера на противоположное, если он целиком не помещается в границах опции boundary.
 * @param {(string|Array.<string>)} [options.trigger='click'] - Событие переключения информера, или колекция из двух событий для показа и скрытия информера. При опции tooltip=true, данная опция по умолчанию соответствует ['mouseenter', 'mouseleave'].
 * @param {boolean} [options.tooltip] - Применять ли к информеру стили и поведение информера-подсказки.
 * @param {number} [options.offset] - Отступ элемента информера от активирующего элемента.
 * @example
 * const popoverInstance = new app.Popover(document.querySelector('.popover-link'), {
 *     title: false, //Не показывать заголовок
 *     content: '<p>Текст информера</p>',
 *     tooltip: true,
 *     boundary: document.body
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <button type="button" data-popover title="Заголовок информера">Показать информер</button>
 *
 * <!--data-popover - селектор по умолчанию-->
 *
 * @example <caption>Активация с помощью хеша</caption>
 * <!--HTML-->
 * <div id="popover-button" data-hash-action="popover">Показать информер</div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-popover-options='{"tooltip": true}'></div>
 */
const AppPopover = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const popoverOptionsData = el.dataset.popoverOptions;
        if (popoverOptionsData) {
            const dataOptions = util.stringToJSON(popoverOptionsData);
            if (!dataOptions) util.error('incorrect data-popover-options format');
            util.extend(this.options, dataOptions);
        }

        //Применение событий триггера тултипа по умолчанию, если не touch-устройство
        if (this.options.tooltip && !util.isTouch && (typeof this.options.trigger === 'undefined')) {
            this.options.trigger = defaultTooltipTrigger;
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppPopover#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const titleAttr = el.title;
        el.removeAttribute('title');

        let isShown = false;

        /**
         * @member {boolean} AppPopover#isShown
         * @memberof components
         * @desc Указывает, показан ли элемент.
         * @readonly
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        //TODO:options docs: delay

        //TODO:init on popoverElement
        let popoverElement;

        /**
         * @member {HTMLElement} AppPopover#popoverElement
         * @memberof components
         * @desc Элемент открытого информера.
         * @readonly
         */
        Object.defineProperty(this, 'popoverElement', {
            enumerable: true,
            get: () => popoverElement
        });

        if (el.classList.contains('popover')) {
            popoverElement = el;
        }

        if (params.trigger && !popoverElement) {
            const triggers = Array.isArray(params.trigger) ? params.trigger : [params.trigger];
            triggers.forEach((trigger, triggerIndex) => {
                el.addEventListener(trigger, event => {
                    event.preventDefault();

                    if (triggers.length === 1) {
                        if (isShown) {
                            this.hide(params);
                            return;
                        }

                        this.show(params);
                    } else if (triggerIndex === 0) {
                        this.show(params);
                    } else {
                        this.hide(params);
                    }
                });
            });
        }

        let toggleTimeout;
        let placement;

        /**
         * @member {string} AppPopover#placement
         * @memberof components
         * @desc Текущее позиионирование открытого информера.
         * @readonly
         */
        Object.defineProperty(this, 'placement', {
            enumerable: true,
            get: () => placement
        });

        let containerElement;

        /**
         * @member {HTMLElement} AppPopover#containerElement
         * @memberof components
         * @desc Элемент, в который добавлен текущий открытый информер.
         * @readonly
         */
        Object.defineProperty(this, 'containerElement', {
            enumerable: true,
            get: () => containerElement
        });

        let uid;

        /**
         * @member {number} AppPopover#uid
         * @memberof components
         * @desc Идентификатор информера, по которому он будет связан с активирующим элементом в атрибутах.
         * @readonly
         */
        Object.defineProperty(this, 'uid', {
            enumerable: true,
            get: () => uid
        });

        let pluginInstance;

        /**
         * @member {Object} AppPopover#pluginInstance
         * @memberof components
         * @desc Объект экземпляра плагина отображения информера.
         * @readonly
         */
        Object.defineProperty(this, 'pluginInstance', {
            enumerable: true,
            get: () => pluginInstance
        });

        if (params.outsideClickHide) {
            document.addEventListener('click', event => {
                if (isShown &&
                    !el.contains(event.target) &&
                    !popoverElement.contains(event.target)) {
                    this.hide();
                }
            });
        }

        //Внутренние функции

        /**
         * @function updatePlacement
         * @desc Обновляет позиционирование информера.
         * @param {string} newPlacement - Новое позиционирование информера.
         * @returns {undefined}
         * @ignore
         */
        const updatePlacement = newPlacement => {
            popoverElement.classList.remove(`${popoverClass}-${placement}`);
            popoverElement.classList.add(`${popoverClass}-${newPlacement}`);
            placement = newPlacement;
        };

        /**
         * @function clearPopover
         * @desc Очищает элемент информера от вспомогательных классов и атрибутов после закрытия.
         * @returns {undefined}
         * @ignore
         */
        const clearPopover = () => {
            if (!pluginInstance) return;

            util.deactivate(el);

            el.removeAttribute(ariaDescribedby);

            if ((containerElement !== false) && containerElement.contains(popoverElement)) {
                popoverElement.remove();
            }
            pluginInstance.destroy();
        };

        this.on({
            /**
             * @event AppPopover#show
             * @memberof components
             * @desc Показывает элемент экземпляра.
             * @param {Object} [options] - Опции открытия информера.
             * @param {(string|Function|HTMLElement|boolean)} [options.title] - Заголовок информера.
             * @param {(string|Function|HTMLElement)} [options.content] - Основной контент информера.
             * @param {string} [options.class] - Произвольный класс, добавляемый в шаблон информера.
             * @param {boolean} [options.html=true] - Отображать ли контент информера в виде HTML.
             * @param {string} [options.template] - HTML-шаблон информера. По умолчанию повторяет HTML-шаблон информера бутстрапа.
             * @param {boolean} [options.outsideClickHide=true] - Скрывать ли информер при нажатии вне его.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации появления информера.
             * @param {('clippingParents'|HTMLElement|Function)} [options.boundary='clippingParents'] - В границах какого элемента пытаться уместить выпадающее меню.
             * @param {(HTMLElement|Function)} [options.container=document.body] - В конец какого элемента добавлять элемент информера.
             * @param {(string|Function)} [options.placement='top'] - Положение информера относительно активирующего элемента по умолчанию.
             * @param {boolean} [options.flipVariations=true] - Изменять ли направление показа информера на противоположное, если он целиком не помещается в границах опции boundary.
             * @param {boolean} [options.tooltip] - Применять ли к информеру стили информера-подсказки.
             * @fires components.AppPopover#shown
             * @returns {undefined}
             * @example
             * popoverInstance.on('show', () => {
             *     console.log('Уведомление показывается');
             * });
             * console.log(popoverInstance.isShown); //false
             * popoverInstance.show({
             *     duration: 0
             * });
             * console.log(popoverInstance.isShown); //true
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (!el.offsetParent) return; //Если элемент скрыт

                clearPopover();
                clearTimeout(toggleTimeout);

                util.defaultsDeep(options, params);

                const title = (typeof options.title === 'undefined') ? titleAttr : options.title;
                const popoverTitle = (typeof title === 'function') ? title.call(this) : title;
                const content = options.content;
                const popoverContent = (typeof content === 'function') ? content.call(this) : content;
                const popoverTemplate = (options.template || template(options.class)).trim();

                placement = (typeof options.placement === 'function') ? options.placement.call(this) : options.placement;

                containerElement = (typeof options.container === 'function') ? options.container.call(this) : (options.container || el.parentNode);

                const offset = options.offset;
                const flipVariations = options.flipVariations;
                const boundary = (typeof options.boundary === 'function') ? options.boundary.call(this) : options.boundary;
                const duration = options.duration || 0;

                const popoverWrapper = document.createElement('div');
                popoverWrapper.innerHTML = popoverTemplate;
                popoverElement = popoverWrapper.firstChild;

                const headerElement = popoverElement.querySelector(`.${headerClass}`);
                const bodyElement = popoverElement.querySelector(`.${bodyClass}`);
                const arrowElement = popoverElement.querySelector(`.${arrowClass}`);

                uid = uid || util.uniqueId();
                popoverElement.id = `${popoverClass}-opened${uid}`;
                el.setAttribute(ariaDescribedby, `${popoverClass}-opened${uid}`);

                popoverElement.style.pointerEvents = 'none';
                if (duration) {
                    popoverElement.style.transitionDuration = `${duration}ms`;
                }
                if (options.tooltip) {
                    popoverElement.classList.add(tooltipClass);
                }
                util.animationDefer(() => {
                    popoverElement.classList.add(showClass);
                });

                if (headerElement && popoverTitle) {
                    if (popoverTitle instanceof HTMLElement) {
                        headerElement.innerHTML = '';
                        headerElement.append(popoverTitle);
                    } else {
                        headerElement.textContent = popoverTitle.trim();
                    }
                }

                if (!options.tooltip && bodyElement && popoverContent) {
                    if (popoverContent instanceof HTMLElement) {
                        bodyElement.innerHTML = '';
                        bodyElement.append(popoverContent);
                    } else {
                        bodyElement[options.html ? 'innerHTML' : 'textContent'] = util.unescapeHtml(popoverContent.trim());
                    }
                }

                containerElement.append(popoverElement);

                //Сохранить ссылку на модуль информера на элементе информера
                popoverElement.modules = {
                    [moduleName]: this
                };

                pluginInstance = popoverPlugin(el, popoverElement, {
                    placement,
                    modifiers: [
                        {
                            name: 'offset',
                            options: {
                                offset: [0, offset]
                            }
                        },
                        {
                            name: 'flip',
                            options: {
                                flipVariations
                            }
                        },
                        {
                            name: 'arrow',
                            options: {
                                element: arrowElement
                            }
                        },
                        {
                            name: 'preventOverflow',
                            options: {
                                boundary
                            }
                        },
                        {
                            name: 'updatePlacement',
                            enabled: true,
                            phase: 'afterWrite',
                            fn({state}) {
                                updatePlacement(state.placement);
                            }
                        }
                    ],
                    onFirstUpdate(state) {
                        updatePlacement(state.placement);
                    }
                });

                isShown = true;

                util.activate(el);

                //Инициализация кнопки закрыть внутри информера
                popoverElement.querySelectorAll(popoverCloseSelector).forEach(element => {
                    element.addEventListener('click', () => this.hide());
                });

                toggleTimeout = setTimeout(() => {
                    popoverElement.style.pointerEvents = 'auto';
                    this.emit('shown');
                }, duration);
            },

            /**
             * @event AppPopover#hide
             * @memberof components
             * @desc Скрывает элемент экземпляра.
             * @param {Object} [options] - Опции скрытия информера.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации появления информера.
             * @fires components.AppPopover#hidden
             * @returns {undefined}
             * @example
             * popoverInstance.on('hide', () => {
             *     console.log('Уведомление скрывается');
             * });
             * console.log(popoverInstance.isShown); //true
             * popoverInstance.hide({
             *     duration: 0
             * });
             * console.log(popoverInstance.isShown); //false
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (!isShown) return;
                isShown = false;

                clearTimeout(toggleTimeout);

                util.defaultsDeep(options, params);

                popoverElement.style.pointerEvents = 'none';
                popoverElement.classList.remove(showClass);

                toggleTimeout = setTimeout(() => {
                    clearPopover();

                    this.emit('hidden');
                }, options.duration);
            }
        });

        this.onSubscribe({
            /**
             * @event AppPopover#shown
             * @memberof components
             * @desc Вызывается, когда элемент полностью показан.
             * @fires components.AppPopover#shown
             * @returns {undefined}
             * @example
             * popoverInstance.on('shown', () => {
             *     console.log('Уведомление показано');
             * });
             */
            shown: true,

            /**
             * @event AppPopover#hidden
             * @memberof components
             * @desc Вызывается, когда элемент полностью скрыт.
             * @returns {undefined}
             * @example
             * popoverInstance.on('hidden', () => {
             *     console.log('Уведомление скрыто');
             * });
             */
            hidden() {
                updatePlacement(params.placement);
            }
        });

        el.classList.add(initializedClass);

        if (
            (typeof el.dataset.popoverTriggered !== 'undefined') &&
            (Array.isArray(params.trigger) && (params.trigger[0] === 'mouseenter'))
        ) {
            delete el.dataset.popoverTriggered;
            this.show();
        }
    }
});

//Настройка возможности управления с помощью адресной строки
require('Components/url').default.then(AppUrl => {
    AppUrl.registerAction({
        id: moduleName,
        selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
        action: `${moduleName}.show`
    });
});
if (window.location.hash) emitInit('url');

addModule(moduleName, AppPopover);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-popover]').forEach(el => new AppPopover(el));

export default AppPopover;
export {AppPopover};