import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'toaster';

//Классы
const initializedClass = `${moduleName}-initialized`;

//Опции нового уведомления по умолчанию
const defaultAppendOptions = {
    toast: true
};

const toasterInit = AppAlert => {
    /**
     * @class AppToaster
     * @memberof components
     * @requires components#AppAlert
     * @classdesc Контейнер для вывода попапов-уведомлений.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент экземпляра.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {boolean} [options.toast=true] - Добавлять ли уведомлениям модификатор toast.
     * @param {*} [options...] - Другие опции модуля [AppAlert]{@link components.AppAlert} и [AppAlert.template]{@link components.AppAlert.template}.
     * @example
     * const toasterInstance = new app.Toaster(document.querySelector('.toaster-element'), {
     *     toast: false
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="toaster -right -top" data-toaster></div>
     *
     * <!--data-toaster - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-toaster-options='{"toast": false}'></div>
     */
    const AppToaster = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const toasterOptionsData = el.dataset.toasterOptions;
            if (toasterOptionsData) {
                const dataOptions = util.stringToJSON(toasterOptionsData);
                if (!dataOptions) util.error('incorrect data-toaster-options format');
                util.extend(this.options, dataOptions);
            }

            /**
             * @member {Object} AppToaster#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            //TODO:store shown alerts
            //TODO:add icons

            this.on({
                /**
                 * @event AppToaster#append
                 * @memberof components
                 * @desc Показывает уведомление.
                 * @param {Object} [options={}] - Опции или экземпляр класса [AppAlert]{@link components.AppAlert}.
                 * @fires components.AppAlert#show
                 * @returns {undefined}
                 * @example
                 * toasterInstance.on('append', () => {
                 *     console.log('Новое уведомление добавлено');
                 * });
                 * toasterInstance.append(app.Alert.template({content: 'Уведомление'}));
                 */
                append(options = {}) {
                    const isObject = util.isObject(options);
                    const isAlert = options.constructor === AppAlert;

                    if (!(isObject || isAlert)) {
                        util.typeError(options, 'options', 'plain object or instance of an Alert class');
                    }

                    let alertInstance;

                    if (isObject) {
                        util.defaultsDeep(options, params);
                        util.defaultsDeep(options, defaultAppendOptions);

                        alertInstance = new AppAlert(AppAlert.template(options), options); //TODO:separate options
                    } else {
                        alertInstance = options;
                    }

                    el.append(alertInstance.alertEl);
                    util.defer(() => {
                        alertInstance.show();
                    });
                }

                //TODO:add clearLast & clear methods, listen to hide events for alerts
            });

            //TODO:event for toast shown & hidden

            el.classList.add(initializedClass);
        }
    });

    addModule(moduleName, AppToaster);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll('[data-toaster]').forEach(el => new AppToaster(el));

    return AppToaster;
};

export default toasterInit;
export {toasterInit};