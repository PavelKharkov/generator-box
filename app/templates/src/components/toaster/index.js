import './style.scss';
import './sync.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const toasterCallback = resolve => {
    const imports = [require('Components/alert').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "toggles" */ './sync.js'))
            .then(toaster => {
                chunks.toggles = true;

                const AppToaster = toaster.toasterInit(...modules);
                resolve(AppToaster);
            });
    });

    emitInit('alert');
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.toggles) {
        toasterCallback(resolve);
        return;
    }

    onInit('toaster', () => {
        toasterCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('toaster');
    });
});

export default importFunc;