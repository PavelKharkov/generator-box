import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'social';

//Классы
const initializedClass = `${moduleName}-initialized`;

const dataAttrs = [
    'accessToken',
    'bare',
    'counter',
    'copy',
    'description',
    'directions',
    'hashtags',
    'image',
    'lang',
    'limit',
    'popupDirection',
    'popupPosition',
    'services',
    'size',
    'title',
    'url',
    'nonce'
];

//Опции по умолчанию
const defaultOptions = {
    theme: {
        services: [
            'vkontakte',
            'facebook',
            'twitter',
            'odnoklassniki',
            'moimir',
            'gplus',
            'skype',
            'telegram'
        ].join(',')
    }
};

let scriptPlaced = false;
let share;

/**
 * @class AppSocial
 * @memberof components
 * @classdesc Модуль для кнопок социальных сетей.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент блока социальных сетей.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {Object} [options.theme={}] - Опции оформления.
 * @param {string} [options.theme.services='vkontakte,facebook,twitter,odnoklassniki,moimir,gplus,skype,telegram'] - Список выводимых социальных сетей через запятую.
 * @param {Object} [options.hooks={}] - Объект с функциями обратного вызова.
 * @param {Function} [options.hooks.onready] - Функция, вызываемая при инициализации плагина социальных сетей.
 * @param {*} [options...] - Другие опции плагина отображения социальных сетей [Yandex share]{@link https://tech.yandex.ru/share/}.
 * @example
 * const socialInstance = new app.Social(document.querySelector('.social-element'), {
 *     theme: {
 *         services: 'vkotankte,facebook'
 *     },
 *     hooks: {
 *         onready() {
 *             console.log('Плагин социальных сетей проинициализирован');
 *         }
 *     }
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-social data-services="vkontakte,facebook,twitter,odnoklassniki"></div>
 *
 * <!--data-social - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-social-options='{"theme": {"services": "vkontakte,facebook,twitter"}}'></div>
 */
const AppSocial = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        const socialOptionsData = el.dataset.socialOptions;
        if (socialOptionsData) {
            const dataOptions = util.stringToJSON(socialOptionsData);
            if (!dataOptions) util.error('incorrect data-social-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppSocial#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        if (!scriptPlaced) {
            util.addScript('//yastatic.net/share2/share.js', () => {
                if (window.Ya && window.Ya.share2) {
                    share = window.Ya.share2;
                }
            });
            scriptPlaced = true;
        }

        let instance;

        const getOptions = () => {
            return instance._options._options; /* eslint-disable-line no-underscore-dangle */
        };

        /**
         * @function AppSocial#getOptions
         * @memberof components
         * @desc Возвращает опции плагина.
         * @returns {Object} Объект с опциями.
         * @readonly
         */
        Object.defineProperty(this, 'getOptions', {
            enumerable: true,
            value: getOptions
        });

        params.theme = util.isObject(params.theme) ? params.theme : {};
        dataAttrs.forEach(dataAttr => {
            if (!el.dataset[dataAttr]) return;
            params.theme[dataAttr] = el.dataset[dataAttr];
        });

        params.hooks = util.isObject(params.hooks) ? params.hooks : {};
        const onReadyFunc = params.hooks.onready;
        params.hooks.onready = (() => {
            if (typeof onReadyFunc === 'function') onReadyFunc();

            util.defer(() => {
                util.defaultsDeep(params, getOptions()); //Сохранение опций из плагина в экземпляр модуля
            });
        });

        /**
         * @member {Object} AppSocial#instance
         * @memberof components
         * @desc Экземпляр плагина отображения социальных сетей [Yandex share]{@link https://tech.yandex.ru/share/}.
         * @readonly
         */
        Object.defineProperty(this, 'instance', {
            enumerable: true,
            get: () => instance
        });

        if (share) {
            instance = share(el, params);
        }

        el.classList.add(initializedClass);

        if (typeof el.dataset.socialTriggered !== 'undefined') {
            delete el.dataset.socialTriggered;
            //TODO:trigger click on init
        }
    }

    /**
     * @function AppScroll.share
     * @memberof components
     * @desc Get-функция. Содержит ссылку плагин для отображения социальных сетей [Yandex share]{@link https://tech.yandex.ru/share/}.
     * @returns {Object}
     */
    static get share() {
        return share;
    }
});

addModule(moduleName, AppSocial);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-social]').forEach(el => new AppSocial(el));

export default AppSocial;
export {AppSocial};