import './style.scss';

let importFunc;
let socialTrigger;

if (__IS_DISABLED_MODULE__) {
    let socialTriggered;

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const socialCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;
                socialTriggered = true;

                const AppSocial = modules.default;
                if (resolve) resolve(AppSocial);
            });
    };

    const initSocial = event => {
        if (socialTriggered) return;

        event.preventDefault();
        event.currentTarget.dataset.socialTriggered = '';

        emitInit('social');
    };

    socialTrigger = (socialItems, socialTriggers = ['click']) => {
        if (__IS_SYNC__) return;

        const items = (socialItems instanceof Node) ? [socialItems] : socialItems;
        if (items.length === 0) return;

        items.forEach(item => {
            socialTriggers.forEach(trigger => {
                item.addEventListener(trigger, initSocial, {once: true});
            });
        });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            socialCallback(resolve);
            return;
        }

        onInit('social', () => {
            socialCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('social');
        });

        const socialItems = document.querySelectorAll('[data-social]');
        socialTrigger(socialItems);
    });
}

export default importFunc;
export {socialTrigger};