import noUiSlider from 'Libs/nouislider';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//TODO:fix round, single slider, handle text
//TODO:fix aria values on container

//Опции
const moduleName = 'range';

const rangePlugin = noUiSlider;

//Селекторы и классы
const sliderPrefix = 'noUi';
const sliderSelector = `.${moduleName}-slider`;
const inputSelector = `.${moduleName}-input`;
const inputFromSelector = `.${moduleName}-input-from`;
const inputToSelector = `.${moduleName}-input-to`;
const handleClass = `.${sliderPrefix}handle`;
const initializedClass = `${moduleName}-initialized`;

const debounceDuration = 100;

const blurFunction = event => {
    event.target.style.outline = '';
};

//Функции форматирования отображаемого значения слайдера
const formatFunctions = {
    /**
     * @function formatFunctions.money
     * @desc Возвращает значение слайдера как цену.
     * @param {string} value - Значение слайдера.
     * @returns {string} Отформатированное значение.
     * @ignore
     */
    money(value) {
        return util.currencyFormat(Number.parseInt(value, 10));
    },

    /**
     * @function formatFunctions.default
     * @desc Возвращает значение слайдера в виде целого числа, перед этим удалив пробелы (по умолчанию).
     * @param {string} value - Значение слайдера.
     * @returns {number} Отформатированное значение.
     * @ignore
     */
    default(value) {
        return Number.parseInt(util.removeDelimiters(value, '', ' '), 10);
    }
};

//Опции по умолчанию
const defaultOptions = {
    connect: true,
    format: {
        from: 'default',
        to: 'default'
    },

    mask: true
};

let AppTouch;
require('Components/touch').default.then(touchModule => {
    AppTouch = touchModule;
});
emitInit('touch');

/**
 * @class AppRange
 * @memberof components
 * @requires libs.nouislider
 * @requires components#AppMask
 * @requires components#AppTouch
 * @classdesc Модуль слайдера диапазонов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент контейнера слайдера диапазонов.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.single] - Имеет ли слайдер только один ползунок.
 * @param {number} [options.start] - Начальные значения слайдера.
 * @param {number} [options.min] - Минимальное значение слайдера.
 * @param {number} [options.max] - Максимальное значение слайдера.
 * @param {(boolean|Array.<boolean>)} [options.connect=true] - Отображать ли заполненную область между ползунками слайдера, либо коллекция с перечислением отрезков слайдера.
 * @param {Object} [options.format] - Объект с функциями форматирования или идентификаторами функций значений слайдера. Возможные идентификаторы: 'default' (по умолчанию), 'money'.
 * @param {(string|Function)} [options.format.from='default'] - Форматирование значений, передаваемых слайдеру.
 * @param {(string|Function)} [options.format.to='default'] - Форматирование значений, приходящих от слайдера.
 * @param {boolean} [options.tooltips] - Показывать ли текущие значения слайдера у переключателей.
 * @param {(boolean|Object)} [options.mask=true] - Опции для подключаемого модуля маски [AppMask]{@link components.AppMask}. Если true то строка для маски вычисляется согласно функции format.to из опций экземпляра. Если false, то не подключать модуль маски.
 * @param {*} [options...] - Другие опции плагина [noUISlider]{@link libs.nouislider}.
 * @example
 * const rangeInstance = new app.Range(document.querySelector('.range-element'), {
 *     format: {
 *         to: 'money'
 *     },
 *     min: 100,
 *     max: 1500
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="range-container" role="slider" aria-valuemin="0" aria-valuemax="100" aria-valuenow="20" data-range>
 *     <div class="range-input-group">
 *         <label for="range-from">От</label>
 *         <input class="form-control range-input range-input-from" type="text" id="range-from" value="20" data-default="20">
 *     </div>
 *     <div class="range-input-group">
 *         <label for="range-to">До</label>
 *         <input class="form-control range-input range-input-to" type="text" id="range-to" value="80" data-default="80">
 *     </div>
 *     <div class="range-slider-wrapper">
 *         <div class="range-slider"></div>
 *     </div>
 * </div>
 *
 * <!--
 * data-range - селектор по умолчанию
 * data-default - значения полей слайдера по умолчанию
 * -->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-range-options='{"single": true}'></div>
 */
const AppRange = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const rangeOptionsData = el.dataset.rangeOptions;
        if (rangeOptionsData) {
            const dataOptions = util.stringToJSON(rangeOptionsData);
            if (!dataOptions) util.error('incorrect data-range-options format');
            util.defaultsDeep(this.options, dataOptions);
        }

        const inputFrom = el.querySelector(inputFromSelector);

        /**
         * @member {HTMLElement} AppRange#inputFrom
         * @memberof components
         * @desc Элемент поля ввода ползунка "От".
         */
        Object.defineProperty(this, 'inputFrom', {
            enumerable: true,
            value: inputFrom
        });

        const inputTo = el.querySelector(inputToSelector);

        /**
         * @member {HTMLElement} AppRange#inputTo
         * @memberof components
         * @desc Элемент поля ввода ползунка "До".
         */
        Object.defineProperty(this, 'inputTo', {
            enumerable: true,
            value: inputTo
        });

        if (inputFrom && !inputTo) {
            this.options.single = true;
            this.options.connect = 'lower';
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppRange#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let isActive = false;

        /**
         * @member {boolean} AppRange#isActive
         * @memberof components
         * @desc Указывает, находится ли слайдер в данный момент в процессе изменения значений.
         * @readonly
         */
        Object.defineProperty(this, 'isActive', {
            enumerable: true,
            get: () => isActive
        });

        let manualTrigger = false;

        /**
         * @member {boolean} AppRange#manualTrigger
         * @memberof components
         * @desc Указывает, что слайдер был изменен программным путем с помощью событий модуля.
         * @readonly
         */
        Object.defineProperty(this, 'manualTrigger', {
            enumerable: true,
            get: () => manualTrigger
        });

        const slider = el.querySelector(sliderSelector);
        if (!slider.modules) {
            slider.modules = {};
        }
        slider.modules[moduleName] = this;

        /**
         * @member {HTMLElement} AppRange#slider
         * @memberof components
         * @desc Элемент слайдера.
         * @readonly
         */
        Object.defineProperty(this, 'slider', {
            enumerable: true,
            value: slider
        });

        const inputs = el.querySelectorAll(inputSelector);
        if (inputs.length > 0) {
            inputs.forEach(input => {
                if (!input.modules) {
                    input.modules = {};
                }
                input.modules[moduleName] = this;
            });
        }

        /**
         * @member {Array.<HTMLElement>} AppRange#inputs
         * @memberof components
         * @desc Коллекция элементов полей ввода для слайдера.
         * @readonly
         */
        Object.defineProperty(this, 'inputs', {
            enumerable: true,
            value: inputs
        });

        //Опции
        //const rangeStep = 1;

        //Вычисление шага
        //const rangeStepSize = Math.pow(10, String(rangeStep - 1).length);
        //const roundedStep = Math.ceil(rangeStep / rangeStepSize) * rangeStepSize;

        //Опции диапазонов при инициализации
        //obj.options.min = Math.floor(util.convertToFloat(el.options.min) / rangeStepSize * 2) * rangeStepSize / 2;
        //obj.options.max = Math.ceil(util.convertToFloat(el.options.max) / rangeStepSize * 2) * rangeStepSize / 2;

        params.min = util.attempt(() => util.stringToFloat(params.min));
        if (params.min instanceof Error) {
            util.typeError(params.min, 'params.min', 'number');
        }

        params.max = util.attempt(() => util.stringToFloat(params.max));
        if (params.max instanceof Error) {
            util.typeError(params.max, 'params.max', 'number');
        }

        const min = params.min;
        const max = params.max;

        const start = Array.isArray(params.start) ? params.start[0] : params.start;
        const end = Array.isArray(params.start) ? params.start[1] : params.start;

        const currentMin = util.stringToFloat(params.start ? start : inputFrom.value) || min;

        /**
         * @member {number} AppRange#currentMin
         * @memberof components
         * @desc Начальное нижнее значение.
         * @readonly
         */
        Object.defineProperty(this, 'currentMin', {
            enumerable: true,
            value: currentMin
        });

        const currentMax = (!params.single && util.stringToFloat(params.end ? end : inputTo.value)) || max;

        /**
         * @member {number} AppRange#currentMax
         * @memberof components
         * @desc Начальное верхнее значение.
         * @readonly
         */
        Object.defineProperty(this, 'currentMax', {
            enumerable: true,
            value: currentMax
        });

        if (!params.range) {
            params.range = {
                min,
                max
            };
        }
        params.start = params.single ? currentMin : [currentMin, currentMax];

        const format = params.format || {};
        const formatTo = (typeof format.to === 'function') ? format.to : formatFunctions[format.to];
        const formatFrom = (typeof format.from === 'function') ? format.from : formatFunctions[format.from];
        params.format = {
            to: formatTo,
            from: formatFrom
        };

        const formattedMax = params.format.to(max);
        const inputSize = String(formattedMax).length;

        //Настройка маски на полях ввода для ввода только цифровых символов
        let maskFrom;
        let maskTo;

        /**
         * @member {Object} AppRange#maskFrom
         * @memberof components
         * @desc Ссылка на [модуль маски]{@link components.AppMask} поля ввода "От".
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'maskFrom', {
            enumerable: true,
            get: () => maskFrom
        });

        /**
         * @member {Object} AppRange#maskTo
         * @memberof components
         * @desc Ссылка на [модуль маски]{@link components.AppMask} поля ввода "До".
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'maskTo', {
            enumerable: true,
            get: () => maskTo
        });

        if (params.mask) {
            const maskRequire = require('Components/mask');
            maskRequire.default.then(AppMask => {
                const isMaskObject = util.isObject(params.mask);
                let maskOptions = isMaskObject && params.mask;

                if (!isMaskObject) {
                    const negativeRangeSign = (params.min < 0) ? '-|' : ''; //Добавлять знак минуса в маску, если в слайдере можно задавать отрицательные значения
                    const maskOption = 'N'.repeat(inputSize);
                    maskOptions = {
                        definitions: {
                            N: {
                                validator: `[${negativeRangeSign} |(\\d)]`
                            }
                        },
                        mask: maskOption,
                        placeholder: ''
                    };
                }

                inputs.forEach((inputEl, index) => {
                    const newMask = new AppMask(inputEl, maskOptions);

                    if (index === 0) {
                        maskTo = newMask;
                    } else {
                        maskFrom = newMask;
                    }
                });
            });
            maskRequire.maskTrigger(inputs);
        }

        //Инициализация слайдера
        const instance = rangePlugin.create(slider, params);

        /**
         * @member {Object} AppRange#instance
         * @memberof components
         * @desc Ссылка на экземпляр плагина слайдера [noUISlider]{@link libs.nouislider}.
         * @readonly
         */
        Object.defineProperty(this, 'instance', {
            enumerable: true,
            value: instance
        });

        //Методы
        const get = () => {
            return instance.get();
        };

        /**
         * @function AppRange#get
         * @memberof components
         * @desc Возвращает значение слайдера.
         * @returns {(number|string|Array.<(number|string)>)} Возвращает числовое или строковое значение, если у слайдера один ползунок или коллекцию значений, если несколько ползунков.
         * @readonly
         * @example
         * //Если у слайдера несколько ползунков
         * console.log(rangeInstance.get()); //Выведет коллекцию со значениями ползунков, отформатированных согласно опции format.to
         *
         * //Если у слайдера один ползунок
         * console.log(rangeInstance.get()); //Выведет одно значение
         */
        Object.defineProperty(this, 'get', {
            enumerable: true,
            value: get
        });

        this.on({
            /**
             * @event AppRange#set
             * @memberof components
             * @desc Устанавливает значение слайдера.
             * @param {(number|Array.<number>)} values - Новое значение слайдера или коллекцию с новыми значениями слайдера, если у слайдера несколько ползунков.
             * @param {number} [handle] - Индекс изменёного поля ввода.
             * @param {boolean} [pluginTrigger] - Вызвано ли событие через плагин слайдера.
             * @returns {(number|Array.<number>)} Изменеённые значения слайдера. Если значение ползунка не менялось, то элемент коллекции будет null.
             * @example
             * rangeInstance.on('set', values => {
             *     console.log(values); //Выведет коллекцию изменнённых значений слайдера (если несколько ползунков)
             *     if (values[0] !== null) {
             *         console.log('Изменено первое значение слайдера');
             *     }
             *     if (values[1] !== null) {
             *         console.log('Изменено второе значение слайдера');
             *     }
             * });
             * rangeInstance.set([10, 100]); //Установка значений обоих ползунков
             * const newValues = rangeInstance.set([null, 1500]); //Установка значения второго ползунка
             * console.log(newValues); //['10', '1 500'] - отформатированные значения слайдера
             */
            set(values, handle, pluginTrigger) {
                isActive = true;
                if (typeof handle !== 'undefined') {
                    manualTrigger = true;
                }
                if (!pluginTrigger) instance.set(values, handle);
                if (typeof handle === 'undefined') {
                    manualTrigger = true;
                }
                setTimeout(() => {
                    manualTrigger = false;
                }, debounceDuration);
                return get();
            },

            /**
             * @event AppRange#reset
             * @memberof components
             * @desc Сбрасывает значения слайдера до начальных значений при инициализации.
             * @returns {undefined}
             * @example
             * rangeInstance.on('reset', () => {
             *     console.log('Значения слайдера были сброшены');
             * });
             * rangeInstance.reset();
             */
            reset() {
                instance.reset();
                setTimeout(() => {
                    manualTrigger = false;
                }, debounceDuration);
            },

            /**
             * @event AppRange#destroy
             * @memberof components
             * @desc Удаляет экземпляр плагина и разметку, добалвяемую им.
             * @returns {undefined}
             * @example
             * rangeInstance.on('destroy', () => {
             *     console.log('Экземпляр плагина был удалён');
             * });
             * rangeInstance.destroy();
             */
            destroy() {
                slider.innerHTML = '';
                slider.className = slider.className.replace(new RegExp(`(^|\\s)${sliderPrefix}\\S+`, 'g'), '');

                instance.destroy();
            }
        });

        this.onSubscribe([
            /**
             * @event AppRange#start
             * @memberof components
             * @desc Вызывается при вызове события start плагина noUISlider.
             * @returns {undefined}
             * @example
             * rangeInstance.on('start', () => {
             *     console.log('Слайдер начал менять значение');
             * });
             */
            'start',

            /**
             * @event AppRange#slide
             * @memberof components
             * @desc Вызывается при вызове события slide плагина noUISlider.
             * @returns {undefined}
             * @example
             * rangeInstance.on('slide', () => {
             *     console.log('Ползунок слайдера начал двигаться');
             * });
             */
            'slide',

            /**
             * @event AppRange#update
             * @memberof components
             * @desc Вызывается при вызове события update плагина noUISlider.
             * @returns {undefined}
             * @example
             * rangeInstance.on('update', () => {
             *     console.log('Значение слайдера изменяется');
             * });
             */
            'update',

            /**
             * @event AppRange#change
             * @memberof components
             * @desc Вызывается при вызове события change плагина noUISlider.
             * @returns {undefined}
             * @example
             * rangeInstance.on('change', () => {
             *     console.log('Значение слайдера изменяется с помощью ползунка');
             * });
             */
            'change',

            /**
             * @event AppRange#end
             * @memberof components
             * @desc Вызывается при вызове события end плагина noUISlider.
             * @returns {undefined}
             * @example
             * rangeInstance.on('end', () => {
             *     console.log('Значение слайдера изменилось');
             * });
             */
            'end'
        ]);

        //Применять стили при выделении ползунков салйдера только при фокусировке через tab
        el.querySelectorAll(handleClass).forEach(handle => {
            handle.addEventListener('mousedown', event => {
                event.target.style.outline = 'none';
                event.target.addEventListener('blur', blurFunction, {once: true});
            });
        });

        //Привязка событий плагина к событиям модуля
        instance.on('start', () => {
            isActive = true;
            if (AppTouch) AppTouch.setPrevented();
            this.emit('start');
        });

        instance.on('slide', () => {
            this.emit('slide');
        });

        instance.on('update', (values, handle) => {
            if (!manualTrigger && (inputs.length > 0)) {
                inputs[handle].value = values[handle];
                inputs[handle].setAttribute('value', values[handle]);
            }
            this.emit('update');
        });

        instance.on('change', () => {
            this.emit('change');
        });

        const debouncedFunc = util.debounce((values, handle) => {
            isActive = false;
            if (!manualTrigger) {
                this.set(values, handle, true);
            }
        }, debounceDuration);
        instance.on('set', (values, handle) => {
            if (!manualTrigger && (inputs.length > 0)) {
                inputs[handle].value = values[handle];
                inputs[handle].setAttribute('value', values[handle]);
            }
            debouncedFunc(values, handle);
        });

        instance.on('end', util.debounce(() => {
            isActive = false;
            manualTrigger = false;
            if (AppTouch) AppTouch.setUnprevented();
            this.emit('end');
        }, debounceDuration));

        //TODO:test keyboard clicks

        if (inputs.length > 0) {
            inputs.forEach(item => {
                item.size = inputSize;
            });
        }

        //События полей ввода
        if (inputFrom) {
            inputFrom.addEventListener('input', () => {
                const value = inputFrom.value.trim() ? util.stringToFloat(inputFrom.value) : 0;

                this.set(params.range ? [value, null] : value, 0);
            });
            inputFrom.addEventListener('blur', () => {
                const value = inputFrom.value.trim() ? util.stringToFloat(inputFrom.value) : 0;
                const newValue = (value > params.min) ? value : params.min;

                const newValues = this.set(params.range ? [newValue, null] : newValue, 0);
                const newValueString = Array.isArray(newValues) ? newValues[0] : newValues;
                const newFromValue = formatTo(util.stringToFloat(newValueString));
                inputFrom.value = newFromValue;
                inputFrom.setAttribute('value', newFromValue);
            });
        }

        if (inputTo) {
            inputTo.addEventListener('input', () => {
                const value = inputTo.value.trim() ? util.stringToFloat(inputTo.value) : 0;

                this.set([null, value], 1);
            });
            inputTo.addEventListener('blur', () => {
                const value = inputTo.value.trim() ? util.stringToFloat(inputTo.value) : 0;
                const newValue = (value < params.max) ? value : params.max;

                const newValues = this.set([null, newValue], 1);
                const newToValue = formatTo(util.stringToFloat(newValues[1]));
                inputTo.value = newToValue;
                inputTo.setAttribute('value', newToValue);
            });
        }

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppRange);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-range]').forEach(el => new AppRange(el));

export default AppRange;
export {AppRange};