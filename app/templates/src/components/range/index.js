import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let rangeTriggered;

const rangeCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "range" */ './sync.js'))
        .then(modules => {
            chunks.range = true;
            rangeTriggered = true;

            const AppRange = modules.default;
            if (resolve) resolve(AppRange);
        });
};

const initRange = () => {
    if (rangeTriggered) return;

    emitInit('range');
};

const rangeTrigger = (rangeItems, rangeTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (rangeItems instanceof Node) ? [rangeItems] : rangeItems;
    if (items.length === 0) return;

    items.forEach(item => {
        rangeTriggers.forEach(trigger => {
            item.addEventListener(trigger, initRange, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.range) {
        rangeCallback(resolve);
        return;
    }

    onInit('range', () => {
        rangeCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('range');
    });

    const rangeItems = document.querySelectorAll('[data-range]');
    rangeTrigger(rangeItems);
});

export default importFunc;
export {rangeTrigger};