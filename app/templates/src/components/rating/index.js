let ratingTrigger;
let importFunc;

if (__IS_DISABLED_MODULE__) {
    require('./style.scss');

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const ratingCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppRating = modules.default;
                resolve(AppRating);
            });
    };

    ratingTrigger = () => {
        //TODO:rating trigger
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            ratingCallback(resolve);
            return;
        }

        onInit('rating', () => {
            ratingCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('rating');
        });

        //TODO:init on trigger
    });
}

export default importFunc;
export {ratingTrigger};