import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'rating';

//Селекторы и классы
const starSelector = `.${moduleName}-star`;
const inputSelector = `.${moduleName}-input`;
const progressSelector = `.${moduleName}-progress`;
const textSelector = `.${moduleName}-text`;
const staticClass = '-static';
const disabledClass = 'disabled';
const hoverActiveClass = 'hover-active';
const starActiveClass = 'active';
const notEmptyClass = 'not-empty';
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    clear: true
};

//TODO:half rating, min option, options for selectors & classes

/**
 * @class AppRating
 * @memberof components
 * @classdesc Модуль для элементов изменения рейтинга.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент контейнера рейтинга.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.static] - Менять ли значение рейтинга по нажатию на кнопки рейтинга.
 * @param {boolean} [option.clear=true] - Возможность очищения значения рейтинга по нажатию на последнюю активную кнопку.
 * @param {number} [option.value] - Значение рейтинга при инициализации.
 * @param {number} [option.max] - Максимальное значение рейтинга.
 * @param {number} [option.value] - Значение рейтинга при инициализации.
 * @ignore
 * @example
 * const ratingInstance = new app.Rating(document.querySelector('.rating-element'), {
 *     value: 3
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="rating" data-rating>
 *     <div class="rating-list">
 *         <input class="rating-input" type="hidden" value="3">
 *         <span class="rating-item"><button class="rating-star" type="button" title="1"></button></span>
 *         <span class="rating-item"><button class="rating-star" type="button" title="2"></button></span>
 *         <span class="rating-item"><button class="rating-star" type="button" title="3"></button></span>
 *     </div>
 * </div>
 *
 * <!--data-rating - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-rating-options='{"static": false}'></div>
 */
const AppRating = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const ratingOptionsData = el.dataset.ratingOptions;
        if (ratingOptionsData) {
            const dataOptions = util.stringToJSON(ratingOptionsData);
            if (!dataOptions) util.error('incorrect data-rating-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppRating#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const input = el.querySelector(inputSelector);

        if (!input.modules) {
            input.modules = {};
        }
        input.modules[moduleName] = this;

        /**
         * @member {HTMLElement} AppRating#input
         * @memberof components
         * @desc Элемент скрытого поля, хранящего значение выбранного рейтинга.
         * @readonly
         */
        Object.defineProperty(this, 'input', {
            enumerable: true,
            value: input
        });
        if (!input) return;

        const stars = [...el.querySelectorAll(starSelector)];

        stars.forEach(star => {
            if (!star.modules) {
                star.modules = {};
            }
            star.modules[moduleName] = this;
        });

        if (!params.max) {
            params.max = stars.length;
        }

        /**
         * @member {Array.<HTMLElement>} AppRating#stars
         * @memberof components
         * @desc Коллекция элементов кнопок изменения рейтинга.
         * @readonly
         */
        Object.defineProperty(this, 'stars', {
            enumerable: true,
            value: stars
        });

        //Сохранение начального значения рейтинга в поле
        let value = Number.parseInt(params.value, 10);

        /**
         * @member {number} AppRating#value
         * @memberof components
         * @desc Числовое значение выбранного рейтинга.
         * @readonly
         */
        Object.defineProperty(this, 'value', {
            enumerable: true,
            get: () => value
        });

        if (Number.isNaN(value)) {
            value = (input.value === '') ? 0 : (Number.parseInt(input.value, 10) || 0);
        }
        input.value = value;
        input.setAttribute('value', value);

        const starValue = params.max / stars.length;

        /**
         * @member {number} AppRating#starValue
         * @memberof components
         * @desc Числовое значение одной кнопки рейтинга.
         * @readonly
         */
        Object.defineProperty(this, 'starValue', {
            enumerable: true,
            value: starValue
        });

        /**
         * @function AppRating#setValue
         * @memberof components
         * @desc Устанавливает значение рейтинга.
         * @ignore
         */
        const setValue = newValue => {
            const formattedValue = Number.parseInt(newValue, 10);
            value = formattedValue;
            input.value = formattedValue;
            input.setAttribute('value', formattedValue);

            stars.forEach((star, starIndex) => {
                const starsValue = (starIndex + 1) * starValue;
                const isActive = (starsValue === newValue) || (starsValue <= newValue);

                const progress = star.querySelector(progressSelector);
                const text = star.querySelector(textSelector);
                if (isActive) {
                    util.activate(star);
                    star.classList.remove(notEmptyClass);
                    if (progress) {
                        progress.style.width = '100%';
                        if (text) {
                            text.style.width = '100%';
                        }
                    }
                } else if ((starsValue - starValue) > newValue) {
                    util.deactivate(star);
                    star.classList.remove(notEmptyClass);
                    if (progress) {
                        progress.style.width = '0%';
                        if (text) {
                            text.style.width = '100%';
                        }
                    }
                } else {
                    util.deactivate(star);
                    if (progress) {
                        const widthValue = 100 / (starValue / (newValue % starValue));
                        progress.style.width = `${widthValue}%`;
                        if (widthValue > 0) {
                            star.classList.add(notEmptyClass);
                        } else {
                            star.classList.remove(notEmptyClass);
                        }
                        if (text) {
                            text.style.width = `${(100 / widthValue) * 100}%`;
                        }
                    }
                }
            });
        };

        const initialValue = value;

        /**
         * @member {number} AppRating#initialValue
         * @memberof components
         * @desc Начальное значение рейтинга.
         * @readonly
         */
        Object.defineProperty(this, 'initialValue', {
            enumerable: true,
            value: initialValue
        });

        //Инициализация браузерных событий
        stars.forEach((item, index) => {
            const indexValue = index + 1;
            const starsValue = indexValue * starValue;
            const formattedValue = Number.parseInt(starsValue, 10);

            item.addEventListener('mouseenter', () => {
                if (params.static) return;

                stars.slice(0, indexValue).forEach(star => {
                    star.classList.add(hoverActiveClass);
                });

                this.emit('over', formattedValue);
            });
            item.addEventListener('mouseleave', () => {
                if (params.static) return;

                stars.forEach(star => {
                    star.classList.remove(hoverActiveClass);
                });

                this.emit('out', formattedValue);
            });
            item.addEventListener('click', () => {
                if (params.static) return;

                if (params.clear) {
                    if (
                        item.classList.contains(starActiveClass) &&
                        ((index === (stars.length - 1)) || !stars[index + 1].classList.contains(notEmptyClass))
                    ) {
                        this.unset();
                    } else {
                        this.set(starsValue);
                    }
                    return;
                }

                this.set(starsValue);
            });
        });

        const deactivate = () => {
            params.static = true;
            el.classList.add(staticClass);
            util.deactivate(el);
        };

        /**
         * @function AppRating#deactivate
         * @memberof components
         * @desc Деактивирует рейтинг.
         * @readonly
         */
        Object.defineProperty(this, 'deactivate', {
            enumerable: true,
            value: deactivate
        });

        const activate = () => {
            params.static = false;
            el.classList.remove(staticClass);
            util.activate(el);
        };

        /**
         * @function AppRating#activate
         * @memberof components
         * @desc Активирует рейтинг.
         * @readonly
         */
        Object.defineProperty(this, 'activate', {
            enumerable: true,
            value: activate
        });

        const disable = () => {
            util.disable(el);
            util.disable(input);
        };

        /**
         * @function AppRating#disable
         * @memberof components
         * @desc Выключает рейтинг.
         * @readonly
         */
        Object.defineProperty(this, 'disable', {
            enumerable: true,
            value: disable
        });

        const enable = () => {
            util.enable(el);
            util.enable(input);
        };

        /**
         * @function AppRating#enable
         * @memberof components
         * @desc Включает рейтинг.
         * @readonly
         */
        Object.defineProperty(this, 'enable', {
            enumerable: true,
            value: enable
        });

        this.onSubscribe([
            /**
             * @event AppRating#over
             * @memberof components
             * @desc Вызвается при наведениии на кнопку рейтинга.
             * @param {number} overValue - Значение рейтинга на наведённой кнопке.
             * @returns {undefined}
             */
            'over',

            /**
             * @event AppRating#out
             * @memberof components
             * @desc Вызвается при убирании наведения с кнопки рейтинга.
             * @param {number} overValue - Значение рейтинга кнопки, с которой убрали наведение.
             * @returns {undefined}
             */
            'out'
        ]);

        this.on({
            /**
             * @event AppRating#set
             * @memberof components
             * @desc Устанавливает новое значение рейтинга.
             * @param {number} newValue - Новое значение рейтинга.
             * @returns {undefined}
             * @example
             * ratingInstance.on('set', () => {
             *     console.log('Установлено новое значение рейтинга');
             * });
             * console.log(ratingInstance.max); //100
             * console.log(ratingInstance.value); //20
             * ratingInstance.set(40);
             * console.log(ratingInstance.value); //40
             */
            set(newValue) {
                setValue(newValue);
            },

            /**
             * @event AppRating#setStar
             * @memberof components
             * @desc Устанавливает новое значение рейтинга по индексу кнопки.
             * @param {number} newStarValue - Индекс кнопки, согласно которой должно быть установлено значение рейтинга.
             * @returns {undefined}
             * @example
             * ratingInstance.on('setStar', () => {
             *     console.log('Активна новая кнопка в рейтинге');
             * });
             * console.log(ratingInstance.max); //100
             * console.log(ratingInstance.value); //20
             * ratingInstance.setStar(2);
             * console.log(ratingInstance.value); //40
             */
            setStar(newStarValue) {
                setValue(newStarValue * starValue);
            },

            /**
             * @event AppRating#unset
             * @memberof components
             * @desc Очищает значение рейтинга.
             * @returns {undefined}
             * @example
             * ratingInstance.on('unset', () => {
             *     console.log('Рейтинг был сброшен');
             * });
             * console.log(ratingInstance.value); //2
             * ratingInstance.unset();
             * console.log(ratingInstance.value); //0
             */
            unset() {
                setValue(0);
            },

            /**
             * @event AppRating#reset
             * @memberof components
             * @desc Устанавливает начальное значение рейтинга.
             * @returns {undefined}
             * @example
             * ratingInstance.on('reset', () => {
             *     console.log('Установлено начальное значение рейтинга');
             * });
             * console.log(ratingInstance.value); //1
             * ratingInstance.set(2);
             * console.log(ratingInstance.value); //2
             * ratingInstance.reset();
             * console.log(ratingInstance.value); //1
             */
            reset() {
                setValue(initialValue);
            }
        });

        if (params.static || el.classList.contains(staticClass)) {
            deactivate();
        } else {
            activate();
        }

        if (params.disabled || el.classList.contains(disabledClass)) {
            disable();
        } else {
            enable();
        }

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppRating);

//Инициализация элементов по data-атрибуту
document.querySelectorAll('[data-rating]').forEach(el => new AppRating(el));

export default AppRating;
export {AppRating};