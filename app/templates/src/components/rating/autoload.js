if (__IS_DISABLED_MODULE__) {
    if (__IS_SYNC__ || document.querySelector('[data-rating], [data-rating-init]')) {
        require('.');
    }
}