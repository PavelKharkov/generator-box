import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let toggleTriggered;

const toggleCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "toggles" */ './sync.js'))
        .then(modules => {
            chunks.toggles = true;
            toggleTriggered = true;

            const AppToggle = modules.default;
            if (resolve) resolve(AppToggle);
        });
};

const initToggle = event => {
    if (toggleTriggered) return;

    event.preventDefault();
    event.currentTarget.dataset.toggleTriggered = '';

    emitInit('toggle');
};

const toggleTrigger = (toggleItems, toggleTriggers = ['click']) => {
    if (__IS_SYNC__) return;

    const items = (toggleItems instanceof Node) ? [toggleItems] : toggleItems;
    if (items.length === 0) return;

    items.forEach(item => {
        toggleTriggers.forEach(trigger => {
            item.addEventListener(trigger, initToggle, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.toggles) {
        toggleCallback(resolve);
        return;
    }

    onInit('toggle', () => {
        toggleCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('toggle');
    });

    const toggleItems = document.querySelectorAll('[data-toggle]');
    toggleTrigger(toggleItems);
});

export default importFunc;
export {toggleTrigger};