import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'toggle';

//data-атрибуты
const defaultDataToggle = moduleName;
const defaultDataToggleId = `${moduleName}Id`;
const defaultDataToggleSelector = `data-${util.kebabCase(defaultDataToggle)}`;
const defaultDataToggleIdSelector = `data-${util.kebabCase(defaultDataToggleId)}`;

//Классы
const showClass = 'show';
const toggledClass = 'toggled';
const togglingClass = 'toggling';
const activeClass = 'active';
const inactiveClass = 'inactive';
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    trigger: 'click',
    toggleInit: false,
    dataToggle: defaultDataToggle,
    dataToggleId: defaultDataToggleId,
    duration: 0
};

/**
 * @class AppToggle
 * @memberof components
 * @requires components#AppUrl
 * @classdesc Модуль переключения видимости контента.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Переключаемый элемент.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.toggleInit=false] - Активировать ли элемент модуля при инициализации.
 * @param {number} [options.duration=0] - Длительность переключения элемента.
 * @param {HTMLElement} [options.parent] - Родительский контейнер элемента. Если указан, то при переключении элемента будет переключать остальные подобные элементы внутри контейнера.
 * @param {string} [options.dataToggle='toggle'] - Data-атрибут для селектора активирующего элемента.
 * @param {string} [options.dataToggleId='toggleId'] - Data-атрибут для селектора переключаемого элемента.
 * @param {(string|Array.<string>|boolean)} [options.trigger='click'] - Событие для переключения на управляющем элементе или коллекция событий. При false не добавляет соыбтие переключение на элементы усправления.
 * @param {string} [options.target] - Идентификатор переключающегося элемента. По умолчанию берется из href или из data-атрибута, указаного в опции dataToggle.
 * @param {(HTMLElement|Array.<HTMLElement>)} [options.controlElements] - Управляющий элемент или коллекция управляющих элементов.
 * @param {(HTMLElement|Array.<HTMLElement>)} [options.targetElements] - Переключающийся элемент или коллекция переключающихся элементов.
 * @param {boolean} [options.outsideClickHide] - Скрывать ли переключающиеся элементы при нажатии вне элементов экземпляра.
 * @param {boolean} [options.tabs] - Запрещать скрывать активный элемент при включенной опции parent.
 * @example
 * const toggleInstance = new app.Toggle(document.querySelector('.toggle-link'), {
 *     toggleInit: true,
 *     duration: 350,
 *     parent: document.querySelector('.toggle-container')
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <a href="#toggle-target" role="button" aria-expanded="false" aria-controls="toggle-target">Переключатель</a>
 * <!--Можно также использовать атрибут data-toggle для указания целевого элемента-->
 *
 * <div class="toggle" id="toggle-target">Контент</div>
 * <!--Можно также использовать атрибут data-toggle-id для идентификации элемента-->
 *
 * <!--data-toggle - селектор по умолчанию-->
 *
 * @example <caption>Активация с помощью хеша</caption>
 * <!--HTML-->
 * <div id="toggle-target" data-hash-action="toggle">Переключатель</div>
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-toggle-options='{"duration": 350}'></div>
 */
const AppToggle = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const toggleOptionsData = el.dataset.toggleOptions;
        if (toggleOptionsData) {
            const dataOptions = util.stringToJSON(toggleOptionsData);
            if (!dataOptions) util.error('incorrect data-toggle-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppToggle#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const stringDataToggle = String(params.dataToggle);
        const stringDataToggleId = String(params.dataToggleId);

        const dataToggle = util.camelCase(stringDataToggle);
        const dataToggleId = util.camelCase(stringDataToggleId);
        const dataToggleSelector = `data-${util.kebabCase(stringDataToggle)}`;
        const dataToggleIdSelector = `data-${util.kebabCase(stringDataToggleId)}`;

        const target = params.target || (() => {
            const href = el.getAttribute('href');
            const datasetToggle = el.dataset[dataToggle];
            if (href || (typeof datasetToggle !== 'undefined')) {
                const hasEmptyDataToggle = (datasetToggle === '') || (datasetToggle === 'true');
                const hasIdHrefAttr = href && href.startsWith('#');
                const targetString = (hasEmptyDataToggle && hasIdHrefAttr)
                    ? href
                    : (datasetToggle || href);

                return targetString.slice(1);
            }

            return el.hasAttribute(dataToggleIdSelector) ? el.getAttribute(dataToggleIdSelector) : el.id;
        })();

        /**
         * @member {string} AppToggle#target
         * @memberof components
         * @desc Идентификатор переключающегося элемента.
         * @readonly
         */
        Object.defineProperty(this, 'target', {
            enumerable: true,
            value: target
        });

        let controlElements;
        if (params.controlElements) {
            params.controlElements = Array.isArray(params.controlElements)
                ? params.controlElements
                : [params.controlElements];
        }
        if (params.controlElements) {
            controlElements = params.controlElements;
        } else {
            const controlElementsElements = util.attempt(() => {
                return document.querySelectorAll(`[${dataToggleSelector}][href="#${target}"], [${dataToggleSelector}="#${target}"]`);
            });
            controlElements = (controlElementsElements instanceof Error) ? [] : [...controlElementsElements];
        }
        //добавлять проинициализированные активирующие элементы даже без атрибута data-toggle
        if (Array.isArray(controlElements) && el.hasAttribute('href') && (typeof el.dataset[dataToggle] === 'undefined')) {
            controlElements.unshift(el);
        }

        /**
         * @member {Array.<HTMLElement>} AppToggle#controlElements
         * @memberof components
         * @desc Коллекция активирующих элементов.
         * @readonly
         */
        Object.defineProperty(this, 'controlElements', {
            enumerable: true,
            value: controlElements
        });

        let targetElements;
        if (params.targetElements) {
            params.targetElements = Array.isArray(params.targetElements) ? params.targetElements : [params.targetElements];
            if (controlElements.length === 0) { //если targetElements передаётся через опции
                controlElements.unshift(el);
            }
        }
        if (params.targetElements) {
            targetElements = params.targetElements;
        } else {
            const targetElementsElements = util.attempt(() => {
                return document.querySelectorAll(`#${target}, [${dataToggleIdSelector}="${target}"]`);
            });
            targetElements = (targetElementsElements instanceof Error) ? [] : [...targetElementsElements];
        }
        if (targetElements.length === 0) {
            util.error('can\'t find any target elements for toggling');
        }

        const toggleElements = [...controlElements, ...targetElements];
        toggleElements.forEach(element => { //Добавить ссылку на модуль toggle всем участвующим элементам и проверить, то что в свойствах controlElements и targetElements только HTML-элементы
            if (!(element instanceof HTMLElement)) {
                util.error({
                    message: 'toggle elements are set incorrectly',
                    element: toggleElements
                });
            }

            if (!element.modules) {
                element.modules = {};
            }
            if (!element.modules[moduleName]) {
                element.modules[moduleName] = this;
            }
        });

        /**
         * @member {Array.<HTMLElement>} AppToggle#targetElements
         * @memberof components
         * @desc Коллекция переключающихся элементов.
         * @readonly
         */
        Object.defineProperty(this, 'targetElements', {
            enumerable: true,
            value: targetElements
        });

        let isShown = targetElements.some(element => {
            return element.classList.contains(showClass);
        });

        /**
         * @member {boolean} AppToggle#isShown
         * @memberof components
         * @desc Указывает, показан ли элемент.
         * @readonly
         * @example
         * const toggleInstance = new app.Toggle(document.querySelector('.toggle-link'));
         * console.log(toggleInstance.isShown); //false
         * toggleInstance.show();
         * console.log(toggleInstance.isShown); //true
         */
        Object.defineProperty(this, 'isShown', {
            enumerable: true,
            get: () => isShown
        });

        let toggleTimeout;

        /**
         * @function hideSiblings
         * @desc Скрывает переключающиеся элементы внутри родительского контейнера.
         * @param {HTMLElement} parentEl - Родительский элемент группы переключающихся блоков.
         * @returns {undefined}
         * @ignore
         */
        const hideSiblings = parentEl => {
            //TODO:save siblings in prop
            const parentActiveTargets = [...parentEl.children].filter(element => {
                return element.matches(`.${toggledClass}.${showClass}`) || element.matches(`.${togglingClass}`);
            });
            const hideOptions = {};
            if (params.tabs) {
                hideOptions.duration = 0;
            }

            parentActiveTargets.forEach(element => {
                const hasAttrs = element.id && element.dataset[dataToggleId];
                const noTarget = (element.id !== target) && (element.dataset[dataToggleId] !== target);
                if (!hasAttrs || noTarget) {
                    element.modules[moduleName].hide(hideOptions);
                }
            });
        };

        //Инициализация браузерных событий
        if (params.trigger) {
            const triggers = Array.isArray(params.trigger) ? params.trigger : [params.trigger];
            triggers.forEach((trigger, triggerIndex) => {
                controlElements.forEach(link => {
                    link.addEventListener(trigger, event => {
                        event.preventDefault();

                        const options = params;
                        if (options.parent && options.tabs && isShown) {
                            return;
                        }

                        if (triggers.length === 1) {
                            const isActive = link.classList.contains(activeClass);

                            if (isActive) {
                                this.hide(options);
                            } else {
                                this.show(options);
                            }
                        } else if (triggerIndex === 0) {
                            this.show(options);
                        } else {
                            this.hide(options);
                        }
                    });
                });
            });
        }

        let hidePrevented;

        /**
         * @member {boolean} AppToggle#hidePrevented
         * @memberof components
         * @desc Указывает, предотвращать ли закрытие контента.
         * @readonly
         */
        Object.defineProperty(this, 'hidePrevented', {
            enumerable: true,
            get: () => hidePrevented
        });

        /**
         * @function AppToggle#preventHide
         * @memberof components
         * @desc Включает предотвращение закрытие контента.
         * @readonly
         */
        Object.defineProperty(this, 'preventHide', {
            enumerable: true,
            value() {
                hidePrevented = true;
            }
        });

        /**
         * @function AppToggle#unpreventHide
         * @memberof components
         * @desc Выключает предотвращение закрытие контента.
         * @readonly
         */
        Object.defineProperty(this, 'unpreventHide', {
            enumerable: true,
            value() {
                hidePrevented = false;
            }
        });

        //Событие нажатия вне элементов модуля
        if (params.outsideClickHide) {
            document.addEventListener('click', event => {
                if (!isShown) return;

                const isClickedInside = toggleElements.find(element => {
                    return element.contains(event.target);
                });
                if (!isClickedInside) this.hide();
            });
        }

        /**
         * @function showControlsFunction
         * @desc Изменение атрибутов управляющих элементов при показе контентного блока.
         * @returns {undefined}
         * @ignore
         */
        const showControlsFunction = () => {
            controlElements.forEach(element => {
                element.classList.remove(inactiveClass);
                util.activate(element);
                element.ariaExpanded = true;
            });
        };

        if (isShown) {
            showControlsFunction();
        }

        this.on({
            /**
             * @event AppToggle#show
             * @memberof components
             * @desc Показывает элемент экземпляра.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации появления элемента.
             * @param {HTMLElement} [options.parent=<значение из опций экземпляра>] - Родительский контейнер элемента. Если указан, то переключает внутри контейнера остальные элементы с таким же идентификатором, как и у элемента.
             * @fires components.AppToggle#shown
             * @returns {undefined}
             * @example
             * toggleInstance.on('show', () => {
             *     console.log('Элемент показывается');
             * });
             * toggleInstance.show({
             *     duration: 0
             * });
             */
            show(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                const currentTargetElements = targetElements.filter(element => {
                    return !element.classList.contains(showClass);
                });
                if (currentTargetElements.length === 0) return; //показывать только скрытые элементы

                if (options.parent && (options.parent instanceof HTMLElement)) {
                    hideSiblings(options.parent);
                }

                util.animationDefer(() => {
                    showControlsFunction();

                    currentTargetElements.forEach(element => {
                        element.classList.add(togglingClass);
                        element.style.transitionDuration = `${options.duration}ms`;
                    });

                    clearTimeout(toggleTimeout);
                    toggleTimeout = setTimeout(() => {
                        currentTargetElements.forEach(element => {
                            element.classList.remove(togglingClass);
                            element.classList.add(showClass);
                        });
                        if (util.isChromeBrowser) { //preventScroll пока что работает только в Хроме
                            currentTargetElements[0].focus({
                                preventScroll: true
                            });
                        }
                        this.emit('shown');
                        isShown = true;
                    }, options.duration);
                });
            },

            /**
             * @event AppToggle#hide
             * @memberof components
             * @desc Скрывает элемент экземпляра.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации скрытия элемента.
             * @fires components.AppToggle#hidden
             * @returns {undefined}
             * @example
             * toggleInstance.on('hide', () => {
             *     console.log('Элемент скрывается');
             * });
             * toggleInstance.hide();
             */
            hide(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                if (hidePrevented) return;

                util.defaultsDeep(options, params);

                if (!Number.isFinite(options.duration)) {
                    util.typeError(options.duration, 'options.duration', 'number');
                }

                const currentTargetElements = targetElements.filter(element => {
                    return element.classList.contains(showClass) || element.classList.contains(togglingClass);
                });
                if (currentTargetElements.length === 0) return; //скрывать только видимые элементы

                controlElements.forEach(element => {
                    util.deactivate(element);
                    element.ariaExpanded = false;
                    element.classList.add(inactiveClass);
                });

                currentTargetElements.forEach(element => {
                    element.style.transitionDuration = `${options.duration}ms`;
                    element.classList.add(togglingClass);
                    util.animationDefer(() => {
                        element.classList.remove(showClass);
                    });
                });

                clearTimeout(toggleTimeout);
                toggleTimeout = setTimeout(() => {
                    currentTargetElements.forEach(element => {
                        element.classList.remove(togglingClass);
                    });
                    currentTargetElements[0].blur();
                    this.emit('hidden');
                    isShown = false;
                }, options.duration);
            },

            /**
             * @event AppToggle#toggle
             * @memberof components
             * @desc Показывает или скрывает элемент экземпляра.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.duration=<значение из опций экземпляра>] - Длительность анимации переключения элемента.
             * @param {HTMLElement} [options.parent=<значение из опций экземпляра>] - Родительский контейнер элемента. Если указан, то при показе переключит остальные подобные элементы внутри контейнера.
             * @fires components.AppToggle#show
             * @fires components.AppToggle#hide
             * @returns {undefined}
             * @example
             * toggleInstance.on('toggle', () => {
             *     console.log('Элемент переключается');
             * });
             * toggleInstance.toggle();
             */
            toggle(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, params);

                const isActive = targetElements.some(element => {
                    return element.classList.contains(showClass) || element.classList.contains(togglingClass);
                });
                if (isActive) {
                    this.hide(options);
                } else {
                    this.show(options);
                }
            }
        });

        this.onSubscribe([
            /**
             * @event AppToggle#shown
             * @memberof components
             * @desc Вызывается, когда элемент полностью показан.
             * @returns {undefined}
             * @example
             * toggleInstance.on('shown', () => {
             *     console.log('элемент показан');
             * });
             */
            'shown',

            /**
             * @event AppToggle#hidden
             * @memberof components
             * @desc Вызывается, когда элемент полностью скрыт.
             * @returns {undefined}
             * @example
             * toggleInstance.on('hidden', () => {
             *     console.log('элемент скрыт');
             * });
             */
            'hidden'
        ]);

        toggleElements.forEach(element => {
            element.classList.add(initializedClass);
        });

        if (params.toggleInit) this.toggle();

        if (typeof el.dataset.toggleTriggered !== 'undefined') {
            delete el.dataset.toggleTriggered;
            this.toggle();
        }
    }
});

//Настройка возможности управления с помощью адресной строки
require('Components/url').default.then(AppUrl => {
    AppUrl.registerAction({
        id: moduleName,
        selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
        action: `${moduleName}.toggle`,
        idSelectors: ['id', `${defaultDataToggleIdSelector}`]
    });
});
if (window.location.hash) emitInit('url');

addModule(moduleName, AppToggle);

//Инициализация элементов по data-атрибуту
document.querySelectorAll(`[${defaultDataToggleSelector}]`).forEach(el => new AppToggle(el));

export default AppToggle;
export {AppToggle};