import {ScrollMagic as ScrollPlugin, addIndicators} from 'Libs/scrollmagic';

import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'scroll';

const defaultControllerOptions = {
    loglevel: 2
};
if (addIndicators) {
    defaultControllerOptions.addIndicators = false;
}
const controller = new ScrollPlugin.Controller(defaultControllerOptions); //Контроллер основной прокрутки страницы

//Классы
const activeClass = `${moduleName}-active`;
const initializedClass = `${moduleName}-initialized`;

//Опции сцены по умолчанию
const defaultOptions = {
    reverse: false,
    triggerHook: 0.75
};

//TODO:html example

/**
 * @class AppScroll
 * @memberof components
 * @requires libs.scrollmagic
 * @classdesc Модуль для работы с прокруткой.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент активирующего контейнера.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {boolean} [options.reverse=false] - Активировать ли события при прокрутке назад.
 * @param {number} [options.triggerHook=0.75] - При какой позиции относительно экрана активировать события при прокрутке.
 * @param {HTMLElement} [options.triggerElement] - Элемент, который обозначает начало сцены.
 * @param {*} [options...] - Другие опции плагина [ScrollMagic]{@link libs.scrollmagic}.
 * @ignore
 * @example
 * const scrollInstance = new app.Scroll(document.querySelector('.scroll-element'), {
 *     triggerHook: 0.5
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-scroll-track></div>
 *
 * <!--data-scroll-track - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-scroll-options='{"reverse": true}'></div>
 */
const AppScroll = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const scrollOptionsData = el.dataset.scrollOptions;
        if (scrollOptionsData) {
            const dataOptions = util.stringToJSON(scrollOptionsData);
            if (!dataOptions) util.error('incorrect data-scroll-options format');
            util.extend(this.options, dataOptions);
        }

        this.options.triggerElement = (typeof this.options.triggerElement === 'undefined') ? el : this.options.triggerElement;
        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppScroll#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const scene = new ScrollPlugin.Scene(params);

        /**
         * @member {Object} AppScroll#scene
         * @memberof components
         * @desc Экземпляр контейнера-сцены плагина прокрутки.
         * @readonly
         */
        Object.defineProperty(this, 'scene', {
            enumerable: true,
            value: scene
        });

        //Методы
        /**
         * @function AppScroll#setPin
         * @memberof components
         * @desc Закрепляет элемент за позицией прокрутки.
         * @param {HTMLElement} [element] - Элемент, который надо закрепить.
         * @returns {Object} Возвращает сцену экземпляра.
         * @readonly
         * @example
         * scrollInstance.setPin(document.querySelector('.pin'));
         */
        Object.defineProperty(this, 'setPin', {
            enumerable: true,
            value(element = util.required('element')) {
                return scene.setPin(element);
            }
        });

        /**
         * @function AppScroll#addTo
         * @memberof components
         * @desc Добавляет сцену к контроллеру.
         * @param {Object} [currentController] - Контроллер, к которому нужно добавить сцену. По умолчанию используется заранее проинициализированный контроллер.
         * @returns {Object} Возвращает сцену экземпляра.
         * @readonly
         * @example
         * scrollInstance.addTo(); //Контроллер указывать не надо
         */
        Object.defineProperty(this, 'addTo', {
            enumerable: true,
            value(currentController = controller) {
                return scene.addTo(currentController);
            }
        });

        /**
         * @function AppScroll#setActive
         * @memberof components
         * @desc Добавляет класс элементу сцены при её активации при прокрутке.
         * @param {string} [addedClass='scroll-active'] - Добавляемый класс.
         * @returns {Object} Возвращает сцену экземпляра.
         * @readonly
         * @example
         * scrollInstance.setActive(); //Добавит элементу экземпляра класс scroll-active
         */
        Object.defineProperty(this, 'setActive', {
            enumerable: true,
            value(addedClass = activeClass) {
                return this.constructor.setActive(scene, addedClass);
            }
        });

        params.triggerElement.classList.add(initializedClass);
    }

    /**
     * @function AppScroll.setActive
     * @memberof components
     * @desc Добавляет класс элементу сцены или сценам при её активации при прокрутке.
     * @param {(Array.<Object>|Object)} [scenes] - Коллекция сцен или сцена, к которой нужно добавлять класс.
     * @param {string} [addedClass='scroll-active'] - Добавляемый класс.
     * @param {number} [interval=0] - Интервал, через который надо запускать действия сцен, если они были активированы одновременно.
     * @returns {(Array.<Object>|Object)} Возвращает коллекцию сцен или сцену к которым применялся данный метод.
     * @example
     * const scrollInstances = [scrollInstance1, scrollInstance2];
     * app.Scroll.setActive(scrollInstances, 'test-class', 50); //Последовательно добавит класс элементам с интервалом 50 мс
     */
    static setActive(scenes = util.required('scenes'), addedClass = activeClass, interval = 0) {
        //TODO:check scenes instanceof AppScroll
        if (!scenes) return null;

        if (Array.isArray(scenes)) {
            let currentInterval = 0;

            return scenes.forEach(scene => {
                const currentSceneInterval = currentInterval;
                const currentScene = scene.on('enter', () => {
                    setTimeout(() => {
                        scene.triggerElement().classList.add(addedClass);
                    }, currentSceneInterval);
                });
                currentInterval += interval;
                return currentScene;
            });
        }

        return scenes.setClassToggle(scenes.triggerElement(), addedClass);
    }

    /**
     * @function AppScroll.controller
     * @memberof components
     * @desc Get-функция. Содержит ссылку на основной контроллер сцен плагина ScrollMagic.
     * @returns {Object}
     */
    static get controller() {
        return controller;
    }
});

addModule(moduleName, AppScroll);

//Инициализация элементов по data-атрибуту. Настройки сцены можно передавать через атрибут data-scroll-track
document.querySelectorAll('[data-scroll-track]').forEach(el => {
    const scene = new AppScroll(el);
    scene.setActive();
    scene.addTo();
});

export default AppScroll;
export {AppScroll};