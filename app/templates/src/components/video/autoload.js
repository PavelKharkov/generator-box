if (__IS_DISABLED_MODULE__) {
    if (__IS_SYNC__ || document.querySelector('[data-lazyload="video"], [data-video-init]')) {
        require('.');
    }
}