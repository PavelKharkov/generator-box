let importFunc;

if (__IS_DISABLED_MODULE__) {
    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const videoCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppVideo = modules.default;
                resolve(AppVideo);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            videoCallback(resolve);
            return;
        }

        onInit('video', () => {
            videoCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('video');
        });
    });
}

export default importFunc;