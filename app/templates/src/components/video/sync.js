import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'video';

const tagName = 'VIDEO';

const lazyloadedClass = 'lazyloaded';
const initializedClass = `${moduleName}-initalized`;

const attrOptions = [
    'src',
    'width',
    'height',
    'autoplay',
    'buffered',
    'controls',
    'crossorigin',
    'loop',
    'muted',
    'playsinline',
    'preload',
    'poster'
];

//Опции по умолчанию
const defaultOptions = {
    srcAttr: 'data-src',
    widthAttr: 'data-width',
    heightAttr: 'data-height',
    autoplayAttr: 'data-autoplay',
    bufferedAttr: 'data-buffered',
    controlsAttr: 'data-controls',
    crossoriginAttr: 'data-crossorigin',
    loopAttr: 'data-loop',
    mutedAttr: 'data-muted',
    playsinlineAttr: 'data-playsinline',
    preloadAttr: 'data-preload',
    posterAttr: 'data-poster',
    sourceSrcAttr: 'data-src',
    sourceTypeAttr: 'data-type'
};

/**
 * @class AppVideo
 * @memberof components
 * @classdesc Модуль для видео.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - Элемент видео.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.srcAttr='data-src'] - Атрибут, с которого будет браться значение атрибута src видео при отложенной загрузке.
 * @param {string} [options.widthAttr='data-width'] - Атрибут, с которого будет браться значение атрибута width видео при отложенной загрузке.
 * @param {string} [options.heightAttr='data-height'] - Атрибут, с которого будет браться значение атрибута height видео при отложенной загрузке.
 * @param {string} [options.autoplayAttr='data-autoplay'] - Атрибут, с которого будет браться значение атрибута autoplay видео при отложенной загрузке.
 * @param {string} [options.bufferedAttr='data-buffered'] - Атрибут, с которого будет браться значение атрибута buffered видео при отложенной загрузке.
 * @param {string} [options.controlsAttr='data-controls'] - Атрибут, с которого будет браться значение атрибута controls видео при отложенной загрузке.
 * @param {string} [options.crossoriginAttr='data-crossorigin'] - Атрибут, с которого будет браться значение атрибута crossorigin видео при отложенной загрузке.
 * @param {string} [options.loopAttr='data-loop'] - Атрибут, с которого будет браться значение атрибута loop видео при отложенной загрузке.
 * @param {string} [options.mutedAttr='data-muted'] - Атрибут, с которого будет браться значение атрибута muted видео при отложенной загрузке.
 * @param {string} [options.playsinlineAttr='data-playsinline'] - Атрибут, с которого будет браться значение атрибута playsinline видео при отложенной загрузке.
 * @param {string} [options.preloadAttr='data-preload'] - Атрибут, с которого будет браться значение атрибута preload видео при отложенной загрузке.
 * @param {string} [options.posterAttr='data-poster'] - Атрибут, с которого будет браться значение атрибута poster видео при отложенной загрузке.
 * @param {string} [options.sourceSrcAttr='data-src'] - Атрибут, с которого будет браться значение атрибута src тега source при отложенной загрузке.
 * @param {string} [options.sourceTypeAttr='data-type'] - Атрибут, с которого будет браться значение атрибута type тега source при отложенной загрузке.
 * @ignore
 * @example
 * const videoInstance = new app.Video(document.querySelector('.video-element'), {
 *     srcAttr: 'data-custom-src'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-lazyload="video" data-width="500" data-autoplay="">
 *     <div data-src="#" data-type="video/mp4"></div>
 *     <div data-src="#" data-type="video/webm"></div>
 * </div>
 *
 * <!--data-lazyload="video" - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-video-options='{"srcAttr": "data-custom-src"}'></div>
 */
const AppVideo = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const videoOptionsData = el.dataset.videoOptions;
        if (videoOptionsData) {
            const dataOptions = util.stringToJSON(videoOptionsData);
            if (!dataOptions) util.error('incorrect data-video-options format');
            util.extend(this.options, dataOptions);
        }

        util.defaultsDeep(this.options, defaultOptions);

        /**
         * @member {Object} AppVideo#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let videoEl = el;

        /**
         * @member {HTMLElement} AppVideo#videoEl
         * @memberof components
         * @desc Элемент тега video.
         * @readonly
         */
        Object.defineProperty(this, 'videoEl', {
            enumerable: true,
            get: () => videoEl
        });

        /**
         * @function initEvents
         * @desc Инициализирует браузерные события тега video.
         * @returns {undefined}
         * @ignore
         */
        const initEvents = () => {
            videoEl.addEventListener('canplay', () => {
                //console.log('video can start', videoEl);
                this.emit('canplay');
            });

            videoEl.addEventListener('error', () => {
                util.error({
                    message: 'error loading video',
                    element: videoEl
                });
            });

            if (videoEl.readyState === videoEl.HAVE_ENOUGH_DATA) {
                util.defer(() => {
                    //console.log('enough data is available', videoEl);
                    this.emit('canplay');
                });
            }
        };

        //TODO:add readyState & networkState, other video props

        this.on({
            /**
             * @event AppVideo#lazyload
             * @memberof components
             * @desc Событие, вызываемое при отложенной загрузке видео.
             * @returns {undefined}
             * @example
             * videoInstance.on('lazyload', () => {
             *     console.log('Видео было проинициализировано с помощью отложенной загрузки');
             * });
             * videoInstance.lazyload();
             */
            lazyload() {
                if (el.tagName === tagName) return;

                const attrs = {};
                attrOptions.forEach(attr => {
                    const attrValue = el.getAttribute(params[`${attr}Attr`]);
                    if (attrValue === null) return;
                    el.removeAttribute(params[`${attr}Attr`]);
                    attrs[attr] = attrValue;
                });

                const newEl = document.createElement('video');

                [...el.attributes].forEach(attr => {
                    newEl.setAttribute(attr.name, attr.value);
                });
                Object.keys(attrs).forEach(attr => {
                    if (attr === null) return;
                    newEl.setAttribute(attr, (/src|poster/).test(attr) ? encodeURI(attrs[attr]) : attrs[attr]);
                });

                [...el.children].forEach(source => {
                    const sourceSrcAttr = source.getAttribute(params.sourceSrcAttr);
                    const sourceTypeAttr = source.getAttribute(params.sourceTypeAttr);
                    if (!(sourceSrcAttr && sourceTypeAttr)) return;

                    const newSourceEl = document.createElement('source');

                    source.removeAttribute(params.sourceSrcAttr);
                    source.removeAttribute(params.sourceTypeAttr);

                    [...source.attributes].forEach(attr => {
                        newSourceEl.setAttribute(attr.name, attr.value);
                    });

                    newSourceEl.src = encodeURI(sourceSrcAttr);
                    newSourceEl.type = sourceTypeAttr;

                    newEl.append(newSourceEl);
                });

                delete newEl.dataset.lazyload;
                newEl.classList.remove(lazyloadedClass);

                el.parentNode.replaceChild(newEl, el);
                videoEl = newEl;
                newEl.modules = {
                    [moduleName]: this
                };

                if (attrs.autoplay !== null) {
                    const playPromise = newEl.play();
                    if (typeof playPromise !== 'undefined') {
                        playPromise.catch(() => {
                            newEl.muted = true;
                            newEl.setAttribute('muted', '');
                            newEl.play();
                        });
                    }
                }

                initEvents();
            }
        });

        this.onSubscribe({
            /**
             * @event AppVideo#canplay
             * @memberof components
             * @desc Событие, вызываемое при начале загрузке достаточного количества кадров, чтобы начать проигрывание видео.
             * @returns {undefined}
             * @example
             * videoInstance.on('canplay', () => {
             *     console.log('Видео может начать проигрываться');
             * });
             */
            canplay() {
                videoEl.classList.add(initializedClass);
            }
        });

        if (videoEl.tagName === tagName) {
            initEvents();
        }

        if (!el.classList.contains(lazyloadedClass)) {
            this.lazyload();
        }
    }
});

addModule(moduleName, AppVideo);

//Инициализация элементов по data-атрибуту
document.querySelectorAll(`[data-lazyload="${moduleName}"]`).forEach(el => new AppVideo(el));

export default AppVideo;
export {AppVideo};