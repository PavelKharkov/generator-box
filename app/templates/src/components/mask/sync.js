import Inputmask from 'Libs/inputmask';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'mask';

const maskPlugin = Inputmask;

//Классы
const initializedClass = `${moduleName}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    onBeforeMask: String,
    showMaskOnHover: false,
    init: true
};

//Маски для ввода телефонов
const phoneSymbol = 'P';

//Простая маска для телефона (позволяет только писать символы, используемые в телефонных номерах, без местоположения символов)
const freePhoneOptions = {
    inputmode: 'tel',
    regex: '[\\d ()+-]*'
};

//Маска для ввода российского телефона
const rusCodeInvalidSymbols = '12567';

const beforePasteFunction = value => {
    if (!value) return value;
    let resultNum = value.replace(/[^\d+]/g, '');

    if ((resultNum.length === 11) && (resultNum.indexOf('8') === 0)) { //При копировании телефона, начинающегося с 8
        resultNum = resultNum.slice(1);
    } else if ((resultNum.length === 12) && resultNum.indexOf('+7') === 0) { //При копировании телефона, начинающегося с +7
        resultNum = resultNum.slice(2);
    }

    if ((resultNum.indexOf('8') === 0) && (resultNum.indexOf('9') === 1)) {
        resultNum = resultNum.slice(1);
    }

    return resultNum;
};

const rusPhoneOptions = {
    mask: `+7 (${phoneSymbol}99) 999 99 99`,
    inputmode: 'tel',
    definitions: {
        [phoneSymbol]: {
            validator: `(?![${rusCodeInvalidSymbols}])[\\d]`
        }
    },

    onBeforeMask(value) {
        return beforePasteFunction(value);
    },
    onBeforePaste(pastedValue) {
        return beforePasteFunction(pastedValue);
    },

    onKeyDown(event) {
        util.defer(() => {
            if (!(event.target && event.target.value)) return;

            const parsedNum = event.target.value.replace(/[^\d+]/g, '');
            if ((parsedNum.indexOf('8') === 2) && (parsedNum.indexOf('9') === 3)) {
                event.target.value = parsedNum.slice(0, 2) + parsedNum.slice(3);
            }
        });
    }
};

//Маска для ввода целых цисел
const numberOptions = {
    inputmode: 'numeric',
    regex: '[\\d]*'
};

//Маска для ввода дробных цисел
const floatNumberOptions = {
    inputmode: 'decimal',
    regex: '[\\d.,]*'
};

//Маска для ввода цен
const priceOptions = {
    inputmode: 'decimal',
    regex: '[\\d., ]*'
};

//TODO:mask for fio (translate)
//regex: '/[^а-яё\- ]/ig'

const maskInit = AppDate => {
    //Маска для даты
    const dateOptions = {
        mask: AppDate.datePattern,
        definitions: {
            [AppDate.definitions.day.charAt(0)]: {
                validator: '[\\d]'
            },
            [AppDate.definitions.month.charAt(0)]: {
                validator: '[\\d]'
            },
            [AppDate.definitions.year.charAt(0)]: {
                validator: '[\\d]'
            }
        }

        //TODO:validate dates
    };

    /**
     * @class AppMask
     * @memberof components
     * @requires libs.inputmask
     * @classdesc Модуль маски для полей ввода.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {HTMLElement} element - Элемент для маскирования.
     * @param {app.Module.instanceOptions} options - Опции экземпляра.
     * @param {string} [options.preset] - Название шаблона настроек маски, берётся из конструктора класса.
     * @param {string} [options.mask] - Строка с маской.
     * @param {string} [options.regex] - Регулярное выражение для символов маски.
     * @param {Function} [options.onBeforeMask] - Функция, вызываемая перед применением маски. По умолчанию конвертирует значение в строку.
     * @param {boolean} [options.showMaskOnHover=false] - Показывать ли маску при наведении на элемент.
     * @param {boolean} [options.init=true] - Применять маску сразу же после инициализации экземпляра модуля.
     * @param {boolean} [options.unmaskedCopy] - Исключать разделяющие символы при копировании значения поля.
     * @param {*} [options...] - Другие опции плагина [Inputmask]{@link libs.inputmask}.
     * @example
     * const maskInstance = new app.Mask(document.querySelector('.masked-input'), {
     *     mask: '+7 (999) 999-99-99'
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <input type="text" data-mask="+7 (999) 999-99-99">
     *
     * <!--data-mask - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <input type="text" data-mask-options='{"regex": "[\\d]*"}'>
     */
    const AppMask = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            const isHTMLElement = el instanceof HTMLElement;

            if (isHTMLElement) {
                const maskOptionsData = el.dataset.maskOptions;
                if (maskOptionsData) {
                    const dataOptions = util.stringToJSON(maskOptionsData);
                    if (!dataOptions) util.error('incorrect data-mask-options format');
                    util.extend(this.options, dataOptions);
                }
            }

            if (this.options.preset) {
                const preset = this.constructor[this.options.preset];
                if (!preset) util.error(`mask preset '${this.options.preset}' not found`);
                util.defaultsDeep(this.options, preset);
            }

            const maskString = this.options.mask || el.dataset.mask;
            if (typeof maskString === 'string') {
                this.options.mask = maskString;
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppMask#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            let value = el.value;

            /**
             * @member {string} AppMask#value
             * @memberof components
             * @desc Текущее значение поля.
             * @readonly
             */
            Object.defineProperty(this, 'value', {
                enumerable: true,
                get: () => value
            });

            el.addEventListener('input', () => {
                value = el.value;
            });

            let instance;

            /**
             * @member {Object} AppMask#instance
             * @memberof components
             * @desc Проинициализированный экземпляр плагина для маски [Inputmask]{@link libs.inputmask}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'instance', {
                enumerable: true,
                get: () => instance
            });

            this.on({
                /**
                 * @event AppMask#remove
                 * @memberof components
                 * @desc Удаляет маску с элемента.
                 * @returns {undefined}
                 * @example
                 * maskInstance.on('remove'), () => {
                 *     console.log('Маска была удалена с элемента');
                 * });
                 * maskInstance.remove();
                 */
                remove() {
                    maskPlugin.remove(el);
                },

                /**
                 * @event AppMask#mask
                 * @memberof components
                 * @desc Применяет маску к элементу.
                 * @returns {undefined}
                 * @example
                 * maskInstance.on('mask', () => {
                 *     console.log('Маска была применена к элементу');
                 * });
                 *
                 * console.log(maskInstance.instance); //undefined
                 * maskInstance.mask();
                 * console.log(maskInstance.instance); //Вернёт объект с проинициализированным плагином
                 */
                mask() {
                    instance = maskPlugin(params);
                    instance.mask(el);
                }
            });

            const getUnmasked = allCharacters => {
                const unmaskedValue = allCharacters
                    ? instance.unmaskedvalue(value)
                    : value.replace(/[\s().-]/g, '');
                return unmaskedValue;
            };

            /**
             * @function AppMask#getUnmasked
             * @memberof components
             * @desc Возвращает чистое введённое значение. По умолчанию, оставляет неразделяющие символы.
             * @param {boolean} allCharacters - Возвращать чистое введённое значение без всех символов в маске.
             * @readonly
             */
            Object.defineProperty(this, 'getUnmasked', {
                enumerable: true,
                get: () => getUnmasked
            });

            if (params.unmaskedCopy) {
                el.addEventListener('copy', event => {
                    event.preventDefault();
                    event.clipboardData.setData('text/plain', getUnmasked());
                });
            }

            if (params.init) this.mask();

            el.classList.add(initializedClass);
        }

        /**
         * @function AppMask.freePhoneOptions
         * @memberof components
         * @desc Get-функция. Базовые настройки простой маски для телефона.
         * @returns {Object} Объект с настройками.
         */
        static get freePhoneOptions() {
            return util.defaultsDeep(freePhoneOptions, defaultOptions);
        }

        /**
         * @function AppMask.rusPhoneOptions
         * @memberof components
         * @desc Get-функция. Базовые настройки простой маски для российского телефона.
         * @returns {Object} Объект с настройками.
         */
        static get rusPhoneOptions() {
            return util.defaultsDeep(rusPhoneOptions, defaultOptions);
        }

        /**
         * @function AppMask.dateOptions
         * @memberof components
         * @desc Get-функция. Локализованная маска для даты.
         * @returns {Object} Объект с настройками.
         */
        static get dateOptions() {
            return util.defaultsDeep(dateOptions, defaultOptions);
        }

        /**
         * @function AppMask.numberOptions
         * @memberof components
         * @desc Get-функция. Маска для ввода целых чисел.
         * @returns {Object} Объект с настройками.
         */
        static get numberOptions() {
            return util.defaultsDeep(numberOptions, defaultOptions);
        }

        /**
         * @function AppMask.floatNumberOptions
         * @memberof components
         * @desc Get-функция. Маска для ввода дробных чисел.
         * @returns {Object} Объект с настройками.
         */
        static get floatNumberOptions() {
            return util.defaultsDeep(floatNumberOptions, defaultOptions);
        }

        /**
         * @function AppMask.priceOptions
         * @memberof components
         * @desc Get-функция. Маска для ввода цен.
         * @returns {Object} Объект с настройками.
         */
        static get priceOptions() {
            return util.defaultsDeep(priceOptions, defaultOptions);
        }
    });

    addModule(moduleName, AppMask);

    //Инициализация элемента по data-атрибуту
    document.querySelectorAll('[data-mask]').forEach(el => new AppMask(el));

    return AppMask;
};

export default maskInit;
export {maskInit};