import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let maskTriggered;

const maskCallback = resolve => {
    const imports = [require('Components/date').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "mask" */ './sync.js'))
            .then(mask => {
                chunks.mask = true;
                maskTriggered = true;

                const AppMask = mask.maskInit(...modules);
                if (resolve) resolve(AppMask);
            });
    });

    emitInit('date');
};

const initMask = () => {
    if (maskTriggered) return;

    emitInit('mask');
};

const defaultMaskTriggers = [
    'click',
    'input',
    'focus'
];
const maskTrigger = (maskItems, maskTriggers = defaultMaskTriggers) => {
    if (__IS_SYNC__) return;

    const items = (maskItems instanceof Node) ? [maskItems] : maskItems;
    if (items.length === 0) return;

    items.forEach(item => {
        maskTriggers.forEach(trigger => {
            item.addEventListener(trigger, initMask, {once: true});
        });
        if (item === document.activeElement) initMask();
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.mask) {
        maskCallback(resolve);
        return;
    }

    onInit('mask', () => {
        maskCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('mask');
    });

    const maskItems = document.querySelectorAll('[data-mask]');
    maskTrigger(maskItems);
});

export default importFunc;
export {maskTrigger};