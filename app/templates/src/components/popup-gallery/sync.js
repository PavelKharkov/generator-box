import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import 'Components/slider';

//Опции
const moduleName = 'popupGallery';

//Классы
const popupGalleryClass = util.kebabCase(moduleName);
const initializedClass = `${popupGalleryClass}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    popup: {
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            arrows: true
        }
    }
};

const defaultPreloadOption = [0, 2];

const popupGalleryInit = AppPopupMagnific => {
    /**
     * @class AppPopupGallery
     * @memberof components
     * @requires libs.jquery
     * @requires libs.magnific-popup
     * @classdesc Модуль галерей в попапе.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {(HTMLElement|string)} element - Элемент попапа или HTML-контент попапа.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {*} [options.popup...] - Опции плагина [Magnific popup]{@link libs.magnific-popup}.
     * @example
     * const popupGalleryInstance = new app.PopupGallery(document.querySelector('.popup-gallery-container'), {
     *     popup: {
     *         preload: false
     *     }
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div data-popup-gallery>
     *     <a href="#" target="_blank" data-popup-gallery-item>
     *         <img src="" alt="">
     *     </a>
     *     <a href="#" target="_blank" data-popup-gallery-item>
     *         <img src="" alt="">
     *     </a>
     * </div>
     *
     * <!--data-popup-gallery - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-popup-gallery-options='{"preload": [0, 3]}'></div>
     */
    const AppPopupGallery = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const popupGalleryOptionsData = el.dataset.popupGalleryOptions;
            if (popupGalleryOptionsData) {
                const dataOptions = util.stringToJSON(popupGalleryOptionsData);
                if (!dataOptions) util.error('incorrect data-popup-gallery-options format');
                util.extend(this.options, dataOptions);
            }

            util.defaultsDeep(this.options, defaultOptions);

            if (this.options.popup && this.options.popup.gallery) {
                this.options.popup.gallery.preload = this.options.popup.gallery.preload || defaultPreloadOption;
            }

            /**
             * @member {Object} AppPopupGallery#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            const items = [...el.querySelectorAll('[data-popup-gallery-item]')];
            if (items.length === 0) return;

            let isShown = false;

            /**
             * @member {boolean} AppPopupGallery#isShown
             * @memberof components
             * @desc Показана ли галерея.
             * @readonly
             */
            Object.defineProperty(this, 'isShown', {
                enumerable: true,
                get: () => isShown
            });

            let slider;

            /**
             * @member {Object} AppPopupGallery#slider
             * @memberof components
             * @desc Если галерея является слайдером, то содержит ссылку на экземпляр модуля слайдера [AppSlider]{@link components.AppSlider}.
             * @readonly
             */
            Object.defineProperty(this, 'slider', {
                enumerable: true,
                get: () => slider
            });

            if (el.modules.slider) {
                slider = el.modules.slider;
            }

            const galleryItems = items.map((item, index) => {
                if (!item.modules) {
                    item.modules = {};
                }
                item.modules[moduleName] = this;

                item.addEventListener('click', event => {
                    event.preventDefault();

                    this.show({index});
                });

                return {
                    type: 'image',
                    src: item.getAttribute('href')
                };
            });
            params.popup.items = galleryItems;

            /**
             * @member {Array.<Object>} AppPopupGallery#galleryItems
             * @memberof components
             * @desc Коллекция элементов галереи.
             * @readonly
             * @example
             * //Пример галереи с двумя изображениями
             * console.log(popupGalleryInstance.galleryItems); //Выведет [{type: 'image', src: '#'}, {type: 'image', src: '#'}]
             */
            Object.defineProperty(this, 'galleryItems', {
                enumerable: true,
                value: galleryItems
            });

            const popup = new AppPopupMagnific(galleryItems, params.popup);

            /**
             * @member {Object} AppPopupGallery#popup
             * @memberof components
             * @desc Ссылка на модуль попапа [AppPopup]{@link components.AppPopup}, использующегося при открытии галереи.
             * @readonly
             */
            Object.defineProperty(this, 'popup', {
                enumerable: true,
                value: popup
            });

            this.on({
                /**
                 * @event AppPopupGallery#show
                 * @memberof components
                 * @desc Показывает галерею.
                 * @param {Object} [options={}] - Опции.
                 * @param {number} [options.index] - Номер изображения, с которого нужно показывать галерею.
                 * @param {number} [options.hash] - Идентификатор изображения, с индекса которого нужно показыывать галерею.
                 * @fires components.AppPopup#show
                 * @fires components.AppPopupGallery#shown
                 * @returns {undefined}
                 * @example
                 * popupGalleryInstance.on('show', () => {
                 *     console.log('Галерея показывается');
                 * });
                 * console.log(popupGalleryInstance.isShown); //false
                 * popupGalleryInstance.show({
                 *     index: 2
                 * });
                 * console.log(popupGalleryInstance.isShown); //true
                 */
                show(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    if (isShown) return;
                    isShown = true;

                    util.defaultsDeep(options, params);
                    if (options.hash) {
                        const currentItem = document.querySelector(`#${options.hash}`);
                        options.index = [...items].indexOf(currentItem);
                    }

                    popup.show(options);
                },

                /**
                 * @event AppPopupGallery#hide
                 * @memberof components
                 * @desc Скрывает галерею.
                 * @fires components.AppPopup#hide
                 * @fires components.AppPopupGallery#hidden
                 * @returns {undefined}
                 * @example
                 * popupGalleryInstance.on('hide', () => {
                 *     console.log('Галерея скрывается');
                 * });
                 * console.log(popupGalleryInstance.isShown); //true
                 * popupGalleryInstance.hide();
                 * console.log(popupGalleryInstance.isShown); //false
                 */
                hide() {
                    if (!isShown) return;

                    isShown = false;

                    popup.hide();
                }
            });

            this.onSubscribe([
                /**
                 * @event AppPopupGallery#shown
                 * @memberof components
                 * @desc Вызывается после показа галереи.
                 * @returns {undefined}
                 * @example
                 * popupGalleryInstance.on('shown', () => {
                 *     console.log('Галерея показана');
                 * });
                 */
                'shown',

                /**
                 * @event AppPopupGallery#hidden
                 * @memberof components
                 * @desc Вызывается после скрытия галереи.
                 * @returns {undefined}
                 * @example
                 * popupGalleryInstance.on('hidden', () => {
                 *     console.log('Галерея скрыта');
                 * });
                 */
                'hidden'
            ]);

            //Привязка к событиям модуля попапа
            popup.on({
                show: () => {
                    if (isShown) return;

                    isShown = true;

                    if (popup.isShown) return;

                    this.show();
                },
                shown: () => {
                    this.emit('shown');
                },

                hide: () => {
                    if (!isShown) return;

                    isShown = false;

                    const currentSlider = slider || el.modules.slider;
                    if (currentSlider) {
                        if (!slider) {
                            slider = currentSlider;
                        }
                        const currentItem = el.querySelector(`[data-popup-gallery-index="${AppPopupMagnific.instance.index}"]`).parentNode;
                        const index =
                            currentSlider.items.indexOf(currentItem);

                        currentSlider.goto({
                            index,
                            duration: 0
                        });
                    }

                    this.hide();
                },
                hidden: () => {
                    this.emit('hidden');
                }
            });

            el.classList.add(initializedClass);

            const currentIndex = el.dataset.popupGalleryTriggered;
            if (typeof currentIndex !== 'undefined') {
                delete el.dataset.popupGalleryTriggered;
                if (currentIndex !== '') {
                    this.show({index: currentIndex});
                }
            }
        }
    });

    //Настройка возможности управления с помощью адресной строки
    require('Components/url').default.then(AppUrl => {
        AppUrl.registerAction({
            id: moduleName,
            selector: `[${AppUrl.dataHashAction}="${moduleName}"]`,
            action: `${moduleName}.show`
        });
    });
    if (window.location.hash) emitInit('url');

    addModule(moduleName, AppPopupGallery);

    //Инициализация элементов по data-атрибуту
    document.querySelectorAll('[data-popup-gallery]').forEach(el => new AppPopupGallery(el));

    return AppPopupGallery;
};

export default popupGalleryInit;
export {popupGalleryInit};