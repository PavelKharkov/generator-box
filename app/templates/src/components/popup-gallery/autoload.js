if (__IS_DISABLED_MODULE__) {
    if (__IS_SYNC__ || document.querySelector('[data-popup-gallery], [data-popup-gallery-init]')) {
        require('.');
    }
}