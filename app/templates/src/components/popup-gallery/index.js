let importFunc;
let popupGalleryTrigger;

if (__IS_DISABLED_MODULE__) {
    let popupGalleryTriggered;

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const popupGalleryCallback = resolve => {
        const imports = [require('Components/popup-magnific').default];

        Promise.all(imports).then(modules => {
            (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
                .then(popupGallery => {
                    chunks.popups = true;
                    popupGalleryTriggered = true;

                    const AppPopupGallery = popupGallery.popupGalleryInit(...modules);
                    if (resolve) resolve(AppPopupGallery);
                });
        });

        emitInit('popupMagnific');
    };

    const initPopupGallery = () => {
        if (popupGalleryTriggered) return;

        emitInit('popupGallery');
    };

    popupGalleryTrigger = (popupGalleryItems, popupGalleryTriggers = ['click']) => {
        if (__IS_SYNC__) return;

        const items = (popupGalleryItems instanceof Node) ? [popupGalleryItems] : popupGalleryItems;
        if (items.length === 0) return;

        items.forEach(item => {
            popupGalleryTriggers.forEach(trigger => {
                item.addEventListener(trigger, initPopupGallery, {once: true});
            });
        });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.popups) {
            popupGalleryCallback(resolve);
            return;
        }

        onInit('popupGallery', () => {
            popupGalleryCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('popupGallery');
        });

        const popupGalleryItems = document.querySelectorAll('[data-popup-gallery]');
        popupGalleryTrigger(popupGalleryItems);
    });
}

export default importFunc;
export {popupGalleryTrigger};