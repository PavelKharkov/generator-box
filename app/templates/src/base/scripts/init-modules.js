/* istanbul ignore file */

/**
 * @namespace fonts
 * @desc Шрифты.
 * @ignore
 */
import 'Fonts';

import 'Layout/base';

/**
 * @namespace components
 * @desc Компоненты.
 */
import 'Components';

/**
 * @namespace layout
 * @desc Компоненты разметки.
 */
import 'Layout';

/**
 * @namespace libs
 * @desc Вендорные модули.
 */

/**
 * @namespace pages
 * @desc Модули страниц.
 * @ignore
 */

if (__IS_BUILDER__) {
    require('Layout/builder/index.js');
}