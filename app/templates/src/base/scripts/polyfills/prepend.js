/* istanbul ignore file */

[
    Element.prototype,
    Document.prototype,
    DocumentFragment.prototype
].forEach(item => {
    Object.defineProperty(item, 'prepend', {
        configurable: true,
        enumerable: true,
        writable: true,
        value(...args) {
            const documentFragment = document.createDocumentFragment();
            args.forEach(argument => {
                const isNode = argument instanceof Node;
                documentFragment.appendChild(isNode ? argument : document.createTextNode(String(argument))); /* eslint-disable-line unicorn/prefer-dom-node-append */
            });
            this.insertBefore(documentFragment, this.firstChild);
        }
    });
});