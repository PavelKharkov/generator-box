/* istanbul ignore file */

const CustomEventFunc = (event, options) => {
    const eventOptions = options || {
        bubbles: false,
        cancelable: false,
        detail: undefined /* eslint-disable-line no-undefined */
    };
    const newEvent = document.createEvent('CustomEvent');
    newEvent.initCustomEvent(event, eventOptions.bubbles, eventOptions.cancelable, eventOptions.detail);
    return newEvent;
};

CustomEventFunc.prototype = window.Event.prototype;
window.CustomEvent = CustomEventFunc;