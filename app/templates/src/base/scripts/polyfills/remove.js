/* istanbul ignore file */

[
    Element.prototype,
    CharacterData.prototype,
    DocumentType.prototype
].forEach(item => {
    Object.defineProperty(item, 'remove', {
        configurable: true,
        enumerable: true,
        writable: true,
        value() {
            this.parentNode.removeChild(this); /* eslint-disable-line unicorn/prefer-dom-node-remove */
        }
    });
});