/* istanbul ignore file */

const nativeAdd = window.DOMTokenList.prototype.add;
window.DOMTokenList.prototype.add = function(...classes) {
    classes.forEach(classItem => nativeAdd.call(this, classItem));
};

const nativeRemove = window.DOMTokenList.prototype.remove;
window.DOMTokenList.prototype.remove = function(...classes) {
    classes.forEach(classItem => nativeRemove.call(this, classItem));
};