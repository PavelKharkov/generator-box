/* istanbul ignore file */

Element.prototype.closest = function(selector) {
    let parentEl = this; /* eslint-disable-line consistent-this, unicorn/no-this-assignment */

    while (parentEl) {
        if (parentEl && parentEl.matches(selector)) return parentEl;
        parentEl = parentEl.parentElement;
    }

    return null;
};