import {Vue, Vuex} from 'Libs/vue';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import state from './state';

Vue.use(Vuex);

const defaultState = JSON.parse(JSON.stringify(state));

state.defaults = defaultState;

const store = new Vuex.Store({
    actions,
    getters,
    mutations,
    state
});

export {
    store,
    actions,
    getters,
    mutations,
    state
};
export default store;