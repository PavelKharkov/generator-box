import app from './app.js';

import Vue from 'Libs/vue';
import store from './store';
import router from './router';

/**
 * @member {Object} app#vue
 * @desc Экземпляр Vue.
 * @readonly
 * @example
 * console.log(app.vue);
 */
Object.defineProperty(app, 'vue', {
    enumerable: true,
    value: new Vue({
        el: '#app',
        name: 'App',
        router,
        store,
        template: '<router-view></router-view>'
    })
});