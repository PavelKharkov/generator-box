/**
 * @module core-js
 * @version 3.19.3
 * @desc [Сайт]{@link https://github.com/zloirock/core-js}.
 * @ignore
 */
import 'core-js/stable/dom-collections/for-each';
import 'core-js/stable/array/find';
import 'core-js/stable/array/from';
import 'core-js/stable/array/includes';
import 'core-js/stable/array/iterator';
import 'core-js/stable/number/is-finite';
import 'core-js/stable/number/is-nan';
import 'core-js/stable/number/parse-float';
import 'core-js/stable/number/parse-int';
import 'core-js/stable/object/assign';
import 'core-js/stable/promise';
import 'core-js/stable/promise/finally';
import 'core-js/stable/string/ends-with';
import 'core-js/stable/string/includes';
import 'core-js/stable/string/repeat';
import 'core-js/stable/string/starts-with';

const testElement = document.createElement('div');

//Полифил node.matches()
if (!Element.prototype.matches && Element.prototype.msMatchesSelector) {
    require('./polyfills/matches.js');
}

//Полифил node.closest()
if (!Element.prototype.closest) {
    require('./polyfills/closest.js');
}

//Полифил node.append()
if (!Element.prototype.append) {
    require('./polyfills/append.js');
}

//Полифил node.prepend()
if (!Element.prototype.prepend) {
    require('./polyfills/prepend.js');
}

//Полифил node.remove()
if (!Element.prototype.remove) {
    require('./polyfills/remove.js');
}

//Полифил множественных параметров для node.classList.add() и node.classList.remove()
if (!testElement.classList.replace) {
    require('./polyfills/class-list.js');
}

//Полифил CustomEvent
if (typeof window.CustomEvent !== 'function') {
    require('./polyfills/custom-event.js');
}