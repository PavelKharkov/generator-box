if (!__IS_DIST__ || __IS_DEBUG__) {
    if (typeof window.debug === 'undefined') {
        window.debug = false; //Нужно поставить true, чтобы включить режим отладки в режиме разработки
    }

    const colors = {
        success: '#28a745',
        info: '#17a2b8',
        warning: '#d89e07',
        danger: '#dc3545'
    };

    const consoleFunc = console.log; /* eslint-disable-line no-console */

    /**
     * @function log
     * @desc Логирует сообщения в режиме отладки.
     * @param {string} message - Логируемое сообщение.
     * @param {('success'|'info'|'warning'|'danger')} colorName - Цвет логируемого сообщения.
     * @param {*} args - Дополнительные логируемые параметры.
     * @throws Выводит ошибку, если сообщение не является непустой строкой.
     * @returns {undefined}
     * @ignore
     */
    window.log = (message, colorName, ...args) => {
        if ((typeof message !== 'string') || (message.length === 0)) {
            throw new TypeError('parameter \'message\' must be a non-empty string');
        }

        const color = colors[colorName];
        const colorString = color ? `color: ${color}` : '';
        consoleFunc(`%c[Debug] ${message}`, colorString, ...args);
    };

    window.log('debug mode on', 'danger');
}