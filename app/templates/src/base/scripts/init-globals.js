import {Module, immutable} from 'Components/module';
import util from 'Layout/main';

import app from './app.js';

/**
 * @member {Object} app#globalConfig
 * @desc Содержит глобальную конфигурацию приложения.
 * @property {string} relPath - Путь к папке страницы относительно корня сайта.
 * @property {string} assetsPath - Путь к папке ресурсов относительно текущей страницы.
 */

/**
 * @function app#ready
 * @desc Запускает функцию после готовности DOM.
 * @param {Function} callback - Функция обратного вызова.
 * @returns {undefined}
 * @example
 * const DOMLoadedFunction = () => {
 *     console.log('DOM is loaded');
 * };
 *
 * app.ready(DOMLoadedFunction);
 */

//Глобальные функции
/**
 * @function app#run
 * @desc Вызывает статическое свойство или метод класса.
 * @param {string} Class - Класс, в котором содержится вызываемое свойство/метод.
 * @param {string} prop - Имя статического свойства/метода.
 * @param {...*} [args] - Параметры, передаваемые вызываемому методу.
 * @throws Выводит ошибку, если класс не найден в переменной app.
 * @returns {*} Возвращаемое значение метода.
 * @example
 * const rgbObject = app.run('Color', 'parseRgb', 'rgb(255, 0, 0, 0.5)');
 */
const run = (Class = util.required('Class'), prop = util.required('prop'), ...args) => {
    const textProp = String(prop);
    if (!app[Class]) {
        util.error(`can't find class '${Class}' in app`);
    }

    const classMethod = app[Class][textProp];
    if (typeof classMethod === 'function') {
        return classMethod(...args);
    }

    return classMethod;
};

/**
 * @function app#init
 * @desc Инициализирует новый экземпляр класса.
 * @param {string} Class - Название инициализируемого класса.
 * @param {(*|app.Module.instanceOptions)} element - Элемент либо опции экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра или функция, возвращающая объект опций, или опция appname.
 * @param {string} [appname] - Название инициализируемого класса, как оно будет отображаться в свойстве modules у элемента экземпляра.
 * @returns {Object} Проинициализированный экземпляр класса.
 * @example
 * const currentDate = new Date();
 * const newModuleInstance = app.init('Date', currentDate, {
 *     someOption: '123'
 * });
 *
 * @example
 * const newModuleInstance = app.init('Module', () => {
 *     console.log('это новый модуль');
 * }, 'some-module'); //В случае с созданием нового модуля без расширения класса, необходимо указывать название экземпляра класса
 */
const init = (Class = util.required('Class'), element = util.required('element'), options, appname) => { /* eslint-disable-line default-param-last, max-params */
    return new app[Class](element, options, appname);
};

/**
 * @function app#extend
 * @desc Инициализирует расширенный класс.
 * @param {string} Class - Название класса в объекте app.
 * @param {Function} ExtendClass - Класс, наследуемый новым классом.
 * @param {Function} [callback] - Функция, вызываемая при инициализации класса.
 * @returns {Object} Созданный класс.
 * @example
 * const TestSlider = app.extend('TestSlider', app.Slider, () => {
 *     console.log('это сообщение появится при инициализации экземпляра этого класса');
 * });
 * const TestSliderInstance = new TestSlider(document.querySelector('.slider-element')); //Также новый класс доступен через app.TestSlider
 */
const extend = (Class = util.required('Class'), ExtendClass = util.required('ExtendClass'), callback) => { /* eslint-disable-line default-param-last */
    const newClass = immutable(class extends ExtendClass {
        constructor(el, opts) {
            const appname = Class.charAt(0).toLowerCase() + Class.slice(1);
            super(el, opts, appname);

            if (typeof callback === 'function') callback.call(this);
        }
    });

    Object.defineProperty(app, Class, {
        enumerable: true,
        value: newClass
    });

    return newClass;
};

Object.defineProperty(app, 'Module', {
    enumerable: true,
    value: Module
});

let readyState = document.readyState;

window.addEventListener('readystatechange', () => {
    readyState = document.readyState;
});

const windowApp = window.app;

/**
 * @member {string} app#readyState
 * @desc Состояние загрузки страницы.
 * @readonly
 */
Object.defineProperty(windowApp, 'readyState', {
    enumerable: true,
    get: () => readyState
});

//Добавление функций инициализации к глобальной переменной приложения
Object.defineProperty(windowApp, 'run', {
    enumerable: true,
    value: run
});
Object.defineProperty(windowApp, 'init', {
    enumerable: true,
    value: init
});
Object.defineProperty(windowApp, 'extend', {
    enumerable: true,
    value: extend
});
Object.defineProperty(windowApp, 'util', {
    enumerable: true,
    value: util
});