import Module from 'Components/module';
import util from 'Layout/main';

/**
 * @namespace app
 * @desc Глобальный контейнер приложения.
 */
//const app = {}; //При необходимости скрытия модулей
const app = window.app || {};
window.app = app;

if (!__IS_DIST__ || __IS_DEBUG__) {
    if (window.debug) window.log('window.app initialized', 'danger');
}

/**
 * @function modules
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для вызова событий при инициализации асинхронных модулей.
 * @ignore
 */
const modules = new Module(
    () => {
        //empty function
    },

    'modules'
);

/**
 * @function addModule
 * @desc Добавляет модуль в глобальный объект приложения. Кроме того, замораживает добавленный модуль. При загрузке каждого модуля вызывает соответствующее событие (Названное как и сам модуль в app: классы записаны с заглавной буквы, экземпляры модулей — со строчной).
 * @param {string} moduleName - Под каким ключом экземпляры модуля будут отображаться в объекте modules у DOM-элементов.
 * @param {Object} Class - Ссылка на сам класс модуля.
 * @ignore
 */
const addModule = (moduleName = util.required('moduleName'), Class = util.required('Class')) => {
    const moduleCase = (Class instanceof Module) ? 'toLowerCase' : 'toUpperCase';
    const appModuleName = moduleName.charAt(0)[moduleCase]() + moduleName.slice(1);
    Object.defineProperty(app, appModuleName, {
        enumerable: true,
        value: Class
    });

    modules.onSubscribe(appModuleName);

    if (!__IS_DIST__ || __IS_DEBUG__) {
        if (window.debug) window.log(`module '${appModuleName}' added`, 'danger');
    }

    util.defer(() => {
        if (!modules.events().includes(appModuleName)) return;

        modules.emit(appModuleName);
        modules.off(appModuleName);
    });
};

/**
 * @function onInit
 * @desc Вызывает функцию обратного вызова при инициализации модуля.
 * @param {string} moduleName - Название модуля после которого нужно вызвать функцию обратного вызова.
 * @param {Function} callback - Функция, которую нужно вызвать после инициализации модуля.
 * @returns {undefined}
 * @ignore
 */
const onInit = (moduleName = util.required('moduleName'), callback = util.required('callback')) => {
    if (__IS_SYNC__) return;

    const appModuleName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1);
    const initEvent = `${appModuleName}Init`;
    modules.onSubscribe(initEvent, () => {
        callback();
    });

    if (!__IS_DIST__ || __IS_DEBUG__) {
        if (window.debug) window.log(`initializing '${appModuleName}' module...`, 'danger');
    }
};

const emitInit = (...args) => {
    if (__IS_SYNC__) return;

    args.forEach(moduleName => {
        const appModuleName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1);
        const initEvent = `${appModuleName}Init`;
        if (!modules.events().includes(initEvent)) return;

        modules.emit(initEvent);
        modules.off(initEvent);
    });
};

/**
 * @function app#emit
 * @desc Вызывает инициализацию модуля и соответствующее событие инициализации.
 * @param {...string} [args] - Список модулей для инициализации.
 * @returns {undefined}
 * @readonly
 */
Object.defineProperty(app, 'emit', {
    enumerable: true,
    value: emitInit
});

const defaultDependencyCallback = nextCallback => {
    nextCallback();
};

const on = (dependencies, callback) => {
    if (!(dependencies && (typeof callback === 'function'))) return;

    const dependenciesArray = Array.isArray(dependencies) ? dependencies : [dependencies];
    const initFunc = dependenciesArray.reduce((prevCallback, currentValue) => {
        const nextCallback = innerCallback => {
            prevCallback(() => {
                if (app[currentValue]) {
                    innerCallback();
                    return;
                }

                modules.onSubscribe(currentValue, innerCallback);
            });
        };
        return nextCallback;
    }, defaultDependencyCallback);

    initFunc(callback);
};

/**
 * @function app#on
 * @desc Вызывает функцию обратного вызова после инициализации модулей.
 * @param {(Array.<string>|string)} dependencies - Коллекция модулей, после которых нужно вызвать функцию обратного вызова или название модуля, после которого нужно вызвать функцию обратного вызова.
 * @param {Function} callback - Функция обратного вызова.
 * @returns {undefined}
 * @readonly
 */
Object.defineProperty(app, 'on', {
    enumerable: true,
    value: on
});

export default app;
export {
    app,
    addModule,
    onInit,
    emitInit
};