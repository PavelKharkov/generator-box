const preloadModules = require('./preload-modules.json');

const stylesFile = 'styles';
const scriptsFile = 'scripts';

const assetsPath = window.app.globalConfig.assetsPath;

/**
 * @function preloadStyle
 * @desc Добавляет тег предзагрузки стиля на страницу.
 * @param {string} chunkName - Название предзагружаемого файла.
 * @param {boolean} [prefetch] - Предзагружать файл с помощью prefetch.
 * @returns {undefined}
 * @ignore
 */
const preloadStyle = (chunkName, prefetch) => {
    const link = document.createElement('link');
    link.rel = prefetch ? 'prefetch' : 'preload';
    link.as = 'style';
    link.href = `${assetsPath + stylesFile}/${chunkName}.${stylesFile}.css?${__COMPILATION_TIME__}`;

    document.head.append(link);
};

/**
 * @function preloadScript
 * @desc Добавляет тег предзагрузки скрипта на страницу.
 * @param {string} chunkName - Название предзагружаемого файла.
 * @param {boolean} [prefetch] - Предзагружать файл с помощью prefetch.
 * @returns {undefined}
 * @ignore
 */
const preloadScript = (chunkName, prefetch) => {
    const link = document.createElement('link');
    link.rel = prefetch ? 'prefetch' : 'preload';
    link.as = 'script';
    link.href = `${assetsPath + scriptsFile}/${chunkName}.${scriptsFile}.js?${__COMPILATION_TIME__}`;

    document.head.append(link);
};

//TODO:tests
/**
 * @function app.preload
 * @desc Добавляет теги предзагрузки стилей и скриптов на страницу.
 * @param {string} chunkName - Название предзагружаемых файлов.
 * @param {boolean} [styles=true] - Добавлять ли файл стилей.
 * @param {boolean} [scripts=true] - Добавлять ли файл скриптов.
 * @returns {undefined}
 * @ignore
 */
window.app.preload = (chunkName, styles = true, scripts = true) => {
    if ((typeof chunkName !== 'string') || !chunkName) return;

    if (styles) preloadStyle(chunkName);
    if (scripts) preloadScript(chunkName);
};

preloadModules.forEach(currentModule => {
    if (typeof currentModule === 'string') {
        preloadStyle(currentModule, true);
        preloadScript(currentModule, true);
        return;
    }

    if (currentModule.css) {
        preloadStyle(currentModule.name, true);
    }
    if (currentModule.js) {
        preloadScript(currentModule.name, true);
    }
});