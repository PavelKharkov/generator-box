/* istanbul ignore file */

import './debug.js';

if (__IS_BUILDER__) {
    window.builderConfig = __GLOBAL_SETTINGS__;
}

__webpack_public_path__ = window.app.globalConfig.relPath; /* eslint-disable-line camelcase */

//Предварительно импортируются полифилы
if (__HAS_POLYFILLS__) {
    require.resolveWeak('./polyfills.js');
}

//Общие вендорные модули
require.resolveWeak('Libs/mobile-detect/sync.js');
require.resolveWeak('Libs/platform/sync.js');