/* istanbul ignore file */

import './preinit.js';
import 'Layout/prolog/init.js';

document.addEventListener('DOMContentLoaded', () => {
    if (!__IS_DIST__ || __IS_DEBUG__) {
        if (window.debug) window.log('DOMContentLoaded emitted', 'danger');
    }

    require('./init-modules.js');
    require('./init-globals.js');
    if (__HAS_VUE__) {
        require('./init-vue.js');
    }

    //Вызов функции после инициализации модулей приложения
    if (__IS_DIST__) {
        require('Layout/epilog/init.js').default();
        return;
    }

    window.addEventListener('load', () => {
        require('Layout/epilog/init.js').default();
    });
});