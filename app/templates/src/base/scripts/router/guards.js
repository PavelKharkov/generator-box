const initRouterGuards = router => {
    router.onError(error => {
        console.log(error); /* eslint-disable-line no-console */
    });
};

export default initRouterGuards;