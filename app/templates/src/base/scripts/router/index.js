import {Vue, VueRouter} from 'Libs/vue';

import initRouterGuards from './guards.js';

Vue.use(VueRouter);

const routes = [];

const router = new VueRouter({
    routes,
    mode: 'history'
});

initRouterGuards(router);

export default router;