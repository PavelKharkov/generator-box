/* eslint-disable unicorn/consistent-function-scoping */

import {throwsError, yieldDataTypesCheck} from '../test';

window.debug = true;
require('../debug.js');
const logSpy = jest.spyOn(window, 'log');
const consoleLog = window.console.log;

describe('debug', () => {
    test('при включении window.debug должно выводиться сообщение о показе сообщений отладчика', () => {
        expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] debug mode on', 'color: #dc3545');
    });

    describe('window.log', () => {
        test('должен вызываться с переданными параметрами', () => {
            window.log('test message', 'red');
            expect(logSpy).toHaveBeenLastCalledWith('test message', 'red');
        });

        describe('некорректные типы параметра \'message\'', () => {
            /* eslint-disable no-undefined */
            const dataTypesAssertions = [
                ['string number', undefined],
                ['space', undefined],
                ['string', undefined],
                ['HTML string', undefined]
            ];
            /* eslint-enable no-undefined */
            yieldDataTypesCheck({
                checkFunction(logFunction, value) {
                    return logFunction(value);
                },
                dataTypesAssertions,
                defaultValue: [throwsError, 'parameter \'message\' must be a non-empty string'],
                setupFunction() {
                    const logFunction = window.log;

                    return logFunction;
                }
            });
        });

        describe('некорректные типы параметра \'color\'', () => {
            yieldDataTypesCheck({
                checkFunction(logFunction, value) {
                    return logFunction('test message', value);
                },
                dataTypesAssertions: [],
                defaultValue: undefined, /* eslint-disable-line no-undefined */
                setupFunction() {
                    const logFunction = window.log;

                    return logFunction;
                }
            });
        });

        test('должен вызывать console.log', () => {
            window.log('test message', 'danger');
            expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] test message', 'color: #dc3545');
        });

        test('должен выводить ошибку, если не передан параметр \'message\'', () => {
            const logResult = (() => window.log());
            expect(logResult).toThrow('parameter \'message\' must be a non-empty string');
        });

        test('должен не применять стили к сообщению, если переданный параметр цвета не передан', () => {
            window.log('test message');
            expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] test message', '');
        });

        test('должен не применять стили к сообщению, если переданный параметр цвета некорректный', () => {
            window.log('test message', 'red');
            expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] test message', '');
        });

        describe('корректные варианты цвета в сообщениях', () => {
            const logColorsArray = [
                ['success', '#28a745'],
                ['info', '#17a2b8'],
                ['warning', '#d89e07'],
                ['danger', '#dc3545']
            ];
            test.each(logColorsArray)('должен корректно обрабатывать идентификатор цвета %s', (colorId, color) => {
                window.log('test message', colorId);
                expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] test message', `color: ${color}`);
            });
        });

        test('должен выводить все остаточные параметры, переданные в функцию логирования', () => {
            window.log('test message', 'red');
            expect(consoleLog).toHaveBeenLastCalledWith('%c[Debug] test message', '');
        });
    });
});