//Сохранение подменяемых методов и свойств
const elementMatches = Element.prototype.matches;
const elementClosest = Element.prototype.closest;
const appendElementTypes = [
    'Element',
    'Document',
    'DocumentFragment'
];
const prependMethods = appendElementTypes.map(elementType => {
    const prepend = window[elementType].prototype.prepend;
    return prepend;
});
const appendMethods = appendElementTypes.map(elementType => {
    const append = window[elementType].prototype.append;
    return append;
});
const removeElementTypes = [
    'Element',
    'CharacterData',
    'DocumentType'
];
const removeMethods = removeElementTypes.map(elementType => {
    const remove = window[elementType].prototype.remove;
    return remove;
});
const classListReplace = DOMTokenList.prototype.replace;
const classListAdd = DOMTokenList.prototype.add;
const classListRemove = DOMTokenList.prototype.remove;
const customEventFunc = window.CustomEvent;

//Удаление подменяемых методов и свойств
delete Element.prototype.matches;
Element.prototype.msMatchesSelector = function() {
    //empty function
};
delete Element.prototype.closest;
appendElementTypes.forEach(elementType => {
    delete window[elementType].prototype.prepend;
    delete window[elementType].prototype.append;
});
removeElementTypes.forEach(elementType => {
    delete window[elementType].prototype.remove;
});
delete DOMTokenList.prototype.replace;
delete window.CustomEvent;

require('../polyfills.js');

describe('polyfills', () => {
    afterAll(() => {
        Element.prototype.matches = elementMatches;
        delete Element.prototype.msMatchesSelector;
        Element.prototype.closest = elementClosest;
        appendElementTypes.forEach((elementType, index) => {
            window[elementType].prototype.prepend = prependMethods[index];
            window[elementType].prototype.append = appendMethods[index];
        });
        removeElementTypes.forEach((elementType, index) => {
            window[elementType].prototype.remove = removeMethods[index];
        });
        DOMTokenList.prototype.replace = classListReplace;
        DOMTokenList.prototype.add = classListAdd;
        DOMTokenList.prototype.remove = classListRemove;
        window.CustomEvent = customEventFunc;
    });

    test('должен подключаться полифил \'Element.prototype.matches\'', () => {
        const matches = Element.prototype.matches;

        expect(typeof matches).toBe('function');
    });

    test('должен подключаться полифил \'Element.prototype.closest\'', () => {
        const closest = Element.prototype.closest;

        expect(typeof closest).toBe('function');
    });

    describe('prepend', () => {
        test.each(appendElementTypes)('должен подключаться в прототипе \'%s\'', elementType => {
            const prepend = window[elementType].prototype.prepend;

            expect(typeof prepend).toBe('function');
        });
    });

    describe('append', () => {
        test.each(appendElementTypes)('должен подключаться в прототипе \'%s\'', elementType => {
            const append = window[elementType].prototype.append;

            expect(typeof append).toBe('function');
        });
    });

    describe('remove', () => {
        test.each(removeElementTypes)('должен подключаться в прототипе \'%s\'', elementType => {
            const remove = window[elementType].prototype.remove;

            expect(typeof remove).toBe('function');
        });
    });

    describe('classList', () => {
        test('должен подключаться полифил метода \'add\'', () => {
            const addClass = window.DOMTokenList.prototype.add;

            expect(typeof addClass).toBe('function');
        });

        test('должен подключаться полифил метода \'remove\'', () => {
            const removeClass = window.DOMTokenList.prototype.remove;

            expect(typeof removeClass).toBe('function');
        });
    });

    test('должен подключаться полифил \'CustomEvent\'', () => {
        const customEvent = window.CustomEvent;

        expect(typeof customEvent).toBe('function');
    });
});