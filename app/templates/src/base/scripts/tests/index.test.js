/* eslint-disable unicorn/consistent-function-scoping */

import '../test';

import {app, addModule} from '../app.js';
import '../init-globals.js';

describe('глобальные функции', () => {
    afterEach(() => {
        delete document.modules;
    });

    describe('добавление глобальных функций и свойств', () => {
        const globalsArray = [
            ['run', 'function'],
            ['init', 'function'],
            ['extend', 'function'],
            ['Module', 'function'],
            ['util', 'object']
        ];
        test.each(globalsArray)('должен добавляться ключ \'%s\' в глобальную переменную приложения', (key, keyType) => {
            expect(typeof app[key]).toBe(keyType);
        });

        test.each(globalsArray)('ключ \'%s\' не должен быть перезаписываемым в глобальной переменной приложения', key => {
            const propertyEdit = (() => {
                app[key] = null;
            });
            const propertyRemove = (() => {
                delete app[key];
            });

            expect(propertyEdit).toThrow(`Cannot assign to read only property '${key}' of object '[object Object]'`);
            expect(propertyRemove).toThrow(`Cannot delete property '${key}' of [object Object]`);
        });
    });

    test('переменная debug должна быть false по умолчанию в режиме отладки', () => {
        require('../debug.js');
        expect(window.debug).toBe(false);
    });

    describe('.run()', () => {
        test('должен выбрасывать ошибку при пустом значении параметра \'Class\'', () => {
            const runResult = (() => app.run());

            expect(runResult).toThrow('missing parameter \'Class\'');
        });

        test('должен выбрасывать ошибку при пустом значении параметра \'prop\'', () => {
            const runResult = (() => app.run('Ajax'));

            expect(runResult).toThrow('missing parameter \'prop\'');
        });

        test('должен корректно вызывать статический метод класса', () => {
            const AppColor = require('Components/color/sync.js').default;
            addModule('color', AppColor);

            const runResult = app.run('Color', 'type');

            expect(runResult).toBeNull();
        });

        test('должен корректно передавать параметры в вызов статического метода класса', () => {
            const AppColor = require('Components/color/sync.js').default;
            addModule('color', AppColor);

            const runResult = app.run('Color', 'type', '#999');

            expect(runResult).toBe('hex');
        });

        test('должен корректно вызывать статическое свойство класса', () => {
            const AppDate = require('Components/date/sync.js').default;
            addModule('date', AppDate);

            const runResult = app.run('Date', 'datePattern');

            expect(runResult).toBe('dd.mm.yyyy');
        });
    });

    describe('.init()', () => {
        test('должен выбрасывать ошибку при пустом значении параметра \'Class\'', () => {
            const initResult = (() => app.init());

            expect(initResult).toThrow('missing parameter \'Class\'');
        });

        test('должен выбрасывать ошибку при пустом значении параметра \'element\'', () => {
            const initResult = (() => app.init('Ajax'));

            expect(initResult).toThrow('missing parameter \'element\'');
        });

        test('должен инициализировать новый модуль с переданными опциями', () => {
            const AppColor = require('Components/color/sync.js').default;
            addModule('color', AppColor);

            const initResult = app.init('Color', '#999', {
                lightColor: 100
            });

            expect(initResult.options.lightColor).toBe(100);
        });

        test('должен инициализировать новый модуль без указания элемента, если он не обязателен', () => {
            const AppForm = require('Components/form/sync.js').default;
            const ajaxInit = require('Components/ajax/sync.js').default;
            const AppAjax = ajaxInit(AppForm);
            addModule('ajax', AppAjax);

            const initResult = app.init('Ajax', {
                testOption: true
            });

            expect(initResult.options.testOption).toBe(true);
        });

        test('должен корректно устанавливать название модуля у свойства \'modules\' элемента', () => {
            const AppColor = require('Components/color/sync.js').default;
            addModule('color', AppColor);

            const colorDiv = document.createElement('div');
            const initResult = app.init('Color', colorDiv, 'custom-color-module');

            expect(colorDiv.modules['custom-color-module']).toBe(initResult);
        });

        test('должен инициализировать модуль без расширения класса, если вместо опций передано название модуля', () => {
            const initResult = app.init('Module', document, 'some-module');

            expect(document.modules['some-module']).toBe(initResult);
        });
    });

    describe('.extend()', () => {
        test('должен выбрасывать ошибку при пустом значении параметра \'Class\'', () => {
            const extendResult = (() => app.extend());

            expect(extendResult).toThrow('missing parameter \'Class\'');
        });

        test('должен выбрасывать ошибку при пустом значении параметра \'ExtendClass\'', () => {
            const extendResult = (() => app.extend('TestSlider'));

            expect(extendResult).toThrow('missing parameter \'ExtendClass\'');
        });

        test('должен корректно устанавливать прототип нового класса', () => {
            const AppDate = require('Components/date/sync.js').default;

            const NewDate = app.extend('NewDate', AppDate);

            expect(NewDate.prototype).toBeInstanceOf(AppDate);
        });

        test('должен корректно устанавливать название класса у элементов экземпляров новых классов', () => {
            const AppColor = require('Components/color/sync.js').default;

            const NewColor = app.extend('NewColor', AppColor);
            const testDiv = document.createElement('div');
            new NewColor(testDiv); /* eslint-disable-line no-new */

            expect(testDiv.modules.newColor).toBeInstanceOf(NewColor);
        });

        test('должен вызвать установленную функцию обратного вызова при инициализации экземпляра класса', () => {
            const AppDate = require('Components/date/sync.js').default;

            const callbackFunction = jest.fn();
            const NewDate = app.extend('NewDate2', AppDate, () => callbackFunction());
            new NewDate(new Date()); /* eslint-disable-line no-new */

            expect(callbackFunction).toHaveBeenCalledWith();
        });

        test('должен препятствовать изменению или удалению добавленного класса из глобальной переменной', () => {
            const AppDate = require('Components/date/sync.js').default;

            app.extend('NewDate3', AppDate);

            const classEdit = (() => {
                app.NewDate3 = null;
            });
            const classRemove = (() => {
                delete app.NewDate3;
            });

            expect(classEdit).toThrow('Cannot assign to read only property \'NewDate3\' of object \'[object Object]\'');
            expect(classRemove).toThrow('Cannot delete property \'NewDate3\' of [object Object]');
        });
    });
});