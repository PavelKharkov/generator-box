/* eslint-disable unicorn/consistent-function-scoping */

import '../test';

let app;
let addModule;

describe('app', () => {
    test('должна выводить сообщение отладчика', () => {
        window.debug = true;
        const appRequires = require('../app.js');
        app = appRequires.app;
        addModule = appRequires.addModule;
        require('../init-globals.js');
        expect(window.console.log).toHaveBeenCalledWith('window.app initialized', 'danger');
        window.debug = false;
    });

    test('должна быть доступна как глобальная переменная', () => {
        expect(window.app).toBeDefined();
    });

    describe('addModule', () => {
        test('должен выбрасывать ошибку при пустом параметре \'moduleName\'', () => {
            const addModuleResult = (() => addModule());

            expect(addModuleResult).toThrow('missing parameter \'moduleName\'');
        });

        test('должен выбрасывать ошибку при пустом параметре \'Class\'', () => {
            const addModuleResult = (() => addModule('module'));

            expect(addModuleResult).toThrow('missing parameter \'Class\'');
        });

        test('должен добавлять метод \'on\' в глобальную переменную приложения', () => {
            const onFunction = app.on;

            expect(typeof onFunction).toBe('function');
        });

        test('должен выводить сообщение отладчика', () => {
            window.debug = true;
            addModule('testModule1', null);

            jest.advanceTimersByTime(1);
            expect(window.console.log).toHaveBeenCalledWith('module \'TestModule1\' added', 'danger');
            window.debug = false;
        });

        test('должен вызывать событие добавления модуля', () => {
            const initFunction = jest.fn();
            app.on('TestModule2', () => initFunction());
            addModule('testModule2', null);

            jest.advanceTimersByTime(1);

            expect(initFunction).toHaveBeenCalledWith();
        });

        test('должен удалять событие инициализации модуля после вызова события', () => {
            const initFunction = jest.fn();
            app.on('TestModule3', () => initFunction());
            addModule('testModule3', null);
            addModule('testModule4', null);

            jest.advanceTimersByTime(1);

            expect(initFunction).toHaveBeenCalledTimes(1);
        });
    });
});