//Сохранение подменяемых методов и свойств
const elementMatches = Element.prototype.matches;
const elementClosest = Element.prototype.closest;
const appendElementTypes = [
    'Element',
    'Document',
    'DocumentFragment'
];
const appendMethods = appendElementTypes.map(elementType => {
    const append = window[elementType].prototype.append;
    return append;
});
const removeElementTypes = [
    'Element',
    'CharacterData',
    'DocumentType'
];
const removeMethods = removeElementTypes.map(elementType => {
    const remove = window[elementType].prototype.remove;
    return remove;
});
const classListReplace = DOMTokenList.prototype.replace;
const classListAdd = DOMTokenList.prototype.add;
const classListRemove = DOMTokenList.prototype.remove;
const customEventFunc = window.CustomEvent;

//Обнуление подменяемых методов и свойств
Element.prototype.matches = 'already initialized';
Element.prototype.closest = 'already initialized';
Element.prototype.append = 'already initialized';
Document.prototype.append = 'already initialized';
DocumentFragment.prototype.append = 'already initialized';
Element.prototype.remove = 'already initialized';
CharacterData.prototype.remove = 'already initialized';
DocumentType.prototype.remove = 'already initialized';
window.DOMTokenList.prototype.add = 'already initialized';
window.DOMTokenList.prototype.remove = 'already initialized';
window.DOMTokenList.prototype.replace = 'already initialized';
window.CustomEvent = function() {
    return 'already initialized';
};

require('../polyfills.js');

describe('polyfills', () => {
    afterAll(() => {
        //Восстановление подменяемых методов и свойств
        Element.prototype.matches = elementMatches;
        delete Element.prototype.msMatchesSelector;
        Element.prototype.closest = elementClosest;
        appendElementTypes.forEach((elementType, index) => {
            window[elementType].prototype.append = appendMethods[index];
        });
        removeElementTypes.forEach((elementType, index) => {
            window[elementType].prototype.remove = removeMethods[index];
        });
        DOMTokenList.prototype.replace = classListReplace;
        DOMTokenList.prototype.add = classListAdd;
        DOMTokenList.prototype.remove = classListRemove;
        window.CustomEvent = customEventFunc;
    });

    describe('должна игнорироваться инициализация полифилов при существовании соответствующего свойства', () => {
        test('должна игнорироваться инициализация полифила \'Element.prototype.matches\', если данное свойство существует', () => {
            expect(Element.prototype.matches).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'Element.prototype.closest\', если данное свойство существует', () => {
            expect(Element.prototype.closest).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'Element.prototype.append\', если данное свойство существует', () => {
            expect(Element.prototype.append).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'Document.prototype.append\', если данное свойство существует', () => {
            expect(Document.prototype.append).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'DocumentFragment.prototype.append\', если данное свойство существует', () => {
            expect(DocumentFragment.prototype.append).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'Element.prototype.remove\', если данное свойство существует', () => {
            expect(Element.prototype.remove).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'CharacterData.prototype.remove\', если данное свойство существует', () => {
            expect(CharacterData.prototype.remove).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'DocumentType.prototype.remove\', если данное свойство существует', () => {
            expect(DocumentType.prototype.remove).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифилов \'DOMTokenList.classList.add\', если данное свойство существует', () => {
            expect(window.DOMTokenList.prototype.add).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифилов \'DOMTokenList.classList.remove\', если данное свойство существует', () => {
            expect(window.DOMTokenList.prototype.remove).toBe('already initialized');
        });

        test('должна игнорироваться инициализация полифила \'CustomEvent\', если данное свойство существует', () => {
            expect(window.CustomEvent()).toBe('already initialized');
        });
    });
});