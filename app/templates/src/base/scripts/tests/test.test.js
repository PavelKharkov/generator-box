/* eslint-disable unicorn/consistent-function-scoping */

import {
    isInstance,
    throwsError,
    dataTypes,
    yieldDataTypesCheck,
    handleDisabled
} from '../test';

describe('утилиты для тестирования', () => {
    test('isInstance', () => {
        expect(isInstance).toBe('__IS_INSTANCE__');
    });

    test('throwsError', () => {
        expect(throwsError).toBe('__THROWS_ERROR__');
    });

    describe('dataTypes', () => {
        test('должен содержать корректное количество сравниваемых типов', () => {
            expect(dataTypes).toHaveLength(21);
        });

        test('должен содержать только ассоциативные коллекции', () => {
            const dataTypesMap = new Map(dataTypes);
            expect(dataTypesMap).toBeInstanceOf(Map);
        });
    });

    describe('yieldDataTypesCheck', () => {
        test('должен выбрасывать ошибку при вызове без параметров', () => {
            const checkResult = (() => yieldDataTypesCheck());

            expect(checkResult).toThrow('parameter \'options.checkFunction\' must be a function');
        });

        test('должен выбрасывать ошибку при некорректном типе параметра \'options\'', () => {
            const checkResult = (() => yieldDataTypesCheck([]));

            expect(checkResult).toThrow('parameter \'options\' must be a plain object');
        });

        describe('некорректные типы опций', () => {
            /* eslint-disable array-bracket-newline */
            const checkOptions = [
                ['checkFunction', 'число', 1, 'parameter \'options.checkFunction\' must be a function'],
                ['dataTypesAssertions', 'объект', {}, 'parameter \'options.dataTypesAssertions\' must be a array'],
                ['describeMessage', 'число', 1, 'parameter \'options.describeMessage\' must be a string'],
                ['setupFunction', 'число', 1, 'parameter \'options.setupFunction\' must be a function']
            ];
            /* eslint-enable array-bracket-newline */
            test.each(checkOptions)('должен выбрасывать ошибку, если получает некорректный тип опции \'%s\' (%s)',
                (optionName, paramType, value, errorMessage) => { /* eslint-disable-line max-params */
                    const checkConfig = {
                        dataTypesAssertions: [],
                        checkFunction() {
                            return null;
                        }
                    };
                    checkConfig[optionName] = value;
                    const checkResult = (() => yieldDataTypesCheck(checkConfig));

                    expect(checkResult).toThrow(errorMessage);
                });
        });

        test('должен делать проверку значения', () => {
            const describeFunc = jest.fn();
            const checkConfig = {
                dataTypesAssertions: [],
                checkFunction() {
                    return null;
                },
                describeFunc
            };
            yieldDataTypesCheck(checkConfig);

            expect(describeFunc).toHaveBeenCalledWith('проверка типов', expect.any(Function));
        });
    });

    describe('handleDisabled', () => {
        test('должен выбрасывать ошибку при пустом параметре', () => {
            const requireResult = (() => handleDisabled());
            expect(requireResult).toThrow('missing parameter \'requireFunction\'');
        });

        test('должен выбрасывать ошибку, если тип параметра не является функцией', () => {
            const requireResult = (() => handleDisabled('abc'));
            expect(requireResult).toThrow('parameter \'requireFunction\' must be a function');
        });

        test('должен корректно возвращать отключённые модули', () => {
            const requireResult = handleDisabled(() => {
                return require('Components/color').default;
            });
            expect(requireResult).toBeInstanceOf(Promise);
        });
    });

    describe('setup', () => {
        //TODO:createRange, customElements, offsetParent, window.console
    });
});