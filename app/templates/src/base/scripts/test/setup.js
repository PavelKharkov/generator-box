const isNullOrUndefined = variable => {
    return variable == null; /* eslint-disable-line no-eq-null, eqeqeq */
};

//Имитация offsetParent
Object.defineProperty(HTMLElement.prototype, 'offsetParent', {
    get() {
        let element = this; /* eslint-disable-line consistent-this, unicorn/no-this-assignment */
        while (
            !isNullOrUndefined(element) &&
            (isNullOrUndefined(element.style) ||
            isNullOrUndefined(element.style.display) ||
            (element.style.display.toLowerCase() !== 'none'))
        ) {
            element = element.parentNode;
        }

        if (!isNullOrUndefined(element)) {
            return null;
        }
        if (!isNullOrUndefined(this.style) && !isNullOrUndefined(this.style.position) && (this.style.position.toLowerCase() === 'fixed')) {
            return null;
        }
        if ((this.tagName.toLowerCase() === 'html') || (this.tagName.toLowerCase() === 'body')) {
            return null;
        }

        return this.parentNode;
    }
});

//Имитация XMLHttpRequest
const xhrMockClass = () => ({
    open: jest.fn(),
    send: jest.fn(),
    setRequestHeader: jest.fn(),
    onreadystatechange: jest.fn(),
    readyState: 4,
    status: 4
});
jest.spyOn(window, 'XMLHttpRequest').mockImplementation(xhrMockClass);

//Имитация customElements
window.customElements = {
    define: jest.fn()
};

//Имитация createRange
window.document.createRange = () => ({
    setStart: jest.fn(),
    setEnd: jest.fn(),
    commonAncestorContainer: {
        nodeName: 'BODY',
        ownerDocument: document
    }
});

//Имитация методов консоли
window.console = {
    log: jest.fn(),
    warn: jest.fn(),
    error: jest.fn()
};

//Имитация метода для вывода сообщений отладчика
window.log = (...args) => {
    window.console.log(...args);
};

//Запуск всех внутренних таймеров
jest.useFakeTimers();