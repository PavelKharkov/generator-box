/* eslint-disable jest/no-conditional-in-test */

import util from 'Layout/main';

//Указатель тестирования на прототип экземпляра для функции проверки типов
const isInstance = '__IS_INSTANCE__';

//Указатель тестирования на выбрасывание ошибки для функции проверки типов
const throwsError = '__THROWS_ERROR__';

//Ассоциативная коллекция типов переменных для проверки
const dataTypes = [
    ['true', true],
    ['false', false],
    ['Array', []],
    ['Object', {}],
    ['zero', 0],
    ['number', 1],
    ['float number', 0.5],
    ['negative number', -1],
    ['NaN', Number.NaN],
    ['string number', '1'],
    ['empty string', ''],
    ['space', ' '],
    ['string', 'abc'],
    ['HTML string', '<div></div>'],
    ['Function', () => {}], /* eslint-disable-line no-empty-function */
    ['Date', new Date(2019, 10, 1)],
    ['Regex', /regex/],
    ['HTMLElement', document.createElement('div')],
    ['Error', new Error()], /* eslint-disable-line unicorn/error-message */
    ['null', null],
    ['undefined', undefined] /* eslint-disable-line no-undefined */

    //TODO:check new types
    //['Self-closing HTMLelement', document.createElement('br')],
    //['Text node', document.createTextNode('abc')],
    //['Map', new Map()],
    //['Set', new Set()],
    //['BigInt', 1n],
    //['Infinity', Number.POSITIVE_INFINITY]
];

const defaultTypesCheckOptions = {
    dataTypesAssertions: [],
    describeMessage: 'проверка типов'
};

/**
 * @function yieldDataTypesCheck
 * @desc Проводит тестирование различных типов параметра функции.
 * @param {Object} [options={}] - Опции проверки типов.
 * @param {Array.<Array>} [options.dataTypesAssertions=[]] - Ассоциативная коллекция типов передаваемых переменных, отличающихся от возвращаемого значения по умолчанию.
 * @param {*} [options.defaultValue] - Возвращаемое значение проверяемого выражения по умолчанию.
 * @param {Function} options.checkFunction - Тестируемое выражение. Имеет два параметра: результат настроечной функции и текущее значение для проверки типа. Тестируется возвращаемое значение функции.
 * @param {Function} [options.setupFunction] - Настроечная функция. Возвращаемое значение передаётся в функцию для тестирования.
 * @param {string} [options.describeMessage='проверка типов'] - Сообщение блока describe.
 * @param {boolean} [options.only] - Запускать ли тесты через модификатор only.
 * @returns {undefined}
 * @ignore
 */
const yieldDataTypesCheck = (options = {}) => {
    if (!util.isObject(options)) {
        util.typeError(options, 'options', 'plain object');
    }

    util.defaultsDeep(options, defaultTypesCheckOptions);

    const {
        checkFunction,
        dataTypesAssertions,
        defaultValue,
        describeMessage,
        setupFunction,
        only
    } = options;

    if (typeof checkFunction !== 'function') {
        util.typeError(checkFunction, 'options.checkFunction', 'function');
    }
    if (!Array.isArray(dataTypesAssertions)) {
        util.typeError(dataTypesAssertions, 'options.dataTypesAssertions', 'array');
    }
    if (typeof describeMessage !== 'string') {
        util.typeError(describeMessage, 'options.describeMessage', 'string');
    }
    if (typeof setupFunction !== 'function') {
        util.typeError(setupFunction, 'options.setupFunction', 'function');
    }

    //Создание объекта со списком проверок типов
    const typesAssertionsObject = {};
    dataTypesAssertions.forEach(typesAssertion => {
        const [assertionType, expectValue] = typesAssertion;
        if (Array.isArray(assertionType)) {
            return assertionType.forEach(assertionTypeInner => {
                typesAssertionsObject[assertionTypeInner] = expectValue;
            });
        }

        typesAssertionsObject[assertionType] = expectValue;
        return null;
    });

    let describeFunc = options.describeFunc || describe;
    if (only) {
        describeFunc = describeFunc.only;
    }

    describeFunc(describeMessage, () => {
        let setupResult;
        if (typeof setupFunction === 'function') {
            setupResult = setupFunction();
        }

        dataTypes.forEach(dataType => {
            const [typeName, value] = dataType;
            const expected = Object.prototype.hasOwnProperty.call(typesAssertionsObject, typeName)
                ? typesAssertionsObject[typeName]
                : defaultValue;

            const expectedArray = Array.isArray(expected);
            let expectedInstanceOf;
            let expectedError;
            const message = (() => {
                if (expectedArray) {
                    expectedInstanceOf = (expected[0] === isInstance);
                    if (expectedInstanceOf) {
                        return `должен иметь корректный прототип, если получает '${typeName}'`;
                    }

                    expectedError = (expected[0] === throwsError);
                    if (expectedError) {
                        return `должен выбрасывать ошибку, если получает '${typeName}'`;
                    }
                }

                return `должен возвращать '${expected}', если получает '${typeName}'`;
            })();

            /* eslint-disable jest/no-conditional-expect */
            test(message, () => { /* eslint-disable-line jest/require-top-level-describe, jest/valid-title */
                if (expectedError) {
                    const checkedValue = (() => checkFunction(setupResult, value));
                    expect(checkedValue).toThrow(expected[1]);
                    return;
                }

                const checkedValue = checkFunction(setupResult, value);
                if (expectedInstanceOf) {
                    expect(checkedValue).toBeInstanceOf(expected[1]);
                } else {
                    expect(checkedValue).toBe(expected);
                }
            });
            /* eslint-enable jest/no-conditional-expect */
        });
    });
};

/**
 * @function handleDisabled
 * @desc Импортирует модуль, выключённый с помощью webpack-переменной __IS_DISABLED_MODULE__.
 * @param {Function} requireFunction - Функция, возвращающая импортируемый модуль.
 * @returns {*} Результат возвращёного модуля.
 * @ignore
 */
const handleDisabled = (requireFunction = util.required('requireFunction')) => {
    if (typeof requireFunction !== 'function') {
        util.typeError(requireFunction, 'requireFunction', 'function');
    }

    /* eslint-disable no-underscore-dangle */
    window.__IS_DISABLED_MODULE__ = true;
    const requiredModule = requireFunction();
    window.__IS_DISABLED_MODULE__ = false;
    /* eslint-enable no-underscore-dangle */

    return requiredModule;
};

/* eslint-disable-next-line jest/no-export */
export {
    isInstance,
    throwsError,
    dataTypes,
    yieldDataTypesCheck,
    handleDisabled
};