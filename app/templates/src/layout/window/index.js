import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, onInit, emitInit} from 'Base/scripts/app.js';

const moduleName = 'window';

//Опции

//Селекторы и классы
const loadedClass = 'window-loaded';
const hasScrollbarClass = 'has-scrollbar';

const lazyloadInitializedClass = 'lazyload-initialized';
const lazyloadedClass = 'lazyloaded';

const lazyloadData = 'lazyload';

//Другие опции
const html = document.documentElement;
const body = document.body;

const loadTimeout = 5000;

/**
 * @function AppWindow
 * @namespace layout.AppWindow
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль взаимодействия с window.
 */
const AppWindow = immutable(new Module(
    window,

    /**
     * @desc Функция-обёртка.
     * @this AppWindow
     * @returns {undefined}
     */
    function() {
        const el = this.el;

        let initialized = false;

        /**
         * @member {boolean} AppWindow.initialized
         * @memberof layout
         * @desc Указывает, что уже было вызвано событие deferredInit.
         * @readonly
         */
        Object.defineProperty(this, 'initialized', {
            enumerable: true,
            get: () => initialized
        });

        let loaded = false;

        /**
         * @member {boolean} AppWindow.loaded
         * @memberof layout
         * @desc Указывает, произошло ли событие window.load.
         * @readonly
         */
        Object.defineProperty(this, 'loaded', {
            enumerable: true,
            get: () => loaded
        });

        //Действия по window.load
        /**
         * @event AppWindow.load
         * @memberof layout
         * @desc Событие, вызываемое при вызове браузерного события window.load. Также вызывается спустя определённое время ожидания (по умолчанию, 5 секунд).
         * @returns {undefined}
         * @example
         * app.window.on('load', () => {
         *     console.log('Страница полностью загружена');
         * });
         */
        this.onSubscribe('load', () => {
            if (!__IS_DIST__ || __IS_DEBUG__) {
                if (window.debug) window.log('window.load emitted', 'danger');
            }

            loaded = true;
            html.classList.add(loadedClass);
            util.defer(() => {
                this.off('load');
            });
        });

        if (document.readyState === 'complete') {
            this.emit('load');
        } else {
            el.addEventListener('load', () => {
                this.emit('load');
            });
            setTimeout(() => {
                if (!loaded) this.emit('load');
            }, loadTimeout);
        }

        /**
         * @function AppWindow.onload
         * @memberof layout
         * @desc Проверяет, произошло ли событие load, тогда вызывает функцию обратного вызова, либо вызывает функцию обратного вызова при загрузке страницы.
         * @param {Function} callback - Функция обратного вызова.
         * @param {*} args - Параметры, передающиеся функции обратного вызова.
         * @fires layout.AppWindow.load
         * @returns {undefined}
         * @readonly
         * @example
         * app.window.onload(data => {
         *     console.log('Страница полностью загружена');
         *     console.log(data); //Выведет '123'
         * }, 123);
         */
        Object.defineProperty(this, 'onload', {
            enumerable: true,
            value(callback, ...args) {
                if (typeof callback !== 'function') util.typeError(callback, 'callback', 'function');

                if (loaded) {
                    callback(...args);
                    return;
                }
                this.on('load', () => callback(...args));
            }
        });

        //TODO:onInitialized method

        const viewport = el.visualViewport;

        /**
         * @function setWindowHeight
         * @desc Устанавливает css-переменную для высоты экрана на мобильных телефонах.
         * @returns {undefined}
         * @ignore
         */
        const setWindowHeight = () => {
            setTimeout(() => {
                util.setStyle('--window-height', `${viewport ? viewport.height : window.innerHeight}px`);
            }, 100);
        };

        //Прокрутка
        /**
         * @function AppWindow.scrollY
         * @memberof layout
         * @desc Возвращает текущую позицию прокрутки.
         * @returns {number} Текущая позиция прокрутки.
         * @readonly
         * @example
         * console.log(app.window.scrollY());
         */
        Object.defineProperty(this, 'scrollY', {
            enumerable: true,
            value() {
                return html.scrollTop || body.scrollTop || 0;
            }
        });

        let prevScroll = 0;

        /**
         * @member {number} AppWindow.prevScroll
         * @memberof layout
         * @desc Предыдущая позиция прокрутки.
         * @readonly
         */
        Object.defineProperty(this, 'prevScroll', {
            enumerable: true,
            get: () => prevScroll
        });

        let htmlAnimate;
        let bodyAnimate;

        /**
         * @member {Object} AppWindow.htmlAnimate
         * @memberof layout
         * @desc Ссылка на модуль анимации [AppAnimation]{@link components.AppAnimation} тега html.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'htmlAnimate', {
            enumerable: true,
            get: () => htmlAnimate
        });

        /**
         * @member {Object} AppWindow.bodyAnimate
         * @memberof layout
         * @desc Ссылка на модуль анимации [AppAnimation]{@link components.AppAnimation} тега body.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'bodyAnimate', {
            enumerable: true,
            get: () => bodyAnimate
        });

        const initAnimation = AppAnimation => {
            htmlAnimate = new AppAnimation(html);
            bodyAnimate = new AppAnimation(body);
        };

        if (__IS_SYNC__) {
            require('Components/animation').default.then(AppAnimation => {
                initAnimation(AppAnimation);
            });
        } else {
            onInit('animation', () => {
                import(/* webpackChunkName: "helpers" */ 'Components/animation/sync.js')
                    .then(modules => {
                        const AppAnimation = modules.default;
                        initAnimation(AppAnimation);
                    });
            });

            this.onload(() => {
                emitInit('animation');
            });
        }

        this.on({
            /**
             * @event AppWindow.scrollTop
             * @memberof layout
             * @desc Прокучивает окно браузера до определеннной позиции.
             * @param {Object} [options={}] - Опции.
             * @param {number} [options.value=0] - Конечная позиция прокрутки окна.
             * @param {number} [options.duration=0] - Длительность анимации прокрутки.
             * @param {Function} [options.easing] - Функция плавности анимации прокрутки.
             * @param {Function} [options.callback] - Функция обратного вызова после завершения анимации прокрутки. В функцию передаются опции вызова события.
             * @fires components.AppAnimation#animate
             * @returns {undefined}
             * @example
             * app.window.on('scrollTop', options => {
             *     console.log(`Страница была прокручена до позиции ${options.value}`);
             * });
             * app.window.scrollTop({
             *     value: 100,
             *     duration: 300,
             *     easing(value) {
             *         return value ** 2; //easeInQuad
             *     },
             *     callback(options) {
             *         console.log(`Страница была прокручена до позиции ${options.value}`);
             *     }
             * });
             */
            scrollTop(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                const {
                    value = 0,
                    duration = 0,
                    easing,
                    callback
                } = options;
                let debouncedCallback;
                if (typeof callback === 'function') {
                    debouncedCallback = util.debounce(() => callback(options), 0);
                }

                const animateOptions = {
                    easing,
                    value: Number(value),
                    property: 'scrollTop',
                    duration: Number(duration)
                };

                if (htmlAnimate) {
                    const htmlAnimation = htmlAnimate.animate(animateOptions);
                    if (htmlAnimation && debouncedCallback) htmlAnimation.resolve(debouncedCallback);
                } else {
                    html.scrollTop = animateOptions.value;
                }

                if (bodyAnimate) {
                    const bodyAnimation = bodyAnimate.animate(animateOptions);
                    if (bodyAnimation && debouncedCallback) bodyAnimation.resolve(debouncedCallback);
                } else {
                    body.scrollTop = animateOptions.value;
                }
            }
        });

        /**
         * @event AppWindow.scroll
         * @memberof layout
         * @desc Вызывается при прокрутке основной области окна.
         * @param {Object} event - Браузерное событие прокрутки окна.
         * @returns {undefined}
         * @example
         * app.window.on('scroll', event => {
         *     console.log('Страница была прокручена');
         *     console.log(event); //Браузерное событие прокрутки
         * });
         * app.window.emit('scroll'); //Принудительный фейковый вызов события прокрутки
         */
        this.onSubscribe('scroll', () => {
            //Сохранение текущей позиции прокрутки
            prevScroll = this.scrollY();

            setWindowHeight();
        });
        /* eslint-enable jsdoc/check-param-names */
        el.addEventListener('scroll', event => this.emit('scroll', event));

        //Методы для вычисления размеров окна
        /**
         * @function AppWindow.width
         * @memberof layout
         * @desc Возвращает ширину окна браузера не включая ширину вертикальной полосы прокрутки.
         * @returns {number} Ширина окна браузера не включая ширину вертикальной полосы прокрутки.
         * @readonly
         * @example
         * console.log(app.window.width());
         */
        Object.defineProperty(this, 'width', {
            enumerable: true,
            value() {
                return html.clientWidth;
            }
        });

        /**
         * @function AppWindow.outerWidth
         * @memberof layout
         * @desc Возвращает ширину окна браузера включая ширину вертикальной полосы прокрутки.
         * @returns {number} Ширина окна браузера включая ширину вертикальной полосы прокрутки.
         * @readonly
         * @example
         * console.log(app.window.outerWidth());
         */
        Object.defineProperty(this, 'outerWidth', {
            enumerable: true,
            value() {
                return window.outerWidth;
            }
        });

        /**
         * @function AppWindow.height
         * @memberof layout
         * @desc Возвращает высоту окна браузера не включая высоту горизонтальной полосы прокрутки.
         * @returns {number} Высота окна браузера не включая высоту горизонтальной полосы прокрутки.
         * @readonly
         * @example
         * console.log(app.window.height());
         */
        Object.defineProperty(this, 'height', {
            enumerable: true,
            value() {
                return html.clientHeight;
            }
        });

        /**
         * @function AppWindow.outerHeight
         * @memberof layout
         * @desc Возвращает высоту окна браузера включая высоту горизонтальной полосы прокрутки.
         * @returns {number} Высота окна браузера включая высоту горизонтальной полосы прокрутки.
         * @readonly
         * @example
         * console.log(app.window.outerHeight());
         */
        Object.defineProperty(this, 'outerHeight', {
            enumerable: true,
            value() {
                return window.outerHeight;
            }
        });

        /**
         * @function AppWindow.scrollbarWidth
         * @memberof layout
         * @desc Возвращает ширину вертикальной полосы прокрутки (соответствует высоте горизонтальной полосе).
         * @returns {number} Ширина полосы прокрутки.
         * @readonly
         */
        Object.defineProperty(this, 'scrollbarWidth', {
            enumerable: true,
            value() {
                return this.outerWidth() - this.width();
            }
        });

        const breakpoints = [
            util.media.lg,
            util.media.md,
            util.media.sm,
            0
        ];

        /**
         * @member {Array.<number>} AppWindow.breakpoints
         * @memberof layout
         * @desc Коллекция со списком пограничных значений ширины окна браузера, используемых в скриптах.
         * @readonly
         */
        Object.defineProperty(this, 'breakpoints', {
            enumerable: true,
            value: breakpoints
        });

        let currentBreakpoint;

        /**
         * @member {number} AppWindow.currentBreakpoint
         * @memberof layout
         * @desc Нижняя граница текущего диапазона ширины экрана.
         * @readonly
         */
        Object.defineProperty(this, 'currentBreakpoint', {
            enumerable: true,
            get: () => currentBreakpoint
        });

        /**
         * @function breakpointChange
         * @desc Изменяет текущее значение диапазона ширины.
         * @param {number} newBreakpoint - Новое пограничное значение ширины окна браузера.
         * @returns {undefined}
         * @ignore
         */
        const breakpointChange = newBreakpoint => {
            //Сохраняет текущую нижнюю границу диапазона ширины в свойство модуля
            currentBreakpoint = newBreakpoint;
        };

        /**
         * @function AppWindow.countBreakpoint
         * @desc Вычисляет нижнюю границу текущего диапазона ширины окна браузера.
         * @param {number} [screenWidth=AppWindow.width()] - Ширина окна браузера.
         * @returns {number} Текущая нижняя граница диапазона ширины.
         * @readonly
         * @example
         * console.log(app.window.countBreakpoint(800)); //768
         */
        Object.defineProperty(this, 'countBreakpoint', {
            enumerable: true,
            value(screenWidth = this.width()) {
                return breakpoints.find(breakpoint => {
                    return screenWidth >= breakpoint;
                });
            }
        });

        //Изменение размеров окна
        /**
         * @event AppWindow.resize
         * @memberof layout
         * @desc Вызывается при изменении размеров окна браузера.
         * @param {Object} event - Браузерное событие изменения размера окна браузера.
         * @returns {undefined}
         * @example
         * app.window.on('resize', event => {
         *     console.log('Размеры окна браузера были изменены');
         *     console.log(event); //Браузерное событие изменения размеров окна браузера
         * });
         * app.window.emit('resize'); //Принудительный фейковый вызов события изменения размеров окна браузера
         */
        this.onSubscribe('resize', () => {
            //Определение и сохранение текущего диапазона ширины окна
            const newBreakpoint = this.countBreakpoint();
            if (newBreakpoint !== currentBreakpoint) breakpointChange(newBreakpoint);

            html.classList[(this.scrollbarWidth() > 0) ? 'add' : 'remove'](hasScrollbarClass);

            setWindowHeight();
        });
        if (viewport) {
            viewport.addEventListener('resize', event => this.emit('resize', event));
        } else {
            el.addEventListener('resize', event => this.emit('resize', event));
        }

        //Прокрутка колёсиком мыши
        let wheelActive;

        /**
         * @member {boolean} AppWindow.wheelActive
         * @memberof layout
         * @desc Указывает, происходит ли прокрутка колёсиком мыши в данный момент.
         * @readonly
         */
        Object.defineProperty(this, 'wheelActive', {
            enumerable: true,
            get: () => wheelActive
        });

        if (!util.isTouch) {
            wheelActive = false;

            /**
             * @function wheelInnerEvent
             * @desc Отложенный вызов события прокрутки колёсиком мыши.
             * @param {Object} event - Браузерное событие прокрутки колёсиком мыши.
             * @fires layout.AppWindow.wheel
             * @returns {undefined}
             * @ignore
             */
            const wheelInnerEvent = util.debounce(event => {
                const deltaValue = -event.deltaY || event.wheelDelta;
                const delta = (event.deltaMode === 1) ? (deltaValue * 32) : deltaValue;

                this.wheel(delta);
            }, 1);

            const wheelEventDelay = 300;

            /**
             * @event AppWindow.wheel
             * @memberof layout
             * @desc Событие, вызываемое при вызове отложенного события прокрутки колёсиком мыши.
             * @param {number} delta - Значение смещения прокрутки браузера при прокрутки колёсиком.
             * @returns {undefined}
             * @example
             * app.window.on('wheel', delta => {
             *     console.log(`Пользователь прокрутил страницу с помощью колёсика мыши (с задержкой) на ${delta}px`);
             * });
             * app.window.wheel(100);
             */
            this.on(['wheel']);

            /**
             * @event AppWindow.wheelOriginal
             * @memberof layout
             * @desc Событие, вызываемое при вызове браузерного события прокрутки колёсиком мыши.
             * @param {Object} event - Браузерный объект события прокрутки.
             * @fires layout.AppWindow.wheel
             * @returns {Object} Возвращает null, если событие активно в данный момент или возвращает браузерное событие прокрутки.
             * @example
             * app.window.on('wheelOriginal', event => {
             *     console.log('Пользователь прокрутил страницу с помощью колёсика мыши (без задержки)');
             *     console.log(event); //Браузерное событие прокрутки колёсиком мыши
             * });
             */
            this.onSubscribe('wheelOriginal', event => {
                if (wheelActive) return null;

                setTimeout(() => {
                    wheelActive = false;
                }, wheelEventDelay);
                wheelActive = true;

                wheelInnerEvent(event);
                return event;
            });
            el.addEventListener('wheel', event => {
                this.emit('wheelOriginal', event);
            });
        }

        /**
         * @event AppWindow.orientationchange
         * @memberof layout
         * @desc Событие, вызываемое при смене ориентации устройства.
         * @param {Object} event - Браузерное событие смены ориентации.
         * @returns {undefined}
         * @example
         * app.window.on('orientationchange', event => {
         *     console.log('Изменена ориентация устройства');
         *     console.log(event); //Браузерное событие изменения ориентации устойства
         * });
         */
        this.onSubscribe(['orientationchange']);
        window.addEventListener('orientationchange', event => {
            setWindowHeight();

            this.emit('orientationchange', event);
        });

        let observer;

        /**
         * @member {Object} AppWindow.observer
         * @memberof layout
         * @desc Экземпляр функции для отслеживания отложенной загрузки модулей при прокрутке страницы.
         * @readonly
         */
        Object.defineProperty(this, 'observer', {
            enumerable: true,
            get: () => observer
        });

        /**
         * @function initObserver
         * @desc Инициализирует объект для отслеживания видимости элементов на странице.
         * @returns {undefined}
         * @ignore
         */
        const initObserver = () => {
            if ('IntersectionObserver' in window) {
                /**
                 * @function observerCallback
                 * @desc Функция, вызываемая через IntersectionObserver при прокрутке страницы.
                 * @param {Array.<Object>} entries - Коллекция объектов с параметрами элементов IntersectionObserver.
                 * @returns {undefined}
                 * @ignore
                 */
                const observerCallback = entries => {
                    entries.forEach(entry => {
                        if (!entry.isIntersecting) return;

                        const target = entry.target;
                        observer.unobserve(target);
                        if (!(target.modules && target.modules[target.dataset[lazyloadData]])) return;

                        target.classList.add(lazyloadedClass);
                        target.modules[target.dataset[lazyloadData]].lazyload();
                    });
                };
                const observerConfig = {
                    rootMargin: '0px 0px 200px 0px',
                    threshold: 0.1
                };

                observer = new IntersectionObserver(observerCallback, observerConfig);
            }
        };

        /**
         * @function AppWindow.initLazyLoading
         * @memberof layout
         * @desc Инициализирует отложенную загрузку модулей по data-атрибуту.
         * @returns {undefined}
         * @readonly
         * @example
         * app.window.initLazyLoading();
         */
        Object.defineProperty(this, 'initLazyLoading', {
            enumerable: true,
            value: () => {
                const lazyloadEl = document.querySelectorAll(`[data-${lazyloadData}]:not(.${lazyloadInitializedClass})`);

                if (observer) {
                    lazyloadEl.forEach(element => {
                        element.classList.add(lazyloadInitializedClass);
                        observer.observe(element);
                    });
                    return;
                }

                lazyloadEl.forEach(element => {
                    if (!(element.modules && element.modules[element.dataset[lazyloadData]])) return;

                    element.classList.add(lazyloadedClass);
                    element.modules[element.dataset[lazyloadData]].lazyload();
                });
            }
        });

        /**
         * @event AppWindow.deferredInit
         * @memberof layout
         * @desc Отложенная инициализация, вызывается после загрузки всех синхронных модулей.
         * @fires layout.AppWindow.scroll
         * @fires layout.AppWindow.resize
         * @returns {undefined}
         * @example
         * console.log(app.window.initialized); //false
         * app.window.on('deferredInit', () => {
         *     console.log('Все синхронные модули были загружены');
         * });
         * console.log(app.window.initialized); //true
         */
        this.onSubscribe('deferredInit', () => {
            this.emit('scroll');
            this.emit('resize');
            setWindowHeight();
            initObserver();
            this.initLazyLoading();

            initialized = true;
        });
    },

    moduleName
));

addModule(moduleName, AppWindow);

export default AppWindow;
export {AppWindow};