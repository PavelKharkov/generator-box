let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const orderCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "order" */ './sync.js'))
        .then(modules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppOrder = modules.default;
            resolve(AppOrder);
        });
};

const importFunc = new Promise(resolve => {
    orderCallback(resolve);
});

export default importFunc;
//TODO:export {modules}