import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

let AppOrderAside;

//Опции
const moduleName = 'order';

const orderEl = document.querySelectorAll('.order');

const selectSelector = '.order__select';

/**
 * @class AppOrder
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppPopover
 * @requires components#AppPopup
 * @requires components#AppSelect
 * @requires layout#AppWindow
 * @classdesc Модуль формы заказа.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const orderInstance = new app.Order(document.querySelector('.order-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="order"></div>
 *
 * <!--.order - селектор по умолчанию-->
 */
const AppOrder = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Страница завершения заказа
        const orderFinishEl = el.querySelector('.order__finish');
        if (orderFinishEl) {
            require('./order-finish');
            return;
        }

        //Корзина
        let orderAside;

        /**
         * @member {Array.<Object>} AppOrder#orderAside
         * @memberof layout
         * @desc Коллекция ссылок на модули корзины [AppOrderAside]{@link layout.AppOrderAside} в оформлении заказа.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'orderAside', {
            enumerable: true,
            get: () => orderAside
        });

        let companySelect;

        /**
         * @member {Object} AppOrder#companySelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего меню [AppSelect]{@link components.AppSelect} для выбора юридического лица.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'companySelect', {
            enumerable: true,
            get: () => companySelect
        });

        let deliverySelect;

        /**
         * @member {Object} AppOrderSteps#deliverySelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего меню [AppSelect]{@link components.AppSelect} для выбора адреса доставки.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'deliverySelect', {
            enumerable: true,
            get: () => deliverySelect
        });

        const masks = [];

        /**
         * @member {Array.<Object>} AppOrder#masks
         * @memberof layout
         * @desc Коллекция ссылок на модули масок [AppMask]{@link layout.AppMask} в оформлении заказа.
         * @readonly
         */
        Object.defineProperty(this, 'masks', {
            enumerable: true,
            value: masks
        });

        let validation;

        /**
         * @member {Object} AppOrder#validation
         * @memberof layout
         * @desc Ссылка на модуль валидации [AppValidation]{@link layout.AppValidation} в оформлении заказа.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'validation', {
            enumerable: true,
            get: () => validation
        });

        const selects = [];

        /**
         * @member {Array.<Object>} AppOrder#selects
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих списков [AppSelect]{@link layout.AppSelect} в оформлении заказа.
         * @readonly
         */
        Object.defineProperty(this, 'selects', {
            enumerable: true,
            value: selects
        });

        const popups = [];

        /**
         * @member {Array.<Object>} AppOrder#popups
         * @memberof layout
         * @desc Коллекция ссылок на модули попапов [AppPopup]{@link layout.AppPopup} в оформлении заказа.
         * @readonly
         */
        Object.defineProperty(this, 'popups', {
            enumerable: true,
            value: popups
        });

        const reinitAside = () => {
            const orderAsideEl = document.querySelectorAll('.order__aside');
            if (orderAsideEl.length > 0) {
                orderAside = [...orderAsideEl].map(element => new AppOrderAside(element));
            }

            AppWindow.emit('resize');
            AppWindow.emit('scroll');
        };

        let AppMask;
        const reinitMasks = () => {
            const phoneInputs = el.querySelectorAll('input[data-validate~="tel"]');
            if (phoneInputs.length > 0) {
                phoneInputs.forEach(input => {
                    const changeMask = new AppMask(input, AppMask.rusPhoneOptions);
                    masks.push(changeMask);
                });
            }

            const changeInput = el.querySelector('.order__form-group.-change .order__input');
            if (changeInput) {
                const changeMask = new AppMask(changeInput, AppMask.numberOptions);
                masks.push(changeMask);

                const changeCheckbox = el.querySelector('.order__form-group.-change input[type="checkbox"]');
                let changeValue = changeInput.value;
                changeCheckbox.addEventListener('change', () => {
                    if (changeCheckbox.checked) {
                        changeValue = changeInput.value;
                        changeInput.value = '';
                        changeInput.setAttribute('value', '');
                        util.disable(changeInput);
                    } else {
                        changeInput.value = changeValue;
                        changeInput.setAttribute('value', changeValue);
                        util.enable(changeInput);
                    }
                });
            }
        };

        let AppValidation;
        const reinitValidation = () => {
            const form = el.querySelector('.form');
            validation = new AppValidation(form);
        };

        const reinitAgreementCheckboxes = () => {
            const agreementCheckboxes = [...el.querySelectorAll('.order__form-check.-agreement')];
            if (agreementCheckboxes.length > 0) {
                const submitButton = el.querySelector('button[type="submit"]');
                agreementCheckboxes.forEach(checkbox => {
                    checkbox.addEventListener('change', () => {
                        const disableSubmit = agreementCheckboxes.find(checkboxInner => {
                            return !checkboxInner.checked;
                        });
                        util[disableSubmit ? 'disable' : 'enable'](submitButton);
                    });
                });
            }
        };

        let AppSelect;
        const reinitSelects = () => {
            const companySelectEl = el.querySelector(`${selectSelector}.-company`);
            const deliverySelectEl = el.querySelector(`${selectSelector}.-delivery`);

            if (companySelectEl) {
                companySelect = new AppSelect(companySelectEl);
            }

            if (deliverySelectEl) {
                deliverySelect = new AppSelect(deliverySelectEl);
            }

            const orderSelectsEl = el.querySelectorAll('[data-select]');
            if (orderSelectsEl.length > 0) {
                selects.length = 0;
                orderSelectsEl.forEach(selectEl => {
                    const orderSelect = (selectEl.modules && selectEl.modules.select) || new AppSelect(selectEl);
                    selects.push(orderSelect);
                });
            }
        };

        let AppPopup;
        const reinitPopups = () => {
            const orderPopupsEl = el.querySelectorAll('[data-popup]');
            if (orderPopupsEl.length > 0) {
                popups.forEach(popupInstance => {
                    popupInstance.unlink();
                });
                popups.length = 0;
                orderPopupsEl.forEach(popupEl => {
                    const orderPopup = (popupEl.modules && popupEl.modules.popup) || new AppPopup(popupEl);
                    popups.push(orderPopup);
                });
            }
        };

        const reinit = callbackFunc => {
            reinitAside();
            reinitMasks();
            reinitValidation();
            reinitAgreementCheckboxes();
            reinitSelects();
            reinitPopups();

            if (callbackFunc) callbackFunc();
        };

        //Инициализация скриптов при загрузке
        AppOrderAside = require('./order-aside').default;
        reinitAside();

        const maskRequire = require('Components/mask');
        maskRequire.default.then(maskModule => {
            AppMask = maskModule;
            reinitMasks();
        });

        maskRequire.maskTrigger(el);

        const validationRequire = require('Components/validation');
        validationRequire.default.then(validationModule => {
            AppValidation = validationModule;
            reinitValidation();
        });

        validationRequire.validationTrigger(el);

        reinitAgreementCheckboxes();

        const selectRequire = require('Components/select');
        selectRequire.default.then(selectModule => {
            AppSelect = selectModule;
            reinitSelects();
        });

        selectRequire.selectTrigger(el);

        const popupRequire = require('Components/popup');
        popupRequire.default.then(popupModule => {
            AppPopup = popupModule;
            reinitPopups();
        });

        popupRequire.popupTrigger(el);

        /**
         * @function AppOrder#reinit
         * @memberof layout
         * @desc Переинициализирует скрипты формы страницы заказа.
         * @param {Function} [callbackFunc] - Функция обратного вызова.
         * @returns {undefined}
         * @readonly
         * @example
         * orderInstance.reinit(() => { //Нужно вызывать после перезагрузки элемента заказа аяксом
         *     submitHandlerReinit();
         * });
         */
        Object.defineProperty(this, 'reinit', {
            enumerable: true,
            value: reinit
        });
    }
});

addModule(moduleName, AppOrder);

orderEl.forEach(el => new AppOrder(el));

export default AppOrder;
export {
    AppOrder,
    AppOrderAside
};