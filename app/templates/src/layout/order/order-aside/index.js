//TODO:example

import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'orderAside';

const innerSelector = '.order-aside__inner';
const fixedClass = 'position-fixed';
const fixedBottomClass = 'fixed-bottom';

/**
 * @class AppOrderAside
 * @memberof layout
 * @requires layout#AppWindow
 * @classdesc Описание модуля.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - HTML-элемент экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 */
const AppOrderAside = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const fixedBlockMin = util.media.xl;
        const cartWrapper = document.querySelector('.order__cart-wrapper');

        AppWindow.on('resize', () => {
            if (AppWindow.width() < fixedBlockMin) {
                const inner = el.querySelector(innerSelector);
                if (inner) cartWrapper.append(inner);
                return;
            }

            const inner = cartWrapper.querySelector(innerSelector);
            if (inner) el.append(inner);
        });

        const fixedBlock = el.querySelector(innerSelector);
        const fixedBlockWrapper = el;
        const updateWidth = () => {
            fixedBlock.style.width = `${fixedBlockWrapper.offsetWidth - (Number.parseInt(util.getStyle('--global-padding'), 10) * 2)}px`;
        };

        //Перерисовка плавающего блока при изменении окна браузера
        AppWindow.on({
            load() {
                util.defer(() => {
                    updateWidth();
                });
            },

            scroll() {
                if (AppWindow.width() < fixedBlockMin) return;

                const fixedBlockHeight = fixedBlock.offsetHeight;
                const blockWrapperOffset = fixedBlockWrapper.getBoundingClientRect().top;
                const blockWrapperBottom = blockWrapperOffset + fixedBlockWrapper.offsetHeight - fixedBlockHeight;

                if (blockWrapperOffset < 0 && blockWrapperBottom > 0) {
                    fixedBlock.classList.remove(fixedBottomClass);
                    fixedBlock.classList.add(fixedClass);
                    return;
                }

                fixedBlock.classList.remove(fixedClass);
                if (blockWrapperBottom <= 0 && Math.abs(blockWrapperBottom) <= fixedBlockHeight) {
                    fixedBlock.classList.add(fixedBottomClass);
                }
            },

            resize() {
                if (AppWindow.width() < fixedBlockMin) return;

                updateWidth();
            }
        });
    }
});

addModule(moduleName, AppOrderAside);

export default AppOrderAside;
export {AppOrderAside};