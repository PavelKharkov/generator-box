import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const popupsCallback = (resolve, isModules) => {
    const imports = [require('Components/popup').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
            .then(popupsContent => {
                chunks.popups = true;

                if (isModules) {
                    const modulesImports = [...modules];

                    const cookieAlert = document.querySelector('#cookies-alert');
                    if (cookieAlert) {
                        modulesImports.push(
                            require('Components/alert').default,
                            require('Components/cookie').default
                        );
                        emitInit('alert', 'cookie');
                    }

                    Promise.all(modulesImports).then(modulesModules => {
                        const popupsModules = popupsContent.popupsInit(...modulesModules);
                        resolve(popupsModules);
                    });
                    return;
                }

                const AppPopupsContent = popupsContent.default(...modules);
                resolve(AppPopupsContent);
            });
    });

    emitInit('popup');
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popups) {
        popupsCallback(resolve);
        return;
    }

    onInit('popupsContent', () => {
        popupsCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('popupsContent');
    });
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popups) {
        popupsCallback(resolve, true);
        return;
    }

    onInit('popupsContent', () => {
        popupsCallback(resolve, true);
    });
});

export default importFunc;
export {modules};