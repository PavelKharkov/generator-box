import './sync.scss';

const {popupsContentInit} = require('./popup-content.js');

const popupsInit = (AppPopup, AppAlert, AppCookie) => {
    const AppPopupsContent = popupsContentInit(AppPopup);

    let AppCookieAlert;
    const cookieAlert = document.querySelector('#cookies-alert');
    if (cookieAlert) {
        const {cookieAlertInit} = require('./cookie-alert');
        AppCookieAlert = cookieAlertInit(AppAlert, AppCookie);
    }

    return {
        AppPopupsContent,
        AppCookieAlert
    };
};

export default popupsContentInit;
export {popupsInit};