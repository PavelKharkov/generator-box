import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'popupContent';

const popupClass = 'popup';
const popupCloseSelector = '[data-popup-close]';

//Опции по умолчанию
const defaultOptions = {
    inner: true
};

let AppPopupContent;

const popupsContentInit = AppPopup => {
    if (AppPopupContent) return AppPopupContent;

    /**
     * @function AppPopupContent
     * @namespace layout.AppPopupContent
     * @instance
     * @requires components#AppPopup
     * @requires components#AppValidation
     * @desc Экземпляр [Module]{@link app.Module}. Модуль базового шаблона для попапов.
     * @async
     */
    AppPopupContent = immutable(new Module(

        /**
         * @desc Функция-обёртка.
         * @this AppPopupContent
         * @returns {undefined}
         */
        function() {
            const moduleOptions = {};
            util.defaultsDeep(moduleOptions, defaultOptions);

            /**
             * @member {Object} AppPopupContent.params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this, moduleOptions);

            /**
             * @function AppPopupContent.baseTemplate
             * @memberof layout
             * @desc Инициализирует разметку контента попапа.
             * @param {Object} [options={}] - Опции шаблона.
             * @param {boolean} [options.inner=true] - Оборачивать ли шаблон попапа в дополнительный контейнер.
             * @param {string} [options.before] - HTML-строка контента перед основным контентом.
             * @param {string} [options.headerAddClass] - Дополнительный класс заголовка попапа.
             * @param {string} [options.titleBefore] - HTML-строка контента в начале заголовка.
             * @param {string} [options.title] - HTML-строка основного контента заголовка.
             * @param {string} [options.titleAfter] - HTML-строка контента в конце заголовка.
             * @param {string} [options.text] - HTML-строка основного контента.
             * @param {string} [options.after] - HTML-строка контента после основного контента.
             * @param {string} [options.popupClass] - Дополнительный класс попапа.
             * @returns {string} HTML-строка шаблона попапа с контентом.
             * @readonly
             * @example
             * const popupTemplate = app.popupContent.baseTemplate({
             *     title: 'Попап',
             *     text: 'Контент',
             *     popupClass: 'test-popup'
             * });
             * console.log(popupTemplate); //HTML-код попапа
             */
            Object.defineProperty(this, 'baseTemplate', {
                enumerable: true,
                value(options = {}) {
                    if (!util.isObject(options)) {
                        util.typeError(options, 'options', 'plain object');
                    }

                    util.defaultsDeep(options, params);

                    let content = '';

                    if (options.before) {
                        content += options.before;
                    }

                    if (options.title) {
                        const headerClasses = `${popupClass}__header${options.headerAddClass ? ` ${options.headerAddClass}` : ''}`;
                        content += `
                        <header class="${headerClasses}">
                            ${options.titleBefore || ''}
                            <h2 class="${popupClass}-header__title">${options.title}</h2>
                            ${options.titleAfter || ''}
                        </header>`;
                    }

                    if (options.text || options.textAfter) {
                        content += `<div class="${popupClass}__content">
                            ${(options.text || '') + (options.textAfter || '')}
                        </div>`;
                    }

                    if (options.after) {
                        content += options.after;
                    }

                    const popupInstance = AppPopup.template({
                        content,
                        inner: options.inner,
                        popupClass: options.popupClass
                    });

                    return popupInstance;
                }
            });

            /**
             * @function AppPopupContent.initCloseButton
             * @memberof layout
             * @desc Инициализирует кнопки закрытия попапа.
             * @param {HTMLElement} element - Родительский контейнер попапа.
             * @returns {undefined}
             * @readonly
             * @example
             * app.popupContent.initCloseButton(popupContent);
             */
            Object.defineProperty(this, 'initCloseButton', {
                enumerable: true,
                value(element) {
                    element.querySelectorAll(popupCloseSelector).forEach(button => {
                        button.addEventListener('click', () => {
                            const popupEl = button.closest(`.${popupClass}`);
                            if (popupEl) popupEl.modules.popup.hide();
                        });
                    });
                }
            });

            return moduleOptions;
        },

        moduleName
    ));

    addModule(moduleName, AppPopupContent);

    return AppPopupContent;
};

export default popupsContentInit;
export {popupsContentInit};