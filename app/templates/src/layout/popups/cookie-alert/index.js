import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'cookieAlert';

const cookieName = 'app_acceptCookies';

const cookieAlert = document.querySelector('#cookies-alert');

const cookieAlertInit = (AppAlert, AppCookie) => {
    /**
     * @class AppCookieAlert
     * @memberof layout
     * @requires components#AppCookie
     * @classdesc Попап-уведомление об использовании cookie.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @example
     * const cookieAlertInstance = new app.CookieAlert(document.querySelector('.cookie-alert-element'));
     */
    const AppCookieAlert = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const alertInstance = el.modules.alert;
            if (AppCookie.get(cookieName) === 'true') {
                alertInstance.hide();
            } else {
                alertInstance.show();
                alertInstance.on('hide', () => {
                    AppCookie.set(cookieName, 'true', {
                        expires: 365
                    });
                });
                el.querySelector('.alert__button').addEventListener('click', () => {
                    alertInstance.hide();
                });
            }
        }
    });

    addModule(moduleName, AppCookieAlert);

    new AppCookieAlert(cookieAlert); /* eslint-disable-line no-new */

    return AppCookieAlert;
};

export default cookieAlertInit;
export {cookieAlertInit};