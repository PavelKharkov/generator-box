let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const productCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "product" */ './sync.js'))
        .then(productModules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppProduct = productModules.default;

            if (isModules) {
                const {
                    AppProductAvailability,
                    AppProductBuy,
                    AppProductGallery,
                    AppProductCart,
                    AppProductCounter,
                    AppProductParameters,
                    AppProductReviews,
                    AppProductTabs
                } = productModules;
                resolve({
                    AppProduct,
                    AppProductAvailability,
                    AppProductBuy,
                    AppProductGallery,
                    AppProductCart,
                    AppProductCounter,
                    AppProductParameters,
                    AppProductReviews,
                    AppProductTabs
                });
                return;
            }

            resolve(AppProduct);
        });
};

const importFunc = new Promise(resolve => {
    productCallback(resolve);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    productCallback(resolve, true);
});

export default importFunc;
export {modules};