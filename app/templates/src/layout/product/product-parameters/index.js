import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

const moduleName = 'productParameters';

/**
 * @class AppProductParameters
 * @memberof layout
 * @requires components#AppPopover
 * @requires layout#AppWindow
 * @classdesc Модуль параметров карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productParameters = new app.ProductParameters(document.querySelector('.product-parameters-element'));
 */
const AppProductParameters = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const popoverLinks = el.querySelectorAll('.product-parameters__popover-link');

        let popovers;

        /**
         * @member {Array.<Object>} AppProductParameters#popovers
         * @memberof layout
         * @desc Коллекция ссылок на экземпляры информеров [AppPopover]{@link components.AppPopover} заголовков свойств товаров.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        if (popoverLinks.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...popoverLinks].map(item => {
                    const popover = new AppPopover(item, {
                        placement() {
                            return (AppWindow.width() < util.media.sm) ? 'bottom' : 'right';
                        }
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(popoverLinks);
        }
    }
});

addModule(moduleName, AppProductParameters);

export default AppProductParameters;
export {AppProductParameters};