import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import AppStateToggler from 'Layout/state-toggler';

const moduleName = 'productBuy';

/**
 * @class AppProductBuy
 * @memberof layout
 * @requires layout#AppStateToggler
 * @classdesc Модуль кнопок покупки в карточке товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productBuyInstance = new app.ProductBuy(document.querySelector('.product-buy-element'));
 */
const AppProductBuy = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const buy = el.querySelectorAll('.product-buy__item');

        const stateTogglers = [...buy].map(buyItem => {
            const stateToggler = new AppStateToggler(buyItem);
            return stateToggler;
        });

        /**
         * @member {Array.<Object>} AppProductBuy#stateToggler
         * @memberof layout
         * @desc Коллекция ссылок на модули переключения состояния [AppStateToggler]{@link layout.AppStateToggler}.
         * @readonly
         */
        Object.defineProperty(this, 'stateToggler', {
            enumerable: true,
            value: stateTogglers
        });
    }
});

addModule(moduleName, AppProductBuy);

export default AppProductBuy;
export {AppProductBuy};