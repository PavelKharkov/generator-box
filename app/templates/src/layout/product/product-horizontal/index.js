import './style.scss';

import util from 'Layout/main';

import AppWindow from 'Layout/window';

const productGallerySelector = '.product__gallery';

const productHorizontal = AppProduct => {
    const productEl = AppProduct.el;

    const leftSideInner = productEl.querySelector('.product__side.-left .product-side__inner');
    if (!leftSideInner) return;

    /**
     * @member {HTMLElement} AppProduct#leftSideInner
     * @memberof layout
     * @desc Элемент левого контейнера карточки.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'leftSideInner', {
        enumerable: true,
        value: leftSideInner
    });

    const breadcrumbs = productEl.querySelector('.breadcrumbs');

    /**
     * @member {HTMLElement} AppProduct#breadcrumbs
     * @memberof layout
     * @desc Элемент хлебных крошек в карточке товара.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'breadcrumbs', {
        enumerable: true,
        value: breadcrumbs
    });

    const tabsEl = productEl.querySelector('.product__tabs');

    /**
     * @member {HTMLElement} AppProduct#tabsEl
     * @memberof layout
     * @desc Элемент вкладок в карточке товара.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'tabsEl', {
        enumerable: true,
        value: tabsEl
    });

    const rightSideInner = productEl.querySelector('.product__side.-right .product-side__inner');

    /**
     * @member {HTMLElement} AppProduct#rightSideInner
     * @memberof layout
     * @desc Элемент правого контейнера карточки.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'rightSideInner', {
        enumerable: true,
        value: rightSideInner
    });

    const galleryMobileEl = productEl.querySelector('.product__gallery-mobile');
    const headerEl = productEl.querySelector('.product__header');

    AppWindow.on('resize', () => {
        if (AppWindow.width() < util.media.lg) {
            const leftGallery = leftSideInner.querySelector(productGallerySelector);
            if (!leftGallery) return;
            headerEl.before(breadcrumbs);
            galleryMobileEl.append(leftGallery);
            return;
        }

        const rightGallery = rightSideInner.querySelector(productGallerySelector);
        if (!rightGallery) return;
        tabsEl.before(breadcrumbs);
        tabsEl.before(rightGallery);
    });

    AppWindow.emit('resize');
};

export default productHorizontal;