import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'productReviews';

/**
 * @class AppProductReviews
 * @memberof layout
 * @requires components#AppCollapse
 * @classdesc Модуль формы отзывов карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productReviews = new app.ProductReviews(document.querySelector('.product-reviews-element'));
 */
const AppProductReviews = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const collapseEl = el.querySelector('.product-reviews-form-toggle__button');

        let collapse;

        /**
         * @member {Object} AppProductReviews#collapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося блока [AppCollapse]{@link components.AppCollapse}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapse', {
            enumerable: true,
            get: () => collapse
        });

        if (collapseEl) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                util.enable(collapseEl);
                collapse = new AppCollapse(collapseEl);
            });

            collapseRequire.collapseTrigger(collapseEl);
        }
    }
});

addModule(moduleName, AppProductReviews);

export default AppProductReviews;
export {AppProductReviews};