import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule, emitInit} from 'Base/scripts/app.js';

const moduleName = 'productGallery';

/**
 * @class AppProductGallery
 * @memberof layout
 * @requires components#AppGallery
 * @requires components#AppUrl
 * @classdesc Модуль галереи карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productGallery = new app.ProductGallery(document.querySelector('.product-gallery-element'));
 */
const AppProductGallery = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const galleryEl = el.querySelector('.gallery');

        let gallery;

        /**
         * @member {Object} AppProductGallery#gallery
         * @memberof layout
         * @desc Ссылка на модуль галереи [AppGallery]{@link components.AppGallery}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'gallery', {
            enumerable: true,
            get: () => gallery
        });

        require('Components/gallery').default.then(AppGallery => {
            gallery = new AppGallery(galleryEl);

            require('Components/url').default.then(AppUrl => {
                AppUrl.testAction('gallery');
            });
            if (window.location.hash) emitInit('url');
        });
        emitInit('gallery');
    }
});

addModule(moduleName, AppProductGallery);

export default AppProductGallery;
export {AppProductGallery};