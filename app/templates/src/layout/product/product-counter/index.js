import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import AppFloatLabel from 'Components/float-label';

const moduleName = 'productCounter';

/**
 * @class AppProductCounter
 * @memberof layout
 * @requires components#AppFloatLabel
 * @requires components#AppCounter
 * @classdesc Модуль счетчика карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productCounter = new app.ProductCounter(document.querySelector('.product-counter-element'));
 */
const AppProductCounter = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const counterEl = el.querySelector('.product-counter__counter');

        let counter;

        /**
         * @member {Object} AppProductCounter#counter
         * @memberof layout
         * @desc Ссылка на модуль счётчика [AppCounter]{@link components.AppCounter}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'counter', {
            enumerable: true,
            get: () => counter
        });

        let floatLabel;

        /**
         * @member {Object} AppProductCounter#floatLabel
         * @memberof layout
         * @desc Ссылка на модуль динамического заголовка поля [AppFloatLabel]{@link components.AppFloatLabel}.
         * @readonly
         */
        Object.defineProperty(this, 'floatLabel', {
            enumerable: true,
            get: () => floatLabel
        });

        if (counterEl) {
            const counterRequire = require('Components/counter');
            counterRequire.default.then(AppCounter => {
                counter = new AppCounter(counterEl);
            });

            counterRequire.counterTrigger(counterEl);

            if (counterEl.classList.contains('-float-label')) {
                floatLabel = new AppFloatLabel(counterEl);
            }
        }
    }
});

addModule(moduleName, AppProductCounter);

export default AppProductCounter;
export {AppProductCounter};