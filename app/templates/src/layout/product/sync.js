import './sync.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

//Инициализация блоков карточки товара
let AppProductAvailability;
let AppProductBuy;
let AppProductGallery;
let AppProductCart;
let AppProductCounter;
let AppProductParameters;
let AppProductReviews;
let AppProductTabs;

//Опции
const moduleName = 'product';

const productEl = document.querySelectorAll('.product');

/**
 * @class AppProduct
 * @memberof layout
 * @requires layout#AppWindow
 * @classdesc Модуль карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productInstance = new app.Product(document.querySelector('.product-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="product"></div>
 *
 * <!--.product - селектор по умолчанию-->
 */
const AppProduct = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        if (__GLOBAL_SETTINGS__.verticalProduct) {
            const productVertical = require('./product-vertical').default;
            if (productVertical) productVertical(this);
        } else {
            const productHorizontal = require('./product-horizontal').default;
            if (productHorizontal) productHorizontal(this);
        }

        //TODO:add product-actions

        //Инициализация кнопок покупки
        let buy;

        /**
         * @member {Array.<Object>} AppProduct#buy
         * @memberof layout
         * @desc Коллекция ссылок на модули кнопок покупки [AppProductBuy]{@link layout.AppProductBuy} в карточке товара.
         * @readonly
         */
        Object.defineProperty(this, 'buy', {
            enumerable: true,
            get: () => buy
        });

        const buyEl = el.querySelector('.product__buy');
        if (buyEl) {
            AppProductBuy = require('./product-buy').default;

            const buyGroupEl = el.querySelectorAll('.product-buy__group');

            buy = [...buyGroupEl].map(element => new AppProductBuy(element));
        }

        //Инициализация таблицы количества товаров на складах
        if (__GLOBAL_SETTINGS__.availability) {
            let availability;

            /**
             * @member {Array.<Object>} AppProduct#availability
             * @memberof layout
             * @desc Коллекция ссылок на модули таблицы количества товаров на складах [AppProductAvailability]{@link layout.AppProductAvailability} в карточке товара.
             * @readonly
             */
            Object.defineProperty(this, 'availability', {
                enumerable: true,
                get: () => availability
            });

            const availabilityEl = el.querySelectorAll('.product__availability');
            if (availabilityEl.length > 0) {
                AppProductAvailability = require('./product-availability').default;

                availability = [...availabilityEl].map(element => new AppProductAvailability(element));
            }
        }

        //Инициализация галереи
        let galleries;

        /**
         * @member {Array.<Object>} AppProduct#galleries
         * @memberof layout
         * @desc Коллекция ссылок на модули галереи [AppProductGallery]{@link layout.AppProductGallery} в карточке товара.
         * @readonly
         */
        Object.defineProperty(this, 'galleries', {
            enumerable: true,
            get: () => galleries
        });

        const productGalleryEl = el.querySelectorAll('.product__gallery');
        if (productGalleryEl.length > 0) {
            AppProductGallery = require('./product-gallery').default;

            galleries = [...productGalleryEl].map(element => new AppProductGallery(element));
        }

        //Блок соц. сетей товара
        const productShareEl = el.querySelectorAll('.product__share');
        if (productShareEl.length > 0) {
            require('./product-share');
        }

        //Счётчик в карточке товара
        if (__GLOBAL_SETTINGS__.buy) {
            let counters;

            /**
             * @member {Array.<Object>} AppProduct#counters
             * @memberof layout
             * @desc Список ссылок на модули счётчиков [AppProductCounter]{@link layout.AppProductCounter} в карточке товара.
             * @readonly
             */
            Object.defineProperty(this, 'counters', {
                enumerable: true,
                get: () => counters
            });

            const productCounterEl = el.querySelectorAll('.product__counter');
            if (productCounterEl.length > 0) {
                AppProductCounter = require('./product-counter').default;

                counters = [...productCounterEl].map(element => new AppProductCounter(element));
            }
        }

        //Корзина
        let carts;

        /**
         * @member {Array.<Object>} AppProduct#carts
         * @memberof layout
         * @desc Список ссылок на модули корзины [AppProductCart]{@link layout.AppProductCart} в карточке товара.
         * @readonly
         */
        Object.defineProperty(this, 'carts', {
            enumerable: true,
            get: () => carts
        });

        const productCartEl = el.querySelectorAll('.product-buy__item.-cart');
        if (productCartEl.length > 0) {
            AppProductCart = require('./product-cart').default;

            carts = [...productCartEl].map(element => new AppProductCart(element));
        }

        //Параметры
        let parameters;

        /**
         * @member {Array.<Object>} AppProduct#parameters
         * @memberof layout
         * @desc Список ссылок на модули параметров [AppProductParameters]{@link layout.AppProductParameters} в карточке товара.
         * @readonly
         */
        Object.defineProperty(this, 'parameters', {
            enumerable: true,
            get: () => parameters
        });

        const productParametersEl = el.querySelectorAll('.product__parameters');
        if (productParametersEl.length > 0) {
            AppProductParameters = require('./product-parameters').default;

            parameters = [...productParametersEl].map(element => new AppProductParameters(element));
        }

        //Отзывы
        if (__GLOBAL_SETTINGS__.reviews) {
            let reviews;

            /**
             * @member {Array.<Object>} AppProduct#reviews
             * @memberof layout
             * @desc Список ссылок на модули формы отзывов [AppProductReviews]{@link layout.AppProductReviews} в карточке товара.
             * @readonly
             */
            Object.defineProperty(this, 'reviews', {
                enumerable: true,
                get: () => reviews
            });

            const productReviewsEl = el.querySelectorAll('.product__reviews');
            if (productReviewsEl.length > 0) {
                AppProductReviews = require('./product-reviews').default;

                reviews = [...productReviewsEl].map(element => new AppProductReviews(element));
            }
        }

        //Вкладки
        let tabs;

        /**
         * @member {Array.<Object>} AppProduct#tabs
         * @memberof layout
         * @desc Список ссылок на модули вкладок [AppProductTabs]{@link layout.AppProductTabs} в карточке товара.
         * @readonly
         */
        Object.defineProperty(this, 'tabs', {
            enumerable: true,
            get: () => tabs
        });

        const productTabsEl = el.querySelectorAll('.product__tabs');
        if (productTabsEl.length > 0) {
            AppProductTabs = require('./product-tabs').default;

            tabs = [...productTabsEl].map(element => new AppProductTabs(element));
        }
    }
});

addModule(moduleName, AppProduct);

//Инициализация элементов по классу
productEl.forEach(el => new AppProduct(el));

export default AppProduct;
export {
    AppProduct,
    AppProductAvailability,
    AppProductBuy,
    AppProductGallery,
    AppProductCart,
    AppProductCounter,
    AppProductParameters,
    AppProductReviews,
    AppProductTabs
};