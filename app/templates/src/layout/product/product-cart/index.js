import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import AppStateToggler from 'Layout/state-toggler';

const moduleName = 'productCart';

/**
 * @class AppProductCart
 * @memberof layout
 * @requires layout#AppStateToggler
 * @classdesc Модуль корзины карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productCart = new app.ProductCart(document.querySelector('.product-cart-element'));
 */
const AppProductCart = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const stateToggler = new AppStateToggler(el);

        /**
         * @member {Object} AppProductCart#stateToggler
         * @memberof layout
         * @desc Ссылка на модуль переключения состояния [AppStateToggler]{@link layout.AppStateToggler}.
         * @readonly
         */
        Object.defineProperty(this, 'stateToggler', {
            enumerable: true,
            value: stateToggler
        });
    }
});

addModule(moduleName, AppProductCart);

export default AppProductCart;
export {AppProductCart};