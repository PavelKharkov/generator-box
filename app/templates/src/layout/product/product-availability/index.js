import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'productAvailability';

/**
 * @class AppProductAvailability
 * @memberof layout
 * @requires components#AppMask
 * @classdesc Модуль таблицы количества товаров на складах карточки товаров.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productAvailabilityInstance = new app.ProductAvailability(document.querySelector('.product-availability-element'));
 */
const AppProductAvailability = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let masks;

        /**
         * @member {Array.<Object>} AppProductAvailability#masks
         * @memberof layout
         * @desc Коллекция ссылок на модули маски [AppMask]{@link components.AppMask} полей ввода в карточке товара.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'masks', {
            enumerable: true,
            get: () => masks
        });

        const availabilityCounterEl = el.querySelectorAll('.product-availability__input');
        if (availabilityCounterEl.length > 0) {
            const maskRequire = require('Components/mask');
            maskRequire.default.then(AppMask => {
                masks = [...availabilityCounterEl].map(item => {
                    const mask = new AppMask(item, AppMask.numberOptions);
                    return mask;
                });
            });

            maskRequire.maskTrigger(availabilityCounterEl);
        }
    }
});

addModule(moduleName, AppProductAvailability);

export default AppProductAvailability;
export {AppProductAvailability};