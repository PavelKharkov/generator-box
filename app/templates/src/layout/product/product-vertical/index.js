import './style.scss';

import util from 'Layout/main';

import AppWindow from 'Layout/window';

const productGallerySelector = '.product__gallery';

const productVertical = AppProduct => {
    const productEl = AppProduct.el;

    const topSideInner = productEl.querySelector('.product__side.-top .product-side__inner');
    if (!topSideInner) return;

    /**
     * @member {HTMLElement} AppProduct#topSideInner
     * @memberof layout
     * @desc Элемент верхнего контейнера карточки.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'topSideInner', {
        enumerable: true,
        value: topSideInner
    });

    const info = productEl.querySelector('.product__info');

    /**
     * @member {HTMLElement} AppProduct#info
     * @memberof layout
     * @desc Элемент блока информации в карточке товара.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'info', {
        enumerable: true,
        value: info
    });

    const infoInner = productEl.querySelector('.product-info__inner');

    /**
     * @member {HTMLElement} AppProduct#infoInner
     * @memberof layout
     * @desc Элемент внутреннего блока информации в карточке товара.
     * @readonly
     */
    Object.defineProperty(AppProduct, 'infoInner', {
        enumerable: true,
        value: infoInner
    });

    const separator = productEl.querySelector('.product__separator');

    AppWindow.on('resize', () => {
        if (AppWindow.width() < util.media.lg) {
            const topGallery = topSideInner.querySelector(productGallerySelector);
            if (topGallery) separator.before(topGallery);
            return;
        }

        const infoGallery = infoInner.querySelector(productGallerySelector);
        if (infoGallery) info.before(infoGallery);
    });

    AppWindow.emit('resize');
};

export default productVertical;