import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule, emitInit} from 'Base/scripts/app.js';

const moduleName = 'productTabs';

/**
 * @class AppProductTabs
 * @memberof layout
 * @requires components#AppTabs
 * @requires components#AppUrl
 * @classdesc Модуль вкладок карточки товара.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const productTabs = new app.ProductTabs(document.querySelector('.product-tabs-element'));
 */
const AppProductTabs = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const tabsMenu = el.querySelector('.product-tabs-menu__list');

        let tabs;

        /**
         * @member {Object} AppProductTabs#tabs
         * @memberof layout
         * @desc Ссылка на модуль вкладок [AppTabs]{@link components.AppTabs}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'tabs', {
            enumerable: true,
            get: () => tabs
        });

        const tabsRequire = require('Components/tabs');
        tabsRequire.default.then(AppTabs => {
            tabs = new AppTabs(tabsMenu, {
                collapse: true
            });

            require('Components/url').default.then(AppUrl => {
                AppUrl.testAction('tabs');
            });
            if (window.location.hash) emitInit('url');
        });

        tabsRequire.tabsTrigger(tabsMenu);
    }
});

addModule(moduleName, AppProductTabs);

export default AppProductTabs;
export {AppProductTabs};