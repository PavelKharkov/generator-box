let AppFacebookPixel;
let AppGoogleTagManager;
let AppVkOpenapi;
let AppYaMetrika;

if (__IS_DISABLED_MODULE__) {
    AppFacebookPixel = require('./facebook-pixel').default;
}

if (__IS_DISABLED_MODULE__) {
    AppGoogleTagManager = require('./google-tag-manager').default;
}

if (__IS_DISABLED_MODULE__) {
    AppVkOpenapi = require('./vk-openapi').default;
}

if (__IS_DISABLED_MODULE__) {
    AppYaMetrika = require('./yandex-metrika').default;
}

export {
    AppFacebookPixel,
    AppGoogleTagManager,
    AppVkOpenapi,
    AppYaMetrika
};