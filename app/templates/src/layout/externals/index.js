import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const externalsCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(modules => {
            chunks.helpers = true;

            const {
                AppFacebookPixel,
                AppGoogleTagManager,
                AppVkOpenapi,
                AppYaMetrika
            } = modules;
            resolve({
                AppFacebookPixel,
                AppGoogleTagManager,
                AppVkOpenapi,
                AppYaMetrika
            });
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        externalsCallback(resolve);
        return;
    }

    onInit('externals', () => {
        externalsCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('externals');
    });
});

export default importFunc;