import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

const moduleName = 'vkOpenapi';

/**
 * @function AppVkOpenapi
 * @namespace layout.AppVkOpenapi
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль VK Openapi.
 * @async
 */
const AppVkOpenapi = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppVkOpenapi
     * @returns {undefined}
     */
    function() {
        const apiId = app.globalConfig.vkOpenapiId;

        /**
         * @member {string} AppVkOpenapi.apiId
         * @memberof layout
         * @desc Идентификатор для инициализации VK Openapi. Берётся из app.globalConfig.vkOpenapiId.
         * @readonly
         */
        Object.defineProperty(this, 'apiId', {
            enumerable: true,
            value: apiId
        });
        if (!apiId) return;

        let vkObject;

        /**
         * @member {Object} AppVkOpenapi.vk
         * @memberof layout
         * @desc Содержит ссылку на скрипт VK Openapi.
         * @readonly
         */
        Object.defineProperty(this, 'vk', {
            enumerable: true,
            get: () => vkObject
        });

        util.addScript('//vk.com/js/api/openapi.js?161', () => {
            if (window.VK) {
                vkObject = window.VK;
            }

            vkObject.Retargeting.Init(apiId);
            vkObject.Retargeting.Hit();
        });
    },

    moduleName
));

addModule(moduleName, AppVkOpenapi);

export default AppVkOpenapi;
export {AppVkOpenapi};