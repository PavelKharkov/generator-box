import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

const moduleName = 'facebookPixel';

/**
 * @function AppFacebookPixel
 * @namespace layout.AppFacebookPixel
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль Facebook Pixel.
 * @async
 */
const AppFacebookPixel = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppFacebookPixel
     * @returns {undefined}
     */
    function() {
        const pixelId = app.globalConfig.facebookPixelId;

        /**
         * @member {string} AppFacebookPixel.pixelId
         * @memberof layout
         * @desc Идентификатор для инициализации Facebook Pixel. Берётся из app.globalConfig.facebookPixelId.
         * @readonly
         */
        Object.defineProperty(this, 'pixelId', {
            enumerable: true,
            value: pixelId
        });
        if (!pixelId) return;

        util.addScript(`https://connect.facebook.net/${__LANG__}_${__REGION__}/fbevents.js`);

        /**
         * @function AppFacebookPixel.fbq
         * @memberof layout
         * @desc Добавляет новую конфигурацию Facebook Pixel.
         * @param {*} args - Параметры добавления конфигурации Facebook Pixel.
         * @returns {undefined}
         * @readonly
         * @example
         * app.facebookPixel.fbq('track', 'Purchase', {currency: 'RUB', value: 30.00});
         */
        const fbq = (...args) => {
            if (fbq.callMethod) {
                fbq.callMethod(...args);
                return;
            }

            fbq.queue.push(args);
        };

        window._fbq = fbq; /* eslint-disable-line no-underscore-dangle */
        fbq.push = fbq;
        fbq.loaded = true;
        fbq.version = '2.0';
        fbq.queue = [];

        Object.defineProperty(this, 'fbq', {
            enumerable: true,
            value: fbq
        });
        window.fbq = fbq;

        /**
         * @function AppFacebookPixel.track
         * @memberof layout
         * @desc Добавляет новое отслеживание в Facebook Pixel.
         * @param {*} args - Параметры отслеживания в Facebook Pixel.
         * @readonly
         * @example
         * app.facebookPixel.fbq('Purchase', {currency: 'RUB', value: 30.00});
         */
        Object.defineProperty(this, 'track', {
            enumerable: true,
            value(...args) {
                fbq('track', ...args);
            }
        });

        //Инициализация ключа по умолчанию
        fbq('init', pixelId);
        this.track('PageView');
    },

    moduleName
));

addModule(moduleName, AppFacebookPixel);

export default AppFacebookPixel;
export {AppFacebookPixel};