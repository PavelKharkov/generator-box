import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'yaMetrika';

//Опции по умолчанию
const defaultOptions = {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
    webvisor: true
};

/**
 * @function AppYaMetrika
 * @namespace layout.AppYaMetrika
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль Yandex Metrika.
 * @async
 */
const AppYaMetrika = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppYaMetrika
     * @returns {undefined}
     */
    function() {
        const moduleOptions = {};
        util.defaultsDeep(moduleOptions, defaultOptions);

        let metrika;

        /**
         * @member {Object} AppYaMetrika.metrika
         * @memberof layout
         * @desc Содержит ссылку на скрипт Yandex Metrika.
         * @readonly
         */
        Object.defineProperty(this, 'metrika', {
            enumerable: true,
            get: () => metrika
        });

        util.addScript('//mc.yandex.ru/metrika/tag.js', () => {
            if (window.Ya && window.Ya._metrika) { /* eslint-disable-line no-underscore-dangle */
                metrika = window.Ya._metrika; /* eslint-disable-line no-underscore-dangle */
            }
        });

        /**
         * @function AppYaMetrika.ym
         * @memberof layout
         * @desc Добавляет новую конфигурацию для счётчика Яндекс метрики.
         * @param {*} args - Параметры добавления новой конфигурации счётчика Яндекс метрики.
         * @returns {undefined}
         * @readonly
         * @example
         * app.yaMetrika.ym(23345636, 'init', {
         *     clickmap: true,
         *     trackLinks: true,
         *     accurateTrackBounce: true,
         *     webvisor: true
         * });
         */
        const ym = (...args) => { /* eslint-disable-line id-length */
            ym.a = ym.a || [];
            ym.a.push(args);
        };
        ym.l = Number(new Date());

        Object.defineProperty(this, 'ym', {
            enumerable: true,
            value: ym
        });
        window.ym = ym;

        const counters = {};

        /**
         * @member {Object} AppYaMetrika.counters
         * @memberof layout
         * @desc Объект с добавленными счётчиками (хранятся по идентификатору).
         * @readonly
         */
        Object.defineProperty(this, 'counters', {
            enumerable: true,
            value: counters
        });

        /**
         * @function AppYaMetrika.addCounter
         * @memberof layout
         * @desc Добавляет новый счётчик Яндекс метрики.
         * @param {Object} options - Опции добавления счётчика.
         * @param {string} options.counterId - Идентификатор счётчика.
         * @param {boolean} [options.clickmap=true] - Опция clickmap Яндекс метрики.
         * @param {boolean} [options.trackLinks=true] - Опция trackLinks Яндекс метрики.
         * @param {boolean} [options.accurateTrackBounce=true] - Опция accurateTrackBounce Яндекс метрики.
         * @param {boolean} [options.webvisor=true] - Опция webvisor Яндекс метрики.
         * @returns {(undefined|Error)} Либо undefined, либо объект с ошибкой.
         * @readonly
         * @example
         * app.yaMetrika.addCounter({
         *     clickmap: true,
         *     trackLinks: true,
         *     accurateTrackBounce: true,
         *     webvisor: true
         * });
         */
        Object.defineProperty(this, 'addCounter', {
            enumerable: true,
            value(options = {}) {
                if (!util.isObject(options)) {
                    util.typeError(options, 'options', 'plain object');
                }

                util.defaultsDeep(options, moduleOptions);

                if (!options.counterId) util.error('incorrect yandex metrika counter id');

                const newCounter = util.attempt(() => {
                    return ym(options.counterId, 'init', {
                        clickmap: options.clickmap,
                        trackLinks: options.trackLinks,
                        accurateTrackBounce: options.accurateTrackBounce,
                        webvisor: options.webvisor
                    });
                });

                if (newCounter instanceof Error) util.errorutil.error('can\'t add yandex metrika counter');

                counters[options.counterId] = newCounter;

                return newCounter;
            }
        });

        /**
         * @function AppYaMetrika.reachGoal
         * @memberof layout
         * @desc Отправляет цели яндекс метрики. Если метрика не найдена, то выведет ошибку.
         * @param {string} goalId - Идентификатор цели.
         * @param {number} [counterId] - Идентификатор счётчика. По умолчанию отправляет цель первому зарегистрированному счётчику.
         * @returns {(Object|Error)} Либо объект, возвращаемый функцией reachGoal, либо объект с ошибкой.
         * @readonly
         * @example
         * document.querySelector('#add-to-cart-button').addEventListener('click', () => {
         *     app.yaMetrika.reachGoal('add-to-cart');
         * });
         */
        Object.defineProperty(this, 'addCounter', {
            enumerable: true,
            value(goalId, counterId) {
                return util.attempt(() => {
                    const counter = counterId ? counters[counterId] : Object.values(counters)[0];
                    return counter.reachGoal(goalId);
                });
            }
        });

        return moduleOptions;
    },

    moduleName
));

addModule(moduleName, AppYaMetrika);

export default AppYaMetrika;
export {AppYaMetrika};