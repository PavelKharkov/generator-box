import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

const moduleName = 'gtag';

/**
 * @function AppGtag
 * @namespace layout.AppGtag
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль Google Tag Manager.
 * @async
 */
const AppGtag = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppGtag
     * @returns {undefined}
     */
    function() {
        const gtagId = app.globalConfig.gtagId;

        /**
         * @member {string} AppGtag.gtagId
         * @memberof layout
         * @desc Идентификатор для инициализации Google Tag Manager. Берётся из app.globalConfig.gtagId.
         * @readonly
         */
        Object.defineProperty(this, 'gtagId', {
            enumerable: true,
            value: gtagId
        });
        if (!gtagId) return;

        util.addScript(`https://www.googletagmanager.com/gtag/js?id=${gtagId}`);

        window.dataLayer = window.dataLayer || [];

        /**
         * @function AppGtag.gtag
         * @memberof layout
         * @desc Добавляет новую конфигурацию Google Tag Manager.
         * @param {*} args - Идентификатор добавляемой конфигурации Google Tag Manager.
         * @returns {undefined}
         * @readonly
         * @example
         * app.gtag.gtag('js', new Date());
         */
        const gtag = (...args) => {
            window.dataLayer.push(args);
        };

        gtag('js', new Date());

        Object.defineProperty(this, 'gtag', {
            enumerable: true,
            value: gtag
        });
        window.gtag = gtag;

        /**
         * @function AppGtag.addConfig
         * @memberof layout
         * @desc Добавляет новую конфигурацию config в Google Tag Manager.
         * @param {string} gtagConfig - Идентификатор добавляемой конфигурации config в Google Tag Manager.
         * @returns {undefined}
         * @readonly
         * @example
         * app.gtag.addConfig('Идентификатор gtag');
         */
        Object.defineProperty(this, 'addConfig', {
            enumerable: true,
            value(gtagConfig) {
                gtag('config', gtagConfig);
            }
        });

        this.addConfig(gtagId);
    },

    moduleName
));

addModule(moduleName, AppGtag);

export default AppGtag;
export {AppGtag};