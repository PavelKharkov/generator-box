import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'stateToggler';

const stateTogglerClass = util.kebabCase(moduleName);
const dataStateToggler = 'state-toggler';
const activeClass = 'state-active';
const inactiveClass = 'state-inactive';
const initializedClass = `${stateTogglerClass}-initialized`;

const defaultState = 'default';

/**
 * @class AppStateToggler
 * @memberof layout
 * @classdesc Модуль переключения состояния блока.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - Элемент контейнера с состояниями.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.state] - Название текущего состояния. По умолчанию берётся из data-state-toggler инициализируемого элемента. Если такого состояния не передано, то становится состояние default.
 * @param {string} [options.itemsSelector] - Селектор элементов состояния. По умолчанию берутся все прямые элементы-потомки.
 * @example
 * const stateTogglerInstance = new app.StateToggler(document.querySelector('.state-toggler-element'), {
 *     state: 'active', //Элемент с data-state="active" будет показан при инициализации
 *     itemsSelector: '.state-toggler-inner-element'
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div data-state-toggler>
 *     <div data-state="default">Элемент 1</div>
 *     <div data-state="active">Элемент 2</div>
 * </div>
 *
 * <!--data-state-toggler - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-state-toggler-options='{"state": "active"}'></div>
 */
const AppStateToggler = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const stateTogglerOptionsData = el.dataset.stateTogglerOptions;
        if (stateTogglerOptionsData) {
            const dataOptions = util.stringToJSON(stateTogglerOptionsData);
            if (!dataOptions) util.error('incorrect data-state-toggler-options format');
            util.extend(this.options, dataOptions);
        }

        /**
         * @member {Object} AppStateToggler#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let initialState = '';

        const items = params.itemsSelector ? el.querySelectorAll(params.itemsSelector) : [...el.children];

        /**
         * @member {Array.<HTMLElement>} AppStateToggler#items
         * @memberof layout
         * @desc Коллекция элементов-состояний.
         * @readonly
         */
        Object.defineProperty(this, 'items', {
            enumerable: true,
            value: items
        });

        const states = {};

        /**
         * @member {Object} AppStateToggler#states
         * @memberof layout
         * @desc Объект с состояниями.
         * @readonly
         */
        Object.defineProperty(this, 'states', {
            enumerable: true,
            value: states
        });

        items.forEach(item => {
            //TODO:event trigger example
            item.addEventListener(`${moduleName}.set`, event => {
                return this.set(event.detail);
            });

            if (!item.dataset.state && !states[defaultState]) {
                item.dataset.state = defaultState;
            }

            if (item.classList.contains(activeClass)) {
                initialState = item.dataset.state;
            }

            states[item.dataset.state] = item;
        });

        let state = initialState || params.state || el.dataset[util.camelCase(dataStateToggler)] || defaultState;
        initialState = state;
        states[state].classList.add(activeClass);

        /**
         * @member {string} AppStateToggler#initialState
         * @memberof layout
         * @desc Хранит название состояния блока при инициализации.
         * @readonly
         */
        Object.defineProperty(this, 'initialState', {
            enumerable: true,
            value: initialState
        });

        /**
         * @member {string} AppStateToggler#state
         * @memberof layout
         * @desc Хранит название текущего состояния блока.
         * @readonly
         */
        Object.defineProperty(this, 'state', {
            enumerable: true,
            get: () => state
        });

        const setState = newState => {
            if (!states[newState]) {
                util.error({
                    message: `no state named "${newState}"`,
                    element: el
                });
            }

            states[state].classList.add(inactiveClass);
            states[state].classList.remove(activeClass);
            state = newState;
            states[state].classList.add(activeClass);
            states[state].classList.remove(inactiveClass);

            return states[state];
        };

        this.on({
            /**
             * @event AppStateToggler#set
             * @memberof layout
             * @desc Устанавливает новое состояние элемента.
             * @param {string} newState - Название нового состояния.
             * @throws Выводит ошибку, если состояние не найдено.
             * @returns {HTMLElement} Текущий активный элемент.
             * @example
             * stateTogglerInstance.on('set', newState => {
             *     console.log(`Установлено новое состояние элемента ${newState}`);
             * });
             * const activeElement = stateTogglerInstance.set('active');
             * console.log(activeElement); //Выведет текущий активный элемент
             */
            set(newState) {
                return setState(newState);
            },

            /**
             * @event AppStateToggler#reset
             * @memberof layout
             * @desc Устанавливает начальное состояние элемента.
             * @throws Выводит ошибку, если состояние не найдено.
             * @returns {HTMLElement} Текущий активный элемент.
             * @example
             * stateTogglerInstance.on('reset', newState => {
             *     console.log(`Установлено изначальное состояние элемента ${newState}`);
             * });
             * const activeElement = stateTogglerInstance.reset('');
             * console.log(activeElement); //Выведет начальный активный элемент
             */
            reset() {
                return setState();
            }
        });

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppStateToggler);

//Инициализация элементов по data-атрибуту
document.querySelectorAll(`[data-${dataStateToggler}]`).forEach(el => new AppStateToggler(el));

export default AppStateToggler;
export {AppStateToggler};