//TODO:example

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

let AppAuthRegister;

//Опции
const moduleName = 'authRegister';

const fetchFunction = util.debounce((ajaxFetch, options) => {
    ajaxFetch(options);
}, 500, true);

const isSymbolInput = input => {
    return input.classList.contains('form-control');
};

const authRegisterEl = document.querySelectorAll('.base-form.-auth-register');

if (authRegisterEl.length > 0) {
    /**
     * @class AppAuthRegister
     * @memberof layout
     * @requires components#AppMask
     * @classdesc Форма авторизации/регистрации.
     * @desc Наследует: [Module]{@link app.Module}.
     * @param {HTMLElement} element - HTML-элемент экземпляра.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     */
    AppAuthRegister = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            let masks;

            /**
             * @member {Array.<Object>} AppAuthRegister#masks
             * @memberof layout
             * @desc Коллекция ссылок на модули масок для полей ввода [AppMask]{@link components.AppMask}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'masks', {
                enumerable: true,
                get: () => masks
            });

            const inputEvent = new CustomEvent('input');

            //TODO:forms prop

            /**
             * @function AppAuthRegister#reinit
             * @memberof layout
             * @desc Переинициализирует скрипты для содержимого формы авторизации/регистрации.
             * @returns {undefined}
             * @readonly
             */
            Object.defineProperty(this, 'reinit', {
                enumerable: true,
                value() {
                    const codeInput = el.querySelector('.form__input.-code');
                    if (!codeInput) return;

                    const codeMaxLength = Number.parseInt(codeInput.maxLength, 10);
                    const codeForm = codeInput.closest('form');

                    //TODO:docs
                    const getNextInput = input => {
                        if (!input.value) return;

                        const isFullValue = codeInput.value.length === codeMaxLength;
                        if (isFullValue) {
                            const ajax = util.attempt(() => codeForm.modules.baseForm.ajax);
                            if (!(ajax instanceof Error)) {
                                fetchFunction(ajax.fetch, codeForm.modules.baseForm.params.fetch);
                            }
                            return;
                        }

                        const nextSibling = input.nextElementSibling;
                        const nextIsInput = nextSibling && isSymbolInput(nextSibling);
                        if (nextIsInput) nextSibling.focus();
                    };

                    const symbolInput = [...el.querySelectorAll('.form__input.-code-symbol')];
                    const symbolInputsLength = symbolInput.length;
                    symbolInput[0].focus();

                    symbolInput.forEach((input, inputIndex) => {
                        //TODO:docs
                        const inputFunction = event => {
                            if ((input.selectionStart === 1) && input.value[0]) {
                                const inputValue = input.value[0];
                                input.value = inputValue;
                                input.setAttribute('value', inputValue);
                            }
                            if ((event.inputType === 'insertFromPaste') && (inputIndex === (symbolInputsLength - 1))) {
                                return;
                            }
                            const formattedValue = input.value.trim();
                            if (!formattedValue) {
                                input.value = '';
                                input.setAttribute('value', '');
                                return;
                            }
                            const valueArray = [...formattedValue];
                            if (valueArray.length > 1) {
                                valueArray.forEach((symbol, index) => {
                                    if ((inputIndex + index) >= symbolInputsLength) {
                                        return;
                                    }

                                    const currentInput = symbolInput[inputIndex + index];
                                    currentInput.value = symbol;
                                    currentInput.setAttribute('value', symbol);
                                });
                                const nextInputIndex = inputIndex + (valueArray.length - 1);
                                if (nextInputIndex >= symbolInputsLength) return;

                                const currentInput = symbolInput[nextInputIndex];
                                currentInput.focus();
                                currentInput.dispatchEvent(inputEvent);
                                return;
                            }

                            const codeValue = [...symbolInput].map(item => {
                                return item.value;
                            }).join('');
                            codeInput.value = codeValue;
                            codeInput.setAttribute('value', codeValue);

                            getNextInput(input, codeInput);
                        };

                        if (!util.isTouch) {
                            input.addEventListener('input', event => {
                                inputFunction(event);
                            });
                        }
                        input.addEventListener('keydown', event => {
                            switch (event.key) {
                                case 'Backspace': {
                                    input.value = '';
                                    input.setAttribute('value', '');
                                    const prevSibling = input.previousElementSibling;
                                    util.defer(() => {
                                        if (prevSibling && isSymbolInput(prevSibling)) prevSibling.focus();
                                    });
                                    break;
                                }
                                case 'ArrowRight': {
                                    const nextSibling = input.nextElementSibling;
                                    util.defer(() => {
                                        if (nextSibling && isSymbolInput(nextSibling)) nextSibling.focus();
                                    });
                                    break;
                                }
                                case 'ArrowLeft': {
                                    const prevSibling = input.previousElementSibling;
                                    if (prevSibling && isSymbolInput(prevSibling)) {
                                        prevSibling.selectionStart = 0;
                                        prevSibling.selectionEnd = 0;
                                        prevSibling.focus();
                                    }
                                    break;
                                }
                                case 'Delete': {
                                    input.value = '';
                                    input.setAttribute('value', '');
                                    break;
                                }
                                default: {
                                    if (util.isTouch) {
                                        util.defer(() => {
                                            inputFunction(event);
                                        });
                                    }
                                }
                            }
                        });
                    });

                    const maskRequire = require('Components/mask');
                    maskRequire.default.then(AppMask => {
                        masks = [];

                        symbolInput.forEach(input => {
                            const maskOptions = {
                                onBeforePaste(pastedValue) {
                                    if (input.value) {
                                        input.value = '';
                                        input.setAttribute('value', '');
                                    }
                                    util.defer(() => {
                                        input.dispatchEvent(inputEvent);
                                    });
                                    return pastedValue;
                                }
                            };
                            util.extend(maskOptions, AppMask.numberOptions);
                            const mask = new AppMask(input, maskOptions);
                            masks.push(mask);
                        });
                    });

                    maskRequire.maskTrigger(symbolInput);
                }
            });

            this.reinit();
        }
    });

    addModule(moduleName, AppAuthRegister);

    authRegisterEl.forEach(el => new AppAuthRegister(el));
}

export default AppAuthRegister;
export {AppAuthRegister};