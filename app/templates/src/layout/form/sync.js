import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

let AppAuthRegister;

//Опции
const moduleName = 'baseForm';

//Классы
const baseFormClass = util.kebabCase(moduleName);
const initializedClass = `${baseFormClass}-initialized`;
const validClass = 'is-valid';

const authRegisterEl = document.querySelector('.base-form.-auth-register');
if (authRegisterEl) {
    AppAuthRegister = require('./auth-register').default;
}

const lang = (window.lang && window.lang[moduleName]) || require(`./lang/${__LANG__}.json`).data;

const baseFormInit = (AppAjax, AppAlert, AppForm, AppSvg, AppToaster, AppAlertPopup) => { /* eslint-disable-line max-params */
    const baseFormEl = document.querySelectorAll('[data-base-form]');

    const toasterEl = document.querySelector('.toaster.-top.-right');
    const toaster = toasterEl && toasterEl.modules.toaster;

    const successIcon = AppSvg.inline({
        id: 'check',
        width: 17,
        height: 13
    });

    const errorIcon = AppSvg.inline({
        id: 'close',
        width: 14,
        height: 14
    });

    //Инициализация уведомления при успешно отправленной форме
    const successAlert = new AppAlert(AppAlert.template({
        icon: successIcon,
        title: 'Спасибо!',
        content: 'Отправлено успешно',
        toast: true,
        color: 'success'
    }), {
        timeout: 2500
    });

    //Инициализация уведомления при отправленной форме с ошибкой
    const errorAlert = new AppAlert(AppAlert.template({
        icon: errorIcon,
        title: 'Ошибка',
        toast: true,
        dismissible: true,
        color: 'danger'
    }));

    //Инициализация попапа-сообщения при успешно отправленной форме
    const successPopup = new AppAlertPopup({
        success: true,
        title: 'Спасибо!',
        text: 'Отправлено успешно',
        textAfter: false
    });

    //Инициализация попапа-сообщения при отправленной форме с ошибкой
    const errorPopup = new AppAlertPopup({
        error: true,
        title: 'Ошибка',
        textAfter: false
    });

    //Опции по умолчанию
    const defaultOptions = {
        successAlert,
        fioMask: true,
        phoneMask: true,
        reset: true
    };

    const defaultFetchOptions = {};

    const alerts = {
        success: successAlert,
        error: errorAlert
    };

    const popups = {
        success: successPopup,
        error: errorPopup
    };

    const defaultFioMask = new RegExp(`[^${lang.fioMaskPattern}\\-\\s]`, 'gi');

    /**
     * @class AppBaseForm
     * @memberof layout
     * @requires components#AppAjax
     * @requires components#AppAlert
     * @requires components#AppForm
     * @requires components#AppMask
     * @requires components#AppToaster
     * @requires components#AppValidation
     * @requires layout#AppAlertPopup
     * @classdesc Модуль обычных форм.
     * @desc Наследует: [AppForm]{@link components.AppForm}.
     * @async
     * @param {HTMLElement} element - Элемент формы.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {Object} [options.successAlert=AppBaseForm.alerts.success] - Ссылка на модуль попапа-сообщения [AppAlertPopup]{@link layout.AppAlertPopup} при успешно отправленной форме.
     * @param {Object} [options.successPopup=AppBaseForm.popups.success] - Ссылка на модуль попапа-сообщения [AppAlertPopup]{@link layout.AppAlertPopup} при успешно отправленной форме.
     * @param {Object} [options.ajax] - Опции модуля ajax [AppAjax]{@link components.AppAjax} формы.
     * @param {Object} [options.fetch] - Опции, передающиеся при отправке ajax [AppAjax]{@link components.AppAjax} формы.
     * @param {(boolean|Object)} [options.validation] - Опции модуля валидации [AppValidation]{@link components.AppValidation} формы. Если false, то не использовать валидацию для формы.
     * @param {(boolean|Object)} [options.fioMask=true] - Опции модуля маски [AppMask]{@link components.AppMask} для полей ввода имени. Если true, то используются опции маски для ввода русских символов. Если false, то не добавлять маску для полей ввода имени.
     * @param {(boolean|Object)} [options.phoneMask=true] - Опции модуля маски [AppMask]{@link components.AppMask} для полей ввода телефона. Если true, то используются опции [freePhoneOptions]{@link components.AppMask.freePhoneOptions}. Если false, то не добавлять маску для полей ввода телефона.
     * @param {boolean} [options.reset=true] - Сбрасывать ли поля формы после отправки.
     * @example
     * const baseFormInstance = new app.BaseForm(document.querySelector('.base-form-element'), {
     *     ajax: {
     *         url: '/api/some_method'
     *     },
     *     fetch: {
     *         addData: {
     *             add_data: 123
     *         }
     *     },
     *     validation: {
     *         validHandler(form, event) {
     *             form.submit(event);
     *         }
     *     },
     *     phoneMask: {
     *         mask: '+7 (999) 999 99 99'
     *     }
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <form data-base-form></form>
     *
     * <!--data-card-slider - селектор по умолчанию-->
     *
     * @example <caption>Добавление опций через data-атрибут</caption>
     * <!--HTML-->
     * <div data-base-form-options='{"ajax": {"method": "get"}}'></div>
     */
    const AppBaseForm = immutable(class extends AppForm {
        constructor(el, opts = {}, appname = moduleName) {
            if (!util.isObject(opts)) {
                util.typeError(opts, 'opts', 'plain object');
            }

            opts.initSubmitEvent = false;

            super(el, opts, appname);

            const baseFormOptionsData = el.dataset.baseFormOptions;
            if (baseFormOptionsData) {
                const dataOptions = util.stringToJSON(baseFormOptionsData);
                if (!dataOptions) util.error('incorrect data-base-form-options format');
                util.extend(this.options, dataOptions);
            }

            util.defaultsDeep(this.options, defaultOptions);

            /**
             * @member {Object} AppBaseForm#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            if (!el.modules) {
                el.modules = {};
            }
            if (el.modules.form) {
                el.modules.form.preventSubmit();
            } else {
                el.modules.form = this;
            }

            this.preventSubmit();

            //Попап при успешной отправке формы
            if (!params.successAlert) {
                params.successPopup = params.successPopup || this.constructor.popups.success;
            }

            //Маски
            let masks;

            if (params.fioMask || params.phoneMask) {
                const maskRequire = require('Components/mask');

                const fioMasksEl = el.querySelectorAll(
                    'input[data-validate~="name"], input[data-validate~="surname"], input[data-validate~="patronymic"]'
                );
                const phoneMasksEl = el.querySelectorAll('input[type="tel"]');

                maskRequire.default.then(AppMask => {
                    masks = [];

                    if (params.fioMask) {
                        if (fioMasksEl.length > 0) {
                            const fioMask = (typeof params.fioMask instanceof RegExp) ? params.fioMask : defaultFioMask;
                            const changeFunc = input => {
                                const value = input.value;
                                if (fioMask.test(value)) {
                                    input.value = value.replace(fioMask, '');
                                }
                            };

                            fioMasksEl.forEach(input => {
                                ['input', 'change'].forEach(event => {
                                    input.addEventListener(event, () => {
                                        changeFunc(input);
                                    });
                                });
                            });
                        }
                    }

                    if (params.phoneMask) {
                        if (phoneMasksEl.length > 0) {
                            const defaultPhoneMask = AppMask.rusPhoneOptions;
                            const phoneMaskOptions = util.isObject(params.phoneMask) ? params.phoneMask : defaultPhoneMask;
                            phoneMasksEl.forEach(input => {
                                const mask = new AppMask(input, phoneMaskOptions);
                                masks.push(mask);
                            });
                        }
                    }
                });

                maskRequire.maskTrigger(fioMasksEl);
                maskRequire.maskTrigger(phoneMasksEl);
            }

            /**
             * @member {Array.<Object>} AppBaseForm#masks
             * @memberof layout
             * @desc Коллекция ссылок на модули масок для полей ввода [AppMask]{@link components.AppMask}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'masks', {
                enumerable: true,
                get: () => masks
            });

            //Валидация
            let validation;

            /**
             * @member {Object} AppBaseForm#validation
             * @memberof layout
             * @desc Ссылка на модуль валидации формы [AppValidation]{@link components.AppValidation}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'validation', {
                enumerable: true,
                get: () => validation
            });

            let ajax;

            /**
             * @member {Object} AppBaseForm#ajax
             * @memberof layout
             * @desc Ссылка на модуль ajax формы [AppAjax]{@link components.AppAjax}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'ajax', {
                enumerable: true,
                get: () => ajax
            });

            if (!util.isObject(params.fetch) || (params.fetch === null)) {
                params.fetch = {};
            }
            util.defaultsDeep(params.fetch, defaultFetchOptions);

            if (params.validation !== false) {
                const validationRequire = require('Components/validation');
                validationRequire.default.then(AppValidation => {
                    const defaultValidationOptions = {
                        validHandler: (form, event) => {
                            //TODO:use method from form module
                            let hiddenInput;
                            const {submitter} = event;
                            if (submitter) {
                                hiddenInput = document.createElement('input');
                                hiddenInput.type = 'hidden';
                                hiddenInput.name = submitter.name;
                                hiddenInput.value = submitter.value;
                                el.append(hiddenInput);
                            }
                            ajax.fetch(params.fetch);
                            if (hiddenInput) hiddenInput.remove();
                        }
                    };
                    if (!util.isObject(params.validation) || (params.validation === null)) {
                        params.validation = {};
                    }
                    util.defaultsDeep(params.validation, defaultValidationOptions);

                    validation = new AppValidation(el, params.validation);

                    const floatLabelsEl = el.querySelectorAll('[data-float-label]');
                    const floatLabels = [...floatLabelsEl].map(item => {
                        return item.modules.floatLabel;
                    });

                    //Аякс
                    const defaultAjaxOptions = {
                        done: () => {
                            if (toaster && params.successAlert) {
                                toaster.append(params.successAlert);
                            }
                            if (params.reset) {
                                el.reset();
                                floatLabels.forEach(floatLabel => {
                                    floatLabel.update();
                                });
                                el.querySelectorAll(`.${validClass}`).forEach(item => {
                                    item.classList.remove(validClass);
                                });
                            }

                            require('Components/popup').default.then(AppPopup => {
                                if (AppPopup.currentPopup) AppPopup.currentPopup.hide();
                                if (params.successPopup) params.successPopup.show();
                            });
                            emitInit('popup');
                        },
                        fail(response, responseStatus) {
                            throw responseStatus;
                        }
                    };
                    if (!util.isObject(params.ajax) || (params.ajax === null)) {
                        params.ajax = {};
                    }
                    util.defaultsDeep(params.ajax, defaultAjaxOptions);

                    ajax = new AppAjax(el, params.ajax);
                });

                validationRequire.validationTrigger(el);

                if (typeof el.dataset.validationTriggered !== 'undefined') {
                    emitInit('validation');
                }
            }

            //TODO:reset method
            //if (baseFormErrorAlert) baseFormErrorAlert.hide();
            //
            //            if (options.data && (options.data.result === false)) {
            //                baseFormErrorAlert = errorAlert();
            //                return;
            //            }
            //
            //            if (app.Popup && app.Popup.currentPopup) {
            //                app.Popup.currentPopup.hide();
            //            }
            //
            //            if (options.form) {
            //                if (!options.noReset) options.form.reset();
            //                options.form.querySelectorAll(`.${validClass}`).forEach(item => {
            //                    item.classList.remove(validClass);
            //                });
            //                options.form.querySelectorAll('[data-float-label]').forEach(item => {
            //                    if (!item.modules) return;
            //                    item.modules.floatLabel.update();
            //                });
            //                options.form.querySelectorAll('.file-item').forEach(item => {
            //                    const parentNode = item.parentNode;
            //                    item.remove();
            //                    if (parentNode.children.length === 0) {
            //                        parentNode.innerHTML = '';
            //                    }
            //                });
            //            }

            util.enable(el);

            el.classList.add(initializedClass);
        }

        /**
         * @function AppBaseForm.alerts
         * @memberof layout
         * @desc Get-функция. Объект с различными вариантами уведомлений, относящихся к формам.
         * @property {Object} success - Ссылка на модуль уведомления [AppAlert]{@link components.AppAlert} при успешно отправленной форме.
         * @property {Object} error - Ссылка на модуль уведомления [AppAlert]{@link components.AppAlert} при отправленной форме с ошибкой.
         * @returns {Object}
         */
        static get alerts() {
            return alerts;
        }

        /**
         * @function AppBaseForm.popups
         * @memberof layout
         * @desc Get-функция. Объект с различными вариантами попапов, относящихся к формам.
         * @property {Object} success - Ссылка на модуль попапа-сообщения [AppAlertPopup]{@link layout.AppAlertPopup} при успешно отправленной форме.
         * @property {Object} error - Ссылка на модуль попапа-сообщения [AppAlertPopup]{@link layout.AppAlertPopup} при отправленной форме с ошибкой.
         * @returns {Object}
         */
        static get popups() {
            return popups;
        }
    });

    addModule(moduleName, AppBaseForm);

    baseFormEl.forEach(el => new AppBaseForm(el));

    return AppBaseForm;
};

export default baseFormInit;
export {
    baseFormInit,
    AppAuthRegister
};