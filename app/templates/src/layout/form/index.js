import './style.scss';
import './auth-register/style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import util from 'Layout/main';
import AppWindow from 'Layout/window';

let importFunc; /* eslint-disable-line prefer-const */

let baseFormTriggered;

const formCallback = (resolve, isModules) => {
    const imports = [
        require('Components/ajax').default,
        require('Components/alert').default,
        require('Components/form').default,
        require('Components/svg').default,
        require('Components/toaster').default,
        require('Layout/alert-popup').default
    ];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "forms" */ './sync.js'))
            .then(formModules => {
                chunks.forms = true;
                baseFormTriggered = true;

                if (isModules) {
                    const modulesImports = [importFunc];

                    Promise.all(modulesImports).then(modulesModules => {
                        const [AppBaseForm] = modulesModules;
                        const {AppAuthRegister} = formModules;
                        resolve({
                            AppBaseForm,
                            AppAuthRegister
                        });
                    });
                    return;
                }

                const AppBaseForm = formModules.baseFormInit(...modules);
                if (resolve) resolve(AppBaseForm);
            });
    });

    emitInit('ajax', 'alert', 'form', 'svg', 'toaster', 'alertPopup');
};

const initBaseForm = event => {
    if (baseFormTriggered) return;

    if (((event.target.tagName === 'BUTTON') || (event.target.tagName === 'INPUT')) &&
        (event.target.type === 'submit')) {
        event.preventDefault();
        event.currentTarget.dataset.validationTriggered = '';
    }

    emitInit('baseForm');
};

const formTrigger = (baseFormItems, baseFormTriggers = ['click', 'input']) => {
    if (__IS_SYNC__) return;

    const items = (baseFormItems instanceof Node) ? [baseFormItems] : baseFormItems;
    if (items.length === 0) return;

    items.forEach(item => {
        baseFormTriggers.forEach(trigger => {
            item.addEventListener(trigger, initBaseForm, {once: true});
        });
        util.enable(item);
    });
};

importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        formCallback(resolve);
        return;
    }

    onInit('baseForm', () => {
        formCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('baseForm');
    });

    const baseFormItems = document.querySelectorAll('[data-base-form]');
    formTrigger(baseFormItems);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        formCallback(resolve, true);
        return;
    }

    onInit('baseForm', () => {
        formCallback(resolve, true);
    });
});

export default importFunc;
export {
    modules,
    formTrigger
};