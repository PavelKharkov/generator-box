//TODO:example

import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'headerSlideoutButton';

/**
 * @class AppHeaderSlideoutButton
 * @memberof layout
 * @classdesc Кнопка открытия выезжающего меню.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - HTML-элемент экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 */
const AppHeaderSlideoutButton = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let slideout;

        /**
         * @member {Object} AppHeaderSlideoutButton#slideout
         * @memberof layout
         * @desc Ссылка на модуль выезжающего меню [AppSlideout]{@link layout.AppSlideout}, показываемого по нажатию кнопки в хедере.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideout', {
            enumerable: true,
            get: () => slideout
        });

        const buttonEl = el.querySelector('.header-slideout-button__button');

        /**
         * @member {HTMLElement} AppHeaderSlideoutButton#buttonEl
         * @memberof layout
         * @desc Кнопка открытия выезжающего меню.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'buttonEl', {
            enumerable: true,
            value: buttonEl
        });

        if (buttonEl) {
            require('Components/slideout').default.then(AppSlideout => {
                const {baseSlideout} = AppSlideout;

                slideout = baseSlideout;
            });
            emitInit('slideout');

            util.enable(buttonEl);
            buttonEl.addEventListener('click', () => {
                if (!slideout) return;
                if (slideout.isShown) {
                    slideout.hide();
                    return;
                }
                slideout.show({
                    type: 'menu',
                    focusOnShow: false
                });
            });
        }
    }
});

addModule(moduleName, AppHeaderSlideoutButton);

export default AppHeaderSlideoutButton;
export {AppHeaderSlideoutButton};