import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'mainMenu';

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

/**
 * @class AppMainMenu
 * @memberof layout
 * @requires components#AppToggle
 * @requires layout#AppWindow
 * @classdesc Модуль главного меню.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const mainMenuInstance = new app.MainMenu(document.querySelector('.main-menu-element'));
 */
const AppMainMenu = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Коллекция внутренних раскрывающихся элементов меню
        const items = [...el.querySelectorAll('.header-main-menu__item.-level1.-has-items')];

        let itemsToggle;

        /**
         * @member {Array.<Object>} AppMainMenu#itemsToggle
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрытия внутренних элементов.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'itemsToggle', {
            enumerable: true,
            get: () => itemsToggle
        });

        if (items.length > 0) {
            //Загрузка скриптов главного меню
            const toggleRequire = require('Components/toggle');
            const toggleTriggers = ['mouseenter', 'mouseleave'];
            toggleRequire.default.then(AppToggle => {
                itemsToggle = items.forEach(item => {
                    const toggleEl = item.querySelector('.header-main-menu__container.-level2');
                    const toggle = new AppToggle(item, {
                        trigger: toggleTriggers,
                        targetElements: toggleEl,
                        duration: transitionDuration
                    });
                    return toggle;
                });
            });

            toggleRequire.toggleTrigger(items, toggleTriggers);
        }
    }
});

addModule(moduleName, AppMainMenu);

export default AppMainMenu;
export {AppMainMenu};