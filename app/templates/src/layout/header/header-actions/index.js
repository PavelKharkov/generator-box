import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import AppStateToggler from 'Layout/state-toggler';

const moduleName = 'headerActions';

/**
 * @class AppHeaderActions
 * @memberof layout
 * @requires layout#AppStateToggler
 * @classdesc Модуль кнопок действий хедера сайта.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const headerActionsInstance = new app.HeaderActions(document.querySelector('.header-actions-element'));
 */
const AppHeaderActions = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Кнопка сравнения
        let compareToggler;

        /**
         * @member {Object} AppHeaderActions#compareToggler
         * @memberof layout
         * @desc Ссылка на модуль изменения состояния кнопки сравнения.
         * @readonly
         */
        Object.defineProperty(this, 'compareToggler', {
            enumerable: true,
            get: () => compareToggler
        });

        const compareTogglerEl = el.querySelector('.header-actions__item.-compare');

        if (compareTogglerEl) {
            compareToggler = new AppStateToggler(compareTogglerEl);
        }

        //Кнопка отложенных
        let wishlistToggler;

        /**
         * @member {Object} AppHeaderActions#wishlistToggler
         * @memberof layout
         * @desc Ссылка на модуль изменения состояния кнопки добавления к отложенным.
         * @readonly
         */
        Object.defineProperty(this, 'wishlistToggler', {
            enumerable: true,
            get: () => wishlistToggler
        });

        const wishlistTogglerEl = el.querySelector('.header-actions__item.-wishlist');

        if (wishlistTogglerEl) {
            wishlistToggler = new AppStateToggler(wishlistTogglerEl);
        }

        //Кнопка корзины
        let cartToggler;

        /**
         * @member {Object} AppHeaderActions#cartToggler
         * @memberof layout
         * @desc Ссылка на модуль изменения состояния кнопки добавления в корзину.
         * @readonly
         */
        Object.defineProperty(this, 'cartToggler', {
            enumerable: true,
            get: () => cartToggler
        });

        const cartTogglerEl = el.querySelector('.header-actions__item.-cart');

        if (cartTogglerEl) {
            cartToggler = new AppStateToggler(cartTogglerEl);
        }
    }
});

addModule(moduleName, AppHeaderActions);

export default AppHeaderActions;
export {AppHeaderActions};