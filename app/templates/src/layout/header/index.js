import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

let AppHeader;
let AppCatalogMenu;
let AppHeaderActions;
let AppHeaderPersonal;
let AppMainMenu;
let AppHeaderSlideoutButton;

//Опции
const moduleName = 'header';

const headerEl = document.querySelectorAll('.header');

if (headerEl.length > 0) {
    /**
     * @class AppHeader
     * @memberof layout
     * @requires components#AppSlideout
     * @classdesc Модуль хедера.
     * @desc Наследует: [Module]{@link app.Module}.
     * @example
     * const headerInstance = new app.Header(document.querySelector('.header-element'));
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <header class="header"></header>
     *
     * <!--.header - селектор по умолчанию-->
     */
    AppHeader = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            //Главное меню
            let mainMenu;

            /**
             * @member {Array.<Object>} AppHeader#mainMenu
             * @memberof layout
             * @desc Коллекция ссылок на модули главного меню [AppMainMenu]{@link layout.AppMainMenu}.
             * @readonly
             */
            Object.defineProperty(this, 'mainMenu', {
                enumerable: true,
                get: () => mainMenu
            });

            const mainMenuEl = document.querySelectorAll('.header__main-menu');
            if (mainMenuEl.length > 0) {
                AppMainMenu = require('./main-menu').default;

                mainMenu = [...mainMenuEl].map(element => new AppMainMenu(element));
            }

            //Блок личного кабинета в хедере
            let headerPersonal;

            /**
             * @member {Array.<Object>} AppHeader#headerPersonal
             * @memberof layout
             * @desc Коллекция ссылок на модули личного кабинета в хедере [AppHeaderPersonal]{@link layout.AppHeaderPersonal}.
             * @readonly
             */
            Object.defineProperty(this, 'headerPersonal', {
                enumerable: true,
                get: () => headerPersonal
            });

            const headerPersonalEl = el.querySelectorAll('.header__personal');
            if (headerPersonalEl.length > 0) {
                AppHeaderPersonal = require('./header-personal').default;

                headerPersonal = [...headerPersonalEl].map(element => new AppHeaderPersonal(element));
            }

            //Меню каталога
            let catalogMenu;

            /**
             * @member {Array.<Object>} AppHeader#catalogMenu
             * @memberof layout
             * @desc Коллекция ссылок на модули меню каталога [AppCatalogMenu]{@link layout.AppCatalogMenu}.
             * @readonly
             */
            Object.defineProperty(this, 'catalogMenu', {
                enumerable: true,
                get: () => catalogMenu
            });

            const catalogMenuEl = document.querySelectorAll('.header__catalog-menu');
            if (catalogMenuEl.length > 0) {
                AppCatalogMenu = require('./catalog-menu').default;

                catalogMenu = [...catalogMenuEl].map(element => new AppCatalogMenu(element));
            }

            //Инициализация кнопок действий
            let headerActions;

            /**
             * @member {Array.<Object>} AppHeader#headerActions
             * @memberof layout
             * @desc Коллекция ссылок на модули кнопок действий хедера [AppHeaderActions]{@link layout.AppHeaderActions}.
             * @readonly
             */
            Object.defineProperty(this, 'headerActions', {
                enumerable: true,
                get: () => headerActions
            });

            const headerActionsEl = el.querySelectorAll('.header__actions');
            if (headerActionsEl.length > 0) {
                AppHeaderActions = require('./header-actions').default;

                headerActions = [...headerActionsEl].map(element => new AppHeaderActions(element));
            }

            //Дополнительное меню в хедере
            require('./header-menu');

            //Кнопка открытия выезжающего меню в хедере
            let slideoutButton;

            /**
             * @member {Array.<Object>} AppHeader#slideoutButton
             * @memberof layout
             * @desc Коллекция ссылок на модули кнопок открытия выезжющего меню [AppSlideoutButton]{@link layout.AppSlideoutButton}.
             * @readonly
             */
            Object.defineProperty(this, 'slideoutButton', {
                enumerable: true,
                get: () => slideoutButton
            });

            const slideoutButtonEl = document.querySelectorAll('.header__slideout-button');
            if (slideoutButtonEl.length > 0) {
                AppHeaderSlideoutButton = require('./slideout-button').default;

                slideoutButton = [...slideoutButtonEl].map(element => new AppHeaderSlideoutButton(element));
            }
        }
    });

    addModule(moduleName, AppHeader);

    //Инициализация элементов по классу
    headerEl.forEach(el => new AppHeader(el));
}

export default AppHeader;
export {
    AppHeader,
    AppCatalogMenu,
    AppHeaderActions,
    AppHeaderPersonal,
    AppMainMenu,
    AppHeaderSlideoutButton
};