import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

const moduleName = 'catalogMenu';

const isTouch = util.isTouch;

/**
 * @class AppCatalogMenu
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppSlideout
 * @requires components#AppToggle
 * @requires layout#AppSlideoutMenu
 * @classdesc Модуль меню каталога.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const catalogMenuInstance = new app.CatalogMenu(document.querySelector('.catalog-menu-element'));
 */
const AppCatalogMenu = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const button = el.querySelector('.header-catalog-menu__button');

        /**
         * @member {HTMLElement} AppCatalogMenu#button
         * @memberof layout
         * @desc Кнопка открытия меню каталога.
         * @readonly
         */
        Object.defineProperty(this, 'button', {
            enumerable: true,
            value: button
        });

        let buttonCollapse;

        /**
         * @member {Object} AppCatalogMenu#buttonCollapse
         * @memberof layout
         * @desc Ссылка на модуль раскрытия меню каталога по кнопке.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'buttonCollapse', {
            enumerable: true,
            get: () => buttonCollapse
        });

        let slideout;

        /**
         * @member {Object} AppCatalogMenu#slideout
         * @memberof layout
         * @desc Ссылка на экземпляр модуля выезжающего меню [AppSlideout]{@link layout.AppSlideout}, содержащего меню каталога.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideout', {
            enumerable: true,
            get: () => slideout
        });

        let slideoutMenu;

        /**
         * @member {Object} AppCatalogMenu#slideoutMenu
         * @memberof layout
         * @desc Ссылка на экземпляр модуля контента выезжающего меню [AppSlideoutMenu]{@link layout.AppSlideoutMenu}, содержащего меню каталога.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideoutMenu', {
            enumerable: true,
            get: () => slideoutMenu
        });

        const slideoutModules = [
            require('Components/slideout').default,
            require('Layout/slideout-menu').default
        ];
        Promise.all(slideoutModules).then(modules => {
            const [AppSlideout, AppSlideoutMenu] = modules;
            const {baseSlideout} = AppSlideout;
            const {baseSlideoutMenu} = AppSlideoutMenu;

            slideout = baseSlideout;
            slideoutMenu = baseSlideoutMenu;

            button.addEventListener('click', () => {
                if (AppWindow.width() < util.media.lg) {
                    slideout.show({
                        type: 'menu',
                        focusOnShow: false
                    });
                    slideoutMenu.showCatalogMenu();
                } else if (buttonCollapse) {
                    buttonCollapse.toggle();
                }
            });
        });

        const mainContainer = el.querySelector('.header-catalog-menu__container.-level1');

        /**
         * @member {HTMLElement} AppCatalogMenu#mainContainer
         * @memberof layout
         * @desc Элемент корневого контейнера меню.
         * @readonly
         */
        Object.defineProperty(this, 'mainContainer', {
            enumerable: true,
            value: mainContainer
        });

        const collapseRequire = require('Components/collapse');
        collapseRequire.default.then(AppCollapse => {
            buttonCollapse = new AppCollapse(button, {
                targetElements: [mainContainer],
                trigger: false,
                outsideClickHide: true
            });

            //Настройка взаимодействия с меню каталога на touch-устройствах
            if (isTouch) {
                //Внутренние ссылки каталога
                const innerLinks = el.querySelectorAll('.header-catalog-menu__link.-has-items');
                if (innerLinks.length > 0) {
                    //Удаление класса нажатого пункта меню
                    const removeClickedClass = () => {
                        innerLinks.forEach(innerLink => {
                            innerLink.classList.remove('clicked');
                        });
                    };

                    //Добавление класса нажатого пункта меню
                    const addClickedClass = link => {
                        link.classList.add('clicked');
                        const container = link.closest('.header-catalog-menu__container');
                        if (!container) return;

                        const item = container.closest('.header-catalog-menu__item');
                        if (!item) return;

                        addClickedClass(item.querySelector('.header-catalog-menu__link'));
                    };

                    innerLinks.forEach(link => {
                        link.addEventListener('click', event => {
                            if (link.classList.contains('-slideout')) return;

                            if (!link.classList.contains('clicked')) {
                                event.preventDefault();
                                removeClickedClass();
                                addClickedClass(link);
                            }
                        });
                    });
                    buttonCollapse.on('hide', () => removeClickedClass());
                }
            }
        });

        collapseRequire.collapseTrigger(button);

        //Внутренние элементы каталога
        let itemsCollapse;

        /**
         * @member {Array.<Object>} AppCatalogMenu#itemsCollapse
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрытия внутренних элементов [AppToggle]{@link components.AppToggle}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'itemsCollapse', {
            enumerable: true,
            get: () => itemsCollapse
        });

        const innerItems = el.querySelectorAll('.header-catalog-menu__item.-has-items');
        if (innerItems.length > 0) {
            const trigger = isTouch ? false : ['mouseenter', 'mouseleave'];

            const toggleRequire = require('Components/toggle');
            toggleRequire.default.then(AppToggle => {
                itemsCollapse = [];
                [...innerItems].forEach(item => {
                    const link = item.querySelector('.header-catalog-menu__link');
                    if (link.classList.contains('-slideout')) return;

                    const targetElement = item.querySelector('.header-catalog-menu__container');

                    const toggle = new AppToggle(item, {
                        trigger,
                        targetElements: [targetElement],
                        outsideClickHide: isTouch,
                        parent: item.closest('.header-catalog-menu__list')
                    });
                    if (isTouch) {
                        link.addEventListener('click', event => {
                            if (!link.classList.contains('clicked')) {
                                event.preventDefault();

                                toggle.show();
                            }
                        });
                    }
                    itemsCollapse.push(toggle);
                });
            });

            toggleRequire.toggleTrigger(innerItems);
        }
    }
});

addModule(moduleName, AppCatalogMenu);

export default AppCatalogMenu;
export {AppCatalogMenu};