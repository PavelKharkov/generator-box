import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'headerPersonal';

const profileLinkBp = util.media.lg;
const profileMenuBp = util.media.xl;

/**
 * @class AppHeaderPersonal
 * @memberof layout
 * @requires components#AppPopup
 * @requires components#AppSlideout
 * @requires layout#AppWindow
 * @classdesc Модуль блока личного кабинета хедера.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const headerPersonalInstance = new app.HeaderPersonal(document.querySelector('.header-personal-element'));
 */
const AppHeaderPersonal = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let slideout;

        /**
         * @member {Object} AppHeaderPersonal#slideout
         * @memberof layout
         * @desc Ссылка на модуль выезжающего меню [AppSlideout]{@link layout.AppSlideout}, содержащего меню каталога.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideout', {
            enumerable: true,
            get: () => slideout
        });

        let popups;

        /**
         * @member {Array.<Object>} AppHeaderPersonal#popups
         * @memberof layout
         * @desc Коллекция ссылок на модули попапов [AppPopup]{@link components.AppPopup} в клонированном блоке ЛК.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popups', {
            enumerable: true,
            get: () => popups
        });

        let clonedProfileLink;

        /**
         * @member {HTMLElement} AppHeaderPersonal#clonedProfileLink
         * @memberof layout
         * @desc Кнопка открытия ЛК в клонированном блоке.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'clonedProfileLink', {
            enumerable: true,
            get: () => clonedProfileLink
        });

        require('Components/slideout').default.then(AppSlideout => {
            const {baseSlideout} = AppSlideout;

            slideout = baseSlideout;
        });
        emitInit('slideout');

        const profileLink = el.querySelector('.header-personal__link.-profile');

        /**
         * @member {HTMLElement} AppHeaderPersonal#profileLink
         * @memberof layout
         * @desc Кнопка открытия личного кабинета.
         * @readonly
         */
        Object.defineProperty(this, 'profileLink', {
            enumerable: true,
            value: profileLink
        });

        if (profileLink) {
            /**
             * @function profileClick
             * @desc Функция, вызываемая при нажатии на ссылку открытия личного кабинета.
             * @param {Object} event - Браузерное событие нажатия.
             * @returns {undefined}
             * @ignore
             */
            const profileClick = event => {
                if (AppWindow.width() < profileMenuBp) {
                    event.preventDefault();

                    if (!slideout) return;
                    slideout.show({
                        type: 'personal',
                        focusOnShow: false
                    });
                }
            };

            /**
             * @function cloneFunc
             * @desc Клонирует блок ЛК для корректного отображения на маленьких разрешениях.
             * @returns {undefined}
             * @ignore
             */
            const cloneFunc = () => {
                if (AppWindow.width() < profileLinkBp) {
                    const headerBottomEl = document.querySelector('.header .header__actions');
                    if (!headerBottomEl) return;

                    const clonedNode = el.cloneNode(true);
                    clonedNode.classList.add('-mobile');
                    headerBottomEl.parentNode.insertBefore(clonedNode, headerBottomEl.nextSibling);

                    AppWindow.off('resize', cloneFunc);

                    const popupItems = clonedNode.querySelectorAll('[data-popup]');
                    if (popupItems.length > 0) {
                        const popupRequire = require('Components/popup');
                        popupRequire.default.then(AppPopup => {
                            popups = [...popupItems].map(element => {
                                const popupInstance = new AppPopup(element);
                                return popupInstance;
                            });
                        });

                        popupRequire.popupTrigger(popupItems);
                    }

                    clonedProfileLink = clonedNode.querySelector('.header-personal__link.-profile');
                    if (clonedProfileLink) {
                        clonedProfileLink.addEventListener('click', event => profileClick(event));
                    }
                }
            };

            profileLink.addEventListener('click', event => profileClick(event));

            AppWindow.on('resize', cloneFunc);
        }
    }
});

addModule(moduleName, AppHeaderPersonal);

export default AppHeaderPersonal;
export {AppHeaderPersonal};