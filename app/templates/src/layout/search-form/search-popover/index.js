import {immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import './ajax/search-suggestions.html';

const moduleName = 'searchPopover';

const searchPopoverInit = (firstInit, AppPopover) => {
    /**
     * @class AppSearchPopover
     * @memberof layout
     * @classdesc Выпадающий блок подсказок поиска.
     * @desc Наследует: [AppPopover]{@link components.AppPopover}.
     * @async
     * @param {HTMLElement} [element] - Элемент выпадающего блока.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @example
     * const searchPopoverInstance = new app.SearchPopover(document.querySelector('.search-popover'));
     */
    const AppSearchPopover = immutable(class extends AppPopover {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);
        }
    });

    if (firstInit) addModule(moduleName, AppSearchPopover);

    return AppSearchPopover;
};

export default searchPopoverInit;
export {searchPopoverInit};