import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppPreloader from 'Components/preloader';

let AppSearchForm;
let AppSearchPopover;

//Опции
const moduleName = 'searchForm';

const searchFormEl = document.querySelectorAll('.search');

let firstInit = true;

//Опции по умолчанию
const defaultOptions = {
    popover: {
        placement: 'bottom',
        outsideClickHide: false,
        trigger: '',
        class: '-search'
    }
};

if (searchFormEl.length > 0) {
    /**
     * @class AppSearchForm
     * @memberof layout
     * @requires components#AppForm
     * @requires components#AppValidation
     * @classdesc Модуль формы поиска.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @param {app.Module.instanceOptions} [options.popover] - Опции экземпляра выпадающего блока подсказок. Повторяют опции [AppPopover]{@link components.AppPopover}.
     * @param {Function} [options.processPopoverData] - Функция, обрабатывающая содержимое подсказок поиска перед заполнением. По умолчанию, функция заполняет не заполняет никакие данные.
     * @example
     * const searchFormInstance = new app.SearchForm(document.querySelector('.search-form-element'));
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="search">
     *     <form class="search__form">
     *         <input class="search__input" type="search" placeholder="Поиск" data-validate="required">
     *         <button class="search__button" type="submit" title="Искать"></button>
     *     </form>
     * </div>
     *
     * <!--.search - селектор по умолчанию-->
     */
    AppSearchForm = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            //TODO:add data-search-options

            util.extend(this.options, defaultOptions);

            /**
             * @member {Object} AppSearchForm#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            const formEl = el.querySelector('.search__form');
            if (!formEl) return;
            if (!formEl.modules) {
                formEl.modules = {};
            }
            formEl.modules[moduleName] = this;

            /**
             * @member {HTMLElement} AppSearchForm#formEl
             * @memberof layout
             * @desc Элемент формы поиска.
             * @readonly
             */
            Object.defineProperty(this, 'formEl', {
                enumerable: true,
                value: formEl
            });

            const input = el.querySelector('.search__input');
            if (!input.modules) {
                input.modules = {};
            }
            input.modules[moduleName] = this;

            /**
             * @member {HTMLElement} AppSearchForm#input
             * @memberof layout
             * @desc Элемент поля ввода формы поиска.
             * @readonly
             */
            Object.defineProperty(this, 'input', {
                enumerable: true,
                value: input
            });

            let form;

            /**
             * @member {Object} AppSearchForm#form
             * @memberof layout
             * @desc Ссылка на модуль формы [AppForm]{@link components.AppForm} поиска.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'form', {
                enumerable: true,
                get: () => form
            });

            let validation;

            /**
             * @member {Object} AppSearchForm#validation
             * @memberof layout
             * @desc Ссылка на модуль валидации [AppValidation]{@link components.AppValidation} формы поиска.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'validation', {
                enumerable: true,
                get: () => validation
            });

            const formRequire = require('Components/form');
            formRequire.default.then(AppForm => {
                form = (formEl.modules && formEl.modules.form) ? formEl.modules.form : new AppForm(formEl);

                const validationRequire = require('Components/validation');
                validationRequire.default.then(AppValidation => {
                    validation = new AppValidation(formEl);
                });

                validationRequire.validationTrigger(formEl);
            });

            formRequire.formTrigger(formEl);

            //Подсказки поиска
            let popover;

            /**
             * @member {Object} AppSearchForm#popover
             * @memberof layout
             * @desc Ссылка на модуль подсказок поиска [AppSearchPopover]{@link components.AppSearchPopover} формы поиска.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'popover', {
                enumerable: true,
                get: () => popover
            });

            const searchPopoverEl = el.querySelector('.search__popover');
            if (searchPopoverEl) {
                util.defaultsDeep(params.popover, {
                    container: formEl,
                    content: searchPopoverEl
                });

                const popoverRequire = require('Components/popover');
                popoverRequire.default.then(AppPopover => {
                    const searchPopoverInit = require('./search-popover').default;
                    AppSearchPopover = searchPopoverInit(firstInit, AppPopover);

                    popover = new AppSearchPopover(input, params.popover);
                    firstInit = false;

                    popover.on('hidden', () => {
                        popover.options.container.append(popover.params.content);
                    });
                });
                emitInit('popover');

                let ajaxInstance;
                require('Components/ajax').default.then(AppAjax => {
                    ajaxInstance = new AppAjax(formEl, {
                        url: formEl.dataset.suggestUrl,
                        method: 'get',
                        responseType: 'text',
                        preloader: false
                    });
                });
                emitInit('ajax');

                //TODO:move to main funcs, add docs
                const startHighlightTag = '<mark class="search-highlight">';
                const endHighlightTag = '</mark>';
                const markText = (text, searchArray) => {
                    if (!text || !Array.isArray(searchArray) || (searchArray.length === 0)) {
                        return text;
                    }

                    let markedText = text;
                    const searchRegExp = new RegExp(`(${searchArray.join('|')})`, 'gi');
                    if (searchRegExp.test(markedText)) {
                        markedText = text.replace(searchRegExp, match => {
                            return startHighlightTag + match + endHighlightTag;
                        });
                    }

                    return markedText;
                };

                const preloader = new AppPreloader(el, {
                    size: 'sm',
                    overlay: false
                });

                /**
                 * @member {Object} AppSearchForm#preloader
                 * @memberof layout
                 * @desc Ссылка на модуль индикатора загрузки [AppPreloader]{@link components.AppPreloader} формы поиска.
                 * @async
                 * @readonly
                 */
                Object.defineProperty(this, 'preloader', {
                    enumerable: true,
                    value: preloader
                });

                const popoverContent = el.querySelector('.search-popover__content');
                let isInit = true;
                let isFocused = false;
                let hasResults = false;
                let ajaxResult;
                const inputFunc = util.debounce(() => {
                    if (!input.value) {
                        hasResults = false;
                        popover.hide();
                        return;
                    }

                    preloader.show();
                    if (ajaxResult) ajaxResult.abort();
                    ajaxResult = ajaxInstance.fetch({
                        done(data) {
                            preloader.hide();

                            if (data.trim()) {
                                hasResults = true;
                                popoverContent.innerHTML = data;
                                popoverContent.querySelectorAll('.search-popover-item__title').forEach(item => {
                                    const searchArray = input.value.split(' ');
                                    const resultText = markText(item.textContent, searchArray);
                                    item.innerHTML = resultText;
                                });
                                if (isFocused && !popover.isShown) popover.show();
                                return;
                            }

                            popover.hide();
                        }
                    });
                }, 500);

                //TODO:add docs
                const inputFocus = () => {
                    isFocused = true;
                    if (hasResults) popover.show();
                    if (isInit) {
                        isInit = false;
                        inputFunc();
                    }
                };
                input.addEventListener('click', inputFocus);
                input.addEventListener('focus', inputFocus);
                input.addEventListener('input', inputFunc);
                document.addEventListener('click', event => {
                    if (!popover || !popover.isShown || (event.target === input) || event.target.closest('.popover.-search')) {
                        return;
                    }

                    isFocused = false;
                    popover.hide();
                });

                if (typeof el.dataset.searchFormTriggered !== 'undefined') {
                    delete el.dataset.searchFormTriggered;
                    inputFocus();
                }
            }
        }
    });

    addModule(moduleName, AppSearchForm);

    //Инициализация элементов по классу
    searchFormEl.forEach(el => new AppSearchForm(el));
}

export default AppSearchForm;
export {
    AppSearchForm,
    AppSearchPopover
};