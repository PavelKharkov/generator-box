import './style.scss';
import './search-popover/style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

let searchFormTriggered;

const searchFormCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "forms" */ './sync.js'))
        .then(modules => {
            chunks.forms = true;
            searchFormTriggered = true;

            if (isModules) {
                const {AppSearchForm, AppSearchPopover} = modules;
                resolve({
                    AppSearchForm,
                    AppSearchPopover
                });
                return;
            }

            const AppSearchForm = modules.default;
            if (resolve) resolve(AppSearchForm);
        });
};

const initSearchForm = event => {
    if (searchFormTriggered) return;

    if (event.target.tagName === 'INPUT') {
        event.currentTarget.dataset.searchFormTriggered = '';
    }

    emitInit('searchForm');
};

const searchFormTrigger = (searchFormItems, searchFormTriggers = ['click', 'input']) => {
    if (__IS_SYNC__) return;

    const items = (searchFormItems instanceof Node) ? [searchFormItems] : searchFormItems;
    if (items.length === 0) return;

    items.forEach(item => {
        searchFormTriggers.forEach(trigger => {
            item.addEventListener(trigger, initSearchForm, {once: true});
        });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        searchFormCallback(resolve);
        return;
    }

    onInit('searchForm', () => {
        searchFormCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('searchForm');
    });

    const searchFormItems = document.querySelectorAll('.search');
    searchFormTrigger(searchFormItems);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.forms) {
        searchFormCallback(resolve, true);
        return;
    }

    onInit('searchForm', () => {
        searchFormCallback(resolve, true);
    });
});

export default importFunc;
export {
    modules,
    searchFormTrigger
};