/**
 * @module spectrum
 * @memberof libs
 * @requires libs.jquery
 * @version 1.8.1
 * @desc [Сайт]{@link http://bgrins.github.io/spectrum/}.
 * @ignore
 */

import 'libs/jquery';

import 'Src/../../../node_modules/spectrum-colorpicker/spectrum.js'; //Модуль хранится в папке node_modules генератора

import 'Src/../../../node_modules/spectrum-colorpicker/spectrum.css';