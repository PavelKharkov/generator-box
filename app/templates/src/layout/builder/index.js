let importFunc;

if (__IS_BUILDER__) {
    const imports = [
        require('Components/form').default,
        require('Components/ajax').default
    ];

    importFunc = Promise.all(imports).then(modules => {
        return (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "builder" */ './sync.js'))
            .then(builder => {
                return builder.builderInit(...modules);
            });
    });
}

export default importFunc;