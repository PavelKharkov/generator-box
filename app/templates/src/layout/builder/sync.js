const builderInit = (AppForm, AppAjax) => {
    const jQuery = require('Libs/jquery').default; //Используется для плагина spectrum
    require('./spectrum');

    require('./style.scss');

    const builderEl = document.querySelector('.builder');

    if (builderEl) {
        const builder = {};

        (function() {
            //
            this.formModules = [];

            //
            this.ajaxModules = [];

            //
            this.message = builderEl.querySelector('.builder__message');

            //
            this.forms = builderEl.querySelectorAll('.builder__item');
            this.forms.forEach(form => {
                this.formModules.push(form.modules.form);

                this.colorpickers = [...form.querySelectorAll('input[type="color"]')].map(input => {
                    const colorpicker = jQuery(input).spectrum({
                        preferredFormat: 'hex'
                    });
                    return colorpicker;
                });

                const ajax = new AppAjax(form, {
                    requestType: 'urlencoded',
                    done: () => {
                        // this.message.classList.remove('error');
                        // this.message.classList.add('success');
                        // this.message.textContent = data.messageText;
                    },
                    fail: () => {
                        // this.message.classList.remove('success');
                        // this.message.classList.add('error');
                        // this.message.textContent = data.messageText;
                    }
                });
                this.ajaxModules.push(ajax);
            });

            window.builder = this;
        }).bind(builder)();
    }
};

export default builderInit;
export {builderInit};