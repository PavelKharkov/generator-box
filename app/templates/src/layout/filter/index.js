import './style.scss';

const filterEl = document.querySelectorAll('.filter');
const filterPreloaders = [];

if (filterEl.length > 0) {
    if (!__IS_SYNC__) {
        const AppPreloader = require('Components/preloader').default;
        filterEl.forEach(item => {
            const filterPreloader = new AppPreloader(item, {
                overlay: false,
                once: true
            });
            filterPreloader.show();
            filterPreloaders.push(filterPreloader);
        });
    }
}

const filterCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "catalog" */ './sync.js'))
        .then(filterModules => {
            if (!__IS_SYNC__) {
                filterPreloaders.forEach(filterPreloader => {
                    filterPreloader.hide();
                });
            }
            const {AppFilter} = filterModules;

            if (isModules) {
                const {AppFilterBlock} = filterModules;
                resolve({
                    AppFilter,
                    AppFilterBlock
                });
                return;
            }

            resolve(AppFilter);
        });
};

const importFunc = new Promise(resolve => {
    filterCallback(resolve);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    filterCallback(resolve, true);
});

export default importFunc;
export {modules};