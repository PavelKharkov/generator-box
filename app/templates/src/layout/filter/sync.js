import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

let AppFilterBlock;

//Опции
const moduleName = 'filter';

//Классы и селекторы
const filterClass = moduleName;
const filterSelector = `.${filterClass}`;

const slideoutClass = 'slideout';
const slideoutFilterClass = `${slideoutClass}__${filterClass}`;
const slideoutFilterInnerClass = `${slideoutClass}-${filterClass}__inner`;

//Другие опции
const filterPlacementBp = util.media.xl;

const filterEl = document.querySelectorAll(filterSelector);

/**
 * @class AppFilter
 * @memberof layout
 * @requires components#AppSlideout
 * @requires layout#AppWindow
 * @classdesc Модуль формы фильтра.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const filterInstance = new app.Filter(document.querySelector('.filter-container'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="filter"></div>
 *
 * <!--.filter - селектор по умолчанию-->
 */
const AppFilter = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let slideoutMarkupInitialized = false;

        /**
         * @member {boolean} AppFilter#slideoutMarkupInitialized
         * @memberof layout
         * @desc Указывает, проинициализирована ли разметка выезжающего меню для фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'slideoutMarkupInitialized', {
            enumerable: true,
            get: () => slideoutMarkupInitialized
        });

        //Инициализация блоков фильтра
        const filterBlockEl = el.querySelectorAll('.filter__block');

        let blocks;

        /**
         * @member {Array.<Object>} AppFilter#blocks
         * @memberof layout
         * @desc Коллекция модулей блоков фильтра [AppFilterBlock]{@link layout.AppFilterBlock}.
         * @readonly
         */
        Object.defineProperty(this, 'blocks', {
            enumerable: true,
            get: () => blocks
        });

        if (filterBlockEl.length > 0) {
            AppFilterBlock = require('./filter-block').default;

            blocks = [...filterBlockEl].map(element => {
                const filterBlock = new AppFilterBlock(element);
                return filterBlock;
            });
        }

        /**
         * @function resetInnerFunc
         * @desc Сбрасывает изменения формы фильтра.
         * @fires layout.AppFilterBlock#reset
         * @returns {undefined}
         * @ignore
         */
        const resetInnerFunc = () => {
            blocks.forEach(block => {
                block.reset();
            });
            //TODO:fix blinking
        };

        const form = el.querySelector('.filter__form');

        /**
         * @member {HTMLElement} AppFilter#form
         * @memberof layout
         * @desc Элемент формы фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'form', {
            enumerable: true,
            value: form
        });

        if (form) {
            form.addEventListener('reset', () => {
                this.reset();
            });
        }

        let slideout;

        /**
         * @member {Object} AppFilter#slideout
         * @memberof layout
         * @desc Выезжающее меню [AppSlideout]{@link layout.AppSlideout} для фильтра.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideout', {
            enumerable: true,
            get: () => slideout
        });

        let slideoutInner;

        /**
         * @member {HTMLElement} AppFilter#slideoutInner
         * @memberof layout
         * @desc Контентная область выезжающего меню фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'slideoutInner', {
            enumerable: true,
            get: () => slideoutInner
        });

        /**
         * @function slideoutMarkupInit
         * @desc Инициализирует HTML-шаблон выезжающего меню для фильтра.
         * @returns {undefined}
         * @ignore
         */
        const slideoutMarkupInit = () => {
            const slideoutFilterEl = document.createElement('div');
            slideoutFilterEl.classList.add(slideoutFilterClass);
            const markup = `
            <div class="${slideoutFilterInnerClass}"></div>`;
            slideoutFilterEl.innerHTML = markup;

            slideout.content.append(slideoutFilterEl);

            slideoutInner = slideout.content.querySelector(`.${slideoutFilterInnerClass}`);

            slideoutMarkupInitialized = true;
        };

        let sidebar;

        /**
         * @member {HTMLElement} AppFilter#sidebar
         * @memberof layout
         * @desc Элемент контейнера для фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'sidebar', {
            enumerable: true,
            get: () => sidebar
        });

        /**
         * @function slideoutInit
         * @desc Инициализирует скрипты для выезжающего меню фильтра.
         * @fires layout.AppWindow.resize
         * @returns {undefined}
         * @ignore
         */
        const slideoutInit = () => {
            if (!slideoutMarkupInitialized) slideoutMarkupInit();

            sidebar = document.querySelector('.sidebar__inner');
            if (!sidebar) return;

            AppWindow.on('resize', () => {
                if (AppWindow.width() < filterPlacementBp) {
                    const sidebarFilter = sidebar.querySelector(filterSelector);
                    if (sidebarFilter) slideoutInner.append(sidebarFilter);
                    return;
                }

                const slideoutFilter = slideoutInner.querySelector(filterSelector);
                if (slideoutFilter) sidebar.append(slideoutFilter);
            });
            AppWindow.emit('resize');
        };

        this.on({
            /**
             * @event AppFilter#reset
             * @memberof layout
             * @desc Сбрасывает значения полей всех блоков фильтра.
             * @fires layout.AppFilterBlock#reset
             * @returns {undefined}
             * @example
             * filterInstance.on('reset', () => {
             *     console.log('Значения формы были сброшены');
             * });
             * filterInstance.reset();
             */
            reset() {
                util.defer(resetInnerFunc);
            }
        });

        require('Components/slideout').default.then(AppSlideout => {
            const {baseSlideout} = AppSlideout;

            slideout = baseSlideout;
            slideout.on('show', options => {
                if ((options.type === 'filter') && !slideout.shownBefore.filter) {
                    slideoutInit();
                }
            });
        });

        let resultPopover;

        /**
         * @member {Object} AppFilter#resultPopover
         * @memberof layout
         * @desc Экземлпяр класса [AppPopover]{@link components.AppPopover} элемента результатов фильтра.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'resultPopover', {
            enumerable: true,
            get: () => resultPopover
        });

        const resultPopoverEl = el.querySelector('.filter__result-popover');
        if (resultPopoverEl) {
            require('Components/popover').default.then(AppPopover => {
                resultPopover = new AppPopover(resultPopoverEl);
            });
            emitInit('popover');
        }
    }
});

addModule(moduleName, AppFilter);

filterEl.forEach(el => new AppFilter(el));

export default AppFilter;
export {AppFilter, AppFilterBlock};