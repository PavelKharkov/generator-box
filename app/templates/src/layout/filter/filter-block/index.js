import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//import AppValidation from 'Components/validation'; //TODO:use
import AppWindow from 'Layout/window';

//Опции
const moduleName = 'filterBlock';

const filterSlideoutBreakpoint = util.media.lg;

//Классы и селекторы
const filterClass = 'filter';
const filterSelector = `.${filterClass}`;

const blockTitleInner = '.filter-block-title__inner';
const blockPopoverLink = '.filter-block-title__popover-link';

const hasCheckedClass = 'has-checked';

/**
 * @class AppFilterBlock
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppPopover
 * @requires components#AppRange
 * @requires components#AppSelect
 * @requires layout#AppWindow
 * @classdesc Модуль для блоков фильтра.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} [element] - Элемент блока формы.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {HTMLElement} [options.filter] - Элемент родительского фильтра [AppFilter]{@link layout.AppFilter}. По умолчанию ищет элемент родительского фильтра по селектору .filter.
 * @example
 * const filterBlockInstance = new app.FilterBlock(document.querySelector('.filter-block'), {
 *     filter: document.querySelector('.filter-container')
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="filter__block"></div>
 *
 * <!--.filter__block - селектор по умолчанию (внутри .filter)-->
 */
const AppFilterBlock = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        if (!this.options.filter) {
            this.options.filter = el.closest(filterSelector);
        }
        if (!this.options.filter) return;

        /**
         * @member {Object} AppFilterBlock#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const filter = params.filter.modules.filter;

        /**
         * @member {Object} AppFilterBlock#filter
         * @memberof layout
         * @desc Ссылка на модуль родительского фильтра [AppFilter]{@link layout.AppFilter}.
         * @readonly
         */
        Object.defineProperty(this, 'filter', {
            enumerable: true,
            value: filter
        });
        if (!filter) return;

        //Сохранение элементов
        const blockMoreButton = el.querySelector('.filter-block-more__button');
        const blockResetButton = el.querySelector('.filter-block__button.-reset');
        const selectEl = el.querySelectorAll('.filter-block__select');
        const popoverEl = el.querySelectorAll(blockPopoverLink);
        const rangeEl = el.querySelectorAll('.filter-block__range');
        const collapseEl = el.querySelector(blockTitleInner);

        const inputsEl = el.querySelectorAll('input[type="text"], input[type="checkbox"], input[type="radio"], select, textarea');
        const textInputEl = el.querySelectorAll('input[type="text"]');
        const checkboxInputEl = el.querySelectorAll('input[type="checkbox"]');
        const radioInputEl = el.querySelectorAll('input[type="radio"]');
        const selectInputEl = el.querySelectorAll('select');

        //Проверяет наличие изменений в блоке
        const hasCheckedTest = () => {
            let isChecked = false;

            //Тест текстовых полей
            [...textInputEl].some(element => {
                const value = element.value.trim();
                if ((value !== '') && (util.removeDelimiters(value) !== element.dataset.default)) {
                    isChecked = true;
                    return true;
                }
                return false;
            });

            //Тест выпадающих списков
            if (!isChecked) {
                [...selectInputEl].some(element => {
                    const value = element.value;
                    if (value !== element.dataset.default) {
                        isChecked = true;
                        return true;
                    }
                    return false;
                });
            }

            //Тест чекбоксов
            if (!isChecked) {
                [...checkboxInputEl].some(element => {
                    if (element.checked) {
                        isChecked = true;
                        return true;
                    }
                    return false;
                });
            }

            //Тест радиокнопок
            if (!isChecked) {
                [...radioInputEl].some(element => {
                    if (element.checked && (typeof element.dataset.default === 'undefined')) {
                        isChecked = true;
                        return true;
                    }
                    return false;
                });
            }

            //Смена класса
            el.classList[isChecked ? 'add' : 'remove'](hasCheckedClass);

            return isChecked;
        };

        if (radioInputEl.length > 0) {
            const defaultRadio = el.querySelector('input[type="radio"][data-default]');
            if (!defaultRadio) {
                radioInputEl[0].dataset.default = '';
            }
        }

        let selects;

        /**
         * @member {Array.<Object>} AppFilterBlock#selects
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих списков в блоках фильтра [AppSelect]{@link components.AppSelect}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'selects', {
            enumerable: true,
            get: () => selects
        });

        //Инициализация выпадающих списков
        if (selectEl.length > 0) {
            const selectRequire = require('Components/select');
            selectRequire.default.then(AppSelect => {
                selects = [...selectEl].map(element => {
                    const select = new AppSelect(element);
                    return select;
                });
            });

            selectRequire.selectTrigger(el);
        }

        let popovers;

        /**
         * @member {Array.<Object>} AppFilterBlock#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули информеров в блоках фильтра [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        //Инициализация информеров
        if (popoverEl.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...popoverEl].map(item => {
                    const popover = new AppPopover(item, {
                        container() {
                            return (AppWindow.width() >= filterSlideoutBreakpoint) ? document.body : filter.slideout.slideoutEl;
                        },
                        placement() {
                            return (AppWindow.width() >= filterSlideoutBreakpoint) ? 'right' : 'bottom';
                        },
                        trigger: 'click'
                    });

                    item.addEventListener('click', event => {
                        event.preventDefault();
                        event.stopPropagation();
                    });

                    return popover;
                });
            });

            popoverRequire.popoverTrigger(popoverEl);
        }

        //Инициализация слайдеров диапазонов
        let ranges;

        /**
         * @member {Array.<Object>} AppFilterBlock#ranges
         * @memberof layout
         * @desc Коллекция ссылок на модули слайдеров диапазонов в блоках фильтра [AppRange]{@link components.AppRange}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'ranges', {
            enumerable: true,
            get: () => ranges
        });

        if (rangeEl.length > 0) {
            const rangeRequire = require('Components/range');
            rangeRequire.default.then(AppRange => {
                ranges = [...rangeEl].map(element => {
                    const range = new AppRange(element);
                    range.on('change', () => {
                        hasCheckedTest();
                    });
                    return range;
                });
            });

            rangeRequire.rangeTrigger(el);
        }

        let collapse;

        /**
         * @member {Object} AppFilterBlock#collapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося блока фильтра [AppCollapse]{@link components.AppCollapse}.
         * @async
         */
        Object.defineProperty(this, 'collapse', {
            enumerable: true,
            get: () => collapse
        });

        let collapseDefaultActive;

        /**
         * @member {boolean} AppFilterBlock#collapseDefaultActive
         * @memberof layout
         * @desc Указывает, раскрыт ли блок фильтра по умолчанию.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapseDefaultActive', {
            enumerable: true,
            get: () => collapseDefaultActive
        });

        let moreCollapse;

        /**
         * @member {Object} AppFilterBlock#moreCollapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося блока внутреннего контента блока фильтра [AppCollapse]{@link components.AppCollapse}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'moreCollapse', {
            enumerable: true,
            get: () => moreCollapse
        });

        const collapseRequire = require('Components/collapse');
        collapseRequire.default.then(AppCollapse => {
            //Инициализация модулей раскрывающихся блоков
            if (collapseEl) {
                if (collapseEl.getAttribute('href')) {
                    collapse = new AppCollapse(collapseEl);
                    collapseDefaultActive = collapse.isShown;
                }
            }

            //Инициализация кнопки "Показать ещё"
            if (blockMoreButton) {
                moreCollapse = new AppCollapse(blockMoreButton);
            }
        });

        if (collapseEl) {
            collapseRequire.collapseTrigger(collapseEl);
        }
        if (blockMoreButton) {
            collapseRequire.collapseTrigger(blockMoreButton);
        }

        //Кнопка "Сбросить"
        if (blockResetButton) {
            blockResetButton.addEventListener('click', event => {
                event.preventDefault();
                event.stopPropagation();

                this.reset();
            });
        }

        //Браузерные события
        inputsEl.forEach(element => {
            element.addEventListener('change', () => {
                hasCheckedTest();
            });
        });

        /**
         * @function AppFilterBlock#hasCheckedTest
         * @memberof layout
         * @desc Проверяет наличие изменений в блоке.
         * @returns {boolean} Имеются ли в блоке фильтра изменения.
         * @readonly
         * @example
         * filterBlockInstance.hasCheckedTest();
         */
        Object.defineProperty(this, 'hasCheckedTest', {
            enumerable: true,
            value: hasCheckedTest
        });

        this.on({
            /**
             * @event AppFilterBlock#reset
             * @memberof layout
             * @desc Сбрасывает значения полей блока до значений по умолчанию.
             * @returns {undefined}
             * @example
             * filterBlockInstance.on('reset', () => {
             *     console.log('Значения полей блока сброшены');
             * });
             * filterBlockInstance.reset();
             */
            reset() {
                //Сброс полей ввода
                inputsEl.forEach(element => {
                    if (/radio|checkbox/i.test(element.type)) {
                        element.checked = element.defaultChecked;
                        return;
                    }

                    const {defaultValue} = element;
                    element.value = defaultValue;
                    element.setAttribute('value', defaultValue);
                });

                //Сброс слайдеров диапазонов
                if (ranges) {
                    ranges.forEach(range => {
                        range.reset();
                    });
                }

                //Сброс выпадающих списков
                if (selects) {
                    selects.forEach(select => {
                        select.reset();
                    });
                }

                //Сброс внутреннего раскрывающегося блока
                if (moreCollapse) {
                    moreCollapse.hide();
                }

                //Сброс раскрытых блоков
                if (collapse) {
                    collapse[collapseDefaultActive ? 'show' : 'hide']();
                }

                //Проверка на изменённые блоки фильтра
                hasCheckedTest();
            },

            /**
             * @event AppFilterBlock#zeroing
             * @memberof layout
             * @desc Обнуляет значения полей блока.
             * @returns {undefined}
             * @example
             * filterBlockInstance.on('zeroing', () => {
             *     console.log('Значения полей блока обнулены');
             * });
             * filterBlockInstance.zeroing();
             */
            zeroing() {
                //Обнуление полей ввода
                inputsEl.forEach(element => {
                    if (/radio|checkbox/i.test(element.type)) {
                        element.checked = false;
                        return;
                    }

                    element.value = '';
                    element.setAttribute('value', '');
                });

                //Обнуление слайдеров диапазонов
                if (ranges) {
                    ranges.forEach(range => {
                        range.set([range.params.min, range.params.max]);
                    });
                }

                //TODO:selects zeroing
                //Обнуление выпадающих списков
                if (selects) {
                    //selects.forEach(select => {
                    //    select.reset();
                    //});
                }

                //Скрытие внутреннего раскрывающегося блока
                if (moreCollapse) {
                    moreCollapse.hide();
                }

                //Скрытие раскрытых блоков
                if (collapse) {
                    collapse.hide();
                }

                //Проверка на изменённые блоки фильтра
                hasCheckedTest();
            }
        });

        //Показывать иконку галочки, если что-либо изменено в блоке
        hasCheckedTest();
    }
});

addModule(moduleName, AppFilterBlock);

export default AppFilterBlock;
export {AppFilterBlock};