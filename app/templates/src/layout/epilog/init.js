import AppWindow from 'Layout/window';

/**
 * @function epilog
 * @desc Функция, вызываемая после загрузки всех синхронных модулей после загрузки DOM.
 * @returns {undefined}
 * @ignore
 */
const epilog = () => {
    AppWindow.emit('deferredInit');

    if (!__IS_DIST__ || __IS_DEBUG__) {
        if (window.debug) window.log('Sync scripts end', 'danger');
    }
};

export default epilog;