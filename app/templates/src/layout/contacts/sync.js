import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'contacts';

const contactsClass = moduleName;
const contactsEl = document.querySelector(`.${contactsClass}`);

/**
 * @class AppContacts
 * @memberof layout
 * @classdesc Модуль страницы контактов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const contactsInstance = new app.Contacts(document.querySelector('.contacts-element'));
 */
const AppContacts = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const mapContainer = el.querySelector('.contacts__map');

        const mapOptionsData = mapContainer.dataset.mapOptions;
        if (mapOptionsData) {
            const dataOptions = util.stringToJSON(mapOptionsData);
            if (!dataOptions) util.error('incorrect data-map-options format');
            util.extend(this.options, dataOptions);
        }

        /**
         * @member {Object} AppContacts#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        const placemarkCoords = params.placemarkCoords && params.placemarkCoords.split(',');
        if (!Array.isArray(placemarkCoords)) {
            util.error({
                message: 'placemarkCoords are not found on map container',
                element: mapContainer
            });
        }

        let map;

        /**
         * @member {Object} AppContacts#map
         * @memberof layout
         * @desc Экземпляр модуля карт [AppMap]{@link components.AppMap} на странице контактов.
         * @readonly
         */
        Object.defineProperty(this, 'map', {
            enumerable: true,
            get: () => map
        });

        require('Components/map').default.then(AppMap => {
            map = new AppMap(mapContainer, {
                init: false
            });

            map.on('init', () => {
                const maps = AppMap.maps;
                const mapInstance = new maps.Map(mapContainer, {
                    center: placemarkCoords,
                    zoom: 15,
                    controls: ['zoomControl']
                });

                //Добавление точки
                const placemarkLayout = maps.templateLayoutFactory.createClass(`
                    <div class="${contactsClass}-map__placemark">
                        <div class="${contactsClass}-map-placemark__image"></div>
                    </div>
                `);
                const placemarkConfig = {
                    hintContent: params.placemarkContent
                };
                const placemarkOptions = {
                    iconLayout: placemarkLayout,
                    iconShape: {
                        type: 'Rectangle',
                        coordinates: [[-25, -25], [25, 25]]
                    }
                };

                const placemark = new maps.Placemark(mapInstance.getCenter(), placemarkConfig, placemarkOptions);
                mapInstance.geoObjects.add(placemark);

                //Настройка поведений карты
                mapInstance.behaviors.disable('scrollZoom');
                if (util.isTouch) {
                    mapInstance.behaviors.disable('drag');
                }
            });
        });
        emitInit('map');
    }
});

addModule(moduleName, AppContacts);

new AppContacts(contactsEl); /* eslint-disable-line no-new */

//TODO:map togglePlacemarkCoords function
//
// const zoom = _map.getZoom();
// const projection = _map.options.get('projection');
// const converter = _map.converter;
// const placemarkCoordsPx = converter.globalToPage(projection.toGlobalPixels(_placemarkCoords, zoom));
// _placemarkCoordsDesktop = projection.fromGlobalPixels(
// converter.pageToGlobal([placemarkCoordsPx[0] + 180, placemarkCoordsPx[1]]), zoom);