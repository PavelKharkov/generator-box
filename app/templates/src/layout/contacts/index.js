import './style.scss'; //TODO:make async styles

import chunks from 'Base/scripts/chunks.js';

let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const contactsCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "contacts" */ './sync.js'))
        .then(modules => {
            chunks.contacts = true;

            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppContacts = modules.default;
            resolve(AppContacts);
        });
};

const importFunc = new Promise(resolve => {
    contactsCallback(resolve);
});

export default importFunc;