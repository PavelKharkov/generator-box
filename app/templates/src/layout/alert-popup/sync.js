import {immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const lang = (window.lang && window.lang.alertPopup) || require(`./lang/${__LANG__}.json`);

//Опции
const moduleName = 'alertPopup';

//Селекторы и классы
const popupClass = 'popup';
const popupIconClass = `${popupClass}-header__icon`;

//Опции по умолчанию
const defaultOptions = {
    headerAddClass: '-center',
    textAddClass: '-center',
    popupClass: '-alert'
};

//Настройки иконок
const successIconConfig = {
    id: 'check',
    width: 40,
    height: 40
};
const errorIconConfig = {
    id: 'close',
    width: 40,
    height: 40
};

/**
 * @function iconTemplate
 * @desc Шаблон иконок.
 * @param {string} [iconClass] - Дополнительный класс контейнера иконки.
 * @param {string} [iconHtml] - HTML-код элемента иконки.
 * @returns {string} HTML-код шаблона иконки.
 * @ignore
 */
const iconTemplate = (iconClass, iconHtml) => {
    const iconClasses = popupIconClass + (iconClass ? ` -${iconClass}` : '');
    return `<div class="${iconClasses}">${iconHtml || ''}</div>`;
};

const alertPopupInit = (AppPopup, AppSvg, AppPopupContent) => {
    /**
     * @class AppAlertPopup
     * @memberof layout
     * @requires components#AppPopup
     * @requires components#AppSvg
     * @requires layout#AppPopupContent
     * @classdesc Модуль для попапа уведомлений.
     * @desc Наследует: [AppPopup]{@link components.AppPopup}.
     * @async
     * @param {(HTMLElement|string)} [element] - Элемент попапа или строка - HTML-контент попапа.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @example
     * const alertPopupInstance = new app.AlertPopup(document.querySelector('.alert-popup-element'), {
     *     text: 'Попап-уведомление'
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="alert alert-success fade -toast" role="alert">
     *     <h2 class="alert-heading">Спасибо!</h2>
     *     Отправлено успешно
     * </div>
     */
    const AppAlertPopup = immutable(class extends AppPopup {
        constructor(el, opts) {
            const alertPopupConfig = AppAlertPopup.template(el);
            super(alertPopupConfig, opts, moduleName);

            AppPopupContent.initCloseButton(this.popupEl);
        }

        /**
         * @function AppAlertPopup.template
         * @memberof layout
         * @desc Возвращает HTML-код попапа уведомления.
         * @param {Object} [options={}] - Опции шаблона.
         * @param {string} [options.before] - Дополнительный контент перед основным контентом.
         * @param {boolean} [options.success] - Показывать ли иконку подтвреждения.
         * @param {boolean} [options.error] - Показывать ли иконку ошибки.
         * @param {string} [options.text] - HTML-строка основного содержимого попапа.
         * @param {string} [options.headerAddClass='-center'] - Класс, добавляемый блоку с заголовком.
         * @param {string} [options.textAddClass='-center'] - Класс, добавляемый блоку с основным контентом.
         * @param {string} [options.textAfter] - Дополнительный контент после основного контента.
         * @param {string} [options.buttonText] - Текст кнопки закрытия попапа после основного контента.
         * @param {string} [options.popupClass='-alert'] - Дополнительный класс попапа.
         * @returns {string} HTML-код попапа уведомления.
         * @example
         * const popupHtml = app.AlertPopup.template({
         *     text: 'Попап-уведомление'
         * });
         * console.log(popupHtml); //HTML-код попапа-уведомления
         */
        static template(options = {}) {
            if (!util.isObject(options)) {
                util.typeError(options, 'options', 'plain object');
            }

            util.defaultsDeep(options, defaultOptions);

            if (options.success) { //Иконка подтверждения
                options.before = iconTemplate('success', AppSvg.inline(successIconConfig));
            } else if (options.error) { //Иконка ошибки
                options.before = iconTemplate('error', AppSvg.inline(errorIconConfig));
            }

            if (options.text) {
                const textClasses = `${popupClass}__text${options.textAddClass ? ` ${options.textAddClass}` : ''}`;
                options.text = `<div class="${textClasses}">${options.text}</div>`;
            }

            if (typeof options.textAfter === 'undefined') {
                const buttonAttrs = `
                    type="button"
                    class="${popupClass}-buttons__button btn btn-primary"
                    title="${lang.closeTitle}"
                    data-popup-close`;
                options.textAfter = `
                    <div class="${popupClass}__buttons">
                        <button ${buttonAttrs}>
                            ${options.buttonText || lang.closeText}
                        </button>
                    </div>`;
            }

            return AppPopupContent.baseTemplate(options);
        }
    });

    addModule(moduleName, AppAlertPopup);

    return AppAlertPopup;
};

export default alertPopupInit;
export {alertPopupInit};