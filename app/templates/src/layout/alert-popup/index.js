import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const alertPopupCallback = resolve => {
    const imports = [
        require('Components/popup').default,
        require('Components/svg').default,
        require('Layout/popups').default
    ];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "popups" */ './sync.js'))
            .then(alertPopup => {
                chunks.popups = true;

                const AppAlertPopup = alertPopup.alertPopupInit(...modules);
                resolve(AppAlertPopup);
            });
    });

    emitInit('popup', 'svg', 'popupsContent');
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.popups) {
        alertPopupCallback(resolve);
        return;
    }

    onInit('alertPopup', () => {
        alertPopupCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('alertPopup');
    });
});

export default importFunc;