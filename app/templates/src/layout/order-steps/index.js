let importFunc;

if (__IS_DISABLED_MODULE__) {
    let AppPreloader;
    if (!__IS_SYNC__) {
        AppPreloader = require('Components/preloader').default;
        AppPreloader.initContentPreloader();
    }

    const orderStepsCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "order" */ './sync.js'))
            .then(modules => {
                if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

                const AppOrder = modules.default;
                resolve(AppOrder);
            });
    };

    importFunc = new Promise(resolve => {
        orderStepsCallback(resolve);
    });
}

export default importFunc;