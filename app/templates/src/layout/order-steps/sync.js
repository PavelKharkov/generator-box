import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppFloatLabel from 'Components/float-label';
import AppWindow from 'Layout/window';

//Опции
const moduleName = 'orderSteps';

const orderStepsEl = document.querySelectorAll('.order.-steps');

const selectSelector = '.order-form__select';

/**
 * @class AppOrderSteps
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppPopover
 * @requires components#AppSelect
 * @requires layout#AppWindow
 * @classdesc Модуль формы заказа.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const orderStepsInstance = new app.OrderSteps(document.querySelector('.order-steps-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="order -steps"></div>
 *
 * <!--.order.-steps - селектор по умолчанию-->
 */
const AppOrderSteps = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Страница завершения заказа
        const orderStepsFinishEl = el.querySelector('.order__finish');
        if (orderStepsFinishEl) {
            require('./order-steps-finish');
            return;
        }

        let profileSelect;

        /**
         * @member {Object} AppOrderSteps#profileSelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего меню [AppSelect]{@link components.AppSelect} для выбора профиля.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'profileSelect', {
            enumerable: true,
            get: () => profileSelect
        });

        let deliverySelect;

        /**
         * @member {Object} AppOrderSteps#deliverySelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего меню [AppSelect]{@link components.AppSelect} для выбора адреса доставки.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'deliverySelect', {
            enumerable: true,
            get: () => deliverySelect
        });

        let addressSelect;

        /**
         * @member {Object} AppOrderSteps#addressSelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего меню [AppSelect]{@link components.AppSelect} для выбора адреса.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'addressSelect', {
            enumerable: true,
            get: () => addressSelect
        });

        let addressDateCollapse;

        /**
         * @member {Object} AppOrderSteps#addressDateCollapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося блока [AppCollapse]{@link components.AppCollapse} для добавления даты адреса доставки.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'addressDateCollapse', {
            enumerable: true,
            get: () => addressDateCollapse
        });

        let orderCheckPopovers;

        /**
         * @member {Array.<Object>} AppOrderSteps#orderCheckPopovers
         * @memberof layout
         * @desc Коллекция ссылок на модули информеров [AppPopover]{@link components.AppPopover} у ссылок в заголовках блоков.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'orderCheckPopovers', {
            enumerable: true,
            get: () => orderCheckPopovers
        });

        let floatLabels;

        /**
         * @member {Array.<Object>} AppOrderSteps#floatLabels
         * @memberof layout
         * @desc Коллекция ссылок на модули динамических заголовков [AppFloatLabel]{@link components.AppFloatLabel} полей формы.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'floatLabels', {
            enumerable: true,
            get: () => floatLabels
        });

        //Основная страница заказа
        const reinit = () => {
            const profileSelectEl = el.querySelector(`.order-form__block.-profile ${selectSelector}`);
            const deliverySelectEl = el.querySelector(`.order-form__block.-delivery ${selectSelector}`);
            const addressSelectEl = el.querySelector(`.order-form__block.-address ${selectSelector}`);

            const selectRequire = require('Components/select');
            selectRequire.default.then(AppSelect => {
                if (profileSelectEl) {
                    profileSelect = new AppSelect(profileSelectEl);
                }

                if (deliverySelectEl) {
                    deliverySelect = new AppSelect(deliverySelectEl);
                }

                if (addressSelectEl) {
                    addressSelect = new AppSelect(addressSelectEl);
                }
            });

            selectRequire.selectTrigger(profileSelectEl);
            selectRequire.selectTrigger(deliverySelectEl);
            selectRequire.selectTrigger(addressSelectEl);

            const addressDateCollapseEl = el.querySelector('.order-form__form-group.-address-date .order-form-toggle__button');
            if (addressDateCollapseEl) {
                const collapseRequire = require('Components/collapse');
                collapseRequire.default.then(AppCollapse => {
                    addressDateCollapse = new AppCollapse(addressDateCollapseEl);
                    addressDateCollapse.on('shown', () => {
                        util.attempt(() => {
                            addressDateCollapse.targetElements[0].querySelector('.order-form__input').focus();
                        });
                    });
                });

                collapseRequire.collapseTrigger(addressDateCollapseEl);
            }

            //TODO:make module for inputs
            //Поле для комментария
            const orderCommentEl = el.querySelector('.order-form__input.-comment');
            if (orderCommentEl) {
                //Смена высоты поля ввода текста
                const recountTextareaHeight = () => { //TODO:change rows
                    orderCommentEl.style.height = 'auto';
                    orderCommentEl.style.height = `${orderCommentEl.scrollHeight - 8}px`;
                };
                orderCommentEl.addEventListener('input', recountTextareaHeight);
                AppWindow.onload(() => {
                    recountTextareaHeight();
                });
            }

            //Информеры на странице заказа
            const orderCheckPopoversEl = el.querySelectorAll('.order-form-title-form-check__link.-popover');
            if (orderCheckPopoversEl.length > 0) {
                const popoverRequire = require('Components/popover');
                popoverRequire.default.then(AppPopover => {
                    orderCheckPopovers = [...orderCheckPopoversEl].map(link => {
                        const popoverOptions = {
                            tooltip: true,
                            placement() {
                                return (AppWindow.width() >= util.media.sm) ? 'right' : 'bottom';
                            }
                        };
                        if (util.isTouch) {
                            popoverOptions.trigger = 'click';
                        }
                        const popover = new AppPopover(link, popoverOptions);

                        return popover;
                    });
                });

                popoverRequire.popoverTrigger(orderCheckPopoversEl);
            }

            //Переинициализация плавающих заголовков полей ввода
            const floatLabelsEl = el.querySelectorAll('[data-float-label]');
            if (!floatLabelsEl[0].modules) {
                floatLabels = [...floatLabelsEl].map(item => {
                    const floatLabel = new AppFloatLabel(item);
                    return floatLabel;
                });
            }
        };

        //Инициализация скриптов при загрузке
        reinit();

        /**
         * @function AppOrderSteps#reinit
         * @memberof layout
         * @desc Переинициализирует скрипты формы страницы заказа.
         * @returns {undefined}
         * @readonly
         * @example
         * orderStepsInstance.reinit(); //Нужно вызывать после перезагрузки элемента заказа аяксом
         */
        Object.defineProperty(this, 'reinit', {
            enumerable: true,
            value: reinit
        });
    }
});

addModule(moduleName, AppOrderSteps);

orderStepsEl.forEach(el => new AppOrderSteps(el));

export default AppOrderSteps;
export {AppOrderSteps};