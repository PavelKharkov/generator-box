import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppStateToggler from 'Layout/state-toggler';
import AppWindow from 'Layout/window';

//Опции
const moduleName = 'catalogItem';

const catalogItemClass = util.kebabCase(moduleName);
const catalogItemSelector = `.${catalogItemClass}, [data-${catalogItemClass}]`;
const initializedClass = `${catalogItemClass}-initialized`;

/**
 * @class AppCatalogItem
 * @memberof layout
 * @requires components#AppCounter
 * @requires components#AppMask
 * @requires layout#AppStateToggler
 * @requires layout#AppWindow
 * @classdesc Модуль элементов каталога.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const catalogItemInstance = new app.CatalogItem(document.querySelector('.catalog-item-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="catalog-item"></div>
 *
 * <!--.catalog-item - селектор по умолчанию-->
 */
const AppCatalogItem = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const compareTogglerEl = el.querySelectorAll('.card-actions__item.-compare');

        let compareTogglers;

        /**
         * @member {Array.<Object>} AppCatalogItem#compareTogglers
         * @memberof layout
         * @desc Коллекция ссылок на модули изменения состояния [AppStateToggler]{@link layout.AppStateToggler} кнопки сравнения.
         * @readonly
         */
        Object.defineProperty(this, 'compareTogglers', {
            enumerable: true,
            get: () => compareTogglers
        });

        if (compareTogglerEl.length > 0) {
            compareTogglers = [];

            compareTogglerEl.forEach(item => {
                compareTogglers.push(new AppStateToggler(item));
            });
        }

        const wishlistTogglerEl = el.querySelectorAll('.card-actions__item.-wishlist');

        let wishlistTogglers;

        /**
         * @member {Array.<Object>} AppCatalogItem#wishlistTogglers
         * @memberof layout
         * @desc Коллекция ссылок на модули изменения состояния [AppStateToggler]{@link layout.AppStateToggler} кнопки добавления к отложенным.
         * @readonly
         */
        Object.defineProperty(this, 'wishlistTogglers', {
            enumerable: true,
            get: () => wishlistTogglers
        });

        if (wishlistTogglerEl.length > 0) {
            wishlistTogglers = [];

            wishlistTogglerEl.forEach(item => {
                wishlistTogglers.push(new AppStateToggler(item));
            });
        }

        if (__GLOBAL_SETTINGS__.buy) {
            //Кнопка добавления в корзину
            let cartTogglers;

            /**
             * @member {Array.<Object>} AppCatalogItem#cartTogglers
             * @memberof layout
             * @desc Коллекция ссылок на модули изменения состояния [AppStateToggler]{@link layout.AppStateToggler} кнопки добавления в корзину.
             * @readonly
             */
            Object.defineProperty(this, 'cartTogglers', {
                enumerable: true,
                get: () => cartTogglers
            });

            const cartTogglerElements = el.querySelectorAll('.card-buy__item.-buy');
            if (cartTogglerElements.length > 0) {
                cartTogglers = [...cartTogglerElements].map(item => {
                    const togglerInstance = new AppStateToggler(item);
                    return togglerInstance;
                });
            }

            //Счётчики количества
            let counters;

            /**
             * @member {Array.<Object>} AppCatalogItem#counters
             * @memberof layout
             * @desc Коллекция модулей счётчика [AppCounter]{@link components.AppCounter} элемента каталога.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'counters', {
                enumerable: true,
                get: () => counters
            });

            const counterElements = el.querySelectorAll('.card-counter__counter');
            if (counterElements.length > 0) {
                const counterRequire = require('Components/counter');
                counterRequire.default.then(AppCounter => {
                    counters = [...counterElements].map(item => {
                        const counter = new AppCounter(item);
                        if (counter.disabled) {
                            require('Components/mask').default.then(() => {
                                const {initialValue, input, mask} = counter;
                                mask.remove();
                                input.value = initialValue;
                                input.setAttribute('value', initialValue);
                            });
                            emitInit('mask');
                        }
                        return counter;
                    });
                });

                counterRequire.counterTrigger(counterElements);
            }
        }

        //Чекбокс, появляющийся при табличном отображении
        const counterCheckbox = el.querySelector('.card-counter__form-check');
        if (counterCheckbox) {
            const counterCheckboxInput = counterCheckbox.querySelector('.card-counter-form-check__input');
            counterCheckboxInput.addEventListener('change', () => {
                el.querySelector('.card__counter').classList[counterCheckboxInput.checked ? 'add' : 'remove']('checked');
            });
        }

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppCatalogItem);

/**
 * @function initCatalogItems
 * @desc Инициализирует элементы каталога.
 * @returns {undefined}
 * @ignore
 */
const initCatalogItems = () => {
    const catalogItemEl = document.querySelectorAll(catalogItemSelector);

    catalogItemEl.forEach(el => new AppCatalogItem(el));
};

//Отложенная загрузка элементов каталога
if (AppWindow.initialized) {
    initCatalogItems();
} else {
    AppWindow.on('deferredInit', () => initCatalogItems());
}

export default AppCatalogItem;
export {AppCatalogItem};