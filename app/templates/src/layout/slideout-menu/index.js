import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const slideoutMenuCallback = resolve => {
    const imports = [require('Components/slideout').default];

    Promise.all(imports).then(modules => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "slideout" */ './sync.js'))
            .then(slideoutMenu => {
                chunks.slideout = true;

                const AppSlideoutMenu = slideoutMenu.slideoutMenuInit(...modules);
                resolve(AppSlideoutMenu);
            });
    });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.slideout) {
        slideoutMenuCallback(resolve);
        return;
    }

    onInit('slideoutMenu', () => {
        slideoutMenuCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('slideoutMenu');
    });
});

export default importFunc;