import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import 'Components/validation';

//Опции
const moduleName = 'slideoutMenu';

//Селекторы и классы
const slideoutClass = 'slideout';
const menuClass = 'menu';
const menuInnerClass = `${slideoutClass}-${menuClass}__inner`;

const innerMenuShowClass = 'inner-menu-show';
const innerMenuShownClass = 'inner-menu-shown';
const innerItemActiveClass = 'inner-item-active';

const slideoutMenuElement = document.querySelectorAll(`.${slideoutClass}__${menuClass}`);

const scrollerAnimateDuration = 300;
const menuChangeDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

let baseSlideoutMenu;

const slideoutMenuInit = AppSlideout => {
    /**
     * @class AppSlideoutMenu
     * @memberof layout
     * @requires components#AppPopup
     * @requires components#AppSlideout
     * @requires components#AppValidation
     * @requires layout#AppSearchForm
     * @classdesc Модуль контента выезжающего меню.
     * @desc Наследует: [Module]{@link app.Module}.
     * @param {HTMLElement} element - Элемент контента меню.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @param {Object} [options.slideout] - Модуль родительского выезюащего меню. Если опция element не задана, то берётся выезжающее меню по умолчанию [AppSlideout.baseSlideout]{@link components.AppSlideout.baseSlideout}. Если задана, то ищется родительский элемент выезжающего меню с селектором .slideout.
     * @example
     * const slideoutMenuInstance = new app.SlideoutMenu(document.querySelector('.slideout-menu-element'), {
     *     slideout: slideoutInstance
     * });
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="slideout__menu"></div>
     *
     * <!--.slideout__menu - селектор по умолчанию-->
     */
    const AppSlideoutMenu = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            /**
             * @member {Object} AppSlideoutMenu#params
             * @memberof components
             * @desc Параметры экземпляра.
             * @readonly
             */
            const params = Module.setParams(this);

            const slideout =
                params.slideout ||
                ((typeof el === 'undefined') ? AppSlideout.baseSlideout : util.attempt(() => el.closest(`.${slideoutClass}`).modules.slideout));

            /**
             * @member {Object} AppSlideoutMenu#slideout
             * @memberof layout
             * @desc Ссылка на родительский модуль выезжающего меню.
             * @readonly
             */
            Object.defineProperty(this, 'slideout', {
                enumerable: true,
                get: () => slideout
            });

            //Прекратить дальнейшую инициализацию контента выезжающего меню, если не найден родительский модуль выезжающего меню
            if ((typeof slideout !== 'object') || !(slideout instanceof AppSlideout)) return;

            let markupInitialized = el instanceof HTMLElement;

            /**
             * @member {boolean} AppSlideoutMenu#markupInitialized
             * @memberof layout
             * @desc Указывает, проинициализирована ли разметка контента меню.
             * @readonly
             */
            Object.defineProperty(this, 'markupInitialized', {
                enumerable: true,
                get: () => markupInitialized
            });

            let slideoutMenuEl = el;

            /**
             * @member {HTMLElement} AppSlideoutMenu#slideoutMenuEl
             * @memberof layout
             * @desc Элемент контента выезжающего меню.
             * @readonly
             */
            Object.defineProperty(this, 'slideoutMenuEl', {
                enumerable: true,
                get: () => slideoutMenuEl
            });

            /**
             * @function AppSlideoutMenu#markupInit
             * @memberof layout
             * @desc Инициализирует разметку контента меню, если экземпляр модуля был инициализирован без параметра элемента.
             * @returns {undefined}
             * @readonly
             * @example
             * slideoutMenuInstance.markupInit();
             */
            Object.defineProperty(this, 'markupInit', {
                enumerable: true,
                value() {
                    const currentSlideoutMenuEl = document.createElement('div');
                    currentSlideoutMenuEl.classList.add(`${slideoutClass}__${menuClass}`);
                    const markup = `
                <div class="${menuInnerClass}"></div>`;
                    currentSlideoutMenuEl.innerHTML = markup;

                    slideout.content.append(currentSlideoutMenuEl);
                    slideoutMenuEl = currentSlideoutMenuEl;
                    if (!slideoutMenuEl.modules) {
                        slideoutMenuEl.modules = {};
                    }
                    if (!slideoutMenuEl.modules[moduleName]) {
                        slideoutMenuEl.modules[moduleName] = this;
                    }

                    if (!slideout.slideoutEl.modules) {
                        slideout.slideoutEl.modules = {};
                    }
                    slideout.slideoutEl.modules[moduleName] = this;

                    markupInitialized = true;
                }
            });

            let scrollerAnimate;

            /**
             * @member {Object} AppSlideoutMenu#scrollerAnimate
             * @memberof layout
             * @desc Ссылка на модуль анимации [AppAnimation]{@link components.AppAnimation} элемента обёртки выезжающего меню.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'scrollerAnimate', {
                enumerable: true,
                get: () => scrollerAnimate
            });

            let menuInner;

            /**
             * @member {HTMLElement} AppSlideoutMenu#menuInner
             * @memberof layout
             * @desc Элемент внутреннего контейнера меню.
             * @readonly
             */
            Object.defineProperty(this, 'menuInner', {
                enumerable: true,
                get: () => menuInner
            });

            let innerMenuTransitioning = false;

            /**
             * @member {boolean} AppSlideoutMenu#innerMenuTransitioning
             * @memberof layout
             * @desc Указывает, происходит ли в данный момент смена уровня вложенности во внутренних меню.
             * @readonly
             */
            Object.defineProperty(this, 'innerMenuTransitioning', {
                enumerable: true,
                get: () => innerMenuTransitioning
            });

            let search;

            /**
             * @member {Object} AppSlideoutMenu#search
             * @memberof layout
             * @desc Ссылка на модуль поиска [AppSearchForm]{@link layout.AppSearchForm}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'search', {
                enumerable: true,
                get: () => search
            });

            let popups;

            /**
             * @member {Array.<Object>} AppSlideoutMenu#popups
             * @memberof layout
             * @desc Коллекция ссылок на модули попапов [AppPopup]{@link components.AppPopup}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'popups', {
                enumerable: true,
                get: () => popups
            });

            let catalogMenuButton;

            /**
             * @member {HTMLElement} AppSlideoutMenu#catalogMenuButton
             * @memberof layout
             * @desc Элемент кнопки открытия меню каталога.
             * @readonly
             */
            Object.defineProperty(this, 'catalogMenuButton', {
                enumerable: true,
                get: () => catalogMenuButton
            });

            let catalogLevel = false;

            /**
             * @member {(number|boolean)} AppSlideoutMenu#catalogLevel
             * @memberof layout
             * @desc Текущий открытый уровень меню каталога. Если меню каталога не открыто, то false.
             * @readonly
             */
            Object.defineProperty(this, 'catalogLevel', {
                enumerable: true,
                get: () => catalogLevel
            });

            let catalogInnerActive = false;

            /**
             * @member {boolean} AppSlideoutMenu#catalogInnerActive
             * @memberof layout
             * @desc Указывает, открыто ли меню каталога в выезжающем меню.
             * @readonly
             */
            Object.defineProperty(this, 'catalogInnerActive', {
                enumerable: true,
                get: () => catalogInnerActive
            });

            const catalogActiveItem = [];

            /**
             * @member {Array.<HTMLElement>} AppSlideoutMenu#catalogActiveItem
             * @memberof layout
             * @desc Коллекция предыдущих активных пунктов меню.
             * @readonly
             */
            Object.defineProperty(this, 'catalogActiveItem', {
                enumerable: true,
                value: catalogActiveItem
            });

            let showCatalogMenu;

            /**
             * @function AppSlideoutMenu#showCatalogMenu
             * @memberof layout
             * @desc Показывает меню каталога в выезжающем меню.
             * @returns {undefined}
             * @readonly
             */
            Object.defineProperty(this, 'showCatalogMenu', {
                enumerable: true,
                get: () => showCatalogMenu
            });

            let catalogMenuTouch;

            /**
             * @member {Object} AppSlideoutMenu#catalogMenuTouch
             * @memberof layout
             * @desc Ссылка на модуль touch-взаимодействий [AppTouch]{@link components.AppTouch} меню каталога.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'catalogMenuTouch', {
                enumerable: true,
                get: () => catalogMenuTouch
            });

            let innerMenuActive = false;

            /**
             * @member {boolean} AppSlideoutMenu#innerMenuActive
             * @memberof layout
             * @desc Указывает, открыт ли внутренний элемент главного меню внутри выезжающего меню.
             * @readonly
             */
            Object.defineProperty(this, 'innerMenuActive', {
                enumerable: true,
                get: () => innerMenuActive
            });

            let mainMenuTouch;

            /**
             * @member {Object} AppSlideoutMenu#mainMenuTouch
             * @memberof layout
             * @desc Ссылка на модуль touch-взаимодействий [AppTouch]{@link components.AppTouch} главного меню.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'mainMenuTouch', {
                enumerable: true,
                get: () => mainMenuTouch
            });

            let locationSelect;

            if (__GLOBAL_SETTINGS__.locations) {
                /**
                 * @member {Object} AppSlideoutMenu#locationSelect
                 * @memberof layout
                 * @desc Ссылка на модуль выпадающего списка [AppDropdown]{@link components.AppDropdown} для выбора местоположения.
                 * @readonly
                 */
                Object.defineProperty(this, 'locationSelect', {
                    enumerable: true,
                    get: () => locationSelect
                });
            }

            /**
             * @function init
             * @desc Инициализирует разметку меню, если экземпляр модуля был инициализирован без параметра элемента.
             * @returns {undefined}
             * @ignore
             */
            const init = () => {
                if (!markupInitialized) this.markupInit();

                menuInner = slideoutMenuEl.querySelector(`.${menuInnerClass}`);
                if (!menuInner) return;

                let menuTimeout;

                require('Components/animation').default.then(AppAnimation => {
                    scrollerAnimate = new AppAnimation(slideout.scroller);
                });
                emitInit('animation');

                //Добавление разметки контента меню в контейнер выезжающего меню и инициализация соответствующих скриптов
                const headerEl = document.querySelector('.header');

                //Блок выбора местоположения
                if (__GLOBAL_SETTINGS__.locations) {
                    const headerLocation = headerEl.querySelector('.header__location');
                    if (headerLocation) {
                        const slideoutLocation = headerLocation.cloneNode(true);
                        menuInner.append(slideoutLocation);

                        const dropdownEl = slideoutLocation.querySelector('[data-dropdown]');
                        if (dropdownEl) {
                            const dropdownRequire = require('Components/dropdown');
                            dropdownRequire.default.then(AppDropdown => {
                                locationSelect = (dropdownEl.modules && dropdownEl.modules.dropdown) ||
                                    new AppDropdown(dropdownEl, {
                                        flip: false
                                    });
                            });

                            dropdownRequire.dropdownTrigger(dropdownEl);
                        }
                    }
                }

                //Форма поиска
                const headerSearch = headerEl.querySelector('.header__search');
                if (headerSearch) {
                    const slideoutSearch = headerSearch.cloneNode(true);
                    menuInner.append(slideoutSearch);

                    const searchFormRequire = require('Layout/search-form');
                    searchFormRequire.default.then(() => {
                        search = slideoutSearch.modules && slideoutSearch.modules.search;
                    });

                    searchFormRequire.searchFormTrigger(slideoutSearch);

                    const headerSearchForm = headerSearch.querySelector('.search__form');
                    const slideoutSearchForm = slideoutSearch.querySelector('.search__form');
                    const headerSearchInput = headerSearch.querySelector('.search__input');
                    const slideoutSearchInput = slideoutSearch.querySelector('.search__input');

                    headerSearchInput.addEventListener('input', () => {
                        slideoutSearchInput.value = headerSearchInput.value;
                        slideoutSearchInput.setAttribute('value', headerSearchInput.value);

                        if (slideoutSearchForm.modules && slideoutSearchForm.modules.validation) {
                            slideoutSearchForm.modules.validation.validateElement(slideoutSearchInput);
                        }
                    });
                    slideoutSearchInput.addEventListener('input', () => {
                        headerSearchInput.value = slideoutSearchInput.value;
                        headerSearchInput.setAttribute('value', slideoutSearchInput.value);

                        if (headerSearchForm.modules && headerSearchForm.modules.validation) {
                            headerSearchForm.modules.validation.validateElement(headerSearchInput);
                        }
                    });
                }

                //Кнопки действий
                const headerActions = headerEl.querySelector('.header-actions__group');
                if (headerActions) menuInner.append(headerActions.cloneNode(true));

                //Блок личного кабинета
                const headerPersonal = headerEl.querySelector('.header__personal');
                if (headerPersonal) {
                    const slideoutPersonal = headerPersonal.cloneNode(true);
                    menuInner.append(slideoutPersonal);

                    const popupEl = slideoutPersonal.querySelectorAll('[data-popup]');
                    if (popupEl.length > 0) {
                        const popupRequire = require('Components/popup');
                        popupRequire.default.then(() => {
                            popups = [...popupEl].map(element => {
                                let popupInstance;
                                const initializedInstance = element.modules && element.modules.popup;
                                if (initializedInstance) {
                                    popupInstance = initializedInstance;
                                } else {
                                    const popupTarget = document.querySelector(element.getAttribute('href'));
                                    if (popupTarget) {
                                        popupInstance = popupTarget.modules && popupTarget.modules.popup;
                                        if (popupInstance) {
                                            popupInstance.addControl(element);
                                        }
                                    }
                                }
                                return popupInstance;
                            });
                        });

                        popupRequire.popupTrigger(popupEl);
                    }
                }

                //Меню каталога
                const headerCatalogMenu = headerEl.querySelector('.header__catalog-menu');
                if (headerCatalogMenu) {
                    const slideoutCatalogMenu = headerCatalogMenu.cloneNode(true);
                    menuInner.append(slideoutCatalogMenu);

                    //Кнопка открытия меню каталога
                    const catalogContainer = slideoutCatalogMenu.querySelector('.header-catalog-menu__container.-level1');
                    //TODO:save elements as props

                    catalogMenuButton = slideoutCatalogMenu.querySelector('.header-catalog-menu__button');

                    showCatalogMenu = () => {
                        slideout.slideoutEl.classList.add(innerMenuShowClass);
                        slideoutCatalogMenu.classList.add(innerMenuShowClass);
                        menuInner.classList.add(innerMenuShowClass);

                        slideoutMenuEl.style.height = window.getComputedStyle(catalogContainer).height;
                        if (scrollerAnimate) {
                            scrollerAnimate.animate({
                                property: 'scrollTop',
                                value: 0,
                                duration: scrollerAnimateDuration
                            });
                        }

                        menuTimeout = setTimeout(() => {
                            menuInner.classList.add(innerMenuShownClass);
                            slideoutCatalogMenu.classList.add(innerMenuShownClass);
                            slideout.slideoutEl.classList.add(innerMenuShownClass);
                        }, scrollerAnimateDuration);

                        catalogLevel = 0;
                        catalogInnerActive = true;
                        slideoutCatalogMenu.dataset.catalogLevel = catalogLevel;
                    };
                    catalogMenuButton.addEventListener('click', () => showCatalogMenu());

                    /**
                     * @function backFunc
                     * @desc Функция, вызываемая при нажатии кнопки "Назад" в меню каталога в выезжающем меню.
                     * @returns {undefined}
                     * @ignore
                     */
                    const backFunc = () => {
                        if (innerMenuTransitioning) return;

                        menuInner.classList.remove(innerMenuShownClass);

                        innerMenuTransitioning = true;
                        catalogLevel -= 1;
                        if (catalogLevel < 0) {
                            catalogLevel = false;
                        } else {
                            catalogContainer.style.left = `-${catalogLevel * 100}%`;
                            slideoutCatalogMenu.dataset.catalogLevel = catalogLevel;
                        }
                        if (catalogLevel === false) {
                            catalogActiveItem.length = 0;
                            menuInner.classList.remove(innerMenuShowClass);
                            slideoutMenuEl.style.height = ''; //TODO:animation (use module)
                            catalogContainer.style.left = '0';
                            delete slideoutCatalogMenu.dataset.catalogLevel;

                            setTimeout(() => {
                                const activeItem = slideoutCatalogMenu.querySelector(`.header-catalog-menu__item.${innerItemActiveClass}`);
                                if (activeItem) activeItem.classList.remove(innerItemActiveClass);
                                innerMenuTransitioning = false;
                                catalogInnerActive = false;

                                menuInner.classList.remove(innerMenuShownClass);
                                slideoutCatalogMenu.classList.remove(innerMenuShowClass, innerMenuShownClass);
                                slideout.slideoutEl.classList.remove(innerMenuShowClass, innerMenuShownClass);
                            }, menuChangeDuration);
                            return;
                        }

                        const currentActiveSelector = (catalogLevel === 0)
                            ? '.header-catalog-menu__container.-level1'
                            : `.header-catalog-menu__item.-level${catalogLevel}.${innerItemActiveClass} > .header-catalog-menu__container`;
                        const currentActiveContainer = slideoutCatalogMenu.querySelector(currentActiveSelector);
                        slideoutMenuEl.style.height = window.getComputedStyle(currentActiveContainer).height;

                        setTimeout(() => {
                            const prevActiveSelector = `.header-catalog-menu__item.-level${catalogLevel + 1}.${innerItemActiveClass}`;
                            const prevActiveItem = slideoutCatalogMenu.querySelector(prevActiveSelector);
                            catalogActiveItem.push(prevActiveItem);
                            prevActiveItem.classList.remove(innerItemActiveClass);
                            menuInner.classList.add(innerMenuShownClass);
                            innerMenuTransitioning = false;
                        }, menuChangeDuration);
                    };

                    slideoutCatalogMenu.querySelector('.header-catalog-menu__link.-back').addEventListener('click', backFunc);

                    //Внутренние элементы меню
                    const hasItemsItems = slideoutCatalogMenu.querySelectorAll('.header-catalog-menu__item.-has-items');
                    if (hasItemsItems.length > 0) {
                        hasItemsItems.forEach(item => {
                            const hasItemsLink = item.querySelector('.header-catalog-menu__link.-has-items');
                            const hasItemsMenu = item.querySelector('.header-catalog-menu__container');
                            hasItemsLink.addEventListener('click', event => {
                                event.preventDefault();
                                if (innerMenuTransitioning) return;
                                innerMenuTransitioning = true;
                                menuInner.classList.remove(innerMenuShownClass);
                                catalogActiveItem.pop();
                                if (
                                    (catalogActiveItem.length > 0) &&
                                    !item.contains(catalogActiveItem[catalogActiveItem.length - 1])
                                ) {
                                    catalogActiveItem.length = 0;
                                }

                                clearTimeout(menuTimeout);

                                item.classList.add(innerItemActiveClass);
                                catalogLevel += 1;
                                catalogContainer.style.left = `-${catalogLevel * 100}%`;
                                slideoutCatalogMenu.dataset.catalogLevel = catalogLevel;

                                slideoutMenuEl.style.height = window.getComputedStyle(hasItemsMenu).height;
                                if (scrollerAnimate) {
                                    scrollerAnimate.animate({
                                        property: 'scrollTop',
                                        value: 0,
                                        duration: scrollerAnimateDuration
                                    });
                                }
                                setTimeout(() => {
                                    innerMenuTransitioning = false;
                                    menuInner.classList.add(innerMenuShownClass);
                                }, menuChangeDuration);
                            });
                        });
                    }

                    if (util.isTouch) {
                        require('Components/touch').default.then(AppTouch => {
                            catalogMenuTouch = new AppTouch(slideoutCatalogMenu);
                            catalogMenuTouch.on({
                                touchmove: event => {
                                    if (!catalogInnerActive) return;

                                    event.stopPropagation();

                                    const moveCoords = catalogMenuTouch.moveCoords;
                                    if (Math.abs(moveCoords[1]) <= 70) {
                                        const offsetWidth = catalogMenuTouch.el.offsetWidth / 3;

                                        if (slideout.currentDirection === 'right') {
                                            slideout.touch.setPrevented();
                                        }
                                        if (moveCoords[0] >= offsetWidth) {
                                            backFunc();
                                        } else if ((catalogActiveItem.length > 0) && (moveCoords[0] <= -offsetWidth)) {
                                            catalogActiveItem[catalogActiveItem.length - 1]
                                                .querySelector('.header-catalog-menu__link.-has-items').click();
                                        }
                                    }
                                },
                                touchend() {
                                    if (slideout.currentDirection === 'right') {
                                        slideout.touch.setUnprevented();
                                    }
                                }
                            });
                        });
                        emitInit('touch');
                    }

                    //Сбрасывать изменения меню каталога в выезжающем меню при закрытии
                    slideout.on('hidden', () => {
                        clearTimeout(menuTimeout);

                        const activeItems = slideoutCatalogMenu.querySelectorAll(`.header-catalog-menu__item.${innerItemActiveClass}`);
                        if (activeItems.length > 0) {
                            activeItems.forEach(activeItem => {
                                activeItem.classList.remove(innerItemActiveClass);
                            });
                        }
                        menuInner.classList.remove(innerMenuShowClass, innerMenuShownClass);
                        slideoutCatalogMenu.classList.remove(innerMenuShowClass, innerMenuShownClass);
                        slideout.slideoutEl.classList.remove(innerMenuShowClass, innerMenuShownClass);

                        catalogLevel = false;
                        catalogInnerActive = false;
                        catalogContainer.style.left = '0';
                        delete slideoutCatalogMenu.dataset.catalogLevel;

                        slideoutMenuEl.style.height = '';
                        innerMenuTransitioning = false;
                        innerMenuActive = false;
                    });
                }

                //Меню хедера
                const headerMenu = headerEl.querySelector('.header__menu');
                if (headerMenu) menuInner.append(headerMenu.cloneNode(true));

                //Главное меню
                const headerMainMenu = headerEl.querySelector('.header__main-menu');
                if (headerMainMenu) {
                    const slideoutMainMenu = headerMainMenu.cloneNode(true);
                    menuInner.append(slideoutMainMenu);

                    const hasItemsItems = slideoutMainMenu.querySelectorAll('.header-main-menu__item.-level1.-has-items');
                    if (hasItemsItems.length > 0) {
                        hasItemsItems.forEach(item => {
                            const hasItemsLink = item.querySelector('.header-main-menu__link.-level1.-has-items');
                            const hasItemsMenu = item.querySelector('.header-main-menu__container.-level2');

                            hasItemsLink.addEventListener('click', event => {
                                event.preventDefault();

                                innerMenuActive = true;

                                slideout.slideoutEl.classList.add(innerMenuShowClass);
                                slideoutMainMenu.classList.add(innerMenuShowClass);
                                menuInner.classList.add(innerMenuShowClass);
                                item.classList.add(innerItemActiveClass);

                                slideoutMenuEl.style.height = window.getComputedStyle(hasItemsMenu).height;
                                if (scrollerAnimate) {
                                    scrollerAnimate.animate({
                                        property: 'scrollTop',
                                        value: 0,
                                        duration: scrollerAnimateDuration
                                    });
                                }
                                menuTimeout = setTimeout(() => {
                                    slideout.slideoutEl.classList.add(innerMenuShownClass);
                                    slideoutMainMenu.classList.add(innerMenuShownClass);
                                    menuInner.classList.add(innerMenuShownClass);
                                }, scrollerAnimateDuration);
                            });

                            /**
                             * @function backFunc
                             * @desc Функция, вызываемая при нажатии кнопки "Назад" в главном меню в выезжающем меню.
                             * @returns {undefined}
                             * @ignore
                             */
                            const backFunc = () => {
                                if (innerMenuTransitioning) return;
                                innerMenuTransitioning = true;

                                clearTimeout(menuTimeout);

                                menuInner.classList.remove(innerMenuShowClass, innerMenuShownClass);
                                slideoutMenuEl.style.height = '';
                                setTimeout(() => {
                                    const activeItem = slideoutMainMenu.querySelector(`.header-main-menu__item.${innerItemActiveClass}`);
                                    if (activeItem) activeItem.classList.remove(innerItemActiveClass);
                                    slideoutMainMenu.classList.remove(innerMenuShowClass, innerMenuShownClass);
                                    slideout.slideoutEl.classList.remove(innerMenuShowClass, innerMenuShownClass);

                                    innerMenuTransitioning = false;
                                    innerMenuActive = false;
                                }, menuChangeDuration);
                            };
                            menuInner.querySelector('.header-main-menu__link.-back').addEventListener('click', backFunc);

                            if (util.isTouch) {
                                require('Components/touch').default.then(AppTouch => {
                                    mainMenuTouch = new AppTouch(slideoutMainMenu);
                                    mainMenuTouch.on({
                                        touchmove: event => {
                                            if (!innerMenuActive) return;

                                            if (slideout.currentDirection === 'right') {
                                                event.stopPropagation();
                                                slideout.touch.setPrevented();
                                            }

                                            const moveCoords = mainMenuTouch.moveCoords;
                                            if ((Math.abs(moveCoords[1]) <= 70) && (moveCoords[0] >= (mainMenuTouch.el.offsetWidth / 3))) {
                                                backFunc();
                                            }
                                        },
                                        touchend() {
                                            if (slideout.currentDirection === 'right') {
                                                slideout.touch.setUnprevented();
                                            }
                                        }
                                    });
                                });
                                emitInit('touch');
                            }
                        });

                        //Сбрасывать изменения главного меню в выезжающем меню при закрытии
                        slideout.on('hidden', () => {
                            clearTimeout(menuTimeout);

                            const activeItems = slideoutMainMenu.querySelectorAll(`.header-main-menu__item.${innerItemActiveClass}`);
                            if (activeItems.length > 0) {
                                activeItems.forEach(activeItem => {
                                    activeItem.classList.remove(innerItemActiveClass);
                                });
                            }
                            menuInner.classList.remove(innerMenuShowClass, innerMenuShownClass);
                            slideoutMainMenu.classList.remove(innerMenuShowClass, innerMenuShownClass);
                            slideout.slideoutEl.classList.remove(innerMenuShowClass, innerMenuShownClass);

                            slideoutMenuEl.style.height = '';
                            innerMenuTransitioning = false;
                            innerMenuActive = false;
                        });
                    }
                }

                //Телефоны
                const headerPhones = headerEl.querySelector('.header__phones');
                if (headerPhones) menuInner.append(headerPhones.cloneNode(true));
            };

            /**
             * @function AppSlideoutMenu#init
             * @memberof layout
             * @desc Инициализирует разметку меню, если экземпляр модуля был инициализирован без параметра элемента.
             * @returns {undefined}
             * @readonly
             * @example
             * slideoutMenuInstance.init();
             */
            Object.defineProperty(this, 'init', {
                enumerable: true,
                value: init
            });

            /**
             * @function showFunc
             * @desc Функция проверки инициализации контента меню.
             * @param {Object} [options] - Опции открытия меню.
             * @param {string} [options.type] - Тип открываемого меню.
             * @returns {undefined}
             * @ignore
             */
            const showFunc = options => {
                if ((options.type === 'menu') && !slideout.shownBefore.menu) {
                    slideout.off('show', showFunc);
                    init();
                }
            };

            slideout.on('show', showFunc);
        }

        /**
         * @function AppSlideoutMenu.baseSlideoutMenu
         * @memberof layout
         * @desc Get-функция. Ссылка на базовый модуль контента выезжающего меню.
         * @returns {Object}
         */
        static get baseSlideoutMenu() {
            return baseSlideoutMenu;
        }
    });

    baseSlideoutMenu = new AppSlideoutMenu();

    addModule(moduleName, AppSlideoutMenu);

    //Инициализация элементов по классу
    slideoutMenuElement.forEach(el => new AppSlideoutMenu(el));

    return AppSlideoutMenu;
};

export default slideoutMenuInit;
export {slideoutMenuInit};