import './style.scss';
import './utilities/style.scss';

import MobileDetect from 'Libs/mobile-detect';
import platform from 'Libs/platform';

/**
 * @namespace util
 * @memberof app
 * @requires libs.mobile-detect
 * @requires libs.platform
 * @desc Вспомогательные переменные и функции.
 */
import util from './init.js';
import './methods.js';
import './selector-methods.js';

const html = document.documentElement;

/**
 * @member {string} app.util#userAgent
 * @desc Хранит значение navigator.userAgent.
 * @readonly
 * @example
 * console.log(app.util.userAgent);
 */
Object.defineProperty(util, 'userAgent', {
    enumerable: true,
    value: window.navigator.userAgent
});

/**
 * @member {Object} app.util#md
 * @desc Хранит инициализованный экземпляр [Mobile detect]{@link libs#mobile-detect}.
 * @readonly
 * @example
 * console.log(app.util.md);
 */
Object.defineProperty(util, 'md', {
    enumerable: true,
    value: new MobileDetect(util.userAgent)
});

//TODO:test platform detection

//Определять ОС с помощью Platform.js
let userOS = 'Unknown OS';

/**
 * @member {string} app.util#os
 * @desc Хранит краткое название ОС пользователя. По умолчанию соответствует 'Unknown OS'.
 * @readonly
 * @example
 * console.log(app.util.os);
 */
Object.defineProperty(util, 'os', {
    enumerable: true,
    get: () => userOS
});

/**
 * @member {Object} app.util#platform
 * @desc Хранит ссылку на [Platform]{@link libs#platform}.
 * @readonly
 * @example
 * console.log(app.util.platform);
 */
Object.defineProperty(util, 'platform', {
    enumerable: true,
    value: platform
});

const osString = String(util.platform.os);
if (osString.includes('Win')) {
    userOS = 'Windows';
} else if (osString.includes('Mac') || osString.startsWith('OS ')) {
    userOS = 'Mac';
} else if (osString.includes('iOS')) {
    userOS = 'iOS';
} else if (osString.includes('Android')) {
    userOS = 'Android';
} else if (osString.includes('X11')) {
    userOS = 'UNIX';
} else if (osString.includes('Linux')) {
    userOS = 'Linux';
} else if (osString.includes('Chrome OS')) {
    userOS = 'Chrome';
}

if (__HAS_POLYFILLS__) {
    if (osString.includes('BlackBerry') || osString.includes('Tablet OS')) {
        userOS = 'BlackBerry';
    }
}

//Добавить класс браузера
const browserPrefix = 'browser';
let browserName = util.platform.name && util.platform.name.toLowerCase().replace(/\s/g, '-');

if (__HAS_POLYFILLS__) {
    if ((browserName === 'blackberry-browser') || (browserName === 'playbook-browser')) {
        browserName = 'blackberry-browser';
    }
}

/**
 * @member {string} app.util#browser
 * @desc Браузер клиента.
 * @readonly
 * @example
 * console.log(app.util.browser);
 */
Object.defineProperty(util, 'browser', {
    enumerable: true,
    value: browserName
});

/**
 * @member {string} app.util#browserVersion
 * @desc Версия браузера клиента.
 * @readonly
 * @example
 * console.log(app.util.browserVersion);
 */
Object.defineProperty(util, 'browserVersion', {
    enumerable: true,
    value: util.platform.version
});

html.classList.add(`${browserPrefix}-${browserName}`);

//Добавление класса для браузера instagram
//if (util.userAgent.includes('Instagram')) {
//    html.classList.add(`${browserPrefix}-instagram`);
//}

/**
 * @member {boolean} app.util#isChromium
 * @desc Является ли браузер клиента браузером на движке Chromium.
 * @readonly
 * @example
 * console.log(app.util.isChromium);
 */
Object.defineProperty(util, 'isChromium', {
    enumerable: true,
    value: util.userAgent.includes('Chrome')
});

/**
 * @member {(string|false)} app.util#chromiumVersion
 * @desc Версия движка Chromium.
 * @readonly
 * @example
 * console.log(app.util.chromiumVersion);
 */
Object.defineProperty(util, 'chromiumVersion', {
    enumerable: true,
    value: util.isChromium ? util.userAgent.split('Chrome/')[1].split(' ')[0] : false
});

//Определение дополнительных свойств
/**
 * @member {boolean} app.util#isMobile
 * @desc Является ли устройство клиента мобильным.
 * @readonly
 * @example
 * console.log(app.util.isMobile);
 */
Object.defineProperty(util, 'isMobile', {
    enumerable: true,
    value: !!util.md.mobile()
});

/**
 * @member {boolean} app.util#isTablet
 * @desc Является ли устройство клиента планшетом.
 * @readonly
 * @example
 * console.log(app.util.isTablet);
 */
Object.defineProperty(util, 'isTablet', {
    enumerable: true,
    value: !!util.md.tablet()
});

//Добавление классов типов устройств
html.classList.add((util.isMobile || util.isTablet) ? 'mobile' : 'desktop');
if (util.isMobile) html.classList.add('mobile-small');
if (util.isTablet) html.classList.add('tablet');

/**
 * @member {boolean} app.util#isTouch
 * @desc Поддерживает ли устройство клиента touch-взаимодействия.
 * @readonly
 * @example
 * console.log(app.util.isTouch);
 */
Object.defineProperty(util, 'isTouch', {
    enumerable: true,
    value: !!((typeof window.ontouchstart !== 'undefined') || window.navigator.msMaxTouchPoints)
});
html.classList.add(util.isTouch ? 'touch' : 'no-touch');

/**
 * @member {Object} app.util#media
 * @desc Список значений основных медиа-запросов.
 * @property {number} [xxl=1366]
 * @property {number} [xl=1200]
 * @property {number} [lg=992]
 * @property {number} [md=768]
 * @property {number} [sm=576]
 * @property {number} [xs=414]
 * @property {number} [xxs=320]
 * @readonly
 * @example
 * console.log(app.util.media);
 */
//TODO:add example with window.width()
Object.defineProperty(util, 'media', {
    enumerable: true,
    value: {
        xxl: 1366,
        xl: 1200,
        lg: 992,
        md: 768,
        sm: 576,
        xs: 414,
        xxs: 320
    }
});

const langAttr = html.lang.split('-');

/**
 * @member {string} app.util#lang
 * @desc Текущий язык сайта.
 * @readonly
 * @example
 * console.log(app.util.lang);
 */
Object.defineProperty(util, 'lang', {
    enumerable: true,
    value: langAttr[0]
});

/**
 * @member {string} app.util#region
 * @desc Текущий регион сайта.
 * @readonly
 * @example
 * console.log(app.util.region);
 */
Object.defineProperty(util, 'region', {
    enumerable: true,
    value: langAttr[1]
});

export default util;