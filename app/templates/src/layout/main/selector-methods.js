import util from './init.js';

//Опции
const disabledClass = 'disabled';
const activeClassStr = 'active';

const formTags = /^(BUTTON|INPUT|SELECT|TEXTAREA|OPTION|OPTGROUP|FIELDSET)$/;
const checkable = /radio|checkbox/i;

//Вспомогательные DOM-методы

//Включение/отключение
/**
 * @function app.util#disable
 * @desc Добавляет элементу атрибуты неактивности.
 * @param {HTMLElement} element - Элемент, которому нужно добавить атрибуты.
 * @returns {undefined}
 * @readonly
 * @example
 * app.util.disable(document.querySelector('button'));
 */
Object.defineProperty(util, 'disable', {
    enumerable: true,
    value(element = util.required('element')) {
        if (formTags.test(element.tagName)) {
            element.disabled = true;
            return;
        }

        element.classList.add(disabledClass);
        element.tabIndex = '-1'; //TODO:fix toggle
        element.ariaDisabled = true;
    }
});

/**
 * @function app.util#enable
 * @desc Удаляет у элемента атрибуты неактивности.
 * @param {HTMLElement} element - Элемент для удаления атрибутов.
 * @returns {undefined}
 * @readonly
 * @example
 * app.util.enable(document.querySelector('button'));
 */
Object.defineProperty(util, 'enable', {
    enumerable: true,
    value(element = util.required('element')) {
        if (formTags.test(element.tagName)) {
            element.removeAttribute('disabled');
            return;
        }

        element.classList.remove(disabledClass);
        element.removeAttribute('tabindex'); //TODO:fix toggle
        element.ariaDisabled = false;
    }
});

/**
 * @function app.util#isDisabled
 * @desc Проверяет, неактивный ли передаваемый элемент.
 * @param {HTMLElement} element - Элемент для проверки.
 * @returns {boolean} True, если элемент неактивен.
 * @readonly
 * @example
 * console.log(app.util.isDisabled(document.querySelector('button')));
 */
Object.defineProperty(util, 'isDisabled', {
    enumerable: true,
    value(element = util.required('element')) {
        if (formTags.test(element.tagName)) {
            return element.disabled || element.classList.contains(disabledClass);
        }

        return element.classList.contains(disabledClass);
    }
});

//Активация/деактивация
/**
 * @function app.util#activate
 * @desc Добавляет элементу атрибуты активности.
 * @param {HTMLElement} element - Элемент для добавления атрибутов.
 * @param {string} [activeClass='active'] - Класс, добавляемый элементу.
 * @returns {undefined}
 * @readonly
 * @example
 * app.util.activate(document.querySelector('button'));
 */
Object.defineProperty(util, 'activate', {
    enumerable: true,
    value(element = util.required('element'), activeClass = activeClassStr) {
        //TODO:aria-expanded arg
        element.classList.add(activeClass);

        if (element.tagName === 'BUTTON') {
            element.ariaPressed = true;
        } else if ((element.tagName === 'INPUT') && checkable.test(element.type)) {
            element.ariaChecked = true;
        } else if (element.tagName === 'OPTION') {
            element.ariaSelected = true;
        }
    }
});

/**
 * @function app.util#deactivate
 * @desc Удаляет у элемента атрибуты активности.
 * @param {HTMLElement} element - Элемент для удаления атрибутов.
 * @param {string} [activeClass='active'] - Класс, удаляемый у элемента.
 * @returns {undefined}
 * @readonly
 * @example
 * app.util.deactivate(document.querySelector('button'));
 */
Object.defineProperty(util, 'deactivate', {
    enumerable: true,
    value(element = util.required('element'), activeClass = activeClassStr) {
        element.classList.remove(activeClass);

        if (element.tagName === 'BUTTON') {
            element.ariaPressed = false;
        } else if ((element.tagName === 'INPUT') && checkable.test(element.type)) {
            element.ariaChecked = false;
        } else if (element.tagName === 'OPTION') {
            element.ariaSelected = false;
        }
    }
});