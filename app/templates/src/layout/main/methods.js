import util from './init.js';

//Методы для функций
/**
 * @function app.util#error
 * @desc Если передаётся строка или объект c единственным свойством message, то возвращает экземпляр ошибки. В остальных случаях возвращает переданный объект.
 * @param {(string|Object)} message - Сообщение об ошибке или объект с информацией об ошибке.
 * @returns {Object} Либо обычный объект, либо экземпляр ошибки.
 * @readonly
 * @example
 * util.error({
 *     message: 'Стандартный вывод ошибки'
 * });
 *
 * @example
 * util.error({
 *     message: 'Нестандартная ошибка',
 *     param: 123,
 *     element: document.querySelector('.element-with-error')
 * });
 */
Object.defineProperty(util, 'error', {
    enumerable: true,
    value(message = util.required('message')) {
        if (util.isObject(message) && (message !== null)) {
            throw (Object.keys(message).length > 1) ? message : new Error(message.message);
        }
        throw new Error(message);
    }
});

/**
 * @function app.util#typeError
 * @desc Выбрасывает ошибку типа переменной.
 * @param {*} value - Ошибочное значение.
 * @param {string} valueName - Название ошибочного параметра.
 * @param {string} type - Ожидаемый тип параметра.
 * @throws Выбрасывает объект с описанием ошибки типа.
 * @readonly
 * @example
 * const numberValue = '1';
 * if (typeof numberValue !== 'number') {
 *     util.typeError(numberValue, 'numberValue', 'number'); //Uncaught Error: 'numberValue' must be a 'number'
 * }
 */
Object.defineProperty(util, 'typeError', {
    enumerable: true,
    value(value, valueName = util.required('valueName'), type = util.required('type')) {
        const errorObject = {
            message: `'${valueName}' must be a ${type}`,
            value
        };
        throw errorObject;
    }
});

/**
 * @function app.util#required
 * @desc Выбрасывает ошибку об обязательном параметре.
 * @param {string} [parameter] - Название обязательного параметра.
 * @throws Выбрасывает ошибку о незаданном параметре.
 * @readonly
 * @example
 * const test = (arg = app.util.required('arg')) => {
 *     //...
 * }
 * test(); //Uncaught Error: missing parameter 'arg'
 */
Object.defineProperty(util, 'required', {
    enumerable: true,
    value(parameter) {
        throw new Error(`missing parameter${parameter ? ` '${parameter}'` : ''}`);
    }
});

//Вспомогательные методы
/**
 * @function app.util#isObject
 * @desc Определяет, является ли передаваемое значение чистым объектом (то есть не функцией, не коллекцией, не другими объекто-подобными типами).
 * @param {*} [value] - Значение для проверки.
 * @returns {boolean}
 * @readonly
 * @example
 * app.util.isObject({}); //true
 * app.util.isObject(new Object()); //true
 * app.util.isObject([]); //false
 * app.util.isObject(function func() {}); //false
 * app.util.isObject(new Date()); //false
 * app.util.isObject(document.body); //false
 * app.util.isObject(null); //false
 */
Object.defineProperty(util, 'isObject', {
    enumerable: true,
    value(value) {
        if ((typeof value === 'undefined') || (value === null)) {
            return false;
        }

        return value.constructor === Object;
    }
});

/**
 * @function app.util#attempt
 * @desc Пытается вызвать функцию, возвращая или результат функции или объект Error.
 * @param {Function} func - Вызываемая функция.
 * @param {...*} [args] - Параметры, передаваемые вызываемой функции.
 * @returns {(*|Error)} Результат функции или объект Error.
 * @readonly
 * @example
 * const result = app.util.attempt(selector => {
 *     return document.querySelector(selector);
 * }, '>');
 * console.log(result); //DOMException: Failed to execute 'querySelector' on 'Document': '>' is not a valid selector.
 * console.log(result instanceof Error); //true
 */
Object.defineProperty(util, 'attempt', {
    enumerable: true,
    value(func = util.required('func'), ...args) {
        if (typeof func !== 'function') {
            util.typeError(func, 'func', 'function');
        }

        try {
            return func.call(window, ...args);
        } catch (error) {
            return (error instanceof Error) ? error : new Error(error);
        }
    }
});

/**
 * @function app.util#extend
 * @desc Рекурсивно копирует свойства добавляемого объекта в целевой объект. Не "чистые" объекты и коллекции копируются не рекурсивно.
 * @param {Object} targetObject - Целевой объект.
 * @param {Object} addedObject - Добавляемый объект.
 * @returns {Object} Изменённый целевой объект.
 * @readonly
 * @example
 * const targetObj = {
 *    a: 1,
 *    b: {c: 2}
 * };
 * const addedObj = {
 *     d: 3,
 *     b: {c: 4, e: 5}
 * };
 * app.util.extend(targetObj, addedObj);
 * console.log(targetObj); // {a: 1, b: {c: 4, e: 5}, d: 3}
 */
Object.defineProperty(util, 'extend', {
    enumerable: true,
    value(targetObject = util.required('targetObject'), addedObject = util.required('addedObject')) {
        if (typeof targetObject !== 'object') {
            util.typeError(targetObject, 'targetObject', 'object');
        }
        if (typeof addedObject !== 'object') {
            util.typeError(addedObject, 'addedObject', 'object');
        }

        Object.keys(addedObject).forEach(prop => {
            const targetObjectProp = targetObject[prop];
            const addedObjectProp = addedObject[prop];
            const isAddedObject = util.isObject(addedObjectProp);

            if (util.isObject(targetObjectProp) && isAddedObject) {
                util.extend(targetObjectProp, addedObjectProp);
                return;
            }

            targetObject[prop] = isAddedObject ? util.extend({}, addedObjectProp) : addedObjectProp;
        });

        return targetObject;
    }
});

/**
 * @function app.util#defaultsDeep
 * @desc Рекурсивно копирует свойства добавляемого объекта, не назначенные в целевом объекте, в целевой объект. Не "чистые" объекты и коллекции копируются не рекурсивно.
 * @param {Object} targetObject - Целевой объект.
 * @param {Object} addedObject - Добавляемый объект.
 * @returns {Object} Изменённый целевой объект.
 * @readonly
 * @example
 * const targetObj = {
 *    a: 1,
 *    b: {c: 2}
 * };
 * const addedObj = {
 *     d: 3,
 *     b: {c: 4, e: 5}
 * };
 * app.util.defaultsDeep(targetObj, addedObj);
 * console.log(targetObj); // {a: 1, b: {c: 2, e: 5}, d: 3}
 */
Object.defineProperty(util, 'defaultsDeep', {
    enumerable: true,
    value(targetObject = util.required('targetObject'), addedObject = util.required('addedObject')) {
        if (typeof targetObject !== 'object') {
            util.typeError(targetObject, 'targetObject', 'object');
        }
        if (typeof addedObject !== 'object') {
            util.typeError(addedObject, 'addedObject', 'object');
        }

        Object.keys(addedObject).forEach(prop => {
            const targetObjectProp = targetObject[prop];
            const addedObjectProp = addedObject[prop];
            const isAddedObject = util.isObject(addedObjectProp);

            if (typeof targetObjectProp === 'undefined') {
                targetObject[prop] = isAddedObject ? util.defaultsDeep({}, addedObjectProp) : addedObjectProp;
            }

            if (util.isObject(targetObjectProp) && isAddedObject) {
                util.defaultsDeep(targetObjectProp, addedObjectProp);
            }
        });

        return targetObject;
    }
});

//Методы с числами
let idCounter = 0;

/**
 * @function app.util#uniqueId
 * @desc Генерирует число, не совпадающее с числами, генерируемыми предыдущими вызовами этой функции.
 * @returns {number} Уникальное число.
 * @readonly
 * @example
 * console.log(app.util.uniqueId()); //1
 * console.log(app.util.uniqueId()); //2
 * console.log(app.util.uniqueId()); //3
 */
Object.defineProperty(util, 'uniqueId', {
    enumerable: true,
    value() {
        idCounter += 1;
        return idCounter;
    }
});

/**
 * @function app.util#randomInt
 * @desc Генерирует случайное число.
 * @param {number} min - Минимальное случайное число.
 * @param {number} max - Максимальное случайное число.
 * @returns {number} Случайное число.
 * @readonly
 * @example
 * console.log(app.util.randomInt(1, 2)); //случайное число от 1 до 2 включительно
 */
Object.defineProperty(util, 'randomInt', {
    enumerable: true,
    value(min = util.required('min'), max = util.required('max')) {
        const minRounded = Math.ceil(min);
        const maxRounded = Math.floor(max);

        if (Number.isNaN(minRounded)) {
            util.typeError(minRounded, 'minRounded', 'number');
        }
        if (Number.isNaN(maxRounded)) {
            util.typeError(maxRounded, 'maxRounded', 'number');
        }

        return Math.floor(Math.random() * (maxRounded - minRounded + 1)) + minRounded;
    }
});

//Методы с временем
/**
 * @function app.util#defer
 * @desc Откладывает выполнение функции на минимальное время.
 * @param {Function} func - Вызываемая функция.
 * @param {...*} [args] - Параметры, передаваемые вызываемой функции.
 * @returns {number} Идентификатор функции задержки.
 * @readonly
 * @example
 * app.util.defer(() => {
 *     console.log(app.util.uniqueId()); //2
 * });
 * console.log(app.util.uniqueId()); //1
 */
Object.defineProperty(util, 'defer', {
    enumerable: true,
    value(func = util.required('func'), ...args) {
        return setTimeout(() => {
            func.call(window, ...args);
        }, 0);
    }
});

/**
 * @function app.util#animationDefer
 * @desc Откладывает выполнение функции на минимальное время обновления анимации.
 * @param {Function} func - Вызываемая функция.
 * @param {...*} [args] - Параметры, передаваемые вызываемой функции.
 * @returns {number} Идентификатор функции задержки.
 * @readonly
 * @example
 * app.util.animationDefer(() => {
 *     console.log(app.util.uniqueId()); //2
 * });
 * console.log(app.util.uniqueId()); //1
 */
Object.defineProperty(util, 'animationDefer', {
    enumerable: true,
    value(func = util.required('func'), ...args) {
        return window.requestAnimationFrame(() => {
            func.call(window, ...args);
        });
    }
});

/**
 * @function app.util#debounce
 * @desc Лимитирует частоту выполнения функции.
 * @param {Function} func - Вызываемая функция.
 * @param {number} wait - Время минимального повторного выполнения функции.
 * @param {boolean} [immediate] - Если передано true, то вызывать функцию сразу же.
 * @returns {Function} Функция с ограниченной частотой выполнения.
 * @readonly
 * @example
 * const debouncedFunc = app.util.debounce(() => {
 *     console.log('эта функция вызывается не чаще, чем один раз в 200ms');
 * }, 200, true);
 */
Object.defineProperty(util, 'debounce', {
    enumerable: true,
    value(func = util.required('func'), wait = util.required('wait'), immediate = false) {
        let timeout;

        /**
         * @desc Возвращаемая отложенная функция.
         * @param {...*} [args] - Параметры функции.
         * @this window
         * @returns {undefined}
         */
        return function(...args) {
            const later = () => {
                timeout = null;
                if (!immediate) {
                    func.call(this, ...args);
                }
            };
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) {
                func.call(this, ...args);
            }
        };
    }
});

//Методы со строками
/**
 * @function app.util#camelCase
 * @desc Преобразует строку в camelCase.
 * @param {string} string - Форматируемая строка.
 * @returns {string} Результат форматирования.
 * @readonly
 * @example
 * console.log(app.util.camelCase('background-color')); //'backgroundColor'
 */
Object.defineProperty(util, 'camelCase', {
    enumerable: true,
    value(string = util.required('string')) {
        return string
            .replace(/^\w|[-A-Z]|\b\w|\s+/g, (match, index) => {
                return (index === 0) ? match.toLowerCase() : match.toUpperCase();
            })
            .replace(/[\s-]+/g, '');
    }
});

/**
 * @function app.util#kebabCase
 * @desc Преобразует строку в kebabCase.
 * @param {string} string - Форматируемая строка.
 * @returns {string} Результат форматирования.
 * @readonly
 * @example
 * console.log(app.util.kebabCase('backgroundColor')); //'background-color'
 */
Object.defineProperty(util, 'kebabCase', {
    enumerable: true,
    value(string = util.required('string')) {
        return string
            .replace(/([a-z])([A-Z])/g, '$1-$2')
            .replace(/[\s_]+/g, '-')
            .toLowerCase();
    }
});

/**
 * @function app.util#removeDelimiters
 * @desc Убирает разделители в строке (точки, пробелы, запятые, двоеточия и дефисы).
 * @param {(string|number)} value - Форматируемая строка.
 * @param {string} [replacer=''] - Строка, заменяющая найденые разделители.
 * @param {string} [delimiters='\\s,.:-'] - Разделители, которые необходимо убрать.
 * @returns {(string|Object)} Результат форматирования или null, если не удалось вернуть строку.
 * @readonly
 * @example
 * console.log(app.util.removeDelimiters('123 456')); //'123456'
 * console.log(app.util.removeDelimiters('123 456.22', '-')); //'123-456-22'
 * console.log(app.util.removeDelimiters('123 456.22', '-', ' ')); //'123-456.22'
 */
Object.defineProperty(util, 'removeDelimiters', {
    enumerable: true,
    value(value = util.required('value'), replacer = '', delimiters = '\\s,.:-') {
        const result = util.attempt(() => {
            return String(value).replace(new RegExp(`[${delimiters}]`, 'g'), replacer);
        });
        if (result instanceof Error) return null;

        return result;
    }
});

/**
 * @function app.util#stringToFloat
 * @desc Переводит строку в дробное число.
 * @param {(string|number)} value - Форматируемая строка.
 * @returns {(number|Object)} Результат форматирования или null, если не удалось вернуть число.
 * @readonly
 * @example
 * console.log(app.util.stringToFloat('123 456 ₽')); //123456
 */
Object.defineProperty(util, 'stringToFloat', {
    enumerable: true,
    value(value = util.required('value')) {
        const result = Number.parseFloat(util.removeDelimiters(value));
        if (Number.isNaN(result)) return null;

        return result;
    }
});

//TODO:currency opt
const currencyFormatDefaults = {
    separator: ' ',
    cents: true
};

/**
 * @function app.util#currencyFormat
 * @desc Форматирует строку в формат вывода валюты.
 * @param {(string|number)} value - Форматируемая строка.
 * @param {Object} [options={}] - Опции форматирования.
 * @param {string} [options.separator=' '] - Строка-разделитель тысяч.
 * @param {boolean} [options.cents=true] - Добавлять ли цифры после точки.
 * @returns {(string|Object)} Результат форматирования или null, если не удалось вернуть строку.
 * @readonly
 * @example
 * console.log(app.util.currencyFormat('12345678.22')); //'12 345 678.22'
 * console.log(app.util.currencyFormat('12345678', {separator: '-'})); //'12-345-678'
 * console.log(app.util.currencyFormat('12345678.22', {cents: false})); //'12 345 678'
 * console.log(app.util.currencyFormat('12345678.123')); //'12 345 678.12'
 */
Object.defineProperty(util, 'currencyFormat', {
    enumerable: true,
    value(value = util.required('value'), options = {}) {
        if (!util.isObject(options)) {
            util.typeError(options, 'options', 'plain object');
        }

        util.defaultsDeep(options, currencyFormatDefaults);

        const hasCents = options.cents && String(value).includes('.');
        const initialValue = util.attempt(() => {
            if (hasCents) {
                return util.removeDelimiters(value, '', '\\s:-');
            }
            return util.removeDelimiters(value);
        });
        if (initialValue instanceof Error) return null;

        let baseValue = initialValue;
        let valueSuffix = '';
        if (hasCents) {
            const values = initialValue.split('.');
            baseValue = values[0];
            valueSuffix = `.${values[1].slice(0, 2)}`;
        }

        const result = util.attempt(() => {
            return baseValue.replace(/\B(?=(\d{3})+(?!\d))/g, options.separator);
        });
        if (result instanceof Error) return null;
        return result + valueSuffix;
    }
});

/**
 * @function app.util#formatPlural
 * @desc Возвращает определенные окончания согласно передаваемому числу.
 * @param {number} num - Число, согласно которому возвращается окончание.
 * @param {string} [one=''] - Окончание для чисел, оканчивающихся на 1.
 * @param {string} [two=''] - Окончание для чисел, оканчивающихся на 2.
 * @param {string} [many=''] - Окончание для чисел чисел, оканчивающихся на остальные числа.
 * @returns {string} Окончание.
 * @readonly
 * @example
 * console.log(`122 товар${formatPlural(122, '', 'а', 'ов')}`); //'122 товара'
 * console.log(`1 товар${formatPlural(1, '', 'а', 'ов')}`); //'1 товар'
 * console.log(`0 товар${formatPlural(0, '', 'а', 'ов')}`); //'1 товаров'
 * console.log(`5 товар${formatPlural(5, '', 'а', 'ов')}`); //'5 товаров'
 */
Object.defineProperty(util, 'formatPlural', {
    enumerable: true,
    value(num = util.required('num'), one = '', two = '', many = '') { /* eslint-disable-line max-params */
        const parsedNum = Number.parseInt(num, 10);
        if (!Number.isFinite(parsedNum)) return '';

        const tenRemainder = parsedNum % 10;
        const hundredRemainder = parsedNum % 100;

        const endOnOne = (tenRemainder === 1) && (hundredRemainder !== 11);
        const endOnTwo = (tenRemainder >= 2) && (tenRemainder <= 4) && ((hundredRemainder < 10) || (hundredRemainder >= 20));
        const notOne = endOnTwo ? two : many;

        return endOnOne ? one : notOne;
    }
});

const htmlEscapes = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#39;'
};
const htmlUnescapes = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '"',
    '&#39;': '\''
};
const unescapedHtml = new RegExp('[&<>"\']', 'g');
const escapedHtml = new RegExp('&(?:amp|lt|gt|quot|#39);', 'g');

/**
 * @function app.util#escapeHtml
 * @desc Преобразует HTML-строку в формат для вставки в HTML-атрибуты.
 * @param {string} string - Строка для преобразования.
 * @returns {string} Результат преобразования.
 * @readonly
 * @example
 * console.log(app.util.escapeHtml('<div></div>')); //&lt;div&gt;&lt;/div&gt;
 * console.log(app.util.escapeHtml('&lt;div&gt;&lt;/div&gt;')); //&lt;div&gt;&lt;/div&gt;
 * console.log(app.util.escapeHtml('<div')); //&lt;div
 * console.log(app.util.escapeHtml(null)); //'null'
 */
Object.defineProperty(util, 'escapeHtml', {
    enumerable: true,
    value(string = util.required('string')) {
        const formattedString = String(string);
        if (unescapedHtml.test(formattedString)) {
            return formattedString.replace(unescapedHtml, key => htmlEscapes[key]);
        }

        return formattedString;
    }
});

/**
 * @function app.util#unescapeHtml
 * @desc Преобразует строку в HTML-формат.
 * @param {string} string - Строка для преобразования.
 * @returns {string} Результат преобразования.
 * @readonly
 * @example
 * console.log(app.util.unescapeHtml('&lt;div&gt;&lt;/div&gt;')); //<div></div>
 * console.log(app.util.unescapeHtml('<div></div>')); //<div></div>
 * console.log(app.util.unescapeHtml('&lt;div')); //<div
 * console.log(app.util.unescapeHtml(null)); //'null'
 */
Object.defineProperty(util, 'unescapeHtml', {
    enumerable: true,
    value(string = util.required('string')) {
        const formattedString = String(string);
        if (escapedHtml.test(formattedString)) {
            return formattedString.replace(escapedHtml, key => htmlUnescapes[key]);
        }

        return formattedString;
    }
});

/**
 * @function app.util#stringToJSON
 * @desc Конверитрует строку в формат json-файла.
 * @param {string} string - Строка для преобразования.
 * @returns {(string|Object)} Результат преобразования или null в случае ошибки.
 * @readonly
 * @example
 * console.log(app.util.stringToJSON('{"prop": "value"}')); //{prop: 'value'}
 * console.log(app.util.stringToJSON('"prop" = "value"')); //null
 */
Object.defineProperty(util, 'stringToJSON', {
    enumerable: true,
    value(string = util.required('string')) {
        const result = util.attempt(() => JSON.parse(string));
        return util.isObject(result) ? result : null;
    }
});

/**
 * @function app.util#isAbsoluteUrl
 * @desc Проверяет, является ли строка абсолютной ссылкой (начинается на 'http://', на 'https://' или на '//').
 * @param {string} string - Строка для проверки.
 * @returns {boolean} Является ли строка абсолютной ссылкой.
 * @readonly
 * @example
 * console.log(app.util.isAbsoluteUrl('https://www.google.com/')); //true
 * console.log(app.util.isAbsoluteUrl('http://www.google.com/')); //true
 * console.log(app.util.isAbsoluteUrl('//www.google.com')); //true
 * console.log(app.util.isAbsoluteUrl('www.google.com')); //false
 * console.log(app.util.isAbsoluteUrl('https://')); //false
 * console.log(app.util.isAbsoluteUrl('//google')); //false
 */
Object.defineProperty(util, 'isAbsoluteUrl', {
    enumerable: true,
    value(string = util.required('string')) {
        const urlRegex = /^(https?:)?\/\/[^/]+/;
        return urlRegex.test(string);
    }
});

/**
 * @function serializeItem
 * @desc Сериализует в строку элемент объекта.
 * @param {string} key - Текущий ключ объекта.
 * @param {*} value - Значение ключа объекта.
 * @returns {string} Сериализованная строка элемента.
 * @ignore
 */
const serializeItem = (key, value) => {
    return ((value !== null) && (typeof value === 'object'))
        ? util.serialize(value, key)
        : `&${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
};

/**
 * @function app.util#serialize
 * @desc Сериализирует объект.
 * @param {(Object|Array.<Array>)} dataObject - Объект с элементами для сериализации или коллекция с коллекциями вида ['ключ', 'значение'].
 * @param {string} [prefix] - Текущий ключ объекта, к которому добавляются форматируемые свойства.
 * @returns {string} Сериализованная строка.
 * @readonly
 * @ignore
 */
Object.defineProperty(util, 'serialize', {
    enumerable: true,
    value(dataObject = util.required('dataObject'), prefix = '') {
        const query = Array.isArray(dataObject)
            ? dataObject.map((dataItem, index) => {
                let key;
                let value;
                if (Array.isArray(dataItem) && dataItem[1]) {
                    key = prefix ? `${prefix}[${dataItem[0]}]` : dataItem[0];
                    value = dataItem[1];
                } else {
                    key = prefix ? `${prefix}[${index}]` : index;
                    value = dataItem;
                }
                return serializeItem(key, value);
            })
            : Object.keys(dataObject).map(dataItem => {
                const key = prefix ? `${prefix}[${dataItem}]` : dataItem;
                const value = dataObject[dataItem];
                return serializeItem(key, value);
            });

        return query.join('');
    }
});

const addSpacesLang = (window.lang && window.lang.addSpaces) || require(`Layout/add-spaces/lang/${__LANG__}.json`).data;

if (addSpacesLang) {
    /**
     * @function app.util#addSpaces
     * @desc Добавляет неразрывные пробелы в переданную HTML-строку.
     * @param {string} htmlString - HTML-строка для редактирования.
     * @returns {string} HTML-строка с неразрывными пробелами.
     * @readonly
     * @example
     * const htmlString = 'Только до конца лета предлагаем сочный набор ягод по специальной цене';
     * app.util.addSpaces(htmlString); //'Только до&nbsp;конца лета предлагаем сочный набор ягод по&nbsp;специальной цене'
     */
    Object.defineProperty(util, 'addSpaces', {
        enumerable: true,
        value(htmlString = util.required('htmlString')) {
            return htmlString.replace(new RegExp(`(\\s|>)(${addSpacesLang.regex})\\s+[^<\\s]`, 'gi'), (...args) => {
                return args[1] + args[0].slice(1).replace(/\s+/, '&nbsp;');
            });
        }
    });
}

//Другие
/**
 * @function app.util#getNodeProperty
 * @desc Возвращает собственное свойство HTML-тега.
 * @param {HTMLElement} element - HTML-элемент, свойство которого нужно получить.
 * @param {string} property - Название свойства, которое необъодимо получить.
 * @returns {*} Значение свойства HTML-тега.
 * @readonly
 * @example
 * console.log(app.util.getNodeProperty(formTag, 'action'));
 */
Object.defineProperty(util, 'getNodeProperty', {
    enumerable: true,
    value(element = util.required('element'), property = util.required('property')) {
        const prototype = Object.getPrototypeOf(element);
        const ownDescriptor = Object.getOwnPropertyDescriptor(prototype, property);
        return ownDescriptor.get.call(element);
    }
});

/**
 * @function app.util#addScript
 * @desc Загружает новый скрипт асинхронно на страницу путём добавления тега в конец head.
 * @param {string} url - Путь к файлу скрипта.
 * @param {Function} [callback] - Функция обратного вызова, вызываемая после загрузки скрипта.
 * @returns {HTMLElement} Элемент добавленного на страницу тега script.
 * @readonly
 * @example
 * const script = app.util.addScript('https://mc.yandex.ru/metrika/tag.js', event => {
 *     console.log(event.currentTarget); //Выведет добавленный на страницу тег script
 * });
 */
Object.defineProperty(util, 'addScript', {
    enumerable: true,
    value(url = util.required('url'), callback) { /* eslint-disable-line default-param-last */
        const script = document.createElement('script');
        script.async = true;
        script.defer = true;
        script.src = url;
        if (typeof callback === 'function') {
            script.addEventListener('load', callback);
        }

        document.head.append(script);
        return script;
    }
});

const rootStyle = document.documentElement;

/**
 * @function app.util#getStyle
 * @desc Получает значение css-переменной.
 * @param {string} cssVariable - Название css-переменной.
 * @param {HTMLElement} [styleElement] - HTML-элемент, у которого задана css-переменная. По-умолчанию, переменная ищется в корневом элементе.
 * @throws Выбрасывает объект с описанием ошибки типа.
 * @returns {string} Значение css-переменной.
 * @readonly
 * @example
 * app.util.getStyle('--slideout-width');
 */
Object.defineProperty(util, 'getStyle', {
    enumerable: true,
    value(cssVariable, styleElement = rootStyle) {
        if ((typeof cssVariable !== 'string') || !cssVariable) {
            util.typeError(cssVariable, 'cssVariable', 'non-empty string');
        }

        const variableValue = window.getComputedStyle(styleElement).getPropertyValue(cssVariable);
        return variableValue.trim();
    }
});

/**
 * @function app.util#setStyle
 * @desc Устанавливает значение css-переменной.
 * @param {string} cssVariable - Название css-переменной.
 * @param {string} value - Значение, устанавливаемое css-переменной.
 * @param {HTMLElement} styleElement - HTML-элемент, у которого должна быть задана css-переменная. По-умолчанию, переменная устанавливается у корневого элемента.
 * @throws Выбрасывает объект с описанием ошибки типа.
 * @readonly
 * @example
 * app.util.setStyle('--slideout-width', '320px');
 */
Object.defineProperty(util, 'setStyle', {
    enumerable: true,
    value(cssVariable, value, styleElement = rootStyle) {
        if ((typeof cssVariable !== 'string') || !cssVariable) {
            util.typeError(cssVariable, 'cssVariable', 'non-empty string');
        }

        styleElement.style.setProperty(cssVariable, value);
    }
});