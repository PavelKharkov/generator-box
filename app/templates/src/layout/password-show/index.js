import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'passwordShow';

//Селекторы и классы
const passwordShowSelector = '.password-show';
const formGroupSelector = '.form-group';

const passwordShowEl = document.querySelectorAll(passwordShowSelector);

/**
 * @class AppPasswordShow
 * @memberof layout
 * @classdesc Модуль кнопки смены видимости пароля.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const passwordShowInstance = new app.PasswordShow(document.querySelector('.password-show-button'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="form-group">
 *     <button class="password-show" type="button" title="Показать/скрыть пароль"></button>
 *     <input type="password">
 * </div>
 *
 * <!--.password-show - селектор по умолчанию-->
 */
const AppPasswordShow = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let isActive = false;

        /**
         * @member {boolean} AppPasswordShow#isActive
         * @memberof layout
         * @desc Указывает, нажата ли кнопка смены видимости пароля.
         * @readonly
         */
        Object.defineProperty(this, 'isActive', {
            enumerable: true,
            get: () => isActive
        });

        const input = util.attempt(() => el.closest(formGroupSelector).querySelector('input[type="password"]'));
        if (!input || (input instanceof Error)) return;

        /**
         * @member {HTMLElement} AppPasswordShow#input
         * @memberof layout
         * @desc Элемент поля с паролем.
         * @readonly
         */
        Object.defineProperty(this, 'input', {
            enumerable: true,
            value: input
        });

        if (!input.modules) {
            input.modules = {};
        }
        input.modules[moduleName] = this;

        el.addEventListener('click', () => {
            if (isActive) {
                isActive = false;
                util.deactivate(el);
                input.type = 'password';
                return;
            }

            isActive = true;
            util.activate(el);
            input.type = 'text';
        });
    }
});

addModule(moduleName, AppPasswordShow);

//Инициализация элементов по классу
passwordShowEl.forEach(el => new AppPasswordShow(el));

export default AppPasswordShow;
export {AppPasswordShow};