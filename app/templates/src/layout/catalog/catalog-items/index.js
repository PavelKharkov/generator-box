import './style.scss';

if (__IS_ENABLED_MODULE__) {
    require('./variants/tile.scss');
}

if (__IS_ENABLED_MODULE__) {
    require('./variants/list.scss');
}

if (__IS_ENABLED_MODULE__) {
    require('./variants/table.scss');
}