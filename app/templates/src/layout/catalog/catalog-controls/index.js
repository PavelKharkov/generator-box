import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'catalogControls';

/**
 * @class AppCatalogControls
 * @memberof layout
 * @requires components#AppSlideout
 * @classdesc Модуль верхнего блока каталога.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const catalogControlsInstance = new app.CatalogControls(document.querySelector('.catalog-controls-element'));
 */
const AppCatalogControls = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const filterButton = el.querySelector('.catalog-filter-button__button');

        /**
         * @member {HTMLElement} AppCatalogControls#filterButton
         * @memberof layout
         * @desc Ссылка на кнопку открытия фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'filterButton', {
            enumerable: true,
            value: filterButton
        });

        let slideout;

        /**
         * @member {Object} AppCatalogControls#slideout
         * @memberof layout
         * @desc Ссылка на экземпляр модуля выезжающего меню [AppSlideout]{@link layout.AppSlideout} для фильтра.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slideout', {
            enumerable: true,
            get: () => slideout
        });

        if (filterButton) {
            require('Components/slideout').default.then(AppSlideout => {
                const {baseSlideout} = AppSlideout;

                slideout = baseSlideout;
            });
            emitInit('slideout');

            util.enable(filterButton);
            filterButton.addEventListener('click', () => {
                if (!slideout) return;
                slideout.show({
                    type: 'filter',
                    focusOnShow: false
                });
            });
        }

        //Кнопки сортировки каталога
        require('../catalog-sort');

        //Кнопки выбора отображения каталога
        require('../catalog-view');
    }
});

addModule(moduleName, AppCatalogControls);

export default AppCatalogControls;
export {AppCatalogControls};