import './sync.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

let AppCatalogControls;

//Опции
const moduleName = 'catalog';

const catalogEl = document.querySelectorAll('.catalog');

/**
 * @class AppCatalog
 * @memberof layout
 * @classdesc Модуль каталога.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const catalogInstance = new app.Catalog(document.querySelector('.catalog-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="catalog"></div>
 *
 * <!--.catalog - селектор по умолчанию-->
 */
const AppCatalog = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Элемент упралвения каталога
        const catalogControlsEl = document.querySelectorAll('.catalog__controls');

        let controls;

        /**
         * @member {Array.<Object>} AppCatalog#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули верхних блоков каталога [AppCartControls]{@link layout.AppCatalogControls}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        if (catalogControlsEl.length > 0) {
            AppCatalogControls = require('./catalog-controls').default;

            controls = [...catalogControlsEl].map(item => {
                const catalogControls = new AppCatalogControls(item);
                return catalogControls;
            });
        }

        //Элемент управления выбором количества отображаемых элементов каталога на странице
        require('./catalog-count');

        //Элементы каталога
        require('./catalog-items');

        //Разводная страница каталога
        require('./catalog-sections');
    }
});

addModule(moduleName, AppCatalog);

catalogEl.forEach(el => new AppCatalog(el));

export default AppCatalog;
export {
    AppCatalog,
    AppCatalogControls
};