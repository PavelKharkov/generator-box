let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const catalogCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "catalog" */ './sync.js'))
        .then(catalogModules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppCatalog = catalogModules.default;

            if (isModules) {
                const {AppCatalogControls} = catalogModules;
                resolve({
                    AppCatalog,
                    AppCatalogControls
                });
                return;
            }

            resolve(AppCatalog);
        });
};

const importFunc = new Promise(resolve => {
    catalogCallback(resolve);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    catalogCallback(resolve, true);
});

export default importFunc;
export {modules};