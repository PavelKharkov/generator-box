let importFunc;

if (__IS_DISABLED_MODULE__) {
    require('./style.scss');

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const scrollupCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const AppScrollup = modules;
                resolve(AppScrollup);
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            scrollupCallback(resolve);
            return;
        }

        onInit('scrollup', () => {
            scrollupCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('scrollup');
        });
    });
}

export default importFunc;