import './sync.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

let AppScrollup;

//Опции
const moduleName = 'scrollup';

const scrollDuration = 300;

const scrollupSelector = '.scrollup';
const fixedClass = 'position-fixed';
const buttonSelector = `${scrollupSelector}__button`;

const scrollupEl = document.querySelectorAll(scrollupSelector);

if (scrollupEl.length > 0) {
    /**
     * @class AppScrollup
     * @memberof layout
     * @requires layout#AppWindow
     * @classdesc Модуль кнопки прокрутки к началу страницы.
     * @desc Наследует: [Module]{@link app.Module}.
     * @ignore
     * @example
     * const scrollupInstance = new app.Scrollup(document.querySelector('.scrollup-element'));
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="scrollup">
     *     <button class="scrollup__button" type="button"></button>
     * </div>
     *
     * <!--.scrollup - селектор по умолчанию-->
     */
    AppScrollup = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            //TODO:add fixed prop

            /**
             * @function AppScrollup#fixedChange
             * @memberof layout
             * @desc Меняет состояние кнопки прокрутки.
             * @param {boolean} [fixed] - Менять ли состояние кнопки на фиксированное.
             * @returns {undefined}
             * @readonly
             * @example
             * scrollupInstance.fixedChange(true);
             */
            Object.defineProperty(this, 'fixedChange', {
                enumerable: true,
                value(fixed) {
                    el.classList[fixed ? 'add' : 'remove'](fixedClass);
                }
            });

            const button = el.querySelector(buttonSelector);

            /**
             * @member {HTMLElement} AppScrollup#button
             * @memberof layout
             * @desc Элемент кнопки.
             * @readonly
             */
            Object.defineProperty(this, 'button', {
                enumerable: true,
                value: button
            });

            button.addEventListener('click', event => {
                event.preventDefault();
                this.scrollTop();
            });

            this.on({
                /**
                 * @event AppScrollup#scrollTop
                 * @memberof layout
                 * @desc Прокрутка к началу страницы.
                 * @fires AppWindow.scrollTop
                 * @returns {undefined}
                 * @example
                 * scrollupInstance.on('scrollTop', () => {
                 *     console.log('Была нажата кнопка прокрутки к началу страницы');
                 * });
                 */
                scrollTop() {
                    AppWindow.scrollTop({
                        value: 0,
                        duration: scrollDuration
                    });
                }
            });
        }
    });

    addModule(moduleName, AppScrollup);

    //Инициализация элементов по классу
    scrollupEl.forEach(el => new AppScrollup(el));
}

export default AppScrollup;
export {AppScrollup};