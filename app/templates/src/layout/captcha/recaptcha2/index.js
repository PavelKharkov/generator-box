import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {app, addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'recaptcha';

const recaptchaClass = 'form-recaptcha';

const globalSitekey = app.globalConfig.recaptchaSitekey;

let scriptPlaced = false;
let grecaptcha;

//Опции по умолчанию
const defaultOptions = {
    sitekey: globalSitekey
};

/**
 * @class AppRecaptcha
 * @memberof layout
 * @classdesc Модуль Google reCaptcha 2.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} [element] - Элемент капчи.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {string} [options.sitekey=AppRecaptcha.sitekey] - Ключ для капчи. По умолчанию берётся из app.globalConfig.recaptchaSitekey.
 * @example
 * const recaptchaInstance = new app.Recaptcha(document.querySelector('.recaptcha-element'), {
 *     sitekey: 'Ключ для Google reCaptcha 2'
 * });
 */
const AppRecaptcha = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        util.defaultsDeep(this.options, defaultOptions);

        if (!this.options.sitekey) {
            this.options.sitekey = this.constructor.sitekey;
        }
        const sitekey = this.options.sitekey;

        /**
         * @member {Object} AppRecaptcha#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        Module.setParams(this);

        /**
         * @member {string} AppRecaptcha#sitekey
         * @memberof layout
         * @desc Ключ Google reCaptcha для данного экземпляра.
         * @readonly
         */
        Object.defineProperty(this, 'sitekey', {
            enumerable: true,
            value: sitekey
        });

        if (!scriptPlaced) {
            util.addScript('//www.google.com/recaptcha/api.js', () => {
                if (window.grecaptcha) {
                    grecaptcha = window.grecaptcha;
                }
            });
            scriptPlaced = true;
        }

        let response;

        /**
         * @member {Object} AppRecaptcha#response
         * @memberof layout
         * @desc Объект с параметрами ответа прохождения капчи.
         * @readonly
         */
        Object.defineProperty(AppRecaptcha, 'response', {
            enumerable: true,
            get: () => response
        });

        /**
         * @function AppRecaptcha#validate
         * @memberof layout
         * @desc Валидирует капчу.
         * @param {Function} callback - Функция обратного вызова при успешном прохождении капчи.
         * @returns {(Object|*)} Объект с параметрами Google reCaptcha или, если капча уже подтверждена, то возвращаемое значение функции обратного вызова.
         * @readonly
         * @example
         * recaptchaInstance.validate(() => {
         *     console.log('Капча успешно подтверждена');
         * });
         */
        Object.defineProperty(AppRecaptcha, 'validate', {
            enumerable: true,
            value(callback) {
                let captchaElement = el.querySelector(`.${recaptchaClass}`);
                if (!captchaElement) {
                    captchaElement = document.createElement('div');
                    captchaElement.classList.add(recaptchaClass);
                    el.append(captchaElement);
                }

                if (typeof response === 'undefined') {
                    response = grecaptcha.render(captchaElement, {
                        sitekey,
                        size: 'invisible',
                        callback
                    });
                }

                if (grecaptcha.getResponse(response)) {
                    return callback();
                }
                return grecaptcha.execute(response);
            }
        });
    }

    /**
     * @function AppRecaptcha.grecaptcha
     * @memberof layout
     * @desc Get-функция. Содержит ссылку на функцию grecaptcha.
     * @returns {Object}
     */
    static get grecaptcha() {
        return grecaptcha;
    }

    /**
     * @function AppRecaptcha.sitekey
     * @memberof layout
     * @desc Get-функция. Ключ Google reCaptcha.
     * @returns {string}
     */
    static get sitekey() {
        return globalSitekey;
    }
});

addModule(moduleName, AppRecaptcha);

export default AppRecaptcha;
export {AppRecaptcha};