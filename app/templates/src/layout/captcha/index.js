let importFunc;

if (__IS_DISABLED_MODULE__) {
    require('./style.scss');

    const {onInit, emitInit} = require('Base/scripts/app.js');
    const chunks = require('Base/scripts/chunks.js');

    const AppWindow = require('Layout/window').default;

    const captchaCallback = resolve => {
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
            .then(modules => {
                chunks.helpers = true;

                const {AppRecapcha} = modules;
                resolve({
                    AppRecapcha
                });
            });
    };

    importFunc = new Promise(resolve => {
        if (__IS_SYNC__ || chunks.helpers) {
            captchaCallback(resolve);
            return;
        }

        onInit('captcha', () => {
            captchaCallback(resolve);
        });
        AppWindow.onload(() => {
            emitInit('captcha');
        });

        //TODO:emit on click
    });
}

export default importFunc;