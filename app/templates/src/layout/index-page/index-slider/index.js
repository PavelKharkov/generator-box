import './style.scss';

import {Module, immutable} from 'Components/module';
//import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

//Опции
const moduleName = 'indexSlider';

const defaultSliderOptions = {
    pluginOptions: {
        slidesPerView: 1,

        init: true,
        grabCursor: true,
        watchOverflow: true,
        lazy: true,
        loop: true,

        autoplay: {
            delay: 4000
        }
    }
};

/**
 * @class AppIndexSlider
 * @memberof layout
 * @requires components#AppSlider
 * @classdesc Модуль верхнего слайдера на главной странице.
 * @desc Наследует: [Module]{@link app.Module}.
 * @example
 * const indexSliderInstance = new app.IndexSlider(document.querySelector('.index-slider-element'));
 */
const AppIndexSlider = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let slider;

        /**
         * @member {Object} AppIndexSlider#slider
         * @memberof layout
         * @desc Ссылка на модуль слайдера [AppSlider]{@link components.AppSlider} для слайдера на главной странице.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'slider', {
            enumerable: true,
            get: () => slider
        });

        const sliderEl = el.querySelector('.index-slider__slider');

        require('Components/slider').default.then(AppSlider => {
            const sliderOptions = {...defaultSliderOptions};
            sliderOptions.pluginOptions = {
                ...sliderOptions.pluginOptions,
                navigation: {
                    prevEl: sliderEl.querySelector('.index-slider-nav__arrow.-prev'),
                    nextEl: sliderEl.querySelector('.index-slider-nav__arrow.-next')
                },
                pagination: {
                    el: sliderEl.querySelector('.index-slider__pagination')
                }
            };
            slider = new AppSlider(sliderEl, sliderOptions);
        });
        emitInit('slider');
    }
});

addModule(moduleName, AppIndexSlider);

export default AppIndexSlider;
export {AppIndexSlider};