import './style.scss';

let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const indexCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "index" */ './sync.js'))
        .then(modules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppIndex = modules.default;
            resolve(AppIndex);
        });
};

const importFunc = new Promise(resolve => {
    indexCallback(resolve);
});

export default importFunc;