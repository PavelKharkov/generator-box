import {Module, immutable} from 'Components/module';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppPreloader from 'Components/preloader';

let AppIndexSlider;
let AppNewsSlider;
let AppSpecialSlider;

const moduleName = 'index';

//Опции
const indexEl = document.querySelector('html.page-index');

/**
 * @class AppIndex
 * @memberof layout
 * @classdesc Модуль главной страницы.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const indexInstance = new app.Index(document.querySelector('.index-element'));
 */
const AppIndex = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Верхний слайдер на главной странице
        let indexSlider;

        /**
         * @member {Array.<Object>} AppIndex#indexSlider
         * @memberof layout
         * @desc Коллекция ссылок на модули верхнего слайдера [AppIndexSlider]{@link layout.AppIndexSlider} главной страницы.
         * @readonly
         */
        Object.defineProperty(this, 'indexSlider', {
            enumerable: true,
            get: () => indexSlider
        });

        const indexSliderEl = el.querySelectorAll('.index-slider');
        if (indexSliderEl.length > 0) {
            AppIndexSlider = require('./index-slider').default;

            indexSlider = [...indexSliderEl].map(element => new AppIndexSlider(element));
        }

        //Слайдер новостей
        let newsSlider;

        /**
         * @member {Array.<Object>} AppIndex#newsSlider
         * @memberof layout
         * @desc Коллекция ссылок на модули блоков слайдера новостей [AppNewsSlider]{@link layout.AppNewsSlider} главной страницы.
         * @readonly
         */
        Object.defineProperty(this, 'newsSlider', {
            enumerable: true,
            get: () => newsSlider
        });

        const newsSliderEl = el.querySelectorAll('.card-slider.-news');
        if (newsSliderEl.length > 0) {
            const newsSliderPreloaders = [...newsSliderEl].map(item => {
                const newsSliderPreloader = new AppPreloader(item, {
                    overlay: false,
                    once: true
                });
                newsSliderPreloader.show();
                return newsSliderPreloader;
            });

            require('Layout/card-slider').default.then(AppCardSlider => {
                const newsSliderInit = require('./news-slider').default;
                AppNewsSlider = newsSliderInit(AppCardSlider);

                newsSlider = [...newsSliderEl].map((item, index) => {
                    newsSliderPreloaders[index].hide();
                    const newsSliderInstance = new AppNewsSlider(item);
                    return newsSliderInstance;
                });
            });
            emitInit('cardSlider');
        }

        //Слайдер акций
        let specialSlider;

        /**
         * @member {Array.<Object>} AppIndex#specialSlider
         * @memberof layout
         * @desc Коллекция ссылок на модули блоков слайдера акций [AppSpecialSlider]{@link layout.AppSpecialSlider} главной страницы.
         * @readonly
         */
        Object.defineProperty(this, 'specialSlider', {
            enumerable: true,
            get: () => specialSlider
        });

        const specialSliderEl = el.querySelectorAll('.card-slider.-special');
        if (specialSliderEl.length > 0) {
            const specialSliderPreloaders = [...specialSliderEl].map(item => {
                const specialSliderPreloader = new AppPreloader(item, {
                    overlay: false,
                    once: true
                });
                specialSliderPreloader.show();
                return specialSliderPreloader;
            });

            require('Layout/card-slider').default.then(AppCardSlider => {
                const specialSliderInit = require('./special-slider').default;
                AppSpecialSlider = specialSliderInit(AppCardSlider);

                specialSlider = [...specialSliderEl].map((item, index) => {
                    specialSliderPreloaders[index].hide();
                    const specialSliderInstance = new AppSpecialSlider(item);
                    return specialSliderInstance;
                });
            });
            emitInit('cardSlider');
        }

        //Блок каталога
        require('./index-catalog');

        //Блок "О компании"
        require('./index-about');
    }
});

addModule(moduleName, AppIndex);

new AppIndex(indexEl); /* eslint-disable-line no-new */

export default AppIndex;
export {
    AppIndex,
    AppIndexSlider,
    AppNewsSlider,
    AppSpecialSlider
};