import './style.scss';

import {immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'specialSlider';

//Опции
const specialSliderDefaults = {
    slider: {
        pluginOptions: {
            slidesPerView: 2.2,
            spaceBetween: 15,

            init: true,
            grabCursor: true,
            watchOverflow: true,
            lazy: true,

            breakpoints: {
                480: {
                    slidesPerView: 3
                },
                [util.media.sm]: {
                    slidesPerView: 4
                },
                [util.media.md]: {
                    slidesPerView: 5
                },
                [util.media.xl]: {
                    slidesPerView: 6
                }
            }
        }
    }
};

const specialSliderInit = AppCardSlider => {
    /**
     * @class AppSpecialSlider
     * @memberof layout
     * @requires layout#AppCardSlider
     * @classdesc Блок слайдера новостей.
     * @desc Наследует: [AppCardSlider]{@link app.AppCardSlider}.
     * @async
     * @param {HTMLElement} [element] - Элемент блока.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @example
     * const specialSliderInstance = new app.SpecialSlider(document.querySelector('.special-slider-element'), {
     *     slider: {
     *         pluginOptions: {slidesPerView: 1}
     *     }
     * });
     */
    const AppSpecialSlider = immutable(class extends AppCardSlider {
        constructor(el, opts, appname = moduleName) {
            const specialSliderConfig = opts || {};
            util.defaultsDeep(specialSliderConfig, specialSliderDefaults);

            super(el, specialSliderConfig, appname);
        }
    });

    addModule(moduleName, AppSpecialSlider);

    return AppSpecialSlider;
};

export default specialSliderInit;
export {specialSliderInit};