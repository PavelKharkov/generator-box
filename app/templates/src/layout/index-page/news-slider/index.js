import './style.scss';

import {immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'newsSlider';

//Опции
const newsSliderDefaults = {
    slider: {
        pluginOptions: {
            slidesPerView: 1,

            init: true,
            grabCursor: true,
            watchOverflow: true,
            lazy: true,

            breakpoints: {
                [util.media.sm]: {
                    slidesPerView: 2
                },
                [util.media.lg]: {
                    slidesPerView: 3
                }
            }
        }
    }
};

const newsSliderInit = AppCardSlider => {
    /**
     * @class AppNewsSlider
     * @memberof layout
     * @requires layout#AppCardSlider
     * @classdesc Блок слайдера новостей.
     * @desc Наследует: [AppCardSlider]{@link app.AppCardSlider}.
     * @async
     * @param {HTMLElement} [element] - Элемент блока.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @example
     * const newsSliderInstance = new app.NewsSlider(document.querySelector('.news-slider-element'), {
     *     slider: {
     *         pluginOptions: {slidesPerView: 1}
     *     }
     * });
     */
    const AppNewsSlider = immutable(class extends AppCardSlider {
        constructor(el, opts, appname = moduleName) {
            const newsSliderConfig = opts || {};
            util.defaultsDeep(newsSliderConfig, newsSliderDefaults);

            super(el, newsSliderConfig, appname);
        }
    });

    addModule(moduleName, AppNewsSlider);

    return AppNewsSlider;
};

export default newsSliderInit;
export {newsSliderInit};