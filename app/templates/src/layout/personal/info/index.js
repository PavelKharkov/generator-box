import './style.scss';

//import {Module, immutable} from 'Components/module';
//import {addModule} from 'Base/scripts/app.js';
//
//const moduleName = 'personalInfo';
//
///**
// * @class AppPersonalInfo
// * @requires components#AppValidation
// * @memberof layout
// * @classdesc Модуль страницы "Мои данные".
// * @desc Наследует: [Module]{@link app.Module}.
// * @async
// * @example
// * const personalInfoInstance = new app.PersonalInfo(document.querySelector('.personal-info-element'));
// *
// * @example <caption>Пример HTML-разметки</caption>
// * <!--HTML-->
// * <div class="personal__info"></div>
// *
// * <!--.personal__info - селектор по умолчанию (при наличии блока .personal)-->
// */
//const AppPersonalInfo = immutable(class extends Module {
//    constructor(el, opts, appname = moduleName) {
//        super(el, opts, appname);
//
//        Module.checkHTMLElement(el);
//
//        const formEl = el.querySelector('.personal-info__form .form');
//        if (!formEl) return;
//
//        let validation;
//
//        /**
//         * @member {Object} AppPersonalInfo#validation
//         * @memberof layout
//         * @desc Ссылка на модуль валидации формы [AppValidation]{@link components.AppValidation}.
//         * @async
//         * @readonly
//         */
//        Object.defineProperty(this, 'validation', {
//            enumerable: true,
//            get: () => validation
//        });
//
//        const validationRequire = require('Components/validation');
//        validationRequire.default.then(AppValidation => {
//            validation = new AppValidation(formEl);
//        });
//
//        validationRequire.validationTrigger(formEl);
//    }
//});
//
//addModule(moduleName, AppPersonalInfo);
//
//export default AppPersonalInfo;
//export {AppPersonalInfo};