import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//Опции
const moduleName = 'orders';

const popoverClass = '-orders-actions';
const pretensionPopoverClass = '-pretension';
const actionsButtonSelector = '.orders-table-actions__button';

/**
 * @class AppOrders
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppPopover
 * @classdesc Модуль таблицы заказов личного кабинета.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const ordersInstance = new app.Orders(document.querySelector('.orders-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="orders"></div>
 *
 * <!--.orders - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppOrders = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const filtersToggleButton = el.querySelector('.orders-top-toggle__button');

        /**
         * @member {HTMLElement} AppOrders#filtersToggleButton
         * @memberof layout
         * @desc Элемент кнопки переключения фильтра.
         * @readonly
         */
        Object.defineProperty(this, 'filtersToggleButton', {
            enumerable: true,
            value: filtersToggleButton
        });

        let filtersCollapse;

        /**
         * @member {Object} AppOrders#filtersCollapse
         * @memberof layout
         * @desc Ссылка на модуль переключения фильтра [AppCollapse]{@link components.AppCollapse}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'filtersCollapse', {
            enumerable: true,
            get: () => filtersCollapse
        });

        //Раскрывающийся блок фильтра
        if (filtersToggleButton) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                filtersCollapse = new AppCollapse(filtersToggleButton, {
                    initialHeight: 0
                });
            });

            collapseRequire.collapseTrigger(filtersToggleButton);
        }

        //Выпадающие блоки меню действий
        const actionsLinks = el.querySelectorAll(actionsButtonSelector);

        /**
         * @member {Array.<HTMLElement>} AppOrders#actionsLinks
         * @memberof layout
         * @desc Коллекция элементов кнопок открытия выпадающих блоков.
         * @readonly
         */
        Object.defineProperty(this, 'actionsLinks', {
            enumerable: true,
            value: actionsLinks
        });

        let popovers;

        /**
         * @member {Array.<Object>} AppOrders#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих блоков [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        let pretensionPopovers;

        /**
         * @member {Array.<Object>} AppOrders#pretensionPopovers
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих блоков претензий [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'pretensionPopovers', {
            enumerable: true,
            get: () => pretensionPopovers
        });

        if (actionsLinks.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...actionsLinks].map(element => {
                    const popoverContent = util.attempt(() => {
                        return element.closest('.orders-table__actions').querySelector('.orders-table-actions__popover');
                    });
                    if (!popoverContent || (popoverContent instanceof Error)) {
                        return null;
                    }

                    const popover = new AppPopover(element, {
                        class: popoverClass,
                        content: popoverContent,
                        placement: 'bottom'
                    });
                    return popover;
                });

                const pretensionIcons = el.querySelectorAll('.orders-table-pretension__icon');
                pretensionPopovers = [...pretensionIcons].map(element => {
                    const popoverWrapper = element.closest('.orders-table__pretension');
                    if (!popoverWrapper) return null;

                    const popoverTitle = popoverWrapper.querySelector('.orders-table-pretension-popover__title');
                    if (!popoverTitle) return null;

                    const popoverContent = popoverWrapper.querySelector('.orders-table-pretension-popover__content');
                    if (!popoverContent) return null;

                    const popover = new AppPopover(element, {
                        class: pretensionPopoverClass,
                        title: popoverTitle.textContent,
                        content: popoverContent.cloneNode(true),
                        placement: 'bottom',
                        trigger: false
                    });

                    popoverWrapper.addEventListener('mouseenter', () => {
                        if ((AppWindow.width() < util.media.lg) || popover.isShown) return;

                        popover.show();
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(actionsLinks);
        }

        //Инициализация элементов управления
        let controls;

        /**
         * @member {Array.<Object>} AppOrders#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули элементов управления [AppPersonalControls]{@link layout.AppPersonalControls}.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const workflowControlsEl = document.querySelectorAll('.top__controls.-orders');
        if (workflowControlsEl.length > 0) {
            const AppPersonalControls = require('../controls').default;

            controls = [...workflowControlsEl].map(element => new AppPersonalControls(element));
        }
    }
});

addModule(moduleName, AppOrders);

export default AppOrders;
export {AppOrders};