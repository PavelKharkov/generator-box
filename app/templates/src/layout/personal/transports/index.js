import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'transports';

/**
 * @class AppTransports
 * @memberof layout
 * @requires components#AppPopover
 * @classdesc Модуль страницы транспортировок.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const transportsInstance = new app.Transports(document.querySelector('.transports-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="transports"></div>
 *
 * <!--.transports - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppTransports = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const commentPopoverEl = el.querySelectorAll('.transports-item-comment__button');

        let popovers;

        /**
         * @member {Array.<Object>} AppTransportsNew#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модуль информера [AppPopover]{@link components.AppPopover} с комментарием к заказу.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        if (commentPopoverEl.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...commentPopoverEl].map(link => {
                    const popover = new AppPopover(link, {
                        class: 'transports-comment',
                        placement: 'top'
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(commentPopoverEl);
        }
    }
});

addModule(moduleName, AppTransports);

export default AppTransports;
export {AppTransports};