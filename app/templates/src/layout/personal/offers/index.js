import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'offers';

const popoverClass = '-offers-actions';
const actionsButtonSelector = '.offers-item-actions__button';

/**
 * @class AppOffers
 * @memberof layout
 * @requires components#AppPopover
 * @classdesc Модуль списка коммерческих предложений.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const offersInstance = new app.Offers(document.querySelector('.offers-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="offers"></div>
 *
 * <!--.offers - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppOffers = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Выпадающие блоки меню действий
        const actionsLinks = el.querySelectorAll(actionsButtonSelector);

        /**
         * @member {Array.<HTMLElement>} AppOffers#actionsLinks
         * @memberof layout
         * @desc Коллекция элементов кнопок открытия выпадающих блоков.
         * @readonly
         */
        Object.defineProperty(this, 'floatLabels', {
            enumerable: true,
            value: actionsLinks
        });

        let popovers;

        /**
         * @member {Array.<Object>} AppOffers#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих блоков [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        if (actionsLinks.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...actionsLinks].map(element => {
                    const popoverContent = util.attempt(() => {
                        return element.closest('.offers-item__actions').querySelector('.offers-item-actions__popover');
                    });
                    if (!popoverContent || (popoverContent instanceof Error)) {
                        return null;
                    }

                    const popover = new AppPopover(element, {
                        class: popoverClass,
                        content: popoverContent,
                        placement: 'bottom'
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(actionsLinks);
        }

        //Инициализация элементов управления
        let controls;

        /**
         * @member {Array.<Object>} AppOffers#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули элементов управления [AppPersonalControls]{@link layout.AppPersonalControls}.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const offersControlsEl = document.querySelectorAll('.top__controls.-offers');
        if (offersControlsEl.length > 0) {
            const AppPersonalControls = require('../controls').default;

            controls = [...offersControlsEl].map(element => new AppPersonalControls(element));
        }
    }
});

addModule(moduleName, AppOffers);

export default AppOffers;
export {AppOffers};