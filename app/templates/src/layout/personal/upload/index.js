import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'upload';

const blockClass = 'upload';
const blockSelector = `.${blockClass}`;

/**
 * @class AppUpload
 * @memberof layout
 * @requires components#AppCollapse
 * @requires components#AppSelect
 * @classdesc Модуль блока выгрузки.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const uploadInstance = new app.Upload(document.querySelector('.upload-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="upload"></div>
 *
 * <!--.upload - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppUpload = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Определение типа блока
        const fullwidth = el.classList.contains('-fullwidth');

        const select = el.querySelector(`${blockSelector}-copy__select`);
        const input = el.querySelector(`${blockSelector}-copy__input`);
        const copyMessage = el.querySelector(`${blockSelector}-copy__message`);

        //TODO:make util func
        /**
         * @function copyToBuffer
         * @desc Копирует выбранную ссылку в буфер обмена.
         * @returns {undefined}
         * @ignore
         */
        const copyToBuffer = () => {
            input.select();
            const isCopied = document.execCommand('copy');
            if (isCopied) {
                util.activate(copyMessage);
            }
        };

        let copySelect;

        /**
         * @member {Object} AppUpload#copySelect
         * @memberof layout
         * @desc Ссылка на модуль выпадающего списка формата выгрузки [AppSelect]{@link components.AppSelect}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'copySelect', {
            enumerable: true,
            get: () => copySelect
        });

        if (select) {
            const selectRequire = require('Components/select');
            selectRequire.default.then(AppSelect => {
                copySelect = new AppSelect(select);
                copySelect.on({
                    change(event) {
                        input.value = event.detail.value;
                        input.setAttribute('value', event.detail.value);
                        copyToBuffer();
                    },
                    showDropdown() {
                        util.deactivate(copyMessage);
                    }
                });
            });

            selectRequire.selectTrigger(el, ['mouseenter']);
        }

        if (fullwidth) {
            const linkIcon = el.querySelector('.input-group-text');
            if (linkIcon) {
                linkIcon.addEventListener('click', () => {
                    copyToBuffer();
                });
            }
        } else if (input) {
            input.addEventListener('focus', () => {
                copyToBuffer();
            });
        }

        let sectionsCollapse;

        /**
         * @member {Object} AppUpload#sectionsCollapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося контента [AppCollapse]{@link components.AppCollapse} списка разделов.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'sectionsCollapse', {
            enumerable: true,
            get: () => sectionsCollapse
        });

        //Древо разделов
        const sections = el.querySelector('.upload__sections');
        if (sections) {
            const sectionsCollapseButton = sections.querySelector('.upload-sections-toggle__button');
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                sectionsCollapse = new AppCollapse(sectionsCollapseButton);
            });

            collapseRequire.collapseTrigger(sectionsCollapseButton);
        }
    }
});

addModule(moduleName, AppUpload);

export default AppUpload;
export {AppUpload};