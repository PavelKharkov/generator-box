import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'clients';

/**
 * @class AppClients
 * @memberof layout
 * @classdesc Модуль страницы "Мои клиенты".
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const clientsInstance = new app.Clients(document.querySelector('.clients-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="clients"></div>
 *
 * <!--.clients - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppClients = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const collapseButton = el.querySelectorAll('.clients-table__button');
        const popoverLinksEl = el.querySelectorAll('.clients-table-progress__item');

        let popoverLinks;

        /**
         * @member {Array.<Object>} AppClients#popoverLinks
         * @memberof layout
         * @desc Коллекция ссылок на модули информеров [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popoverLinks', {
            enumerable: true,
            get: () => popoverLinks
        });

        if (popoverLinksEl.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popoverLinks = [...popoverLinksEl].map(link => {
                    const popover = new AppPopover(link, {
                        container: el.querySelector('.clients__table'),
                        tooltip: true
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(popoverLinksEl);
        }

        let collapses;

        /**
         * @member {Array.<Object>} AppClients#collapses
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрывающихся блоков [AppCollapse]{@link components.AppCollapse} таблицы.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapses', {
            enumerable: true,
            get: () => collapses
        });

        let collapsesInner;

        /**
         * @member {Array.<Object>} AppClients#collapsesInner
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрывающихся блоков [AppCollapse]{@link components.AppCollapse} внутренних элементов таблицы.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapsesInner', {
            enumerable: true,
            get: () => collapsesInner
        });

        if (collapseButton.length > 0) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                collapses = [...collapseButton].map(button => {
                    const collapse = new AppCollapse(button);

                    return collapse;
                });

                const collapseInnerButton = [...el.querySelectorAll('.clients-table-bottom-button__button')];

                collapsesInner = collapseInnerButton.map(button => {
                    const collapseInner = new AppCollapse(button);

                    return collapseInner;
                });
            });

            collapseRequire.collapseTrigger(collapseButton);
        }

        //Инициализация элементов управления
        let controls;

        /**
         * @member {Array.<Object>} AppClients#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули элементов управления [AppPersonalControls]{@link layout.AppPersonalControls}.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const clientsControlsEl = document.querySelectorAll('.top__controls.-clients');
        if (clientsControlsEl.length > 0) {
            const AppPersonalControls = require('../controls').default;

            controls = [...clientsControlsEl].map(element => new AppPersonalControls(element));
        }
    }
});

addModule(moduleName, AppClients);

export default AppClients;
export {AppClients};