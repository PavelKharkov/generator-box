import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'managersCards';

const popoverClass = '-managers-actions';
const actionsButtonSelector = '.managers-cards-actions__button';

/**
 * @class AppManagersCards
 * @memberof layout
 * @requires components#AppPopover
 * @classdesc Модуль списка менеджеров (в виде карточек).
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const managersCardsInstance = new app.ManagersCards(document.querySelector('.managers-cards-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="managers__cards"></div>
 *
 * <!--.managers__cards - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppManagersCards = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Выпадающие блоки действий
        const actionsLinks = el.querySelectorAll(actionsButtonSelector);

        /**
         * @member {Array.<HTMLElement>} AppManagersCards#actionsLinks
         * @memberof layout
         * @desc Коллекция элементов кнопок открытия выпадающих блоков.
         * @readonly
         */
        Object.defineProperty(this, 'actionsLinks', {
            enumerable: true,
            value: actionsLinks
        });

        let popovers;

        /**
         * @member {Array.<Object>} AppManagersCards#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих блоков [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        if (actionsLinks.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...actionsLinks].map(element => {
                    const popoverContent = util.attempt(() => {
                        return element.closest('.managers-cards__actions').querySelector('.managers-cards-actions__popover');
                    });
                    if (!popoverContent || (popoverContent instanceof Error)) {
                        return null;
                    }

                    const popover = new AppPopover(element, {
                        class: popoverClass,
                        content: popoverContent,
                        placement: 'bottom'
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(actionsLinks);
        }
    }
});

addModule(moduleName, AppManagersCards);

export default AppManagersCards;
export {AppManagersCards};