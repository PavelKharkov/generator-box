import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'transportsNew';

/**
 * @class AppTransportsNew
 * @memberof layout
 * @requires components#AppCollapse
 * @classdesc Модуль страницы новой транспортировки.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const transportsNewInstance = new app.TransportsNew(document.querySelector('.transports-new-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="transports-new"></div>
 *
 * <!--.transports-new - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppTransportsNew = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        document.body.classList.add('page-transports-new');

        const ordersCollapse = el.querySelector('.transports-new-orders-toggle__button');

        let collapse;

        /**
         * @member {Object} AppTransportsNew#collapse
         * @memberof layout
         * @desc Ссылка на модуль раскрывающегося блока [AppCollapse]{@link components.AppCollapse} добавления заказа.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapse', {
            enumerable: true,
            get: () => collapse
        });

        const collapseRequire = require('Components/collapse');
        collapseRequire.default.then(AppCollapse => {
            collapse = new AppCollapse(ordersCollapse);
        });

        collapseRequire.collapseTrigger(ordersCollapse);
    }
});

addModule(moduleName, AppTransportsNew);

export default AppTransportsNew;
export {AppTransportsNew};