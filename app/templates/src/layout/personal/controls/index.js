import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

const moduleName = 'personalControls';

/**
 * @class AppPersonalControls
 * @memberof layout
 * @requires components#AppDatepicker
 * @requires components#AppMask
 * @requires components#AppPopover
 * @requires components#AppSelect
 * @requires layout#AppWindow
 * @classdesc Модуль формы управления страницами личного кабинета.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const controlsInstance = new app.Controls(document.querySelector('.controls-element'));
 */
const AppPersonalControls = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const form = el.querySelector('.form');
        if (!form) return;

        /**
         * @member {HTMLElement} AppPersonalControls#form
         * @memberof layout
         * @desc Элемент формы элементов управления.
         * @readonly
         */
        Object.defineProperty(this, 'form', {
            enumerable: true,
            value: form
        });

        let selects;

        /**
         * @member {Array.<Object>} AppPersonalControls#selects
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих списков [AppSelect]{@link components.AppSelect}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'selects', {
            enumerable: true,
            get: () => selects
        });

        //Выпадающие списки
        const selectsEl = el.querySelectorAll('.top-controls__select');
        if (selectsEl.length > 0) {
            const selectRequire = require('Components/select');
            selectRequire.default.then(AppSelect => {
                selects = [...selectsEl].map(select => {
                    const selectInstance = new AppSelect(select, {
                        classNames: {
                            containerInner: 'form-control form-control-sm'
                        }
                    });
                    selectInstance.on('change', () => {
                        form.submit();
                    });
                    return selectInstance;
                });
            });

            selectRequire.selectTrigger(selectsEl);
        }

        let datepickerPopover;

        /**
         * @member {Object} AppPersonalControls#datepickerPopover
         * @memberof layout
         * @desc Ссылка на модуль выпадающего блока [AppPopover]{@link components.AppPopover} с выбором дат.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'datepickerPopover', {
            enumerable: true,
            get: () => datepickerPopover
        });

        let datepickerMasks;

        /**
         * @member {Array.<Object>} AppPersonalControls#datepickerMasks
         * @memberof layout
         * @desc Коллекция ссылок на модули масок полей ввода дат [AppMask]{@link components.AppMask}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'datepickerMasks', {
            enumerable: true,
            get: () => datepickerMasks
        });

        let startDate;

        /**
         * @member {Date} AppPersonalControls#startDate
         * @memberof layout
         * @desc Начальная выбранная дата в календаре.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'startDate', {
            enumerable: true,
            get: () => startDate
        });

        let endDate;

        /**
         * @member {Date} AppPersonalControls#endDate
         * @memberof layout
         * @desc Конечная выбранная дата в календаре.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'endDate', {
            enumerable: true,
            get: () => endDate
        });

        let datepicker;

        /**
         * @member {Object} AppPersonalControls#datepicker
         * @memberof layout
         * @desc Ссылка на модуль календаря [AppDatepicker]{@link components.AppDatepicker}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'datepicker', {
            enumerable: true,
            get: () => datepicker
        });

        //Выбор дат
        const detepickerGroup = el.querySelector('.top-controls__date');
        if (detepickerGroup) {
            const datepickerInner = el.querySelector('.top-controls-date__inputs-group');
            const currentMonthButton = el.querySelector('.top-controls-date__link.-current-month');
            const previousMonthButton = el.querySelector('.top-controls-date__link.-previous-month');

            const datepickerRequire = require('Components/datepicker');
            const popoverRequire = require('Components/popover');

            Promise.all([
                require('Components/date').default,
                datepickerRequire.default
            ]).then(modules => {
                const [AppDate, AppDatepicker] = modules;

                const popover = el.querySelector('.top-controls-date__popover');

                //TODO:change calendar data on input change

                popoverRequire.default.then(AppPopover => {
                    datepickerPopover = new AppPopover(datepickerInner, {
                        trigger: false,
                        container: detepickerGroup,
                        content: popover,
                        placement: 'bottom',
                        fallbackPlacement: []
                    });
                    datepickerInner.addEventListener('click', () => {
                        datepickerPopover.show();
                    });

                    if (typeof datepickerInner.dataset.popoverTriggered !== 'undefined') {
                        delete datepickerInner.dataset.popoverTriggered;
                        datepickerPopover.show();
                    }
                });

                //Блок выбора дат
                const datepickerInput = el.querySelectorAll('.top-controls-date__input-group .top-controls-date__input');
                const datepickerFrom = el.querySelector('.top-controls-date__input-group.-from .top-controls-date__input');
                const datepickerTo = el.querySelector('.top-controls-date__input-group.-to .top-controls-date__input');
                const datepickerHiddenFrom = el.querySelector('.top-controls-date__input-group.-from input[type="hidden"]');
                const datepickerHiddenTo = el.querySelector('.top-controls-date__input-group.-to input[type="hidden"]');
                const calendarsEl = el.querySelector('.top-controls-date-popover__calendars');
                const datepickerClear = el.querySelector('.top-controls-date-popover__clear');
                const datepickerSubmit = el.querySelector('.top-controls-date-popover__submit');

                const maskRequire = require('Components/mask');
                maskRequire.default.then(AppMask => {
                    datepickerMasks = [...datepickerInput].map(input => {
                        const mask = new AppMask(input, AppMask.dateOptions);
                        return mask;
                    });
                });

                maskRequire.maskTrigger(datepickerInput);

                let isStartDate = true;

                startDate = datepickerFrom.value ? AppDate.getDate(datepickerFrom.value) : null;

                endDate = datepickerTo.value ? AppDate.getDate(datepickerTo.value) : null;

                /**
                 * @function setStartDate
                 * @desc Устанавливает начальную дату диапазона в календаре.
                 * @returns {undefined}
                 * @ignore
                 */
                const setStartDate = () => {
                    const date = startDate;
                    if (datepickerHiddenFrom) {
                        const formattedDate = AppDate.formatDate(date);
                        datepickerHiddenFrom.value = formattedDate;
                        datepickerHiddenFrom.setAttribute('value', formattedDate);
                    }

                    const shownDate = AppDate.shownDate(date);
                    datepickerFrom.value = shownDate;
                    datepickerFrom.setAttribute('value', shownDate);
                };

                /**
                 * @function setStartDate
                 * @desc Устанавливает конечную дату диапазона в календаре.
                 * @returns {undefined}
                 * @ignore
                 */
                const setEndDate = () => {
                    const date = endDate;
                    if (datepickerHiddenTo) {
                        const formattedDate = AppDate.formatDate(date);
                        datepickerHiddenTo.value = formattedDate;
                        datepickerHiddenTo.setAttribute('value', formattedDate);
                    }

                    const shownDate = AppDate.shownDate(date);
                    datepickerTo.value = shownDate;
                    datepickerTo.setAttribute('value', shownDate);
                };

                let prevDate;

                datepicker = new AppDatepicker(calendarsEl, {
                    minDate: false,
                    numberOfMonths: (AppWindow.width() < util.media.sm) ? 1 : 2,
                    onSelect() {
                        const currentDate = datepicker.instance.getDate();
                        if (isStartDate) {
                            if (prevDate) {
                                endDate = prevDate;
                            }
                            startDate = currentDate;
                            prevDate = startDate;
                        } else {
                            if (prevDate) {
                                startDate = prevDate;
                            }
                            endDate = currentDate;
                            prevDate = endDate;
                        }

                        if (endDate && (startDate > endDate)) {
                            const tempStartDate = startDate;
                            startDate = endDate;
                            endDate = tempStartDate;
                        }

                        if (startDate) {
                            datepicker.instance.setStartRange(startDate);
                            setStartDate();
                        }

                        if (endDate) {
                            datepicker.instance.setEndRange(endDate);
                            setEndDate();
                        }

                        datepicker.instance.gotoDate(currentDate); //перерисовка месяцев
                        isStartDate = !isStartDate;
                    }
                });
                calendarsEl.append(datepicker.instance.el);
                datepicker.defaultRender.call(datepicker.instanceOptions());

                if (endDate) {
                    datepicker.instance.setEndRange(endDate);
                    datepicker.instance.gotoDate(endDate);
                }
                if (startDate) {
                    datepicker.instance.setStartRange(startDate); //TODO:make module method
                    datepicker.instance.gotoDate(startDate);
                }

                //TODO:make datepicker component reset method
                //TODO:docs
                const datepickerReset = () => {
                    prevDate = null;
                    startDate = null;
                    endDate = null;
                    datepicker.instance.setStartRange(null);
                    datepicker.instance.setEndRange(null);
                    datepicker.instance.setDate(null);
                };

                //Кнопка установки текущего месяца на календаре
                currentMonthButton.addEventListener('click', () => {
                    datepickerReset();

                    const newStartDate = new Date();
                    const newEndDate = new Date();

                    newStartDate.setDate(1);
                    newStartDate.setHours(0, 0, 0, 0);
                    startDate = newStartDate;

                    newEndDate.setMonth(startDate.getMonth() + 1);
                    newEndDate.setDate(0);
                    endDate = newEndDate;

                    isStartDate = true;

                    datepicker.instance.setDate(startDate);
                    datepicker.instance.setDate(endDate);

                    form.submit();
                });

                if (typeof currentMonthButton.dataset.datepickerTrigger !== 'undefined') {
                    delete currentMonthButton.dataset.datepickerTrigger;
                    currentMonthButton.click();
                }

                //Кнопка установки предыдущего месяца на календаре
                previousMonthButton.addEventListener('click', () => {
                    datepickerReset();

                    const newStartDate = new Date();
                    const newEndDate = new Date();

                    newStartDate.setMonth(newStartDate.getMonth() - 1);
                    newStartDate.setDate(1);
                    newStartDate.setHours(0, 0, 0, 0);
                    startDate = newStartDate;

                    newEndDate.setDate(0);
                    endDate = newEndDate;

                    isStartDate = true;

                    datepicker.instance.setDate(startDate);
                    datepicker.instance.setDate(endDate);

                    form.submit();
                });

                if (typeof previousMonthButton.dataset.datepickerTrigger !== 'undefined') {
                    delete previousMonthButton.dataset.datepickerTrigger;
                    previousMonthButton.click();
                }

                //Кнопка сброса выбранных дат в календаре
                datepickerClear.addEventListener('click', () => {
                    datepickerReset();

                    isStartDate = true;

                    datepickerFrom.value = '';
                    datepickerFrom.setAttribute('value', '');
                    datepickerTo.value = '';
                    datepickerTo.setAttribute('value', '');

                    if (datepickerHiddenFrom) {
                        datepickerHiddenFrom.value = '';
                        datepickerHiddenFrom.setAttribute('value', '');
                    }
                    if (datepickerHiddenTo) {
                        datepickerHiddenTo.value = '';
                        datepickerHiddenTo.setAttribute('value', '');
                    }

                    form.submit();
                });
                datepickerSubmit.addEventListener('click', () => {
                    datepickerPopover.hide();
                });
            });

            datepickerRequire.datepickerTrigger(detepickerGroup);
            const datepickerButtonsInit = event => {
                if (!((event.target === currentMonthButton) || (event.target === previousMonthButton))) return;

                event.target.dataset.datepickerTrigger = '';
            };
            detepickerGroup.addEventListener('click', datepickerButtonsInit, {once: true});

            popoverRequire.popoverTrigger(datepickerInner);
        }

        //Кнопка сброса
        //const resetButton = el.querySelector('.top-controls-reset__button');
        //if (resetButton) {
        //    form.addEventListener('reset', () => {
        //        util.defer(() => {
        //            form.submit();
        //        });
        //    });
        //}
    }
});

addModule(moduleName, AppPersonalControls);

export default AppPersonalControls;
export {AppPersonalControls};