import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

let AppPersonalSidebar;

//Опции
const moduleName = 'sidebarPersonal';

//Классы и селекторы
const slideoutClass = 'slideout';
const personalClass = 'personal';
const slideoutPersonalClass = `${slideoutClass}__${personalClass}`;
const slideoutPersonalInnerClass = `${slideoutClass}-${personalClass}__inner`;
const sidebarPersonalSelector = `.sidebar__${personalClass}`;

//Другие опции
const sidebarPlacementBp = util.media.xl;

const personalSidebarEl = document.querySelectorAll(sidebarPersonalSelector);

if (personalSidebarEl.length > 0) {
    /**
     * @class AppSidebarPersonal
     * @memberof layout
     * @requires components#AppCollapse
     * @requires components#AppPopover
     * @requires components#AppSlideout
     * @requires layout#AppWindow
     * @classdesc Модуль меню личного кабинета.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @example
     * const sidebarPersonalInstance = new app.SidebarPersonal(document.querySelector('.sidebar-personal-element'));
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="sidebar__personal"></div>
     *
     * <!--.sidebar__personal - селектор по умолчанию-->
     */
    AppPersonalSidebar = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const popoverLinksEl = el.querySelectorAll('.sidebar-personal-progress__item');
            const collapseLinksEl = el.querySelectorAll('.sidebar-personal-menu__link.-collapse');

            let popoverLinks;

            /**
             * @member {Array.<Object>} AppSidebarPersonal#popoverLinks
             * @memberof layout
             * @desc Коллекция ссылок на модули информеров [AppPopover]{@link components.AppPopover}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'popoverLinks', {
                enumerable: true,
                get: () => popoverLinks
            });

            if (popoverLinksEl.length > 0) {
                const popoverRequire = require('Components/popover');
                popoverRequire.default.then(AppPopover => {
                    const progressEl = el.querySelector('.sidebar-personal__progress');

                    popoverLinks = [...popoverLinksEl].map(link => {
                        const popover = new AppPopover(link, {
                            container: progressEl,
                            tooltip: true
                        });
                        return popover;
                    });
                });

                popoverRequire.popoverTrigger(popoverLinksEl);
            }

            let collapseLinks;

            /**
             * @member {Array.<Object>} AppSidebarPersonal#collapseLinks
             * @memberof layout
             * @desc Коллекция ссылок на модули раскрывающихся блоков [AppCollapse]{@link components.AppCollapse}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'collapseLinks', {
                enumerable: true,
                get: () => collapseLinks
            });

            if (collapseLinksEl.length > 0) {
                const collapseRequire = require('Components/collapse');
                collapseRequire.default.then(AppCollapse => {
                    collapseLinks = [...collapseLinksEl].map(link => {
                        return new AppCollapse(link);
                    });
                });

                collapseRequire.collapseTrigger(collapseLinksEl);
            }

            //Выезжающее меню ЛК
            let slideoutMarkupInitialized = false;

            /**
             * @member {boolean} AppSidebarPersonal#slideoutMarkupInitialized
             * @memberof layout
             * @desc Указывает, проинициализирован ли шаблон выезжающего меню личного кабинета.
             * @readonly
             */
            Object.defineProperty(this, 'slideoutMarkupInitialized', {
                enumerable: true,
                get: () => slideoutMarkupInitialized
            });

            let slideout;

            /**
             * @member {Object} AppSidebarPersonal#slideout
             * @memberof layout
             * @desc Ссылка на модуль выезжающего меню для меню ЛК [AppSlideout]{@link layout.AppSlideout}.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'slideout', {
                enumerable: true,
                get: () => slideout
            });

            let slideoutInner;

            /**
             * @member {HTMLElement} AppSidebarPersonal#slideoutInner
             * @memberof layout
             * @desc Элемент контента выезжающего меню, содержащего меню ЛК.
             * @readonly
             */
            Object.defineProperty(this, 'slideoutInner', {
                enumerable: true,
                get: () => slideoutInner
            });

            /**
             * @function slideoutMarkupInit
             * @desc Инициализирует шаблон выезжающего меню.
             * @returns {undefined}
             * @ignore
             */
            const slideoutMarkupInit = () => {
                const slideoutPersonalEl = document.createElement('div');
                slideoutPersonalEl.classList.add(slideoutPersonalClass);
                const markup = `
                <div class="${slideoutPersonalInnerClass}"></div>`;
                slideoutPersonalEl.innerHTML = markup;

                slideout.content.append(slideoutPersonalEl);
                slideoutInner = slideout.content.querySelector(`.${slideoutPersonalInnerClass}`);

                slideoutMarkupInitialized = true;
            };

            const sidebar = document.querySelector('.sidebar__inner');

            /**
             * @member {HTMLElement} AppSidebarPersonal#sidebar
             * @memberof layout
             * @desc Элемент боковой панели, содержащей меню ЛК.
             * @readonly
             */
            Object.defineProperty(this, 'sidebar', {
                enumerable: true,
                value: sidebar
            });

            /**
             * @function slideoutMarkupInit
             * @desc Инициализирует скрипты выезжающего меню.
             * @fires layout.AppWindow.resize
             * @returns {undefined}
             * @ignore
             */
            const slideoutInit = () => {
                if (!slideoutMarkupInitialized) slideoutMarkupInit();

                AppWindow.on('resize', () => {
                    if (AppWindow.width() < sidebarPlacementBp) {
                        const sidebarPersonal = sidebar.querySelector(sidebarPersonalSelector);
                        if (sidebarPersonal) slideoutInner.append(sidebarPersonal);
                        return;
                    }

                    const sidebarPersonal = slideoutInner.querySelector(sidebarPersonalSelector);
                    if (sidebarPersonal) sidebar.append(sidebarPersonal);
                });
                AppWindow.emit('resize');
            };

            require('Components/slideout').default.then(AppSlideout => {
                const {baseSlideout} = AppSlideout;

                slideout = baseSlideout;
                slideout.on('show', options => {
                    if ((options.type === 'personal') && !slideout.shownBefore.personal) {
                        slideoutInit();
                    }
                });
            });
            emitInit('slideout');
        }
    });

    addModule(moduleName, AppPersonalSidebar);

    //Инициализация элементов по классу
    personalSidebarEl.forEach(el => new AppPersonalSidebar(el));
}

export default AppPersonalSidebar;
export {AppPersonalSidebar};