import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'subscribeControls';

/**
 * @class AppSubscribeControls
 * @memberof layout
 * @requires components#AppPopup
 * @classdesc Модуль верхнего блока страницы подписок.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const subscribeControlsInstance = new app.SubscribeControls(document.querySelector('.subscribe-controls-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="subscribe"></div>
 *
 * <!--.subscribe - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppSubscribeControls = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const clearButton = el.querySelector('.top-controls__item.-unsubscribe .top-controls-item__link');

        /**
         * @member {HTMLElement} AppSubscribeControls#clearButton
         * @memberof layout
         * @desc Кнопка открытия попапа отмены подписок.
         * @readonly
         */
        Object.defineProperty(this, 'clearButton', {
            enumerable: true,
            value: clearButton
        });

        let clearPopup;

        /**
         * @member {Object} AppSubscribeControls#clearPopup
         * @memberof layout
         * @desc Ссылка на модуль попапа [AppPopup]{@link components.AppPopup} отмены подписок.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'clearPopup', {
            enumerable: true,
            get: () => clearPopup
        });

        if (clearButton) {
            const popupRequire = require('Components/popup');
            popupRequire.default.then(AppPopup => {
                clearPopup = new AppPopup(clearButton);
            });

            popupRequire.popupTrigger(clearButton);
        }
    }
});

addModule(moduleName, AppSubscribeControls);

export default AppSubscribeControls;
export {AppSubscribeControls};