import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'workflow';

/**
 * @class AppWorkflow
 * @memberof layout
 * @classdesc Модуль блока документооборота.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const workflowInstance = new app.Workflow(document.querySelector('.workflow-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="workflow"></div>
 *
 * <!--.workflow - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppWorkflow = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Выделение ряда или рядов таблицы
        const rowChecks = [...el.querySelectorAll('.workflow-table__cell.-check input')];
        const checkAll = el.querySelector('.workflow-table__header-cell.-check input');
        const downloadLink = el.querySelector('.workflow-download__link');

        /**
         * @function checkedTest
         * @desc Проверяет отмеченные ряды таблицы.
         * @returns {undefined}
         * @ignore
         */
        const checkedTest = () => {
            const checkedCount = rowChecks.reduce((accumulator, check) => {
                return accumulator + (check.checked ? 1 : 0);
            }, 0);
            util[(checkedCount > 0) ? 'enable' : 'disable'](downloadLink);
            checkAll.checked = (checkedCount === rowChecks.length);
        };

        checkAll.addEventListener('change', () => {
            rowChecks.forEach(check => {
                check.checked = checkAll.checked;
            });

            util[checkAll.checked ? 'enable' : 'disable'](downloadLink);
        });
        rowChecks.forEach(check => {
            check.addEventListener('change', checkedTest);
        });

        //Инициализация элементов управления
        let controls;

        /**
         * @member {Array.<Object>} AppWorkflow#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули элементов управления [AppPersonalControls]{@link layout.AppPersonalControls}.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const workflowControlsEl = document.querySelectorAll('.top__controls.-workflow');
        if (workflowControlsEl.length > 0) {
            const AppPersonalControls = require('../controls').default;

            controls = [...workflowControlsEl].map(element => new AppPersonalControls(element));
        }
    }
});

addModule(moduleName, AppWorkflow);

export default AppWorkflow;
export {AppWorkflow};