import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'legals';

/**
 * @class AppLegals
 * @memberof layout
 * @requires components#AppCollapse
 * @classdesc Модуль таблицы "Юридические лица".
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const legalsInstance = new app.Legals(document.querySelector('.legals-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="legals"></div>
 *
 * <!--.legals - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppLegals = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const collapseButton = el.querySelectorAll('.legals-table__button');

        let collapses;

        /**
         * @member {Array.<Object>} AppLegals#collapses
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрывающихся блоков [AppCollapse]{@link components.AppCollapse} таблицы.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapses', {
            enumerable: true,
            get: () => collapses
        });

        if (collapseButton.length > 0) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                collapses = [...collapseButton].map(button => {
                    const collapse = new AppCollapse(button);

                    return collapse;
                });
            });

            collapseRequire.collapseTrigger(collapseButton);
        }
    }
});

addModule(moduleName, AppLegals);

export default AppLegals;
export {AppLegals};