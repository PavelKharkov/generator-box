import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppTable from 'Components/table';
import AppWindow from 'Layout/window';

//Опции
const moduleName = 'kanban';

const popoverClass = '-kanban-actions';
const actionsButtonSelector = '.kanban-table-actions__button';

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

/**
 * @class AppKanban
 * @memberof layout
 * @requires components#AppTable
 * @requires components#AppTouch
 * @requires layout#AppWindow
 * @classdesc Модуль страницы канбана заказов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const kanbanInstance = new app.Kanban(document.querySelector('.kanban-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="kanban"></div>
 *
 * <!--.kanban - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppKanban = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const tableEl = el.querySelector('.kanban-table__table');
        const tableHeader = el.querySelector('.kanban-table__thead');
        const headerCells = el.querySelectorAll('.kanban-table__header-cell');
        const headerRow = tableHeader.querySelector('.kanban-table__row');
        const rowCells = el.querySelectorAll('.kanban-table__tbody .kanban-table__row .kanban-table__cell');

        const table = new AppTable(tableEl, {
            fixedBlock: true
        });

        /**
         * @member {Object} AppKanban#table
         * @memberof layout
         * @desc Ссылка на модуль основной таблицы [AppTable]{@link components.AppTable}.
         * @readonly
         */
        Object.defineProperty(this, 'table', {
            enumerable: true,
            value: table
        });

        /**
         * @function tableHeaderLoad
         * @desc Функция, вызываемая при инициализации заголовка таблицы.
         * @returns {undefined}
         * @ignore
         */
        const tableHeaderLoad = () => {
            setTimeout(() => {
                AppWindow.emit('scroll');
                AppWindow.emit('resize');
            }, transitionDuration);
        };

        const tableScroller = table.wrapper;
        AppWindow.on('resize', () => {
            headerCells.forEach((headerCell, cellIndex) => {
                headerCell.style.minWidth = `${rowCells[cellIndex].offsetWidth}px`;
            });
            tableHeader.style.width = `${tableScroller.offsetWidth}px`;
        });
        AppWindow.onload(tableHeaderLoad);

        tableScroller.addEventListener('scroll', () => {
            headerRow.style.left = `${-tableScroller.scrollLeft}px`;
        });

        let touch;

        /**
         * @member {Object} AppKanban#touch
         * @memberof layout
         * @desc Ссылка на модуль управления touch-событиями [AppTouch]{@link components.AppTouch} для прокрутки таблицы.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'touch', {
            enumerable: true,
            get: () => touch
        });

        require('Components/touch').default.then(AppTouch => {
            touch = new AppTouch(tableScroller);

            let startLeft;
            touch.on({
                touchstart() {
                    startLeft = tableScroller.scrollLeft;
                },
                touchmove() {
                    tableScroller.scrollLeft = startLeft - touch.moveCoords[0];
                }
            });
        });
        emitInit('touch');

        //Выпадающие блоки меню действий
        const actionsLinks = el.querySelectorAll(actionsButtonSelector);

        /**
         * @member {Array.<HTMLElement>} AppKanban#actionsLinks
         * @memberof layout
         * @desc Коллекция элементов кнопок открытия выпадающих блоков.
         * @readonly
         */
        Object.defineProperty(this, 'actionsLinks', {
            enumerable: true,
            value: actionsLinks
        });

        let popovers;

        /**
         * @member {Array.<Object>} AppKanban#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули информеров [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        if (actionsLinks.length > 0) {
            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...actionsLinks].map(element => {
                    const popoverContent = util.attempt(() => {
                        return element.closest('.kanban-table__actions').querySelector('.kanban-table-actions__popover');
                    });
                    if (!popoverContent || (popoverContent instanceof Error)) {
                        return null;
                    }

                    const popover = new AppPopover(element, {
                        class: popoverClass,
                        content: popoverContent,
                        placement: 'bottom'
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(actionsLinks);
        }

        //Инициализация элементов управления
        let controls;

        /**
         * @member {Array.<Object>} AppKanban#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули элементов управления [AppPersonalControls]{@link layout.AppPersonalControls}.
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        const kanbanControlsEl = document.querySelectorAll('.top__controls.-kanban');
        if (kanbanControlsEl.length > 0) {
            const AppPersonalControls = require('../controls').default;

            controls = [...kanbanControlsEl].map(element => new AppPersonalControls(element));
        }
    }
});

addModule(moduleName, AppKanban);

export default AppKanban;
export {AppKanban};