import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

import 'Components/float-label';
import AppTable from 'Components/table';
import AppWindow from 'Layout/window';

//Опции
const moduleName = 'offerNew';

const popoverClass = '-offer-new-actions';
const actionsButtonSelector = '.offer-new-top-actions__link';
const popoverBreakpoint = util.media.md;

/**
 * @class AppOfferNew
 * @memberof layout
 * @requires components#AppFLoatLabel
 * @requires components#AppCollapse
 * @requires components#AppFile
 * @requires components#AppMask
 * @requires components#AppPopover
 * @requires components#AppTable
 * @requires layout#AppWindow
 * @classdesc Модуль страницы нового коммерческого предложения.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const offerNewInstance = new app.OfferNew(document.querySelector('.offer-new-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="offer-new"></div>
 *
 * <!--.offer-new - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppOfferNew = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const tableContainer = el.querySelector('.offer-new__table');

        /**
         * @member {HTMLElement} AppOfferNew#tableContainer
         * @memberof layout
         * @desc Элемент обёртки таблицы.
         * @readonly
         */
        Object.defineProperty(this, 'tableContainer', {
            enumerable: true,
            value: tableContainer
        });

        const table = tableContainer.querySelector('.offer-new-table__table');

        /**
         * @member {HTMLElement} AppOfferNew#table
         * @memberof layout
         * @desc Элемент таблицы.
         * @readonly
         */
        Object.defineProperty(this, 'table', {
            enumerable: true,
            value: table
        });

        //Поле для загрузки файла
        let logoFile;

        /**
         * @member {Object} AppOfferNew#logoFile
         * @memberof layout
         * @desc Ссылка на модуль загрузки файла [AppFile]{@link components.AppFile}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'logoFile', {
            enumerable: true,
            get: () => logoFile
        });

        const fileEl = el.querySelector('.offer-new-top-image__file');
        if (fileEl) {
            const fileRequire = require('Components/file');
            fileRequire.default.then(AppFile => {
                logoFile = new AppFile(fileEl);
            });

            fileRequire.fileTrigger(fileEl);
        }

        let phoneMask;

        /**
         * @member {Object} AppOfferNew#phoneMask
         * @memberof layout
         * @desc Ссылка на модуль маски поля ввода телефона [AppMask]{@link components.AppMask}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'phoneMask', {
            enumerable: true,
            get: () => phoneMask
        });

        let counterMasks;

        /**
         * @member {Array.<Object>} AppOfferNew#counterMasks
         * @memberof layout
         * @desc Коллекция ссылок на модули маски полей ввода цен [AppMask]{@link components.AppMask}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'counterMasks', {
            enumerable: true,
            get: () => counterMasks
        });

        const topTelInput = el.querySelector('.offer-new-top__input[type="tel"]');
        const counterEl = tableContainer.querySelectorAll('input.offer-new-table-properties__input');

        const maskRequire = require('Components/mask');
        maskRequire.default.then(AppMask => {
            if (topTelInput) {
                phoneMask = new AppMask(topTelInput, AppMask.freePhoneOptions);
            }

            if (counterEl.length > 0) {
                counterMasks = [...counterEl].map(item => {
                    const counterMask = new AppMask(item, AppMask.priceOptions);
                    return counterMask;
                });
            }
        });

        maskRequire.maskTrigger(topTelInput);
        maskRequire.maskTrigger(counterEl);

        //Поле для ввода комментария в верхней таблице
        const topTextarea = el.querySelector('.offer-new-top__input.-textarea');
        if (topTextarea) {
            /**
             * @function recountTextareaHeight
             * @desc Изменяет высоту текстового поля в верхней форме.
             * @returns {undefined}
             * @ignore
             */
            const recountTextareaHeight = () => { //TODO:change rows
                topTextarea.parentNode.style.height = 'auto';
                topTextarea.style.height = '0';
                topTextarea.parentNode.style.height = `${topTextarea.scrollHeight + 2}px`;
                topTextarea.style.height = '100%';
            };
            topTextarea.addEventListener('input', recountTextareaHeight);
            AppWindow.onload(() => {
                recountTextareaHeight();
            });
        }

        //Инициализация плавающего заголовка таблицы
        const tableInstance = new AppTable(table, {
            responsive: false,
            fixedBlock: true
        });

        /**
         * @member {Object} AppOfferNew#tableInstance
         * @memberof layout
         * @desc Ссылка на модуль таблицы [AppTable]{@link components.AppTable}.
         * @readonly
         */
        Object.defineProperty(this, 'tableInstance', {
            enumerable: true,
            value: tableInstance
        });

        //Выпадающие блоки действий
        const actionsLinks = el.querySelectorAll(actionsButtonSelector);

        /**
         * @member {Array.<HTMLElement>} AppOfferNew#actionsLinks
         * @memberof layout
         * @desc Коллекция элементов кнопок открытия меню действий.
         * @readonly
         */
        Object.defineProperty(this, 'actionsLinks', {
            enumerable: true,
            value: actionsLinks
        });

        let actionsPopovers;

        /**
         * @member {Array.<Object>} AppOfferNew#actionsPopovers
         * @memberof layout
         * @desc Коллекция ссылок на модули выпадающих блоков [AppPopover]{@link components.AppPopover} меню действий.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'actionsPopovers', {
            enumerable: true,
            get: () => actionsPopovers
        });

        let popovers;

        /**
         * @member {Array.<Object>} AppOfferNew#popovers
         * @memberof layout
         * @desc Коллекция ссылок на модули информеров [AppPopover]{@link components.AppPopover}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'popovers', {
            enumerable: true,
            get: () => popovers
        });

        const popoverLinks = [...tableContainer.querySelectorAll('.offer-new-table__popover-link, .offer-new-table-properties__popover-link')];

        const popoverRequire = require('Components/popover');
        popoverRequire.default.then(AppPopover => {
            actionsPopovers = [...actionsLinks].map(element => {
                const popoverContent = util.attempt(() => {
                    return element.closest('.offer-new-top-actions__item').querySelector('.offer-new-top-actions__popover');
                });
                if (!popoverContent || (popoverContent instanceof Error)) {
                    return null;
                }

                const popover = new AppPopover(element, {
                    class: popoverClass,
                    content: popoverContent,
                    placement: 'bottom'
                });
                return popover;
            });

            //Информеры
            if (popoverLinks.length > 0) {
                popovers = popoverLinks.map(popoverLink => {
                    const content = popoverLink.dataset.id ? document.querySelector(`#${popoverLink.dataset.id}`) : null;
                    const popoverOptions = {
                        placement() {
                            return ((AppWindow.width() >= popoverBreakpoint) && !tableInstance.isHeaderFixed) ? 'right' : 'bottom';
                        },
                        trigger: util.isTouch ? 'click' : ['mouseenter', 'mouseleave'],
                        container: tableContainer
                    };
                    if (content) {
                        popoverOptions.content = content;
                    }
                    const popover = new AppPopover(popoverLink, popoverOptions);
                    return popover;
                });
            }
        });

        popoverRequire.popoverTrigger(actionsLinks);
        popoverRequire.popoverTrigger(popoverLinks);

        //Переключатель фильтров
        let collapse;

        /**
         * @member {Object} AppOfferNew#AppCollapse
         * @memberof layout
         * @desc Ссылка на модуль переключателя фильтров [AppCollapse]{@link components.AppCollapse}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapse', {
            enumerable: true,
            get: () => collapse
        });

        const filterToggleEl = tableContainer.querySelector('.offer-new-table-checkboxes-toggle__button');
        if (filterToggleEl) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                collapse = new AppCollapse(filterToggleEl, {
                    initialHeight: 0
                });
            });

            collapseRequire.collapseTrigger(filterToggleEl);
        }

        //Переключатели свойств в фильтре
        const checkboxes = tableContainer.querySelectorAll('.offer-new-table-checkboxes-form-check__input');
        if (checkboxes.length > 0) {
            /**
             * @function toggleProperty
             * @desc Переключает свойство элементов.
             * @param {HTMLElement} checkbox - Элемент переключателя.
             * @returns {undefined}
             * @ignore
             */
            const toggleProperty = checkbox => {
                const propertyId = checkbox.dataset.property;
                if (!propertyId) return;
                const propertyBlocks = table.querySelectorAll(`[data-property="${propertyId}"]`);

                if (propertyBlocks.length > 0) {
                    propertyBlocks.forEach(block => {
                        block[checkbox.checked ? 'removeAttribute' : 'setAttribute']('hidden', true);
                    });
                }
            };

            checkboxes.forEach(checkbox => {
                checkbox.addEventListener('change', () => {
                    toggleProperty(checkbox);
                });
                toggleProperty(checkbox);
            });
        }

        //Поле ввода комментария
        const commentEl = [...tableContainer.querySelectorAll('.offer-new-table-comment__form-group')];

        let floatLabels;

        /**
         * @member {Array.<Object>} AppOfferNew#floatLabels
         * @memberof layout
         * @desc Коллекция ссылок на модули анимированных заголовков полей [AppFloatLabel]{@link components.AppFloatLabel}.
         * @readonly
         */
        Object.defineProperty(this, 'floatLabels', {
            enumerable: true,
            get: () => floatLabels
        });

        if (commentEl.length > 0) {
            /**
             * @function recountTextareaHeight
             * @desc Изменеяет высоту текстового поля комментария.
             * @param {HTMLElement} input - Элемент текстового поля.
             * @returns {undefined}
             * @ignore
             */
            const recountCommentHeight = input => {
                input.parentNode.style.height = 'auto';
                input.style.height = '0';
                input.parentNode.style.height = `${input.scrollHeight + 2}px`;
                input.style.height = '100%';
            };

            floatLabels = commentEl.map(item => {
                const floatLabel = item.modules.floatLabel;
                const button = item.querySelector('.offer-new-table-comment__button');
                const input = item.querySelector('.offer-new-table-comment__input');

                if (button) {
                    button.addEventListener('click', () => {
                        input.value = '';
                        input.setAttribute('value', '');
                        input.textContent = '';
                        recountCommentHeight(input);
                        if (floatLabel) floatLabel.update();
                    });
                }

                if (input) {
                    input.addEventListener('input', () => recountCommentHeight(input));
                    AppWindow.onload(() => {
                        recountCommentHeight(input);
                    });
                }

                return floatLabel;
            });
        }
    }
});

addModule(moduleName, AppOfferNew);

export default AppOfferNew;
export {AppOfferNew};