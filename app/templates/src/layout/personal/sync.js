let AppClients;
let AppKanban;
let AppLegals;
let AppManagers;
let AppManagersCards;
let AppOfferDetail;
let AppOfferNew;
let AppOffers;
let AppOrders;
let AppPersonalControls;
let AppPersonalInfo;
let AppPersonalSidebar;
let AppSubscribeControls;
let AppTransportsNew;
let AppTransports;
let AppUpload;
let AppWorkflow;

//Опции
const personalEl = document.querySelector('.personal');
const personalSidebarEl = document.querySelector('.sidebar__personal');

if (personalSidebarEl) {
    if (__GLOBAL_SETTINGS__.manager) {
        require('Layout/avatar');
    }
    AppPersonalSidebar = require('./sidebar').default;
}

if (personalEl) {
    require('Layout/avatar');
    require('./sync.scss');

    //Страница "Каталоги"
    require('./catalogs');

    //Верхний блок личного кабинета
    const personalControlsEl = document.querySelectorAll('.top__controls.-personal');
    if (personalControlsEl.length > 0) {
        AppPersonalControls = require('./controls').default;
    }

    //Страница "Мои клиенты"
    const clientsEl = document.querySelectorAll('.clients');
    if (clientsEl.length > 0) {
        AppClients = require('./clients').default;

        clientsEl.forEach(el => new AppClients(el));
    }

    //Страница "Канбан заказов"
    const kanbanEl = document.querySelectorAll('.kanban');
    if (kanbanEl.length > 0) {
        AppKanban = require('./kanban').default;

        kanbanEl.forEach(el => new AppKanban(el));
    }

    //Страница "Юридические лица"
    const legalsEl = document.querySelectorAll('.legals');
    if (legalsEl.length > 0) {
        AppLegals = require('./legals').default;

        legalsEl.forEach(el => new AppLegals(el));
    }

    require('./legals-add');

    if (__GLOBAL_SETTINGS__.manager) {
        //Страница "Менеджеры"
        const managersEl = document.querySelectorAll('.managers__items');
        if (managersEl.length > 0) {
            AppManagers = require('./managers').default;

            managersEl.forEach(el => new AppManagers(el));
        }

        //Страница "Менеджеры" (В виде карточек)
        const managersCardsEl = document.querySelectorAll('.managers__cards');
        if (managersCardsEl.length > 0) {
            AppManagersCards = require('./managers-cards').default;

            managersCardsEl.forEach(el => new AppManagersCards(el));
        }
    }

    //Страница "Уведомления"
    require('./notifications');

    if (__GLOBAL_SETTINGS__.offers) {
        //Страница "Подробная КП"
        const offerDetailEl = document.querySelectorAll('.offer-detail');
        if (offerDetailEl.length > 0) {
            AppOfferDetail = require('./offer-detail').default;

            offerDetailEl.forEach(el => new AppOfferDetail(el));
        }

        //Страница "Новое КП"
        const offerNewEl = document.querySelectorAll('.offer-new');
        if (offerNewEl.length > 0) {
            AppOfferNew = require('./offer-new').default;

            offerNewEl.forEach(el => new AppOfferNew(el));
        }

        //Страница "Мои КП"
        const offersEl = document.querySelectorAll('.offers');
        if (offersEl.length > 0) {
            AppOffers = require('./offers').default;

            offersEl.forEach(el => new AppOffers(el));
        }
    }

    //Страница "Мои заказы"
    const ordersEl = document.querySelectorAll('.orders');
    if (ordersEl.length > 0) {
        AppOrders = require('./orders').default;

        ordersEl.forEach(el => new AppOrders(el));
    }

    //Страница "Страница заказа"
    require('./orders-detail');

    //Страница "Отмена заказа"
    require('./orders-cancel');

    //Страница "Мои данные"
    //const infoEl = document.querySelectorAll('.personal__info');
    //if (infoEl.length > 0) {
    //    AppPersonalInfo = require('./info').default;
    //
    //    infoEl.forEach(el => new AppPersonalInfo(el));
    //}
    require('./info');

    //Страница "Претензии"
    require('./pretensions');

    //Страница "Детальная претензия"
    require('./pretensions-detail');

    //Страница "Скидки"
    require('./sales');

    //Страница "Подписки"
    const subscribeEl = document.querySelectorAll('.subscribe');
    if (subscribeEl.length > 0) {
        AppSubscribeControls = require('./subscribe').default;

        //Элементы управления на странице подписок
        const subscribeControlsEl = document.querySelectorAll('.top__controls.-subscribe');
        if (subscribeControlsEl.length > 0) {
            subscribeControlsEl.forEach(el => new AppSubscribeControls(el));
        }
    }

    //Страница "Детальная транспортировки"
    require('./transports-detail');

    //Страница "Новая транспортировка"
    const transportsNewEl = document.querySelectorAll('.transports-new');
    if (transportsNewEl.length > 0) {
        AppTransportsNew = require('./transports-new').default;

        transportsNewEl.forEach(el => new AppTransportsNew(el));
    }

    //Страница "Транспортировки"
    const transportsEl = document.querySelectorAll('.transports');
    if (transportsEl.length > 0) {
        AppTransports = require('./transports').default;

        transportsEl.forEach(el => new AppTransports(el));
    }

    //Страница "Выгрузка"
    const uploadEl = document.querySelectorAll('.upload');
    if (uploadEl.length > 0) {
        AppUpload = require('./upload').default;

        uploadEl.forEach(el => new AppUpload(el));
    }

    //Страница "Документооборот"
    const workflowEl = document.querySelectorAll('.workflow');
    if (workflowEl.length > 0) {
        AppWorkflow = require('./workflow').default;

        workflowEl.forEach(el => new AppWorkflow(el));
    }
}

export {
    AppClients,
    AppKanban,
    AppLegals,
    AppManagers,
    AppManagersCards,
    AppOfferDetail,
    AppOfferNew,
    AppOffers,
    AppOrders,
    AppPersonalControls,
    AppPersonalInfo,
    AppPersonalSidebar,
    AppSubscribeControls,
    AppTransportsNew,
    AppTransports,
    AppUpload,
    AppWorkflow
};