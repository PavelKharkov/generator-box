import './style.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'offerDetail';

/**
 * @class AppOfferDetail
 * @memberof layout
 * @requires components#AppCollapse
 * @classdesc Модуль подробной страницы коммерческих предложений.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const offerDetailInstance = new app.OfferDetail(document.querySelector('.offer-detail-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="offer-detail"></div>
 *
 * <!--.offer-detail - селектор по умолчанию (при наличии блока .personal)-->
 */
const AppOfferDetail = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        let collapses;

        /**
         * @member {Array.<Object>} AppOfferDetail#collapses
         * @memberof layout
         * @desc Коллекция ссылок на модули раскрывающихся блоков [AppCollapse]{@link components.AppCollapse}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapses', {
            enumerable: true,
            get: () => collapses
        });

        const productsEl = el.querySelectorAll('.offer-detail-table__row');
        if (productsEl.length > 0) {
            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                collapses = [];
                productsEl.forEach(productEl => {
                    const toggleEl = productEl.querySelector('.offer-detail-table-text-toggle__button');
                    if (!toggleEl) return;

                    const toggle = new AppCollapse(toggleEl);
                    collapses.push(toggle);

                    const innerToggleEl = productEl.querySelector('.offer-detail-table-properties-toggle__button');
                    if (!innerToggleEl) return;

                    innerToggleEl.addEventListener('click', event => {
                        event.preventDefault();
                        toggle.toggle();
                    });
                });
            });

            collapseRequire.collapseTrigger(productsEl);
        }

        if (!util.isTouch) {
            const dataHover = 'data-hover';
            const hoverClass = 'hover';

            /**
             * @function addHoverClass
             * @desc Добавляет класс при наведении на ячейку таблицы.
             * @param {string} hoverId - Идентификатор ячеек в ряду таблицы.
             * @returns {undefined}
             * @ignore
             */
            const addHoverClass = hoverId => {
                util.defer(() => {
                    el.querySelectorAll(`[${dataHover}=${hoverId}]`).forEach(hoverCell => {
                        hoverCell.classList.add(hoverClass);
                    });
                });
            };

            const hoverCells = el.querySelectorAll(`[${dataHover}]`);
            hoverCells.forEach(cell => {
                cell.addEventListener('mouseenter', () => {
                    const hoverId = cell.dataset.hover;
                    if (hoverId) addHoverClass(hoverId);
                });
                cell.addEventListener('mouseleave', () => {
                    el.querySelectorAll(`[${dataHover}]`).forEach(hoverCell => {
                        hoverCell.classList.remove(hoverClass);
                    });
                });
            });
        }
    }
});

addModule(moduleName, AppOfferDetail);

export default AppOfferDetail;
export {AppOfferDetail};