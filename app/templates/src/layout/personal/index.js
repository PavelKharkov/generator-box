import './style.scss';

let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const personalCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "personal" */ './sync.js'))
        .then(modules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const {
                AppClients,
                AppKanban,
                AppLegals,
                AppManagers,
                AppManagersCards,
                AppOfferDetail,
                AppOfferNew,
                AppOffers,
                AppOrders,
                AppPersonalControls,
                AppPersonalInfo,
                AppPersonalSidebar,
                AppSubscribeControls,
                AppTransportsNew,
                AppTransports,
                AppUpload,
                AppWorkflow
            } = modules;
            resolve({
                AppClients,
                AppKanban,
                AppLegals,
                AppManagers,
                AppManagersCards,
                AppOfferDetail,
                AppOfferNew,
                AppOffers,
                AppOrders,
                AppPersonalControls,
                AppPersonalInfo,
                AppPersonalSidebar,
                AppSubscribeControls,
                AppTransportsNew,
                AppTransports,
                AppUpload,
                AppWorkflow
            });
        });
};

const importFunc = new Promise(resolve => {
    personalCallback(resolve);
});

export default importFunc;