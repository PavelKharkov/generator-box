import util from 'Layout/main';

const bitrix = window.BX;
const frameCacheVars = window.frameCacheVars;

let hasComposite;

/**
 * @function app.util#compositeInit
 * @desc Вызывает функцию после инициализации композита в Битриксе.
 * @param {Function} [callback] - Функция обратного вызова.
 * @returns {undefined}
 * @readonly
 * @example
 * app.util.compositeInit(() => {
 *     console.log('Композит загружен');
 * });
 */
util.compositeInit = callback => {
    if (typeof callback !== 'function') return;
    if (!bitrix) {
        callback();
        return;
    }

    if (typeof hasComposite !== 'undefined') {
        callback(hasComposite);
        return;
    }

    if (typeof frameCacheVars === 'undefined') {
        bitrix.ready(() => {
            hasComposite = false;
            callback(hasComposite);
        });
        return;
    }

    bitrix.addCustomEvent('onFrameDataReceived', () => {
        hasComposite = true;
        callback(hasComposite);
    });
};