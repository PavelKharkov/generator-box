import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const bitrixCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "helpers" */ './sync.js'))
        .then(() => {
            resolve();
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.helpers) {
        bitrixCallback(resolve);
        return;
    }

    onInit('bitrix', () => {
        bitrixCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('bitrix');
    });
});

export default importFunc;

if (__IS_DISABLED_MODULE__) {
    require('./composite.js');
}