import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'document';

const el = document;

const keys = {};

/**
 * @function AppDocument
 * @namespace layout.AppDocument
 * @instance
 * @desc Экземпляр [Module]{@link app.Module}. Модуль для работы с document.
 */
const AppDocument = immutable(new Module(

    /**
     * @desc Функция-обёртка.
     * @this AppDocument
     * @returns {undefined}
     */
    function() {
        /**
         * @member {Object} AppDocument#keys
         * @memberof layout
         * @desc Хранилище идентификаторов нажатых клавиш.
         * @readonly
         */
        Object.defineProperty(this, 'keys', {
            enumerable: true,
            value: keys
        });

        el.addEventListener('keydown', event => { //Сохраняет клавишу при нажатии
            keys[event.key] = true;
        });
        el.addEventListener('keyup', event => { //Удаляет из хранилища клавишу, когда её отпускают
            keys[event.key] = false;
        });

        //Добавляет outline-стили только при фокусировке через tab
        el.addEventListener('mousedown', event => {
            const button = event.target.closest('button');
            if (!button) return;

            button.style.outline = 'none';

            //Функция для удаления события нажатия на кнопку
            const blurFunction = () => {
                button.style.outline = '';
            };
            button.addEventListener('blur', blurFunction, {once: true});
        });
    },

    moduleName
));

addModule(moduleName, AppDocument);

export default AppDocument;
export {AppDocument};