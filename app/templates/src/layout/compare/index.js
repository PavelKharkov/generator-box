import {emitInit} from 'Base/scripts/app.js';

let importFunc;

const compareEl = document.querySelector('.compare');

if (compareEl) {
    let AppPreloader;
    if (!__IS_SYNC__) {
        AppPreloader = require('Components/preloader').default;
        AppPreloader.initContentPreloader();
    }

    const compareCallback = resolve => {
        const imports = [
            require('Components/popup').default,
            require('Components/slider').default
        ];

        Promise.all(imports).then(modules => {
            (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "compare" */ './sync.js'))
                .then(compare => {
                    if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

                    const AppCompare = compare.compareInit(...modules);
                    resolve(AppCompare);
                });
        });

        emitInit('popup', 'slider');
    };

    importFunc = new Promise(resolve => {
        compareCallback(resolve);
    });
}

export default importFunc;