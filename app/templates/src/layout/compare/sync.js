import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

import AppWindow from 'Layout/window';

//TODO:fix mobile slider if 1 element

//Опции
const moduleName = 'compare';

//Классы
const collapseInactiveClass = 'inactive';

const compareEl = document.querySelectorAll('.compare');

const compareInit = (AppPopup, AppSlider) => {
    /**
     * @class AppCompare
     * @memberof layout
     * @requires components#AppSlider
     * @requires layout#AppWindow
     * @classdesc Модуль страницы сравнения.
     * @desc Наследует: [Module]{@link app.Module}.
     * @async
     * @example
     * const compareInstance = new app.Compare(document.querySelector('.compare-element'));
     *
     * @example <caption>Пример HTML-разметки</caption>
     * <!--HTML-->
     * <div class="compare"></div>
     *
     * <!--.compare - селектор по умолчанию-->
     */
    const AppCompare = immutable(class extends Module {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);

            Module.checkHTMLElement(el);

            const editPopupEl = el.querySelector('.compare-top__link.-edit');

            let editPopup;

            /**
             * @member {Object} AppCompare#editPopup
             * @memberof layout
             * @desc Ссылка на модуль попапа [AppPopup]{@link components.AppPopup} редактирования товаров в сравнении.
             * @readonly
             */
            Object.defineProperty(this, 'editPopup', {
                enumerable: true,
                get: () => editPopup
            });

            if (editPopupEl) {
                editPopup = new AppPopup(el.querySelector('.compare-top__link.-edit'));
            }

            const sliderElements = el.querySelectorAll('.compare-slider__slider');
            if (sliderElements.length !== 2) return;
            let sliderItem = el.querySelectorAll('.compare-slider__item');
            if (sliderItem.length < 2) return;

            const compareInfo = el.querySelector('.compare__info');
            const infoSections = compareInfo.querySelector('.compare-info__sections');
            const infoSection = compareInfo.querySelectorAll('.compare-info__section');
            const sliderPagination = compareInfo.querySelectorAll('.compare-info-pages__item');

            /**
             * @function recountPages
             * @desc Меняет текст в блоке пагинации слайдеров.
             * @param {number} index - Индекс текущего активного слайдера.
             * @returns {undefined}
             * @ignore
             */
            const recountPages = index => {
                const activeItem = sliderElements[index].querySelector('.compare-slider__item.swiper-slide-active');
                if (!activeItem) return;

                sliderPagination[index].querySelector('.compare-info-pages__title').textContent =
                    activeItem.querySelector('.compare-slider__link').textContent;
                sliderPagination[index].querySelector('.compare-info-pages__current').textContent = activeItem.dataset.id;
                sliderPagination[index].querySelector('.compare-info-pages__all').textContent =
                    sliderElements[index].querySelectorAll('.compare-slider__item:not(.swiper-slide-duplicate)').length;
            };

            let toggles;

            /**
             * @function updateToggles
             * @desc Обновляет модули раскрытия блоков внутри слайдеров.
             * @returns {undefined}
             * @ignore
             */
            const updateToggles = () => {
                if (!toggles) return;

                toggles.forEach(collapse => {
                    const target = collapse.target;
                    collapse.targetElements.length = 0;
                    collapse.targetElements.push(...el.querySelectorAll(`[id="${target}"], [data-collapse-id="${target}"]`));
                });
            };

            const headerSliderEl = el.querySelector('.compare-header__slider');
            const headerSliderItems = headerSliderEl.querySelectorAll('.compare-header__inner');

            sliderElements[0].querySelectorAll('.compare-slider__item').forEach((item, itemIndex) => {
                const itemContent = item.querySelector('.compare-slider__top').cloneNode(true);
                //TODO:sync button states

                headerSliderItems[itemIndex].append(itemContent);
            });

            let headerSlider; /* eslint-disable-line prefer-const */
            const sliders = [...sliderElements].map((item, index) => {
                const sliderConfig = {
                    pluginOptions: {
                        slidesPerView: 1,

                        init: false,
                        loop: true,

                        breakpoints: {
                            [util.media.lg]: {
                                loop: false,
                                slidesPerView: 'auto'
                            }
                        }
                    }
                };

                if (index === 0) {
                    sliderConfig.pluginOptions.navigation = {
                        prevEl: item.querySelector('.compare-slider-nav__arrow.-prev'),
                        nextEl: item.querySelector('.compare-slider-nav__arrow.-next')
                    };
                }

                const slider = new AppSlider(item, sliderConfig);
                slider.on({
                    initialized() {
                        util.defer(() => {
                            recountPages(index);
                            updateToggles();
                        });
                    },
                    slideChange() {
                        util.defer(() => {
                            recountPages(index);
                            updateToggles();

                            if (index !== 0) return;

                            headerSlider.goto({index: slider.instance.activeIndex});
                        });
                    }
                });
                slider.init();
                return slider;
            });

            /**
             * @member {Array.<Object>} AppCompare#sliders
             * @memberof layout
             * @desc Коллекция ссылок на экземпляры основных слайдеров [AppSlider]{@link components.AppSlider} страницы сравнения.
             * @readonly
             */
            Object.defineProperty(this, 'sliders', {
                enumerable: true,
                value: sliders
            });

            const headerSliderConfig = {
                pluginOptions: {
                    slidesPerView: 1,

                    init: false,
                    loop: true,

                    breakpoints: {
                        [util.media.lg]: {
                            loop: false,
                            slidesPerView: 'auto'
                        }
                    },

                    navigation: {
                        prevEl: headerSliderEl.querySelector('.compare-header-nav__arrow.-prev'),
                        nextEl: headerSliderEl.querySelector('.compare-header-nav__arrow.-next')
                    }
                }
            };

            headerSlider = new AppSlider(headerSliderEl, headerSliderConfig);
            headerSlider.on({
                slideChange() {
                    sliders[0].goto({index: headerSlider.instance.activeIndex});
                }
            });
            headerSlider.init();

            /**
             * @member {Object} AppCompare#headerSlider
             * @memberof layout
             * @desc Ссылка на экземпляр слайдера [AppSlider]{@link components.AppSlider} плавающего заголовка страницы сравнения.
             * @readonly
             */
            Object.defineProperty(this, 'headerSlider', {
                enumerable: true,
                value: headerSlider
            });

            if (!util.isTouch) {
                /**
                 * @function propertyEnter
                 * @desc Функция, вызываемая при наведении на свойство для сравнения.
                 * @param {number} propertyIndex - Индекс текущего активного свойства внутри элемента сравнения.
                 * @returns {undefined}
                 * @ignore
                 */
                const propertyEnter = propertyIndex => {
                    util.defer(() => {
                        sliderItem.forEach(hoverItem => {
                            hoverItem.querySelectorAll('.compare-slider__property')[propertyIndex].classList.add('hover');
                        });
                    });
                };

                /**
                 * @function propertyLeave
                 * @desc Функция, вызываемая при прекращении наведения на свойство для сравнения.
                 * @param {number} propertyIndex - Индекс текущего активного свойства внутри элемента сравнения.
                 * @returns {undefined}
                 * @ignore
                 */
                const propertyLeave = propertyIndex => {
                    sliderItem.forEach(hoverItem => {
                        hoverItem.querySelectorAll('.compare-slider__property')[propertyIndex].classList.remove('hover');
                    });
                };

                sliderItem.forEach(item => {
                    let even = false;
                    const properties = item.querySelectorAll('.compare-slider__property');
                    properties.forEach((property, propertyIndex) => {
                        if (even) property.classList.add('-even');
                        even = !even;

                        property.addEventListener('mouseenter', () => {
                            propertyEnter(propertyIndex);
                        });

                        property.addEventListener('mouseleave', () => {
                            propertyLeave(propertyIndex);
                        });
                    });
                });

                infoSections.querySelectorAll('.compare-info-section__property').forEach((property, propertyIndex) => {
                    property.addEventListener('mouseenter', () => {
                        propertyEnter(propertyIndex);
                    });

                    property.addEventListener('mouseleave', () => {
                        propertyLeave(propertyIndex);
                    });
                });
            }

            /**
             * @function recountHeights
             * @desc Пересчитывает высоты элементов внутри слайдеров сравнения.
             * @returns {undefined}
             * @ignore
             */
            const recountHeights = () => {
                sliderItem = el.querySelectorAll('.compare-slider__item');
                if (sliderItem.length === 0) return;

                //Определение высоты элементов
                let topMaxHeight = 0;
                let bottomMaxHeight = 0;
                const propsHeights = [];
                sliderItem.forEach(item => {
                    const topBlock = item.querySelector('.compare-slider__top');
                    topBlock.style.height = 'auto';
                    const topBlockHeight = topBlock.offsetHeight;
                    if (topBlockHeight > topMaxHeight) {
                        topMaxHeight = topBlockHeight;
                    }

                    const bottomBlock = item.querySelector('.compare-slider__bottom');
                    bottomBlock.style.height = 'auto';
                    const bottomBlockHeight = bottomBlock.offsetHeight;
                    if (bottomBlockHeight > bottomMaxHeight) {
                        bottomMaxHeight = bottomBlockHeight;
                    }

                    const sectionBlock = item.querySelectorAll('.compare-slider__section');
                    let itemNum = 0;
                    sectionBlock.forEach((block, index) => {
                        const sectionButton = infoSection[index].querySelector('.compare-info-section__button');
                        const sectionButtonHeight = sectionButton.offsetHeight;

                        if ((sectionButtonHeight > propsHeights[itemNum]) || !propsHeights[itemNum]) {
                            propsHeights[itemNum] = sectionButtonHeight;
                        }
                        itemNum += 1;

                        const sectionProps = infoSection[index].querySelectorAll('.compare-info-section__property');
                        sectionProps.forEach((prop, propIndex) => {
                            const infoPropHeight = prop.offsetHeight;
                            if ((infoPropHeight > propsHeights[itemNum]) || !propsHeights[itemNum]) {
                                propsHeights[itemNum] = infoPropHeight;
                            }
                            itemNum += 1;

                            const propText = block.querySelectorAll('.compare-slider__property')[propIndex].querySelector('.compare-slider__text');
                            propText.style.height = 'auto';
                            const itemPropHeight = propText.offsetHeight;
                            if ((itemPropHeight > propsHeights[itemNum]) || !propsHeights[itemNum]) {
                                propsHeights[itemNum] = itemPropHeight;
                            }
                            itemNum += 1;
                        });
                    });
                });

                //Применение высоты элементов
                const topBlocks = el.querySelectorAll('.compare-slider__top');
                topBlocks.forEach(block => {
                    block.style.height = `${topMaxHeight}px`;
                });

                const bottomBlocks = el.querySelectorAll('.compare-slider__bottom');
                bottomBlocks.forEach(block => {
                    block.style.height = `${bottomMaxHeight}px`;
                });

                compareInfo.style.top = `${topMaxHeight}px`;

                infoSections.style.paddingTop = `${bottomMaxHeight + 1}px`;

                sliderItem.forEach(item => {
                    const sectionBlock = item.querySelectorAll('.compare-slider__section');
                    let itemNum = 0;
                    sectionBlock.forEach((block, sectionIndex) => {
                        block.style.paddingTop = `${propsHeights[itemNum]}px`;
                        itemNum += 1;

                        const sectionProps = block.querySelectorAll('.compare-slider__property');
                        sectionProps.forEach((prop, propIndex) => {
                            prop.style.paddingTop = `${propsHeights[itemNum]}px`;
                            itemNum += 1;
                            prop.querySelector('.compare-slider__text').style.height = `${propsHeights[itemNum]}px`;

                            const infoProperty = infoSection[sectionIndex].querySelectorAll('.compare-info-section__property')[propIndex + 1];
                            if (infoProperty) {
                                infoProperty.style.marginTop = `${propsHeights[itemNum] + 1}px`;
                            } else {
                                const nextSection = infoSection[sectionIndex + 1];
                                if (nextSection) {
                                    nextSection.style.paddingTop = `${propsHeights[itemNum] + 1}px`;
                                }
                            }
                            itemNum += 1;
                        });
                    });
                });
            };

            recountHeights();
            AppWindow.on('resize', () => {
                recountHeights();
            });

            /**
             * @member {Array.<Object>} AppCompare#toggles
             * @memberof layout
             * @desc Коллекция ссылок на экземпляры раскрывающихся блоков [AppCompare]{@link components.AppCompare} свойств товаров.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'toggles', {
                enumerable: true,
                get: () => toggles
            });

            const collapseButtons = infoSections.querySelectorAll('.compare-info-section__button');

            const collapseRequire = require('Components/collapse');
            collapseRequire.default.then(AppCollapse => {
                toggles = [...collapseButtons].map(item => {
                    const collapse = new AppCollapse(item);
                    const collapseEl = collapse.el;
                    collapse.on({
                        hide() {
                            collapseEl.parentNode.classList.add(collapseInactiveClass);
                        },
                        shown() {
                            collapseEl.parentNode.classList.remove(collapseInactiveClass);
                        }
                    });

                    if (collapseEl.classList.contains(collapseInactiveClass)) {
                        collapseEl.parentNode.classList.add(collapseInactiveClass);
                    }

                    return collapse;
                });
            });

            collapseRequire.collapseTrigger(collapseButtons);

            let popovers;

            /**
             * @member {Array.<Object>} AppCompare#popovers
             * @memberof layout
             * @desc Коллекция ссылок на экземпляры информеров [AppPopover]{@link components.AppPopover} заголовков свойств товаров.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'popovers', {
                enumerable: true,
                get: () => popovers
            });

            const popoverLinks = infoSections.querySelectorAll('.compare-info-section__popover-link');

            const popoverRequire = require('Components/popover');
            popoverRequire.default.then(AppPopover => {
                popovers = [...popoverLinks].map(item => {
                    const popover = new AppPopover(item, {
                        placement() {
                            return (AppWindow.width() < util.media.sm) ? 'bottom' : 'right';
                        }
                    });
                    return popover;
                });
            });

            popoverRequire.popoverTrigger(popoverLinks);

            //Плавающие заголовки слайдеров
            let isTopFixed = false;

            /**
             * @member {boolean} AppCompare#isTopFixed
             * @memberof layout
             * @desc Указывает, является ли верхняя часть слайдера плавающей в данный момент.
             * @readonly
             */
            Object.defineProperty(this, 'isTopFixed', {
                enumerable: true,
                get: () => isTopFixed
            });

            const fixedHeader = el.querySelector('.compare__header');
            const sliderWrapper = el.querySelector('.compare__sliders');
            const sliderFixedHeader = el.querySelector('.compare-info__pages');

            /**
             * @function updateHeaderWidth
             * @desc Пересчитывает ширины заголовка слайдеров сравнения.
             * @returns {undefined}
             * @ignore
             */
            const updateHeaderWidth = () => {
                fixedHeader.style.width = `${document.querySelector('.main-content__inner').offsetWidth}px`;
            };

            const fixedClass = 'position-fixed';
            const fixedBottomClass = 'fixed-bottom';
            const wrapperBottomOffset = 120;

            AppWindow.on({
                load() {
                    updateHeaderWidth();
                },
                scroll() {
                    if (AppWindow.width() >= util.media.lg) {
                        const sliderWrapperOffset = sliderWrapper.getBoundingClientRect().top;
                        const sliderWrapperBottom = sliderWrapperOffset + sliderWrapper.offsetHeight - fixedHeader.offsetHeight;

                        if ((sliderWrapperOffset < 0) && (sliderWrapperBottom > 0)) {
                            isTopFixed = true;
                            fixedHeader.classList.remove(fixedBottomClass);
                            fixedHeader.classList.add(fixedClass);
                        } else {
                            isTopFixed = false;
                            fixedHeader.classList.remove(fixedClass);
                            if (sliderWrapperBottom <= 0) {
                                fixedHeader.classList.add(fixedBottomClass);
                            }
                        }
                        return;
                    }

                    const sliderHeaderFixedTop = el.querySelector(
                        '.compare__slider.-left .compare-slider__item.swiper-slide-active .compare-slider__title'
                    );
                    const sliderWrapperOffset = sliderHeaderFixedTop.getBoundingClientRect().top;
                    const sliderWrapperBottom = sliderWrapperOffset + sliderWrapper.offsetHeight - sliderFixedHeader.offsetHeight;

                    if ((sliderWrapperOffset < 0) && (sliderWrapperBottom > wrapperBottomOffset)) {
                        isTopFixed = true;
                        sliderFixedHeader.classList.remove(fixedBottomClass);
                        sliderFixedHeader.classList.add(fixedClass);
                        return;
                    }

                    isTopFixed = false;
                    sliderFixedHeader.classList.remove(fixedClass);
                    if (sliderWrapperBottom <= wrapperBottomOffset) {
                        sliderFixedHeader.classList.add(fixedBottomClass);
                    }
                },
                resize() {
                    updateHeaderWidth();
                }
            });

            let headerSwipe;

            /**
             * @member {Array.<Object>} AppCompare#headerSwipe
             * @memberof layout
             * @desc Коллекция ссылок на экземпляры модулей обработки touch-событий [AppTouch]{@link components.AppTouch} заголовков свойств товаров в адаптивной версии.
             * @async
             * @readonly
             */
            Object.defineProperty(this, 'headerSwipe', {
                enumerable: true,
                get: () => headerSwipe
            });

            if (util.isTouch) {
                require('Components/touch').default.then(AppTouch => {
                    const pagesTitles = [...el.querySelectorAll('.compare-info-pages__item')];

                    let itemSwiped = false;

                    headerSwipe = pagesTitles.map((item, itemIndex) => {
                        const title = item.querySelector('.compare-info-pages__title');
                        const touchInstance = new AppTouch(title);

                        item.querySelector('.compare-info-pages__arrow.-prev').addEventListener('click', () => {
                            sliders[itemIndex].prev();
                        });
                        item.querySelector('.compare-info-pages__arrow.-next').addEventListener('click', () => {
                            sliders[itemIndex].next();
                        });

                        touchInstance.on({
                            touchmove() {
                                const verticalDelta = Math.abs(touchInstance.moveCoords[1]);
                                if (!itemSwiped && (Math.abs(touchInstance.moveCoords[0]) >= 30) && (verticalDelta <= 50)) {
                                    if (touchInstance.moveCoords[0] >= 30) {
                                        sliders[itemIndex].prev();
                                    } else if (touchInstance.moveCoords[0] <= 30) {
                                        sliders[itemIndex].next();
                                    }
                                    itemSwiped = true;
                                }
                            },

                            touchend() {
                                itemSwiped = false;
                            }
                        });

                        return touchInstance;
                    });
                });
                emitInit('touch');
            }
        }
    });

    addModule(moduleName, AppCompare);

    compareEl.forEach(el => new AppCompare(el));

    return AppCompare;
};

export default compareInit;
export {compareInit};