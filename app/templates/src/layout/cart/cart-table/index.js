import './style.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'cartTable';

/**
 * @class AppCartTable
 * @memberof layout
 * @requires components#AppCounter
 * @classdesc Модуль таблицы товаров в корзине.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const cartTableInstance = new app.CartTable(document.querySelector('.cart-table-element'));
 */
const AppCartTable = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const counterEl = el.querySelectorAll('.cart-table__counter');

        let counters;

        /**
         * @member {Array.<Object>} AppCartTable#counters
         * @memberof layout
         * @desc Коллекция модулей счётчиков [AppCounter]{@link components.AppCounter} в таблице товаров корзины.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'counters', {
            enumerable: true,
            get: () => counters
        });

        if (counterEl.length > 0) {
            const counterRequire = require('Components/counter');
            counterRequire.default.then(AppCounter => {
                counters = [...counterEl].map(item => {
                    const counter = new AppCounter(item);
                    return counter;
                });
            });

            counterRequire.counterTrigger(counterEl);
        }

        //TODO:state toggler for compare
    }
});

addModule(moduleName, AppCartTable);

export default AppCartTable;
export {AppCartTable};