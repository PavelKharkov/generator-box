import './sync.scss';

import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

let AppCartControls;
let AppCartTable;

//Опции
const moduleName = 'cart';

const cartEl = document.querySelectorAll('.cart');

/**
 * @class AppCart
 * @memberof layout
 * @classdesc Модуль корзины.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const cartInstance = new app.Cart(document.querySelector('.cart-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="cart"></div>
 *
 * <!--.cart - селектор по умолчанию-->
 */
const AppCart = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const cartTableEl = el.querySelectorAll('.cart__table');
        const cartCouponEl = el.querySelectorAll('.cart__coupon');
        const cartControlsEl = document.querySelectorAll('.top__controls.-cart');

        //Верхний блок в корзине
        let controls;

        /**
         * @member {Array.<Object>} AppCart#controls
         * @memberof layout
         * @desc Коллекция ссылок на модули верхних блоков корзины [AppCartControls]{@link layout.AppCartControls}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'controls', {
            enumerable: true,
            get: () => controls
        });

        if (cartControlsEl.length > 0) {
            AppCartControls = require('./cart-controls').default;

            controls = [...cartControlsEl].map(item => {
                const cartControls = new AppCartControls(item);
                return cartControls;
            });
        }

        //Таблица товаров в корзине
        let tables;

        /**
         * @member {Array.<Object>} AppCart#tables
         * @memberof layout
         * @desc Коллекция ссылок на модули таблицы товаров корзины [AppCartTable]{@link layout.AppCartTable}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'tables', {
            enumerable: true,
            get: () => tables
        });

        if (cartTableEl.length > 0) {
            AppCartTable = require('./cart-table').default;

            tables = [...cartTableEl].map(item => {
                const cartTable = new AppCartTable(item);
                return cartTable;
            });
        }

        //Блок купона
        if (__GLOBAL_SETTINGS__.coupons && (cartCouponEl.length > 0)) {
            require('./cart-coupon');
        }
    }
});

addModule(moduleName, AppCart);

//Инициализация элементов по классу
cartEl.forEach(el => new AppCart(el));

export default AppCart;
export {
    AppCart,
    AppCartControls,
    AppCartTable
};