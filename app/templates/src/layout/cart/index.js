let AppPreloader;
if (!__IS_SYNC__) {
    AppPreloader = require('Components/preloader').default;
    AppPreloader.initContentPreloader();
}

const cartCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "order" */ './sync.js'))
        .then(cartModules => {
            if (!__IS_SYNC__) AppPreloader.removeContentPreloader();

            const AppCart = cartModules.default;

            if (isModules) {
                const {AppCartControls, AppCartTable} = cartModules;
                resolve({
                    AppCart,
                    AppCartControls,
                    AppCartTable
                });
                return;
            }

            resolve(AppCart);
        });
};

const importFunc = new Promise(resolve => {
    cartCallback(resolve);
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    cartCallback(resolve, true);
});

export default importFunc;
export {modules};