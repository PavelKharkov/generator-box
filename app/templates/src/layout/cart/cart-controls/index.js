import {Module, immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'cartControls';

/**
 * @class AppCartControls
 * @memberof layout
 * @requires components#AppPopup
 * @classdesc Модуль верхнего блока корзины.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @example
 * const cartControlsInstance = new app.CartControls(document.querySelector('.cart-controls-element'));
 */
const AppCartControls = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const clearButton = el.querySelector('.top-controls__item.-clear .top-controls-item__link');

        /**
         * @member {HTMLElement} AppCartControls#clearButton
         * @memberof layout
         * @desc Кнопка открытия попапа очистки корзины.
         * @readonly
         */
        Object.defineProperty(this, 'clearButton', {
            enumerable: true,
            value: clearButton
        });

        let clearPopup;

        /**
         * @member {Object} AppCartControls#clearPopup
         * @memberof layout
         * @desc Ссылка на модуль попапа [AppPopup]{@link components.AppPopup} очистки корзины.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'clearPopup', {
            enumerable: true,
            get: () => clearPopup
        });

        if (clearButton) {
            const popupRequire = require('Components/popup');
            popupRequire.default.then(AppPopup => {
                clearPopup = new AppPopup(clearButton);
            });

            popupRequire.popupTrigger(clearButton);
        }
    }
});

addModule(moduleName, AppCartControls);

export default AppCartControls;
export {AppCartControls};