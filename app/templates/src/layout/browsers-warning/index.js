//Попап с предупреждением об устаревшем браузере

import util from 'Layout/main';

let AppBrowsersWarning;

//Опции
const platformVersion = Number.parseFloat(util.isChromium ? util.chromiumVersion : util.browserVersion);

//Проверка на устаревший браузер пользователя
if (
    (util.isChromium && (platformVersion < 60)) || //Chromium < 60
    ((util.browser === 'firefox') && (platformVersion < 60)) || //Firefox < 60
    ((util.browser === 'safari') && (platformVersion < 12)) || //Safari < 12
    ((util.browser === 'microsoft-edge') && (platformVersion < 79)) || //Edge < 79
    ((util.browser === 'ie') && (platformVersion < 11)) //IE < 11
) {
    AppBrowsersWarning =
        (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "browsers-warning" */ './sync.js'))
            .then(modules => {
                return modules.default;
            });
}

export default AppBrowsersWarning;
export {AppBrowsersWarning};