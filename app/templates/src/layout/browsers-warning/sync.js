import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';

//Опции
const moduleName = 'browsersWarning';

const browsersWarningTemplate = require('./template.js').default;

const wrapperEl = document.createElement('div');
wrapperEl.style.display = 'none';
wrapperEl.innerHTML = browsersWarningTemplate;
document.body.append(wrapperEl);

const browsersWarningEl = document.querySelectorAll('.browsers-warning');

const transitionDuration = Number.parseInt(util.getStyle('--transition-duration'), 10);

//Классы и селекторы
const alertSelector = '.browsers-warning__alert';
const popupSelector = '.browsers-warning__popup';
const activeClass = 'active';
const showClass = 'show';
const moreButtonSelector = '.browsers-warning-alert__button.-more';
const popupCloseButtonSelector = '.browsers-warning-popup__button.-close';
const popupContinueButtonSelector = '.browsers-warning-popup__button.-continue';
const alertCloseButtonSelector = '.browsers-warning-alert__button.-close';

/**
 * @class AppBrowsersWarning
 * @memberof layout
 * @classdesc Модуль уведомления об устаревших браузерах.
 * @desc Наследует: [Module]{@link app.Module}.
 * @ignore
 * @example
 * const browsersWarningInstance = new app.BrowsersWarning(document.querySelector('.browsers-warning-element'));
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <div class="browsers-warning"></div>
 *
 * <!--.browsers-warning - селектор по умолчанию-->
 */
const AppBrowsersWarning = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Элементы
        const alertEl = el.querySelector(alertSelector);
        if (!alertEl) return;
        const popupEl = el.querySelector(popupSelector);
        if (!popupEl) return;

        const header = document.querySelector('.header');
        const moreButton = alertEl.querySelector(moreButtonSelector);
        const popupCloseButton = popupEl.querySelector(popupCloseButtonSelector);
        const popupContinueButton = popupEl.querySelector(popupContinueButtonSelector);
        const alertCloseButton = alertEl.querySelector(alertCloseButtonSelector);

        if (header) {
            header.insertBefore(alertEl, header.firstChild);
        }
        alertEl.classList.add(activeClass);
        moreButton.focus();

        /**
         * @function showFunc
         * @desc Показывает попап уведомления.
         * @returns {undefined}
         * @ignore
         */
        const showFunc = () => {
            document.body.append(popupEl);
            popupEl.classList.add(activeClass);
            util.defer(() => {
                popupEl.classList.add(showClass);
            });
        };

        /**
         * @function hideFunc
         * @desc Скрывает попап уведомления.
         * @returns {undefined}
         * @ignore
         */
        const hideFunc = () => {
            alertEl.classList.remove(activeClass);
            popupEl.classList.remove(showClass);

            setTimeout(() => {
                popupEl.classList.remove(activeClass);
            }, transitionDuration);
        };

        const keydownFunc = event => {
            if (event.key !== 'Escape') return;

            hideFunc();
            document.removeEventListener('keydown', keydownFunc);
        };

        const closeButtonClick = () => {
            hideFunc();
            document.removeEventListener('keydown', keydownFunc);
        };

        popupCloseButton.addEventListener('click', closeButtonClick);
        popupContinueButton.addEventListener('click', closeButtonClick);
        moreButton.addEventListener('click', showFunc);
        alertCloseButton.addEventListener('click', () => {
            hideFunc();
            document.removeEventListener('keydown', keydownFunc);
        });
        document.addEventListener('keydown', keydownFunc);
    }
});

browsersWarningEl.forEach(el => new AppBrowsersWarning(el));

export default AppBrowsersWarning;
export {AppBrowsersWarning};