const baseClass = 'browsers-warning';
const alertClass = `${baseClass}-alert`;
const popupClass = `${baseClass}-popup`;
const browsersClass = `${popupClass}-browsers`;

/* eslint-disable max-len, vue/max-len */
const browsersWarningTemplate = `
    <div class="${baseClass}">
        <div class="${baseClass}__alert" role="alert" aria-labelledby="${alertClass}-title" aria-describedby="${alertClass}-desc" aria-hidden="true">
            <div class="${alertClass}__container">
                <div>
                    <div class="${alertClass}__inner">
                        <div class="${alertClass}__title" id="${alertClass}-desc">
                            <button class="${alertClass}__button -close" type="button" title="Закрыть (Esc)"></button>
                            <span id="${alertClass}-title">Ваш браузер устарел</span> и не может
                            обеспечить достаточной безопасности при работе с сайтом, а также может некорректно
                            отображать содержимое сайта.
                            <button class="${alertClass}__button -more"
                                    type="button">Подробнее
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="${baseClass}__popup fade" tabindex="-1" role="dialog" aria-labelledby="${popupClass}-title" aria-describedby="${popupClass}-desc" aria-hidden="true">
            <div class="${popupClass}__container">
                <div>
                    <div class="${popupClass}__inner">
                        <div class="${popupClass}__content">
                            <button class="${popupClass}__button -close" type="button" title="Закрыть (Esc)"></button>
                            <div class="${popupClass}__top">
                                <div class="${popupClass}__title" id="${popupClass}-title">Ваш браузер устарел</div>
                                <div class="${popupClass}__text" id="${popupClass}-desc">Он и
                                    не может обеспечить достаточной безопасности при работе с сайтом, а также
                                    может не корректно отображать содержимое сайта, поэтому мы предлагаем вам
                                    воспользоваться одним из нижеперечисленных бесплатных браузеров для открытия
                                    сайта:
                                </div>
                            </div>

                            <div class="${popupClass}__browsers">
                                <div class="${browsersClass}__item -chrome">
                                    <a class="${browsersClass}__link" href="https://www.google.ru/chrome/" target="_blank" title="Скачать браузер Google Chrome" rel="noopener">
                                        <div class="${browsersClass}__image"></div>
                                        <div class="${browsersClass}__title">Google Chrome</div>
                                    </a>
                                </div>
                                <div class="${browsersClass}__item -yandex">
                                    <a class="${browsersClass}__link" href="https://browser.yandex.ru/" target="_blank" title="Скачать Яндекс.Браузер" rel="noopener">
                                        <div class="${browsersClass}__image"></div>
                                        <div class="${browsersClass}__title">Яндекс.Браузер</div>
                                    </a>
                                </div>
                                <div class="${browsersClass}__item -firefox">
                                    <a class="${browsersClass}__link" href="https://www.mozilla.org/ru/firefox/" target="_blank" title="Скачать браузер Mozilla Firefox" rel="noopener">
                                        <div class="${browsersClass}__image"></div>
                                        <div class="${browsersClass}__title">Mozilla Firefox
                                        </div>
                                    </a>
                                </div>
                                <div class="${browsersClass}__item -edge">
                                    <a class="${browsersClass}__link" href="https://www.microsoft.com/ru-ru/edge" target="_blank" title="Скачать браузер Microsoft Edge" rel="noopener">
                                        <div class="${browsersClass}__image"></div>
                                        <div class="${browsersClass}__title">Microsoft Edge</div>
                                        <div class="${browsersClass}__desc">(Только на Windows 10)</div>
                                    </a>
                                </div>
                                <div class="${browsersClass}__item -safari">
                                    <a class="${browsersClass}__link" href="https://www.apple.com/ru/safari/" target="_blank" title="Скачать браузер Apple Safari" rel="noopener">
                                        <div class="${browsersClass}__image"></div>
                                        <div class="${browsersClass}__title">Apple Safari</div>
                                        <div class="${browsersClass}__desc">(Только на macOS)</div>
                                    </a>
                                </div>
                            </div>

                            <div class="${popupClass}__bottom">
                                <button class="${popupClass}__button -continue" type="button">Всё равно перейти на сайт</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
/* eslint-enable max-len, vue/max-len */

export default browsersWarningTemplate;