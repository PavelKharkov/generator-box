import './style.scss';

import {immutable} from 'Components/module';
import {addModule} from 'Base/scripts/app.js';

const moduleName = 'cardSliderTabs';

const cardSliderTabsInit = AppTabs => {
    /**
     * @class AppCardSliderTabs
     * @memberof layout
     * @classdesc Меню вкладок блока слайдера элементов .card.
     * @desc Наследует: [AppTabs]{@link components.AppTabs}.
     * @async
     * @param {HTMLElement} [element] - Элемент меню вкладок.
     * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
     * @example
     * const cardSliderTabsInstance = new app.CardSliderTabs(document.querySelector('.card-slider-tabs-container'), {
     *     collapse: true
     * });
     */
    const AppCardSliderTabs = immutable(class extends AppTabs {
        constructor(el, opts, appname = moduleName) {
            super(el, opts, appname);
        }
    });

    addModule(moduleName, AppCardSliderTabs);

    return AppCardSliderTabs;
};

export default cardSliderTabsInit;
export {cardSliderTabsInit};