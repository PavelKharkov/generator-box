import './style.scss';

import {onInit, emitInit} from 'Base/scripts/app.js';
import chunks from 'Base/scripts/chunks.js';

import AppWindow from 'Layout/window';

const cardSliderEl = document.querySelectorAll('[data-card-slider]');

let cardSliderPreloaders;
if (!__IS_SYNC__) {
    const AppPreloader = require('Components/preloader').default;
    cardSliderPreloaders = [...cardSliderEl].map(item => {
        const cardSliderPreloader = new AppPreloader(item, {
            overlay: false,
            once: true
        });
        cardSliderPreloader.show();
        return cardSliderPreloader;
    });
}

const cardSliderCallback = (resolve, isModules) => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "sliders" */ './sync.js'))
        .then(modules => {
            chunks.sliders = true;

            if (!__IS_SYNC__ && cardSliderPreloaders) {
                cardSliderPreloaders.forEach(preloader => {
                    preloader.hide();
                });
            }

            const {AppCardSlider} = modules;

            if (isModules) {
                const {AppCardSliderTabs} = modules;
                resolve({
                    AppCardSlider,
                    AppCardSliderTabs
                });
                return;
            }

            resolve(AppCardSlider);
        });
};

const importFunc = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.sliders) {
        cardSliderCallback(resolve);
        return;
    }

    onInit('cardSlider', () => {
        cardSliderCallback(resolve);
    });
    AppWindow.onload(() => {
        emitInit('cardSlider');
    });
});

//Подключение отдельных модулей
const modules = new Promise(resolve => {
    if (__IS_SYNC__ || chunks.sliders) {
        cardSliderCallback(resolve, true);
        return;
    }

    onInit('cardSlider', () => {
        cardSliderCallback(resolve, true);
    });
});

export default importFunc;
export {modules};