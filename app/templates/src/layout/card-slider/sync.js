import './sync.scss';

import {Module, immutable} from 'Components/module';
import util from 'Layout/main';
import {addModule, emitInit} from 'Base/scripts/app.js';

let AppCardSliderTabs;

//Опции
const moduleName = 'cardSlider';

const cardSliderEl = document.querySelectorAll('[data-card-slider]');

//Селекторы и классы
const cardSliderClass = util.kebabCase(moduleName);
const sliderSelector = `.${cardSliderClass}__slider`;
const tabsSelector = `.${cardSliderClass}-tabs-menu__list`;
const initializedClass = `${cardSliderClass}-initialized`;

//Опции по умолчанию
const defaultOptions = {
    slider: {}
};

//Опции плагина слайдера по умолчанию
const defaultPluginOptions = {
    slidesPerView: 1,

    init: true,
    grabCursor: true,
    watchOverflow: true,
    lazy: true,

    breakpoints: {
        480: {
            slidesPerView: 2
        },
        [util.media.md]: {
            slidesPerView: 3
        },
        [util.media.lg]: {
            slidesPerView: 4
        },
        [util.media.xl]: {
            slidesPerView: 5
        }
    }
};

/**
 * @class AppCardSlider
 * @memberof layout
 * @requires components#AppSlider
 * @classdesc Блок со слайдером элементов .card.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} [element] - Элемент слайдера.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 * @param {(app.Module.instanceOptions|Array.<app.Module.instanceOptions>)} [options.slider={}] - Опции экземпляра слайдера. Повторяют опции [AppSlider]{@link components.AppSlider}. Если элемент содержит несколько слайдеров, то опции слайдеров можно указывать как коллекцию объектов.
 * @param {app.Module.instanceOptions} [options.tabs] - Опции экземпляра табов. Повторяют опции [AppTabs]{@link components.AppTabs}.
 * @example
 * const cardSliderInstance = new app.CardSlider(document.querySelector('.card-slider-container'), {
 *     //Установка разных опций для слайдеров
 *     slider: [
 *         {pluginOptions: {slidesPerView: 1}},
 *         {pluginOptions: {slidesPerView: 2}}
 *     ]
 * });
 *
 * @example <caption>Пример HTML-разметки</caption>
 * <!--HTML-->
 * <section class="card-slider" data-card-slider>
 *     <div class="card-slider__slider swiper" hidden>
 *         <div class="card-slider__items swiper-wrapper">
 *             <div class="card swiper-slide">Элемент слайдера 1</div>
 *             <div class="card swiper-slide">Элемент слайдера 2</div>
 *         </div>
 *     </div>
 * </section>
 *
 * <!--data-card-slider - селектор по умолчанию-->
 *
 * @example <caption>Добавление опций через data-атрибут</caption>
 * <!--HTML-->
 * <div data-card-slider-options='{"slider": {"pluginOptions": {"slidesPerView": 1}}}'></div>
 */
const AppCardSlider = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        const sliderEl = el.querySelectorAll(sliderSelector);
        if (sliderEl.length === 0) return;

        const cardSliderOptionsData = el.dataset.cardSliderOptions;
        if (cardSliderOptionsData) {
            const dataOptions = util.stringToJSON(cardSliderOptionsData);
            if (!dataOptions) util.error('incorrect data-card-slider-options format');
            util.extend(this.options, dataOptions);
        }

        util.extend(this.options, defaultOptions);

        //Установка опций плагина по умолчанию
        //TODO:docs
        const setDefaultPluginOptions = sliderOptions => {
            if (sliderOptions.pluginOptions &&
                (util.isObject(sliderOptions.pluginOptions) && (Object.keys(sliderOptions.pluginOptions).length > 0))) {
                return;
            }

            if (!sliderOptions.pluginOptions) {
                sliderOptions.pluginOptions = {};
            }
            util.extend(sliderOptions.pluginOptions, defaultPluginOptions);
        };

        if (this.options.slider) {
            if (Array.isArray(this.options.slider)) {
                this.options.slider.forEach(sliderOptions => {
                    setDefaultPluginOptions(sliderOptions);
                });
            } else {
                setDefaultPluginOptions(this.options.slider);
            }
        }

        /**
         * @member {Object} AppCardSlider#params
         * @memberof components
         * @desc Параметры экземпляра.
         * @readonly
         */
        const params = Module.setParams(this);

        let sliders;

        /**
         * @member {Array.<Object>} AppCardSlider#sliders
         * @memberof layout
         * @desc Коллекция ссылок на экземпляры модуля [AppSlider]{@link components.AppSlider}.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'sliders', {
            enumerable: true,
            get: () => sliders
        });

        require('Components/slider').default.then(AppSlider => {
            sliders = [...sliderEl].map((item, sliderIndex) => {
                const sliderContainer = item.parentNode;
                const sliderOptions = Array.isArray(params.slider) ? params.slider[sliderIndex] : params.slider;
                sliderOptions.pluginOptions = {
                    ...sliderOptions.pluginOptions,
                    navigation: {
                        prevEl: sliderContainer.querySelector('.card-slider-nav__arrow.-prev'),
                        nextEl: sliderContainer.querySelector('.card-slider-nav__arrow.-next')
                    },
                    pagination: {
                        el: sliderContainer.querySelector('.card-slider__pagination')
                    }
                };

                const slider = new AppSlider(item, sliderOptions);

                return slider;
            });
        });
        emitInit('slider');

        let tabs;

        /**
         * @member {Array.<Object>} AppProduct#tabs
         * @memberof layout
         * @desc Коллекция ссылок на модули табов [AppCardSliderTabs]{@link layout.AppCardSliderTabs} в блоке слайдера карточек.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'tabs', {
            enumerable: true,
            get: () => tabs
        });

        const tabsEl = el.querySelectorAll(tabsSelector);
        if (tabsEl.length > 0) {
            const tabsRequire = require('Components/tabs');
            tabsRequire.default.then(() => {
                tabs = [...tabsEl].map(item => {
                    const cardSliderTabs = new AppCardSliderTabs(item, params.tabs);
                    return cardSliderTabs;
                });
            });

            tabsRequire.tabsTrigger(tabsEl);
        }

        el.classList.add(initializedClass);
    }
});

addModule(moduleName, AppCardSlider);

require('Components/tabs').default.then(AppTabs => {
    const cardSliderTabsInit = require('./card-slider-tabs').default;
    AppCardSliderTabs = cardSliderTabsInit(AppTabs);
});

//Инициализация элементов по data-атрибуту
cardSliderEl.forEach(el => new AppCardSlider(el));

export default AppCardSlider;
export {
    AppCardSlider,
    AppCardSliderTabs
};