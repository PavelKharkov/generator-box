import './sync.scss';

import {Module, immutable} from 'Components/module';
//import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'checkboxTree';

const checkboxTreeEl = document.querySelectorAll('.checkbox-tree');

//Селекторы и классы
const collapseSelector = '.checkbox-tree__button.-collapse';
const checkboxSelector = '.checkbox-tree-form-check__input';
const containerSelector = '.checkbox-tree__container';
const listSelector = '.checkbox-tree__list';
const itemSelector = '.checkbox-tree__item';
const firstLevelClass = '-level1';

/**
 * @class AppCheckboxTree
 * @memberof layout
 * @requires components#AppCollapse
 * @classdesc Древо разделов.
 * @desc Наследует: [Module]{@link app.Module}.
 * @async
 * @param {HTMLElement} element - HTML-элемент экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 */
const AppCheckboxTree = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Раскрывающиеся блоки
        const collapses = [];

        /**
         * @member {Object} AppCheckboxTree#collapses
         * @memberof layout
         * @desc Ссылки на модули раскрывающегося контента [AppCollapse]{@link components.AppCollapse} разделов.
         * @async
         * @readonly
         */
        Object.defineProperty(this, 'collapses', {
            enumerable: true,
            value: collapses
        });

        const collapseRequire = require('Components/collapse');
        const initCollapse = AppCollapse => {
            const collapseButtons = el.querySelectorAll(collapseSelector);
            collapseButtons.forEach(button => {
                const collapseInstance = new AppCollapse(button);

                collapseInstance.on('hidden', () => {
                    collapseInstance.targetElements[0].querySelectorAll(collapseSelector).forEach(innerButton => {
                        innerButton.modules.collapse.hide({
                            duration: 0
                        });
                    });
                });

                collapses.push(collapseInstance);
                collapseRequire.collapseTrigger(button);
            });
        };

        collapseRequire.default.then(AppCollapse => {
            initCollapse(AppCollapse);
        });

        /**
         * @function setParentCheckboxes
         * @desc Находит родительский элемент раздела и переключает в нем чекбоксы.
         * @returns {undefined}
         * @ignore
         */
        const setParentCheckboxes = currentItem => {
            const isInner = !currentItem.classList.contains(firstLevelClass);
            if (!isInner) return;

            const parentContainer = currentItem.closest(containerSelector).closest(itemSelector);

            const parentCheckbox = parentContainer.querySelector(checkboxSelector);
            const childItems = [...parentContainer.querySelectorAll(`${containerSelector} > ${listSelector} > ${itemSelector}`)];

            let isAllChecked = true;
            let isSomeChecked = false;

            childItems.find(item => {
                const checkbox = item.querySelector(checkboxSelector);
                if (checkbox.checked) {
                    isSomeChecked = true;
                }
                if (!checkbox.checked) {
                    isAllChecked = false;
                }

                return !isAllChecked && isSomeChecked;
            });

            if (isAllChecked) {
                parentCheckbox.checked = true;
                parentCheckbox.indeterminate = false;
            } else {
                parentCheckbox.checked = false;
                parentCheckbox.indeterminate = isSomeChecked;
            }

            setParentCheckboxes(parentContainer);
        };

        //Чекбоксы
        const checkboxes = el.querySelectorAll(checkboxSelector);
        checkboxes.forEach(checkbox => {
            checkbox.addEventListener('change', () => {
                const wrapperItem = checkbox.closest(itemSelector);

                const hasItems = wrapperItem.classList.contains('-has-items');
                if (hasItems) {
                    const isChecked = checkbox.checked;

                    wrapperItem.querySelectorAll(`${containerSelector} ${checkboxSelector}`)
                        .forEach(innerCheckbox => {
                            innerCheckbox.checked = isChecked;
                            innerCheckbox.indeterminate = false;
                        });
                }

                setParentCheckboxes(wrapperItem);
            });
        });
    }
});

addModule(moduleName, AppCheckboxTree);

checkboxTreeEl.forEach(el => new AppCheckboxTree(el));

export default AppCheckboxTree;
export {AppCheckboxTree};