import './style.scss';

const checkboxTreeCallback = resolve => {
    (__IS_SYNC__ ? import(/* webpackChunkName: "modules" */ './sync.js') : import(/* webpackChunkName: "personal" */ './sync.js'))
        .then(modules => {
            const {AppCheckboxTree} = modules.default;
            resolve(AppCheckboxTree);
        });
};

const importFunc = new Promise(resolve => {
    checkboxTreeCallback(resolve);
});

export default importFunc;