/**
 * @module nouislider
 * @memberof libs
 * @version 15.5.1
 * @desc [Сайт]{@link https://refreshless.com/nouislider/}. Экспортирует noUiSlider.
 */

import noUiSlider from 'nouislider/dist/nouislider.js';

import 'nouislider/dist/nouislider.css';

export default noUiSlider;