/**
 * @module svg4everybody
 * @memberof libs
 * @version 2.1.9
 * @desc [Сайт]{@link https://github.com/jonathantneal/svg4everybody}. Экспортирует svg4everybody.
 */

import svg4everybody from 'svg4everybody/dist/svg4everybody.js';

export default svg4everybody;