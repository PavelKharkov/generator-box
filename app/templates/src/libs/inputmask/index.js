/**
 * @module inputmask
 * @memberof libs
 * @version 5.0.7
 * @desc [Сайт]{@link https://github.com/RobinHerbots/Inputmask}. Экспортирует Inputmask.
 */

import Inputmask from 'inputmask/dist/inputmask.js';

export default Inputmask;