/**
 * @module twentytwenty
 * @memberof libs
 * @requires libs.jquery
 * @requires libs.jquery-event-move
 * @version 0.1.0
 * @desc [Сайт]{@link https://github.com/zurb/twentytwenty}.
 * @ignore
 */

import 'Libs/jquery';
import 'Libs/jquery-event-move';

import './jquery.twentytwenty.js';

import './twentytwenty.custom.scss';