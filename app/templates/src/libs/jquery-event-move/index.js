/**
 * @module jquery-event-move
 * @memberof libs
 * @requires libs.jquery
 * @version 2.0.0
 * @desc [Сайт]{@link https://github.com/stephband/jquery.event.move}.
 * @ignore
 */

import 'Libs/jquery';

import jqueryEventMove from './jquery.event.move.js';

jqueryEventMove();