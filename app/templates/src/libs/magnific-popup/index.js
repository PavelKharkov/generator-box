/**
 * @module magnific-popup
 * @memberof libs
 * @requires libs.jquery
 * @version 1.1.0
 * @desc [Сайт]{@link https://github.com/dimsemenov/Magnific-Popup}.
 */

import 'Libs/jquery';

import 'magnific-popup/dist/jquery.magnific-popup.js';

import './magnific-popup.custom.scss';