/**
 * @module platform
 * @memberof libs
 * @version 1.3.5
 * @desc [Сайт]{@link https://github.com/bestiejs/platform.js/}. Экспортирует platform.
 */

import platform from 'platform/platform.js';

window.app.platform = platform;

export default platform;