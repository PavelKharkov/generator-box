/**
 * @module pikaday
 * @memberof libs
 * @version 1.8.2
 * @desc [Сайт]{@link https://github.com/Pikaday/Pikaday}. Экспортирует Pikaday.
 */

import Pikaday from 'pikaday/pikaday.js';

import './pikaday.custom.scss';

export default Pikaday;