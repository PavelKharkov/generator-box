/**
 * @module masonry
 * @memberof libs
 * @version 4.2.2
 * @desc [Сайт]{@link https://github.com/desandro/masonry}. Экспортирует Masonry.
 * @ignore
 */

import Masonry from 'masonry-layout';

export default Masonry;