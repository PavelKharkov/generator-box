/**
 * @module choices
 * @memberof libs
 * @version 10.1.0
 * @desc [Сайт]{@link https://github.com/jshjohnson/Choices}. Экспортирует Choices.
 */

import Choices from 'choices.js/public/assets/scripts/choices.js';

import './choices.custom.scss';

export default Choices;