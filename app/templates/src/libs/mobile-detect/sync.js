/**
 * @module mobile-detect
 * @memberof libs
 * @version 1.4.5
 * @desc [Сайт]{@link http://hgoebl.github.io/mobile-detect.js/}. Экспортирует MobileDetect.
 */

import MobileDetect from 'mobile-detect/mobile-detect.js';

window.app.MobileDetect = MobileDetect;

export default MobileDetect;