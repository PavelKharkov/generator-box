/**
 * @module owl-carousel
 * @memberof libs
 * @requires libs.jquery
 * @version 2.3.4
 * @desc [Сайт]{@link https://owlcarousel2.github.io/OwlCarousel2/}.
 */

import 'Libs/jquery';

import 'owl.carousel/dist/owl.carousel.js';

import './owl.carousel.scss';