/**
 * @module popper
 * @memberof libs
 * @version 2.11.5
 * @desc [Сайт]{@link https://github.com/FezVrasta/popper.js}. Экспортирует Popper.
 */

import {createPopper} from '@popperjs/core/dist/esm/popper.js';

export default createPopper;