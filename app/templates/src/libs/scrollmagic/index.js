/**
 * @module scrollmagic
 * @memberof libs
 * @version 2.0.8
 * @desc [Сайт]{@link https://github.com/janpaepke/ScrollMagic}. Экспортирует ScrollMagic, addIndicators.
 * @ignore
 */

import ScrollMagic from 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js';

let addIndicators = false;

if (!__IS_DIST__ || __IS_DEBUG__) {
    require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js');
    addIndicators = true;
}

export default ScrollMagic;
export {ScrollMagic, addIndicators};