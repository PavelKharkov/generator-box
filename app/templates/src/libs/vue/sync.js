//TODO:docs

import Vue from 'vue/dist/vue.esm.js';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

window.app.Vue = Vue;
//TODO:remove
//window.app.VueRouter = VueRouter;
//window.app.Vuex = Vuex;

export default Vue;
export {
    Vue,
    VueRouter,
    Vuex
};