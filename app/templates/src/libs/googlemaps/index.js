/**
 * @module googlemaps
 * @memberof libs
 * @version 1.0.12
 * @desc [Сайт]{@link https://github.com/googlemaps/js-markerclusterer}. Экспортирует MarkerClusterer.
 */

import {MarkerClusterer} from '@googlemaps/markerclusterer';

export {MarkerClusterer};