/**
 * @module validate
 * @memberof libs
 * @version 0.13.1
 * @desc [Сайт]{@link https://validatejs.org/}. Экспортирует validate.
 */

import validate from 'validate.js/validate.js';

export default validate;