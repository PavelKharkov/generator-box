/**
 * @module photoswipe
 * @memberof libs
 * @version 4.1.3
 * @desc [Сайт]{@link http://photoswipe.com/}. Экспортирует PhotoSwipe, PhotoSwipeUIDefault.
 * @ignore
 */

import PhotoSwipe from 'photoswipe/dist/photoswipe.js';
import PhotoSwipeUIDefault from 'photoswipe/dist/photoswipe-ui-default.js';

import './photoswipe.custom.scss';

export {PhotoSwipe, PhotoSwipeUIDefault};