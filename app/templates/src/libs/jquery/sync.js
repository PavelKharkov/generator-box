/**
 * @module jquery
 * @memberof libs
 * @version 3.6.0
 * @desc [Сайт]{@link https://jquery.com/}. Экспортирует jQuery.
 */

import jQuery from 'jquery/dist/jquery.slim.js';

window.app.jQuery = jQuery;

window.jQuery = jQuery;
window.$ = window.jQuery;

export default jQuery;