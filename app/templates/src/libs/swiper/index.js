/**
 * @module swiper
 * @memberof libs
 * @version 8.1.3
 * @desc [Сайт]{@link https://swiperjs.com/}. Экспортирует Swiper.
 */

import Swiper from 'swiper/bundle';

import 'swiper/css/bundle';

export default Swiper;