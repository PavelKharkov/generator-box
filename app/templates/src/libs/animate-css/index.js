/**
 * @module animate-css
 * @memberof libs
 * @version 3.70.0
 * @desc [Сайт]{@link https://daneden.github.io/animate.css/}.
 * @ignore
 */

import 'animate.css/animate.css';