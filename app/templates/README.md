# <%= projectName %>

[Демо](http://verstka.100up.ru/box/) | [Документация: скрипты](http://verstka.100up.ru/box-docs/scripts/) | [Документация: шаблоны](http://verstka.100up.ru/box-docs/templates/)

## Требования:
Корректно работает с node.js 16.13.2 и выше и необходимо использовать [Yarn](https://yarnpkg.com/) 1.22.18 и выше

Для корректной работы IDE нужно установить глобально:
```bash
npm i @babel/eslint-parser eslint eslint-plugin-jest eslint-plugin-jsdoc eslint-plugin-unicorn eslint-plugin-vue sass stylelint stylelint-scss vue-eslint-parser -g
```

## Начало работы:
```bash
yarn install
```

## Доступные команды:
```bash
# запуск webpack в режиме разработки
yarn dev

# компиляция файлов для релиза
yarn dist

# запуск линтера скриптов
yarn lint

# запуск линтера стилей
yarn lint-styles

# запуск линтера шаблонов
yarn lint-templates

# создание нового модуля
yarn module путь_к/модулю [-a]
# если не указана родительская папка, то модуль создается в src/layout/. С флагом -a в папке модуля добавляется файл autoload.js

# проверка css-кода на наличие неиспользуемых классов
yarn purify-css

# рендер файла шаблона в html-файл в модуле
yarn render-template путь_к/модулю [код для рендера]

# сортировка css-правил. Автоматически запускается перед компиляцией для релиза
yarn sort-styles

# запуск тестов
yarn test
# тесты выводятся в терминале и в файле tests/results.html. Состояние покрытия тестов находится в coverage/
```