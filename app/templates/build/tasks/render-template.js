'use strict';

//Пример:
//yarn render-template layout/browsers-warning +unsupportedBrowsersWarning

const fs = require('node:fs');
const path = require('node:path');

global.templatesPath = path.resolve();
const templatesPath = global.templatesPath;
const commonConfigPath = path.join(templatesPath, 'build', 'config', 'common');
const config = require(path.join(commonConfigPath, 'config.js'));
const pugLocals = require(path.join(commonConfigPath, 'pug.js'));
const globalSettings = require(path.join(commonConfigPath, `${config.globalSettingsFilename}.json`));

const pug = require('pug');
const beautify = require('js-beautify').html;

const taskStartTime = new Date();
console.log(`${config.formatTime(taskStartTime)} Starting 'render-template'...`);

if (process.argv[2]) { //Путь модуля должен быть установлен в параметрах
    const modulePathArg = process.argv[2];
    const modulePathArray = modulePathArg.split('/');
    if (modulePathArray.length < 2) throw new Error('incorrect module path');

    const afterCode = process.argv[3];

    const srcPath = config.paths.src;
    let modulePath = path.join(srcPath, ...modulePathArray);
    let templatePath;
    if (modulePath.endsWith(`.${config.devTemplateExt}`)) {
        templatePath = modulePath;
        modulePath = path.dirname(modulePath);
    } else {
        templatePath = path.join(modulePath, config.moduleTemplateFile);
    }
    if (!fs.existsSync(templatePath)) throw new Error('template file doesn\'t exists');

    const moduleName = path.basename(templatePath);

    //Добавление глобальных переменных для компиляции
    pugLocals.basedir = path.join(templatesPath, srcPath);
    pugLocals.filename = moduleName;
    Object.assign(pugLocals.globalConfig, {
        templatesRoot: templatesPath,
        defaultLanguage: 'ru',
        defaultRegion: 'RU',
        settings: globalSettings
    });

    const content = fs.readFileSync(templatePath, config.encoding);

    const filenameLine = `- const filename = '${moduleName}';`;
    const pageLine = '- const page = {};';
    const renderContent = [
        filenameLine,
        pageLine,
        'include /base/templates/main.pug',
        content
    ];
    if (afterCode) {
        renderContent.push(afterCode);
    }
    const pugRender = pug.render(renderContent.join('\n'), pugLocals);

    const output = beautify(pugRender, config.beautifyConfig);

    const outputFilename = `${path.basename(templatePath, `.${config.devTemplateExt}`)}.${config.distTemplateExt}`;
    const outputFile = path.join(modulePath, outputFilename);
    fs.writeFile(outputFile, output, config.encoding, () => {
        console.log(`Successfully saved file '${outputFilename}' in '${modulePath}'`);

        const finishTime = new Date();
        console.log(`${config.formatTime(finishTime)} Finished 'render-template' after ${finishTime.getTime() - taskStartTime.getTime()} ms`);
    });
}