'use strict';

const path = require('node:path');

const stylelint = require('stylelint');

global.templatesPath = (typeof global.templatesPath === 'undefined') ? path.resolve() : global.templatesPath;
const buildConfigPath = path.join(global.templatesPath, 'build', 'config');
const sortStylesConfigPath = path.join(buildConfigPath, 'sort-styles');

const isCliRun = process.argv.includes('--cli-run');

const sortStyles = callback => {
    stylelint.lint({
        files: [
            'src/**/*.css',
            'src/**/*.scss'
        ],
        configFile: path.join(sortStylesConfigPath, '.stylelintrc'),
        ignorePath: path.join(sortStylesConfigPath, '.stylelintignore'),
        fix: true
    })
        .then(data => {
            if (data.errored) {
                const errors = data.results.filter(({errored}) => {
                    return errored;
                });
                errors.forEach(({source, warnings}) => {
                    console.log(source, warnings);
                    //TODO:custom log
                });
                return;
            }

            if (typeof callback === 'function') callback(); /* eslint-disable-line callback-return */
        });
};

if (isCliRun) sortStyles();

module.exports = sortStyles;