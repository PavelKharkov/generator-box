'use strict';

//Переменные окружения
/* eslint-disable no-process-env */
process.env.NODE_ENV = process.env.NODE_ENV.trim();
const nodeEnv = process.env.NODE_ENV;
/* eslint-enable no-process-env */

const path = require('node:path');
const v8 = require('node:v8');

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

global.templatesPath = path.resolve();
const buildPath = path.join(global.templatesPath, 'build');
const webpackConfigDir = path.join(buildPath, 'config', 'webpack');
const webpackConfig = require(path.join(webpackConfigDir, 'config.js'));
const webpackCallbacks = require(path.join(webpackConfigDir, 'callbacks.js'));

webpackCallbacks.preinit(taskStartTime => {
    console.log(`node heap limit = ${v8.getHeapStatistics().heap_size_limit / (1024 * 1024)} Mb`);

    //Запуск вебпака
    if (nodeEnv === 'development') {
        webpackCallbacks.developmentPreinit();
        const compiler = webpack(webpackConfig);
        const server = new WebpackDevServer(webpackConfig.devServer, compiler);

        webpackCallbacks.developmentCallback(compiler, server, taskStartTime);
    } else {
        webpackCallbacks.productionPreinit(() => {
            const compiler = webpack(webpackConfig, () => {
                //empty function
            });

            webpackCallbacks.productionCallback(compiler);
        });
    }
});