'use strict';

const fs = require('node:fs');
const path = require('node:path');

const configFile = require('pug-lint/lib/config-file');
const PugLint = require('pug-lint');

const templatesPath = path.resolve();
const config = require(path.join(templatesPath, 'build', 'config', 'common', 'config.js'));

const wrapUnderscore = message => `\u001B[4m${message}\u001B[0m`;
//const wrapGreen = message => `\u001B[32m${message}\u001B[0m`; //TODO:use
const wrapRed = message => `\u001B[31m${message}\u001B[0m`;

const linterConfig = configFile.load();
const linter = new PugLint();
linter.configure(linterConfig);

//Рекурсивно собирает коллекцию файлов шаблонов в директории
const addFilesInDir = modulesDirectory => {
    const modulesPath = path.join(templatesPath, modulesDirectory);
    return fs.readdirSync(modulesPath).reduce((accumulator, file) => {
        const filePath = path.join(modulesPath, file);
        if (fs.statSync(filePath).isDirectory()) {
            accumulator.push(...addFilesInDir(path.join(modulesDirectory, file), config.devTemplateExt));
        }

        if (path.extname(filePath) === `.${config.devTemplateExt}`) {
            accumulator.push(filePath);
        }

        return accumulator;
    }, []);
};

//Проверка файла
const getFileErrors = filePath => linter.checkFile(filePath);

//Корневые директории исходников с файлами модулей
const moduleDirectories = [
    'base',
    'components',
    'layout',
    'pages'
];

const errorFiles = [];

const files = moduleDirectories.reduce((accumulator, modulesDirectory) => {
    const modulesDirPath = path.join(config.paths.src, modulesDirectory);
    accumulator.push(...addFilesInDir(modulesDirPath));
    return accumulator;
}, []);

files.forEach(file => {
    const allErrors = getFileErrors(file);
    if (allErrors.length === 0) return;

    const resultErrors = allErrors.filter(error => {
        error.code = error.code.slice(9).toLowerCase();
        const srcLines = error.src.split('\n').reverse();
        const errorLine = srcLines.length - error.line;
        const checkedLines = srcLines.slice(errorLine + 1);

        const disabledError = checkedLines.some((line, disabledIndex) => {
            const codeRegexp = new RegExp(`${error.code}(,|$)`, 'i');
            if (line.includes('//-pug-lint-disable-line ') && codeRegexp.test(line)) return true;

            if (line.includes('//-pug-lint-disable ')) {
                if (codeRegexp.test(line)) {
                    const enabledError = checkedLines.some((line2, enabledIndex) => { /* eslint-disable-line max-nested-callbacks */
                        if (disabledIndex <= enabledIndex) return false;
                        if (line2.includes('//-pug-lint-enable ') && codeRegexp.test(line2)) return true;
                        return false;
                    });
                    return !enabledError;
                }
            }

            return false;
        });

        return !disabledError;
    });
    if (resultErrors.length === 0) return;
    errorFiles.push(resultErrors);
});
if (errorFiles.length === 0) return;

//Вывод ошибок в консоли
let errorsCount = 0;
errorFiles.forEach(errors => {
    const filename = [wrapUnderscore(errors[0].filename)];
    const fileMessage = errors.reduce((accumulator, error) => {
        errorsCount += 1;
        const lineColumn = `${error.line}:${error.column || 0}`;
        const errorContent = `${wrapRed('error')}  ${error.msg}  ${error.code}`;
        accumulator.push(`  ${lineColumn}  ${errorContent}`);
        return accumulator;
    }, filename);
    console.log(fileMessage.join('\n'));
});

console.log(wrapRed(`✖ ${errorsCount} errors`));