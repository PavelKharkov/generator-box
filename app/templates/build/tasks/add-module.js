'use strict';

const fs = require('node:fs');
const path = require('node:path');

global.templatesPath = path.resolve();
const config = require(path.join(global.templatesPath, 'build', 'config', 'common', 'config.js'));
const moduleConfigPath = path.join(config.paths.buildConfig, 'add-module');

const taskStartTime = new Date();
console.log(`${config.formatTime(taskStartTime)} Starting 'add-module'...`);

const args = process.argv;

if (args[2]) { //Путь модуля должен быть установлен в параметрах
    const modulePath = args[2].trim();
    if (!modulePath) return;
    const modulePathArray = modulePath.split('/');
    const defaultDirRegex = new RegExp(`^(${config.paths.componentsName}|${config.paths.dataName}|${config.paths.fontsName}|${config.paths.layoutName}|${config.paths.libsName}|${config.paths.pagesName})$`);
    const defaultDirName = defaultDirRegex.test(modulePathArray[0]) ? '' : config.paths.layoutName;
    const moduleSrcPath = path.join(config.paths.src, defaultDirName, modulePath);

    if (fs.existsSync(moduleSrcPath) && fs.lstatSync(moduleSrcPath).isDirectory()) {
        console.log(`module already exists at '${moduleSrcPath}'`);
    } else {
        fs.mkdirSync(moduleSrcPath);

        //Определение типа модуля
        const defaultModuleType = 'base';
        let moduleType = defaultModuleType;
        const typesRegex = new RegExp(`^(${config.paths.dataName}|${config.paths.fontsName}|${config.paths.libsName}|${config.paths.pagesName})$`);
        if (typesRegex.test(modulePathArray[0])) {
            moduleType = modulePathArray[0];
        }

        const moduleConfigTypePath = path.join(moduleConfigPath, moduleType);

        const moduleName = path.basename(modulePath);
        const dirName = defaultDirName || modulePathArray[0];
        const moduleNameUpper = moduleName.charAt(0).toUpperCase() + moduleName.slice(1).replaceAll('-', ' ');
        const moduleNamePascal = moduleNameUpper
            .replace(/^\w|[-A-Z]|\b\w|\s+/g, match => {
                return match.toUpperCase();
            })
            .replace(/[\s-]+/g, '');
        const moduleNameCamel = moduleNamePascal.charAt(0).toLowerCase() + moduleNamePascal.slice(1);

        if (moduleType === defaultModuleType) {
            const hasAutoload = args.includes('-a');
            /* eslint-disable max-depth */
            if (hasAutoload) {
                const moduleAutoloadScriptFile = path.join(moduleConfigTypePath, config.moduleAutoloadScriptFile);
                if (fs.existsSync(moduleAutoloadScriptFile)) {
                    const autoloadScriptData = fs.readFileSync(moduleAutoloadScriptFile, config.encoding)
                        .replaceAll('moduleNameKebab', moduleName);
                    fs.writeFileSync(
                        path.join(moduleSrcPath, config.moduleAutoloadScriptFile),
                        autoloadScriptData,
                        config.encoding
                    );
                }
            }
            /* eslint-enable max-depth */
        }

        //Запись файла скриптов
        const moduleScriptFile = path.join(moduleConfigTypePath, config.moduleScriptFile);
        if (fs.existsSync(moduleScriptFile)) {
            const scriptData = fs.readFileSync(moduleScriptFile, config.encoding)
                .replaceAll('dirName', dirName)
                .replaceAll('moduleNameKebab', moduleName)
                .replaceAll('moduleNamePascal', moduleNamePascal)
                .replaceAll('moduleNameCamel', moduleNameCamel);
            fs.writeFileSync(
                path.join(moduleSrcPath, config.moduleScriptFile),
                scriptData,
                config.encoding
            );
        }

        //Запись файла стилей
        const moduleStyleFile = path.join(moduleConfigTypePath, config.moduleStyleFile);
        if (fs.existsSync(moduleStyleFile)) {
            const styleData = fs.readFileSync(moduleStyleFile, config.encoding)
                .replaceAll('moduleNameKebab', moduleName);
            fs.writeFileSync(
                path.join(moduleSrcPath, config.moduleStyleFile),
                styleData,
                config.encoding
            );
        }

        //Запись файла шаблона
        const moduleTemplateFile = path.join(moduleConfigTypePath, config.moduleTemplateFile);
        if (fs.existsSync(moduleTemplateFile)) {
            const templateData = fs.readFileSync(moduleTemplateFile, config.encoding)
                .replaceAll('dirName', dirName)
                .replaceAll('moduleNameKebab', moduleName)
                .replaceAll('moduleNameCamel', moduleNameCamel)
                .replaceAll('moduleNameUpper', moduleNameUpper);
            fs.writeFileSync(
                path.join(moduleSrcPath, config.moduleTemplateFile),
                templateData,
                config.encoding
            );
        }

        //Запись файла контента
        const moduleDataFile = path.join(moduleConfigTypePath, config.moduleDataFile);
        if (fs.existsSync(moduleDataFile)) {
            const dataData = fs.readFileSync(moduleDataFile, config.encoding);
            fs.writeFileSync(
                path.join(moduleSrcPath, config.moduleDataFile),
                dataData,
                config.encoding
            );
        }
    }
}

const finishTime = new Date();
console.log(`${config.formatTime(finishTime)} Finished 'add-module' after ${finishTime.getTime() - taskStartTime.getTime()} ms`);