'use strict';

//Встраивает функции require в html-код для extract-loader

exports.default = source => {
    const result = source.replace(/\${require\(`?([^)]*)`\)}/g, (...match) => {
        return `" + require(${JSON.stringify(match[1])}) + "`;
    });

    return result;
};