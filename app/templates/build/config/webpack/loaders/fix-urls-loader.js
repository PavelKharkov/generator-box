'use strict';

//Сбрасывает все не-абсолютные пути в css-файлах

exports.default = function(source, sourceMap, meta) {
    const result = source.replace(/url\((["'](?!data:)[^~]|(?!data:)[^"'~]).*\)/g, 'url(\'~\')');
    this.callback(null, result, sourceMap, meta);
};