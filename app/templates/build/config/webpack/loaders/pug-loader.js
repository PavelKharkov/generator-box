'use strict';

//Компилирует pug-файлы в html-код

const pug = require('pug');

exports.default = function(source) {
    const defaultOptions = {
        filename: this.resourcePath,
        doctype: 'html'
    };
    const options = {
        ...defaultOptions,
        ...this.query
    };

    const template = pug.compile(source, options);
    template.dependencies.forEach(dependency => {
        return this.addDependency(dependency);
    });

    const data = options.data || {};
    const renderedTemplate = template(data);
    return renderedTemplate;
};