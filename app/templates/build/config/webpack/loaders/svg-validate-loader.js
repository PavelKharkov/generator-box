'use strict';

//Валидирует svg-файлы

const path = require('node:path');

exports.default = function(source) {
    if ((path.extname(this.resourcePath) === '.svg') && !source.startsWith('<?xml ')) {
        throw new Error(`Incorrect svg file at ${this.resourcePath}. Use Adobe Illustrator to correctly save it.`);
    }

    return source;
};