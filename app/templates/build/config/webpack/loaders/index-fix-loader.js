'use strict';

//Поправляет пути к ресурсам в шаблоне главной страницы

const path = require('node:path');

exports.default = function(source) {
    const pathArray = this.resourcePath.split(path.sep);
    let result = source;

    if (pathArray[pathArray.length - 2] === 'index') {
        result = result
            //меняет пути в html-атрибутах
            .replace(new RegExp('="(..)?/assets/', 'g'), '="assets/')
            //меняет пути в атрибутах style
            .replace(new RegExp('\\(../assets/', 'g'), '(assets/')
            .replace(new RegExp('\\(\'../assets/', 'g'), '(\'assets/')
            .replace(new RegExp('\\(&quot;../assets/', 'g'), '(&quot;assets/');
    }

    return result;
};