'use strict';

//Добавляет в начало файлов страниц переменную с названием файла

const path = require('node:path');

exports.default = function(source) {
    const pathArray = this.resourcePath.split(path.sep);
    const result = `- const filename = '${pathArray[pathArray.length - 2]}';\n${source}`;

    return result;
};