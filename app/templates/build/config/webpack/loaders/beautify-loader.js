'use strict';

//Форматирует получившийся html-код

const path = require('node:path');
const beautify = require('js-beautify').html;

const config = require(path.join(global.templatesPath, 'build', 'config', 'common', 'config.js'));

exports.default = source => {
    const result = beautify(source, config.beautifyConfig);
    return result;
};