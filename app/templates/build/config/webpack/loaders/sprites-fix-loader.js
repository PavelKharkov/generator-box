'use strict';

//Меняет пути к файлам спрайтов на идентификатор спрайта

const path = require('node:path');

const config = require(path.join(global.templatesPath, 'build', 'config', 'common', 'config.js'));

const hashSymbol = '#';

exports.default = source => {
    //Ищет тег use с атрибутом require внутри тегов svg
    const result = source.replace(/(<use require="" xlink:href=")([^"]*)/g, (match, part1, part2) => {
        const spritePath = path.posix.join('..', config.paths.distAssetsName, config.paths.mediaName, config.spriteFile);
        const spriteId = part2.includes(hashSymbol)
            ? part2.slice(part2.indexOf(hashSymbol) + 1)
            : `sprite-${part2.slice(part2.lastIndexOf('\\') + 1, -4)}`;
        return part1 + spritePath + hashSymbol + spriteId;
    });

    return result;
};