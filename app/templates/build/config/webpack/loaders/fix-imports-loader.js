'use strict';

//Удаляет импорты из вендорных модулей, которые должны подключаться в отдельном файле
exports.default = source => {
    //Замена импортов jQuery на глобальную переменную
    const result = source.replace(
        /require\(('jquery'|"jquery"| "jquery" )\)/,
        'window.jQuery'
    );

    return result;
};