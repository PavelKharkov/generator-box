'use strict';

const fs = require('node:fs');
const path = require('node:path');

const rimraf = require('rimraf');
const webpackBundleAnalyzer = require('webpack-bundle-analyzer');
const favicons = require('favicons');

const templatesPath = global.templatesPath;
const distPath = global.distPath;
const buildPath = path.join(templatesPath, 'build');
const commonConfigPath = path.join(buildPath, 'config', 'common');
if (!fs.existsSync(commonConfigPath)) fs.mkdirSync(commonConfigPath);
const config = require(path.join(commonConfigPath, 'config.js'));
const serverConfig = require(path.join(commonConfigPath, 'server.js'));
const sortStyles = require(path.join(buildPath, 'tasks', 'sort-styles.js'));

const webpackConfigDir = path.join(buildPath, 'config', 'webpack');
const webpackConfig = require(path.join(webpackConfigDir, 'config.js'));

const nodeModulesPath = path.join(templatesPath, 'node_modules');
const faviconsConfigPath = path.join(nodeModulesPath, 'favicons', 'dist', 'config', 'icons.json');
const faviconsConfig = require(faviconsConfigPath);

const faviconsFilesPath = path.join(nodeModulesPath, 'favicons', 'dist', 'config', 'files.json');
const faviconsFilesConfig = require(faviconsFilesPath);

const faviconPath = path.join(templatesPath, config.paths.icons);
const svgFavicon = path.join(faviconPath, 'favicon.svg');
const isSvgFavicon = fs.existsSync(svgFavicon);
const faviconFile = isSvgFavicon ? svgFavicon : path.join(faviconPath, 'favicon.png');

const scssStringIndent = '    ';

//Функция для перемещения файлов рекурсивно из одной папки в другую
const yieldFilesMove = (currentDir, inputPath, outputPath) => {
    const dirPath = path.join(inputPath, currentDir);
    fs.readdirSync(dirPath).forEach(file => {
        const filePath = path.join(dirPath, file);
        if (fs.lstatSync(filePath).isDirectory()) {
            yieldFilesMove(path.join(currentDir, file), inputPath, outputPath);
            fs.rmdirSync(filePath);
            return;
        }

        fs.renameSync(filePath, path.join(outputPath, currentDir, file));
    });
};

const callbacks = {
    //Общая прединициализация вебпака
    preinit(callback) {
        const taskStartTime = new Date(); //Время начала запуска скрипта
        console.log(`${config.formatTime(taskStartTime)} Starting 'webpack'...`);

        //Выводить сообщение при завершении работы через терминал
        process.on('SIGINT', () => {
            const prefix = config.formatTime(new Date());
            console.log(`${prefix} Webpack stopped`); //Показывать, в какой момент была завершена работа
            process.exit(0); /* eslint-disable-line no-process-exit */
        });

        //Проверка наличия папки с конфигурацией приложения
        const baseConfigDir = path.join(templatesPath, config.paths.baseConfig);
        if (!fs.existsSync(baseConfigDir)) fs.mkdirSync(baseConfigDir);

        //Сохранение глобальных пользовательских настроек для стилей
        const globalSettings = JSON.parse(fs.readFileSync(path.join(commonConfigPath, `${config.globalSettingsFilename}.json`), config.encoding));
        const settingsArray = Object.keys(globalSettings);
        const stylesDataStart = [
            '//stylelint-disable',
            '$global-settings: (\n'
        ];
        const stylesData = `${settingsArray.reduce((accumulator, key, index) => {
            const propName = accumulator + scssStringIndent + key;
            const propValue = globalSettings[key] + ((index < (settingsArray.length - 1)) ? ',' : '');
            return `${propName}: ${propValue}\n`;
        }, stylesDataStart.join('\n'))});`;

        //Создание файла с глобальными пользовательскими настройками для стилей
        const globalSettingsStylesFile = path.join(baseConfigDir, `${config.globalSettingsFilename}.${config.devStyleExt}`);
        fs.writeFileSync(globalSettingsStylesFile, stylesData, config.encoding);

        //Определение хоста и порта перед запуском сервера
        if (webpackConfig.mode === 'development') {
            serverConfig.getHost(host => {
                serverConfig.getPort(host, port => {
                    webpackConfig.devServer.host = host;
                    webpackConfig.devServer.port = port;

                    callback(taskStartTime);
                });
            });
            return;
        }

        callback(taskStartTime);
    },

    //Прединициализация вебпака для разработки
    developmentPreinit() {
        const webpackStartTime = new Date();
        console.log(`${config.formatTime(webpackStartTime)} Starting webpack...`);
    },

    //Настройка слушателей после инициалзации вебпака для разработки
    developmentCallback(compiler, server, taskStartTime) {
        const protocol = `http${webpackConfig.devServer.https ? 's' : ''}:`;
        const addressHost = `//${webpackConfig.devServer.host}:${webpackConfig.devServer.port}`;
        const serverAddress = `[${protocol + addressHost}]`;

        server.startCallback(() => {
            const finishTime = new Date();
            console.log(`${config.formatTime(finishTime)} Finished 'webpack' after ${finishTime.getTime() - taskStartTime.getTime()} ms`);
        });

        compiler.hooks.watchRun.tap('watchRun', () => {
            const prefix = `${config.formatTime(new Date())} ${serverAddress}`;
            console.log(`${prefix} Compiling...`);
        });
        compiler.hooks.done.tap('done', stats => {
            const {startTime, endTime: finishTime} = stats;
            const prefix = `${config.formatTime(new Date())} ${serverAddress}`;
            console.log(`${prefix} Compilation finished after ${finishTime - startTime} ms`); //Показывать, сколько времени длилась компиляция
        });
    },

    //Прединициализация вебпака для релиза
    productionPreinit(callback) {
        //Очистка временной директории
        rimraf.sync(config.paths.temp, {
            glob: false
        });

        //Настройка плагина анализатора с уникальным хостом и портом
        webpackConfig.plugins.push(new webpackBundleAnalyzer.BundleAnalyzerPlugin({
            analyzerHost: webpackConfig.devServer.host,
            analyzerPort: webpackConfig.devServer.port
        }));

        //Сортировка стилей в исходниках перед компиляцией
        sortStyles(() => {
            callback();
        });
    },

    //Настройка слушателей после инициалзации вебпака для релиза
    productionCallback(compiler) {
        compiler.hooks.done.tapAsync('done', stats => {
            if (stats.hasErrors()) {
                stats.compilation.errors.forEach(error => {
                    console.log(error);
                });
            }

            let isFinished = true;
            const logWebpackFinish = () => {
                const startTime = stats.startTime;
                const finishTime = stats.endTime;
                console.log(`${config.formatTime(new Date())} Finished 'webpack' after ${finishTime - startTime} ms`);
            };

            //Перемещение файлов в корневую папку
            const yieldFilesToRoot = () => {
                const rootPath = path.join(global.templatesPath, config.frontendPath.replace(/\w+\b/g, '..'));

                yieldFilesMove('/', distPath, rootPath);
                fs.rmdirSync(distPath);

                logWebpackFinish();
            };

            //Генерация файлов фавиконок
            if (fs.existsSync(faviconFile)) {
                isFinished = false;

                const taskStartTime = new Date();
                console.log(`${config.formatTime(taskStartTime)} Starting favicons...`);

                const iconsPath = path.join(config.paths.distAssetsName, config.paths.mediaName, config.paths.iconsName);
                const distMediaPath = path.join(distPath, config.paths.distAssetsName, config.paths.mediaName);
                if (!fs.existsSync(distMediaPath)) fs.mkdirSync(distMediaPath);
                const distIconsPath = path.join(distMediaPath, config.paths.iconsName);
                if (!fs.existsSync(distIconsPath)) fs.mkdirSync(distIconsPath);
                const iconsManifestPath = path.join(config.frontendPath, config.staticPath, iconsPath);

                //Корректировка конфигурации favicons
                const faviconFilename = 'favicon.ico';
                const faviconsFaviconsConfig = faviconsConfig.favicons;
                const faviconsEntries = Object.entries(faviconsFaviconsConfig).filter(item => {
                    return item[0] === faviconFilename;
                });
                faviconsConfig.favicons = Object.fromEntries(faviconsEntries);

                const faviconsIcoConfig = faviconsFaviconsConfig[faviconFilename];
                const faviconsSizes = faviconsIcoConfig.sizes;
                faviconsIcoConfig.sizes = faviconsSizes.filter(favicon => {
                    return favicon.width === 32;
                });

                const includedAndroidIcons = new Set(['android-chrome-192x192.png', 'android-chrome-512x512.png']);
                const faviconsAndroidConfig = faviconsConfig.android;
                const faviconsAndroidEntries = Object.entries(faviconsAndroidConfig).filter(item => {
                    return includedAndroidIcons.has(item[0]);
                });
                faviconsConfig.android = Object.fromEntries(faviconsAndroidEntries);

                const faviconsAppleConfig = faviconsConfig.appleIcon;
                const faviconsAppleEntries = Object.entries(faviconsAppleConfig).filter(item => {
                    return item[0] === 'apple-touch-icon-180x180.png';
                });
                faviconsConfig.appleIcon = Object.fromEntries(faviconsAppleEntries);

                delete faviconsConfig.appleStartup;
                delete faviconsConfig.coast;
                delete faviconsConfig.firefox;
                delete faviconsConfig.windows;
                delete faviconsConfig.yandex;

                fs.writeFileSync(faviconsConfigPath, JSON.stringify(faviconsConfig, null, 2), config.encoding);

                //Корректировка содержимого файла manifest
                const manifestConfig = faviconsFilesConfig.android['manifest.json'];
                const manifestIconsConfig = manifestConfig.icons;
                manifestConfig.icons = manifestIconsConfig.filter(item => {
                    return includedAndroidIcons.has(item.src);
                });

                delete faviconsFilesConfig.firefox;
                delete faviconsFilesConfig.windows;
                delete faviconsFilesConfig.yandex;

                fs.writeFileSync(faviconsFilesPath, JSON.stringify(faviconsFilesConfig, null, 2), config.encoding);

                favicons(
                    faviconFile,
                    /* eslint-disable camelcase */
                    {
                        path: iconsManifestPath,
                        appName: config.projectName,
                        appDescription: config.projectDescription,
                        developerName: config.developerName,
                        developerURL: config.developerSite,
                        background: config.projectThemeColor,
                        theme_color: config.projectThemeColor,
                        lang: `${config.defaultLanguage}-${config.defaultRegion}`,
                        display: 'browser',
                        orientation: 'portrait',
                        start_url: '.',
                        version: '1.0',
                        icons: {
                            android: true, //Создает manifest.json
                            appleIcon: true, //Создает большие .png изображения
                            appleStartup: false,
                            coast: false,
                            favicons: true, //Создает .ico файл
                            firefox: false,
                            windows: false,
                            yandex: false
                        }
                    },
                    /* eslint-enable camelcase */
                    (error, response) => {
                        if (error) {
                            console.log(error.message);
                            return;
                        }

                        response.files.forEach(file => {
                            fs.writeFileSync(path.join(distIconsPath, file.name), file.contents, config.encoding);
                        });
                        response.images.forEach(file => {
                            fs.writeFileSync(path.join(distIconsPath, file.name), file.contents, config.encoding);
                        });

                        //Добавление файла favicon.ico в корневую папку
                        const faviconItem = response.images.find(item => {
                            return item.name === faviconFilename;
                        });
                        fs.writeFileSync(path.join(distPath, faviconItem.name), faviconItem.contents, config.encoding);

                        const finishTime = new Date();
                        const taskDuration = finishTime.getTime() - taskStartTime.getTime();
                        console.log(`${config.formatTime(finishTime)} Finished 'favicons' after ${taskDuration} ms`);

                        if (config.shouldMoveToRoot) {
                            yieldFilesToRoot();
                            return;
                        }

                        logWebpackFinish();
                    }
                );
            }

            if (!isFinished) return;

            if (config.shouldMoveToRoot) {
                yieldFilesToRoot();
                return;
            }

            logWebpackFinish();
        });
    }
};

module.exports = callbacks;