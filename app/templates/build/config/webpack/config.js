'use strict';

//Переменные окружения
/* eslint-disable no-process-env */
const isDist = process.env.NODE_ENV === 'production';
/* eslint-enable no-process-env */

const fs = require('node:fs');
const path = require('node:path');

const webpack = require('webpack');

const rimraf = require('rimraf');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const SpritesmithPlugin = require('webpack-spritesmith');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

const templatesPath = global.templatesPath;
const commonConfigPath = path.join(templatesPath, 'build', 'config', 'common');
const config = require(path.join(commonConfigPath, 'config.js'));
const pugLocals = require(path.join(commonConfigPath, 'pug.js'));
const nodeModulesPath = path.join(templatesPath, 'node_modules');
const webpackConfigPath = path.join(templatesPath, config.paths.buildConfig, 'webpack');
const loadersPath = path.join(webpackConfigPath, 'loaders');
const globalSettings = require(path.join(commonConfigPath, `${config.globalSettingsFilename}.json`));

//Плагины для лоадеров
const sortMediaQueries = require('postcss-sort-media-queries');
const autoprefixer = require('autoprefixer');
const postcssFlexbugsFixes = require('postcss-flexbugs-fixes');

const hasPolyfills = config.hasPolyfills;
const isDebug = config.isDebug === 'true';
const isSyncModules = config.isSyncModules === 'true';
const hasVue = config.hasVue;

const VueLoaderPlugin = hasVue && require('vue-loader/lib/plugin'); /* eslint-disable-line global-require */

const pathPosix = path.posix;

//Правка путей для записи релизных файлов в родительскую папку проекта
if ((config.staticPath !== config.paths.dist) && isDist) {
    config.paths.dist = config.staticPath;
    if (!fs.existsSync(config.paths.dist)) {
        fs.mkdirSync(config.paths.dist);
    }
}
global.distPath = config.paths.dist; //Сохранение текущей релизной директории для коллбеков webpack

//Очистка директории релиза перед компиляцией
if (!config.shouldMoveToRoot) {
    rimraf.sync(config.paths.dist, {
        glob: false
    });
}

let shouldShowProgress = true;
const isProgressPluginUsed = true;

const currentDate = new Date();
const currentTime = currentDate.getTime();

const devServerConfig = isDist
    ? {}
    : {
        allowedHosts: 'all',
        client: {
            logging: 'warn',
            overlay: false
        },
        devMiddleware: {
            stats: 'errors-only',
            publicPath: '/'
        },
        hot: 'only',
        https: false,
        liveReload: false,
        open: {
            app: {
                name: 'chrome'
            },
            target: ['sitemap']
        },
        static: {
            directory: path.join(templatesPath, config.paths.dist),
            watch: {
                ignored: [
                    '**/tests/**/*.test.js', //игнорировать файлы тестов
                    '**/node_modules'
                ]
            }
        }
    };

const webpackConfig = {
    mode: isDist ? 'production' : 'development',

    entry: (() => {
        const entry = {
            main: path.join(templatesPath, config.paths.src, config.moduleScriptFile),
            vendor: path.join(templatesPath, config.paths.src, 'vendor.js'),
            pages: path.join(templatesPath, config.paths.src, 'pages.js')
        };
        if (hasPolyfills) {
            entry.polyfills = path.join(templatesPath, config.paths.src, 'polyfills.js');
        }
        if (!isSyncModules) {
            entry.prefetch = path.join(templatesPath, config.paths.src, 'prefetch.js');
        }
        return entry;
    })(),

    output: {
        path: path.join(templatesPath, config.paths.dist),
        filename: path.join(config.paths.distAssetsName, config.paths.scriptsDirName, `[name].${config.distScriptFile}`),
        chunkFilename: path.join(config.paths.distAssetsName, config.paths.scriptsDirName, `[name].${config.distScriptFile}?${currentTime}`),
        publicPath: '../../' //путь от генерируемого js-файла до корня публичных файлов
    },

    module: {
        parser: {
            javascript: {
                amd: false
            }
        },

        rules: (() => {
            const regexpSep = '[/\\\\]';
            const rules = [];

            //Правила для файлов стилей
            const postcssPlugins = [
                sortMediaQueries, //Объединяет и сортирует media-правила

                autoprefixer({ //Добавляет браузерные префиксы
                    cascade: false
                }),

                postcssFlexbugsFixes //Чинит flexbox-баги в разных браузерах
            ];

            const styleUse = [
                MiniCssExtractPlugin.loader,
                {
                    loader: path.join(nodeModulesPath, 'css-loader'),
                    options: {
                        url: {
                            filter(url) {
                                return (url !== '~'); //Пропускать url('~')
                            }
                        },
                        sourceMap: !isDist,
                        esModule: false
                    }
                },
                path.join(loadersPath, 'fix-urls-loader.js'), //сбрасывает все не-абсолютные пути в css-файлах
                {
                    loader: path.join(nodeModulesPath, 'postcss-loader'),
                    options: {
                        postcssOptions: {
                            ident: 'postcss',
                            plugins: postcssPlugins
                        },
                        sourceMap: !isDist
                    }
                }
            ];
            styleUse.push({
                loader: path.join(nodeModulesPath, 'sass-loader'),
                options: {
                    additionalData: `$dist: ${isDist};\n`,
                    sassOptions: {
                        includePaths: [path.join(templatesPath, config.paths.baseStyles)],
                        outputStyle: 'expanded'
                    },
                    sourceMap: !isDist
                }
            });
            rules.push({
                test: /\.(s?css)$/,
                use: styleUse
            });

            //Правила для html-шаблонов
            Object.assign(pugLocals.globalConfig, {
                isDist,
                hasPolyfills,
                compilationTime: currentTime,
                templatesRoot: templatesPath,
                settings: globalSettings
            });
            const templatesRulesUseCommon = [];
            if (isDist) {
                templatesRulesUseCommon.push(path.join(loadersPath, 'beautify-loader.js')); //форматирует получившийся html-код
            }
            templatesRulesUseCommon.push(
                path.join(loadersPath, 'index-fix-loader.js'), //поправляет пути к ресурсам в шаблоне главной страницы
                path.join(loadersPath, 'sprites-fix-loader.js'), //меняет пути к файлам спрайтов на идентификатор спрайта
                {
                    loader: path.join(nodeModulesPath, 'extract-loader'),
                    options: {
                        publicPath: '../'
                    }
                },
                path.join(loadersPath, 'interpolate-requires-loader.js'), //встраивает функции require в html-код для extract-loader
                {
                    loader: path.join(nodeModulesPath, 'html-loader'),
                    options: {
                        sources: false,
                        minimize: false
                    }
                },
                {
                    loader: path.join(loadersPath, 'pug-loader.js'), //компилирует pug-файлы в html-код
                    options: {
                        basedir: path.join(templatesPath, config.paths.src),
                        data: pugLocals
                    }
                },
                path.join(loadersPath, 'pug-filename-loader.js') //добавляет в начало файлов страниц переменную с названием файла
            );

            //Правила для шаблонов страниц
            const pugFormat = '.pug';
            const pagesRulesUse = [
                {
                    loader: path.join(nodeModulesPath, 'file-loader'),
                    options: {
                        name(file) {
                            const dirname = path.dirname(file);
                            const parentDir = path.basename(dirname);
                            return pathPosix.join(
                                config.paths.distTemplatesName,
                                ((parentDir === 'index') ? '' : parentDir),
                                config.distTemplateFile
                            );
                        }
                    }
                },
                ...templatesRulesUseCommon
            ];
            rules.push({
                test: new RegExp(`${regexpSep + config.paths.pagesName}(?:${regexpSep}.+){2}\\${pugFormat}$`),
                use: pagesRulesUse
            });

            if (hasVue) {
                //Правила для html-шаблонов
                const templatesRulesUse = [...templatesRulesUseCommon];
                rules.push({
                    test: new RegExp(`${regexpSep}(${config.paths.componentsName}|${config.paths.layoutName})(?:${regexpSep}.+){2}\\${pugFormat}$`),
                    use: templatesRulesUse
                });

                //Правила для vue-шаблонов
                const vueRulesUse = [
                    {
                        loader: path.join(nodeModulesPath, 'vue-loader'),
                        options: {}
                    }
                ];
                rules.push({
                    test: /\.vue$/,
                    use: vueRulesUse
                });
            }

            //Правила media-файлов
            const mediaUploadsInner = `(${config.paths.mediaName}|${config.paths.uploadsName})`;
            const regexpMediaUploads = regexpSep + mediaUploadsInner + regexpSep;
            const mediaDirsRegexp = new RegExp(`^${mediaUploadsInner}$`);
            const mediaImagesFormats = 'gif|png|jpe?g';

            const transformMediaFilesPath = file => {
                const cwdPath = path
                    .relative(global.templatesPath, file)
                    .split(path.sep)
                    .reverse();
                const mediaDirIndex = cwdPath.findIndex(item => {
                    return mediaDirsRegexp.test(item);
                });

                const mediaDir = cwdPath[mediaDirIndex];
                const moduleName = cwdPath[mediaDirIndex + 1];
                const innerFilePath = cwdPath.slice(0, mediaDirIndex).reverse();

                return pathPosix.join(mediaDir, moduleName, pathPosix.join(...innerFilePath));
            };

            const mediaFileLoader = {
                loader: path.join(nodeModulesPath, 'file-loader'),
                options: {
                    esModule: false,
                    name(file) {
                        const currentCompilationDate = config.formatTime(new Date());
                        if (isDist) console.log(`${currentCompilationDate} Media query: ${file}`);
                        return transformMediaFilesPath(file);
                    },
                    outputPath: config.paths.distAssetsName
                }
            };
            const mediaImageLoader = {
                loader: path.join(nodeModulesPath, 'image-webpack-loader'),
                options: {
                    disable: !isDist,
                    gifsicle: {
                        interlaced: false
                    },
                    mozjpeg: {
                        quality: 75
                    },
                    optipng: {
                        enabled: false
                    },
                    pngquant: {
                        speed: 1,
                        quality: [1, 1]
                    },
                    svgo: {
                        plugins: ['preset-default']
                    }
                }
            };
            const mediaUse = [mediaFileLoader];
            if (isDist) mediaUse.push(mediaImageLoader);
            rules.push({
                test: new RegExp(`${regexpMediaUploads}.+\\.(${mediaImagesFormats})$`),
                use: mediaUse
            });

            //Правила svg media-файлов
            const svgFormat = '.svg';
            const svgMediaUse = [mediaFileLoader];
            if (isDist) svgMediaUse.push(mediaImageLoader);
            svgMediaUse.push(path.join(loadersPath, 'svg-validate-loader.js')); //валидирует svg-файлы
            rules.push({
                test: new RegExp(`${regexpMediaUploads}.+\\${svgFormat}$`),
                use: svgMediaUse
            });

            //Правила файлов спрайтов
            const spritesUse = [
                {
                    loader: path.join(nodeModulesPath, 'svg-sprite-loader'),
                    options: {
                        extract: true,
                        esModule: false,
                        spriteFilename: config.spriteFile,
                        outputPath: `${path.join(config.paths.distAssetsName, config.paths.mediaName)}/`,
                        symbolId(file) {
                            return `sprite-${path.basename(file, svgFormat)}`;
                        }
                    }
                }
            ];
            if (isDist) {
                //Настройка оптимизации svg-файла спрайтов
                const spritesImageLoader = {
                    loader: path.join(nodeModulesPath, 'image-webpack-loader'),
                    options: {
                        disable: !isDist,
                        svgo: {
                            plugins: [
                                'preset-default',
                                {
                                    name: 'removeAttrs',
                                    params: {
                                        attrs: [
                                            'svg:style',
                                            'class',
                                            'fill',
                                            'id',
                                            'data-.*'
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                };
                spritesUse.push(spritesImageLoader);
            }
            spritesUse.push(path.join(loadersPath, 'svg-validate-loader.js')); //валидирует svg-файлы
            rules.push({
                test: new RegExp(`${regexpSep + config.paths.spritesName + regexpSep}.+\\${svgFormat}$`),
                use: spritesUse
            });

            //Правила файлов шрифтов
            const fontsFormats = 'ttf|eot|woff2?';
            /* eslint-disable unicorn/no-array-push-push */
            rules.push({
                test: new RegExp(`${regexpSep + config.paths.fontsName + regexpSep}.+\\.(${fontsFormats})$`),
                include: [path.join(templatesPath, config.paths.fonts)],
                use: [
                    {
                        loader: path.join(nodeModulesPath, 'file-loader'),
                        options: {
                            name(file) {
                                const currentCompilationDate = config.formatTime(new Date());
                                if (isDist) console.log(`${currentCompilationDate} Fonts query: ${file}`);
                                const fileDir = path.dirname(file);
                                const moduleName = path.basename(fileDir);
                                return pathPosix.join(config.paths.fontsName, moduleName, '[name].[ext]');
                            },
                            outputPath: config.paths.distAssetsName
                        }
                    }
                ]
            });

            //Правила других медиа-файлов. При необходимости нужно добавить форматы файлов в регулярное выражение
            const mediaOtherFormats = 'mp4|ogv|webm|mp3|wav|ogg';
            rules.push({
                test: new RegExp(`${regexpMediaUploads}.+\\.(${mediaOtherFormats})$`),
                use: [
                    {
                        loader: path.join(nodeModulesPath, 'file-loader'),
                        options: {
                            esModule: false,
                            name(file) {
                                const currentCompilationDate = config.formatTime(new Date());
                                if (isDist) console.log(`${currentCompilationDate} Media query: ${file}`);
                                return transformMediaFilesPath(file);
                            },
                            outputPath: config.paths.distAssetsName
                        }
                    }
                ]
            });

            //Правила корневых файлов
            rules.push({
                test: new RegExp(`${regexpSep + config.paths.rootName + regexpSep}.+`),
                use: [
                    {
                        loader: path.join(nodeModulesPath, 'file-loader'),
                        options: {
                            name(file) {
                                const currentCompilationDate = config.formatTime(new Date());
                                if (isDist) console.log(`${currentCompilationDate} Root files query: ${file}`);
                                return '[name].[ext]';
                            }
                        }
                    }
                ]
            });

            //Правила файлов для аякса
            const htmlFormat = '.html';
            rules.push({
                test: new RegExp(`${regexpSep + config.paths.ajaxName + regexpSep}.+\\${htmlFormat}$`),
                use: [
                    {
                        loader: path.join(nodeModulesPath, 'file-loader'),
                        options: {
                            esModule: false,
                            name(file) {
                                const currentCompilationDate = config.formatTime(new Date());
                                if (isDist) console.log(`${currentCompilationDate} Ajax files query: ${file}`);
                                return pathPosix.join(config.paths.ajaxName, '[name].[ext]');
                            },
                            outputPath: config.paths.distAssetsName
                        }
                    }
                ]
            });

            /* eslint-enable unicorn/no-array-push-push */

            //Правила файлов скриптов
            const scriptsRules = {
                test: /\.js$/,
                use: [path.join(loadersPath, 'fix-imports-loader.js')] //удаляет импорты из вендорных модулей, которые должны подключаться в отдельном файле
            };
            if (hasPolyfills) {
                scriptsRules.use.push({
                    loader: path.join(nodeModulesPath, 'babel-loader'),
                    options: {
                        presets: ['@babel/preset-env'],
                        cacheDirectory: true
                    }
                });
            }
            rules.push(scriptsRules);

            return rules;
        })()
    },

    resolve: {
        alias: {
            Src: path.join(templatesPath, config.paths.src),
            Base: path.join(templatesPath, config.paths.base),
            Components: path.join(templatesPath, config.paths.components),
            Data: path.join(templatesPath, config.paths.data),
            Fonts: path.join(templatesPath, config.paths.fonts),
            Layout: path.join(templatesPath, config.paths.layout),
            Libs: path.join(templatesPath, config.paths.libs),
            Pages: path.join(templatesPath, config.paths.pages),
            Sprites: path.join(templatesPath, config.paths.sprites)
        },
        modules: [nodeModulesPath],
        symlinks: false
    },

    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true /* eslint-disable-line camelcase */
                    }
                }
            }),
            new CssMinimizerPlugin()
        ],
        splitChunks: {
            cacheGroups: {
                defaultVendors: false,
                default: false
            }
        }
    },

    plugins: (() => {
        const plugins = [];
        if (hasVue) {
            plugins.push(new VueLoaderPlugin());
        }

        if (isProgressPluginUsed) {
            const progressPlugin = new webpack.ProgressPlugin((percentage, message, ...args) => { //Показывать прогресс компиляции
                const currentProgressTime = config.formatTime(new Date());

                if (shouldShowProgress) {
                    const progressInfo = `${Math.floor(percentage * 100)}% — ${message} (${[...args].join(', ')})`;
                    console.log(`${currentProgressTime} ${progressInfo}`);
                }
                if (!isDist && (percentage === 1)) {
                    if (shouldShowProgress) {
                        const protocol = `http${devServerConfig.https ? 's' : ''}:`;
                        const addressHost = `//${devServerConfig.host}:${devServerConfig.port}/`;
                        const addressTarget = devServerConfig.open[0].target;
                        const serverAddress = protocol + addressHost + addressTarget;
                        console.log(`${currentProgressTime} Starting server at ${serverAddress}`);
                    }

                    shouldShowProgress = false;
                }
            });

            plugins.push(progressPlugin);
        }

        plugins.push(
            //Игнорировать зависимость moment.js в плагине Pikaday
            new webpack.IgnorePlugin({
                resourceRegExp: /moment$/,
                contextRegExp: /pikaday$/
            }),

            new webpack.DefinePlugin({
                __GLOBAL_SETTINGS__: globalSettings, //Глобальные пользовательские настройки сайта
                __IS_BUILDER__: JSON.stringify(false), //Переменная, указывающая на то, что сайт запущен на сервере-конструкторе
                __IS_ENABLED_MODULE__: JSON.stringify(true), //Переменная для включённых модулей
                __IS_DISABLED_MODULE__: JSON.stringify(false), //Переменная для отключённых модулей
                __IS_DIST__: JSON.stringify(isDist), //Запущена ли компиляция в режиме для релиза
                __HAS_POLYFILLS__: JSON.stringify(hasPolyfills), //Подключать ли полифилы при транспилинге скриптов
                __IS_DEBUG__: JSON.stringify(isDebug), //Компилировать ли скрипты режиме отладки
                __IS_SYNC__: JSON.stringify(isSyncModules), //Загружать ли модули синхронно в режиме для релиза
                __LANG__: JSON.stringify(config.defaultLanguage), //Язык приложения по умолчанию
                __REGION__: JSON.stringify(config.defaultRegion), //Регион приложения по умолчанию
                __COMPILATION_TIME__: JSON.stringify(currentTime), //Время начала компиляции
                __HAS_VUE__: JSON.stringify(hasVue) //Используется ли библиотка Vue в проекте
            }),

            new SpriteLoaderPlugin({
                plainSprite: true
            }),

            new MiniCssExtractPlugin({
                filename: path.join(config.paths.distAssetsName, config.paths.stylesDirName, `[name].${config.distStyleFile}`),
                chunkFilename: path.join(config.paths.distAssetsName, config.paths.stylesDirName, `[name].${config.distStyleFile}?${currentTime}`),
                linkType: false,
                /* eslint-disable no-var, object-shorthand */
                insert: function(linkTag) {
                    var chunk = linkTag.getAttribute('href').split('\\')[2].split('.')[0];
                    var chunkEl = document.querySelector(`[data-chunk="${chunk}"]`);
                    if (chunkEl) {
                        chunkEl.append(linkTag);
                        return;
                    }
                    document.head.append(linkTag);
                }
                /* eslint-enable no-undef, no-var, object-shorthand */
            })
        );

        //Подключать плагин для png-спрайтов при наличии файлов png-спрайтов
        const spritesDir = path.join(templatesPath, config.paths.sprites);
        const baseConfig = path.join(templatesPath, config.paths.baseConfig);
        if (!fs.existsSync(baseConfig)) fs.mkdirSync(baseConfig);
        const spritePngConfigFile = path.join(baseConfig, `sprite-png-config.${config.devStyleExt}`);
        const hasPingFiles = fs.readdirSync(spritesDir).some(file => {
            return path.extname(file) === '.png';
        });
        const stylelintDisableComment = '//stylelint-disable';
        if (hasPingFiles) {
            plugins.push(new SpritesmithPlugin({
                src: {
                    cwd: spritesDir,
                    glob: '**/*.png'
                },
                target: {
                    image: path.join(templatesPath, config.paths.distMedia, config.spritePngFile),
                    css: [
                        [
                            spritePngConfigFile,
                            {format: 'customTemplate'}
                        ]
                    ]
                },
                apiOptions: {
                    cssImageRef: config.spritePngFile
                },
                spritesmithOptions: {
                    padding: 2
                },
                customTemplates: {
                    customTemplate({sprites, spritesheet}) {
                        const spritesString = sprites.map(sprite => {
                            const variableName = `sprite-${sprite.name}`;
                            const spriteCoords = `${sprite.x}px, ${sprite.y}px, ${sprite.offset_x}px, ${sprite.offset_y}px`;
                            const spriteSizes = `${sprite.width}px, ${sprite.height}px, ${sprite.total_width}px, ${sprite.total_height}px`;
                            const line = [
                                `$${variableName}:`,
                                `(${spriteCoords},`,
                                `${spriteSizes},`,
                                `'${sprite.image}',`,
                                `'${variableName}');`
                            ];
                            return line.join(' ');
                        }).join('\n');

                        const dataString = [
                            stylelintDisableComment,
                            spritesString,
                            `$spritesheet: (${spritesheet.width}px, ${spritesheet.height}px);\n`
                        ];
                        return dataString.join('\n');
                    }
                }
            }));
        } else {
            fs.writeFileSync(spritePngConfigFile, stylelintDisableComment, config.encoding);
        }

        if (isDist) {
            if (isSyncModules) {
                plugins.push(new webpack.optimize.LimitChunkCountPlugin({
                    maxChunks: 1
                }));
            }
        }

        return plugins;
    })(),

    performance: {
        hints: false
    },

    infrastructureLogging: {
        level: 'warn'
    },
    stats: 'errors-only',

    devtool: isDist ? config.distSourceMaps : config.devSourceMaps,
    bail: isDist
};

webpackConfig.devServer = devServerConfig;

if (!isProgressPluginUsed) {
    const currentProgressTime = config.formatTime(currentDate);
    setTimeout(() => {
        const protocol = `http${devServerConfig.https ? 's' : ''}:`;
        const addressHost = `//${devServerConfig.host}:${devServerConfig.port}/`;
        const addressTarget = devServerConfig.open[0].target;
        const serverAddress = protocol + addressHost + addressTarget;
        console.log(`${currentProgressTime} Starting server at ${serverAddress}`);
    }, 0);
}

module.exports = webpackConfig;