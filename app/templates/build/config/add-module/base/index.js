import './style.scss';

import {Module, immutable} from 'Components/module';
//import util from 'Layout/main';
import {addModule} from 'Base/scripts/app.js';

//Опции
const moduleName = 'moduleNameCamel';

const moduleNameCamelEl = document.querySelectorAll('.moduleNameKebab');

/**
 * @class AppmoduleNamePascal
 * @memberof dirName
 * @classdesc Описание модуля.
 * @desc Наследует: [Module]{@link app.Module}.
 * @param {HTMLElement} element - HTML-элемент экземпляра.
 * @param {app.Module.instanceOptions} [options] - Опции экземпляра.
 */
const AppmoduleNamePascal = immutable(class extends Module {
    constructor(el, opts, appname = moduleName) {
        super(el, opts, appname);

        Module.checkHTMLElement(el);

        //Код инициализации экземпляра
    }
});

addModule(moduleName, AppmoduleNamePascal);

moduleNameCamelEl.forEach(el => new AppmoduleNamePascal(el));

export default AppmoduleNamePascal;
export {AppmoduleNamePascal};