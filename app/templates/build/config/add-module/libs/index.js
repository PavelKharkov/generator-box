/**
 * @module moduleNameKebab
 * @memberof libs
 * @version 1.0.0
 * @desc [Сайт]{@link http://example.com/}. Экспортирует moduleNamePascal.
 * @ignore
 */

import moduleNamePascal from 'moduleNameKebab';

export default moduleNamePascal;