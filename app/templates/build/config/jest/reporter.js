'use strict';

const fs = require('node:fs');
const path = require('node:path');

const templatesPath = global.templatesPath;
const config = require(path.join(templatesPath, 'build', 'config', 'common', 'config.js'));

const testResultsDir = path.join(templatesPath, config.paths.tests);
if (!fs.existsSync(testResultsDir)) fs.mkdirSync(testResultsDir);

const ansiRegex = new RegExp([
    '[\\u001B\\u009B][[\\]()#;?]*(?:(?:(?:[a-zA-Z\\d]*(?:;[-a-zA-Z\\d\\/#&.:=?%@~_]*)*)?\\u0007)',
    '(?:(?:\\d{1,4}(?:;\\d{0,4})*)?[\\dA-PR-TZcf-ntqry=><~]))'
].join('|'), 'g');
const removeAnsi = string => {
    return (typeof string === 'string') ? string.replace(ansiRegex, '') : string;
};

const htmlEscapes = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#39;'
};
const unescapedHtml = new RegExp('[&<>"\']', 'g');
const escapeHtml = string => {
    const formattedString = String(string);
    if (unescapedHtml.test(formattedString)) {
        return formattedString.replace(unescapedHtml, key => htmlEscapes[key]);
    }

    return formattedString;
};

const itemsProp = '_items';

const addTestResult = (prevAncestors, ancestor, testResult) => {
    if (!prevAncestors[ancestor]) {
        prevAncestors[ancestor] = {
            [itemsProp]: []
        };
    }

    if (testResult) {
        prevAncestors[ancestor][itemsProp].push({
            title: testResult.title,
            status: testResult.status,
            duration: testResult.duration
        });
    }
};

class CustomReporter {
    constructor(globalConfig, options) {
        this.globalConfig = globalConfig;
        this.options = options;
    }

    onRunComplete(contexts, results) {
        const black = '#2b2b2b';
        const white = '#bbb';
        const green = '#70ff70';
        const red = '#de4c36';
        const yellow = '#cdcd00';
        const magenta = '#ff70ff';
        const blue = '#589df6';

        /* eslint-disable max-len, vue/max-len */
        let content = `
        <!doctype html>
        <html lang="ru-RU">
        <head>
        <title>Результаты тестов</title>
        </head>

        <body style="background-color: ${black}; margin: 0; padding: 50px; color: ${white}; line-height: 1.5; font-size: 16px; font-family: 'Courier New', Courier, monospace;">
        `;

        const testDate = new Date(results.startTime)
            .toLocaleDateString('ru-RU', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
            })
            .replaceAll('-', '.');
        content += `
        <div>Дата проверки: <span>${testDate}</span></div>
        <div>Паттерн проверяемых файлов: <span>${path.join(this.globalConfig.testPathPattern)}</span></div>
        <div><a href="../coverage/lcov-report/index.html" target="_blank" style="color: ${blue}">Покрытие тестов</a></div>
        <br>
        <br>
        `;

        results.testResults.forEach(({
            numFailingTests,
            numPassingTests,
            testFilePath,
            failureMessage,
            testResults
        }) => {
            const resultsTree = {};

            const isFail = (numFailingTests > 0) || (numPassingTests === 0);
            const badgeHtml = `<span style="display: inline-block; padding: 1px 4px; background-color: ${isFail ? red : green}; color: ${black}">${isFail ? 'FAIL' : 'PASS'}</span>`;
            content += `
            <div>
                <div>${badgeHtml} <span>${path.relative(templatesPath, testFilePath)}</span></div>
            `;

            if (failureMessage) {
                content += `
                <br>
                <pre style="color: ${red}; padding-left: 20px; margin: 0; white-space: pre-wrap;">${escapeHtml(removeAnsi(failureMessage))}</pre>
                <br>
                `;
            }

            testResults.forEach(testResult => {
                testResult.ancestorTitles.reduce((accumulator, ancestor, ancestorIndex) => {
                    addTestResult(accumulator, ancestor, (ancestorIndex === (testResult.ancestorTitles.length - 1)) && testResult);
                    return accumulator[ancestor];
                }, resultsTree);
            });

            const renderResults = levelObject => {
                Object.keys(levelObject).forEach(resultLevel => {
                    if (resultLevel === '_items') return;

                    content += `
                    <div style="padding-left: 20px; margin-top: 5px;">
                        <div>${resultLevel}</div>
                        <div style="padding-left: 20px">
                    `;
                    levelObject[resultLevel][itemsProp].forEach(testResult => {
                        const iconHtml = (() => { /* eslint-disable-line max-nested-callbacks */
                            switch (testResult.status) {
                                case 'passed':
                                    return `<span style="color: ${green}">√</span>`;
                                case 'failed':
                                    return `<span style="color: ${red}">×</span>`;
                                case 'pending':
                                    return `<span style="color: ${yellow}">○</span>`;
                                case 'todo':
                                    return `<span style="color: ${magenta}">✎</span>`;
                                default:
                                    return '';
                            }
                        })();
                        content += `<div>${iconHtml} ${testResult.title} ${(testResult.duration > 0) ? `(${testResult.duration} мс.)` : ''}</div>`;
                    });
                    renderResults(levelObject[resultLevel]);

                    content += '</div></div>';
                });
            };
            renderResults(resultsTree);

            content += '</div><br>';
        });

        const skippedTestSuites = results.numPendingTestSuites ? `<span style="color: ${yellow}">${results.numPendingTestSuites} пропущено</span>` : '';
        const failedTestSuites = results.numFailedTestSuites ? `<span style="color: ${red}">${results.numFailedTestSuites} не пройдено</span>` : '';
        const passedTestSuites = results.numPassedTestSuites ? `<span style="color: ${green}">${results.numPassedTestSuites} пройдено</span>` : '';
        const totalTestSuites = `${results.numTotalTestSuites} всего`;

        const skippedTests = results.numPendingTests ? `<span style="color: ${yellow}">${results.numPendingTests} пропущено</span>` : '';
        const todoTests = results.numTodoTests ? `<span style="color: ${magenta}">${results.numTodoTests} отложено</span>` : '';
        const failedTests = results.numFailedTests ? `<span style="color: ${red}">${results.numFailedTests} не пройдено</span>` : '';
        const passedTests = results.numPassedTests ? `<span style="color: ${green}">${results.numPassedTests} пройдено</span>` : '';
        const totalTests = `${results.numTotalTests} всего`;

        const testTime = (new Date() - new Date(results.startTime)) / 1000;
        content += `
        <br>
        <div>Тестовые файлы:
        ${skippedTestSuites + (skippedTestSuites ? ', ' : '')}
        ${failedTestSuites + (failedTestSuites ? ', ' : '')}
        ${passedTestSuites + (passedTestSuites ? ', ' : '') + totalTestSuites}</div>
        <div>Тесты:
        ${skippedTests + (skippedTests ? ', ' : '')}
        ${todoTests + (todoTests ? ', ' : '')}
        ${failedTests + (failedTests ? ', ' : '')}
        ${passedTests + (passedTests ? ', ' : '') + totalTests}</div>
        <div>Время: <span style="color: ${yellow}">${testTime} сек.</span></div>

        </body>
        </html>
        `;
        /* eslint-enable max-len, vue/max-len */

        fs.writeFileSync(path.join(testResultsDir, 'results.html'), content, config.encoding);
    }

    getLastError() {
        return this._shouldFail && new Error('custom reporter reported an error'); /* eslint-disable-line no-underscore-dangle */
    }
}

module.exports = CustomReporter;