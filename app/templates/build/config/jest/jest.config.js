'use strict';

const path = require('node:path');

global.templatesPath = path.resolve();
const templatesPath = global.templatesPath;
const commonConfigPath = path.join(templatesPath, 'build', 'config', 'common');
const config = require(path.join(commonConfigPath, 'config.js'));
const globalSettings = require(path.join(commonConfigPath, `${config.globalSettingsFilename}.json`));

const rootDir = '<rootDir>';
const rootSrc = `${rootDir}/${config.paths.src}`;
const configDir = `${rootDir}/build/config/jest/`;
const mocksDir = `${configDir}mocks`;

const argv = process.argv;
const testFiles = argv[4] && !argv[4].startsWith('--') && argv[4];
const isCoverage = argv.includes('--coverage');

const currentDate = new Date();
const currentTime = currentDate.getTime();

const jestConfig = {
    collectCoverage: isCoverage, //Для определения покрытия тестов нужно запускать команду test с флагом --coverage
    collectCoverageFrom: [`${rootSrc}/**${(isCoverage && testFiles) ? `/${testFiles}` : ''}/*.js`],
    coveragePathIgnorePatterns: [
        `${rootSrc}/fonts/`,
        `${rootSrc}/libs/`,
        `${rootSrc}/pages/`,
        `${rootDir}/node_modules/`
    ],
    coverageReporters: ['lcov'],

    globals: {
        __GLOBAL_SETTINGS__: globalSettings,
        __IS_BUILDER__: false,
        __IS_ENABLED_MODULE__: true,
        __IS_DISABLED_MODULE__: false,
        __IS_DIST__: true,
        __HAS_POLYFILLS__: true,
        __IS_DEBUG__: true,
        __IS_SYNC__: true,
        __LANG__: config.defaultLanguage,
        __REGION__: config.defaultRegion,
        __COMPILATION_TIME__: JSON.stringify(currentTime),
        __HAS_VUE__: true,

        //Инициализация глобальной переменной приложения
        app: {
            globalConfig: {
                relPath: './',
                assetsPath: `./${config.paths.distAssetsName}/`
            }
        }
    },

    moduleDirectories: ['node_modules', rootDir],
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': `${mocksDir}/file-mock.js`,
        '\\.(scss|css)$': `${mocksDir}/style-mock.js`,

        '^Src(.*)$': `${rootSrc}$1`,
        '^Base(.*)$': `${rootSrc}/${config.paths.baseName}$1`,
        '^Components(.*)$': `${rootSrc}/${config.paths.componentsName}$1`,
        '^Data(.*)$': `${rootSrc}/${config.paths.dataName}$1`,
        '^Fonts(.*)$': `${rootSrc}/${config.paths.fontsName}$1`,
        '^Layout(.*)$': `${rootSrc}/${config.paths.layoutName}$1`,
        '^Libs(.*)$': `${rootSrc}/${config.paths.libsName}$1`,
        '^Pages(.*)$': `${rootSrc}/${config.paths.pagesName}$1`,
        '^Sprites(.*)$': `${rootSrc}/${config.paths.spritesName}$1`
    },
    transformIgnorePatterns: ['/node_modules/(?!@popperjs/core)'],

    reporters: [
        'default',
        `${configDir}reporter.js`
    ],
    rootDir: templatesPath,
    testEnvironment: 'jsdom',
    testEnvironmentOptions: {
        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36' //Chrome 96
    },

    verbose: true
};

module.exports = jestConfig;