'use strict';

//Настройка валидации опций
const optionsValidation = {
    styles: {
        'shared-color-primary': {
            validate: 'color'
        }
    }
};

module.exports = optionsValidation;