'use strict';

const path = require('node:path');

const express = require('express');
const apiRouter = require(path.join(global.serverPath, 'api'));
const distPath = path.resolve('dist');

//Настройка роутера
const router = express.Router();
router.use(express.static(distPath));

Object.keys(apiRouter).forEach(routePath => {
    const apiRoute = apiRouter[routePath];

    if (String(apiRoute) === '[object Object]') {
        const method = apiRoute.method || 'post';
        if (apiRoute.upload) { //При использовании плагина multer для обработки запросов в формате FormData
            router[method](routePath, apiRoute.upload, apiRoute.callback);
        } else {
            router[method](routePath, apiRoute.callback);
        }
        return;
    }

    router.post(routePath, apiRoute);
});

module.exports = router;