'use strict';

const path = require('node:path');

const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const rimraf = require('rimraf');

const webpackConfigDir = path.join(global.templatesPath, 'build', 'config', 'webpack');
const webpackConfig = require(path.join(webpackConfigDir, 'config.js'));
const webpackCallbacks = require(path.join(webpackConfigDir, 'callbacks.js'));

const configPath = path.join(global.appPath, 'config', 'common');
const config = require(path.join(configPath, 'config.js'));
const distPath = path.join(global.templatesPath, config.paths.dist);

const hmrPath = '/__webpack_hmr';
const host = 'localhost';

const preinit = callback => {
    webpackCallbacks.preinit(taskStartTime => {
        webpackConfig.devServer.host = host;
        webpackConfig.entry.main = [
            path.resolve('node_modules', `webpack-hot-middleware/client?noInfo=true&path=${hmrPath}`),
            webpackConfig.entry.main
        ];
        webpackConfig.infrastructureLogging = {
            level: 'error'
        };

        callback(taskStartTime);
    });

    return webpackConfig;
};

const init = app => {
    rimraf.sync(distPath, {
        glob: false
    });
    webpackCallbacks.developmentPreinit();

    const compiler = webpack(webpackConfig);
    app.use(webpackDevMiddleware(compiler, {
        publicPath: '/',
        writeToDisk: true
    }));
    app.use(webpackHotMiddleware(compiler, {
        path: hmrPath
    }));

    return compiler;
};

const postinit = (compiler, server, taskStartTime) => {
    webpackCallbacks.developmentCallback(compiler, server, taskStartTime);
};

module.exports = {
    preinit,
    init,
    postinit
};