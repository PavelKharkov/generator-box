'use strict';

const path = require('node:path');

const express = require('express');

const serverConfig = require(path.join(global.serverPath, 'config'));

const configPath = path.join(global.appPath, 'config', 'common');
const config = require(path.join(configPath, 'config.js'));
const distPath = path.join(global.templatesPath, config.paths.dist);

const init = port => {
    const app = express();

    app.set('port', port);

    app.use(express.json());
    app.use(express.urlencoded({
        extended: true
    }));
    app.use(serverConfig.router);

    return app;
};

const postinit = (app, server, taskStartTime) => {
    server.on('error', error => {
        throw error;
    });
    server.on('listening', () => {
        const finishTime = new Date();
        console.log(`${config.formatTime(finishTime)} Finished 'server' after ${finishTime.getTime() - taskStartTime.getTime()} ms`);
    });

    //Страница 404
    app.get('*', (request, response, next) => {
        const error = new Error(`${request.ip} tried to reach ${request.originalUrl}`);
        error.status = 404;
        error.shouldRedirect = true;
        next(error);
    });

    //Глобальный обработчик ошибок express
    app.use((error, request, response, next) => { /* eslint-disable-line no-unused-vars, max-params */
        if (request.originalUrl.endsWith('.html')) console.error(error.message);

        if (!error.status) {
            error.status = 500;
        }
        if (error.shouldRedirect) {
            response.sendFile(path.join(distPath, '404', 'index.html'));
            return;
        }

        response.status(error.status).send(error.message);
    });
};

module.exports = {
    init,
    postinit
};