'use strict';

/* eslint-disable no-process-env */
process.env.NODE_ENV = 'development';
/* eslint-enable no-process-env */

const http = require('node:http');
const path = require('node:path');

global.appPath = path.resolve('app');
global.templatesPath = path.join(global.appPath, 'templates');
global.serverPath = path.join(global.appPath, 'server');

const configPath = path.join(global.appPath, 'config', 'common');
const config = require(path.join(configPath, 'config.js'));

const webpackCallbacks = require(path.join(global.serverPath, 'webpack.js'));
const builderInit = require(path.join(global.serverPath, 'builder'));
const expressCallbacks = require(path.join(global.serverPath, 'express'));

const webpackConfigDir = path.join(global.templatesPath, 'build', 'config', 'webpack');
const webpackConfig = require(path.join(webpackConfigDir, 'config.js'));

const serverTaskStartTime = new Date();
console.log(`${config.formatTime(serverTaskStartTime)} Starting 'server'...`);

const argv = process.argv;
const builder = argv.includes('--builder');

//Преднастройка вебпака перед инициализацией сервера
webpackCallbacks.preinit(taskStartTime => {
    //Настройка конструктора
    if (builder) builderInit(webpackConfig);

    //Настройка express
    const app = expressCallbacks.init(webpackConfig.devServer.port);

    //Запуск вебпака
    const compiler = webpackCallbacks.init(app);

    //Инициализация сервера
    const server = http.createServer(app);

    //Настройка прослушивания событий вебпака
    webpackCallbacks.postinit(compiler, server, taskStartTime);

    //Настройка прослушивания сервера
    expressCallbacks.postinit(app, server, taskStartTime);
});