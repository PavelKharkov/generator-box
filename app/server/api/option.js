'use strict';

const fs = require('node:fs');
const path = require('node:path');

const lodash = require('lodash');

const commonConfigPath = path.join(global.appPath, 'config', 'common');
const config = require(path.join(commonConfigPath, 'config.js'));
const optionsValidation = require(path.join(global.serverPath, 'config', 'validation'));

const apiOption = (request, response) => {
    const requestData = request.body;

    const builderConfigFile = path.join(commonConfigPath, `${config.globalSettingsFilename}.json`);
    const builderConfig = JSON.parse(fs.readFileSync(builderConfigFile, config.encoding));

    const moduleName = requestData.module;
    delete requestData.module;
    const isStyles = requestData.styles === 'true';
    if (isStyles) delete requestData.styles;

    if (!builderConfig.styles) {
        builderConfig.styles = {};
    }
    Object.keys(requestData).some(option => {
        const validateType =
            optionsValidation[moduleName] && optionsValidation[moduleName][option] && optionsValidation[moduleName][option].validate;
        if (validateType) {
            if ((validateType === 'color') && (requestData[option].indexOf('#') !== 0)) {
                return response.send({
                    result: 'error',
                    option: requestData,
                    messageText: 'Неверное значение опции'
                });
            }
        } else {
            return response.send({
                result: 'error',
                option: requestData,
                messageText: 'Неверное название опции'
            });
        }
        return false;
    });

    lodash.assign(builderConfig[moduleName], requestData);
    //fs.writeFileSync(builderConfigFile, JSON.stringify(builderConfig, null, 2), config.encoding);

    //Запись файла стилей
    if (isStyles) {
        //TODO:write global-settings file
        //const stylesFile = path.join(global.templatesPath, config.paths.baseConfig, `${config.globalSettingsFilename}.${config.devStyleExt}`);

        //

        //fs.writeFileSync(stylesFile, resultStyles, config.encoding);
    }

    return response.send({
        result: 'success',
        option: requestData,
        messageText: 'Опции успешно сохранены'
    });
};

module.exports = apiOption;