'use strict';

const path = require('node:path');

const apiDir = 'api';
const apiRouterPath = `/${apiDir}/`;
const apiPath = path.join(global.serverPath, apiDir);

//
const apiOption = require(path.join(apiPath, 'option.js'));

const apiRouter = {
    [`${apiRouterPath}option`]: apiOption
};

module.exports = apiRouter;