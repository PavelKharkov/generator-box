'use strict';

const webpack = require('webpack');

const builderInit = webpackConfig => {
    webpackConfig.plugins.unshift(new webpack.DefinePlugin({
        __IS_BUILDER__: JSON.stringify(true) //Переменная, указывающая на то, что сайт запущен на сервере-конструкторе
    }));

    const scssUse = webpackConfig.module.rules[1].use;
    const sassLoaderOptions = scssUse[scssUse.length - 1].options;
    const scssData = sassLoaderOptions.prependData;
    sassLoaderOptions.prependData = `${scssData}$builder: true;\n`;

    const pugUse = webpackConfig.module.rules[2].use;
    const pugHtmlLoaderOptions = pugUse[pugUse.length - 2].options;
    pugHtmlLoaderOptions.data.globalConfig.isBuilder = true;
};

module.exports = builderInit;