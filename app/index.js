'use strict';

const path = require('node:path');

global.appPath = __dirname;
const generatorConfig = path.join(global.appPath, 'config', 'generator');

module.exports = require(path.join(generatorConfig, 'main.js'));