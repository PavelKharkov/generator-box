'use strict';

const {exec} = require('node:child_process');
const path = require('node:path');

const rimraf = require('rimraf');

global.appPath = path.resolve('app');
const configDir = path.join(global.appPath, 'config');
const config = require(path.join(configDir, 'common', 'config.js'));

const startTime = new Date();
console.log(`${config.formatTime(startTime)} Starting 'docs'...`);

const templatesPath = path.join(global.appPath, 'templates');

const args = process.argv;
const isInspect = args.includes('-i'); //Не рендерить файлы а только проверить компиляцию скриптов на наличие ошибок
const type = (() => { //Тип файлов для документации (по умолчанию — скрипты, -s — стили, -t — разметка)
    if (args.includes('-s')) {
        return 'styles';
    } else if (args.includes('-t')) {
        return 'templates';
    }

    return 'scripts';
})();

const jsdocConfigDir = path.join(configDir, 'jsdoc');
const distDocsDir = 'docs';
const files = path.join(templatesPath, config.paths.src);
let destination;
let configure;

switch (type) {
    case 'scripts': //Генерация документации скриптов
        destination = path.join(distDocsDir, config.paths.scriptsDirName);
        configure = path.join(jsdocConfigDir, 'scripts.json');
        break;
    case 'templates': //Генерация документации разметки
        destination = path.join(distDocsDir, config.paths.templatesDirName);
        configure = path.join(jsdocConfigDir, 'templates.json');
        break;
    case 'styles': //Генерация документации стилей
        destination = path.join(distDocsDir, config.paths.stylesDirName);
        configure = path.join(jsdocConfigDir, 'styles.json');
        break;
    default:
        break;
}

const commands = [
    'jsdoc',
    files,
    '--recurse',
    `--configure ${configure}`,
    `--destination ${destination}`
];
if (isInspect) commands.push('--explain');

rimraf.sync(destination, {
    glob: false
});

exec(commands.join(' '), (error, stdout, stderr) => {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }

    if (stdout) {
        console.log(`stdout: ${stdout}`);
    } else if (stderr) {
        console.error(`stderr: ${stderr}`);
    }

    const finishTime = new Date();
    console.log(`${config.formatTime(finishTime)} Finished 'docs' after ${finishTime.getTime() - startTime.getTime()} ms`);
});