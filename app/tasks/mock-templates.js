'use strict';

const fs = require('node:fs');
const path = require('node:path');

global.appPath = path.resolve('app');
const configDir = path.join(global.appPath, 'config');
const commonConfigPath = path.join(configDir, 'common');

const configPath = path.join(commonConfigPath, 'config.js');
const config = require(configPath);

const renderData = require(path.join(configDir, 'generator', 'mock-data.json'));
renderData.defaultLanguage = renderData.defaultLanguageAndRegion.slice(0, renderData.defaultLanguageAndRegion.indexOf('-'));
renderData.defaultRegion = renderData.defaultLanguageAndRegion.slice(renderData.defaultLanguageAndRegion.indexOf('-') + 1);

//Компиляция файлов для разработки коробки
const startTime = new Date();
console.log(`${config.formatTime(startTime)} Starting 'mock-templates'...`);

const templatesPath = path.join(global.appPath, 'templates');

//Компиляция файлов настроек
const buildConfigDir = path.join(templatesPath, config.paths.buildConfig);

const configFiles = [
    {
        file: path.join(commonConfigPath, 'config.js'),
        render: true
    },
    {file: path.join(commonConfigPath, 'paths.js')},
    {file: path.join(commonConfigPath, 'pug.js')},
    {file: path.join(commonConfigPath, 'server.js')},
    {file: path.join(commonConfigPath, 'global-settings.json')}
];
configFiles.forEach(({file, render}) => {
    const fileBuildPath = path.join(buildConfigDir, path.basename(path.dirname(file)));
    if (!fs.existsSync(fileBuildPath)) fs.mkdirSync(fileBuildPath);

    let fileContent = fs.readFileSync(file, config.encoding);
    if (render) {
        Object.keys(renderData).forEach(key => {
            fileContent = fileContent.replace(new RegExp(`<%= ${key} %>`, 'g'), renderData[key]);
        });
    }

    fs.writeFileSync(
        path.join(fileBuildPath, path.basename(file)),
        fileContent,
        config.encoding
    );
});

//Копирование других файлов
const babelrcPath = path.join('babel.config.js');
fs.writeFileSync(path.join(templatesPath, babelrcPath), fs.readFileSync(babelrcPath, config.encoding), config.encoding); //используется модулем jest

const eslintConfigPath = path.join('.eslintrc.json');
const eslintConfigData = fs.readFileSync(eslintConfigPath, config.encoding);
const eslintObj = JSON.parse(eslintConfigData);
delete eslintObj.extends;
delete eslintObj.plugins;
fs.writeFileSync(path.join(templatesPath, eslintConfigPath), JSON.stringify(eslintObj, null, 2), config.encoding);

const finishTime = new Date();
console.log(`${config.formatTime(finishTime)} Finished 'mock-templates' after ${finishTime.getTime() - startTime.getTime()} ms`);