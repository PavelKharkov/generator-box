const getCurrentSectionName = () => {
    const path = window.location.pathname;
    const pageUrl = path.split('/').pop();
    const resultPageUrl = pageUrl.slice(0, pageUrl.indexOf('.html'));

    const sectionName = resultPageUrl
        .slice(resultPageUrl.includes('.') ? (resultPageUrl.indexOf('.') + 1) : 0)
        .replace('module-', '');

    return sectionName;
};

const getCurrentHashName = () => {
    const pageSubSectionHash = window.location.hash;

    if (pageSubSectionHash) {
        const pageSubSectionId = pageSubSectionHash.slice(1).replace('.', '');
        return pageSubSectionId;
    }

    return false;
};

const highlightActiveHash = event => {
    //Check for and remove old hash active state
    const oldUrl = event && event.oldURL;
    if (oldUrl && oldUrl.includes('#')) {
        let prevSubSectionEl;
        try {
            prevSubSectionEl = document.querySelector(`#${getCurrentSectionName()}-${oldUrl.slice(oldUrl.indexOf('#') + 1)}-nav`);
        } catch (error) { /* eslint-disable-line unicorn/prefer-optional-catch-binding */
            //no error
        }
        if (prevSubSectionEl) {
            prevSubSectionEl.classList.remove('active');
        }
    }

    if (getCurrentHashName()) {
        let currentSectionEl;
        try {
            currentSectionEl = document.querySelector(`#${getCurrentSectionName()}-${getCurrentHashName()}-nav`);
        } catch (error) { /* eslint-disable-line unicorn/prefer-optional-catch-binding */
            //no error
        }
        if (currentSectionEl) currentSectionEl.classList.add('active');
    }
};

const highlightActiveSection = () => {
    const pageId = getCurrentSectionName();

    let activeSectionEl;
    try {
        activeSectionEl = document.querySelector(`#${pageId}-nav`);
    } catch (error) { /* eslint-disable-line unicorn/prefer-optional-catch-binding */
        //no error
    }
    if (activeSectionEl) activeSectionEl.classList.add('active');
};

window.addEventListener('load', () => {
    const navEl = document.querySelector('nav');
    const topNavEl = document.querySelector('.top-nav-wrapper');

    //If an anchor hash is in the URL highlight the menu item
    highlightActiveHash();
    //If a specific page section is in the URL highlight the menu item
    highlightActiveSection();

    //If a specific page section is in the URL scroll that section up to the top
    let currentSectionEl;
    try {
        currentSectionEl = navEl.querySelector(`#${getCurrentSectionName()}-nav`);
    } catch (error) { /* eslint-disable-line unicorn/prefer-optional-catch-binding */
        //no error
    }
    if (currentSectionEl) {
        navEl.scrollTop += currentSectionEl.getBoundingClientRect().top - topNavEl.offsetHeight;
    }

    //Function to scroll to anchor when clicking an anchor link
    document.querySelectorAll('a[href*="#"]:not([href="#"])').forEach(link => {
        link.addEventListener('click', () => {
            if ((window.location.pathname.replace(/^\//, '') === link.pathname.replace(/^\//, '')) &&
                (window.location.hostname === link.hostname)) {
                let hashTarget;
                try {
                    hashTarget = document.querySelector(link.hash);
                } catch (error) { /* eslint-disable-line unicorn/prefer-optional-catch-binding */
                    //no error
                }
                const target = hashTarget ? hashTarget : document.querySelector(`[name="${link.hash.slice(1)}"]`);
                if (!target) return;

                document.querySelectorAll('html, body').forEach(item => { /* eslint-disable-line max-nested-callbacks */
                    item.scrollTop += target.getBoundingClientRect().top;
                });
            }
        });
    });
});

//If a new anchor section is selected, change the highlighted menu item
window.addEventListener('hashchange', event => {
    highlightActiveHash(event);
});