window.addEventListener('load', () => {
    const highlightedArray = (window.location.hash.indexOf('#code') === 0) && window.location.hash.slice(1).split('.');
    const highlightedBlock = highlightedArray[0];
    const highlightedLine = highlightedArray[1];

    document.querySelectorAll('pre').forEach((item, itemIndex) => {
        const codeElement = item.querySelectorAll('code')[0];

        item.classList.add('line-numbers', 'linkable-line-numbers');
        item.id = `code-${itemIndex}`;
        item.dataset.line = ((item.id === highlightedBlock) && highlightedLine) ? highlightedLine : -1;

        //Определяет тип документации
        const titleText = document.querySelector('.reference-title').textContent;
        let currentLanguage = 'javascript';
        if (!titleText.includes('скрипты')) {
            currentLanguage = titleText.includes('стили') ? 'scss' : 'html';
        }

        //Подсвечиваить блоки с комментарием <!--HTML--> как html
        const htmlComment = '<!--HTML-->';
        if (codeElement.textContent.indexOf(htmlComment) === 0) {
            codeElement.textContent = codeElement.textContent.slice(htmlComment.length + 1);
            currentLanguage = 'html';
        }
        codeElement.classList.add(`language-${currentLanguage}`);
        window.Prism.highlightElement(codeElement);

        //Прокручивать до выделенной строки кода при окрытии страницы
        if (highlightedLine) {
            setTimeout(() => {
                const highlightedPosition =
                    item.querySelector('.line-highlight').getBoundingClientRect().top - document.body.getBoundingClientRect().top;
                window.scrollTo(0, highlightedPosition);
            }, 0);
        }
    });

    //Открывать сторонние ссылки в новом окне
    document.querySelectorAll('a').forEach(link => {
        if (/^(http:\/\/|https:\/\/)/.test(link.getAttribute('href'))) {
            link.target = '_blank';
        }
    });
});