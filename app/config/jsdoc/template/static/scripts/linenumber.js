(() => {
    const source = document.querySelector('.prettyprint.source.linenums');
    if (!source) return;

    const anchorHash = window.location.hash.slice(1);
    const lines = source.querySelectorAll('li');

    lines.forEach((lineItem, index) => {
        const lineId = `line${index + 1}`;
        lineItem.id = lineId;
        if (lineId === anchorHash) {
            lineItem.classList.add('selected');
        }
    });
})();