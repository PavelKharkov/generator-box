'use strict';

const doop = require('jsdoc/util/doop');
const env = require('jsdoc/env');
const fs = require('jsdoc/fs');
const helper = require('jsdoc/util/templateHelper');
const logger = require('jsdoc/util/logger');
const path = require('jsdoc/path');
const {taffy} = require('taffydb');
const template = require('jsdoc/template');
const util = require('util');
const {Filter} = require('jsdoc/src/filter');
const {Scanner} = require('jsdoc/src/scanner');

const htmlsafe = helper.htmlsafe;
const linkto = helper.linkto;
const resolveAuthorLinks = helper.resolveAuthorLinks;
const hasOwnProp = Object.prototype.hasOwnProperty;

let data;
let view;

let outdir = path.normalize(env.opts.destination);

const defaultEncoding = 'utf8';

const findSpec = spec => {
    return helper.find(data, spec); /* eslint-disable-line unicorn/no-array-callback-reference, unicorn/no-array-method-this-argument */
};

const tutorialLink = tutorial => {
    return helper.toTutorial(tutorial, null, {
        tag: 'em',
        classname: 'disabled',
        prefix: 'Tutorial: '
    });
};

const getAncestorLinks = doclet => {
    return helper.getAncestorLinks(data, doclet);
};

const hashToLink = (doclet, hash) => {
    let url;

    if (!/^(#.+)/.test(hash)) return hash;

    url = helper.createLink(doclet);
    url = url.replace(/(#.+|$)/, hash);

    return `<a href="${url}">${hash}</a>`;
};

const needsSignature = ({kind, type, meta}) => {
    let needsSig = false;

    if ((kind === 'function') || (kind === 'class') || (kind === 'event')) {
        needsSig = true;
    } else if ((kind === 'typedef') && type && type.names && (type.names.length > 0)) {
        type.names.find(nameItem => {
            if (nameItem.toLowerCase() === 'function') {
                needsSig = true;
                return true;
            }
            return false;
        });
    } else if ((kind === 'namespace') && meta && meta.code && meta.code.type && /[Ff]unction/.test(meta.code.type)) {
        needsSig = true;
    }

    return needsSig;
};

const getSignatureAttributes = ({optional, nullable}) => {
    const attributes = [];

    if (optional) attributes.push('opt');

    if (nullable === true) {
        attributes.push('nullable');
    } else if (nullable === false) {
        attributes.push('non-null');
    }

    return attributes;
};

const updateItemName = item => {
    const attributes = getSignatureAttributes(item);
    const optIndex = attributes.indexOf('opt');
    if (optIndex !== -1) {
        attributes[optIndex] = 'опц.';
    }
    let itemName = item.name || '';

    if (item.variable) {
        itemName = `&hellip;${itemName}`;
    }

    if (attributes && attributes.length > 0) {
        itemName = util.format('%s<span class="signature-attributes">%s</span>', itemName, attributes.join(', '));
    }

    return itemName;
};

const addParamAttributes = params => {
    return params
        .filter(({name}) => {
            return name && !name.includes('.');
        })
        .map(item => {
            return updateItemName(item);
        });
};

const buildItemTypeStrings = item => {
    const types = [];

    if (item && item.type && item.type.names) {
        item.type.names.forEach(name => {
            types.push(linkto(name, htmlsafe(name)));
        });
    }

    return types;
};

const buildAttribsString = attributes => {
    let attributesString = '';

    if (attributes && attributes.length > 0) {
        attributesString = htmlsafe(util.format('(%s) ', attributes.join(', ')));
    }

    return attributesString;
};

const addNonParamAttributes = items => {
    let types = [];

    items.forEach(item => {
        types = [...types, buildItemTypeStrings(item)];
    });

    return types;
};

const addSignatureParams = doclet => {
    const params = doclet.params ? addParamAttributes(doclet.params) : [];

    doclet.signature = util.format('%s(%s)', (doclet.signature || ''), params.join(', '));
};

const addSignatureReturns = doclet => {
    const attribs = [];
    let attribsString = '';
    let returnTypes = [];
    let returnTypesString = '';
    const source = doclet.yields || doclet.returns;

    if (source) {
        source.forEach(item => {
            helper.getAttribs(item).forEach(attrib => {
                if (!attribs.includes(attrib)) {
                    attribs.push(attrib);
                }
            });
        });

        attribsString = buildAttribsString(attribs);
    }

    if (source) {
        returnTypes = addNonParamAttributes(source);
    }
    if (returnTypes.length > 0) {
        returnTypesString = util.format(' &rarr; %s{%s}', attribsString, returnTypes.join('|'));
    }

    doclet.signature = `
        <span class="signature">${doclet.signature || ''}</span>
        <span class="type-signature">${returnTypesString}</span>`;
};

const addSignatureTypes = doclet => {
    const types = doclet.type ? buildItemTypeStrings(doclet) : [];

    const typesText = (types.length > 0) ? ` :${types.join('|')}` : '';
    doclet.signature = `${doclet.signature || ''}<span class="type-signature">${typesText}</span>`;
};

const addAttribs = doclet => {
    const attribs = helper.getAttribs(doclet);
    let attribsString = buildAttribsString(attribs);

    switch (attribsString) {
        case '(async, static, readonly) ':
            attribsString = '(асинхр., статическое, только для чтения) ';
            break;
        case '(async, static) ':
            attribsString = '(асинхр., статическое) ';
            break;
        case '(async, readonly) ':
            attribsString = '(асинхр., только для чтения) ';
            break;
        case '(readonly) ':
            attribsString = '(только для чтения) ';
            break;
        case '(static) ':
            attribsString = '(статическое) ';
            break;
        case '(static, readonly) ':
            attribsString = '(статическое, только для чтения) ';
            break;
        default:
            break;
    }

    doclet.attribs = util.format('<span class="type-signature">%s</span>', attribsString);
};

const shortenPaths = (files, commonPrefix) => {
    Object.keys(files).forEach(file => {
        files[file].shortened = files[file].resolved
            .replace(commonPrefix, '')
            .replaceAll('\\', '/');
    });

    return files;
};

const getPathFromDoclet = ({meta}) => {
    if (!meta) return null;

    if (meta.path) {
        return (meta.path === 'null')
            ? meta.filename
            : path.join(meta.path, meta.filename);
    }

    return null;
};

const generate = (title, docs, filename, resolveLinks) => { /* eslint-disable-line max-params */
    const docData = {
        env,
        title,
        docs
    };

    const outpath = path.join(outdir, filename);
    let html = view.render('container.tmpl', docData);

    if (resolveLinks !== false) {
        html = helper.resolveLinks(html);
    }

    fs.writeFileSync(outpath, html, defaultEncoding);
};

const generateSourceFiles = (sourceFiles, encoding = defaultEncoding) => {
    Object.keys(sourceFiles).forEach(file => {
        let source;
        const sourceOutfile = helper.getUniqueFilename(sourceFiles[file].shortened);

        helper.registerLink(sourceFiles[file].shortened, sourceOutfile);

        try {
            source = {
                kind: 'source',
                code: helper.htmlsafe(fs.readFileSync(sourceFiles[file].resolved, encoding))
            };
        } catch (error) {
            logger.error('Error while generating source file %s: %s', file, error.message);
        }

        generate(`Source: ${sourceFiles[file].shortened}`, [source], sourceOutfile, false);
    });
};

/**
 * @desc Look for classes or functions with the same name as modules (which indicates that the module
 * exports only that class or function), then attach the classes or functions to the `module`
 * property of the appropriate module doclets. The name of each class or function is also updated
 * for display purposes. This function mutates the original arrays.
 * @private
 * @param {Array.<module:jsdoc/doclet.Doclet>} doclets - The array of classes and functions to
 * check.
 * @param {Array.<module:jsdoc/doclet.Doclet>} modules - The array of module doclets to search.
 */
const attachModuleSymbols = (doclets, modules) => {
    const symbols = {};

    doclets.forEach(symbol => {
        symbols[symbol.longname] = symbols[symbol.longname] || [];
        symbols[symbol.longname].push(symbol);
    });

    modules.forEach(moduleItem => {
        if (symbols[moduleItem.longname]) {
            module.modules = symbols[moduleItem.longname]
                .filter(({description, kind}) => {
                    return description || (kind === 'class');
                })
                .map(symbol => {
                    const doopedSymbol = doop(symbol);

                    if ((doopedSymbol.kind === 'class') || (doopedSymbol.kind === 'function')) {
                        doopedSymbol.name = `${doopedSymbol.name.replace('module:', '(require("')}"))`;
                    }

                    return doopedSymbol;
                });
        }
    });
};

const buildMemberNav = (items, itemHeading, itemsSeen, linktoFn) => { /* eslint-disable-line max-params */
    let nav = '';

    if (items.length > 0) {
        let itemsNav = '';

        items.forEach(item => {
            let displayName;
            const methods = findSpec({
                kind: 'function',
                memberof: item.longname
            });
            const events = findSpec({
                kind: 'event',
                memberof: item.longname
            });

            if (!hasOwnProp.call(item, 'longname')) {
                itemsNav += `<li id="${item.name.replace('/', '_')}-nav">${linktoFn('', item.name)}</li>`;
            } else if (!hasOwnProp.call(itemsSeen, item.longname)) {
                displayName = item[env.conf.templates.default.useLongnameInNav ? 'longname' : 'name'];
                const nameText = linktoFn(item.longname, displayName.replace(/\b(module|event):/g, ''));
                itemsNav += `<li id="${item.name.replace('/', '_')}-nav">${nameText}`;

                if (methods.length > 0) {
                    itemsNav += '<ul class="methods">';
                    methods.forEach(method => {
                        itemsNav += `<li data-type="method" id="${item.name.replace('/', '_')}-${method.name}-nav">
                            ${linkto(method.longname, method.name)}
                        </li>`;
                    });
                    itemsNav += '</ul>';
                }

                if (events.length > 0) {
                    itemsNav += '<ul class="events">';
                    events.forEach(eventObj => {
                        itemsNav += `<li data-type="event" id="${item.name.replace('/', '_')}-${eventObj.name}-nav">
                            ${linkto(eventObj.longname, eventObj.name)}
                        </li>`;
                    });
                    itemsNav += '</ul>';
                }
                itemsNav += '</li>';

                itemsSeen[item.longname] = true;
            }
        });

        if (itemsNav !== '') {
            nav += `<h3>${itemHeading}</h3><ul>${itemsNav}</ul>`;
        }
    }

    return nav;
};

const linktoTutorial = (longName, name) => {
    return tutorialLink(name);
};

const linktoExternal = (longName, name) => {
    return linkto(longName, name.replace(/(^"|"$)/g, ''));
};

/**
 * @desc Create the navigation sidebar.
 * @param {Object} members - The members that will be used to create the sidebar.
 * @param {Array<Object>} members.classes - .
 * @param {Array<Object>} members.externals - .
 * @param {Array<Object>} members.globals - .
 * @param {Array<Object>} members.mixins - .
 * @param {Array<Object>} members.modules - .
 * @param {Array<Object>} members.namespaces - .
 * @param {Array<Object>} members.tutorials - .
 * @param {Array<Object>} members.events - .
 * @param {Array<Object>} members.interfaces - .
 * @returns {string} The HTML for the navigation sidebar.
 */
const buildNav = members => {
    let globalNav;
    let nav = '';
    const seen = {};
    const seenTutorials = {};

    nav += buildMemberNav(members.classes, 'Классы', seen, linkto);
    nav += buildMemberNav(members.modules, 'Модули', {}, linkto);
    nav += buildMemberNav(members.externals, 'Внешние скрипты', seen, linktoExternal);
    nav += buildMemberNav(members.namespaces, 'Пространства имён', seen, linkto);
    nav += buildMemberNav(members.mixins, 'Миксины', seen, linkto);
    nav += buildMemberNav(members.tutorials, 'Руководства', seenTutorials, linktoTutorial);
    nav += buildMemberNav(members.interfaces, 'Интерфейсы', seen, linkto);

    if (members.globals.length > 0) {
        globalNav = '';

        members.globals.forEach(({kind, longname, name}) => {
            if ((kind !== 'typedef') && !hasOwnProp.call(seen, longname)) {
                globalNav += `<li>${linkto(longname, name)}</li>`;
            }
            seen[longname] = true;
        });

        nav += globalNav
            ? `<h3 id="global-nav">Глобальные переменные</h3><ul>${globalNav}</ul>`
            : `<h3 id="global-nav">${linkto('global', 'Глобальные переменные')}</h3>`;
    }

    return nav;
};

/**
 * @desc .
 * @param {Object} taffyData - See <http://taffydb.com/>.
 * @param {Object} opts - .
 * @param {Object} tutorials - .
 */
exports.publish = (taffyData, opts, tutorials) => {
    const sourceFilePaths = [];
    let sourceFiles = {};
    let staticFileFilter;
    let staticFilePaths;
    let staticFileScanner;

    data = taffyData;

    const conf = env.conf.templates || {};
    conf.default = conf.default || {};

    const templatePath = path.normalize(opts.template);
    view = new template.Template(path.join(templatePath, 'tmpl'));

    const indexUrl = helper.getUniqueFilename('index');

    const globalUrl = helper.getUniqueFilename('global');
    helper.registerLink('global', globalUrl);

    view.layout = conf.default.layoutFile
        ? path.getResourcePath(path.dirname(conf.default.layoutFile), path.basename(conf.default.layoutFile))
        : 'layout.tmpl';

    helper.setTutorials(tutorials);

    data = helper.prune(data);
    data.sort('longname, version, since');
    helper.addEventListeners(data);

    data().each(doclet => {
        let sourcePath;

        doclet.attribs = '';

        if (doclet.examples) {
            doclet.examples = doclet.examples.map(example => {
                let caption;
                let code;

                if (/^\s*<caption>([\s\S]+?)<\/caption>(\s*[\n\r])([\s\S]+)$/i.test(example)) {
                    caption = RegExp.$1;
                    code = RegExp.$3;
                }

                return {
                    caption: caption || '',
                    code: code || example
                };
            });
        }
        if (doclet.see) {
            doclet.see.forEach((seeItem, seeIndex) => {
                doclet.see[seeIndex] = hashToLink(doclet, seeItem);
            });
        }

        if (doclet.meta) {
            sourcePath = getPathFromDoclet(doclet);
            sourceFiles[sourcePath] = {
                resolved: sourcePath,
                shortened: null
            };
            if (!sourceFilePaths.includes(sourcePath)) {
                sourceFilePaths.push(sourcePath);
            }
        }
    });

    const packageInfo = (findSpec({kind: 'package'}) || [])[0];
    if (packageInfo && packageInfo.name) {
        outdir = path.join(outdir, packageInfo.name, (packageInfo.version || ''));
    }
    fs.mkPath(outdir);

    const fromDir = path.join(templatePath, 'static');
    const staticFiles = fs.ls(fromDir, 3);

    staticFiles.forEach(fileName => {
        const toDir = fs.toDir(fileName.replace(fromDir, outdir));

        fs.mkPath(toDir);
        fs.copyFileSync(fileName, toDir);
    });

    if (conf.default.staticFiles) {
        staticFilePaths = conf.default.staticFiles.include ||
            conf.default.staticFiles.paths ||
            [];
        staticFileFilter = new Filter(conf.default.staticFiles);
        staticFileScanner = new Scanner();

        staticFilePaths.forEach(filePath => {
            const newFilePath = path.resolve(env.pwd, filePath);
            const extraStaticFiles = staticFileScanner.scan([newFilePath], 10, staticFileFilter);

            extraStaticFiles.forEach(fileName => {
                const sourcePath = fs.toDir(newFilePath);
                const toDir = fs.toDir(fileName.replace(sourcePath, outdir));

                fs.mkPath(toDir);
                fs.copyFileSync(fileName, toDir);
            });
        });
    }

    if (sourceFilePaths.length > 0) {
        sourceFiles = shortenPaths(sourceFiles, path.commonPrefix(sourceFilePaths));
    }
    data().each(doclet => {
        let docletPath;
        const url = helper.createLink(doclet);

        helper.registerLink(doclet.longname, url);

        if (doclet.meta) {
            docletPath = getPathFromDoclet(doclet);
            docletPath = sourceFiles[docletPath].shortened;
            if (docletPath) {
                doclet.meta.shortpath = docletPath;
            }
        }
    });

    data().each(doclet => {
        const url = helper.longnameToUrl[doclet.longname];

        doclet.id = url.includes('#')
            ? helper.longnameToUrl[doclet.longname].split(/#/).pop()
            : doclet.name;

        if (needsSignature(doclet)) {
            addSignatureParams(doclet);
            addSignatureReturns(doclet);
            addAttribs(doclet);
        }
    });

    data().each(doclet => {
        doclet.ancestors = getAncestorLinks(doclet);

        if (doclet.kind === 'member') {
            addSignatureTypes(doclet);
            addAttribs(doclet);
        }

        if (doclet.kind === 'constant') {
            addSignatureTypes(doclet);
            addAttribs(doclet);
            doclet.kind = 'member';
        }
    });

    const members = helper.getMembers(data);
    members.tutorials = tutorials.children;

    const outputSourceFiles = conf.default && conf.default.outputSourceFiles !== false;

    view.find = findSpec;
    view.linkto = linkto;
    view.resolveAuthorLinks = resolveAuthorLinks;
    view.tutorialLink = tutorialLink;
    view.htmlsafe = htmlsafe;
    view.outputSourceFiles = outputSourceFiles;

    view.nav = buildNav(members);
    attachModuleSymbols(findSpec({longname: {left: 'module:'}}), members.modules);

    if (outputSourceFiles) {
        generateSourceFiles(sourceFiles, opts.encoding);
    }

    if (members.globals.length > 0) {
        generate('Global', [{kind: 'globalobj'}], globalUrl);
    }

    const files = findSpec({kind: 'file'});
    const packages = findSpec({kind: 'package'});

    const mainPagePackage = {
        kind: 'mainpage',
        readme: opts.readme,
        longname: (opts.mainpagetitle) ? opts.mainpagetitle : 'Main Page'
    };
    const homePackages = [
        ...packages,
        mainPagePackage,
        ...files
    ];
    generate('Home', homePackages, indexUrl);

    const classes = taffy(members.classes);
    const modules = taffy(members.modules);
    const namespaces = taffy(members.namespaces);
    const mixins = taffy(members.mixins);
    const externals = taffy(members.externals);
    const interfaces = taffy(members.interfaces);

    Object.keys(helper.longnameToUrl).forEach(longname => {
        /* eslint-disable unicorn/no-array-callback-reference, unicorn/no-array-method-this-argument */
        const myClasses = helper.find(classes, {longname});
        const myExternals = helper.find(externals, {longname});
        const myInterfaces = helper.find(interfaces, {longname});
        const myMixins = helper.find(mixins, {longname});
        const myModules = helper.find(modules, {longname});
        const myNamespaces = helper.find(namespaces, {longname});
        /* eslint-enable unicorn/no-array-callback-reference, unicorn/no-array-method-this-argument */

        if (myModules.length > 0) {
            generate(`Модуль: ${myModules[0].name}`, myModules, helper.longnameToUrl[longname]);
        }

        if (myClasses.length > 0) {
            generate(`Класс: ${myClasses[0].name}`, myClasses, helper.longnameToUrl[longname]);
        }

        if (myNamespaces.length > 0) {
            generate(`Пространство имён: ${myNamespaces[0].name}`, myNamespaces, helper.longnameToUrl[longname]);
        }

        if (myMixins.length > 0) {
            generate(`Миксин: ${myMixins[0].name}`, myMixins, helper.longnameToUrl[longname]);
        }

        if (myExternals.length > 0) {
            generate(`Внешний скрипт: ${myExternals[0].name}`, myExternals, helper.longnameToUrl[longname]);
        }

        if (myInterfaces.length > 0) {
            generate(`Интерфейс: ${myInterfaces[0].name}`, myInterfaces, helper.longnameToUrl[longname]);
        }
    });

    const generateTutorial = (title, tutorial, filename) => {
        const tutorialData = {
            title,
            header: tutorial.title,
            content: tutorial.parse(),
            children: tutorial.children
        };
        const tutorialPath = path.join(outdir, filename);
        let html = view.render('tutorial.tmpl', tutorialData);

        html = helper.resolveLinks(html);

        fs.writeFileSync(tutorialPath, html, defaultEncoding);
    };

    const saveChildren = ({children}) => {
        children.forEach(child => {
            generateTutorial(`Руководство: ${child.title}`, child, helper.tutorialToUrl(child.name));
            saveChildren(child);
        });
    };

    saveChildren(tutorials);
};