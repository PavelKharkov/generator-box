'use strict';

/**
 * @file Выводит рендер pug-сниппета.
 */

const path = require('path');

global.appPath = path.resolve('app');
const templatesPath = path.join(global.appPath, 'templates');
const commonConfigPath = path.join(global.appPath, 'config', 'common');
const config = require(path.join(commonConfigPath, 'config.js'));
const pugLocals = require(path.join(commonConfigPath, 'pug.js'));
const globalSettings = require(path.join(commonConfigPath, `${config.globalSettingsFilename}.json`));

//const logger = require('jsdoc/util/logger');
const pug = require('pug');
const beautifyHtml = require('js-beautify').html;

const beautifyConfig = config.beautifyConfig;

const baseTemplatesPath = path.posix.join(config.paths.baseName, config.paths.templatesDirName);

exports.handlers = {
    newDoclet(docletEvent) {
        if (docletEvent.doclet && docletEvent.doclet.examples) {
            const moduleDir = path.basename(path.dirname(docletEvent.doclet.meta.path));
            const moduleName = path.basename(docletEvent.doclet.meta.path);
            const moduleFilename = docletEvent.doclet.meta.filename;

            //Добавление глобальных переменных для компиляции
            pugLocals.basedir = path.join(templatesPath, config.paths.src);
            pugLocals.filename = moduleName;
            Object.assign(pugLocals.globalConfig, {
                docs: true,
                templatesRoot: templatesPath,
                defaultLanguage: 'ru',
                defaultRegion: 'RU',
                settings: globalSettings
            });

            docletEvent.doclet.examples = docletEvent.doclet.examples.map(content => {
                //logger.error(content);

                let mainInclude = `include /${baseTemplatesPath}/`;
                if (moduleDir === 'base') {
                    if (/includes-docs|mixins/.test(moduleFilename)) {
                        mainInclude += 'main.pug';
                    } else if (!/config|main/.test(moduleFilename)) {
                        mainInclude += 'config.pug';
                    }
                } else {
                    mainInclude += 'main.pug';
                }

                /* eslint-disable prefer-template */
                const filenameLine = `- const filename = '${moduleName}';`;
                const pageLine = '- const page = {};';
                const selfIncludeLine = `include /${moduleDir}/${moduleName}/${moduleFilename}`;
                const includesContent = [
                    filenameLine,
                    pageLine,
                    mainInclude,
                    selfIncludeLine,
                    content
                ];
                const pugRender = pug.render(includesContent.join('\n'), pugLocals);

                const output =
                '\n\nРезультат:\n' +
                beautifyHtml(pugRender, beautifyConfig);
                /* eslint-enable prefer-template */

                //Удаление ссылок на подключаемые файлы шаблонов
                let isPrevInclude = false;
                const shownContent = content
                    .split('\n')
                    .filter(line => {
                        const isInclude = line.startsWith('include ');
                        if (isInclude || (isPrevInclude && (line === ''))) {
                            isPrevInclude = isInclude;
                            return false;
                        }
                        return true;
                    })
                    .join('\n');

                return shownContent + output;
            });
        }
    }
};