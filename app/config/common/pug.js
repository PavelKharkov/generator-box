'use strict';

const fs = require('fs');
const path = require('path');

const config = require('./config.js');

const lodash = require('lodash');

const pugLocals = {
    globalConfig: {
        isDist: false,
        isDocs: false,
        hasPolyfills: false,
        isBuilder: false,

        isSyncModules: (config.isSyncModules === 'true'),
        hasVue: config.hasVue,

        encoding: config.encoding,
        defaultLanguage: config.defaultLanguage,
        defaultRegion: config.defaultRegion,
        projectName: config.projectName,
        projectThemeColor: config.projectThemeColor,
        developerName: config.developerName,
        developerSite: config.developerSite,

        distScriptFile: config.distScriptFile,
        distStyleFile: config.distStyleFile,
        spriteFile: config.spriteFile,
        moduleScriptFile: config.moduleScriptFile,

        distAssetsName: config.paths.distAssetsName,
        scriptsDirName: config.paths.scriptsDirName,
        stylesDirName: config.paths.stylesDirName,
        mediaName: config.paths.mediaName,
        uploadsName: config.paths.uploadsName,
        ajaxName: config.paths.ajaxName,
        iconsName: config.paths.iconsName,
        spritesName: config.paths.spritesName,

        fonts: config.paths.fonts,
        pages: config.paths.pages
    },

    fs,
    path: (() => {
        const result = {...path};
        delete result.win32;
        delete result.posix;
        return result;
    })(),
    _: lodash
};

module.exports = pugLocals;