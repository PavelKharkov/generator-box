'use strict';

const path = require('path');

//Основные директории
const paths = {
    //Основные пути
    dist: 'dist', //Путь к релизной папке по умолчанию
    src: 'src', //Путь к папке исходников
    build: 'build', //Путь к папке разработки
    tests: 'tests', //Путь к папке с результатами тестов
    temp: 'temp', //Путь к временной папке

    distTemplatesName: '', //Название папки с html-страницами в релизной папке
    distAssetsName: 'assets', //Название папки ресурсов в релизной папке

    //Названия папок исходников
    baseName: 'base',
    componentsName: 'components',
    dataName: 'data',
    fontsName: 'fonts',
    layoutName: 'layout',
    libsName: 'libs',
    pagesName: 'pages',
    spritesName: 'sprites',
    iconsName: 'icons',

    //Названия типовых папок разработки
    templatesDirName: 'templates',
    scriptsDirName: 'scripts',
    stylesDirName: 'styles',

    //Другие папки
    mediaName: 'media',
    uploadsName: 'uploads',
    rootName: 'root',
    ajaxName: 'ajax',

    prologName: 'prolog',
    epilogName: 'epilog'
};

//Пути релизных папок
paths.distAssets = path.join(paths.dist, paths.distAssetsName);

Object.defineProperties(paths, {
    //Пути папок исходников
    base: {value: path.join(paths.src, paths.baseName)},
    components: {value: path.join(paths.src, paths.componentsName)},
    data: {value: path.join(paths.src, paths.dataName)},
    fonts: {value: path.join(paths.src, paths.fontsName)},
    layout: {value: path.join(paths.src, paths.layoutName)},
    libs: {value: path.join(paths.src, paths.libsName)},
    pages: {value: path.join(paths.src, paths.pagesName)},
    sprites: {value: path.join(paths.src, paths.spritesName)},

    //Пути релизных папок
    distTemplates: {value: path.join(paths.dist, paths.distTemplatesName)},
    distScripts: {value: path.join(paths.distAssets, paths.scriptsDirName)},
    distStyles: {value: path.join(paths.distAssets, paths.stylesDirName)},

    distFonts: {value: path.join(paths.distAssets, paths.fontsName)},
    distMedia: {value: path.join(paths.distAssets, paths.mediaName)},
    distUploads: {value: path.join(paths.distAssets, paths.uploadsName)},
    distAjax: {value: path.join(paths.distAssets, paths.ajaxName)},

    //Пути папок разработки
    buildConfig: {value: path.join(paths.build, 'config')}
});

//Пути папок исходников
Object.defineProperties(paths, {
    icons: {value: path.join(paths.layout, paths.iconsName, paths.mediaName)},

    //Пути вспомогательных папок
    baseConfig: {value: path.join(paths.base, 'config')},
    baseScripts: {value: path.join(paths.base, paths.scriptsDirName)},
    baseStyles: {value: path.join(paths.base, paths.stylesDirName)},
    baseTemplates: {value: path.join(paths.base, paths.templatesDirName)}
});

module.exports = paths;