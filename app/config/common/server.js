'use strict';

const net = require('node:net');
const os = require('node:os');

const config = require('./config.js');

const defaultPort = 4000;

const serverConfig = {
    //Определение IP-адреса
    getHost(callback) {
        const interfaces = os.networkInterfaces();
        Object.keys(interfaces).find(interfaceName => {
            return interfaces[interfaceName].find(({family, internal, address}) => {
                //Поиск не внутренего (например 127.0.0.1) IPv4 адреса
                if ((family === 'IPv4') && !internal) {
                    callback(address);
                    return true;
                }

                return false;
            });
        });
    },

    //Определение свободного порта
    getPort(host, callback, port = defaultPort) {
        const testServer = net.createServer(socket => {
            socket.write('Echo server\n');
            socket.pipe(socket);
        });

        testServer.listen(port, host);
        testServer.on('error', () => {
            const nextPort = port + 1;
            console.log(`${config.formatTime(new Date())} Port ${port} in use, checking ${nextPort}...`);
            this.getPort(host, callback, nextPort);
        });
        testServer.on('listening', () => {
            testServer.close();
            console.log(`${config.formatTime(new Date())} Found free port ${port}`);
            callback(port);
        });
    }
};

module.exports = serverConfig;