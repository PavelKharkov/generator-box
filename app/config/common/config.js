'use strict';

const config = {
    frontendPath: '<%= frontendPath %>', //Путь к месту хранения файлов для разработки начиная с корня сайта (например, /local/templates/common/frontend)
    staticPath: '<%= staticPath %>', //Путь к месту хранения скомпилированных файлов относительно папки разработки (например, ../static)
    shouldMoveToRoot: false, //Перемещать ли скомпилированные файлы в корень сайта

    defaultLanguage: '<%= defaultLanguage %>',
    defaultRegion: '<%= defaultRegion %>',
    projectName: '<%= projectDistName %>',
    projectDescription: '<%= projectDesc %>',
    projectThemeColor: '#ffffff',
    developerName: '<%= authorName %>',
    developerSite: '<%= authorSite %>',
    encoding: 'utf8',
    isDebug: '<%= isDebug %>', //Добавлять ли код для режима отладки
    isSyncModules: '<%= isSyncModules %>', //Подключать модули синхронно
    hasPolyfills: false, //Добавлять ли полифилы для IE
    distSourceMaps: false, //Какие карты кода использовать в релизных файлах
    devSourceMaps: 'eval-cheap-source-map', //Какие карты кода использовать в файлах для разработки
    hasVue: false, //Использовать библиотеку Vue

    //Расширения файлов исходников
    devTemplateExt: 'pug',
    devScriptExt: 'js',
    devStyleExt: 'scss',
    devDataExt: 'json',

    //Расширения скомпилированных файлов
    distTemplateExt: 'html',
    distScriptExt: 'js',
    distStyleExt: 'css',

    //Названия скомпилированных файлов
    distTemplateFilename: 'index',
    distScriptFilename: 'scripts',
    distStyleFilename: 'styles',
    spriteFile: 'sprite.svg',
    spritePngFile: 'sprite.png',

    //Другие файлы
    gitkeepFile: '.gitkeep',
    globalSettingsFilename: 'global-settings'
};

//Файлы модулей
Object.defineProperties(config, {
    moduleTemplateFile: {value: `template.${config.devTemplateExt}`},
    moduleAutoloadScriptFile: {value: `autoload.${config.devScriptExt}`},
    moduleScriptFile: {value: `index.${config.devScriptExt}`},
    moduleStyleFile: {value: `style.${config.devStyleExt}`},
    moduleDataFile: {value: `data.${config.devDataExt}`},
    includesFile: {value: `includes.${config.devTemplateExt}`}
});

//Скомпилированные файлы
Object.defineProperties(config, {
    distTemplateFile: {value: `${config.distTemplateFilename}.${config.distTemplateExt}`},
    distScriptFile: {value: `${config.distScriptFilename}.${config.distScriptExt}`},
    distStyleFile: {value: `${config.distStyleFilename}.${config.distStyleExt}`}
});

//Выводит текущее время в удобном для чтения формате
config.formatTime = date => {
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    return `[${hours}:${minutes}:${seconds}]`;
};

//Настройки beautify
/* eslint-disable camelcase */
config.beautifyConfig = {
    indent_size: 4,
    inline: [],
    wrap_line_length: 0
};
/* eslint-enable camelcase */

config.paths = require('./paths.js');

module.exports = config;