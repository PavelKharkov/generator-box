'use strict';

const path = require('node:path');
const Generator = require('yeoman-generator');

const configDir = path.join(global.appPath, 'config');
const generatorConfig = path.join(configDir, 'generator');

const writing = require(path.join(generatorConfig, 'writing.js'));
const promptQuestions = require(path.join(generatorConfig, 'prompts.js'));
const dataMock = require(path.join(generatorConfig, 'mock-data.json'));

const promptAnswers = {};
let currentQuestion = 'initType'; //Первый задаваемый вопрос генератора

//Инициализация генератора
class Box extends Generator {
    constructor(args, opts) {
        super(args, opts);

        //Рекурсивный вызов функции вопросов
        this.promptCall = () => {
            return this
                .prompt(promptQuestions[currentQuestion])
                .then(answers => {
                    Object.keys(answers).forEach(prop => {
                        promptAnswers[prop] = answers[prop];
                        //this.log(`${promptQuestions[currentQuestion].message}:`, answers[prop]);
                    });

                    if (promptQuestions[currentQuestion].next) {
                        currentQuestion = promptQuestions[currentQuestion].next;
                        return this.promptCall();
                    }

                    return null;
                });
        };

        this.writeTemplates = () => {
            if (this.options.sprite) {
                //TODO:sprite option
            } else {
                const templatePromptAnswers =
                    (Object.keys(promptAnswers).length === 0) ? dataMock : promptAnswers;

                const langAndRegion = templatePromptAnswers.defaultLanguageAndRegion.split('-');
                templatePromptAnswers.defaultLanguage = langAndRegion[0];
                templatePromptAnswers.defaultRegion = langAndRegion[1];

                writing.writeTemplatesRoot.call(this, templatePromptAnswers);
                writing.writeTemplatesBuild.call(this, templatePromptAnswers);
                writing.writeTemplatesSrc.call(this, templatePromptAnswers);
            }
        };
    }

    initializing() {
        //Опции
        this.option('module', {
            desc: 'Добавить модуль из коробки в проект',
            alias: 'm',
            type: String
        });

        this.option('sprite', {
            desc: 'Добавить спрайт из коробки',
            alias: 's',
            type: Boolean
        });
    }

    prompting() {
        if (this.options.module) {
            //
        } else {
            const promptTest = this.promptCall()
                .then(() => {
                    const logAnswers = Object.keys(promptAnswers).map(answer => {
                        return `${answer}: ${promptAnswers[answer]}`;
                    });
                    this.log(`\nУказанные параметры:\n${logAnswers.join('\n')}`);

                    promptAnswers.isDebug = (typeof promptAnswers.isDebug === 'undefined') ? 'false' : promptAnswers.isDebug;
                    promptAnswers.isSyncModules = (promptAnswers.initType === 'Лендинг') ? 'true' : 'false';
                });

            return promptTest;
        }

        return null;
    }

    writing() {
        if (!this.options.module) this.writeTemplates();
    }

    // install() {
    //     this.log(this.options.module);
    //
    //     this.spawnCommand('npm', ['-v']);
    // }
}

module.exports = Box;

/*
TODO:base TODOs
Build:
Css in js
Base64
integrate fonts converter
Imageresize
show errors on toaster
change file-loader to asset modules
Parallel webpack
extract-loader: use esModule - https://github.com/peerigon/extract-loader/issues/111

Scss:
Timeline
Will change
media print

Pug:
use captureblock
Skip to main link
Test button & fieldset flex
Th add scope attr
Number format with thinsp
Role region on carousels
Screen reader test
Srcset special interface
Paragraph hash bookmarks
Menu custom content
Rel alternate for lang
languageNav mixin - https://stackoverflow.com/a/14548705/11917894

Js:
test init modules on self-closing tags
include type to jsdoc @throws
Add focus to scrolled section
Ion plugins ideas
Clock widget
Tabs swipe
Search input overlay
Emergence js
Transitionend
Sort table with aria-sort
Tofixed to prices
Autotab to next input
Detach unused elements
Tutorial tooltips tour
Form store data in localstorage
Sortable tables lists save to cookies
Sort anything
Page transitions
Endless scroll, pull to refresh
Month picker
Dial widget
Test click by keyboard on anything
https://tympanus.net/codrops/2018/04/25/particle-effects-for-buttons/
safe url pattern in bootstrap
share api

функция для пропорционального уменьшения шрифта у заголовков
поменять все image на picture
менять стили элементов в джс через цсс-переменные
сделать функцию прокрутки до элемента и дата атрибут
добавить контейнер для помещения тултипов
опция иконки у ссылки
не инициализировать селекты на таче
инициализировать селект независимо от нативного
инсталляция небольшого магазина
добавить возможность подгружать свг в интерфейс popup-gallery
обернуть все базовые формы в классы col и row
сделать отдельную настройку вида страницы "Заказ оформлен"
поправить в миксине меню настройку опций current, hasItems, collapse, image у внутренних уровней
Переименовать миксин productActions в productBuy и использовать productActions для кнопок около галереи
сделать функцию для обновления id в клонированных формах
разделить верстку детальной и списка новостей
*/