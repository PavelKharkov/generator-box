'use strict';

//Вопросы генератора
module.exports = {
    //Копирование проекта
    initType: {
        type: 'list',
        name: 'initType',
        message: 'Какие файлы должен содержать проект при инициализации?',
        choices: [
            'Скопировать все файлы',
            'Многостраничный сайт',
            'Лендинг'
        ],
        default: 'Скопировать все файлы',
        next: 'frontendPath'
    },

    //Путь к шаблону сайта
    frontendPath: {
        type: 'input',
        name: 'frontendPath',
        message: 'Путь к месту хранения файлов для разработки начиная с корня сайта (используется в файле manifest.json) (например: /local/templates/main/frontend)', /* eslint-disable-line max-len, vue/max-len */
        default: '/',
        next: 'staticPath'
    },

    //Путь к шаблону сайта
    staticPath: {
        type: 'input',
        name: 'staticPath',
        message: 'Путь к месту хранения скомпилированных файлов относительно папки разработки (например: ../static)',
        default: '../static',
        next: 'projectDistName'
    },

    //Название проекта
    projectDistName: {
        type: 'input',
        name: 'projectDistName',
        message: 'Название проекта (может содержать любые символы, кроме одинарных кавычек)',
        default: 'Интернет-магазин',
        next: 'projectName'
    },
    projectName: {
        type: 'input',
        name: 'projectName',
        message: 'Системное название проекта (только англ. символы и дефисы без пробелов)',
        default: 'box-project',
        next: 'projectDesc'
    },

    //Описание проекта
    projectDesc: {
        type: 'input',
        name: 'projectDesc',
        message: 'Краткое описание проекта',
        default: 'Новый интернет-магазин',
        next: 'defaultLanguageAndRegion'
    },

    //Язык и регион
    defaultLanguageAndRegion: {
        type: 'input',
        name: 'defaultLanguageAndRegion',
        message: 'Язык и регион проекта по умолчанию (через дефис)',
        default: 'ru-RU',
        next: 'gitUrl'
    },

    //Путь к гиту
    gitUrl: {
        type: 'input',
        name: 'gitUrl',
        message: 'Адрес git-репозитория',
        default: '',
        next: 'authorName'
    },

    //Разработчик
    authorName: {
        type: 'input',
        name: 'authorName',
        message: 'Имя/название разработчика',
        default: 'Internet studio 100up.ru',
        next: 'authorEmail'
    },
    authorEmail: {
        type: 'input',
        name: 'authorEmail',
        message: 'Email разработчика',
        default: 'info@100up.ru',
        next: 'authorSite'
    },
    authorSite: {
        type: 'input',
        name: 'authorSite',
        message: 'Сайт разработчика',
        default: 'https://www.100up.ru/'
    }
};