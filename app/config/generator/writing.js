'use strict';

const fs = require('node:fs');
const path = require('node:path');

const configPath = path.join(global.appPath, 'config');
const commonConfigPath = path.join(configPath, 'common');
const config = require(path.join(commonConfigPath, 'config.js'));

module.exports = {
    //Корневые файлы
    writeTemplatesRoot(promptAnswers) {
        const rootPath = path.join(global.appPath, '..');

        //Корневые файлы генератора
        const rootFiles = [
            '.husky',
            'babel.config.js',
            '.editorconfig',
            '.eslintrc.json',
            '.gitattributes',
            '.npmrc'
        ];
        this.fs.copy(
            rootFiles.map(file => {
                return this.templatePath(path.join(rootPath, file));
            }),
            this.destinationPath()
        );

        //Корневые файлы шаблона
        const templateRootFiles = [
            '.browserslistrc',
            '.eslintignore',
            '.gitignore',
            '.npmignore',
            '.stylelintrc',
            '.yarnclean',
            'yarn.lock'
        ];
        this.fs.copy(
            templateRootFiles.map(file => {
                return this.templatePath(file);
            }),
            this.destinationPath()
        );

        //Запись названия проекта в README
        this.fs.copyTpl(
            this.templatePath('README.md'),
            this.destinationPath('README.md'),
            promptAnswers
        );

        //Запись параметров проекта в package.json
        const packageJsonObject = JSON.parse(fs.readFileSync(this.templatePath('package.json'), config.encoding));
        packageJsonObject.name = promptAnswers.projectName;
        packageJsonObject.description = promptAnswers.projectDesc;
        packageJsonObject.repository.url = promptAnswers.gitUrl;
        const authorName = promptAnswers.authorName;
        const authorEmail = promptAnswers.authorEmail ? ` <${promptAnswers.authorEmail}>` : '';
        const authorSite = promptAnswers.authorSite ? ` (${promptAnswers.authorSite})` : '';
        packageJsonObject.author = authorName + authorEmail + authorSite;

        const generatorPackageJsonObject = JSON.parse(fs.readFileSync(this.templatePath(path.join(rootPath, 'package.json')), config.encoding));
        packageJsonObject.peerDependencies['generator-box'] = generatorPackageJsonObject.version;
        if (promptAnswers.staticPath !== '/') {
            packageJsonObject.scripts['purify-css'] =
                packageJsonObject.scripts['purify-css'].replace(/dist\//g, `${promptAnswers.staticPath.replace(/\\/, '/')}/`);
        }

        this.fs.writeJSON(
            this.destinationPath('package.json'),
            packageJsonObject
        );
    },

    //Папка разработки
    writeTemplatesBuild(promptAnswers) {
        this.fs.copy(
            this.templatePath(config.paths.build),
            this.destinationPath(config.paths.build),
            {
                globOptions: {
                    dot: true
                }
            }
        );
        const buildConfigFiles = [
            'paths.js',
            'pug.js',
            'server.js',
            'global-settings.json'
        ];
        this.fs.copy(
            buildConfigFiles.map(file => {
                return this.templatePath(path.join(commonConfigPath, file));
            }),
            this.destinationPath(config.paths.buildConfig, 'common')
        );
        this.fs.copyTpl(
            this.templatePath(path.join(commonConfigPath, 'config.js')),
            this.destinationPath(path.join(config.paths.buildConfig, 'common', 'config.js')),
            promptAnswers
        );
    },

    //Папки исходников
    writeTemplatesSrc(promptAnswers) {
        //Вспомогательная функция для копирования директории модулей
        const yieldModulesCopy = (modulesFiles, modulesDir) => {
            let indexFileContents = '';
            const isLibs = (modulesDir === config.paths.libs);
            const isPages = (modulesDir === config.paths.pages);

            const filesToCopy = modulesFiles.map((moduleDir, moduleIndex) => {
                let moduleDirName = moduleDir;
                let noIndexFile = false;
                let autoload = false;
                if (typeof moduleDir === 'object') {
                    moduleDirName = moduleDir.name;
                    noIndexFile = moduleDir.noIndex;
                    autoload = moduleDir.autoload;
                }

                const fileToCopy = this.templatePath(path.join(modulesDir, moduleDirName));
                if (
                    isLibs ||
                    noIndexFile ||
                    moduleDirName.endsWith(`.${config.devScriptExt}`) ||
                    moduleDirName.endsWith(`.${config.devTemplateExt}`)
                ) {
                    return fileToCopy;
                }

                const isIndexPage = isPages && (moduleDirName === 'index');
                const isSitemap = isPages && (moduleDirName === 'sitemap');
                if (isSitemap) return fileToCopy;

                const eslintComment = isIndexPage ? ' /* eslint-disable-line unicorn/import-index */' : '';
                const indexPageString = isIndexPage ? '/' : '';
                const autoloadString = autoload ? '/autoload.js' : '';
                indexFileContents += `import './${moduleDirName + indexPageString + autoloadString}';${eslintComment}`;
                if (moduleIndex !== (modulesFiles.length - 1)) {
                    indexFileContents += '\n';
                }

                return fileToCopy;
            });

            if (isPages) {
                indexFileContents += '\n\n//Подключение спрайтов происходит в этом файле, поэтому его надо импортировать последним\n';
                indexFileContents += 'import \'./sitemap\';';
            }

            this.fs.copy(
                filesToCopy,
                this.destinationPath(modulesDir)
            );
            this.fs.write(path.join(modulesDir, config.moduleScriptFile), indexFileContents);
        };

        //Корневые папки исходников
        const srcFiles = [
            '.eslintrc.json',
            config.moduleScriptFile,
            'pages.js',
            'polyfills.js',
            'prefetch.js',
            'vendor.js'
        ];
        this.fs.copy(
            srcFiles.map(file => {
                return this.templatePath(path.join(config.paths.src, file));
            }),
            this.destinationPath(config.paths.src)
        );

        //Вспомогательные папки скриптов
        this.fs.copy(
            this.templatePath(config.paths.baseScripts),
            this.destinationPath(config.paths.baseScripts)
        );

        //Вспомогательные папки стилей
        this.fs.copy(
            this.templatePath(config.paths.baseStyles),
            this.destinationPath(config.paths.baseStyles)
        );

        //Вспомогательные папки шаблонов разметки
        this.fs.copy(
            this.templatePath(config.paths.baseTemplates),
            this.destinationPath(config.paths.baseTemplates)
        );

        //Определение типа установки
        let initType;
        switch (promptAnswers.initType) {
            case 'Скопировать все файлы':
                initType = 'full';
                break;
            case 'Многостраничный сайт':
                initType = 'site';
                break;
            case 'Лендинг':
                initType = 'single-page';
                break;
            default:
                break;
        }

        switch (initType) {
            case 'full': //Скопировать все файлы
                //Компоненты
                this.fs.copy(
                    this.templatePath(config.paths.components),
                    this.destinationPath(config.paths.components)
                );

                //Данные
                this.fs.copy(
                    this.templatePath(config.paths.data),
                    this.destinationPath(config.paths.data)
                );

                //Шрифты
                this.fs.copy(
                    this.templatePath(config.paths.fonts),
                    this.destinationPath(config.paths.fonts)
                );

                //Шаблоны проекта
                this.fs.copy(
                    this.templatePath(config.paths.layout),
                    this.destinationPath(config.paths.layout)
                );

                //Вендорные модули
                this.fs.copy(
                    this.templatePath(config.paths.libs),
                    this.destinationPath(config.paths.libs)
                );

                //Страницы
                this.fs.copy(
                    this.templatePath(config.paths.pages),
                    this.destinationPath(config.paths.pages)
                );

                //Спрайты
                this.fs.copy(
                    this.templatePath(config.paths.sprites),
                    this.destinationPath(config.paths.sprites)
                );
                break;
            case 'site': //Многостраничный сайт
                {
                    //Компоненты
                    const componentsFiles = [
                        {
                            name: config.moduleScriptFile
                        },
                        {
                            name: config.includesFile
                        },
                        {
                            name: 'ajax',
                            autoload: true
                        },
                        {
                            name: 'alert',
                            autoload: true
                        },
                        {
                            name: 'animation',
                            autoload: true
                        },
                        {
                            name: 'badge'
                        },
                        {
                            name: 'bem',
                            noIndex: true
                        },
                        {
                            name: 'button'
                        },
                        {
                            name: 'card'
                        },
                        {
                            name: 'check'
                        },
                        {
                            name: 'close'
                        },
                        {
                            name: 'collapse',
                            autoload: true
                        },
                        //{
                        //    name: 'color',
                        //    autoload: true
                        //},
                        {
                            name: 'cookie'
                        },
                        {
                            name: 'counter',
                            autoload: true
                        },
                        {
                            name: 'date',
                            autoload: true
                        },
                        //{
                        //    name: 'datepicker',
                        //    autoload: true
                        //},
                        {
                            name: 'dropdown',
                            autoload: true
                        },
                        {
                            name: 'file',
                            autoload: true
                        },
                        {
                            name: 'float-label',
                            autoload: true
                        },
                        {
                            name: 'form',
                            autoload: true
                        },
                        {
                            name: 'gallery',
                            autoload: true
                        },
                        //{
                        //    name: 'history'
                        //},
                        //{
                        //    name: 'iframe',
                        //    autoload: true
                        //},
                        {
                            name: 'image',
                            autoload: true
                        },
                        //{
                        //    name: 'image-diff',
                        //    autoload: true
                        //},
                        {
                            name: 'input'
                        },
                        //{
                        //    name: 'layout'
                        //},
                        {
                            name: 'link'
                        },
                        {
                            name: 'list'
                        },
                        {
                            name: 'map',
                            autoload: true
                        },
                        {
                            name: 'mask',
                            autoload: true
                        },
                        {
                            name: 'menu',
                            noIndex: true
                        },
                        {
                            name: 'module',
                            noIndex: true
                        },
                        {
                            name: 'money'
                        },
                        {
                            name: 'nav'
                        },
                        {
                            name: 'pagination'
                        },
                        {
                            name: 'popover',
                            autoload: true
                        },
                        {
                            name: 'popup',
                            autoload: true
                        },
                        //{
                        //    name: 'popup-gallery',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'popup-magnific',
                        //    autoload: true
                        //},
                        {
                            name: 'popup-zoom-gallery',
                            autoload: true
                        },
                        {
                            name: 'preloader',
                            autoload: true
                        },
                        {
                            name: 'progress'
                        },
                        {
                            name: 'range',
                            autoload: true
                        },
                        //{
                        //    name: 'rating',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'scroll',
                        //    autoload: true
                        //},
                        {
                            name: 'select',
                            autoload: true
                        },
                        {
                            name: 'slideout',
                            autoload: true
                        },
                        {
                            name: 'slider',
                            autoload: true
                        },
                        //{
                        //    name: 'slider-owl',
                        //    autoload: true
                        //},
                        {
                            name: 'social',
                            autoload: true
                        },
                        {
                            name: 'svg'
                        },
                        {
                            name: 'table',
                            autoload: true
                        },
                        {
                            name: 'tabs',
                            autoload: true
                        },
                        {
                            name: 'toaster',
                            autoload: true
                        },
                        {
                            name: 'toggle',
                            autoload: true
                        },
                        {
                            name: 'touch',
                            autoload: true
                        },
                        {
                            name: 'url'
                        },
                        {
                            name: 'validation',
                            autoload: true
                        }
                        //{
                        //    name: 'video',
                        //    autoload: true
                        //}
                    ];
                    yieldModulesCopy(componentsFiles, config.paths.components);

                    //Данные
                    const dataFiles = [
                        config.gitkeepFile,
                        'catalog-item',
                        'catalog-menu',
                        'footer-menu',
                        'header-menu',
                        'index-slider',
                        'locations',
                        'logo',
                        'main-menu',
                        'payment-types'
                    ];
                    this.fs.copy(
                        dataFiles.map(file => {
                            return this.templatePath(path.join(config.paths.data, file));
                        }),
                        this.destinationPath(config.paths.data)
                    );

                    //Шрифты
                    this.fs.copy(
                        this.templatePath(path.join(config.paths.fonts, config.moduleScriptFile)),
                        this.destinationPath(path.join(config.paths.fonts, config.moduleScriptFile))
                    );
                    this.fs.write(path.join(config.paths.fonts, config.moduleScriptFile), '');

                    //Шаблоны проекта
                    const layoutFiles = [
                        {
                            name: config.moduleScriptFile
                        },
                        {
                            name: config.includesFile
                        },
                        //{
                        //    name: 'about'
                        //},
                        {
                            name: 'add-spaces',
                            noIndex: true
                        },
                        {
                            name: 'agreement-checkbox',
                            noIndex: true
                        },
                        {
                            name: 'alert-popup',
                            autoload: true
                        },
                        {
                            name: 'base'
                        },
                        {
                            name: 'breadcrumbs'
                        },
                        //{
                        //    name: 'browsers-warning'
                        //},
                        //{
                        //    name: 'builder'
                        //},
                        {
                            name: 'card-slider',
                            autoload: true
                        },
                        //{
                        //    name: 'cart',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'catalog',
                        //    autoload: true
                        //},
                        {
                            name: 'catalog-item',
                            autoload: true
                        },
                        //{
                        //    name: 'captcha',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'checkbox-tree'
                        //},
                        //{
                        //    name: 'compare',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'contacts',
                        //    autoload: true
                        //},
                        {
                            name: 'epilog',
                            noIndex: true
                        },
                        {
                            name: 'errorpage'
                        },
                        //{
                        //    name: 'externals',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'filter',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'float-nav',
                        //    autoload: true
                        //},
                        {
                            name: 'foot',
                            noIndex: true
                        },
                        {
                            name: 'footer'
                        },
                        {
                            name: 'form',
                            autoload: true
                        },
                        {
                            name: 'header',
                            autoload: true
                        },
                        {
                            name: 'icons',
                            noIndex: true
                        },
                        {
                            name: 'index-page',
                            autoload: true
                        },
                        {
                            name: 'location-select'
                        },
                        {
                            name: 'logo',
                            noIndex: true
                        },
                        {
                            name: 'main'
                        },
                        //{
                        //    name: 'main-preloader',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'news'
                        //},
                        //{
                        //    name: 'order',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'order-steps',
                        //    autoload: true
                        //},
                        {
                            name: 'password-show',
                            autoload: true
                        },
                        {
                            name: 'payment-types'
                        },
                        //{
                        //    name: 'personal',
                        //    autoload: true
                        //},
                        {
                            name: 'phones',
                            noIndex: true
                        },
                        {
                            name: 'popups'
                        },
                        //{
                        //    name: 'product',
                        //    autoload: true
                        //},
                        {
                            name: 'prolog',
                            noIndex: true
                        },
                        //{
                        //    name: 'scrollup',
                        //    autoload: true
                        //},
                        {
                            name: 'search-form',
                            autoload: true
                        },
                        //{
                        //    name: 'sidebar'
                        //},
                        {
                            name: 'slideout-menu',
                            autoload: true
                        },
                        {
                            name: 'state-toggler',
                            autoload: true
                        },
                        //{
                        //    name: 'subscribe-section'
                        //},
                        {
                            name: 'text-content'
                        },
                        {
                            name: 'toolkit',
                            noIndex: true
                        },
                        //{
                        //    name: 'top-controls'
                        //},
                        {
                            name: 'window'
                        }
                    ];
                    if (promptAnswers.frontendPath !== '/') {
                        layoutFiles.push({
                            name: 'bitrix',
                            autoload: true
                        });
                    }
                    yieldModulesCopy(layoutFiles, config.paths.layout);

                    //Вендорные модули
                    const libsFiles = [
                        //'animate-css',
                        'choices',
                        //'easyzoom',
                        //'googlemaps',
                        'inputmask',
                        //'jquery',
                        //'jquery-event-move',
                        //'magnific-popup',
                        //'masonry',
                        'mobile-detect',
                        'nouislider',
                        //'owl-carousel',
                        //'photoswipe',
                        //'pikaday',
                        'platform',
                        'popper',
                        //'scrollmagic',
                        //'svg4everybody',
                        'swiper',
                        //'twentytwenty',
                        'validate'
                        //'vue'
                    ];
                    yieldModulesCopy(libsFiles, config.paths.libs);

                    //Страницы
                    const pagesFiles = [
                        config.moduleScriptFile,
                        '404',
                        'index',
                        'sitemap',
                        'text'
                    ];
                    yieldModulesCopy(pagesFiles, config.paths.pages);

                    //Спрайты
                    this.fs.copy(
                        this.templatePath(path.join(config.paths.sprites, config.gitkeepFile)),
                        this.destinationPath(path.join(config.paths.sprites, config.gitkeepFile))
                    );
                }
                break;
            case 'single-page': //Лендинг
                {
                    //Компоненты
                    const componentsFiles = [
                        {
                            name: config.moduleScriptFile
                        },
                        {
                            name: config.includesFile
                        },
                        {
                            name: 'ajax',
                            autoload: true
                        },
                        {
                            name: 'alert',
                            autoload: true
                        },
                        {
                            name: 'animation',
                            autoload: true
                        },
                        //{
                        //    name: 'badge'
                        //},
                        {
                            name: 'bem',
                            noIndex: true
                        },
                        {
                            name: 'button'
                        },
                        //{
                        //    name: 'card'
                        //},
                        //{
                        //    name: 'check'
                        //},
                        {
                            name: 'close'
                        },
                        //{
                        //    name: 'collapse',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'color',
                        //    autoload: true
                        //},
                        {
                            name: 'cookie'
                        },
                        //{
                        //    name: 'counter',
                        //    autoload: true
                        //},
                        {
                            name: 'date',
                            autoload: true
                        },
                        //{
                        //    name: 'datepicker',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'dropdown',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'file',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'float-label',
                        //    autoload: true
                        //},
                        {
                            name: 'form',
                            autoload: true
                        },
                        //{
                        //    name: 'gallery',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'history'
                        //},
                        //{
                        //    name: 'iframe',
                        //    autoload: true
                        //},
                        {
                            name: 'image',
                            autoload: true
                        },
                        //{
                        //    name: 'image-diff',
                        //    autoload: true
                        //},
                        {
                            name: 'input'
                        },
                        //{
                        //    name: 'layout'
                        //},
                        {
                            name: 'link'
                        },
                        {
                            name: 'list'
                        },
                        //{
                        //    name: 'map',
                        //    autoload: true
                        //},
                        {
                            name: 'mask',
                            autoload: true
                        },
                        {
                            name: 'menu',
                            noIndex: true
                        },
                        {
                            name: 'module',
                            noIndex: true
                        },
                        //{
                        //    name: 'money'
                        //},
                        {
                            name: 'nav'
                        },
                        //{
                        //    name: 'pagination'
                        //},
                        //{
                        //    name: 'popover',
                        //    autoload: true
                        //},
                        {
                            name: 'popup',
                            autoload: true
                        },
                        //{
                        //    name: 'popup-gallery',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'popup-magnific',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'popup-zoom-gallery',
                        //    autoload: true
                        //},
                        {
                            name: 'preloader',
                            autoload: true
                        },
                        //{
                        //    name: 'progress'
                        //},
                        //{
                        //    name: 'range',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'rating',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'scroll',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'select',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'slideout',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'slider',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'slider-owl',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'social',
                        //    autoload: true
                        //},
                        {
                            name: 'svg'
                        },
                        //{
                        //    name: 'table',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'tabs',
                        //    autoload: true
                        //},
                        {
                            name: 'toaster',
                            autoload: true
                        },
                        //{
                        //    name: 'toggle',
                        //    autoload: true
                        //},
                        {
                            name: 'touch',
                            autoload: true
                        },
                        {
                            name: 'url'
                        },
                        {
                            name: 'validation',
                            autoload: true
                        }
                        //{
                        //    name: 'video',
                        //    autoload: true
                        //}
                    ];
                    yieldModulesCopy(componentsFiles, config.paths.components);

                    //Данные
                    const dataFiles = [
                        config.gitkeepFile,
                        'logo'
                    ];
                    this.fs.copy(
                        dataFiles.map(file => {
                            return this.templatePath(path.join(config.paths.data, file));
                        }),
                        this.destinationPath(config.paths.data)
                    );

                    //Шрифты
                    this.fs.copy(
                        this.templatePath(path.join(config.paths.fonts, config.moduleScriptFile)),
                        this.destinationPath(path.join(config.paths.fonts, config.moduleScriptFile))
                    );
                    this.fs.write(path.join(config.paths.fonts, config.moduleScriptFile), '');

                    //Шаблоны проекта
                    const layoutFiles = [
                        {
                            name: config.moduleScriptFile
                        },
                        {
                            name: config.includesFile
                        },
                        //{
                        //    name: 'about'
                        //},
                        {
                            name: 'add-spaces',
                            noIndex: true
                        },
                        {
                            name: 'agreement-checkbox',
                            noIndex: true
                        },
                        {
                            name: 'alert-popup',
                            autoload: true
                        },
                        {
                            name: 'base'
                        },
                        //{
                        //    name: 'breadcrumbs'
                        //},
                        //{
                        //    name: 'browsers-warning'
                        //},
                        //{
                        //    name: 'builder'
                        //},
                        //{
                        //    name: 'card-slider',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'cart',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'catalog',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'catalog-item',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'captcha',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'checkbox-tree'
                        //},
                        //{
                        //    name: 'compare',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'contacts',
                        //    autoload: true
                        //},
                        {
                            name: 'epilog',
                            noIndex: true
                        },
                        {
                            name: 'errorpage'
                        },
                        //{
                        //    name: 'externals',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'filter',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'float-nav',
                        //    autoload: true
                        //},
                        {
                            name: 'foot',
                            noIndex: true
                        },
                        //{
                        //    name: 'footer'
                        //},
                        {
                            name: 'form',
                            autoload: true
                        },
                        //{
                        //    name: 'header',
                        //    autoload: true
                        //},
                        {
                            name: 'icons',
                            noIndex: true
                        },
                        //{
                        //    name: 'index-page',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'location-select'
                        //},
                        {
                            name: 'logo',
                            noIndex: true
                        },
                        {
                            name: 'main'
                        },
                        //{
                        //    name: 'main-preloader',
                        //    noIndex: true
                        //},
                        //{
                        //    name: 'news'
                        //},
                        //{
                        //    name: 'order',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'order-steps',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'password-show',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'payment-types'
                        //},
                        //{
                        //    name: 'personal',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'phones',
                        //    noIndex: true
                        //},
                        {
                            name: 'popups'
                        },
                        //{
                        //    name: 'product',
                        //    autoload: true
                        //},
                        {
                            name: 'prolog',
                            noIndex: true
                        },
                        //{
                        //    name: 'scrollup',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'search-form',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'sidebar'
                        //},
                        //{
                        //    name: 'slideout-menu',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'state-toggler',
                        //    autoload: true
                        //},
                        //{
                        //    name: 'subscribe-section'
                        //},
                        {
                            name: 'text-content'
                        },
                        {
                            name: 'toolkit',
                            noIndex: true
                        },
                        //{
                        //    name: 'top-controls'
                        //},
                        {
                            name: 'window'
                        }
                    ];
                    if (promptAnswers.frontendPath !== '/') {
                        layoutFiles.push({
                            name: 'bitrix',
                            autoload: true
                        });
                    }
                    yieldModulesCopy(layoutFiles, config.paths.layout);

                    //Вендорные модули
                    const libsFiles = [
                        //'animate-css',
                        //'choices',
                        //'easyzoom',
                        //'googlemaps',
                        'inputmask',
                        //'jquery',
                        //'jquery-event-move',
                        //'magnific-popup',
                        //'masonry',
                        'mobile-detect',
                        //'nouislider',
                        //'owl-carousel',
                        //'photoswipe',
                        //'pikaday',
                        'platform',
                        //'popper',
                        //'scrollmagic',
                        //'svg4everybody',
                        //'swiper',
                        //'twentytwenty',
                        'validate'
                        //'vue'
                    ];
                    yieldModulesCopy(libsFiles, config.paths.libs);

                    //Страницы
                    const pagesFiles = [
                        config.moduleScriptFile,
                        '404',
                        'index',
                        'sitemap',
                        'text'
                    ];
                    yieldModulesCopy(pagesFiles, config.paths.pages);

                    //Спрайты
                    this.fs.copy(
                        this.templatePath(path.join(config.paths.sprites, config.gitkeepFile)),
                        this.destinationPath(path.join(config.paths.sprites, config.gitkeepFile))
                    );
                }
                break;
            default:
                break;
        }
    }
};