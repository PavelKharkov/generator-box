# generator-box

[Демо](http://verstka.100up.ru/box/) | [Документация: скрипты](http://verstka.100up.ru/box-docs/scripts/) | [Документация: шаблоны](http://verstka.100up.ru/box-docs/templates/)

## Требования:
Корректно работает с node.js 16.13.2 и выше и необходимо использовать [Yarn](https://yarnpkg.com/) 1.22.18 и выше

Предварительно нужно установить [Yeoman CLI](http://yeoman.io/)
```
npm i yo@4.3.0 -g
```

Для корректной работы IDE нужно установить глобально:
```bash
npm i @babel/eslint-parser eslint eslint-plugin-jest eslint-plugin-jsdoc eslint-plugin-unicorn eslint-plugin-vue sass stylelint stylelint-scss vue-eslint-parser -g
```

## Начало работы:
```bash
yarn install
yarn link
```

После этого в папке, где нужно развернуть сайт:
```bash
yo box
```

## Доступные команды:
```bash
# запуск webpack в режиме разработки
yarn dev

# компиляция файлов для релиза
yarn dist

# компиляция документации *
yarn docs [-t|-s] [-i]
# файлы документации генерируются в docs/. По умолчанию генерируется документация скриптов. C флагом -t генерируется документация шаблонов. С флагом -s — документация стилей. С флагом -i файлы документации не генерируются, но отображается дополнительная информация о результате генерации

# запуск линтера скриптов
yarn lint

# запуск линтера стилей
yarn lint-styles

# запуск линтера шаблонов
yarn lint-templates

# настраивание файлов для разработки коробки *
yarn mock-templates

# создание нового модуля
yarn module путь_к/модулю [-a]
# если не указана родительская папка, то модуль создается в app/templates/src/layout/. С флагом -a в папке модуля добавляется файл autoload.js

# проверка css-кода на наличие неиспользуемых классов
yarn purify-css

# рендер файла шаблона в html-файл в модуле
yarn render-template путь_к/модулю [код для рендера]

# запуск сервера конструктора сайтов
yarn server

# сортировка css-правил. Автоматически запускается перед компиляцией для релиза
yarn sort-styles

# запуск тестов
yarn test
# тесты выводятся в терминале и в файле app/templates/tests/results.html. Состояние покрытия тестов находится в app/templates/coverage/

# * — команды доступны только в генераторе
```